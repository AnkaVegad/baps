<%@ Page Title="" Language="C#" MasterPageFile="~/AP.master" AutoEventWireup="true" EnableEventValidation="false" CodeFile="ReportingLinks.aspx.cs" Inherits="ReportingLinks" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<!--Page Header-->
<table border="0" width="1095px">
    <tr>
        <td align="left">
            <asp:Label ID="Label11" Text="Reporting Links" runat="server" CssClass="page_header" Width="175px" />
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:Label ID="Label12" Text="Click a button to open an MBI report" runat="server" Width="375px" />
        </td>
    </tr>
    <tr><td style="font-size:3px">&nbsp;</td></tr>
</table>

<!-- Buttons-->
<table border="0" width="1100">
    <tr><td>
            <asp:Button ID="cmdActivityLevel" runat="server" Text="Activity Level Pivot" causesvalidation="false" Enabled="true"
                width="180px" CssClass="form_button" />&nbsp;
    </td></tr>
    <tr><td>
            <asp:Button ID="cmdVariance" runat="server" Text="Variance Pivot" causesvalidation="false" Enabled="true"
                width="180px" CssClass="form_button" />&nbsp;
    </td></tr>
    <tr><td>
            <asp:Button ID="cmdFullYearView" runat="server" Text="Full Year View Pivot" causesvalidation="false" Enabled="true"
                width="180px" CssClass="form_button" />&nbsp;
    </td></tr>
    <tr><td>
            <asp:Button ID="cmdAPTracker" runat="server" Text="A&P Tracker" causesvalidation="false" Enabled="true"
                width="180px" CssClass="form_button" />&nbsp;
    </td></tr>
</table><br />
    
</asp:Content>

