﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Drawing;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;

public partial class AdminMenu : System.Web.UI.Page
{
 
    protected void Page_Load(object sender, EventArgs e)
    {

    }

#region events

    protected void cmdSuppliers_Click(object sender, EventArgs e)
    {
        Response.Redirect("SupplierView.aspx");
    }

    protected void cmdUserAdmin_Click(object sender, EventArgs e)
    {
        Response.Redirect("UserAdminView.aspx");
    }

#endregion
}
