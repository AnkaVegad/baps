﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ProjectMaster : System.Web.UI.Page
{
    string strSingular = "Project";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            String UID = this.UniqueID.Replace("$", "_") + "_";
            grid.Attributes.Add("OnScroll", "savescroll('" + UID + "'); return false;");
            
            //populate selection dropdowns
            FillProjectYear();
            FillBudgetResponsibilityArea();
            FillBrand();
            FillCountry();
            FillPulsingPlanPriority();

            //set year 
            cboProjectYear.SelectedValue = Session["SelectedYear"].ToString();

            DataTable tbl = UserDataAccess.SelectUser(Session["CurrentUserName"].ToString());
            //set area 
            if (Session["SelectedBudgetResponsibilityAreaID"] == null)
            {
                if (tbl.Rows[0]["LastBudgetResponsibilityAreaID"] == DBNull.Value)
                //allows for user's first login when table entry will be blank
                {
                    Session["SelectedBudgetResponsibilityAreaID"] = Common.ADOLookup("BudgetResponsibilityAreaID", "qryUserAuthorisations", "UserName = '" + Session["CurrentUserName"].ToString().Replace("'", "''") + "'");
                }
                else
                {
                    Session["SelectedBudgetResponsibilityAreaID"] = tbl.Rows[0]["LastBudgetResponsibilityAreaID"].ToString();
                }
            }
            try { cboBudgetResponsibilityArea.SelectedValue = Session["SelectedBudgetResponsibilityAreaID"].ToString(); }
            catch { cboBudgetResponsibilityArea.SelectedValue = "0"; }

            if (Session["SelectedBrandID"] == null)
            {
                Session["SelectedBrandID"] = tbl.Rows[0]["LastBrandID"].ToString();
            }
            try { cboBrand.SelectedValue = Session["SelectedBrandID"].ToString(); }
            catch { cboBrand.SelectedValue = "0"; }

            mvw.ActiveViewIndex = 0;
            PopulateGrid();
        }

        //enable new and save button for admin and above
        if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 15)
        {
            cmdNew.Enabled = true;
            cmdEdit.Text = "Edit Project";
            cmdCopy.Enabled = true;
            cmdDelete.Enabled = true;
            cmdUpdate.Enabled = true;
        }
        else
        {
            cmdEdit.Text = "View Project";
        }
    }

    public void SetScroll()
    {
        String UID = this.UniqueID.Replace("$", "_") + "_";
        Page page = HttpContext.Current.CurrentHandler as Page;
        page.ClientScript.RegisterStartupScript(typeof(Page), "setscroll", "<script type='text/javascript'>setscroll('" + UID + "');</script>");
    }
    #region gridevents

    public void PopulateGrid()
    {
        //populate the project grid
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "PopulateProjectMaster";

        cmd.Parameters.AddWithValue("@Year", cboProjectYear.SelectedValue);
        cmd.Parameters.AddWithValue("@Area", cboBudgetResponsibilityArea.SelectedValue);
        cmd.Parameters.AddWithValue("@CountryCode", cboCountry.SelectedValue);
        cmd.Parameters.AddWithValue("@BrandID", cboBrand.SelectedValue);
        cmd.Parameters.AddWithValue("@PulsingPlanPriorityID", cboPulsingPlanPriority.SelectedValue);
        cmd.Parameters.AddWithValue("@SearchText", txtSearch.Text);
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());

        DataTable tbl1 = DataAccess.ExecuteSelectCommand(cmd);
        if (tbl1.Rows.Count == 0)
        {
            cmdEdit.Enabled = false;
            cmdDelete.Enabled = false;
            cmdCopy.Enabled = false;
        }
        else if (tbl1.Rows.Count > 0)
        {
            cmdEdit.Enabled = true;
            if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 15)
            {
                cmdEdit.Text = "Edit Project";
                cmdUpdate.Enabled = true;
                cmdDelete.Enabled = true;
                cmdCopy.Enabled = true;
            }
            else
            {
                cmdEdit.Text = "View Project";
            }
        }
        grd.DataSource = tbl1;
        grd.DataBind();

        //grd.SelectedIndex = 0;

        //Not needed as we have scrapped pagination
        //max rows retrieved message
        /*if (grd.PageCount == 6 && grd.PageSize == 20)
        {
            txtMessage.Text = "The maximum number of rows has been retrieved; to see other projects, change your selection criteria.";
        }
        else
        {
            if (grd.PageCount == 11 && grd.PageSize == 10)
            {
                txtMessage.Text = "The maximum number of rows has been retrieved; to see other projects, change your selection criteria.";
            }
            else
            {
                txtMessage.Text = String.Empty;
            }
        }*/
    }

    //protected void grd_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    HiddenSelectedProjectID.Value = grd.SelectedValue.ToString();
    //    //enable new and save button for admin and above
    //    /*if (Convert.ToInt16(Session["CurrentUserLevelID"]) >= 15)
    //    {
    //        cmdNew.Enabled = false;
    //        cmdEdit.Enabled = false;
    //        cmdCopy.Enabled = false;
    //        cmdDelete.Enabled = false;
    //    }*/
    //}


    protected void grd_Sorted(object sender, EventArgs e)
    {
        /*SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "PopulateProjectMaster";

        cmd.Parameters.AddWithValue("@Year", cboProjectYear.SelectedValue);
        cmd.Parameters.AddWithValue("@Area", cboBudgetResponsibilityArea.SelectedValue);
        cmd.Parameters.AddWithValue("@CountryCode", cboCountry.SelectedValue);
        cmd.Parameters.AddWithValue("@BrandID", cboBrand.SelectedValue);
        cmd.Parameters.AddWithValue("@PulsingPlanPriorityID", cboPulsingPlanPriority.SelectedValue);
        cmd.Parameters.AddWithValue("@SearchText", txtSearch.Text);
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());

        DataTable tbl1 = DataAccess.ExecuteSelectCommand(cmd);
        if (tbl1.Rows.Count == 0)
        {
            cmdEdit.Enabled = false;
            cmdDelete.Enabled = false;
            cmdCopy.Enabled = false;
        }
        else if (tbl1.Rows.Count > 0)
        {
            cmdEdit.Enabled = true;
            if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 15)
            {
                cmdEdit.Text = "Edit";
                cmdUpdate.Enabled = true;
                cmdDelete.Enabled = true;
                cmdCopy.Enabled = true;
            }
            else
            {
                cmdEdit.Text = "View";
            }
        }
        DataView dvSortedView = new DataView(tbl1);
        dvSortedView.Sort = e.SortExpression + " " + getSortDirectionString(e.SortDirection);
        grd.DataSource = dvSortedView;
        grd.DataBind();*/
    }
    /*
    private string getSortDirectionString(SortDirection sortDirection)
    {
        string newSortDirection = String.Empty;
        if (sortDirection == SortDirection.Ascending)
        {
            newSortDirection = "ASC";
        }
        else
        {
            newSortDirection = "DESC";
        }
        return newSortDirection;
    }
    */
    protected void grd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            //e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(this.grd, "Select$" + e.Row.RowIndex);
        }
        if (e.Row.RowIndex == Convert.ToInt32(HiddenSelectedProjectID.Value))
        {
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                e.Row.Cells[i].CssClass = "grid_selected";
            }
        }
    }

    #endregion

    #region events

    protected void cmdOK_Click(object sender, EventArgs e)
    //added 03/08/14 for panel
    {
        mvw.ActiveViewIndex = 0;
    }
    protected void cmdGo_Click(object sender, EventArgs e)
    {
        ProjectEdit1.ClearForm();
        HiddenSelectedProjectID.Value = "0";
        PopulateGrid();
    }

    protected void cmdEdit_Click(object sender, EventArgs e)
    {
        if (HiddenSelectedProjectID.Value != null)
        {
            grd.SelectedIndex = Convert.ToInt32(HiddenSelectedProjectID.Value);
            ProjectEdit1.PopulateForm(ProjectDataAccess.SelectProject(Convert.ToInt32(grd.SelectedValue)));
            if(Convert.ToInt32(Session["CurrentUserLevelID"].ToString()) >= 15)
                ProjectEdit1.ToggleControlState(false);
            else
                ProjectEdit1.ToggleControlState(true);
            mvw.ActiveViewIndex = 1;
            lblHeader.Text = "Edit " + strSingular;
        }
    }

    protected void cmdCopy_Click(object sender, EventArgs e)
    {
        if (HiddenSelectedProjectID.Value != null)
        {
            grd.SelectedIndex = Convert.ToInt32(HiddenSelectedProjectID.Value);
            ProjectEdit1.PopulateCopy(ProjectDataAccess.SelectProject(Convert.ToInt32(grd.SelectedValue)));
            mvw.ActiveViewIndex = 1;
            lblHeader.Text = "New " + strSingular;
        }
    }

    protected void cmdUpdate_Click(object sender, EventArgs e)
    {
        string strMessage;
        //try
        //{
        if (ProjectEdit1.ProjectIDValue == "0") //New
        {
            strMessage = ProjectEdit1.CreateProject();
            if (strMessage.Equals("") || strMessage.ToUpper().Equals("[NONE]"))
            //completed without errors
            {
                // update grid
                PopulateGrid();
                mvw.ActiveViewIndex = 0;
            }
            else
            {
                if (strMessage == "Not all customers were added")
                {
                    //save anyway and close
                    grd.DataBind();
                    lblMessage2.Text = strMessage;
                    mp1.Show();
                    //mvw.ActiveViewIndex = 0;
                }
                else
                {
                    MsgBox("Error", strMessage);
                }
            }
            
        }
        else
        {
            int intProjectID = Convert.ToInt32(ProjectEdit1.ProjectIDValue);
            strMessage = ProjectEdit1.UpdateProject();
            if (strMessage.Equals("") || strMessage.ToUpper().Equals("[NONE]"))
            //completed without errors
            {
                mvw.ActiveViewIndex = 0;
                //update single line in grid rather than refresh
                /*Project p = ProjectDataAccess.SelectProject(intProjectID);
                grd.SelectedRow.Cells[1].Text = p.ProjectName;
                grd.SelectedRow.Cells[2].Text = p.ProjectYear;
                grd.SelectedRow.Cells[3].Text = p.ProjectOwner;
                grd.SelectedRow.Cells[4].Text = Common.ADOLookup("BudgetResponsibilityAreaName", "tblBudgetResponsibilityArea", "BudgetResponsibilityAreaID = " + p.BudgetResponsibilityAreaID);
                grd.SelectedRow.Cells[5].Text = Common.ADOLookup("CountryName", "tblCountry", "CountryCode = '" + p.CountryCode + "'");
                grd.SelectedRow.Cells[6].Text = Common.ADOLookup("PulsingPlanPriorityName", "tblPulsingPlanPriority", "PulsingPlanPriorityID = " + p.PulsingPlanPriorityID);
                grd.SelectedRow.Cells[7].Text = (new System.Globalization.DateTimeFormatInfo().GetMonthName(Convert.ToInt32(p.ProjectStartMonth))).ToString().Substring(0,3) +
                                          '-' + (new System.Globalization.DateTimeFormatInfo().GetMonthName(Convert.ToInt32(p.ProjectEndMonth))).ToString().Substring(0,3);*/
                PopulateGrid();
                SetScroll();
                //lblMessage2.Text = "Project changes saved.";
                //mp1.Show();
            }
            else
            {
                MsgBox("Error", strMessage);
            }
        }
        //}
        //catch
        //{
        //    MsgBox("Error", "Could not save " + strSingular);
        //}
    }

    protected void cmdNew_Click(object sender, EventArgs e)
    {
        //enable controls for new record
        ProjectEdit1.NewRecord();
        lblHeader.Text = "New " + strSingular;

        grd.SelectedIndex = -1;
        mvw.ActiveViewIndex = 1;
    }

    protected void cmdDelete_Click(object sender, EventArgs e)
    {
        try
        {
            if (HiddenSelectedProjectID.Value != null)
            {
                grd.SelectedIndex = Convert.ToInt32(HiddenSelectedProjectID.Value);
                ProjectDataAccess.DeleteProject(Convert.ToInt32(grd.SelectedValue));
                ProjectEdit1.ClearForm();
                grd.SelectedIndex = 0;
                HiddenSelectedProjectID.Value = "0";
                // update grid
                PopulateGrid();
            }
        }
        catch //if item has non-cascaded dependencies
        {
            MsgBox("Error", "Could not delete " + strSingular);
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        ProjectEdit1.ClearForm();
        //ResetButtons();
        mvw.ActiveViewIndex = 0;
        PopulateGrid();
        SetScroll();
    }

    #endregion

    #region methods

    protected void MsgBox(string strTitle, string strMessage)
    {
        ClientScript.RegisterStartupScript(this.GetType(), strTitle, "alert('" + Common.Left(strMessage, 35) + "');", true);
    }

    #endregion

    #region dropdowns

    protected void FillBudgetResponsibilityArea()
    //fill BudgetResponsibilityArea dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillBudgetResponsibilityArea";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboBudgetResponsibilityArea;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillBrand()
    //fill brand dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillBrand";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboBrand;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillProjectYear()
    //fill year dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillYear";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboProjectYear;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillCountry()
    //fill Country dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillCountry";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboCountry;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillPulsingPlanPriority()
    //fill PulsingPlanPriority dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillPulsingPlanPriority";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboPulsingPlanPriority;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void cboBudgetResponsibilityArea_TextChanged(object sender, EventArgs e)
    {
        ProjectEdit1.ClearForm();
        HiddenSelectedProjectID.Value = "0";
        PopulateGrid();
    }

    protected void cboBrand_TextChanged(object sender, EventArgs e)
    {
        ProjectEdit1.ClearForm();
        HiddenSelectedProjectID.Value = "0";
        PopulateGrid();
    }

    protected void cboProjectYear_TextChanged(object sender, EventArgs e)
    {
        ProjectEdit1.ClearForm();
        HiddenSelectedProjectID.Value = "0";
        PopulateGrid();
    }

    protected void cboCountry_TextChanged(object sender, EventArgs e)
    {
        ProjectEdit1.ClearForm();
        HiddenSelectedProjectID.Value = "0";
        PopulateGrid();
    }

    protected void cboPulsingPlanPriority_TextChanged(object sender, EventArgs e)
    {
        ProjectEdit1.ClearForm();
        HiddenSelectedProjectID.Value = "0";
        PopulateGrid();
    }

    #endregion
}
