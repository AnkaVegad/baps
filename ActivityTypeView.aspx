﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AP_rs.master" AutoEventWireup="true" EnableEventValidation="false" CodeFile="ActivityTypeView.aspx.cs" Inherits="ActivityTypeView" %>

<%@ Register src="User_Controls/ActivityTypeGrid.ascx" tagname="ActivityTypeGrid" tagprefix="uc1" %>
<%@ Register src="User_Controls/ActivityTypeEdit.ascx" tagname="ActivityTypeEdit" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:MultiView ID="mvw" runat="server">
    <asp:View ID="vw1" runat="server">
        <uc1:ActivityTypeGrid ID="ActivityTypeGrid1" runat="server" />
        <div id="footer">
            <br />&nbsp;   
            <!--Grid Buttons-->
            <asp:Button ID="cmdNew" runat="server" Text="New Activity Type" causesvalidation="false"
                onclick="cmdNew_Click" CssClass="form_button" enabled="true" width="120px" />
            <asp:Button ID="cmdEdit" runat="server" Text="Edit Activity Type" causesvalidation="false"
                onclick="cmdEdit_Click" CssClass="form_button" enabled="true" width="120px" />
            <asp:Button ID="cmdDelete" runat="server" Text="Delete Activity Type" causesvalidation="false" width="120px" 
                onclick="cmdDelete_Click" OnClientClick="return confirm('Are you sure you want to delete this activity type?');"
                CssClass="form_button" Enabled="true" />
            <asp:Button ID="cmdGLHierarchy" runat="server" Text="GL Hierarchy" causesvalidation="false" width="120px" 
                CssClass="form_button" Enabled="true" PostBackUrl="~/GLHierarchyView.aspx" />
        </div>
    </asp:View>
    <asp:View ID="vw2" runat="server">
        <!--Edit Area-->
        <uc2:ActivityTypeEdit ID="ActivityTypeEdit1" runat="server" />
        <div id="footer">
            <br />&nbsp;   
            <asp:Button ID="cmdUpdate" runat="server" Text="Save" 
                onclick="cmdUpdate_Click" CssClass="form_button" Enabled="true" validationgroup="ActivityType" />&nbsp;
            <asp:Button ID="cmdCancel" runat="server" Text="Cancel" causesvalidation="false"
                onclick="cmdCancel_Click" CssClass="form_button" Enabled="true" />
        </div>
    </asp:View>
</asp:MultiView>
</asp:Content>

