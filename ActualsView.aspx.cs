﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb;

public partial class ActualsView: System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //check session not expired and high enough user level 
        if (Session["SelectedProjectToCustomerID"] == null || Convert.ToInt16(Session["CurrentUserLevelID"]) >= 5)
        {
            Response.Redirect("ProjectView.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                FillYear();
                //get current year from session variable set in global.asax
                cboYear.SelectedValue = Session["SelectedYear"].ToString();
                FillActualsMonthYear();
            }
            //Populate the snapshot grids
            grd.DataBind();
            //grdBudget.DataBind();
        }
    }

#region events

    protected void cmdUploadTCC_Click(object sender, EventArgs e)
    //upload actuals
    {
        if (uplTCC.HasFile)
            try
            {
                //clear staging table
                RebateHeaderDataAccess.ClearRebatesInput();
                //Step 1 - upload the file to the server, Uploads folder
                string strFilename = uplTCC.FileName;
                Label70013.Text = strFilename;
                uplTCC.SaveAs(Server.MapPath("Uploads/" + strFilename));
                //Step 2 - extract the data from file and put into staging table
                string strMessage = ExtractExcel(strFilename, "Sheet1", "S1P_Z2SDREBATES");
                if (strMessage == "OK")
                {
                    //Step 3 - process data in staging table
                    int intRecordsAffected = RebateHeaderDataAccess.InsertRebateActuals(Session["CurrentUserName"].ToString(), cboActualsMonthYear.SelectedValue);
                    Label70013.Text = "TCC Actuals upload was successful";
                    //Populate the actuals grids
                    grd.DataBind();
                    //grdBudget.DataBind();
                }
                else
                {
                    Label70013.Text = "ERROR: " + strMessage;
                }
            }
            catch (Exception ex)
            {
                Label70013.Text = "ERROR: " + ex.Message.ToString();        
            } 
        else
        {
            Label70013.Text = "You have not specified a file.";
        }
    }

#endregion

#region gridevents

    protected void grd_Sorted(object sender, EventArgs e)
    {

    }

    protected void grd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;
            //highlight on hover
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            e.Row.Attributes["onclick"] = cs.GetPostBackClientHyperlink(this.grd, "Select$" + e.Row.RowIndex);
        }
    }

#endregion

#region dropdowns

    protected void FillYear()
    //fill year dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillYear";
        cmd.Parameters.AddWithValue("@ZeroValueText", "[None]");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboYear;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillActualsMonthYear()
    //fill year dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillActualsMonthYear";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboActualsMonthYear;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void cboYear_TextChanged(object sender, EventArgs e)
    {

    }

#endregion

#region methods

    protected string ExtractExcel(string strFilename, string strSheetname, string strTablename)
    //note: works for MS Excel 2003 (.xls) files only at present
    {
        try
        {
            string xConnStr = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source=" + Server.MapPath("Uploads/" + strFilename) + ";" + "Extended Properties=Excel 8.0;";
            using (OleDbConnection connection = new OleDbConnection(xConnStr))
            {
                OleDbCommand command = new OleDbCommand("Select * FROM [" + strSheetname + "$]", connection);
                connection.Open();
                // Create DbDataReader to Data Worksheet
                using (OleDbDataReader dr = command.ExecuteReader())
                {
                    // SQL Server Connection String
                    string sqlConnectionString = APConfiguration.DbConnectionString;
                    // Bulk Copy to SQL Server
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(sqlConnectionString))
                    {
                        bulkCopy.DestinationTableName = strTablename;
                        bulkCopy.WriteToServer(dr);
                    }
                }
            }
            return "OK";
        }
        catch (Exception ex)
        {
            return ex.Message.ToString();
        }
    }
    
    protected void MsgBox(string strTitle, string strMessage)
    {
        ClientScript.RegisterStartupScript(this.GetType(), strTitle, "alert('" + Common.Left(strMessage, 35) + "');", true);
    }

#endregion
}
