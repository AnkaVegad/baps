﻿using System;
using System.Data;
using System.Data.SqlClient;

public partial class GLHierarchyView : System.Web.UI.Page

{
    string strSingular = "GL Hierarchy";

    protected void Page_Load(object sender, EventArgs e)
    {
        //check session not expired and high enough user level
        if (Session["SelectedProjectToCustomerID"] == null || Convert.ToInt16(Session["CurrentUserLevelID"]) >= 5)
        {
            Response.Redirect("ProjectView.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                mvw.ActiveViewIndex = 0;
                GLHierarchyGrid1.HeaderText = strSingular;
            }
        }
    }

#region events

    protected void cmdUpdate_Click(object sender, EventArgs e)
    {
        string strMessage;
        //try 
        //{
            if (Page.IsValid)
            {
                if (Session["Mode"].ToString() == "New")
                {
                    strMessage = GLHierarchyEdit1.InsertGLHierarchy();
                }
                else
                {
                    strMessage = GLHierarchyEdit1.UpdateGLHierarchy();
                }
                if (strMessage != "")
                {
                    MsgBox("Message", strMessage);
                }
                else
                {
                    GLHierarchyGrid1.RefreshGrid();
                    mvw.ActiveViewIndex = 0;
                }
            }
        //}
        //catch 
        //{
        //    MsgBox("Message", "Could not save " + strSingular + ".");
        //}
    }

    protected void cmdNew_Click(object sender, EventArgs e)
    {
        mvw.ActiveViewIndex = 1;
        //enable controls for new record
        GLHierarchyEdit1.NewRecord();
        Session["Mode"] = "New";
        GLHierarchyEdit1.HeaderText = Session["Mode"].ToString() + " " + strSingular;
    }

    protected void cmdEdit_Click(object sender, EventArgs e)
    {
        if (GLHierarchyGrid1.SelectedValue != null)
        {
            int intSelectedGLHierarchy = Convert.ToInt32(GLHierarchyGrid1.SelectedValue);
            GLHierarchy c = GLHierarchyDataAccess.SelectGLHierarchy(intSelectedGLHierarchy);
            GLHierarchyEdit1.PopulateForm(c);
            mvw.ActiveViewIndex = 1;
            Session["Mode"] = "Edit";
            GLHierarchyEdit1.HeaderText = Session["Mode"].ToString() + " " + strSingular;
        }
    }

    protected void cmdDelete_Click(object sender, EventArgs e)
    {
        //try //in case referential integrity prevents deletion
        //{
            GLHierarchyDataAccess.DeleteGLHierarchy(Convert.ToInt32(GLHierarchyGrid1.SelectedValue));
            GLHierarchyEdit1.ClearControls();
            // update grid
            GLHierarchyGrid1.DataBind();
        //}
        //catch
        //{
        //    MsgBox("Message", "Could not delete " + strSingular); 
        //}
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        GLHierarchyEdit1.ClearControls();
        mvw.ActiveViewIndex = 0;
    }

    protected void cmdCreate_Click(object sender, EventArgs e)
    {
        try
        {
            //execute the create command
            SqlCommand cmd = DataAccess.CreateCommand();
            cmd.CommandType = CommandType.Text;
            string strSQL = "INSERT INTO tblGLHierarchyItem (GLHierarchyItemName, GLHierarchyLevel) VALUES ('" + txtNewHierarchyItem.Text.Replace("'", "''") +"', '" + txtLevel.Text + "')";
            cmd.CommandText = strSQL;
            int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
            txtNewHierarchyItem.Text = "";
            txtLevel.Text = "";
            GLHierarchyEdit1.FillGLLevels();
            MsgBox("Message", "Item added");
        }
        catch
        {
            MsgBox("Message", "Item could not be added");
        }
    }

#endregion

#region methods

    protected void MsgBox(string strTitle, string strMessage)
    {
        ClientScript.RegisterStartupScript(this.GetType(), strTitle, "alert('" + Common.Left(strMessage, 35) + "');", true);
    }

#endregion

}
