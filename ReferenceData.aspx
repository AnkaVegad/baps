<%@ Page Title="" Language="C#" MasterPageFile="~/AP.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<!--Page Header-->
<table border="0" width="1095px">
    <tr>
        <td align="left">
            <asp:Label ID="Label11" Text="Maintenance Menu" runat="server" CssClass="page_header" Width="175px" />
        </td>
    </tr>
    <tr><td style="font-size:3px">&nbsp;</td></tr>
</table>

<!-- Buttons-->
<table border="0" width="1100">
    <tr><td>
            <asp:Button ID="cmdCustomers" runat="server" Text="Customers" causesvalidation="false" Enabled="true"
                width="130px" CssClass="form_button" Visible="true" PostBackUrl="CustomerView.aspx" />&nbsp;
    </td></tr>
    <tr><td>
            <asp:Button ID="cmdCustomerGroups" runat="server" Text="Customer Groups" causesvalidation="false" Enabled="true"
                width="130px" CssClass="form_button" Visible="true" PostBackUrl="CustomerGroupView.aspx" />&nbsp;
    </td></tr>
    <tr><td>
            <asp:Button ID="cmdProducts" runat="server" Text="Product Hierarchy" causesvalidation="false" Enabled="true"
                width="130px" CssClass="form_button" Visible="true" PostBackUrl="ProductHierarchyView.aspx" />&nbsp;
    </td></tr>
    <tr><td>
            <asp:Button ID="cmdBrands" runat="server" Text="Brands" causesvalidation="false" Enabled="true"
                width="130px" CssClass="form_button" Visible="true" PostBackUrl="BrandView.aspx" />&nbsp;
    </td></tr>
    <tr><td>
            <asp:Button ID="cmdSubBrands" runat="server" Text="Brand-Markets" causesvalidation="false" Enabled="true"
                width="130px" CssClass="form_button" Visible="true" PostBackUrl="SubBrandView.aspx" />&nbsp;
    </td></tr>
    <tr><td>
            <asp:Button ID="cmdActivityTypes" runat="server" Text="Activity Types" causesvalidation="false" Enabled="true"
                width="130px" CssClass="form_button" PostBackUrl="ActivityTypeView.aspx" />&nbsp;
    </td></tr>
    <tr><td>
        <asp:Button ID="cmdGLCodeHierarchy" runat="server" Text="GL Hierarchy" causesvalidation="false" Enabled="true"
            width="130px" CssClass="form_button" PostbackURL="GLHierarchyView.aspx" />&nbsp;
    </td></tr>
    <tr><td>
            <asp:Button ID="cmdForecastSnapshots" runat="server" Text="Generate Snapshots" causesvalidation="false" Enabled="true"
                width="130px" CssClass="form_button" PostBackUrl="ForecastSnapshotView.aspx" />&nbsp;
    </td></tr>
    <tr><td>
        <asp:Button ID="cmdRebateActuals" runat="server" Text="Actuals" causesvalidation="false" Enabled="true"
            width="130px" CssClass="form_button" PostbackURL="ActualsView.aspx" />&nbsp;
    </td></tr>
    <tr><td>
        <asp:Button ID="cmdSettings" runat="server" Text="Settings" causesvalidation="false" Enabled="true"
            width="130px" CssClass="form_button" PostbackURL="OptionView.aspx" />&nbsp;
    </td></tr>

</table><br />
    
</asp:Content>

