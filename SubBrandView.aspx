﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AP.master" AutoEventWireup="true" EnableEventValidation="false" CodeFile="SubBrandView.aspx.cs" Inherits="SubBrandView" %>

<%@ Register src="User_Controls/SubBrandGrid.ascx" tagname="SubBrandGrid" tagprefix="uc1" %>
<%@ Register src="User_Controls/SubBrandEdit.ascx" tagname="SubBrandEdit" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:MultiView ID="mvw" runat="server">
    <asp:View ID="vw1" runat="server">
        <uc1:SubBrandGrid ID="SubBrandGrid1" runat="server" />
        <!--Grid Buttons-->
        <asp:Button ID="cmdNew" runat="server" Text="New Brand-Market" causesvalidation="false"
            onclick="cmdNew_Click" CssClass="form_button" enabled="true" width="150px" />
        <asp:Button ID="cmdEdit" runat="server" Text="Edit Brand-Market" causesvalidation="false"
            onclick="cmdEdit_Click" CssClass="form_button" enabled="true" width="150px" />
        <asp:Button ID="cmdDelete" runat="server" Text="Delete Brand-Market" causesvalidation="false" width="150px" 
            onclick="cmdDelete_Click" OnClientClick="return confirm('Are you sure you want to delete this Brand-Market?');"
            CssClass="form_button" Enabled="true" /><br /><br />
    </asp:View>
    <asp:View ID="vw2" runat="server">
        <!--Edit Area-->
        <uc2:SubBrandEdit ID="SubBrandEdit1" runat="server" />
        <asp:Button ID="cmdUpdate" runat="server" Text="Save" 
            onclick="cmdUpdate_Click" CssClass="form_button" Enabled="true" validationgroup="SubBrand" />&nbsp;
        <asp:Button ID="cmdCancel" runat="server" Text="Cancel" causesvalidation="false"
            onclick="cmdCancel_Click" CssClass="form_button" Enabled="true" />
    </asp:View>
</asp:MultiView>
</asp:Content>

