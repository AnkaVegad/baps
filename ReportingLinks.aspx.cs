﻿using System;


public partial class ReportingLinks : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["SelectedProjectToCustomerID"] == null) //check for session expiry
        {
            Response.Redirect("ProjectView.aspx");
        }
        if (!IsPostBack)
        {
            cmdActivityLevel.PostBackUrl = Common.ADOLookup("OptionValue", "tblOption", "OptionName = 'ActivityLevelPivotURL'");
            cmdFullYearView.PostBackUrl = Common.ADOLookup("OptionValue", "tblOption", "OptionName = 'FullYearViewPivotURL'");
            cmdVariance.PostBackUrl = Common.ADOLookup("OptionValue", "tblOption", "OptionName = 'VariancePivotURL'");
            cmdAPTracker.PostBackUrl = Common.ADOLookup("OptionValue", "tblOption", "OptionName = 'APTrackerURL'");
        }
    }

}
