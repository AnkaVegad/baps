﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Drawing;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    string strSingular = "Activity";
    string strPlural = "Activities";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        //check for session expired
        if (Session["SelectedProjectToCustomerID"] == null)
        {
            Response.Redirect("ProjectView.aspx");
        }
        if (Page.IsPostBack)
        {
            //populate the headings
            ProjectID.Text = cboProject.SelectedValue;
            CustomerID.Text = cboCustomer.SelectedValue;
        }
        else //not postback
        {
            //clear the clipboard
            Session["SelectedItems"] = null;
            //retrieve the ProjectID and CustomerID for this ProjectToCustomer record
            DataTable tbl = ProjectDataAccess.SelectProjectToCustomer(Convert.ToInt32(Session["SelectedProjectToCustomerID"]));
            DataRow dr = tbl.Rows[0];
            ProjectID.Text =  dr["ProjectID"].ToString();
            CustomerID.Text = dr["CustomerID"].ToString();
            BudgetResponsibilityAreaID.Text = dr["BudgetResponsibilityAreaID"].ToString();
            ProjectToCustomerID.Text = Session["SelectedProjectToCustomerID"].ToString();
            //populate dropdowns
            FillView();
            FillRows();
            FillActivityType(0, Convert.ToInt32(BudgetResponsibilityAreaID.Text), 1);
            //Retrieve and set user's last selected view
            tbl = UserDataAccess.SelectUser(Session["CurrentUserName"].ToString());
            try
            {
                cboView.SelectedValue = tbl.Rows[0]["ActivityView"].ToString();
                cboRows.SelectedValue = tbl.Rows[0]["NoOfRows"].ToString();
            }
            catch //if can't find user's settings
            {
                cboView.SelectedValue = APConfiguration.DefaultActivityView;
                cboView.SelectedValue = APConfiguration.DefaultActivityRows;
            }
            mvw.ActiveViewIndex = 0;
        }
        //populate the header values and annual summary
        DataTable t = ActivityDataAccess.SelectActivityViewHeader(int.Parse(ProjectID.Text), int.Parse(CustomerID.Text));
        DataRow r = t.Rows[0];
        IONumber.Text = r["IONumber"].ToString();
        ProjectName.Text = r["ProjectName"].ToString();
        CustomerName.Text = r["CustomerName"].ToString();
        ProjectYear.Text = r["ProjectYear"].ToString();
        AnnualTotals1.PopulateAnnualValues(r);
        //populate the drop-downs for navigation criteria
        FillProjectsForSelectedCustomer();
        FillCustomersForSelectedProject();

        //and select current values; Try ... Catch added 06/12/12 in case user is read-only in a project or customer they don't have authorisations for
        try { cboCustomer.SelectedValue = CustomerID.Text; }
        catch { }
        try { cboProject.SelectedValue = ProjectID.Text; }
        catch { }
        
        //populate and format the activity detail grid 
        grd.DataBind();
        ShowGridColumns(cboView.SelectedValue);
        grd.PageSize = Convert.ToInt16(cboRows.SelectedValue);
        //enable buttons for all except reader
        if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 35 && Common.CheckAuthorisations(Convert.ToInt32(Session["SelectedProjectToCustomerID"]), Session["CurrentUserName"].ToString()) > 0)
        {
            if (IONumber.Text != String.Empty) //can't add activity if no IO
            {
                cmdNewAP.Enabled = true;
                cmdNewTCC.Enabled = true;
                cmdNewCpn.Enabled = true;
                cmdNewPR.Enabled = true;
            }
            else
            {
                cmdNewAP.Enabled = false;
                cmdNewTCC.Enabled = false;
                cmdNewCpn.Enabled = false;
                cmdNewPR.Enabled = false;
            }
            cmdCopyAll.Enabled = true;
            cmdPaste.Enabled = true;
        }
        else
        {
            cmdNewAP.Enabled = false;
            cmdNewTCC.Enabled = false;
            cmdNewCpn.Enabled = false;
            cmdNewPR.Enabled = false;
            cmdCopyAll.Enabled = false;
            cmdPaste.Enabled = false;
        }
    }

#region gridevents

    protected void grd_SelectedIndexChanged(object sender, EventArgs e)
    {
        //edit record
        Activity a = ActivityDataAccess.SelectActivity(Convert.ToInt32(grd.SelectedValue));
        ActivityEdit1.PopulateForm(a);

        //enable/disable buttons unless read only
        if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 35 && Common.CheckAuthorisations(Convert.ToInt32(Session["SelectedProjectToCustomerID"]), Session["CurrentUserName"].ToString()) > 0)
        {
            cmdUpdate.Enabled = true;
            cmdDelete.Enabled = true;
            cmdCopy.Enabled = true;
            //cmdCopyAll.Enabled = true; should always be enabled if not a reader
            //cmdPaste.Enabled = true; should always be enabled if not a reader
            //if [A&P] enable PRF and receipting buttons

            //changed 01/03/12 to allow Rebate maint functionality
            cmdPOHeader.Enabled = true; 
            cmdReceipt.Enabled = (a.SpendTypeID == 1);
            if (a.SpendTypeID == 1 && a.POID == String.Empty)
            {
                cmdSupplierLookup.Enabled = true;
            }
            else
            {
                cmdSupplierLookup.Enabled = false;
            }
            //cmdSupplierLookup.Enabled = (a.POID == String.Empty);
            cmdActivityTypeLookup.Enabled = true;
        }
        cmdCancel.Enabled = true;
        cmdNewAP.Enabled = false;
        cmdNewTCC.Enabled = false;
        cmdNewCpn.Enabled = false;

        //store selected activity in session variable
        Session["SelectedActivityID"] = Convert.ToInt32(grd.SelectedValue);
    }

    protected void grd_Sorted(object sender, EventArgs e)
    {
        //grd.SelectedIndex = -1;
    }
    
    protected void grd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(this.grd, "Select$" + e.Row.RowIndex);
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[1].Text = "Total Forecast";
            //display the totals amended AH 18/01/11 to fix totals calculation issue when project or customer changed in activity view
            int intProjectToCustomerID = Convert.ToInt32(Common.ADOLookup("ProjectToCustomerID", "tblProjectToCustomer", "ProjectID = " + cboProject.SelectedValue + " AND CustomerID = " + cboCustomer.SelectedValue));
            DataTable tbl = ActivityDataAccess.SelectForecastTotals(intProjectToCustomerID);
            if (tbl.Rows.Count > 0)
            {
                DataRow r = tbl.Rows[0];
                for (int i = 1; i <= 9; i++)
                {
                    e.Row.Cells[i + 6].HorizontalAlign = HorizontalAlign.Right;
                    e.Row.Cells[i + 6].Text = r["Month0" + i].ToString();
                }
                for (int i = 10; i <= 12; i++)
                {
                    e.Row.Cells[i + 6].HorizontalAlign = HorizontalAlign.Right;
                    e.Row.Cells[i + 6].Text = r["Month" + i].ToString();
                }
            }
            //colour code the cell 
            //if (intTotal < 0) { e.Row.Cells[12].BackColor = Color.PapayaWhip; } else { e.Row.Cells[12].BackColor = Color.LightGreen; }

            //Add Second Footer Row - also updated 18/01/11 to fix totals issue
            tbl = ActivityDataAccess.SelectActualTotals(intProjectToCustomerID);
            if (tbl.Rows.Count > 0) //because there may be no actuals 
            {
                GridViewRow row = new GridViewRow(0, 0, DataControlRowType.Footer, DataControlRowState.Normal);
                TableCell cell = new TableCell();
                cell.Text = "Actuals";
                row.Cells.Add(cell);
                //full year view
                if (cboView.SelectedValue == "1")
                {
                    for (int i = 1; i <= 1; i++)
                    {
                        cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                }
                else
                {
                    for (int i = 1; i <= 5; i++)
                    {
                        cell = new TableCell();
                        row.Cells.Add(cell);
                    }
                }
                DataRow dr = tbl.Rows[0];
                //months Jan to Mar - views 1 and 2
                if (cboView.SelectedValue == "1" | cboView.SelectedValue == "2")
                {
                    for (int i = 1; i <= 3; i++)
                    {
                        cell = new TableCell();
                        cell.HorizontalAlign = HorizontalAlign.Right;
                        cell.Text = String.Format("{0:0}", dr["Month0" + i]);
                        row.Cells.Add(cell);
                    }
                }
                //months Apr to Jun - views 1, 2 and 3
                if (cboView.SelectedValue == "1" | cboView.SelectedValue == "2" | cboView.SelectedValue == "3")
                {
                    for (int i = 4; i <= 6; i++)
                    {
                        cell = new TableCell();
                        cell.HorizontalAlign = HorizontalAlign.Right;
                        cell.Text = String.Format("{0:0}", dr["Month0" + i]);
                        row.Cells.Add(cell);
                    }
                }
                //months Jul to Sep - views 1, 3 and 4
                if (cboView.SelectedValue == "1" | cboView.SelectedValue == "3" | cboView.SelectedValue == "4")
                {
                    for (int i = 7; i <= 9; i++)
                    {
                        cell = new TableCell();
                        cell.HorizontalAlign = HorizontalAlign.Right;
                        cell.Text = String.Format("{0:0}", dr["Month0" + i]);
                        row.Cells.Add(cell);
                    }
                }
                //months Oct to Dec - views 1 and 4
                if (cboView.SelectedValue == "1" | cboView.SelectedValue == "4")
                {
                    for (int i = 10; i <= 12; i++)
                    {
                        cell = new TableCell();
                        cell.HorizontalAlign = HorizontalAlign.Right;
                        cell.Text = String.Format("{0:0}", dr["Month" + i]);
                        row.Cells.Add(cell);
                    }
                }
                grd.Controls[0].Controls.Add(row);
            }
        }
    }

#endregion

#region events

    protected void cmdMaster_Click(object sender, EventArgs e)
    {
        //try 
        //{
        //if (Common.CheckAuthorisations(Convert.ToInt32(ProjectGrid1.SelectedValue), Session["CurrentUserName"].ToString()) > 0)
        //{
        //enable controls to edit record
        int intProjectID = Convert.ToInt32(ProjectID.Text);
        Project a = ProjectDataAccess.SelectProject(intProjectID);
        ProjectEdit1.PopulateForm(a);
        ProjectEdit1.ToggleControlState(false);
        mvw.ActiveViewIndex = 1;
        //***lock fields for editing***//
        //}
        //else
        //{
        //    MsgBox("BAPS", "No authorisation for project");
        //}
        //}
        //catch { } //in case no selected item in grid
    }

    protected void cmdGo_Click(object sender, EventArgs e)
    {

    }

    protected void cmdUpdate_Click(object sender, EventArgs e)
    {
        //try //***reset at go-live***
        //{
            if (Page.IsValid)
            {
                if (Session["Mode"].ToString() == "New")
                {
                    ActivityEdit1.CreateActivity();
                    // refresh grid
                    grd.DataBind();
                }
                else
                {
                    ActivityEdit1.UpdateActivity();
                    // update selected line only in grid
                    Activity a = ActivityDataAccess.SelectActivity(Convert.ToInt32(grd.SelectedValue));
                    grd.SelectedRow.Cells[1].Text = a.ActivityName;
                    grd.SelectedRow.Cells[2].Text = Common.ADOLookup("SpendTypeName", "tblSpendType", "SpendTypeID = " + a.SpendTypeID);
                    grd.SelectedRow.Cells[3].Text = Common.ADOLookup("ActivityTypeName", "tblActivityType", "ActivityTypeID = " + a.ActivityTypeID);
                    grd.SelectedRow.Cells[4].Text = a.DocumentNumber;
                    grd.SelectedRow.Cells[5].Text = a.ActivityStatusName;
                    if (a.SupplierID == "")
                    {
                        grd.SelectedRow.Cells[6].Text = "";
                    }
                    else
                    {
                        grd.SelectedRow.Cells[6].Text = Common.ADOLookup("SupplierName", "tblSupplier", "SupplierID = " + a.SupplierID); 
                    }
                    grd.SelectedRow.Cells[7].Text = a.Month01;
                    grd.SelectedRow.Cells[8].Text = a.Month02;
                    grd.SelectedRow.Cells[9].Text = a.Month03;
                    grd.SelectedRow.Cells[10].Text = a.Month04;
                    grd.SelectedRow.Cells[11].Text = a.Month05;
                    grd.SelectedRow.Cells[12].Text = a.Month06;
                    grd.SelectedRow.Cells[13].Text = a.Month07;
                    grd.SelectedRow.Cells[14].Text = a.Month08;
                    grd.SelectedRow.Cells[15].Text = a.Month09;
                    grd.SelectedRow.Cells[16].Text = a.Month10;
                    grd.SelectedRow.Cells[17].Text = a.Month11;
                    grd.SelectedRow.Cells[18].Text = a.Month12;
                    grd.SelectedIndex = -1;
                }
                ResetButtons();
                //refresh annual totals
                RefreshAnnualTotals();
            }
        //}
        //catch
        //{
        //    MsgBox("Error", "Could not save " + strSingular);
        //}
    }

    protected void cmdBackToActivities_Click(object sender, EventArgs e)
    {
        //ActivityEdit1.ClearForm();
        //ResetButtons();
        mvw.ActiveViewIndex = 0;
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        if (Session["Mode"].ToString() == "Edit")
        {
            ActivityEdit1.ClearForm();
            ResetButtons();
            grd.SelectedIndex = -1;
        }
        else
        {
            if (Session["Mode"].ToString() == "New")
            {
                ActivityEdit1.ClearForm();
                ResetButtons();
            }
            else //Mode is "Free"
            {
                //no action
            }
        }
        Session["SelectedActivityID"] = null;
    }

    protected void cmdPOHeader_Click(object sender, EventArgs e)
    {
        //amended 09/06/12: button now available for TCC activity as well as A&P
        Session["ReturnToScreen"] = "ActivityView";
        if (ActivityEdit1.SpendTypeIDValue == "1")
        {
            if (ActivityEdit1.POIDText == "")
            {
                //redirect to PRF with supplier (if known) and line item already populated
                Session["Mode"] = "New";
                Response.Redirect("POHeader.aspx?Mode=NewPRSelectedItem");
            }
            else
            {
                //redirect to PRF with all header details and line items populated
                Session["Mode"] = "Edit";
                Response.Redirect("POHeader.aspx?Mode=ExistingPR");
            }
        }
        else //(TCC or Coupons)
        {
            if (ActivityEdit1.RebateIDText == "")
            {
                //redirect to Rebate Request with activity already populated
                Session["Mode"] = "New";
                Response.Redirect("RebateHeader.aspx?Mode=NewRebateSelectedItem");
            }
            else
            {
                //redirect to Rebate Request with all header details and activities populated
                Session["Mode"] = "Edit";
                Response.Redirect("RebateHeader.aspx?Mode=ExistingRebate");
            }
        }
    }

    protected void cmdReceipt_Click(object sender, EventArgs e)
    //if simple activity and unreceipted, request receipt for selected activity, otherwise go to receipting worklist screen
    //amended AH 11/04/11: always show worklist for phased POs
    {
        //if (POHeaderDataAccess.ReadyToReceipt(Convert.ToInt32(grd.SelectedValue)) == 1)
        if (POHeaderDataAccess.ReadyToReceipt(Convert.ToInt32(grd.SelectedValue)) == 1 && Common.ADOLookup("CountOfActivityDetail", "qryActivity", "ActivityID = " + grd.SelectedValue) == "1")
        //only one value to receipt
        {
            int intActivityDetailID = Convert.ToInt32(Common.ADOLookup("ActivityDetailID", "tblActivityDetail", "ActivityID = " + grd.SelectedValue));
            WorklistDataAccess.UpdateReceiptedStatus(intActivityDetailID, "1", Session["CurrentUserName"].ToString());
            //refresh grid
            grd.DataBind();
            MsgBox("Message", "Receipt has been requested.");
        }
        else
        //multiple months to receipt, or none
        {
            //go to the multiple receipting screen
            mvw.ActiveViewIndex = 2;
            grdWorklist2.DataBind();
        }
    }

#endregion

#region gridbuttons

    protected void cmdNewAP_Click(object sender, EventArgs e)
    {
        //enable controls for new record
        ActivityEdit1.NewRecord();
        ActivityEdit1.BudgetResponsibilityAreaIDSelected = BudgetResponsibilityAreaID.Text;
        ActivityEdit1.SpendTypeIDSelected = "1"; //triggers population of ActivityType DropDown
        //enable/disable buttons
        cmdNewAP.Enabled = false;
        cmdNewTCC.Enabled = false;
        cmdNewCpn.Enabled = false;
        cmdUpdate.Enabled = true;
        cmdCancel.Enabled = true;
        cmdDelete.Enabled = false;
        cmdSupplierLookup.Enabled = true;
        cmdActivityTypeLookup.Enabled = true;

        grd.SelectedIndex = -1;
        Session["Mode"] = "New";
    }

    protected void cmdNewTCC_Click(object sender, EventArgs e)
    {
        //enable controls for new record
        ActivityEdit1.NewRecord();
        ActivityEdit1.BudgetResponsibilityAreaIDSelected = BudgetResponsibilityAreaID.Text;
        ActivityEdit1.SpendTypeIDSelected = "2";
        //enable/disable buttons
        cmdNewAP.Enabled = false;
        cmdNewTCC.Enabled = false;
        cmdNewCpn.Enabled = false;
        cmdUpdate.Enabled = true;
        cmdCancel.Enabled = true;
        cmdDelete.Enabled = false;
        cmdSupplierLookup.Enabled = true;
        cmdActivityTypeLookup.Enabled = true;

        grd.SelectedIndex = -1;
        Session["Mode"] = "New";
    }

    protected void cmdNewCpn_Click(object sender, EventArgs e)
    {
        //enable controls for new record
        ActivityEdit1.NewRecord();
        ActivityEdit1.BudgetResponsibilityAreaIDSelected = BudgetResponsibilityAreaID.Text;
        ActivityEdit1.SpendTypeIDSelected = "3";
        //enable/disable buttons
        cmdNewAP.Enabled = false;
        cmdNewTCC.Enabled = false;
        cmdNewCpn.Enabled = false;
        cmdUpdate.Enabled = true;
        cmdCancel.Enabled = true;
        cmdDelete.Enabled = false;
        cmdSupplierLookup.Enabled = true;
        cmdActivityTypeLookup.Enabled = true;

        grd.SelectedIndex = -1;
        Session["Mode"] = "New";
    }

    protected void cmdDelete_Click(object sender, EventArgs e)
    {
        try
        {
            string strMessage = ActivityDataAccess.DeleteActivity(Convert.ToInt32(grd.SelectedValue), Session["CurrentUserName"].ToString());
            if (strMessage == "")
            {
                //clear form
                ActivityEdit1.ClearForm();
                ResetButtons();
                //refresh grid
                grd.DataBind();
                //refresh annual totals
                RefreshAnnualTotals();
            }
            else
            {
                MsgBox("Error", strMessage);
            }
        }
        catch
        {
            MsgBox("Error", "Could not delete " + strSingular);
        }
    }

    protected void cmdNewPR_Click(object sender, EventArgs e)
    {
        Session["ReturnToScreen"] = "ActivityView";
        //no selected activity; redirect to blank PRF
        Session["Mode"] = "New";
        Response.Redirect("POHeader.aspx?Mode=NewPRNoSelection");
    }

    protected void cmdCopy_Click(object sender, EventArgs e)
    //transfer activity details from grid to clipboard 
    {
        CopyActivity(Convert.ToInt32(grd.SelectedValue));
    }

    protected void cmdCopyAll_Click(object sender, EventArgs e)
    {
        int intActivityID;
        foreach (GridViewRow gr in grd.Rows)
        {
            intActivityID = Convert.ToInt32(grd.DataKeys[gr.RowIndex].Value);
            CopyActivity(intActivityID);
        }
    }

    protected void cmdPaste_Click(object sender, EventArgs e)
    {
        Response.Redirect("Paste.aspx");
    }

#endregion

#region methods

    protected void RefreshAnnualTotals()
    //refresh annual totals
    {
        DataTable t = ActivityDataAccess.SelectActivityViewHeader(int.Parse(ProjectID.Text), int.Parse(CustomerID.Text));
        DataRow r = t.Rows[0];
        AnnualTotals1.PopulateAnnualValues(r);
    }

    protected void ResetButtons()
    //recast 15/12/12 for new permissions regime
    {
        //enable/disable buttons
        cmdUpdate.Enabled = false;
        cmdCancel.Enabled = false;
        if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 35 && Common.CheckAuthorisations(Convert.ToInt32(Session["SelectedProjectToCustomerID"]), Session["CurrentUserName"].ToString()) > 0)
        {
            cmdNewAP.Enabled = true;
            cmdNewTCC.Enabled = true;
            cmdNewCpn.Enabled = true;
            cmdCopyAll.Enabled = true; //"should always be enabled if not a reader" no longer holds
            cmdPaste.Enabled = true;
            cmdNewPR.Enabled = true;
        }
        else
        {
            cmdNewAP.Enabled = false;
            cmdNewTCC.Enabled = false;
            cmdNewCpn.Enabled = false;
            cmdCopyAll.Enabled = false;
            cmdPaste.Enabled = false;
            cmdNewPR.Enabled = false;
        }
        cmdDelete.Enabled = false;
        cmdCopy.Enabled = false;
        cmdPOHeader.Enabled = false;
        cmdReceipt.Enabled = false;
        cmdSupplierLookup.Enabled = false;
        cmdActivityTypeLookup.Enabled = false;
    }

    protected void CopyActivity(int intActivityID)
    //transfer activity details from grid to clipboard 
    {
        Activity a = ActivityDataAccess.SelectActivity(intActivityID);
        DataTable tbl;
        if (Session["SelectedItems"] == null) { tbl = ActivityDataAccess.BlankSelectedItemsTable(); }
        else { tbl = (DataTable)Session["SelectedItems"]; }
        //loop through tbl to see if record with this unique ID has already been added
        //to prevent duplication if refresh clicked
        bool duplicate = false;
        for (int i = tbl.Rows.Count - 1; i >= 0; i--)
        {
            DataRow dr = tbl.Rows[i];
            string dtitem = dr["ActivityID"].ToString();
            if (dtitem == a.ActivityID.ToString())
            {
                duplicate = true;
            }
        }
        //add the selected item to the table
        if (!duplicate)
        {
            DataRow dr = ActivityDataAccess.CopyActivityToTable(a, tbl.NewRow());
            tbl.Rows.Add(dr);
        }
        //store basket table in session variable
        Session["SelectedItems"] = tbl;
    }

    protected void ShowGridColumns(string strView)
    {
        int i = 0;
        switch (cboView.SelectedValue)
        {
            case "1": //full year
                for (i = 7; i <= 18; i++)
                {
                    grd.Columns[i].Visible = true;
                }
                for (i = 3; i <= 6; i++)
                {
                    grd.Columns[i].Visible = false;
                }
                break;
            case "2": //Jan-Jun
                for (i = 2; i <= 12; i++)
                {
                    grd.Columns[i].Visible = true;
                }
                for (i = 13; i <= 18; i++)
                {
                    grd.Columns[i].Visible = false;
                }
                break;
            case "3": //Apr-Sep
                for (i = 2; i <= 6; i++)
                {
                    grd.Columns[i].Visible = true;
                }
                for (i = 7; i <= 9; i++)
                {
                    grd.Columns[i].Visible = false;
                }
                for (i = 10; i <= 15; i++)
                {
                    grd.Columns[i].Visible = true;
                }
                for (i = 16; i <= 18; i++)
                {
                    grd.Columns[i].Visible = false;
                }
                break;
            case "4": //Jul-Dec
                for (i = 2; i <= 6; i++)
                {
                    grd.Columns[i].Visible = true;
                }
                for (i = 7; i <= 12; i++)
                {
                    grd.Columns[i].Visible = false;
                }
                for (i = 13; i <= 18; i++)
                {
                    grd.Columns[i].Visible = true;
                }
                break;
        }
    }

    protected void MsgBox(string strTitle, string strMessage)
    {
        ClientScript.RegisterStartupScript(this.GetType(), strTitle, "alert('" + Common.Left(strMessage, 35) + "');", true);
    }

#endregion

#region dropdowns

    protected void FillCustomersForSelectedProject()
    //fill list of customers for currently selected project for which user has permissions
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillCustomersForSelectedProject";
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        cmd.Parameters.AddWithValue("@ProjectID", ProjectID.Text);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboCustomer;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillProjectsForSelectedCustomer()
    //fill list of projects for currently selected customer for which user has permissions
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillProjectsForSelectedCustomer";
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
//        cmd.Parameters.AddWithValue("@ProjectYear", Session["SelectedYear"].ToString());
        cmd.Parameters.AddWithValue("@ProjectYear", ProjectYear.Text);
        cmd.Parameters.AddWithValue("@CustomerID", CustomerID.Text);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboProject;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillActivityType(int intSpendTypeID, int intBudgetResponsibilityAreaID, int intMaxActiveIndicator)
    //Fill the ActivityTypeID dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillActivityType";
        cmd.Parameters.AddWithValue("@SpendTypeID", intSpendTypeID);
        cmd.Parameters.AddWithValue("@BudgetResponsibilityAreaID", intBudgetResponsibilityAreaID);
        cmd.Parameters.AddWithValue("@MaxActiveIndicator", intMaxActiveIndicator);
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboActivityType;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillView()
    //populate the view mode dropdown
    {
        cboView.Items.Add(new ListItem("Full Year", "1"));
        cboView.Items.Add(new ListItem("Jan-Jun", "2"));
        cboView.Items.Add(new ListItem("Apr-Sep", "3"));
        cboView.Items.Add(new ListItem("Jul-Dec", "4"));
    }

    protected void FillRows()
    //populate the No Of Rows dropdown
    {
        cboRows.Items.Add(new ListItem("5", "5"));
        cboRows.Items.Add(new ListItem("10", "10"));
        cboRows.Items.Add(new ListItem("15", "15"));
        cboRows.Items.Add(new ListItem("20", "20"));
        cboRows.Items.Add(new ListItem("25", "25"));
        cboRows.Items.Add(new ListItem("30", "30"));
        cboRows.Items.Add(new ListItem("40", "40"));
        cboRows.Items.Add(new ListItem("50", "50"));
    }

    protected void cboProject_TextChanged(object sender, EventArgs e)
    {
        Session["SelectedProjectToCustomerID"] = Common.ADOLookup("ProjectToCustomerID", "tblProjectToCustomer", "ProjectID = " + cboProject.SelectedValue + " AND CustomerID = " + cboCustomer.SelectedValue);
        ProjectToCustomerID.Text = Session["SelectedProjectToCustomerID"].ToString();
        //Session["SelectedBrandID"] = "0"; //***is this right?***
        //ensure no activity selected (added 15/12/12)
        ActivityEdit1.ClearForm();
        ResetButtons();
        grd.SelectedIndex = -1;
        Session["SelectedActivityID"] = null;
    }

    protected void cboCustomer_TextChanged(object sender, EventArgs e)
    {
        Session["SelectedCustomerID"] = cboCustomer.SelectedValue;
        Session["SelectedProjectToCustomerID"] = Common.ADOLookup("ProjectToCustomerID", "tblProjectToCustomer", "ProjectID = " + cboProject.SelectedValue + " AND CustomerID = " + cboCustomer.SelectedValue);
        ProjectToCustomerID.Text = Session["SelectedProjectToCustomerID"].ToString();
        //ensure no activity selected (added 15/12/12)
        ActivityEdit1.ClearForm();
        ResetButtons();
        grd.SelectedIndex = -1;
        Session["SelectedActivityID"] = null;
    }
    
    protected void cboView_TextChanged(object sender, EventArgs e)
    {
        //save user's activity view setting
        ActivityDataAccess.SetUserActivityView(Session["CurrentUserName"].ToString(), Convert.ToInt32(cboView.SelectedValue));
    }

    protected void cboRows_TextChanged(object sender, EventArgs e)
    {
        //save user's activity rows setting
        ActivityDataAccess.SetUserActivityRows(Session["CurrentUserName"].ToString(), Convert.ToInt32(cboRows.SelectedValue));
    }

#endregion

#region supplierlookup

    protected void cmdSaveSupplier_Click(object sender, EventArgs e)
    {
        //Populate supplier ID and supplier name in ActivityEdit screen (and set txtChanged flag to true)
        ActivityEdit1.SupplierIDValue = SupplierGrid1.SelectedValue;
        //mvw.ActiveViewIndex = 0;
    }

    protected void cmdCancelSupplier_Click(object sender, EventArgs e)
    {
        //temporary fix - isChanged flag always set to true when exiting this screen (in case another change has already been made)
        ActivityEdit1.ChangedValue = "1";
        //mvw.ActiveViewIndex = 0;
    }

    protected void cmdSupplierLookup_Click(object sender, EventArgs e)
    {
        SupplierGrid1.PopulateGrid();
        //mvw.ActiveViewIndex = 1;
    }

#endregion

#region activitytypelookup

    protected void cmdSaveActivityType_Click(object sender, EventArgs e)
    {
        //Populate activitytype ID and activitytype name in ActivityEdit screen (and set txtChanged flag to true)
        ActivityEdit1.ActivityTypeIDValue = ActivityTypeGrid1.SelectedValue;
        //mvw.ActiveViewIndex = 0;
    }

    protected void cmdCancelActivityType_Click(object sender, EventArgs e)
    {
        //temporary fix - isChanged flag always set to true when exiting this screen
        ActivityEdit1.ChangedValue = "1";
        //mvw.ActiveViewIndex = 0;
    }

    protected void cmdActivityTypeLookup_Click(object sender, EventArgs e)
    {
        //Select the spend type and BRA (added 24/02/13)
        ActivityTypeGrid1.SelectFilters(ActivityEdit1.SpendTypeIDValue, BudgetResponsibilityAreaID.Text);
        mp2.Show();
        //mvw.ActiveViewIndex = 3;
    }

#endregion

#region receipting

    protected void cmdRequestReceipt_Click(object sender, EventArgs e)
    {
        WorklistDataAccess.UpdateReceiptedStatus(Convert.ToInt32(grdWorklist2.SelectedValue), "1", Session["CurrentUserName"].ToString());
        mvw.ActiveViewIndex = 0;
        grd.DataBind();
        cmdRequestReceipt.Enabled = false;
        grdWorklist2.SelectedIndex = -1;
        MsgBox("Message", "Receipt has been requested.");
    }

    protected void cmdResetReceipt_Click(object sender, EventArgs e)
    {
        WorklistDataAccess.UpdateReceiptedStatus(Convert.ToInt32(grdWorklist2.SelectedValue), "0");
        mvw.ActiveViewIndex = 0;
        grd.DataBind();
        cmdResetReceipt.Enabled = false;
        grdWorklist2.SelectedIndex = -1;
        MsgBox("Message", "Receipt status has been reset.");
    }

    protected void cmdCancelReceipt_Click(object sender, EventArgs e)
    {
        grdWorklist2.SelectedIndex = -1;
        mvw.ActiveViewIndex = 0;
    }

    protected void grdWorklist2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(this.grdWorklist2, "Select$" + e.Row.RowIndex);
        }
    }

    protected void grdWorklist2_SelectedIndexChanged(object sender, EventArgs e)
    {
        string strStatus = grdWorklist2.SelectedRow.Cells[6].Text;
        //enable/disable buttons unless read only
        if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 35 && Common.CheckAuthorisations(Convert.ToInt32(Session["SelectedProjectToCustomerID"]), Session["CurrentUserName"].ToString()) > 0)
        {
            //if Awaiting Receipt enable Receipt button
            cmdRequestReceipt.Enabled = (strStatus == "Awaiting Receipt");
            //if Requested enable Reset button
            cmdResetReceipt.Enabled = (strStatus == "Receipt Requested");
        }
        //enable/disable buttons for administrators
        if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 15)
        {
            //if Receipted or Requested enable Reset button
            cmdResetReceipt.Enabled = (strStatus == "Receipted" || strStatus == "Receipt Requested");
        }
    }

    protected void grdWorklist2_Sorted(object sender, EventArgs e)
    {
        //grdWorklist2.SelectedIndex = -1;
    }
#endregion

}
