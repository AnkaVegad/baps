<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RebateHeaderPrint.aspx.cs" Inherits="RebateHeaderPrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Print Rebate Request Form</title>
</head>
<body style="text-align:left;margin:10px;">
    <form id="frmPOHeaderPrint" runat="server">
    <div id="po_header_print">
        <!--Page Header-->
        <br /><br />
        <table border="0" width="1095px">
            <tr><td align="left">
                <asp:Label ID="Label1" Text="Rebate Request" runat="server" CssClass="page_header"></asp:Label>
            </td>
            <td align="left">
                <asp:Label ID="Label901" Text="Project" runat="server" CssClass="page_header" />
                <asp:Label ID="ProjectName" runat="server" CssClass="page_header_text" Width="250px" />
            </td>
            <td align="left">
                <asp:Label ID="Label902" Text="Customer" runat="server" CssClass="page_header"></asp:Label>
                <asp:Label ID="CustomerName" runat="server" CssClass="page_header_text" Width="150px" />
            </td>
            <td align="left">
                <asp:Label ID="Label903" Text="IO Number" runat="server" CssClass="page_header"></asp:Label>
                <asp:Label ID="IONumber" runat="server" CssClass="page_header_text" Width="110px" />
            </td></tr>
        </table>
        <br /><br />


        <!--Upper Edit Area-->
        <table class="print_table" border="0" width="1100px">
            <tr><td valign="top">
            <table border="0">
                <tr><td valign="top">
                    <table style="width:370px;" border="0">
                        <tr><td>Accrual Type</td><td width="180">
                            <asp:Label ID="AccrualTypeName" runat="server" Width="180px" CSSClass="print_label_text" />
                        </td></tr>
                        <tr><td>Agreement Type</td><td>
                            <asp:Label ID="AgreementTypeCode" runat="server" Width="80px" CSSClass="print_label_text" />
                        </td></tr>
                        <tr><td valign="top">Comments</td><td width="15">
                            <asp:Label ID="Comments" runat="server" Width="176px" Height="55px" CSSClass="print_label_text" />
                        </td></tr>
                    </table>
                </td>
                <td valign="top">
                    <table style="width:310px;" border="0">
                        <tr><td>Customer</td><td>
                            <asp:Label ID="CustomerName2" runat="server" Width="150px" CSSClass="print_label_text" />
                        </td></tr>
                        <tr><td>SAP Customer Code</td><td>
                            <asp:Label ID="SAPCustomerCode" runat="server" Width="80px" CSSClass="print_label_text" />
                        </td></tr>
                        <tr><td>Valid From Date</td><td>
                            <asp:Label ID="ValidFromDate" runat="server" Width="80px" CSSClass="print_label_text" />
                        </td></tr>
                        <tr><td>Valid To Date</td><td>
                            <asp:Label ID="ValidToDate" runat="server" Width="80px" CSSClass="print_label_text" />
                        </td></tr>
                    </table>
                </td>
                <td valign="top">
                    <table style="width:300px;" border="0">
                        <tr><td>Requested By</td><td>
                            <asp:Label ID="RequestedBy" runat="server" Width="147px" CSSClass="print_label_text"/>
                        </td></tr>
                        <tr><td>Approver</td><td>
                            <asp:Label ID="ApprovedL1By" runat="server" Width="147px" CSSClass="print_label_text"/>
                        </td></tr>
                        <tr><td>Date Approved</td><td>
                            <asp:Label ID="ApprovedL1Date" runat="server" Width="80px" CSSClass="print_label_text" />
                        </td></tr>
                        <tr><td>Rebate Creator</td><td>
                            <asp:Label ID="RebateCreator" runat="server" Width="147px" CSSClass="print_label_text"/>
                        </td></tr>
                        <tr><td>Agreement No</td><td>
                            <asp:Label ID="AgreementNumber" runat="server" Width="80px" CSSClass="print_label_text" />
                        </td></tr>
                    </table>
                </td>
            </table>
        </td></tr></table>

        <!--Grid-->
        <table border="0" width="800px">
            <tr>
                <td>
                    Activities
                </td>
            </tr>
            <tr><td valign="top">
                <div id="rebateheader_grid">
                <asp:GridView ID="grd" runat="server" width="650px"
                    AutoGenerateColumns="False" GridLines="Vertical" RowStyle-Wrap="false" EmptyDataText="No Activities" 
                    HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
                    SelectedRowStyle-Wrap="false" AllowSorting="True" onsorting="grd_Sorting"
                    onrowdatabound="grd_RowDataBound" DataKeyNames="ActivityID" ShowFooter="True" >
                    <Columns>
                        <asp:BoundField DataField="ActivityID" InsertVisible="False" ReadOnly="True" Visible="False" />
                        <asp:BoundField DataField="ShortText" HeaderText="Short Text" SortExpression="ShortText" />
                        <asp:BoundField DataField="ActivityTypeName" HeaderText="Type" SortExpression="ActivityTypeName" />
                        <asp:BoundField DataField="MonthShortName" HeaderText="Month" SortExpression="MonthRequired" />
                        <asp:BoundField DataField="ItemQuantity" HeaderText="Quantity" SortExpression="ItemQuantity" />
                        <asp:BoundField DataField="UnitPrice" HeaderText="Unit Price" SortExpression="POItemNo" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:0.00}" />
                        <asp:BoundField DataField="TotalCost" HeaderText="Total Cost" SortExpression="POItemNo" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:0.00}" />
                    </Columns>
                    <RowStyle CssClass="grid_row" Wrap="False" />
                    <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
                    <HeaderStyle CssClass="grid_header" Wrap="False" />
                    <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
                </asp:GridView>
                </div>
            </td>
            </tr>
            <tr>
                <td valign="top">
                    Allocation To Products
                </td>
            </tr>
            <tr><td valign="top">
                <div id="rebateheader_grid">
                <asp:GridView ID="grdActivityToProduct" runat="server" width="430px"
                    AutoGenerateColumns="False" GridLines="Vertical" RowStyle-Wrap="false" EmptyDataText="[Not Allocated]" 
                    HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
                    SelectedRowStyle-Wrap="false" AllowSorting="True" onsorting="grdActivityToProduct_Sorting" 
                    DataKeyNames="PRODH" ShowFooter="True">
                    <Columns>
                        <asp:BoundField DataField="PRODH" HeaderText="Product Code" SortExpression="PRODH" />
                        <asp:BoundField DataField="VTEXT" HeaderText="Product Name" SortExpression="VTEXT" />
                        <asp:BoundField DataField="PropnPerCent" HeaderText="(%)" SortExpression="PropnPerCent" DataFormatString="{0:0}" />
                    </Columns>
                    <RowStyle CssClass="grid_row" Wrap="False" />
                    <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
                    <HeaderStyle CssClass="grid_header" Wrap="False" />
                    <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
                </asp:GridView>
                </div>
            </td></tr>
        </table>
        <table><tr>
            <td>Currency</td>
            <td>
                <asp:Label ID="CurrencyCode" runat="server" visible="true" Width="30px" CssClass="print_label_text" />
            </td>
        </tr></table>

    </div>
    </form>
</body>
</html>

