﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class Reallocation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //check session not expired and high enough user level
        if (Session["SelectedProjectToCustomerID"] == null || Convert.ToInt16(Session["CurrentUserLevelID"]) >= 15)
        {
            Response.Redirect("ProjectView.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                Grid1.HeaderText = "Reallocate To/From";
            }
        }
    }

#region events

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Grid1.Save();
        }
    }

    protected void cmdBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("ProjectView.aspx");
    }

    protected void cmdClear_Click(object sender, EventArgs e)
    {
        Grid1.ClearForm();
    }

#endregion

}
