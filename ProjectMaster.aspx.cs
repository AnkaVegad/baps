﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ProjectMaster : System.Web.UI.Page
{
    string strSingular = "Project";
 
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {

        }
        else
        {
            //populate selection dropdowns
            FillProjectYear();
            FillBudgetResponsibilityArea();
            FillBrand();
            FillCountry();
            FillPulsingPlanPriority();

            //set year 
            cboProjectYear.SelectedValue = Session["SelectedYear"].ToString();

            //enable new and save button for admin and above
            if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 15)
            {
                cmdNew.Enabled = true;
                cmdCopy.Enabled = true;
                cmdUpdate.Enabled = true;
            }

            DataTable tbl = UserDataAccess.SelectUser(Session["CurrentUserName"].ToString());
            //set area 
            if (Session["SelectedBudgetResponsibilityAreaID"] == null)
            {
                if (tbl.Rows[0]["LastBudgetResponsibilityAreaID"] == DBNull.Value)
                //allows for user's first login when table entry will be blank
                {
                    Session["SelectedBudgetResponsibilityAreaID"] = Common.ADOLookup("BudgetResponsibilityAreaID", "qryUserAuthorisations", "UserName = '" + Session["CurrentUserName"].ToString().Replace("'", "''") + "'");
                }
                else
                {
                    Session["SelectedBudgetResponsibilityAreaID"] = tbl.Rows[0]["LastBudgetResponsibilityAreaID"].ToString();
                }
            }
            try { cboBudgetResponsibilityArea.SelectedValue = Session["SelectedBudgetResponsibilityAreaID"].ToString(); }
            catch { cboBudgetResponsibilityArea.SelectedValue = "0"; }

            if (Session["SelectedBrandID"] == null)
            {
                Session["SelectedBrandID"] = tbl.Rows[0]["LastBrandID"].ToString();
            }
            try { cboBrand.SelectedValue = Session["SelectedBrandID"].ToString(); }
            catch { cboBrand.SelectedValue = "0"; }

            mvw.ActiveViewIndex = 0;
        }

        //populate the project grid
        string strSQL = "SELECT DISTINCT TOP 101 ProjectID, ProjectName, ProjectYear, BudgetResponsibilityAreaName, CountryName, PulsingPlanPriorityName, PulsingPlanTiming FROM qryProjectMaster WHERE ProjectID > 0 ";
        if (cboProjectYear.SelectedValue != "0")
        {
            strSQL += "AND ProjectYear=" + cboProjectYear.SelectedValue + " ";
        }
        if (cboBudgetResponsibilityArea.SelectedValue != "0")
        {
            strSQL += "AND BudgetResponsibilityAreaID=" + cboBudgetResponsibilityArea.SelectedValue + " ";
        }
        if (cboCountry.SelectedValue != "0")
        {
            strSQL += "AND CountryCode='" + cboCountry.SelectedValue + "' ";
        }
        if (cboPulsingPlanPriority.SelectedValue != "0")
        {
            strSQL += "AND PulsingPlanPriorityID=" + cboPulsingPlanPriority.SelectedValue + " ";
        }
        if (cboBrand.SelectedValue != "0")
        {
            strSQL += "AND BrandID=" + cboBrand.SelectedValue + " ";
        }
        if (txtSearch.Text != String.Empty)
        {
            strSQL += "AND ProjectName Like '%" + txtSearch.Text + "%' ";
        }
        //strSQL += "AND EXISTS(SELECT 'X' FROM qryUserAuthorisations WHERE UserName = '" + Session["CurrentUserName"] + "' AND (qryUserAuthorisations.BrandID = dbo.qryProjectMaster.BrandID OR qryUserAuthorisations.BrandID = 0) AND (qryUserAuthorisations.BudgetResponsibilityAreaID = qryProjectMaster.BudgetResponsibilityAreaID)) ";
        //strSQL += "AND EXISTS(SELECT 'X' FROM qryUserAuthorisations WHERE UserName = '" + Session["CurrentUserName"].ToString().Replace("'", "''") + "' AND (qryUserAuthorisations.BrandID = dbo.qryProjectMaster.BrandID OR qryUserAuthorisations.BrandID = 0) AND (qryUserAuthorisations.CustomerID = dbo.qryProjectMaster.CustomerID OR qryUserAuthorisations.CustomerID = 0) AND (qryUserAuthorisations.BudgetResponsibilityAreaID = qryProjectMaster.BudgetResponsibilityAreaID)) ";
        strSQL += "AND EXISTS(SELECT 'X' FROM qryUserAuthorisations WHERE UserName = '" + Session["CurrentUserName"].ToString().Replace("'", "''") + "' AND ((qryUserAuthorisations.BrandID = dbo.qryProjectMaster.BrandID AND qryUserAuthorisations.CategoryID = 1) OR (qryUserAuthorisations.BrandID = dbo.qryProjectMaster.UKCategoryID AND qryUserAuthorisations.CategoryID = 2) OR (qryUserAuthorisations.BrandID = 0)) AND (qryUserAuthorisations.CustomerID = dbo.qryProjectMaster.CustomerID OR qryUserAuthorisations.CustomerID = 0) AND (qryUserAuthorisations.BudgetResponsibilityAreaID = qryProjectMaster.BudgetResponsibilityAreaID)) ";
        strSQL += "ORDER BY ProjectName;";
        sds.SelectCommand = strSQL;
        grd.DataBind();

        //max rows retrieved message
        if (grd.PageCount == 6 && grd.PageSize == 20)
        {
            txtMessage.Text = "The maximum number of rows has been retrieved; to see other projects, change your selection criteria.";
        }
        else
        {
            if (grd.PageCount == 11 && grd.PageSize == 10)
            {
                txtMessage.Text = "The maximum number of rows has been retrieved; to see other projects, change your selection criteria.";
            }
            else
            {
                txtMessage.Text = String.Empty;
            }
        }

    }

#region gridevents

    protected void grd_SelectedIndexChanged(object sender, EventArgs e)
    {
        //enable new and save button for admin and above
        if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 15)
        {
            cmdEdit.Enabled = true;
            cmdCopy.Enabled = true;
            cmdDelete.Enabled = true;
        }
    }

    protected void grd_PageIndexChanged(object sender, EventArgs e)
    {
        grd.SelectedIndex = -1;
    }

    protected void grd_Sorted(object sender, EventArgs e)
    {
        grd.SelectedIndex = -1;
    }

    protected void grd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(this.grd, "Select$" + e.Row.RowIndex);
        }
    }

#endregion

#region events

    protected void cmdOK_Click(object sender, EventArgs e)
    //added 03/08/14 for panel
    {
        mvw.ActiveViewIndex = 0;
    }

    protected void cmdGo_Click(object sender, EventArgs e)
    {
        ProjectEdit1.ClearForm();
    }

    protected void cmdEdit_Click(object sender, EventArgs e)
    {
        if (grd.SelectedValue != null)
        {
            ProjectEdit1.PopulateForm(ProjectDataAccess.SelectProject(Convert.ToInt32(grd.SelectedValue)));
            mvw.ActiveViewIndex = 1;
            lblHeader.Text = "Edit " + strSingular;
        }
    }

    protected void cmdCopy_Click(object sender, EventArgs e)
    {
        if (grd.SelectedValue != null)
        {
            ProjectEdit1.PopulateCopy(ProjectDataAccess.SelectProject(Convert.ToInt32(grd.SelectedValue)));
            mvw.ActiveViewIndex = 1;
            lblHeader.Text = "New " + strSingular;
        }
    }

    protected void cmdUpdate_Click(object sender, EventArgs e)
    {
        string strMessage;
        //try
        //{
            if (ProjectEdit1.ProjectIDValue == "0") //New
            {
                strMessage = ProjectEdit1.CreateProject();
                if (strMessage == "")
                //completed without errors
                {
                    // update grid
                    grd.DataBind();
                    mvw.ActiveViewIndex = 0;
                }
                else
                {
                    if (strMessage == "Not all customers were added")
                    {
                        //save anyway and close
                        grd.DataBind();
                        lblMessage2.Text = strMessage;
                        mp1.Show();
                        //mvw.ActiveViewIndex = 0;
                    }
                    else
                    {
                        MsgBox("Error", strMessage);
                    }
                }
            }
            else
            {
                int intProjectID = Convert.ToInt32(ProjectEdit1.ProjectIDValue);
                strMessage = ProjectEdit1.UpdateProject();
                if (strMessage == "")
                //completed without errors
                {
                    mvw.ActiveViewIndex = 0;
                    //update single line in grid rather than refresh
                    Project p = ProjectDataAccess.SelectProject(intProjectID);
                    grd.SelectedRow.Cells[1].Text = p.ProjectName;
                    grd.SelectedRow.Cells[2].Text = p.ProjectYear;
                    grd.SelectedRow.Cells[3].Text = Common.ADOLookup("BudgetResponsibilityAreaName", "tblBudgetResponsibilityArea", "BudgetResponsibilityAreaID = " + p.BudgetResponsibilityAreaID);
                    grd.SelectedRow.Cells[4].Text = Common.ADOLookup("CountryName", "tblCountry", "CountryCode = '" + p.CountryCode + "'");
                    grd.SelectedRow.Cells[5].Text = Common.ADOLookup("PulsingPlanPriorityName", "tblPulsingPlanPriority", "PulsingPlanPriorityID = " + p.PulsingPlanPriorityID);
                    grd.SelectedRow.Cells[6].Text = p.PulsingPlanTiming;
                    //lblMessage2.Text = "Project changes saved.";
                    //mp1.Show();
                }
                else
                {
                    MsgBox("Error", strMessage);
                }
            }
        //}
        //catch
        //{
        //    MsgBox("Error", "Could not save " + strSingular);
        //}
    }

    protected void cmdNew_Click(object sender, EventArgs e)
    {
        //enable controls for new record
        ProjectEdit1.NewRecord();
        lblHeader.Text = "New " + strSingular;

        grd.SelectedIndex = -1;
        mvw.ActiveViewIndex = 1;
    }

    protected void cmdDelete_Click(object sender, EventArgs e)
    {
        try
        {
            if (grd.SelectedValue != null)
            {
                ProjectDataAccess.DeleteProject(Convert.ToInt32(grd.SelectedValue));
                ProjectEdit1.ClearForm();
                // update grid
                grd.DataBind();
            }
        }
        catch //if item has non-cascaded dependencies
        {
            MsgBox("Error", "Could not delete " + strSingular);
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        ProjectEdit1.ClearForm();
        //ResetButtons();
        grd.SelectedIndex = -1;
        mvw.ActiveViewIndex = 0;
    }

#endregion

#region methods

    protected void MsgBox(string strTitle, string strMessage)
    {
        ClientScript.RegisterStartupScript(this.GetType(), strTitle, "alert('" + Common.Left(strMessage, 35) + "');", true);
    }

#endregion

#region dropdowns

    protected void FillBudgetResponsibilityArea()
    //fill BudgetResponsibilityArea dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillBudgetResponsibilityArea";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboBudgetResponsibilityArea;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillBrand()
    //fill brand dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillBrand";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboBrand;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillProjectYear()
    //fill year dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillYear";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboProjectYear;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillCountry()
    //fill Country dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillCountry";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboCountry;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillPulsingPlanPriority()
    //fill PulsingPlanPriority dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillPulsingPlanPriority";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboPulsingPlanPriority;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void cboBudgetResponsibilityArea_TextChanged(object sender, EventArgs e)
    {
        ProjectEdit1.ClearForm();
    }

    protected void cboBrand_TextChanged(object sender, EventArgs e)
    {
        ProjectEdit1.ClearForm();
    }

    protected void cboProjectYear_TextChanged(object sender, EventArgs e)
    {
        ProjectEdit1.ClearForm();
    }

    protected void cboCountry_TextChanged(object sender, EventArgs e)
    {
        ProjectEdit1.ClearForm();
    }

    protected void cboPulsingPlanPriority_TextChanged(object sender, EventArgs e)
    {
        ProjectEdit1.ClearForm();
    }

#endregion

}
