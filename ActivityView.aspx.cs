﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Drawing;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using System.IO;

public partial class ActivityView : System.Web.UI.Page
{
    string strSingular = "Activity";

    protected void Page_Load(object sender, EventArgs e)
    {
        //check for session expired
        if (Session["SelectedProjectToCustomerID"] == null || Session["SelectedBudgetResponsibilityAreaID"] == null)
        {
            Response.Redirect("ProjectView_rs.aspx");
        }
        //default to grid view on first opening
        if (!IsPostBack) 
        {
            CancelFileActions();
            mvw.ActiveViewIndex = 0;
        }
        //enable buttons for all except reader
        if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 35 && Common.CheckAuthorisations(Convert.ToInt32(Session["SelectedProjectToCustomerID"]), Session["CurrentUserName"].ToString()) > 0)
        {
            if (Common.ADOLookup("IONumber", "tblProjectToCustomer", "ProjectToCustomerID = " + Session["SelectedProjectToCustomerID"]) != String.Empty)
                //Grid1.IONumberValue != String.Empty) //can't add activity if no IO
            {
                cmdNewActivity.Enabled = true;
                cmdEdit.Enabled = true;
                cmdPOHeader.Enabled = true;
                cmdUpdate.Enabled = true;
                cmdDelete.Enabled = true;
                cmdReceipt.Enabled = true;
            }
            else
            {
                cmdNewActivity.Enabled = false;
                cmdEdit.Enabled = false;
                cmdPOHeader.Enabled = false;
                cmdUpdate.Enabled = false;
                cmdDelete.Enabled = false;
                cmdReceipt.Enabled = false;
            }
            cmdCopyAll.Enabled = true;
            cmdPaste.Enabled = true;
        }
        else
        {
            cmdNewActivity.Enabled = false;
            cmdEdit.Enabled = false;
            cmdPOHeader.Enabled = false;
            cmdUpdate.Enabled = false;
            cmdDelete.Enabled = false;
            cmdCopyAll.Enabled = false;
            cmdPaste.Enabled = false;
            cmdReceipt.Enabled = false;
        }
    }

#region events

    //Button gets displayed on ActivityEdit user control
     protected void cmdCancel_Click(object sender, EventArgs e)
    {
        CancelFileActions();
        String strSelectedActivityID = ActivityEdit_rs1.ActivityIDValue;
        ActivityEdit_rs1.ClearControls();
        mvw.ActiveViewIndex = 0;
        Grid1.RefreshGrid();
    }

    protected void cmdBackToProjects_Click(object sender, EventArgs e)
    {
        String strSelectedActivityID = ActivityEdit_rs1.ActivityIDValue;
        ActivityEdit_rs1.ClearControls();
        mvw.ActiveViewIndex = 0;
        Grid1.RefreshGrid();
    }


    //Button gets displayed on ActivityEdit user control
    protected void cmdUpdate_Click(object sender, EventArgs e)
    {
        string strMessage;
        
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "SaveFileActions";
        cmd.Parameters.AddWithValue("@Username", Session["CurrentUserName"].ToString());
        DataTable dtf = DataAccess.ExecuteSelectCommand(cmd);

        for (int i = 0; i < dtf.Rows.Count; i++)
        {
            string fileName = Server.MapPath("Uploads/" + dtf.Rows[i]["FileName"].ToString());
            File.Delete(fileName);
        }

        if (ActivityEdit_rs1.ActivityIDValue == "0") //New
        {
            strMessage = ActivityEdit_rs1.CreateRecord();
        }
        else //edit
        {
            strMessage = ActivityEdit_rs1.UpdateRecord();
        }
        if (strMessage == "[none]")
        {
            // switch to grid and refresh - changed to "populate" 23/12/14
            mvw.ActiveViewIndex = 0;
            DataTable tbl = ActivityDataAccess.SelectActivityViewHeader(Convert.ToInt32(Session["SelectedProjectToCustomerID"]));
            DataRow r = tbl.Rows[0];
            Grid1.RefreshGrid();
        }
        else
        {
            MsgBox("Error", strMessage);
        }
    }

       
    //Button gets displayed on ActivityGrid user control
    protected void cmdDelete_Click(object sender, EventArgs e)
    {
        try
        {
            string strMessage = ActivityDataAccess.DeleteActivity(Convert.ToInt32(Grid1.SelectedValue), Session["CurrentUserName"].ToString());
            if (strMessage == "")
            {
                Grid1.RefreshGrid();
            }
            else
            {
                Grid1.RefreshGrid();
                MsgBox("Error", strMessage);
            }
        }
        catch
        {
            Grid1.RefreshGrid();
            MsgBox("Error", "Could not delete " + strSingular);
        }
    }


    //Button gets displayed on ActivityGrid user control
    protected void cmdEdit_Click(object sender, EventArgs e)
    {
        //enable controls to edit record
        Activity a = ActivityDataAccess.SelectActivity(Convert.ToInt32(Grid1.SelectedValue));
        Session["SelectedActivityID"] = Grid1.SelectedValue;
        Session["SelectedActivityIndex"] = Grid1.SelectedIndex;
        CancelFileActions();
        ActivityEdit_rs1.PopulateForm(a);
        mvw.ActiveViewIndex = 1;
    }

    private void CancelFileActions()
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "CancelFileActions";
        cmd.Parameters.AddWithValue("@Username", Session["CurrentUserName"].ToString());
        DataTable dtf = DataAccess.ExecuteSelectCommand(cmd);

        for (int i = 0; i < dtf.Rows.Count; i++)
        {
            string fileName = Server.MapPath("Uploads/" + dtf.Rows[i]["FileName"].ToString());
            File.Delete(fileName);
        }
    }

    //Button gets displayed on ActivityGrid user control
    protected void cmdNewActivity_Click(object sender, EventArgs e)
    {
        ActivityEdit_rs1.NewRecord();
        mvw.ActiveViewIndex = 1;
        Session["Mode"] = "New";
    }

    //Button gets displayed on ActivityGrid user control
    protected void cmdPOHeader_Click(object sender, EventArgs e)
    {
        //amended 09/06/12: button now available for TCC activity as well as A&P
        Session["ReturnToScreen"] = "ActivityView_rs";
        if (Grid1.SelectedSpendTypeName == "A&P")
        {
            if (Common.ADOLookup("POID", "tblActivity", "ActivityID = " + Grid1.SelectedValue) == "")
            {
                //redirect to PRF with supplier (if known) and line item already populated
                Session["Mode"] = "New";
                //Response.Write("POHeader.aspx?Mode=NewPRSelectedItem");
                Response.Redirect("POHeader_rs.aspx?Mode=NewPRSelectedItem");
            }
            else
            {
                //redirect to PRF with all header details and line items populated
                Session["Mode"] = "Edit";
                //Response.Write("POHeader.aspx?Mode=ExistingPR");
                Response.Redirect("POHeader_rs.aspx?Mode=ExistingPR");
            }
        }
        else //(TCC or Coupons)
        {
            if (Common.ADOLookup("RebateID", "tblActivity", "ActivityID = " + Grid1.SelectedValue) == "")
            {
                //redirect to Rebate Request with activity already populated
                Session["Mode"] = "New";
                //Response.Write("RebateHeader.aspx?Mode=NewRebateSelectedItem");
                Response.Redirect("RebateHeader.aspx?Mode=NewRebateSelectedItem");
            }
            else
            {
                //redirect to Rebate Request with all header details and activities populated
                Session["Mode"] = "Edit";
                //Response.Write("RebateHeader.aspx?Mode=ExistingRebate");
                Response.Redirect("RebateHeader.aspx?Mode=ExistingRebate");
            }
        }
        Grid1.RefreshGrid();
    }

    //Button gets displayed on ActivityGrid user control
    protected void cmdReceipt_Click(object sender, EventArgs e)
    //if simple activity and unreceipted, request receipt for selected activity, otherwise go to receipting worklist screen
    //amended AH 11/04/11: always show worklist for phased POs
    {
        //if (POHeaderDataAccess.ReadyToReceipt(Convert.ToInt32(grd.SelectedValue)) == 1)
        if (POHeaderDataAccess.ReadyToReceipt(Convert.ToInt32(Grid1.SelectedValue)) == 1 && Common.ADOLookup("CountOfActivityDetail", "qryActivity", "ActivityID = " + Grid1.SelectedValue) == "1")
        //only one value to receipt
        {
            int intActivityDetailID = Convert.ToInt32(Common.ADOLookup("ActivityDetailID", "tblActivityDetail", "ActivityID = " + Grid1.SelectedValue));
            WorklistDataAccess.UpdateReceiptedStatus(intActivityDetailID, "1", Session["CurrentUserName"].ToString());
            //refresh grid - Changed by Vidya
            Grid1.RefreshGrid();
            MsgBox("Message", "Receipt has been requested.");
        }
        else
        //multiple months to receipt, or none
        {
            //***go to the multiple receipting screen - want this to be a popup***
            mvw.ActiveViewIndex = 3;
            grdWorklist2.DataBind();
        }
    }

    //Button gets displayed on ActivityGrid user control
    protected void cmdCopy_Click(object sender, EventArgs e)
    //transfer activity details from grid to clipboard 
    {
        CopyActivity(Convert.ToInt32(Grid1.SelectedValue));
    }

    //Button gets displayed on ActivityGrid user control
    protected void cmdCopyAll_Click(object sender, EventArgs e)
    {
//        int intActivityID;
//        foreach (GridViewRow gr in grd.Rows)
//        {
//            intActivityID = Convert.ToInt32(grd.DataKeys[gr.RowIndex].Value);
//            CopyActivity(intActivityID);
//        }
    }

    //Button gets displayed on ActivityGrid user control
    protected void cmdPaste_Click(object sender, EventArgs e)
    {
        Response.Redirect("Paste.aspx");
    }

    //Button gets displayed on ActivityGrid user control
    protected void cmdMaster_Click(object sender, EventArgs e)
    {
        //try 
        //{
        //if (Common.CheckAuthorisations(Convert.ToInt32(ProjectGrid1.SelectedValue), Session["CurrentUserName"].ToString()) > 0)
        //{
        //enable controls to edit record
        int intProjectID = Convert.ToInt32(Common.ADOLookup("ProjectID", "qryProjectToCustomer", "ProjectToCustomerID = " + Grid1.SelectedProjectToCustomerID));
        Project p = ProjectDataAccess.SelectProject(intProjectID);
        ProjectEdit1.PopulateForm(p);
        ProjectEdit1.ToggleControlState(false);
        mvw.ActiveViewIndex = 2;
        //***lock fields for editing***//
        //}
        //else
        //{
        //    MsgBox("BAPS", "No authorisation for project");
        //}
        //}
        //catch { } //in case no selected item in grid
    }

#endregion

#region methods

    protected void CopyActivity(int intActivityID)
    //transfer activity details from grid to clipboard 
    {
        Activity a = ActivityDataAccess.SelectActivity(intActivityID);
        DataTable tbl;
        if (Session["SelectedItems"] == null) { tbl = ActivityDataAccess.BlankSelectedItemsTable(); }
        else { tbl = (DataTable)Session["SelectedItems"]; }
        //loop through tbl to see if record with this unique ID has already been added
        //to prevent duplication if refresh clicked
        bool duplicate = false;
        for (int i = tbl.Rows.Count - 1; i >= 0; i--)
        {
            DataRow dr = tbl.Rows[i];
            string dtitem = dr["ActivityID"].ToString();
            if (dtitem == a.ActivityID.ToString())
            {
                duplicate = true;
            }
        }
        //add the selected item to the table
        if (!duplicate)
        {
            DataRow dr = ActivityDataAccess.CopyActivityToTable(a, tbl.NewRow());
            tbl.Rows.Add(dr);
        }
        //store basket table in session variable
        Session["SelectedItems"] = tbl;
    }
    
    protected void MsgBox(string strTitle, string strMessage)
    {
        ClientScript.RegisterStartupScript(this.GetType(), strTitle, "alert('" + Common.Left(strMessage, 35) + "');", true);
    }

#endregion

#region receipting

    protected void cmdRequestReceipt_Click(object sender, EventArgs e)
    {
        WorklistDataAccess.UpdateReceiptedStatus(Convert.ToInt32(grdWorklist2.SelectedValue), "1", Session["CurrentUserName"].ToString());
        mvw.ActiveViewIndex = 0;
        Grid1.RefreshGrid();
        cmdRequestReceipt.Enabled = false;
        grdWorklist2.SelectedIndex = -1;
        MsgBox("Message", "Receipt has been requested.");
    }

    protected void cmdResetReceipt_Click(object sender, EventArgs e)
    {
        WorklistDataAccess.UpdateReceiptedStatus(Convert.ToInt32(grdWorklist2.SelectedValue), "0");
        mvw.ActiveViewIndex = 0;
        Grid1.RefreshGrid(); 
        cmdResetReceipt.Enabled = false;
        grdWorklist2.SelectedIndex = -1;
        MsgBox("Message", "Receipt status has been reset.");
    }

    protected void cmdCancelReceipt_Click(object sender, EventArgs e)
    {
        grdWorklist2.SelectedIndex = -1;
        mvw.ActiveViewIndex = 0;
    }

    protected void grdWorklist2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(this.grdWorklist2, "Select$" + e.Row.RowIndex);
        }
    }

    protected void grdWorklist2_SelectedIndexChanged(object sender, EventArgs e)
    {
        string strStatus = grdWorklist2.SelectedRow.Cells[6].Text;
        //enable/disable buttons unless read only
        if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 35 && Common.CheckAuthorisations(Convert.ToInt32(Session["SelectedProjectToCustomerID"]), Session["CurrentUserName"].ToString()) > 0)
        {
            //if Awaiting Receipt enable Receipt button
            cmdRequestReceipt.Enabled = (strStatus == "Awaiting Receipt");
            //if Requested enable Reset button
            cmdResetReceipt.Enabled = (strStatus == "Receipt Requested");
        }
        //enable/disable buttons for administrators
        if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 15)
        {
            //if Receipted or Requested enable Reset button
            cmdResetReceipt.Enabled = (strStatus == "Receipted" || strStatus == "Receipt Requested");
        }
    }

    protected void grdWorklist2_Sorted(object sender, EventArgs e)
    {
        //grdWorklist2.SelectedIndex = -1;
    }

#endregion

}
