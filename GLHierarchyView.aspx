﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AP_rs.master" AutoEventWireup="true" EnableEventValidation="false" CodeFile="GLHierarchyView.aspx.cs" Inherits="GLHierarchyView" %>

<%@ Register src="User_Controls/GLHierarchyGrid.ascx" tagname="GLHierarchyGrid" tagprefix="uc1" %>
<%@ Register src="User_Controls/GLHierarchyEdit.ascx" tagname="GLHierarchyEdit" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:MultiView ID="mvw" runat="server">
    <asp:View ID="vw1" runat="server">
        <uc1:GLHierarchyGrid ID="GLHierarchyGrid1" runat="server" />
        <!--Grid Buttons-->
        <asp:Button ID="cmdNew" runat="server" Text="New" causesvalidation="false"
            onclick="cmdNew_Click" CssClass="form_button" enabled="true" width="70px" />&nbsp;
        <asp:Button ID="cmdEdit" runat="server" Text="Edit" causesvalidation="false"
            onclick="cmdEdit_Click" CssClass="form_button" enabled="true" width="70px" />&nbsp;
        <asp:Button ID="cmdDelete" runat="server" Text="Delete" causesvalidation="false" width="70px" 
            onclick="cmdDelete_Click" OnClientClick="return confirm('Are you sure you want to delete this GL Hierarchy record?');"
            CssClass="form_button" Enabled="true" />
        &nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label87034" runat="server" Text="Create new Hierarchy Item:"></asp:Label>
        <asp:TextBox ID="txtNewHierarchyItem" runat="server" MaxLength="50"></asp:TextBox>
        &nbsp;Level&nbsp;<asp:TextBox ID="txtLevel" runat="server" MaxLength="1" Width="10px"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*Required" 
            controltovalidate="txtNewHierarchyItem" Display="Dynamic" ValidationGroup="HierarchyItem" />
        <asp:Button ID="cmdCreate" runat="server" Text="Create" onclick="cmdCreate_Click" CausesValidation="True" ValidationGroup="HierarchyItem" />

    </asp:View>
    <asp:View ID="vw2" runat="server">
        <!--Edit Area-->
        <uc2:GLHierarchyEdit ID="GLHierarchyEdit1" runat="server" /><br />
        <asp:Button ID="cmdUpdate" runat="server" Text="Save" causesvalidation="true"
            onclick="cmdUpdate_Click" CssClass="form_button" Enabled="true" validationgroup="GLHierarchy" />&nbsp;
        <asp:Button ID="cmdCancel" runat="server" Text="Cancel" causesvalidation="false"
            onclick="cmdCancel_Click" CssClass="form_button" Enabled="True" />
    </asp:View>
</asp:MultiView>
</asp:Content>

