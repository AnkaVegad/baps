﻿using System;

public partial class SupplierView : System.Web.UI.Page

{
    string strSingular = "Supplier";
    string strPlural = "Suppliers";

    protected void Page_Load(object sender, EventArgs e)
    {
        //check session not expired and high enough user level
        if (Session["SelectedProjectToCustomerID"] == null || Convert.ToInt16(Session["CurrentUserLevelID"]) >= 15)
        {
            Response.Redirect("ProjectView.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                SupplierGrid1.PopulateGrid();
                mvw.ActiveViewIndex = 0;
                SupplierGrid1.HeaderText = strPlural;
            }
        }
    }

#region events

    protected void cmdUpdate_Click(object sender, EventArgs e)
    {
        int intResult;
        try 
        {
            if (Session["Mode"].ToString() == "New")
            {
                intResult = SupplierEdit1.CreateRecord();
            }
            else
            {
                intResult = SupplierEdit1.UpdateRecord();
            }
            if (intResult == 0)
            {
                MsgBox("Message", "The " + strSingular + " already exists.");
            }
            else
            {
                SupplierGrid1.RefreshGrid();
                mvw.ActiveViewIndex = 0;
            }
        }
        catch 
        {
            MsgBox("Message", "Could not save " + strSingular + ".");
        }
    }

    protected void cmdNew_Click(object sender, EventArgs e)
    {
        mvw.ActiveViewIndex = 1;
        //enable controls for new record
        SupplierEdit1.NewRecord();
        Session["Mode"] = "New";
        SupplierEdit1.HeaderText = Session["Mode"].ToString() + " " + strSingular;
    }

    protected void cmdEdit_Click(object sender, EventArgs e)
    {
        //try 
        //{
            SupplierEdit1.PopulateForm(Convert.ToInt32(SupplierGrid1.SelectedValue));
            mvw.ActiveViewIndex = 1;
            Session["Mode"] = "Edit";
            SupplierEdit1.HeaderText = Session["Mode"].ToString() + " " + strSingular;
        //}
        //catch { } //in case no selection in grid
    }

    protected void cmdDelete_Click(object sender, EventArgs e)
    {
        try //in case referential integrity prevents deletion
        {
            SupplierDataAccess.DeleteSupplier(Convert.ToInt32(SupplierGrid1.SelectedValue));
            SupplierEdit1.ClearControls();
            // update grid
            SupplierGrid1.DataBind();
        }
        catch
        {
            MsgBox("Message", "Could not delete " + strSingular); 
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        SupplierEdit1.ClearControls();
        mvw.ActiveViewIndex = 0;
    }
    
#endregion

#region methods

    protected void MsgBox(string strTitle, string strMessage)
    {
        ClientScript.RegisterStartupScript(this.GetType(), strTitle, "alert('" + Common.Left(strMessage, 35) + "');", true);
    }

#endregion

}
