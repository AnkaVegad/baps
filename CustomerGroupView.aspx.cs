﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class CustomerGroupView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //check session not expired and high enough user level
        if (Session["SelectedProjectToCustomerID"] == null || Convert.ToInt16(Session["CurrentUserLevelID"]) >= 5)
        {
            Response.Redirect("ProjectView.aspx");
        }
        else
        {
            //Load the grid only the first time the page is loaded
            if (!Page.IsPostBack)
            {
                BindGrid();
            }
        }
    }

    private void BindGrid()
    //Populate the GridView with data
    {
        //Get a data table object containing the LibraryImages
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.Text;
        string strSQL = "SELECT * FROM tblCustomerGroup";
        cmd.CommandText = strSQL;
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        grd.DataSource = tbl;
        //bind the databound controls to the datasource
        grd.DataBind();
    }

    protected void grd_RowEditing(object sender, GridViewEditEventArgs e)
    {
        //set the row to enable edit mode for
        grd.EditIndex = e.NewEditIndex;
        //reload the grid
        BindGrid();
    }
    protected void grd_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        grd.EditIndex = -1;
        BindGrid();
    }
    protected void grd_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        //Retrieve updated data
        string id = grd.DataKeys[e.RowIndex].Value.ToString();
        string value = ((TextBox)grd.Rows[e.RowIndex].FindControl("txtCustomerGroupName")).Text;

        //execute the update command
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.Text;
        string strSQL = "UPDATE tblCustomerGroup SET CustomerGroupName = '" + value + "' WHERE CustomerGroupID = '" + id + "'";
        cmd.CommandText = strSQL;
        int RowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        //cancel edit mode
        grd.EditIndex = -1;
        //bind the databound controls to the datasource
        BindGrid();
    }

    protected void grd_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string id = grd.DataKeys[e.RowIndex].Value.ToString();

        //execute the update command
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.Text;
        string strSQL = "DELETE FROM tblCustomerGroup WHERE CustomerGroupID = " + id;
        cmd.CommandText = strSQL;
        int RowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        //bind the databound controls to the datasource
        BindGrid();
    }

    protected void cmdUpdate_Click(object sender, EventArgs e)
    {
        //execute the create command
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.Text;
        string strSQL = "INSERT INTO tblCustomerGroup (CustomerGroupName, CreatedBy) VALUES ('" + txtNewCustomerGroupName.Text + "', '" + Session["CurrentUserName"] + "')";
        cmd.CommandText = strSQL;

        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        //bind the database controls to the datasource
        BindGrid();
        txtNewCustomerGroupName.Text = "";
    }
}
