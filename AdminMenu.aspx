<%@ Page Title="" Language="C#" MasterPageFile="~/AP.master" AutoEventWireup="true" EnableEventValidation="false" CodeFile="AdminMenu.aspx.cs" Inherits="AdminMenu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<!--Page Header-->
<table border="0" width="1095px">
    <tr>
        <td align="left">
            <asp:Label ID="Label11" Text="Admin Menu" runat="server" CssClass="page_header" Width="175px" />
        </td>
    </tr>
    <tr><td style="font-size:3px">&nbsp;</td></tr>
</table>

<!-- Buttons-->
<table border="0" width="1100">
    <tr><td>
            <asp:Button ID="cmdSuppliers" runat="server" Text="Suppliers" causesvalidation="false" Enabled="true"
                width="130px" onclick="cmdSuppliers_Click" CssClass="form_button" Visible="true" />&nbsp;
    </td></tr>
    <tr><td>
            <asp:Button ID="cmdUserAdmin" runat="server" Text="User Admin" causesvalidation="false" Enabled="true"
                width="130px" onclick="cmdUserAdmin_Click" CssClass="form_button" Visible="true" />&nbsp;
    </td></tr>
</table><br />
    
</asp:Content>

