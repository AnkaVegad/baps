<%@ Page Title="" Language="C#" MasterPageFile="~/AP.master" EnableEventValidation="false" AutoEventWireup="true" CodeFile="RebateHeader.aspx.cs" Inherits="RebateHeader" %>

<%@ Register src="User_Controls/ActivityEdit.ascx" tagname="ActivityEdit" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:MultiView ID="mvw" runat="server">
    <asp:View ID="vw1" runat="server">
        <script type="text/javascript" language="javascript">
            window.onbeforeunload = confirmExit;

            function setNeedToConfirm(bool) {
                //set isChanged flag
                var ctl = document.all.ctl00$ContentPlaceHolder1$ActivityEdit1$txtChanged
                if (bool) { ctl.value = "1"; }
                else { ctl.value = "0"; }
            }

            function isChanged() {
                //only save if some data have changed
                var isChanged = (document.all.ctl00$ContentPlaceHolder1$ActivityEdit1$txtChanged.value == "1");
                setNeedToConfirm(false);
                return isChanged;
            }

            function confirmExit() {
                //display a message if user has made changes and is navigating away without clicking Save
                if (document.all.ctl00$ContentPlaceHolder1$ActivityEdit1$txtChanged.value == "1") {
                    return "You will lose any unsaved changes."
                }
            }

            function openPopUp() {
                //open the print preview in a popup window
                {
                    window.open("RebateHeaderPrint.aspx", "Print");
                }
            }  
        </script>
        <!--Page Header-->
        <table border="0" width="1095px">
            <tr>
                <td align="left">
                    <asp:Label ID="Label1" Text="Rebate Request" runat="server" CssClass="page_header"></asp:Label>
                </td>
                <td align="left">
                    <asp:Label ID="RebateID" runat="server" visible="true" Width="30px" CssClass="label_text" />
                    <asp:Label ID="ProjectToCustomerID" runat="server" visible="false" Width="30px" CssClass="label_text" />
                    <asp:Label ID="BudgetResponsibilityAreaID" runat="server" visible="false" Width="30px" CssClass="label_text" />
                </td>
                <td align="left">
                    <asp:TextBox ID="ProjectID" runat="server" visible="false"></asp:TextBox>
                    <asp:Label ID="Label901" Text="Project" runat="server" CssClass="page_header" />
                </td>
                <td align="left">
                    <asp:Label ID="ProjectName" runat="server" CssClass="page_header_text" Width="250px" />
                </td>
                <td align="left">
                    <asp:TextBox ID="CustomerID" runat="server" visible="false"></asp:TextBox>
                    <asp:Label ID="Label902" Text="Customer" runat="server" CssClass="page_header"></asp:Label>
                </td>
                <td align="left">
                    <asp:Label ID="CustomerName" runat="server" CssClass="page_header_text" Width="150px" />
                </td>
                <td align="left">
                    <asp:Label ID="Label903" Text="IO Number" runat="server" CssClass="page_header"></asp:Label>
                </td>
                <td align="left">
                    <asp:Label ID="IONumber" runat="server" CssClass="page_header_text" Width="110px" />
                </td>
            </tr>
        </table>
        
        <!--Upper Edit Area-->
        <table border="0" width="1100px"><tr><td valign="top">
            <table>
                <tr><td valign="top">
                    <table style="width:370px;" border="0">
                        <tr><td>Accrual Type</td><td width="180">
                            <asp:DropDownList ID="AccrualTypeID" runat="server" Width="120px" Enabled="False"
                                AutoPostBack="true" DataTextField="EntityName" DataValueField="ID" />
                           <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="*Required" 
                                ControlToValidate="AccrualTypeID" Display="Dynamic" MaximumValue="2" 
                                MinimumValue="1" ValidationGroup="RebateHeader" Type="Integer" />
                        </td></tr>
                        <tr><td>Agreement Type</td><td>
                            <asp:DropDownList ID="AgreementTypeCode" runat="server" Width="60px"
                                DataTextField="EntityName" DataValueField="ID" />
                            <asp:RangeValidator ID="RangeValidator4" runat="server" ErrorMessage="*Required" 
                                ControlToValidate="AgreementTypeCode" Display="Dynamic" MaximumValue="zzzz" 
                                MinimumValue="1111" ValidationGroup="RebateHeader" Type="String" />
                        </td></tr>
                        <tr><td valign="top">Comments</td><td width="15">
                            <asp:TextBox ID="Comments" runat="server" Width="250px" MaxLength="255" 
                                Enabled="True" Height="55px" TextMode="MultiLine" />
                        </td></tr>
                    </table>
                </td>
                <td valign="top">
                    <table style="width:310px;" border="0">
                        <tr><td>Customer</td><td>
                            <asp:TextBox ID="CustomerName2" runat="server" Width="150px" enabled="true" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*Required"
                                controltovalidate="CustomerName2" Display="Dynamic"  ValidationGroup="RebateHeader" />
                        </td></tr>
                        <tr><td>SAP Customer Code</td><td>
                            <asp:TextBox ID="SAPCustomerCode" runat="server" Width="80px" enabled="true" />
                        </td></tr>
                        <tr><td>Valid From Date</td><td>
                            <asp:DropDownList ID="ValidFromDay" runat="server" Width="40px" />
                            <asp:RangeValidator ID="RangeValidator101" runat="server" ErrorMessage="*Required" 
                                ControlToValidate="ValidFromDay" Display="Dynamic" MaximumValue="31" 
                                MinimumValue="1" ValidationGroup="RebateHeader" Type="Integer" />
                            <asp:DropDownList ID="ValidFromMonth" runat="server" Width="55px" 
                                DataTextField="EntityName" DataValueField="ID" />
                            <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="*Required" 
                                ControlToValidate="ValidFromMonth" Display="Dynamic" MaximumValue="12" 
                                MinimumValue="1" ValidationGroup="RebateHeader" Type="Integer" />
                            <asp:Label ID="ValidFromYear" runat="server" Width="40px" CssClass="pseudo_text_box" />
                        </td></tr>
                        <tr><td>Valid To Date</td><td>
                            <asp:DropDownList ID="ValidToDay" runat="server" Width="40px" />
                            <asp:RangeValidator ID="RangeValidator5" runat="server" ErrorMessage="*Required" 
                                ControlToValidate="ValidToDay" Display="Dynamic" MaximumValue="31" 
                                MinimumValue="1" ValidationGroup="RebateHeader" Type="Integer" />
                            <asp:DropDownList ID="ValidToMonth" runat="server" Width="55px" 
                                DataTextField="EntityName" DataValueField="ID" />
                            <asp:RangeValidator ID="RangeValidator6" runat="server" ErrorMessage="*Required" 
                                ControlToValidate="ValidToMonth" Display="Dynamic" MaximumValue="12" 
                                MinimumValue="1" ValidationGroup="RebateHeader" Type="Integer" />
                            <asp:Label ID="ValidToYear" runat="server" Width="40px" CssClass="pseudo_text_box" />
                        </td></tr>
                    </table>
                </td>
                <td valign="top">
                    <table style="width:300px;" border="0">
                        <tr><td>Requested By</td><td>
                            <asp:Label ID="RequestedBy" runat="server" Width="147px" CSSClass="pseudo_text_box"/>
                        </td></tr>
                        <tr><td>Approver</td><td>
                            <asp:DropDownList ID="ApprovedL1By" runat="server" Width="150px" autopostback="true"
                                DataTextField="EntityName" DataValueField="ID" 
                                onselectedindexchanged="ApprovedL1By_SelectedIndexChanged" />
                            <asp:RangeValidator ID="RangeValidator3" runat="server" ErrorMessage="*Required" 
                                ControlToValidate="ApprovedL1By" Display="Dynamic" MaximumValue="zzz" MinimumValue="1" 
                                ValidationGroup="RebateHeader" />
                        </td></tr>
                        <tr><td>Date Approved</td><td>
                            <asp:TextBox ID="ApprovedL1Date" runat="server" Width="80px" MaxLength="10" Enabled="false" />
                            <asp:Button ID="cmdApprove" runat="server" Text="Approve" causesvalidation="false" Width="60px" Height="18px"
                                onclick="cmdApprove_Click" CssClass="form_button" Visible="false" />
                        </td></tr>
                        <tr><td>Rebate Creator</td><td>
                            <asp:DropDownList ID="RebateCreator" runat="server" Width="150px" DataTextField="EntityName" DataValueField="ID" />
                            <asp:RangeValidator ID="RangeValidator10" runat="server" ErrorMessage="*Required" 
                                ControlToValidate="RebateCreator" Display="Dynamic" MaximumValue="zzz" MinimumValue="1" 
                                ValidationGroup="RebateHeader" />
                        </td></tr>
                        <tr><td>Agreement No</td><td>
                            <asp:TextBox ID="AgreementNumber" runat="server" Width="80px" MaxLength="12" />
                        </td></tr>
                    </table>
                </td>
                <td valign="top" align="right">
                    <table style="width:100px;" border="0">
                        <tr><td align="right">
                            <asp:Button ID="cmdUpdate" runat="server" Text="Save" causesvalidation="true"
                                onclick="cmdUpdate_Click" CssClass="form_button" Enabled="true" ValidationGroup="RebateHeader" />            
                        </td></tr><tr><td align="right">
                            <asp:Button ID="cmdCancel" runat="server" Text="Cancel" causesvalidation="false"
                                onclick="cmdCancel_Click" CssClass="form_button" Enabled="true" />
                        </td></tr><tr><td align="right">
                            <asp:Button ID="cmdPrint" runat="server" Text="Print" causesvalidation="false"
                                onclientclick="Javascript:openPopUp();" CssClass="form_button" Enabled="true" />
                        </td></tr>
                    </table>
                </td></tr>
            </table>
        </td></tr></table>

        <!--Grid-->
        <table border="0">
            <tr>
                <td>
                    Activities
                </td>
                <td>
                    Allocation To Products
                </td>
            </tr>
            <tr><td valign="top">
                <div id="rebateheader_grid">
                <asp:GridView ID="grd" runat="server" width="650px"
                    AutoGenerateColumns="False" GridLines="Vertical" RowStyle-Wrap="false" EmptyDataText="No Activities" 
                    HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
                    SelectedRowStyle-Wrap="false" AllowSorting="True" onsorting="grd_Sorting"
                    onselectedindexchanged="grd_SelectedIndexChanged" 
                    onrowdatabound="grd_RowDataBound" DataKeyNames="ActivityID" ShowFooter="True" >
                    <Columns>
                        <asp:BoundField DataField="ActivityID" InsertVisible="False" ReadOnly="True" Visible="False" />
                        <asp:BoundField DataField="ShortText" HeaderText="Short Text" SortExpression="ShortText" />
                        <asp:BoundField DataField="ActivityTypeName" HeaderText="Type" SortExpression="ActivityTypeName" />
                        <asp:BoundField DataField="MonthShortName" HeaderText="Month" SortExpression="MonthRequired" />
                        <asp:BoundField DataField="ItemQuantity" HeaderText="Quantity" SortExpression="ItemQuantity" />
                        <asp:BoundField DataField="UnitPrice" HeaderText="Unit Price" SortExpression="POItemNo" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:0.00}" />
                        <asp:BoundField DataField="TotalCost" HeaderText="Total Cost" SortExpression="POItemNo" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:0.00}" />
                    </Columns>
                    <RowStyle CssClass="grid_row" Wrap="False" />
                    <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
                    <HeaderStyle CssClass="grid_header" Wrap="False" />
                    <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
                </asp:GridView>
                <table style="width:650px;" border="0">
                    <tr><td style="font-size:2px">&nbsp;</td></tr>
                    <tr><td>
                        <asp:Button ID="cmdEdit" runat="server" Text="Edit Item" 
                        onclick="cmdEdit_Click" CssClass="form_button" Enabled="false" CausesValidation="false"/>
                        <asp:Button ID="cmdNew" runat="server" Text="New Item" 
                        onclick="cmdNew_Click" CssClass="form_button" Enabled="true" CausesValidation="false"/>
                        <asp:Button ID="cmdRemove" runat="server" Text="Remove Item" Width="100px"
                        onclick="cmdRemove_Click" CssClass="form_button" Enabled="false" CausesValidation="false"/>
                    </td></tr>
                </table>
                </div>
                <!--Activity Grid Buttons-->
                <table style="width:650px;" border="0">
                    <tr><td>Currency&nbsp;
                        <asp:Label ID="CurrencyCode" runat="server" visible="true" Width="30px" CssClass="pseudo_text_box" />
                    </td></tr>
                    <tr><td>Unallocated Activities:
                        <asp:DropDownList ID="cboSelectItem" runat="server" Width="400px" DataTextField="EntityName" DataValueField="ID">
                        </asp:DropDownList>            
                        <asp:Button ID="cmdAdd" runat="server" Text="Add Item" 
                        onclick="cmdAdd_Click" CssClass="form_button" Enabled="true" CausesValidation="false"/>
                        <asp:TextBox ID="txtTotalValue" runat="server" Width="70px" Visible="false"></asp:TextBox>
                    </td></tr>
                </table>
            </td>
            
            <td valign="top">
                <!--Allocation to Product(s) Grid
                AllowSorting, onsorting event and <div> are required to prevent the column headers appearing bold-->
                <div id="rebateheader2_grid">
                <asp:GridView ID="grdActivityToProduct" runat="server" width="430px"
                    AutoGenerateColumns="False" GridLines="Vertical" RowStyle-Wrap="false" EmptyDataText="[Not Allocated]" 
                    HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
                    SelectedRowStyle-Wrap="false" AllowSorting="True" onsorting="grdActivityToProduct_Sorting" 
                    onrowdatabound="grdActivityToProduct_RowDataBound" DataKeyNames="PRODH" ShowFooter="True">
                    <Columns>
                        <asp:BoundField DataField="PRODH" HeaderText="Product Code" SortExpression="PRODH" />
                        <asp:BoundField DataField="VTEXT" HeaderText="Product Name" SortExpression="VTEXT" />
                        <asp:BoundField DataField="PropnPerCent" HeaderText="(%)" SortExpression="PropnPerCent" DataFormatString="{0:0}" />
                    </Columns>
                    <RowStyle CssClass="grid_row" Wrap="False" />
                    <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
                    <HeaderStyle CssClass="grid_header" Wrap="False" />
                    <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
                </asp:GridView>
                <table border="0"><tr><td>
                    </td></tr><tr><td style="font-size:2px">&nbsp;</td></tr><tr><td>
                        <asp:Button ID="cmdRemoveProduct" runat="server" Text="Remove" causesvalidation="false"
                            onclick="cmdRemoveProduct_Click" CssClass="form_button" Enabled="true" />
                        <asp:TextBox ID="txtTotalProportion" runat="server" Width="30px" Visible="true" 
                            CSSClass="hidden_text" Enabled="false" />
                        <asp:RangeValidator ID="RangeValidator7" runat="server" ErrorMessage="*Proportions must add up to 100"
                            validationgroup="RebateHeader"
                            controltovalidate="txtTotalProportion" MinimumValue="100" MaximumValue="100" Display="Dynamic" Type="Integer" />
                    </td></tr>
                    <tr><td>
                        <asp:DropDownList ID="cboAddProduct" runat="server" Width="300px" Enabled="true"
                            DataTextField="EntityName" DataValueField="ID" />
                        <asp:RangeValidator ID="RangeValidator8" ControlToValidate="cboAddProduct" MaximumValue="GB99" MinimumValue="GB00"
                            runat="server" ErrorMessage="Required" Type="String" Display="Dynamic" ValidationGroup="ActivityToProduct" />
                        <asp:TextBox ID="txtProportion" runat="server" Width="30px" enabled="true" MaxLength="3" />%
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtProportion" 
                            ErrorMessage="*Required" Display="Dynamic" ValidationGroup="ActivityToProduct" />
                        <asp:RangeValidator ID="RangeValidator9" runat="server" ControlToValidate="txtProportion" 
                            MaximumValue="100" MinimumValue="1"
                            ErrorMessage="*Must be between 10 and 100" Type="Double" Display="Dynamic" ValidationGroup="ActivityToProduct" />
                        <asp:Button ID="cmdAddProduct" runat="server" Text="Add" causesvalidation="true"
                            onclick="cmdAddProduct_Click" CssClass="form_button" Enabled="true" Width="60px" ValidationGroup="ActivityToProduct" />
                    </td></tr>
                </table>        
                </div>
            </td></tr>
        </table>
        
        <!--Lower Edit Area-->
        <div id="edit_area">
            <table><tr><td valign="top">
                <uc1:ActivityEdit ID="ActivityEdit1" runat="server" />
            </td><td valign="top">
                <table style="width:85px;" border="0">
                    <tr><td align="right">
                        <asp:Button ID="cmdSaveActivity" runat="server" Text="Save" CausesValidation="true"
                        onclick="cmdSaveActivity_Click" CssClass="form_button" Enabled="False" 
                        onclientclick="return isChanged();" ValidationGroup="Activity" />            
                    </td></tr><tr><td align="right">
                        <asp:Button ID="cmdCancelActivity" runat="server" Text="Cancel" causesvalidation="false"
                        onclick="cmdCancelActivity_Click" CssClass="form_button" Enabled="False" 
                        onclientclick="Javascript:setNeedToConfirm(false);" />
                    </td></tr>
                </table>
            </td></tr></table>
        </div><br /><br />    
    </asp:View>
</asp:MultiView>
</asp:Content>

