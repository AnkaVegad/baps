﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AP.master" AutoEventWireup="true" EnableEventValidation="false" CodeFile="SupplierView.aspx.cs" Inherits="SupplierView" %>

<%@ Register src="User_Controls/SupplierGrid.ascx" tagname="SupplierGrid" tagprefix="uc1" %>
<%@ Register src="User_Controls/SupplierEdit.ascx" tagname="SupplierEdit" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:MultiView ID="mvw" runat="server">
    <asp:View ID="vw1" runat="server">
        <uc1:SupplierGrid ID="SupplierGrid1" runat="server" />
        <!--Grid Buttons-->
        <asp:Button ID="cmdNew" runat="server" Text="New Supplier" causesvalidation="false"
            onclick="cmdNew_Click" CssClass="form_button" enabled="true" width="120px" />&nbsp;
        <asp:Button ID="cmdEdit" runat="server" Text="Edit Supplier" causesvalidation="false"
            onclick="cmdEdit_Click" CssClass="form_button" enabled="true" width="120px" />&nbsp;
        <asp:Button ID="cmdDelete" runat="server" Text="Delete Supplier" causesvalidation="false" width="120px" 
            onclick="cmdDelete_Click" OnClientClick="return confirm('Are you sure you want to delete this supplier?');"
            CssClass="form_button" Enabled="true" />
    </asp:View>
    <asp:View ID="vw2" runat="server">
        <!--Edit Area-->
        <uc2:SupplierEdit ID="SupplierEdit1" runat="server" /><br />
        <asp:Button ID="cmdUpdate" runat="server" Text="Save" causesvalidation="true"
            onclick="cmdUpdate_Click" CssClass="form_button" Enabled="true" validationgroup="Supplier" />&nbsp;
        <asp:Button ID="cmdCancel" runat="server" Text="Cancel" causesvalidation="false"
            onclick="cmdCancel_Click" CssClass="form_button" Enabled="True" />
    </asp:View>
</asp:MultiView>
</asp:Content>

