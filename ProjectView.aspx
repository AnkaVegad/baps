<%@ Page Title="" Language="C#" MasterPageFile="~/AP.master" EnableEventValidation="false" AutoEventWireup="true" CodeFile="ProjectView.aspx.cs" Inherits="ProjectView" %>

<%@ Register src="User_Controls/ProjectGrid.ascx" tagname="ProjectGrid" tagprefix="uc1" %>
<%@ Register src="User_Controls/ProjectToCustomerEdit.ascx" tagname="ProjectToCustomerEdit" tagprefix="uc2" %>
<%@ Register src="User_Controls/WorklistSummary.ascx" tagname="WorklistSummary" tagprefix="uc3" %>
<%@ Register src="User_Controls/ProjectEdit.ascx" tagname="ProjectEdit" tagprefix="uc4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:MultiView ID="mvw" runat="server">
    <asp:View ID="vw1" runat="server">
        <uc3:WorklistSummary ID="WorklistSummary1" runat="server" />
        <br />
        <uc1:ProjectGrid ID="ProjectGrid1" runat="server" />
        <!--Buttons-->
        <asp:Button ID="cmdEdit" runat="server" Text="Edit" enabled="true"
            onclick="cmdEdit_Click" CssClass="form_button" />
        <asp:Button ID="cmdNew" runat="server" Text="New" enabled="false"
            onclick="cmdNew_Click" CssClass="form_button" />
        <asp:Button ID="cmdDelete" runat="server" Text="Delete" enabled="false"
            onclick="cmdDelete_Click" CssClass="form_button" 
            OnClientClick="return confirm('Are you sure you want to delete this record?');" />
        <asp:Button ID="cmdBudget" runat="server" Text="Budgets"
            onclick="cmdBudget_Click" CssClass="form_button" />
        <asp:Button ID="cmdActivities" runat="server" Text="Activities"
            onclick="cmdActivities_Click" CssClass="form_button" />
        <asp:Button ID="cmdMaster" runat="server" Text="Master"
            onclick="cmdMaster_Click" CssClass="form_button" />
    </asp:View>
    <asp:View ID="vw2" runat="server">
        <uc2:ProjectToCustomerEdit ID="ProjectToCustomerEdit1" runat="server" Visible="true" /><br />
        <asp:Button ID="cmdUpdate" runat="server" Text="Save" causesvalidation="true"
            onclick="cmdUpdate_Click" CssClass="form_button" Enabled="false" />            
        <asp:Button ID="cmdCancel" runat="server" Text="Cancel" causesvalidation="false"
            onclick="cmdCancel_Click" CssClass="form_button" />            
    </asp:View>
    <!--added 28/02/14----------------------------------------------------------------->
    <asp:View ID="vw3" runat="server">
        <!--Page Header-->
        <br />
        <table border="0" width="1100px">
            <tr>
                <td align="left">
                    <asp:Label ID="Label1" runat="server" CssClass="page_header" Text="Project Master" />
                </td>
            </tr>
        </table><br />
        <!--Edit Area-->
        <div id="project_edit_area">
            <uc4:ProjectEdit ID="ProjectEdit1" runat="server" /><br />
            <table width="1080px">
                <tr><td><asp:Button ID="cmdClose" runat="server" Text="Back To Projects" causesvalidation="false"
                    onclick="cmdCancel_Click" CssClass="form_button" width="110px" />
                </td></tr>            
            </table>
        </div>     
    </asp:View>
    <!--------------------------------------------------------------------------------->
</asp:MultiView>
</asp:Content>

