﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class POHeader : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["SelectedProjectToCustomerID"] == null) //check for session expiry
        {
            Response.Redirect("ProjectView.aspx");
        }
        if (IsPostBack)
        {
            try
            {
                //DeliveryAddress.Text = POHeaderDataAccess.SelectDeliveryAddress(Convert.ToInt32(DeliveryAddressID.SelectedValue));  // files missing
            }
            catch { }
            
        }
        else // not postback
        {
            //DataTables for use later
            DataTable tbl;
            DataTable t;
            DataRow r;
            //populate project header
            if (Request.QueryString["Mode"] == "NewPRSelectedItem" || Request.QueryString["Mode"] == "ExistingPR")
            {
                //populate project header from Activity
                t = ActivityDataAccess.SelectActivityToTable(Convert.ToInt32(Session["SelectedActivityID"]));
                r = t.Rows[0];
                POID.Text = r["POID"].ToString(); //?? sometimes no POID at this point?
            }
            else
            {
                //populate project header from Project 
                t = ProjectDataAccess.SelectProjectToCustomer(Convert.ToInt32(Session["SelectedProjectToCustomerID"]));
                r = t.Rows[0];
            }
            ProjectID.Text = r["ProjectID"].ToString();
            CustomerID.Text = r["CustomerID"].ToString();
            IONumber.Text = r["IONumber"].ToString();
            BudgetResponsibilityAreaID.Text = r["BudgetResponsibilityAreaID"].ToString();
            ProjectName.Text = r["ProjectName"].ToString();
            CustomerName.Text = r["CustomerName"].ToString();
            ProjectToCustomerID.Text = r["ProjectToCustomerID"].ToString();
            CurrencyCode.Text = r["CurrencyCode"].ToString();

            //fill dropdowns pt2 (after table extract but before PO Header values set)
            //FillNPICoordinators(Convert.ToInt32(r["BudgetResponsibilityAreaID"])); // files missing
            //FillActivity(Convert.ToInt32(ProjectToCustomerID.Text));  // files missing
            //FillDeliveryAddressID();  // files missing

            //populate POHeader and selected item table
            if (Request.QueryString["Mode"] == "NewPRSelectedItem")
            {
                //Populate default delivery address
                PopulateDeliveryAddress(BudgetResponsibilityAreaID.Text);
                //Fill released by dropdown with available approvers 
                //FillApprovers(Convert.ToInt32(r["BudgetResponsibilityAreaID"]), Convert.ToInt32(CustomerID.Text), 0);  // files missing
                //populate supplier in POHeader if known from activity
                if (r["SupplierID"].ToString() != "") { SupplierID.Text = r["SupplierID"].ToString(); }
                SAPVendorCode.Text = r["SAPVendorCode"].ToString();
                SupplierName.Text = r["SupplierName"].ToString();
                //populate RequisitionBy with current user
                RequisitionBy.Text = Session["CurrentUserName"].ToString();
                //populate selected item table with selected activity
                tbl = ActivityDataAccess.BlankSelectedItemsTable();
                DataRow dr = tbl.NewRow();
                Activity a = ActivityDataAccess.SelectActivity(Convert.ToInt32(Session["SelectedActivityID"]));
                CopyToTable(a, dr);
                tbl.Rows.Add(dr);
                //remove selected item from unallocated items dropdownlist
                RemoveFromUnallocated(a);
            }
            else
            {
                if (Request.QueryString["Mode"] == "ExistingPR")
                //populate POID, POHeader fields (including Approvers dropdown) and selected items table
                {
                    tbl = PopulateExistingPOID(Convert.ToInt32(POID.Text));
                    Session["SelectedPOID"] = POID.Text;
                }
                else
                {
                    if (Request.QueryString["Mode"] == "ExistingPOID")  //(user has come from Worklist)
                    //populate POID, POHeader fields (including Approvers dropdown) and selected items table
                    {
                        tbl = PopulateExistingPOID(Convert.ToInt32(Session["SelectedPOID"]));
                    }
                    else
                    //populate RequisitionBy with current user (Mode=NewPRNoSelection)
                    {
                        //Populate default delivery address
                        PopulateDeliveryAddress(BudgetResponsibilityAreaID.Text);
                        //Fill released by dropdown with available approvers 
                        //FillApprovers(Convert.ToInt32(r["BudgetResponsibilityAreaID"]), Convert.ToInt32(CustomerID.Text), 0); // files missing
                        RequisitionBy.Text = Session["CurrentUserName"].ToString();
                        //set up blank selected items table
                        tbl = ActivityDataAccess.BlankSelectedItemsTable();
                    }
                }
            }
            //store selected items in basket
            Session["SelectedItems"] = tbl;
            //populate line items grid
            grd.DataSource = tbl;
            grd.DataBind();

            //go to default view
            mvw.ActiveViewIndex = 0;
        }
        //enable/disable fields 
        PONumber.Enabled = !(PRNumber.Text.Trim() == String.Empty);
        ReleasedBy.Enabled = (PRNumber.Text.Trim() == String.Empty);
        if (POID.Text.Trim() != String.Empty && PONumber.Text.Trim() == String.Empty) { PRNumber.Enabled = true; } 
        else { PRNumber.Enabled = false; }
        if (PONumber.Text.Trim() == String.Empty) 
        {
            //ReleasedBy.Enabled = true;
            cmdAdd.Enabled = true;
            cmdNew.Enabled = true;
        }
        else
        {
            //ReleasedBy.Enabled = false;
            cmdAdd.Enabled = false;
            cmdNew.Enabled = false;
            cmdSupplierLookup.Enabled = false;
        } 
    }

#region events

    protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
    //if PONumber is entered PRNumber must not be blank
    {
        if (PONumber.Text.Trim() != String.Empty && PRNumber.Text.Trim() == String.Empty)
        {
            args.IsValid = false;
        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void cmdUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["CurrentUserName"] == null)
            {
                Response.Redirect("ProjectView.aspx");
            }
            else
            {
                DataTable tbl = (DataTable)Session["SelectedItems"];
                if (tbl.Rows.Count == 0)
                {
                    MsgBox("Error", "Select one or more activities.");
                }
                else
                {
                    //loop through selected activities creating history records here, before PO Header is updated
                    foreach (DataRow r in tbl.Rows)
                    {
                        int intActivityID = Convert.ToInt32(r["ActivityID"]);
                        if (intActivityID != 0)
                        //existing activity
                        {
                            ActivityDataAccess.InsertActivityHistory(intActivityID);
                        }
                    }
                    //
                    int intPOID;
                    PO p = new PO();
                    p.SupplierID = Convert.ToInt32(SupplierID.Text);
                    p.Comments = Comments.Text;
                    p.DeliveryAddressID = Convert.ToInt32(DeliveryAddressID.SelectedValue);
                    p.RequisitionBy = RequisitionBy.Text;
                    p.NPICoordinator = NPICoordinator.SelectedValue;
                    p.ReleasedBy = ReleasedBy.SelectedValue;
                    if (POID.Text == "") //Create POHeader
                    {
                        p.POID = 0;
                        p.CreatedBy = Session["CurrentUserName"].ToString();
                        intPOID = POHeaderDataAccess.InsertPOHeader(p);
                    }
                    else //edit existing POHeader
                    {
                        p.POID = Convert.ToInt32(POID.Text);
                        p.PRNumber = PRNumber.Text;
                        p.PONumber = PONumber.Text;
                        p.LastUpdatedBy = Session["CurrentUserName"].ToString();
                        //Update PO Header
                        string strMessage = POHeaderDataAccess.UpdatePOHeader(p);
                        if (strMessage != "")
                        {
                            MsgBox("Error", strMessage);
                            intPOID = 0;
                        }
                        else
                        {
                            intPOID = Convert.ToInt32(POID.Text);
                            //if POValue has changed and PONumber or PRNumber already allocated, create a PO change record
                            if (p.PONumber != String.Empty || p.PRNumber != String.Empty)
                            {
                                //get current value of PO
                                object objOldPOValue = POHeaderDataAccess.GetPOValue(intPOID);
                                if (objOldPOValue != DBNull.Value) //no records for this PO Number - probably means it has just been entered
                                {
                                    double dblOldPOValue = Convert.ToDouble(objOldPOValue);
                                    //fix for suspected floating point error added 24/01/13 
                                    //multiplier reduced from 10000 to 1000 18/07/13 because numbers too big for int32
                                    int intOldPOValue = Convert.ToInt32(dblOldPOValue * 1000);
                                    //get new value of PO
                                    double dblNewPOValue = CalculatePOValue((DataTable)Session["SelectedItems"]);
                                    int intNewPOValue = Convert.ToInt32(dblNewPOValue * 1000);
                                    //insert POChange record
                                    if (intOldPOValue != intNewPOValue) { POHeaderDataAccess.InsertPOChange(intPOID, dblOldPOValue, dblNewPOValue, Session["CurrentUserName"].ToString()); }
                                }
                            }
                            //clear this POID from all existing activities
                            POHeaderDataAccess.ClearPOIDForActivities(intPOID);
                        }
                    }

                    if (intPOID != 0)
                    {
                        //loop through selected activities allocating new POID and updating details
                        foreach (DataRow r in tbl.Rows)
                        {
                            Activity a = CopyFromTable(r);
                            a.POID = intPOID.ToString();
                            if (a.ActivityID == 0)
                            //new Activity
                            {
                                ActivityDataAccess.InsertActivity(a);
                            }
                            else
                            //existing activity (suppress history record added 18/11/12)
                            {
                                ActivityDataAccess.UpdateActivity(a, 0);
                            }
                        }
                        LeaveScreen();
                    }
                }
            }
        }
        catch 
        {
            MsgBox("Error", "An error occurred during saving.");
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        LeaveScreen();
    }

#endregion

#region gridevents

    double dblTotal = 0;

    protected void grd_SelectedIndexChanged(object sender, EventArgs e)
    {
        cmdEdit.Enabled = true;
        cmdRemove.Enabled = true;
    }

    protected void grd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            double dblCurrent = Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "TotalCost"));
            //increment the total 
            dblTotal += dblCurrent;
            
            //highlight on hover
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(this.grd, "Select$" + e.Row.RowIndex);
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[6].Text = "Total";
            //display the total
            e.Row.Cells[7].HorizontalAlign = HorizontalAlign.Right;
            e.Row.Cells[7].Text = String.Format("{0:0.00}", dblTotal);
            //colour code the cell 
            //if (intTotal < 0) { e.Row.Cells[6].BackColor = Color.PapayaWhip; } else { e.Row.Cells[6].BackColor = Color.LightGreen; }
        }
    }

    protected void grd_Sorting(object sender, EventArgs e)
    {
    }

#endregion

#region gridbuttons

    protected void cmdAdd_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["SelectedItems"] == null) //check for session expiry
            {
                Response.Redirect("ProjectView.aspx");
            }
            else
            {
                if (cboSelectItem.Items.Count == 0)
                {
                    MsgBox("", "No items to add.");
                }
                else
                {
                    //transfer activity details from cboSelectItem into activity object 
                    Activity a = ActivityDataAccess.SelectActivity(Convert.ToInt32(cboSelectItem.SelectedValue));
                    //retrieve temp table from session
                    DataTable tbl = (DataTable)Session["SelectedItems"];
                    //loop through tbl to see if record with this unique ID has already been added
                    //to prevent duplication if refresh clicked
                    bool duplicate = false;
                    for (int i = tbl.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = tbl.Rows[i];
                        string dtitem = dr["ActivityID"].ToString();
                        if (dtitem == a.ActivityID.ToString())
                        {
                            duplicate = true;
                        }
                    }
                    if (!duplicate)
                    {
                        //add the selected item to the table
                        DataRow dr = tbl.NewRow();
                        CopyToTable(a, dr);
                        tbl.Rows.Add(dr);
                        //populate supplier in POHeader if known from activity
                        //***Added 10/11/10***
                        if (a.SupplierID.ToString() != "")
                        {
                            DataTable tblS = SupplierDataAccess.SelectSupplier(Convert.ToInt32(a.SupplierID));
                            SupplierID.Text = tblS.Rows[0]["SupplierID"].ToString();
                            SAPVendorCode.Text = tblS.Rows[0]["SAPVendorCode"].ToString();
                            SupplierName.Text = tblS.Rows[0]["SupplierName"].ToString();
                        }
                    }
                    //refresh grid
                    grd.DataSource = tbl;
                    grd.DataBind();
                    if (tbl.Rows.Count == 0) { cmdUpdate.Enabled = false; } else { cmdUpdate.Enabled = true; }
                    //remove from the unallocated activities dropdown list
                    RemoveFromUnallocated(a);
                    //enable/disable buttons
                    cmdEdit.Enabled = true;
                    cmdRemove.Enabled = true;
                    //store basket table in session variable
                    Session["SelectedItems"] = tbl;
                }
            }
        }
        catch 
        {
            MsgBox("Error", "An error has occurred.");
        }
    }

    protected void cmdEdit_Click(object sender, EventArgs e)
    {
        try //in case no selected item 
        {
            if (grd.SelectedValue.ToString() != "")
            {
                Activity a = new Activity();
                DataTable tbl;
                tbl = (DataTable)Session["SelectedItems"];

                //loop through tbl to find the record
                for (int i = tbl.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow dr = tbl.Rows[i];
                    string dtitem = dr["ActivityID"].ToString();
                    if (dtitem == grd.SelectedValue.ToString())
                    {
                        a = CopyFromTable(dr);
                    }
                }

                //edit record
                ActivityEdit1.PopulateForm(a);
                ActivityEdit1.SupplierIDValue = SupplierID.Text; //also disables the supplier field
                ActivityEdit1.SpendTypeIDValue = "1";
                cmdSaveActivity.Enabled = true;
                cmdCancelActivity.Enabled = true;
                cmdActivityTypeLookup.Enabled = true;
                cmdUpdate.Enabled = false;
                cmdCancel.Enabled = false;
                cmdPrint.Enabled = false;
                cmdAdd.Enabled = false;
                cmdNew.Enabled = false;
                cmdEdit.Enabled = false;
                cmdRemove.Enabled = false;

                Session["Mode"] = "New";
            }
        }
        catch { }
    }

    protected void cmdNew_Click(object sender, EventArgs e)
    {
        //enable and populate controls for new A&P record
        ActivityEdit1.NewRecord();
        ActivityEdit1.SupplierIDValue = SupplierID.Text;
        ActivityEdit1.BudgetResponsibilityAreaIDSelected = BudgetResponsibilityAreaID.Text;
        ActivityEdit1.SpendTypeIDValue = "1";
        //ActivityEdit1.FillActivityType(1, Convert.ToInt32(BudgetResponsibilityAreaID.Text), 1); remmed out 28/01/13

        //enable/disable buttons
        cmdSaveActivity.Enabled = true;
        cmdCancelActivity.Enabled = true;
        cmdActivityTypeLookup.Enabled = true;
        cmdUpdate.Enabled = false;
        cmdCancel.Enabled = false;
        cmdPrint.Enabled = false;
        cmdAdd.Enabled = false;
        cmdEdit.Enabled = false;
        cmdNew.Enabled = false;
        cmdRemove.Enabled = false;

        grd.SelectedIndex = -1;
        Session["Mode"] = "New";
    }

    protected void cmdRemove_Click(object sender, EventArgs e)
    //remove the selected line item from the grid
    {
        try //in case no selected item 
        {
            //loop through the table and delete if criteria found
            //*** at present removing one "New" record will remove them all - perhaps use ActivityName as criterion?***
            DataTable tbl = (DataTable)Session["SelectedItems"];
            //mark record for deletion
            for (int i = tbl.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = tbl.Rows[i];
                string dtitem = dr["ActivityID"].ToString();
                if (dtitem == grd.SelectedValue.ToString())
                {
                    tbl.Rows[i].Delete();
                }
            }
            //delete
            tbl.AcceptChanges();
            //add back in to the Select Items dropdown list
            cboSelectItem.Items.Add(new ListItem(grd.SelectedRow.Cells[1].Text, grd.SelectedValue.ToString()));
            //refresh grid
            grd.DataSource = tbl;
            grd.DataBind();
            if (tbl.Rows.Count == 0) { cmdUpdate.Enabled = false; } else { cmdUpdate.Enabled = true; }
            //enable buttons
            cmdEdit.Enabled = true;
            cmdRemove.Enabled = true;
            //store basket table back in session variable
            Session["SelectedItems"] = tbl;
        }
        catch { }
    }
#endregion

#region methods

    protected DataTable PopulateExistingPOID(int intPOID)
    {
        /*PO p = POHeaderDataAccess.SelectPOHeader(intPOID);
        //populate header fields
        POID.Text = intPOID.ToString();
        SupplierID.Text = p.SupplierID.ToString();
        SupplierName.Text = p.SupplierName;
        SAPVendorCode.Text = p.SAPVendorCode;
        Comments.Text = p.Comments;
        DeliveryAddressID.SelectedValue = p.DeliveryAddressID.ToString();
        DeliveryAddress.Text = p.DeliveryAddress;
        RequisitionBy.Text = p.RequisitionBy.ToLower();
        NPICoordinator.SelectedValue = p.NPICoordinator.ToLower();
        PONumber.Text = p.PONumber.ToUpper();
        PRNumber.Text = p.PRNumber.ToUpper();
        if (PRNumber.Text.Trim() == String.Empty)
        {
            //Fill released by dropdown with available approvers 
            //FillApprovers(Convert.ToInt32(BudgetResponsibilityAreaID.Text), Convert.ToInt32(CustomerID.Text), 0);  // files missing
        }
        else
        {
            //Fill released by dropdown with just the selected approver
            ReleasedBy.Items.Add(new ListItem(p.ReleasedBy.ToLower(), p.ReleasedBy.ToLower()));
        }
        ReleasedBy.SelectedValue = p.ReleasedBy.ToLower();
        
        //populate selected items table
        DataTable tbl = POHeaderDataAccess.SelectActivitiesForPOID(intPOID);
        return tbl;*/ //Vidya
        return null;
    }
    
    protected void RemoveFromUnallocated(Activity a)
    {
        DropDownList d = cboSelectItem;
        for (int i = 0; i < d.Items.Count; i++)
        {
            if (d.Items[i].Value == a.ActivityID.ToString())
            {
                d.Items.RemoveAt(i);
                return;
            }
        }
    }

    protected void LeaveScreen()
    {
        //clear selected items 
        Session["SelectedItems"] = null;
        Session["SelectedActivityID"] = null;

        if ((String)Session["ReturnToScreen"] == "Worklist")
        {
            Response.Redirect("Worklist.aspx");
        }
        else if ((String)Session["ReturnToScreen"] == "ActivityView_rs")
        {
            Response.Redirect("ActivityView.aspx");
        }
        else if ((String)Session["ReturnToScreen"] == "ActivityView")
        {
            Response.Redirect("Default.aspx");
        }
        else if ((String)Session["ReturnToScreen"] == "PurchOrderView")
        {
            Response.Redirect("PurchOrderView.aspx");
        }
    }
    
    protected void CopyToTable(Activity a, DataRow dr)
    {
        dr["ActivityID"] = a.ActivityID;
        dr["ProjectToCustomerID"] = ProjectToCustomerID.Text;
        dr["ActivityName"] = a.ActivityName;
        dr["ShortText"] = a.ShortText;
        dr["ItemQuantity"] = a.ItemQuantity;
        dr["UnitPrice"] = String.Format("{0:0.00}", a.UnitPrice);
        dr["MonthRequired"] = a.MonthRequired;
        dr["ProjectMonth"] = a.ProjectMonth;
        dr["MonthShortName"] = a.MonthShortName;
        dr["POID"] = POID.Text;
        dr["DocumentNumber"] = a.DocumentNumber;
        dr["SpendTypeID"] = a.SpendTypeID;
        dr["ActivityTypeID"] = a.ActivityTypeID;
        dr["ActivityTypeName"] = a.ActivityTypeName;
        dr["GLCode"] = a.GLCode;
        dr["CommodityCode"] = a.CommodityCode;
        if (a.SupplierID != "")
        {
            dr["SupplierID"] = a.SupplierID;
        }
        dr["SupplierRefNo"] = a.SupplierRefNo;
        dr["Comments"] = a.Comments;
        dr["LastUpdatedBy"] = a.LastUpdatedBy;
        dr["CreatedBy"] = a.CreatedBy;
        dr["Month01"] = a.Month01;
        dr["Month02"] = a.Month02;
        dr["Month03"] = a.Month03;
        dr["Month04"] = a.Month04;
        dr["Month05"] = a.Month05;
        dr["Month06"] = a.Month06;
        dr["Month07"] = a.Month07;
        dr["Month08"] = a.Month08;
        dr["Month09"] = a.Month09;
        dr["Month10"] = a.Month10;
        dr["Month11"] = a.Month11;
        dr["Month12"] = a.Month12;
        dr["TotalCost"] = String.Format("{0:0.00}", a.UnitPrice * a.ItemQuantity);
    }

    protected Activity CopyFromTable(DataRow dr)
    {
        Activity a = new Activity();

        a.ActivityID = Convert.ToInt32(dr["ActivityID"]);
        a.ProjectToCustomerID = Convert.ToInt32(dr["ProjectToCustomerID"]);
        a.ActivityName = dr["ActivityName"].ToString();
        a.ShortText = dr["ShortText"].ToString();
        a.ItemQuantity = Convert.ToInt32(dr["ItemQuantity"]);
        a.UnitPrice = Convert.ToDecimal(dr["UnitPrice"]);
        a.MonthRequired = dr["MonthRequired"].ToString();
        a.ProjectMonth = dr["ProjectMonth"].ToString();
        a.MonthShortName = dr["MonthShortName"].ToString();
        a.POID = dr["POID"].ToString();
        a.DocumentNumber = dr["DocumentNumber"].ToString();
        a.BudgetResponsibilityAreaID = BudgetResponsibilityAreaID.Text;
        a.SpendTypeID = Convert.ToInt32(dr["SpendTypeID"]);
        a.ActivityTypeID = Convert.ToInt32(dr["ActivityTypeID"]);
        a.ActivityTypeName = dr["ActivityTypeName"].ToString();
        a.SupplierID = dr["SupplierID"].ToString();
        a.SupplierRefNo = dr["SupplierRefNo"].ToString();
        a.Comments = dr["Comments"].ToString();
        //a.LastUpdatedBy = dr["LastUpdatedBy"].ToString();
        a.LastUpdatedBy = Session["CurrentUserName"].ToString(); //amended 20/11/13
        a.CreatedBy = dr["CreatedBy"].ToString();
        a.Month01 = dr["Month01"].ToString();
        a.Month02 = dr["Month02"].ToString();
        a.Month03 = dr["Month03"].ToString();
        a.Month04 = dr["Month04"].ToString();
        a.Month05 = dr["Month05"].ToString();
        a.Month06 = dr["Month06"].ToString();
        a.Month07 = dr["Month07"].ToString();
        a.Month08 = dr["Month08"].ToString();
        a.Month09 = dr["Month09"].ToString();
        a.Month10 = dr["Month10"].ToString();
        a.Month11 = dr["Month11"].ToString();
        a.Month12 = dr["Month12"].ToString();
        return a;
    }

    protected double CalculatePOValue(DataTable tbl)
    {
        double dblPOValue = 0;
        foreach (DataRow r in tbl.Rows)
        {
            dblPOValue += Convert.ToDouble(r["UnitPrice"]) * Convert.ToInt32(r["ItemQuantity"]);
        }
        return dblPOValue;
    }

    protected void PopulateDeliveryAddress(string strBudgetResponsibilityAreaID)
    {
        string strDeliveryAddressID = Common.ADOLookup("DeliveryAddressID", "tblBudgetResponsibilityArea", "BudgetResponsibilityAreaID = " + strBudgetResponsibilityAreaID);
        DeliveryAddressID.SelectedValue = strDeliveryAddressID;
        //DeliveryAddress.Text = POHeaderDataAccess.SelectDeliveryAddress(Convert.ToInt32(strDeliveryAddressID));  // files missing
    }

    protected void MsgBox(string strTitle, string strMessage)
    {
        ClientScript.RegisterStartupScript(this.GetType(), strTitle, "alert('" + Common.Left(strMessage, 35) + "');", true);
    }

#endregion

#region editactivitybuttons

    protected void cmdSaveActivity_Click(object sender, EventArgs e)
    //save activity record to data table (not to database yet)
    {
        if (Page.IsValid)
        {
            //transfer activity details from Activity Edit area into Activity object 
            Activity a = ActivityEdit1.SelectActivity;
            //retrieve temp table from session
            DataTable tbl = (DataTable)Session["SelectedItems"];
            //if not a new record prepare by removing the current record from the temporary table
            if (a.ActivityID.ToString() != "0")
            {
                //first, mark record for deletion
                string dtitem;
                for (int i = tbl.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow r = tbl.Rows[i];
                    dtitem = r["ActivityID"].ToString();
                    if (dtitem == a.ActivityID.ToString())
                    {
                        tbl.Rows[i].Delete();
                    }
                }
                //then delete
                tbl.AcceptChanges();
            }
            //add the selected item to the table
            DataRow dr = tbl.NewRow();
            CopyToTable(a, dr);
            tbl.Rows.Add(dr);
            //Clear form
            ActivityEdit1.ClearForm();
            //enable/disable buttons
            cmdSaveActivity.Enabled = false;
            cmdCancelActivity.Enabled = false;
            cmdActivityTypeLookup.Enabled = false;
            //cmdUpdate.Enabled = true; ***see below***
            cmdCancel.Enabled = true;
            cmdPrint.Enabled = true;
            cmdAdd.Enabled = true;
            cmdEdit.Enabled = false;
            cmdNew.Enabled = true;
            cmdRemove.Enabled = false;
            //refresh grid
            grd.DataSource = tbl;
            grd.DataBind();
            if (tbl.Rows.Count == 0) { cmdUpdate.Enabled = false; } else { cmdUpdate.Enabled = true; }
            //store basket table back in session variable
            Session["SelectedItems"] = tbl;
        }
    }

    protected void cmdCancelActivity_Click(object sender, EventArgs e)
    //cancel editing activity record
    {
        //clear form
        ActivityEdit1.ClearForm();
        //enable/disable buttons
        cmdSaveActivity.Enabled = false;
        cmdCancelActivity.Enabled = false;
        cmdActivityTypeLookup.Enabled = false;
        cmdUpdate.Enabled = true;
        cmdCancel.Enabled = true;
        cmdPrint.Enabled = true;
        cmdAdd.Enabled = true;
        cmdEdit.Enabled = false;
        cmdNew.Enabled = true;
        cmdRemove.Enabled = false;

    }

#endregion

#region dropdowns

    protected void FillActivity(int intProjectToCustomerID)
    //Fill the list of unallocated A&P activities
    {
        {
            //Get a data table object containing the values
            SqlCommand cmd = DataAccess.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "FillActivity";
            cmd.Parameters.AddWithValue("@ProjectToCustomerID", intProjectToCustomerID);
            DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
            DropDownList d = cboSelectItem;
            d.DataSource = tbl;
            d.DataBind();
        }
    }

    protected void FillNPICoordinators(int intBudgetResponsibilityAreaID)
    //Fill the NPI Coordinator dropdownlist for the budget responsibility area
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillNPICoordinators";
        cmd.Parameters.AddWithValue("@BudgetResponsibilityAreaID", intBudgetResponsibilityAreaID);
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = NPICoordinator;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillApprovers(int intBudgetResponsibilityAreaID, int intCustomerID, int intBrandID)
    //Fill the Approver dropdownlist for the budget responsibility area, customer and brand
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillApprovers";
        cmd.Parameters.AddWithValue("@BudgetResponsibilityAreaID", intBudgetResponsibilityAreaID);
        cmd.Parameters.AddWithValue("@CustomerID", intCustomerID);
        cmd.Parameters.AddWithValue("@BrandID", intBrandID);
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = ReleasedBy;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillDeliveryAddressID()
    //Fill the DeliveryAddress dropdown
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillDeliveryAddress";
        cmd.Parameters.AddWithValue("@BudgetResponsibilityAreaID", BudgetResponsibilityAreaID.Text);
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = DeliveryAddressID;
        d.DataSource = tbl;
        d.DataBind();
    }

#endregion

#region supplierbuttons

    protected void cmdSaveSupplier_Click(object sender, EventArgs e)
    {
        //Populate supplier details in POHeader screen 
        SupplierID.Text = SupplierGrid1.SelectedValue;
        DataTable tbl = SupplierDataAccess.SelectSupplier(Convert.ToInt32(SupplierGrid1.SelectedValue));
        SupplierName.Text = tbl.Rows[0]["SupplierName"].ToString();
        SAPVendorCode.Text = tbl.Rows[0]["SAPVendorCode"].ToString();
        mvw.ActiveViewIndex = 0;
    }

    protected void cmdCancelSupplier_Click(object sender, EventArgs e)
    {
        mvw.ActiveViewIndex = 0;
    }

    protected void cmdSupplierLookup_Click(object sender, EventArgs e)
    {
        SupplierGrid1.PopulateGrid();
        mvw.ActiveViewIndex = 1;
    }

#endregion

#region activitytypelookup

    protected void cmdSaveActivityType_Click(object sender, EventArgs e)
    {
        //Populate activitytype ID and activitytype name in ActivityEdit screen (and set txtChanged flag to true)
        ActivityEdit1.ActivityTypeIDValue = ActivityTypeGrid1.SelectedValue;
    }

    protected void cmdCancelActivityType_Click(object sender, EventArgs e)
    {
        //temporary fix - isChanged flag always set to true when exiting this screen
        ActivityEdit1.ChangedValue = "1";
    }

    protected void cmdActivityTypeLookup_Click(object sender, EventArgs e)
    {
        //Select the spend type and BRA (added 24/02/13)
        ActivityTypeGrid1.SelectFilters(ActivityEdit1.SpendTypeIDValue, BudgetResponsibilityAreaID.Text);
        mp2.Show();
    }

#endregion

}
