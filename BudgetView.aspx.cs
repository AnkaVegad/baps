﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class BudgetView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //check session not expired and high enough user level
        if (Session["SelectedProjectToCustomerID"] == null || Convert.ToInt16(Session["CurrentUserLevelID"]) >= 15)
        {
            Response.Redirect("ProjectView.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                //retrieve the ProjectID and CustomerID for this ProjectToCustomer record
                DataTable tbl = ProjectDataAccess.SelectProjectToCustomer(Convert.ToInt32(Session["SelectedProjectToCustomerID"]));
                DataRow dr = tbl.Rows[0];
                ProjectID.Text = dr["ProjectID"].ToString();
                CustomerID.Text = dr["CustomerID"].ToString();
                //populate annual totals
                PopulateAnnualTotals(int.Parse(ProjectID.Text), int.Parse(CustomerID.Text));

                SetMode("Grid");
            }
            //populate grids
            grdReallocationFrom.DataSource = ReallocationDataAccess.SelectReallocationFrom(Convert.ToInt32(ProjectToCustomerID.Text));
            grdReallocationFrom.DataBind();
            grdReallocationTo.DataSource = ReallocationDataAccess.SelectReallocationTo(Convert.ToInt32(ProjectToCustomerID.Text));
            grdReallocationTo.DataBind();
        }
    }

#region events

    protected void cmdUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            if (Page.IsValid)
            {
                string strMessage = ProjectToCustomerEdit1.UpdateRecord();
                if (strMessage == "[none]")
                {
                    SetMode("Grid");
                }
                else
                {
                    MsgBox("Error", strMessage);
                }
            }
            PopulateAnnualTotals(int.Parse(ProjectID.Text), int.Parse(CustomerID.Text));
        }
        catch
        {
            MsgBox("Error", "Could not save changes.");
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        SetMode("Grid");
    }

    protected void cmdEdit_Click(object sender, EventArgs e)
    {
        //enable controls to edit record
        Session["Mode"] = "Edit";
        SetMode("Edit");
        ProjectToCustomerEdit1.PopulateForm(Convert.ToInt32(Session["SelectedProjectToCustomerID"]));
    }

    protected void cmdReallocateFrom_Click(object sender, EventArgs e)
    {
        //check permissions (added 06/12/12)
        if (Common.CheckAuthorisations(Convert.ToInt32(Session["SelectedProjectToCustomerID"]), Session["CurrentUserName"].ToString()) > 0)
        {
            Response.Redirect("Reallocation.aspx?Mode=From");
        }
    }

    protected void cmdReallocateTo_Click(object sender, EventArgs e)
    {
        //check permissions (added 06/12/12)
        if (Common.CheckAuthorisations(Convert.ToInt32(Session["SelectedProjectToCustomerID"]), Session["CurrentUserName"].ToString()) > 0)
        {
            Response.Redirect("Reallocation.aspx?Mode=To");
        }
    }

    protected void cmdBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("ProjectView.aspx");
    }

#endregion

#region methods

    protected void SetMode(string strMode)
    {
        if (strMode == "Edit")
        {
            cmdUpdate.Visible = true;
            cmdCancel.Visible = true;
            cmdEdit.Visible = false;
            cmdReallocateFrom.Visible = false;
            cmdReallocateTo.Visible = false;
            cmdBack.Visible = false;
            AnnualTotals1.Visible = false;
            ProjectToCustomerEdit1.Visible = true;
        }
        if (strMode == "Grid")
        {
            cmdUpdate.Visible = false;
            cmdCancel.Visible = false;
            //administrator and above can reallocate budgets
            if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 15)
            {
                cmdEdit.Visible = true;
                cmdReallocateFrom.Visible = true;
                cmdReallocateTo.Visible = true;
            }
            cmdBack.Visible = true;
            AnnualTotals1.Visible = true;
            ProjectToCustomerEdit1.Visible = false;
        }
    }

    protected void PopulateAnnualTotals(int intProjectID, int intCustomerID)
    //populate annual totals
    {
        DataTable t = ActivityDataAccess.SelectActivityViewHeader(intProjectID, intCustomerID);
        DataRow r = t.Rows[0];
        //populate ProjectToCustomer data
        IONumber.Text = r["IONumber"].ToString();
        ProjectName.Text = r["ProjectName"].ToString();
        CustomerName.Text = r["CustomerName"].ToString();
        ProjectToCustomerID.Text = r["ProjectToCustomerID"].ToString();
        ProjectYear.Text = r["ProjectYear"].ToString();
        AnnualTotals1.PopulateAnnualValues(r);
    }

    protected void MsgBox(string strTitle, string strMessage)
    {
        ClientScript.RegisterStartupScript(this.GetType(), strTitle, "alert('" + Common.Left(strMessage, 35) + "');", true);
    }

#endregion

}
