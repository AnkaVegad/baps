<%@ Page Title="" Language="C#" MasterPageFile="~/AP.master" AutoEventWireup="true" CodeFile="ForecastSnapshot.aspx.cs" Inherits="ForecastSnapshot" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <!--Page Header-->
    <table border="0" width="1095px">
        <tr>
            <td align="left">
                <asp:Label ID="Label11" Text="Audit" runat="server" CssClass="page_header" />
            </td><td>
                <asp:Label ID="ProjectToCustomerID" runat="server" visible="false" Width="30px" CssClass="label_text" />
                <asp:Label ID="ProjectID" runat="server" visible="false" Width="30px" />
                <asp:Label ID="CustomerID" runat="server" visible="false" Width="30px" />
            </td>
            <td align="right">
                <asp:Label ID="Label901" Text="Project" runat="server" CssClass="page_header" />
            </td>
            <td align="left">
                <asp:Label ID="ProjectName" Text="" runat="server" CssClass="page_header_text" Width="250px" />
            </td>
            <td align="right">
                <asp:Label ID="Label902" Text="Customer" runat="server" CssClass="page_header" />
            </td>
            <td align="left">
                <asp:Label ID="CustomerName" Text="" runat="server" CssClass="page_header_text" Width="150px" />
            </td>
            <td align="right">
                <asp:Label ID="Label903" Text="IO Number" runat="server" CssClass="page_header"></asp:Label>
            </td>
            <td align="left">
                <asp:Label ID="IONumber" Text="" runat="server" CssClass="page_header_text" Width="110px" />
            </td>
        </tr>
        <tr><td style="font-size:3px">&nbsp;</td></tr>
    </table>

    <!--Data Source - required to take advantage of sorting and paging functionality of the GridView-->
    <asp:SqlDataSource ID="sds" runat="server" 
        ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
        SelectCommandType="StoredProcedure" SelectCommand="SelectForecastSnapshot">
        <SelectParameters>
           <asp:ControlParameter Name="ProjectToCustomerID" ControlID="ProjectToCustomerID" PropertyName="Text" />
        </SelectParameters>
    </asp:SqlDataSource>

    <!--ForecastSnapshot Grid-->
    <table border="0" width="1095px"><tr><td align="left">
        Forecast Snapshot
    </td></tr></table><br />
    <div id="project_grid">
        <asp:GridView ID="grd" runat="server" width="1080px"
            AutoGenerateColumns="False" GridLines="Vertical"
            RowStyle-Wrap="false" EmptyDataText="No Records Found" 
            HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
            SelectedRowStyle-Wrap="false"             
            AllowSorting="True" onsorted="grd_Sorted" AllowPaging="False"
            DataKeyNames="ForecastSnapshotID" DataSourceID="sds">
            <Columns>
                <asp:BoundField DataField="ForecastSnapshotID" ReadOnly="True" Visible="False" />
                <asp:BoundField DataField="SpendTypeName" HeaderText="Spend Type" SortExpression="SpendTypeName" />
                <asp:BoundField DataField="SnapshotMonth" HeaderText="Snapshot Month" SortExpression="SnapshotMonth" />
                <asp:BoundField DataField="SnapshotYear" HeaderText="Snapshot Year" SortExpression="SnapshotYear" />
                <asp:BoundField DataField="Month01" HeaderText="Jan" SortExpression="Month01" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month02" HeaderText="Feb" SortExpression="Month02" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month03" HeaderText="Mar" SortExpression="Month03" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month04" HeaderText="Apr" SortExpression="Month04" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month05" HeaderText="May" SortExpression="Month05" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month06" HeaderText="Jun" SortExpression="Month06" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month07" HeaderText="Jul" SortExpression="Month07" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month08" HeaderText="Aug" SortExpression="Month08" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month09" HeaderText="Sep" SortExpression="Month09" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month10" HeaderText="Oct" SortExpression="Month10" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month11" HeaderText="Nov" SortExpression="Month11" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month12" HeaderText="Dec" SortExpression="Month12" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
            </Columns>
            <RowStyle CssClass="grid_row" Wrap="False" />
            <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
            <HeaderStyle CssClass="grid_header" Wrap="False" />
            <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
        </asp:GridView>
    </div>
<br />
    <!--Data Source - required to take advantage of sorting and paging functionality of the GridView-->
    <asp:SqlDataSource ID="sdsBudget" runat="server" 
        ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
        SelectCommandType="StoredProcedure" SelectCommand="sp_ap_SelectBudgetSnapshot">
        <SelectParameters>
           <asp:ControlParameter Name="ProjectToCustomerID" ControlID="ProjectToCustomerID" PropertyName="Text" />
        </SelectParameters>
    </asp:SqlDataSource>

    <!--BudgetSnapshot Grid-->
    <table border="0" width="1095px"><tr><td align="left">
        Budget Snapshot
    </td></tr></table><br />
    <div id="budget_snapshot_grid">
        <asp:GridView ID="grdBudget" runat="server" width="500px"
            AutoGenerateColumns="False" GridLines="Vertical"
            RowStyle-Wrap="false" EmptyDataText="No Records Found" 
            HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
            SelectedRowStyle-Wrap="false"             
            AllowSorting="True" onsorted="grd_Sorted" AllowPaging="False"
            DataKeyNames="BudgetSnapshotID" DataSourceID="sdsBudget">
            <Columns>
                <asp:BoundField DataField="BudgetSnapshotID" ReadOnly="True" Visible="False" />
                <asp:BoundField DataField="SpendTypeName" HeaderText="Spend Type" SortExpression="SpendTypeName" />
                <asp:BoundField DataField="SnapshotMonth" HeaderText="Snapshot Month" SortExpression="SnapshotMonth" />
                <asp:BoundField DataField="SnapshotYear" HeaderText="Snapshot Year" SortExpression="SnapshotYear" />
                <asp:BoundField DataField="BudgetValue" HeaderText="Budget Value" SortExpression="BudgetValue" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
            </Columns>
            <RowStyle CssClass="grid_row" Wrap="False" />
            <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
            <HeaderStyle CssClass="grid_header" Wrap="False" />
            <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
        </asp:GridView>
    </div>


</asp:Content>

