﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="POHeaderPrint.aspx.cs" Inherits="POHeaderPrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Print Purchase Requisition Form</title>
</head>
<body style="text-align:left;margin:10px;">
    <form id="frmPOHeaderPrint" runat="server">
    <div id="po_header_print">
        <!--Page Header-->
        <br /><br />
        <table border="0" width="700px">
            <tr><td align="left">
                <asp:Label ID="Label1" Text="Purchase Requisition Form" runat="server" CssClass="print_page_header"></asp:Label>
            </td>
            <td align="left">
                <asp:Label ID="Label901" Text="Project:" runat="server" CssClass="print_page_header" />
                <asp:Label ID="ProjectName" runat="server" CssClass="print_page_header" Width="250px" />
            </td>
            <td align="left">
                <asp:Label ID="Label902" Text="Customer:" runat="server" CssClass="print_page_header"></asp:Label>
                <asp:Label ID="CustomerName" runat="server" CssClass="print_page_header" Width="150px" />
            </td>
            <td align="left">
                <asp:Label ID="Label903" Text="IO Number:" runat="server" CssClass="print_page_header"></asp:Label>
                <asp:Label ID="IONumber" runat="server" CssClass="print_page_header" Width="110px" />
            </td></tr>
        </table>
        <br /><br />
        
        
        <!--Upper Edit Area-->
        <table class="print_table" width="700px" border="0">
            <tr><td valign="top">
                <table border="0">
                    <tr><td>Supplier</td><td>
                        <asp:Label ID="SupplierName" runat="server" Width="180px" CSSClass="print_label_text" />
                    </td></tr>
                    <tr><td>SAP Vendor Code</td><td>
                        <asp:Label ID="SAPVendorCode" runat="server" Width="80px" CSSClass="print_label_text" />
                    </td></tr>
                    <tr><td valign="top">Comments</td><td width="15">
                        <asp:Textbox ID="Comments" runat="server" Width="176px" Height="55px" CSSClass="print_label_text" 
                            TextMode="MultiLine" style="overflow:hidden" />
                    </td></tr>
                </table>
            </td>
            <td valign="top">
                <table border="0">
                    <tr><td>Delivery Address</td></tr>
                    <tr><td>
                        <asp:Label ID="DeliveryAddress" runat="server" Width="175px" Height="80px" CSSClass="print_label_text" />
                    </td></tr>               
                </table>
            </td>
            <td valign="top">
                <table style="width:150px;" border="0">
                    <tr><td>Requisition By</td><td>
                        <asp:Label ID="RequisitionBy" runat="server" Width="150px" CSSClass="print_label_text" />
                    </td></tr>
                    <tr><td>NPI Coordinator</td><td>
                        <asp:Label ID="NPICoordinator" runat="server" Width="150px" CSSClass="print_label_text" />
                    </td></tr>
                    <tr><td>Approver</td><td>
                        <asp:Label ID="ReleasedBy" runat="server" Width="150px" CSSClass="print_label_text" />
                    </td></tr>
                    <tr><td>PR Number</td><td>
                        <asp:Label ID="PRNumber" runat="server" Width="80px" Height="13px" CSSClass="print_label_text" />
                    </td></tr>
                    <tr><td>PO Number</td><td>
                        <asp:Label ID="PONumber" runat="server" Width="80px" Height="13px" CSSClass="print_label_text" />
                    </td></tr>
                </table>
            </td>
            <td valign="top">
            </td></tr>
        </table>

        <table>
            <tr><td align="left">
                Line Items
            </td></tr>
        </table>

        <!--Grid-->
        <div id="poheader_grid">
        <asp:GridView ID="grd" runat="server" width="700px"
            AutoGenerateColumns="False" GridLines="Vertical" RowStyle-Wrap="false" EmptyDataText="No Activities" 
            HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
            SelectedRowStyle-Wrap="false" AllowSorting="false" onrowdatabound="grd_RowDataBound"
            DataKeyNames="ActivityID" ShowFooter="True">
            <Columns>
                <asp:BoundField DataField="ActivityID" InsertVisible="False" ReadOnly="True" Visible="False" />
                <asp:BoundField DataField="ShortText" HeaderText="Short Text" SortExpression="ShortText" />
                <asp:BoundField DataField="ActivityTypeName" HeaderText="Type" SortExpression="ActivityTypeName" />
                <asp:BoundField DataField="SupplierRefNo" HeaderText="Supplier's Ref" SortExpression="SupplierRefNo" />
                <asp:BoundField DataField="ItemQuantity" HeaderText="Quantity" SortExpression="ItemQuantity" />
                <asp:BoundField DataField="MonthShortName" HeaderText="Month" SortExpression="MonthRequired" />
                <asp:BoundField DataField="UnitPrice" HeaderText="Unit Price" SortExpression="POItemNo" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:0.00}" />
                <asp:BoundField DataField="TotalCost" HeaderText="Total Cost" SortExpression="POItemNo" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:0.00}" />
            </Columns>
            <RowStyle CssClass="grid_row" Wrap="False" />
            <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
            <HeaderStyle CssClass="grid_header" Wrap="False" />
            <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
        </asp:GridView>
        </div>
        <table><tr>
            <td>Currency</td>
            <td>
                <asp:Label ID="CurrencyCode" runat="server" visible="true" Width="30px" CssClass="print_label_text" />
            </td>
        </tr></table>

    </div>
    </form>
</body>
</html>
