<%@ Page Title="" Language="C#" MasterPageFile="~/AP.master" AutoEventWireup="true" CodeFile="Audit.aspx.cs" Inherits="Audit" %>

<%@ Register src="User_Controls/ReallocationGrids.ascx" tagname="ReallocationGrids" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <!--Page Header-->
    <table border="0" width="1095px">
        <tr>
            <td align="left">
                <asp:Label ID="Label11" Text="Audit" runat="server" CssClass="page_header" />
            </td><td>
                <asp:Label ID="ProjectToCustomerID" runat="server" visible="false" Width="30px" CssClass="label_text" />
                <asp:Label ID="ProjectID" runat="server" visible="false" Width="30px" />
                <asp:Label ID="CustomerID" runat="server" visible="false" Width="30px" />
            </td>
            <td align="right">
                <asp:Label ID="Label901" Text="Project" runat="server" CssClass="page_header" />
            </td>
            <td align="left">
                <asp:Label ID="ProjectName" Text="" runat="server" CssClass="page_header_text" Width="250px" />
            </td>
            <td align="right">
                <asp:Label ID="Label902" Text="Customer" runat="server" CssClass="page_header" />
            </td>
            <td align="left">
                <asp:Label ID="CustomerName" Text="" runat="server" CssClass="page_header_text" Width="150px" />
            </td>
            <td align="right">
                <asp:Label ID="Label903" Text="IO Number" runat="server" CssClass="page_header"></asp:Label>
            </td>
            <td align="left">
                <asp:Label ID="IONumber" Text="" runat="server" CssClass="page_header_text" Width="110px" />
            </td>
            <td align="right">
                <asp:Label ID="lblSearch" Text="Search:" runat="server"></asp:Label>
                <asp:TextBox ID="txtSearch" Runat="server" Width="120px"></asp:TextBox>
                <asp:Button ID="cmdGo" Text="Go" Runat="server" CausesValidation="false" />
            </td>
        </tr>
        <tr><td style="font-size:3px">&nbsp;</td></tr>
    </table>

    <!--Data Source - required to take advantage of sorting and paging functionality of the GridView-->
    <asp:SqlDataSource ID="sds1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
        FilterExpression="ActivityName Like '%{0}%'" SelectCommandType="StoredProcedure" SelectCommand="SelectActivityHistory">
        <SelectParameters>
           <asp:ControlParameter Name="ProjectToCustomerID" ControlID="ProjectToCustomerID" PropertyName="Text" />
        </SelectParameters>
        <FilterParameters>
            <asp:ControlParameter ControlID="txtSearch" PropertyName="Text" />
        </FilterParameters>
    </asp:SqlDataSource>

    <!--Activity Audit Grid-->
    <table border="0" width="1095px"><tr><td align="left">
        Activity History
    </td></tr></table><br />
    <div id="audit_grid">
        <asp:GridView ID="grdAudit1" runat="server" width="1100px"
            AutoGenerateColumns="False" GridLines="Vertical"
            RowStyle-Wrap="false" EmptyDataText="No Records Found" 
            HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
            SelectedRowStyle-Wrap="false"             
            AllowSorting="True" onsorted="grdAudit1_Sorted" AllowPaging="False" onpageindexchanged="grdAudit1_PageIndexChanged"
            DataKeyNames="ActivityID" PageSize="20" DataSourceID="sds1">
            <Columns>
                <asp:BoundField DataField="ActivityID" HeaderText="ID" SortExpression="ActivityID" ReadOnly="True" Visible="True" />
                <asp:BoundField DataField="ActivityName" HeaderText="Activity Name" SortExpression="ActivityName" />
                <asp:BoundField DataField="SpendTypeName" HeaderText="Spend" SortExpression="SpendTypeName" />
                <asp:BoundField DataField="DocumentNumber" HeaderText="Document" SortExpression="DocumentNumber" />
                <asp:BoundField DataField="ActivityStatus" HeaderText="Status" SortExpression="ActivityStatus" />
                <asp:BoundField DataField="SupplierName" HeaderText="Supplier" SortExpression="SupplierName" />
                <asp:BoundField DataField="LastUpdatedBy" HeaderText="User" SortExpression="LastUpdatedBy" />
                <asp:BoundField DataField="LastUpdatedDate" HeaderText="Date/Time" SortExpression="LastUpdatedDate" />
                <asp:BoundField DataField="Month01" HeaderText="Jan" SortExpression="Month01" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month02" HeaderText="Feb" SortExpression="Month02" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month03" HeaderText="Mar" SortExpression="Month03" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month04" HeaderText="Apr" SortExpression="Month04" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month05" HeaderText="May" SortExpression="Month05" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month06" HeaderText="Jun" SortExpression="Month06" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month07" HeaderText="Jul" SortExpression="Month07" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month08" HeaderText="Aug" SortExpression="Month08" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month09" HeaderText="Sep" SortExpression="Month09" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month10" HeaderText="Oct" SortExpression="Month10" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month11" HeaderText="Nov" SortExpression="Month11" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month12" HeaderText="Dec" SortExpression="Month12" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
            </Columns>
            <RowStyle CssClass="grid_row" Wrap="False" />
            <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
            <HeaderStyle CssClass="grid_header" Wrap="False" />
            <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
        </asp:GridView>
    </div>
    <br /><br />
    <!--Data Source - required to take advantage of sorting and paging functionality of the GridView-->
    <asp:SqlDataSource ID="sds2" runat="server" 
        ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
        SelectCommandType="StoredProcedure" SelectCommand="SelectProjectToCustomerHistory">
        <SelectParameters>
           <asp:ControlParameter Name="ProjectToCustomerID" ControlID="ProjectToCustomerID" PropertyName="Text" />
        </SelectParameters>
    </asp:SqlDataSource>

    <!--Budget Audit Grid-->
    <table border="0" width="1095px"><tr><td align="left">
        Budget History
    </td></tr></table><br />

    <div id="project_grid">
        <asp:GridView ID="grdAudit2" runat="server" width="1100px"
            AutoGenerateColumns="False" GridLines="Vertical"
            RowStyle-Wrap="false" EmptyDataText="No Records Found" 
            HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
            SelectedRowStyle-Wrap="false"
            AllowSorting="True" onsorted="grdAudit2_Sorted" AllowPaging="True" onpageindexchanged="grdAudit2_PageIndexChanged"
            DataKeyNames="ProjectToCustomerID" PageSize="20" DataSourceID="sds2">
            <Columns>
                <asp:BoundField DataField="ProjectToCustomerID" ReadOnly="True" Visible="False" />
                <asp:BoundField DataField="IONumber" HeaderText="IO" SortExpression="IONumber" />
                <asp:BoundField DataField="AllocatedAPBudget" HeaderText="A&P Budget" SortExpression="AllocatedAPBudget" />
                <asp:BoundField DataField="AllocatedTCCBudget" HeaderText="TCC Budget" SortExpression="AllocatedTCCBudget" />
                <asp:BoundField DataField="LastUpdatedBy" HeaderText="User" SortExpression="LastUpdatedBy" />
                <asp:BoundField DataField="LastUpdatedDate" HeaderText="Date/Time" SortExpression="LastUpdatedDate" />
                <asp:BoundField DataField="Comments" HeaderText="Comments" SortExpression="Comments" />
            </Columns>
            <RowStyle CssClass="grid_row" Wrap="False" />
            <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
            <HeaderStyle CssClass="grid_header" Wrap="False" />
            <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
        </asp:GridView>
    </div><br /><br />
    
    <!--Reallocation Grids-->
    <uc1:ReallocationGrids ID="ReallocationGrids1" runat="server" />
    <br />

</asp:Content>

