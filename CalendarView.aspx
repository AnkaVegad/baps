﻿

<%@ Page Title="" Language="C#" MasterPageFile="~/AP_rs.master" EnableEventValidation="false" ValidateRequest="false" AutoEventWireup="true" CodeFile="CalendarView.aspx.cs" Inherits="CalendarView"%>

<%@ Register src="User_Controls/CalendarGrid.ascx" tagname="CalendarGrid" tagprefix="uc1" %>
<%@ Register src="User_Controls/ProjectEdit_rs.ascx" tagname="ProjectEdit" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:MultiView ID="mvw" runat="server">
    <asp:View ID="calerdar_view" runat="server">
        <uc1:CalendarGrid ID="CalendarGrid1" runat="server" />
    </asp:View>
    <asp:View ID="project_view" runat="server">
        <uc2:ProjectEdit ID="ProjectEdit1" runat="server" />
        <div id="footer">
            <br />
        <!--Buttons-->        
        <asp:Button ID="cmdSave" runat="server" Text="Save"
            onclick="cmdSave_Click" CssClass="form_button" ValidationGroup="Project"/>
        <asp:Button ID="cmdCancel" runat="server" Text="Cancel"
            onclick="cmdCancel_Click" CssClass="form_button" />
        </div>  
    </asp:View>
</asp:MultiView>
</asp:Content>