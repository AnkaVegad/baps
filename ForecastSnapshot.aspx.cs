﻿using System;
using System.Data;

public partial class ForecastSnapshot : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //check session not expired and high enough user level
        //amended AH 03/10/12 to allow all users to view
        if (Session["SelectedProjectToCustomerID"] == null) // || Convert.ToInt16(Session["CurrentUserLevelID"]) >= 15)
        {
            Response.Redirect("ProjectView.aspx");
        }
        else
        {
            //retrieve the ProjectID and CustomerID for this ProjectToCustomer record
            DataTable tbl;
            if (IsPostBack)
            {
                tbl = ProjectDataAccess.SelectProjectToCustomer(Convert.ToInt32(ProjectToCustomerID.Text));
            }
            else
            {
                tbl = ProjectDataAccess.SelectProjectToCustomer(Convert.ToInt32(Session["SelectedProjectToCustomerID"]));
            }
            DataRow dr = tbl.Rows[0];
            ProjectID.Text = dr["ProjectID"].ToString();
            CustomerID.Text = dr["CustomerID"].ToString();
            ProjectToCustomerID.Text = Session["SelectedProjectToCustomerID"].ToString();

            //populate the header values
            DataTable t = ActivityDataAccess.SelectActivityViewHeader(int.Parse(ProjectID.Text), int.Parse(CustomerID.Text));
            DataRow r = t.Rows[0];
            IONumber.Text = r["IONumber"].ToString();
            ProjectName.Text = r["ProjectName"].ToString();
            CustomerName.Text = r["CustomerName"].ToString();

            //Populate the activity audit grid
            grd.DataBind();
        }
    }

    #region gridevents

        protected void grd_Sorted(object sender, EventArgs e)
        {

        }
    #endregion
}
