﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AP.master" AutoEventWireup="true" CodeFile="OptionView.aspx.cs" Inherits="OptionView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <!--Page Header-->
    <table border="0" width="1100px">
        <tr>
            <td align="left">
                <asp:Label ID="Label1" Text="Settings" runat="server" CssClass="page_header"></asp:Label>
            </td>
        </tr>
    </table>
    <br /><br />


    <div id="activity_grid">
        <asp:GridView ID="grd" runat="server" width="1070px"
            AutoGenerateColumns="False" GridLines="Vertical"
            RowStyle-Wrap="false" EmptyDataText="No Records Found" 
            HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
            SelectedRowStyle-Wrap="false"
            onrowcancelingedit="grd_RowCancelingEdit" OnRowEditing="grd_RowEditing" OnRowUpdating="grd_RowUpdating"
            DataKeyNames="OptionName" EnableModelValidation="True" >
            <Columns>
                <asp:BoundField DataField="OptionName" HeaderText="Setting" ReadOnly="True" Visible="True" SortExpression="OptionName" />
                <asp:TemplateField HeaderText="Value" SortExpression="OptionValue">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtOptionValue" runat="server" 
                            Text='<%# Bind("OptionValue") %>' TextMode="MultiLine" Width="800px"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("OptionValue") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField HeaderText="Actions" ShowEditButton="True" 
                    ShowHeader="True" />
            </Columns>
            <RowStyle CssClass="grid_row" Wrap="False" />
            <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
            <HeaderStyle CssClass="grid_header" Wrap="False" />
            <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
        </asp:GridView>
    </div>

</asp:Content>

