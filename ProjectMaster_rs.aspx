﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AP_rs.master" AutoEventWireup="true" CodeFile="ProjectMaster_rs.aspx.cs"  ValidateRequest="false" EnableEventValidation="false" Inherits="ProjectMaster" %>

<%@ Register src="User_Controls/ProjectEdit_rs.ascx" tagname="ProjectEdit" tagprefix="uc1" %>
<%@ Register src="User_Controls/FilterDropDown.ascx" tagname="FilterDropDown" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
    $(function () {
        $("[id*=grd] td").bind("click", function () {
            var row = $(this).parent();
            var GridSelection = document.getElementById('ctl00$ContentPlaceHolder1$HiddenSelectedProjectID');
            var index = -1;

            $("[id*=grd] tr").each(function () {
                if ($(this)[0] != row[0]) {
                    if (row[0].innerHTML.indexOf(">Total Forecast<") < 0 && row[0].innerHTML.indexOf(">Actuals<") < 0) {
                        index = index + 1;
                        $("td", this).removeClass("grid_selected");
                    }
                }
                else {
                    if (row[0].innerHTML.indexOf(">Total Forecast<") < 0 && row[0].innerHTML.indexOf(">Actuals<") < 0) {
                        if(GridSelection!=null)
                            GridSelection.value = index;
                    }
                }
            });
            $("td", row).each(function () {
                if (row[0].innerHTML.indexOf(">Total Forecast<") < 0 && row[0].innerHTML.indexOf(">Actuals<") < 0) {
                    if (!$(this).hasClass("grid_selected")) {
                        $(this).addClass("grid_selected");
                    } else {
                        //Don't do anything. Code below to deselect row
                        //$(this).removeClass("grid_selected");
                    }
                }
            });
        });
    });
</script>

<script type="text/javascript">
    function setscroll(ctlID) {
        var panel = document.getElementById('<%=grid.ClientID %>');
        var div_value = document.cookie.substring(document.cookie.indexOf("div_position=") + 13, document.cookie.length);
        panel.scrollTop = div_value.substring(0, div_value.indexOf(","));
        panel.scrollLeft = div_value.substring(div_value.indexOf(",") + 1, div_value.length);
    }
</script>
<script type="text/javascript">
    function savescroll(ctlID) {
        var panel = document.getElementById('<%=grid.ClientID %>');
        document.cookie = "div_position=" + panel.scrollTop + ',' + panel.scrollLeft.toString() + ";";
    }
</script>

<asp:MultiView ID="mvw" runat="server">
    <asp:View ID="vw1" runat="server">
      <div id="resizable1" class="resizable ui-widget-content" style="height:85%">
         <!--left panel-->
        <br />
        <asp:Label ID="Label900" Text="Project Master" runat="server" CssClass="page_header"/>
        <br />
        <br /> 
        <table border="0">
            <tr>
                <td>
                    <asp:Label ID="Label902" Text="Area" runat="server"/>
                </td>
                <td>
                    <asp:DropDownList id="cboBudgetResponsibilityArea" runat="server" AutoPostBack="true" Width="200px"
                        DataTextField="EntityName" DataValueField="ID" ontextchanged="cboBudgetResponsibilityArea_TextChanged" ForeColor="Black"/>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label910" Text="Brand" runat="server"/>
                </td>
                <td>
                    <asp:DropDownList id="cboBrand" runat="server" AutoPostBack="true" Width="200px"
                        DataTextField="EntityName" DataValueField="ID" ontextchanged="cboBrand_TextChanged" ForeColor="Black" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label901" Text="Year" runat="server"/>
                </td>
                <td>
                    <asp:DropDownList id="cboProjectYear" runat="server" AutoPostBack="true" Width="200px"
                        DataTextField="EntityName" DataValueField="ID" ontextchanged="cboProjectYear_TextChanged" ForeColor="Black"/>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label903" Text="Country" runat="server"/>
                </td>
                <td>
                    <asp:DropDownList id="cboCountry" runat="server" AutoPostBack="true" Width="200px"
                        DataTextField="EntityName" DataValueField="ID" ontextchanged="cboCountry_TextChanged" ForeColor="Black"/>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label904" Text="Priority" runat="server"/>
                </td>
                <td>
                    <asp:DropDownList id="cboPulsingPlanPriority" runat="server" AutoPostBack="true" Width="200px"
                        DataTextField="EntityName" DataValueField="ID" ontextchanged="cboPulsingPlanPriority_TextChanged" ForeColor="Black"/>
                </td>
            </tr>
        </table>
      </div>
      <div id="resizable2" class="ui-widget-content" style="height:85%">
           <!--Page Header-->
        <br />
                    <table>
                        <tr>
                            <td style="text-align:right">
                                <asp:Label ID="lblSearch" Text="Search:" runat="server"></asp:Label>
                                <asp:TextBox ID="txtSearch" Runat="server" Width="150px"></asp:TextBox>
                                <asp:Button ID="cmdGo" Text="Go" Runat="server" CausesValidation="false" onclick="cmdGo_Click" ForeColor="Black"/>
                            </td>
                        </tr>
                    </table> 
          <br />
        
        <!--Data Source - required to take advantage of sorting and paging functionality of the GridView-->
        <asp:SqlDataSource ID="sds" runat="server" 
            ConnectionString="<%$ ConnectionStrings:APConnectionString %>">
        </asp:SqlDataSource>

        <!--Project Grid-->
        <div id="project_grid">
            <asp:Panel id="grid" style="overflow-x:auto; overflow-y:auto; height:97%; position:relative;" runat="server">
                <asp:HiddenField ID="HiddenSelectedProjectID" runat="server" value="0"/>
            <asp:GridView ID="grd" runat="server" width="98.6%"
                AutoGenerateColumns="False" GridLines="Vertical"
                RowStyle-Wrap="false" EmptyDataText="No Records Found" 
                HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
                SelectedRowStyle-Wrap="false" AllowSorting="true"
                OnRowDataBound="grd_RowDataBound" OnSorting="grd_Sorted"
                DataKeyNames="ProjectID" ShowFooter="true">
                <Columns>
                    <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="ProjectID" ReadOnly="True" Visible="false" />
                    <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="ProjectName" HeaderText="Project Name" SortExpression="ProjectName"/>
                    <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="ProjectYear" HeaderText="Year" SortExpression="Year"/>
                    <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="ProjectOwner" HeaderText="Project Owner" SortExpression="ProjectOwner"/>
                    <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="BudgetResponsibilityAreaName" HeaderText="Area" SortExpression="BudgetResponsibilityAreaName"/>
                    <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="CountryName" HeaderText="Country" SortExpression="CountryName"/>
                    <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="PulsingPlanPriorityName" HeaderText="Priority" SortExpression="PulsingPlanPriorityName"/>
                    <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="PulsingPlanTiming" HeaderText="Timing" SortExpression="PulsingPlanTiming"/>
                </Columns>
                <RowStyle CssClass="grid_row" Wrap="False" BackColor="#EFF3FB" />
                <HeaderStyle BackColor="Gray" CssClass="GVFixedHeader" ForeColor="White" Wrap="false" Height="15px"></HeaderStyle>           
                <FooterStyle CssClass="grid_footer" Wrap="False" />
                <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" BackColor="White" />
            </asp:GridView>
                </asp:Panel>
            <asp:Label ID="txtMessage" runat="server" />
       </div><br />

       <div id="footer">
           <br />
           &nbsp;
            <!--Grid Buttons-->
            <asp:Button ID="cmdNew" runat="server" Text="New Project" causesvalidation="false"
                onclick="cmdNew_Click" CssClass="form_button" enabled="false" width="100px" />
            <asp:Button ID="cmdEdit" runat="server" Text="Edit Project" causesvalidation="false"
                onclick="cmdEdit_Click" CssClass="form_button" enabled="true" width="100px" />
            <asp:Button ID="cmdCopy" runat="server" Text="Copy Project" causesvalidation="false"
                onclick="cmdCopy_Click" CssClass="form_button" enabled="false" width="100px" />
            <asp:Button ID="cmdDelete" runat="server" Text="Delete Project" causesvalidation="false" width="100px" 
                onclick="cmdDelete_Click" OnClientClick="return confirm('Are you sure you want to delete this project?');"
                CssClass="form_button" Enabled="false" /><br /><br />
       </div>
    </asp:View>
    <asp:View ID="vw2" runat="server">
       <div id="resizable2" class="ui-widget-content">
         <!--Page Header-->
        <br />
        <table border="0" width="1100px">
            <tr>
                <td align="left">
                    <asp:Label ID="lblHeader" runat="server" CssClass="page_header"/>
                </td>
            </tr>
        </table><br />
        <!--Edit Area-->
        <div id="project_edit_area">
            <uc1:ProjectEdit ID="ProjectEdit1" runat="server" />
            <table width="1080px">
                <tr><td align="right">
                    <asp:Button ID="cmdUpdate" runat="server" Text="Save" 
                        onclick="cmdUpdate_Click" CssClass="form_button" Enabled="false" validationgroup="Project" />&nbsp;
                    <asp:Button ID="cmdCancel" runat="server" Text="Cancel" causesvalidation="false"
                        onclick="cmdCancel_Click" CssClass="form_button" Enabled="true" />
                </td></tr>
            </table>
        </div>  
        </div>
        <!-- Project Creation message ModalPopupExtender -->
        <asp:Button id="cmdDummy" runat="server" style="display:none;" />
        <asp:ModalPopupExtender ID="mp1" runat="server" PopupControlID="Panel1" TargetControlID="cmdDummy" BackgroundCssClass="modalBackground" />
        <asp:Panel ID="Panel1" runat="server" CssClass="smallModalPopup" style = "display:none" HorizontalAlign="Center">    
            <asp:Label ID="lblMessage1" runat="server" Text="Project Saved."></asp:Label><br /><br />
            <asp:Label ID="lblMessage2" runat="server" Text=""></asp:Label><br /><br /><br /><br /><br /><br />
            <!--Buttons-->
            <asp:Button ID="cmdOK" runat="server" Text="OK" onclick="cmdOK_Click" 
                CssClass="form_button" Enabled="true" CausesValidation="true"/>
        </asp:Panel>
        
     </asp:View>
    </asp:MultiView>
</asp:Content>
