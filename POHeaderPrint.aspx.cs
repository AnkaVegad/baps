﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class POHeaderPrint : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //check for session expired
        if (Session["SelectedProjectToCustomerID"] == null)
        {
            Response.Redirect("ProjectView.aspx");
        }

        //populate project header from Project 
        DataTable t = ProjectDataAccess.SelectProjectToCustomer(Convert.ToInt32(Session["SelectedProjectToCustomerID"]));
        DataRow r = t.Rows[0];
        ProjectName.Text = r["ProjectName"].ToString();
        CustomerName.Text = r["CustomerName"].ToString();
        IONumber.Text = r["IONumber"].ToString();
        CurrencyCode.Text = r["CurrencyCode"].ToString();

        PO p = POHeaderDataAccess.SelectPOHeader(Convert.ToInt32(Session["SelectedPOID"]));
        //populate header fields
        SupplierName.Text = Common.ADOLookup("SupplierName", "tblSupplier", "SupplierID = " + p.SupplierID.ToString());
        SAPVendorCode.Text = p.SAPVendorCode;
        Comments.Text = p.Comments;
        DeliveryAddress.Text = p.DeliveryAddress;
        RequisitionBy.Text = p.RequisitionBy.ToLower();
        NPICoordinator.Text = p.NPICoordinator.ToLower();
        ReleasedBy.Text = p.ReleasedBy.ToLower();
        PONumber.Text = p.PONumber.ToUpper();
        PRNumber.Text = p.PRNumber.ToUpper();

        //populate selected items table
        DataTable tbl = POHeaderDataAccess.SelectActivitiesForPOID(Convert.ToInt32(Session["SelectedPOID"]));

        //populate line items grid
        grd.DataSource = tbl;
        grd.DataBind();

    }

    double dblTotal = 0;
    
    protected void grd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            double dblCurrent = Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "TotalCost"));
            //increment the total  
            dblTotal += dblCurrent;
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[5].Text = "Total";
            //display the total
            e.Row.Cells[6].HorizontalAlign = HorizontalAlign.Right;
            e.Row.Cells[6].Text = String.Format("{0:0.00}", dblTotal);
        }
    }
}
