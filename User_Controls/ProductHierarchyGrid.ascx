﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductHierarchyGrid.ascx.cs" Inherits="User_Controls_ProductHierarchyGrid" %>

    <!--Page Header-->
    <table border="0" width="1000px">
        <tr>
            <td align="left">
                <asp:Label ID="lblHeader" Text="Product Hierarchy" runat="server" CssClass="page_header" />
            </td>
            <td align="right">
                <asp:Label ID="lblSearch" Text="Search:" runat="server" Visible="false"></asp:Label>
                <asp:TextBox ID="txtSearch" Runat="server" Width="120px" Visible="false"/>
                <asp:Button ID="cmdGo" Text="Go" Runat="server" CausesValidation="false" Visible="false" />
            </td>
        </tr>
        <tr><td style="font-size:3px">&nbsp;</td></tr>
    </table>

    <!--Data Source - required to take advantage of sorting and paging functionality of the GridView-->
    <asp:SqlDataSource ID="sds" runat="server" 
        ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
        FilterExpression="SubBrandName Like '%{0}%'" >
        <FilterParameters>
            <asp:ControlParameter ControlID="txtSearch" PropertyName="Text" />
        </FilterParameters>
    </asp:SqlDataSource>

    <!--ProductHierarchy Grid-->
    <div id="project_grid">
        <asp:GridView ID="grd" runat="server" width="1000px"
            AutoGenerateColumns="False" GridLines="Vertical"
            RowStyle-Wrap="false" EmptyDataText="No Records Found" 
            HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
            SelectedRowStyle-Wrap="false"
            AllowSorting="True" onsorted="grd_Sorted" AllowPaging="True" onpageindexchanged="grd_PageIndexChanged"
            onselectedindexchanged="grd_SelectedIndexChanged" 
            OnRowDataBound="grd_RowDataBound" 
            DataKeyNames="ProductHierarchyID" ShowFooter="False" PageSize="25">
            <Columns>
                <asp:BoundField DataField="ProductHierarchyID" ReadOnly="True" Visible="false" />
                <asp:BoundField DataField="BusinessGroupName" HeaderText="Business Group" SortExpression="BusinessGroupName" />
                <asp:BoundField DataField="BusinessUnitName" HeaderText="Business Unit" SortExpression="BusinessUnitName" />
                <asp:BoundField DataField="EuropeanCategoryName" HeaderText="European Category" SortExpression="EuropeanCategoryName" />
                <asp:BoundField DataField="UKCategoryName" HeaderText="UK Category" SortExpression="UKCategoryName" />
                <asp:BoundField DataField="BrandName" HeaderText="Brand" SortExpression="BrandName" />
                <asp:BoundField DataField="SubBrandName" HeaderText="Brand-Market" SortExpression="SubBrandName" />
                <asp:BoundField DataField="StatusName" HeaderText="Status" SortExpression="StatusName" />
            </Columns>
            <RowStyle CssClass="grid_row" Wrap="False" />
            <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
            <HeaderStyle CssClass="grid_header" Wrap="False" />
            <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
        </asp:GridView>
    </div><br />
