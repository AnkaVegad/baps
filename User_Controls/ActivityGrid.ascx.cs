﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

public partial class User_Controls_ActivityGrid : System.Web.UI.UserControl
{

    protected void Page_Load(object sender, EventArgs e)
    {
        cboColumn.OkButtonClicked += new EventHandler(cboColumn_OkButtonClicked);
        if (!IsPostBack && SelectedActivityID.Value != null)
        {
            String UID = this.UniqueID.Replace("$", "_") + "_";
            grid.Attributes.Add("OnScroll", "savescroll('" + UID + "'); return false;");

            ProjectToCustomerID.Value = Session["SelectedProjectToCustomerID"].ToString();
            DataTable tbl = ActivityDataAccess.SelectActivityViewHeader(Convert.ToInt32(ProjectToCustomerID.Value));
            DataRow r = tbl.Rows[0];

            ProjectID.Value = r["ProjectID"].ToString();
            CustomerID.Value = r["CustomerID"].ToString();
            lblProjectYear.Text = r["ProjectYear"].ToString();
            BudgetResponsibilityAreaID.Value = r["BudgetResponsibilityAreaID"].ToString();
            cboProject.SelectedValue = ProjectID.Value;
            cboCustomer.SelectedValue = CustomerID.Value;
            SpendTypeID.Value = "0";

            FillCustomersForSelectedProject();

            cboCustomer.SelectedValue = r["CustomerID"].ToString();

            FillProject();
            FillSpendType();
            FillColumns();
            FillActivityType(1);

            if (Session["SelectedActivityIndex"] == null)
            {
                SelectedActivityID.Value = "0";
            }
            else
            {
                SelectedActivityID.Value = Session["SelectedActivityIndex"].ToString();
            }

            PopulateGrid();
            //SetScroll();
        }
    }



    protected void LinkDownload_click(object sender, CommandEventArgs e)
    {
        //Response.Write(e.CommandArgument);
        Download(e.CommandArgument.ToString());
    }

    private void Download(string Name)
    {
        //string strFilename = FileUpload1.FileName;
        string strFilePath = Server.MapPath("Uploads/" + Name);
        //  FileUpload1.SaveAs(strFilePath);
        Response.Clear();
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + Name);
        Response.WriteFile(strFilePath);
        Response.Flush();
        Response.End();
    }

    public void SetScroll()
    {
        String UID = this.UniqueID.Replace("$", "_") + "_";
        Page page = HttpContext.Current.CurrentHandler as Page;
        page.ClientScript.RegisterStartupScript(typeof(Page), "setscroll", "<script type='text/javascript'>setscroll('" + UID + "');</script>");
    }
    #region properties

    public string SelectedValue
    {
        get
        {
            try
            {
                grd.SelectedIndex = Convert.ToInt32(SelectedActivityID.Value);
                Session["SelectedActivityID"] = grd.SelectedValue;
                return grd.SelectedValue.ToString();
            }
            catch { return "0"; }
        }
    }

    public string SelectedProjectToCustomerID
    {
        get
        {
            try
            {
                return ProjectToCustomerID.Value;
            }
            catch { return "0"; }
        }
    }

    public string SelectedIndex
    {
        get
        {
            try
            {
                return grd.SelectedIndex.ToString();
            }
            catch { return "0"; }
        }
    }

    public string SelectedSpendTypeName
    {
        get
        {
            try
            {
                grd.SelectedIndex = Convert.ToInt32(SelectedActivityID.Value);
                string returnValue = Common.ADOLookup("SpendTypeName", "qryActivity", "ActivityID = " + grd.SelectedValue);
                return returnValue;
            }
            catch { return ""; }
        }
    }

    public string HeaderText
    {
        set { lblHeader.Text = value; }
    }

    public string IONumberValue
    {
        get { return lblIONumber.Text; }
    }

    #endregion

    #region methods

    public void SetActivityID(String strSelectedActivityID)
    {
        SelectedActivityID.Value = strSelectedActivityID;
    }

    public void RefreshGrid()
    {
        //SelectedActivityID.Value = "0";
        PopulateGrid();
        SetScroll();
        //grd.SelectedIndex = Convert.ToInt32(Session["SelectedActivityIndex"].ToString());
    }

    public void PopulateGrid()
    {
        //populate the grid 
        Session["SelectedProjectToCustomerID"] = Common.ADOLookup("ProjectToCustomerID", "tblProjectToCustomer", "ProjectID = " + ProjectID.Value + " AND CustomerID = " + CustomerID.Value);
        ProjectToCustomerID.Value = Session["SelectedProjectToCustomerID"].ToString();
        //ProjectToCustomerID.Value = "1";

        DataTable tbl = ActivityDataAccess.SelectActivityViewHeader(Convert.ToInt32(ProjectToCustomerID.Value));
        DataRow r = tbl.Rows[0];

        string strSQL = "SELECT * FROM qryActivity b WHERE ActivityID > 0 ";
        strSQL += "AND ProjectToCustomerID = " + ProjectToCustomerID.Value + " ";
        if (cboActivityType.SelectedValue != "0")
        {
            strSQL += "AND ActivityTypeID = " + cboActivityType.SelectedValue + " ";
        }
        if (cboSpendType.SelectedValue != "0")
        {
            strSQL += "AND SpendTypeID = " + SpendTypeID.Value + " ";
        }
        sds.SelectCommand = strSQL;
        grd.DataBind();

        //update left panel data
        lblBudgetResponsibilityAreaName.Text = r["BudgetResponsibilityAreaName"].ToString();
        BudgetResponsibilityAreaID.Value = r["BudgetResponsibilityAreaID"].ToString();
        ProjectID.Value = r["ProjectID"].ToString();
        CustomerID.Value = r["CustomerID"].ToString();
        lblProjectYear.Text = r["ProjectYear"].ToString();
        lblCurrencyCode.Text = r["CurrencyCode"].ToString().ToUpper().Replace("NULL", "");

        lblIONumber.Text = r["IONumber"].ToString();
        lblPulsingPlanPriorityName.Text = r["PulsingPlanPriorityName"].ToString();

        if (SpendTypeID.Value.Equals("0"))
        {
            lblBudget.Text = String.Format("{0:N0}", r["Budget"]);
            lblBudget.Style.Add("text-align", "right");
            lblBalance.Text = String.Format("{0:N0}", r["Balance"]);
            lblBalance.Style.Add("text-align", "right");
            if (Convert.ToInt32(r["Balance"]) < 0) { lblBalance.BackColor = Color.PapayaWhip; } else { lblBalance.BackColor = Color.LightGreen; }
            lblFYView.Text = String.Format("{0:N0}", r["FYView"]);
            lblFYView.Style.Add("text-align", "right");
            lblVariance.Text = String.Format("{0:N0}", r["Variance"]);
            lblVariance.Style.Add("text-align", "right");
            if (Convert.ToInt32(r["Variance"]) < 0) { lblVariance.BackColor = Color.PapayaWhip; } else { lblVariance.BackColor = Color.LightGreen; }
        }
        else if (SpendTypeID.Value.Equals("1"))
        {
            lblBudget.Text = String.Format("{0:N0}", r["AllocatedAPBudget"]);
            lblBudget.Style.Add("text-align", "right");
            lblBalance.Text = String.Format("{0:N0}", r["APBalance"]);
            lblBalance.Style.Add("text-align", "right");
            if (Convert.ToInt32(r["APBalance"]) < 0) { lblBalance.BackColor = Color.PapayaWhip; } else { lblBalance.BackColor = Color.LightGreen; }
            lblFYView.Text = String.Format("{0:N0}", r["APFYView"]);
            lblFYView.Style.Add("text-align", "right");
            lblVariance.Text = String.Format("{0:N0}", r["APVariance"]);
            lblVariance.Style.Add("text-align", "right");
            if (Convert.ToInt32(r["APVariance"]) < 0) { lblVariance.BackColor = Color.PapayaWhip; } else { lblVariance.BackColor = Color.LightGreen; }
        }
        else if (SpendTypeID.Value.Equals("2"))
        {
            lblBudget.Text = String.Format("{0:N0}", r["AllocatedTCCBudget"]);
            lblBudget.Style.Add("text-align", "right");
            lblBalance.Text = String.Format("{0:N0}", r["TCCBalance"]);
            lblBalance.Style.Add("text-align", "right");
            if (Convert.ToInt32(r["TCCBalance"]) < 0) { lblBalance.BackColor = Color.PapayaWhip; } else { lblBalance.BackColor = Color.LightGreen; }
            lblFYView.Text = String.Format("{0:N0}", r["TCCFYView"]);
            lblFYView.Style.Add("text-align", "right");
            lblVariance.Text = String.Format("{0:N0}", r["TCCVariance"]);
            lblVariance.Style.Add("text-align", "right");
            if (Convert.ToInt32(r["TCCVariance"]) < 0) { lblVariance.BackColor = Color.PapayaWhip; } else { lblVariance.BackColor = Color.LightGreen; }
        }
        else if (SpendTypeID.Value.Equals("3"))
        {
            lblBudget.Text = String.Format("{0:N0}", r["AllocatedCPNBudget"]);
            lblBudget.Style.Add("text-align", "right");
            lblBalance.Text = String.Format("{0:N0}", r["CPNBalance"]);
            lblBalance.Style.Add("text-align", "right");
            if (Convert.ToInt32(r["CPNBalance"]) < 0) { lblBalance.BackColor = Color.PapayaWhip; } else { lblBalance.BackColor = Color.LightGreen; }
            //lblFYView.Text = String.Format("{0:N0}", r["CPNFYView"]);
            lblFYView.Text = String.Format("{0:N0}", 0);
            lblFYView.Style.Add("text-align", "right");
            //lblVariance.Text = String.Format("{0:N0}", r["CPNVariance"]);
            lblVariance.Text = String.Format("{0:N0}", 0);
            lblVariance.Style.Add("text-align", "right");
            //if (Convert.ToInt32(r["CPNVariance"]) < 0) { lblVariance.BackColor = Color.PapayaWhip; } else { lblVariance.BackColor = Color.LightGreen; }
            lblVariance.BackColor = Color.LightGreen;
        }

        HideColumns();
    }
    public void HideColumns()
    {
        GetPreviousColumnsSelected();
        String[] items = null;
        if (!cboColumnHiddenField.Value.ToString().Equals(""))
            items = cboColumnHiddenField.Value.ToString().Split(',');

        foreach (DataControlField col in grd.Columns)
        {
            int removeColumn = 1;
            if (items != null)
            {
                foreach (string item in items)
                {

                    if (col.HeaderText.Contains(item))
                    {
                        removeColumn = 0;
                    }
                }
            }
            if (removeColumn == 1 && col.HeaderText != "Activity Name")
            {
                col.Visible = false;
            }
            else
            {
                col.Visible = true;
            }
        }
    }



    protected void SetColumnsSelected()
    {
        DataTable tbl = new DataTable();
        DataColumn clm;

        clm = new DataColumn("UserName");
        clm.DefaultValue = "0";
        tbl.Columns.Add(clm);

        clm = new DataColumn("SelectionName");
        clm.DefaultValue = "0";
        tbl.Columns.Add(clm);

        clm = new DataColumn("SelectionValue");
        clm.DefaultValue = "0";
        tbl.Columns.Add(clm);

        DataRow dr = tbl.NewRow();
        dr[0] = Session["CurrentUserName"].ToString();
        dr[1] = "LastActivityViewColumnsSelected";
        if (cboColumnHiddenField.Value.ToString() != "")
            dr[2] = cboColumnHiddenField.Value.ToString();
        else
            dr[2] = "Spend,Activity Type,Document,Status,Supplier,Total,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec,Files";
        //dr[2] = Request.Cookies["Column"].Value;
        //dr[2] = cboColumnHiddenField.Value.ToString();
        tbl.Rows.Add(dr);
        UserDataAccess.UpdateUserSelections(tbl);
    }

    protected void GetPreviousColumnsSelected()
    {
        DataTable tbl = UserDataAccess.SelectUserSelections(Session["CurrentUserName"].ToString());
        String columns = "-1";
        for (int i = 0; i < tbl.Rows.Count; i++)
        {
            if (tbl.Rows[i]["SelectionName"].ToString() == "LastActivityViewColumnsSelected")
            {
                columns = tbl.Rows[i]["SelectionValue"].ToString();
            }
        }
        cboColumn.SetSelectedName(columns);
        cboColumnHiddenField.Value = columns;
        cboColumnIDHiddenField.Value = cboColumn.getSelectedID();
    }

    #endregion

    #region gridevents

    /*protected void grd_SelectedIndexChanged(object sender, EventArgs e)
    {
        //store selected activity in session variable
        Session["SelectedActivityID"] = Convert.ToInt32(grd.SelectedValue);
        PopulateGrid();
    }*/

    protected void grd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
        }
        if (e.Row.RowIndex == Convert.ToInt32(SelectedActivityID.Value))
        {
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                e.Row.Cells[i].CssClass = "grid_selected";
            }
        }

        //Add forecast and activity footer as a front end action
        if (txtSearch.Text == "")
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[2].Text = "Total Forecast";
                String items = cboColumnIDHiddenField.Value.ToString();
                items = "," + items + ",";
                int intProjectToCustomerID = Convert.ToInt32(ProjectToCustomerID.Value); //move this into .master page
                int intActivityTypeID = Convert.ToInt32(cboActivityType.SelectedValue);
                int intSpendTypeID = Convert.ToInt32(cboSpendType.SelectedValue);
                DataTable tbl = ActivityDataAccess.SelectForecastTotals(intProjectToCustomerID, intActivityTypeID, intSpendTypeID);
                DataTable tbl2 = ActivityDataAccess.SelectActualTotals(intProjectToCustomerID, intSpendTypeID);

                int startColumn = 8;

                if (tbl.Rows.Count > 0)
                {
                    DataRow r = tbl.Rows[0];
                    double sum = 0;
                    for (int i = 1; i <= 9; i++)
                    {
                        if (items.Contains("," + i.ToString() + ","))
                        {
                            sum += double.Parse(r["Month0" + i].ToString());
                        }

                    }
                    for (int i = 10; i <= 12; i++)
                    {
                        if (items.Contains("," + i.ToString() + ","))
                        {
                            sum += double.Parse(r["Month" + i].ToString());
                        }
                    }

                    e.Row.Cells[startColumn].Text = String.Format("{0:N0}", int.Parse(sum.ToString()));
                    lblForecast.Text = String.Format("{0:N0}", int.Parse(sum.ToString()));
                    lblForecast.Style.Add("text-align", "right");
                    for (int i = 1; i <= 9; i++)
                    {
                        e.Row.Cells[i + startColumn].HorizontalAlign = HorizontalAlign.Right;
                        e.Row.Cells[i + startColumn].Text = String.Format("{0:N0}", r["Month0" + i]);
                    }
                    for (int i = 10; i <= 12; i++)
                    {
                        e.Row.Cells[i + startColumn].HorizontalAlign = HorizontalAlign.Right;
                        e.Row.Cells[i + startColumn].Text = String.Format("{0:N0}", r["Month" + i]);
                    }
                }
                else
                {

                    lblForecast.Text = "0";
                    lblForecast.Style.Add("text-align", "right");
                    for (int i = 0; i <= 12; i++)
                    {
                        e.Row.Cells[i + startColumn].HorizontalAlign = HorizontalAlign.Right;
                        e.Row.Cells[i + startColumn].Text = String.Format("{0:N0}", 0);
                    }
                }
                //colour code the cell 
                //if (intTotal < 0) { e.Row.Cells[12].BackColor = Color.PapayaWhip; } else { e.Row.Cells[12].BackColor = Color.LightGreen; }

                //Add Second Footer Row
                if (tbl2.Rows.Count > 0 && cboActivityType.SelectedIndex == 0) //because there may be no actuals or ActivityType has been filtered
                {
                    GridViewRow row = new GridViewRow(0, 0, DataControlRowType.Footer, DataControlRowState.Normal);
                    TableCell cell;

                    if (cboColumnHiddenField.Value.ToString().Contains("Files"))
                    {
                        cell = new TableCell();
                        cell.Text = "";
                        row.Cells.Add(cell);
                    }

                    cell = new TableCell();
                    cell.Text = "Actuals";
                    row.Cells.Add(cell);

                    

                    cell = new TableCell();
                    cell.Text = "";
                    row.Cells.Add(cell);
                    //full year view
                    for (int i = 1; i <= 5; i++)
                    {
                        cell = new TableCell();
                        if (items.Contains("," + (i + 13).ToString() + ","))
                            row.Cells.Add(cell);
                    }
                    DataRow dr = tbl2.Rows[0];
                    double sum = 0;
                    for (int i = 1; i <= 9; i++)
                    {
                        if (items.Contains("," + i.ToString() + ","))
                        {
                            sum += double.Parse(dr["Month0" + i].ToString());
                        }
                    }
                    for (int i = 10; i <= 12; i++)
                    {
                        if (items.Contains("," + i.ToString() + ","))
                        {
                            sum += double.Parse(dr["Month" + i].ToString());
                        }
                    }
                    cell = new TableCell();
                    cell.Text = String.Format("{0:N0}", double.Parse(sum.ToString()));
                    if (items.Contains(",19,"))
                        row.Cells.Add(cell);
                    for (int i = 1; i <= 9; i++)
                    {
                        cell = new TableCell();
                        cell.HorizontalAlign = HorizontalAlign.Right;
                        cell.Text = String.Format("{0:N0}", dr["Month0" + i]);
                        if (items.Contains("," + i.ToString() + ","))
                            row.Cells.Add(cell);
                    }
                    for (int i = 10; i <= 12; i++)
                    {
                        cell = new TableCell();
                        cell.HorizontalAlign = HorizontalAlign.Right;
                        cell.Text = String.Format("{0:N0}", dr["Month" + i]);
                        if (items.Contains("," + i.ToString() + ","))
                            row.Cells.Add(cell);
                    }
                    grd.Controls[0].Controls.Add(row);
                }

            }
        }
    }

    #endregion

    #region dropdowns

    protected void FillActivityType(int intMaxActiveIndicator)
    //Fill the ActivityTypeID dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillActivityType";
        cmd.Parameters.AddWithValue("@SpendTypeID", SpendTypeID.Value);
        cmd.Parameters.AddWithValue("@BudgetResponsibilityAreaID", BudgetResponsibilityAreaID.Value);
        cmd.Parameters.AddWithValue("@MaxActiveIndicator", intMaxActiveIndicator);
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboActivityType;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillSpendType()
    //populate the SpendType dropdownlist
    {
        //Get a data table object containing the values
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillSpendType";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboSpendType;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillCustomersForSelectedProject()
    //fill list of customers for currently selected project for which user has permissions
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillCustomersForSelectedProject";
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        cmd.Parameters.AddWithValue("@ProjectID", ProjectID.Value);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboCustomer;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillProject()
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillProjectsForSelectedCustomer";
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        cmd.Parameters.AddWithValue("@ProjectYear", lblProjectYear.Text);
        cmd.Parameters.AddWithValue("@CustomerID", CustomerID.Value);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboProject;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillColumns()
    {
        DataTable dt1 = new DataTable();
        dt1.Columns.Add(new DataColumn("EntityName", typeof(System.String)));
        dt1.Columns.Add(new DataColumn("ID", typeof(System.String)));
        //dt1.Rows.Add(new string[] { "Activity Name", "14" });
        dt1.Rows.Add(new string[] { "Files", "13" });
        dt1.Rows.Add(new string[] { "Spend", "15" });
        dt1.Rows.Add(new string[] { "Activity Type", "16" });
        dt1.Rows.Add(new string[] { "Document", "17" });
        dt1.Rows.Add(new string[] { "Status", "18" });
        dt1.Rows.Add(new string[] { "Supplier", "19" });
        dt1.Rows.Add(new string[] { "Total", "20" });
        dt1.Rows.Add(new string[] { "Jan", "1" });
        dt1.Rows.Add(new string[] { "Feb", "2" });
        dt1.Rows.Add(new string[] { "Mar", "3" });
        dt1.Rows.Add(new string[] { "Apr", "4" });
        dt1.Rows.Add(new string[] { "May", "5" });
        dt1.Rows.Add(new string[] { "Jun", "6" });
        dt1.Rows.Add(new string[] { "Jul", "7" });
        dt1.Rows.Add(new string[] { "Aug", "8" });
        dt1.Rows.Add(new string[] { "Sep", "9" });
        dt1.Rows.Add(new string[] { "Oct", "10" });
        dt1.Rows.Add(new string[] { "Nov", "11" });
        dt1.Rows.Add(new string[] { "Dec", "12" });
        cboColumn.AddDataToList(dt1, "EntityName", "ID");
    }

    protected void cboSpendType_SelectedIndexChanged(object sender, EventArgs e)
    {
        SpendTypeID.Value = cboSpendType.SelectedValue;
        SelectedActivityID.Value = "0";
        PopulateGrid();
    }

    protected void cboProject_SelectedIndexChanged(object sender, EventArgs e)
    {
        ProjectID.Value = cboProject.SelectedValue;

        Session["SelectedProjectToCustomerID"] = Common.ADOLookup("ProjectToCustomerID", "tblProjectToCustomer", "ProjectID = " + ProjectID.Value + " AND CustomerID = " + CustomerID.Value);
        ProjectToCustomerID.Value = Session["SelectedProjectToCustomerID"].ToString();
        SelectedActivityID.Value = "0";
        PopulateGrid();
        FillCustomersForSelectedProject();
        cboCustomer.SelectedValue = CustomerID.Value;
        FillActivityType(1);
    }

    protected void cboColumn_OkButtonClicked(object sender, EventArgs e)
    {
        cboColumnHiddenField.Value = cboColumn.getSelectedValues();
        cboColumnIDHiddenField.Value = cboColumn.getSelectedID();
        SetColumnsSelected();
        HideColumns();
        PopulateGrid();
    }

    protected void cboCustomer_TextChanged(object sender, EventArgs e)
    {
        CustomerID.Value = cboCustomer.SelectedValue;

        Session["SelectedProjectToCustomerID"] = Common.ADOLookup("ProjectToCustomerID", "tblProjectToCustomer", "ProjectID = " + ProjectID.Value + " AND CustomerID = " + CustomerID.Value);
        ProjectToCustomerID.Value = Session["SelectedProjectToCustomerID"].ToString();

        FillProject();
        DataTable tbl = ActivityDataAccess.SelectActivityViewHeader(Convert.ToInt32(ProjectToCustomerID.Value));
        DataRow r = tbl.Rows[0];
        cboProject.SelectedValue = r["ProjectID"].ToString();

        SelectedActivityID.Value = "0";
        PopulateGrid();
    }

    protected void cboActivityType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (cboActivityType.SelectedIndex != 0)
        {
            Budget.Visible = false;
            lblBudget.Visible = false;
            FYView.Visible = false;
            lblFYView.Visible = false;
            Balance.Visible = false;
            lblBalance.Visible = false;
            Forecast.Visible = false;
            lblForecast.Visible = false;
            Variance.Visible = false;
            lblVariance.Visible = false;
        }
        else
        {
            Budget.Visible = true;
            lblBudget.Visible = true;
            FYView.Visible = true;
            lblFYView.Visible = true;
            Balance.Visible = true;
            lblBalance.Visible = true;
            Forecast.Visible = true;
            lblForecast.Visible = true;
            Variance.Visible = true;
            lblVariance.Visible = true;
        }
        SelectedActivityID.Value = "0";
        PopulateGrid();
    }
    protected void txtSearch_TextChanged(object sender, EventArgs e)
    {
        if (txtSearch.Text == "")
        {
            Budget.Visible = true;
            lblBudget.Visible = true;
            FYView.Visible = true;
            lblFYView.Visible = true;
            Balance.Visible = true;
            lblBalance.Visible = true;
        }
        else
        {
            Budget.Visible = false;
            lblBudget.Visible = false;
            FYView.Visible = false;
            lblFYView.Visible = false;
            Balance.Visible = false;
            lblBalance.Visible = false;
        }
        SelectedActivityID.Value = "0";
        PopulateGrid();
    }


    protected void ImageButton1_Click(object sender, CommandEventArgs e)
    {
        grd.SelectedIndex = Int32.Parse(SelectedActivityID.Value);
        int FileCount;

        int POID = Int32.Parse(DataAccess.ADOLookup("POID", "tblActivity", "ActivityID = " + grd.SelectedValue));

        DataTable dt = new DataTable();
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "GetFileDetailsUsingPOID";
        cmd.Parameters.AddWithValue("@POID", POID);
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        dt = DataAccess.ExecuteSelectCommand(cmd);
        FileCount = dt.Rows.Count;

        if (FileCount > 1)
        {
            GridViewDocuments.DataSource = dt;
            GridViewDocuments.DataBind();
            ShowFileList.Show();
        }
        else if (FileCount == 1)
        {
            //Let user download the file
            Download(dt.Rows[0]["FileName"].ToString());

        }

        else
        {
            // lblMessage.Text = "No files available.";
        }
    }


    protected Boolean IsFilePresent(int Filecount)
    {
        return Filecount >= 1;
    }

    protected string GetImageType(int POID, int filecount)
    {
        if (filecount > 1)
            return "/Images/Type_Folder.png";
        else
        {
            //Get file name from db using ADO lookup function, passing ActivityID
                //grd.SelectedValue = ActivityID;
            try
            {
                string value = Common.ADOLookup("FileName", "tblPOIDFileDetails", "POID = " + POID);
                string extn = value.Substring(value.LastIndexOf('.') + 1);
                if (extn == "pdf")
                {
                    return "/Images/Type_pdf.png";
                }
                else if (extn == "docx" || extn == "doc")
                {
                    return "/Images/Type_doc.png";
                }
                else if (extn == "xlsx" || extn == "xls")
                {
                    return "/Images/Type_excel.png";
                }
                else if (extn == "txt")
                {
                    return "/Images/Type_txt.png";
                }
                else
                {
                    return "/Images/Type_generic.png";
                }
               // return "/Images/Type_pdf.png";
            }
            catch (Exception ex)
            {
                return "/Images/Type_generic.png";
            }
            //return "/Images/Type_generic.png";
            //Based on extension, return a file icon
        }

    }
  
}

       
                

#endregion