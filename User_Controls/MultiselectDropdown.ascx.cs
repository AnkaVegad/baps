﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class User_Controls_MultiselectDropdown : System.Web.UI.UserControl
{
    public bool AutoPostBack = true;

    private String strSelectedList;
    private String strSelectedID;

    public String positions;
    public int selectedCount;
    public event EventHandler OkButtonClicked;

    public int Width = 150;

    protected void Page_Load(object sender, EventArgs e)
    {
        MainContainer.Width = Width + 50;
        DisplayBox.Width = Width;

        if (!IsPostBack)
        {
            String UID = this.UniqueID.Replace("$", "_") + "_";
            DropdownImage.Attributes.Add("OnClick", "retainPreviousSelections('" + UID + "', '" + Count() + "');ToggleDropdown('" + UID + "'); return false;");
            SelectAll.Attributes.Add("OnClick", "ToggleCheck('" + UID + "', '"+ Count() +"'); return true;");
            CancelButton.Attributes.Add("OnClick", "retainPreviousSelections('" + UID + "', '" + Count() + "'); CloseDropdown('" + UID + "'); return false;");
            DropdownCheckList.Attributes.Add("OnClick", "ValidateSelectAll('" + UID + "', '" + Count() + "'); return true;");
            DisplayBox.Attributes.Add("OnClick", "OpenDropdown('" + UID + "'); return false;");
            //PanelContainer.Attributes.Add("onfocusout", "FocusLost('" + UID + "', '" + Count() + "'); return false;");
        }
    }

    public void AddDataToList(DataTable tbl, String DataTextField, String DataValueField)
    {
        DropdownCheckList.DataSource = tbl;
        DropdownCheckList.DataTextField = DataTextField;
        DropdownCheckList.DataValueField = DataValueField;
        DropdownCheckList.DataBind();
    }

    private void CheckMultipleValues(string SelectedID)
    {
        string[] items = SelectedID.Split(',');
        int count = 0;
        foreach (string element in items)
        {
            ListItem item = DropdownCheckList.Items.FindByValue(element);
            if (item != null)
            {
                count++;
                item.Selected = true;
            }
        }
        if (count == 0)
        {
            SelectAllValues();
        }
    }

    private void CheckMultipleNames(string SelectedNames)
    {
        string[] items = SelectedNames.Split(',');
        int count = 0;
        foreach (string element in items)
        {
            ListItem item = DropdownCheckList.Items.FindByText(element);
            if (item != null)
            {
                count++;
                item.Selected = true;
            }
        }
        if (count == 0)
        {
            SelectAllValues();
        }
    }

    private void SelectAllValues()
    {
        SelectAll.Checked = true;
        foreach (ListItem item in DropdownCheckList.Items)
        {
            item.Selected = true;
        }
    }

    public void DeselectAllValues()
    {
        SelectAll.Checked = false;
        foreach (ListItem item in DropdownCheckList.Items)
        {
            item.Selected = false;
        }
        DisplayBox.Text = "";
    }

    public void SetSelectedValue(string SelectedID)
    {
        if (SelectedID == "-1")
        {
            SelectAllValues();
        }
        else
        {
            CheckMultipleValues(SelectedID);
        }
        setVariables();
    }

    public void SetSelectedName(string SelectedName)
    {
        if (SelectedName == "-1")
        {
            SelectAllValues();
        }
        else
        {
            CheckMultipleNames(SelectedName);
        }
        setVariables();
    }

    private void setVariables()
    {
        strSelectedList = "";
        strSelectedID = "";
        positions = "";

        selectedCount = 0;
        int index = 0;
        foreach (ListItem item in DropdownCheckList.Items)
        {
            if (item.Selected)
            {
                selectedCount++;
                strSelectedList = strSelectedList + item.Text + ",";
                strSelectedID = strSelectedID + item.Value + ",";
                positions = positions + index + ",";
            }
            index++;
        }
        if (selectedCount > 0)
        {
            strSelectedList = strSelectedList.Substring(0, strSelectedList.Length - 1);
            strSelectedID = strSelectedID.Substring(0, strSelectedID.Length - 1);
            positions = positions.Substring(0, positions.Length - 1);
        }
        if (selectedCount == index){
            DisplayBox.Text = "<All>";
            SelectAll.Checked = true;
        }
        else if (selectedCount > 1)
            DisplayBox.Text = "<Multi>";
        else
            DisplayBox.Text = strSelectedList;
        HiddenValue.Value = positions;
    }

    public bool CheckContinuous()
    {
        int hasGap = 0;
        int hasOneTrue = 0;
        foreach (ListItem item in DropdownCheckList.Items)
        {
            if (item.Selected == true && hasOneTrue == 0)
            {
                hasOneTrue = 1;
            }

            if(item.Selected==false && hasOneTrue == 1)
            {
                hasGap = 1;
            }

            if (item.Selected == true && hasGap == 1)
            {
                return false;
            }
        }
        return true;
    }

    public String getSelectedValues()
    {
        return strSelectedList;
    }

    public String getSelectedValues(string selectedID)
    {
        string tempSelectedList = "";
        string[] items = selectedID.Split(',');
        foreach (ListItem item in DropdownCheckList.Items)
        {
            foreach(string selected in items)
            {
                if (item.Value == selected)
                {
                    tempSelectedList = tempSelectedList + item.Text + ", ";
                }
            }
        }
        if (tempSelectedList.Length > 1)
            tempSelectedList = tempSelectedList.Substring(0, tempSelectedList.Length - 2);
        return tempSelectedList;
    }

    public String getSelectedID()
    {
        return strSelectedID;
    }

    public String getSelectedPositions()
    {
        return positions;
    }

    public int getSelectedCount()
    {
        int selectedCount = 0;
        foreach (ListItem item in DropdownCheckList.Items)
        {
            if (item.Selected == true)
            {
                selectedCount++;
            }
        }
        return selectedCount;
    }

    public int Count()
    {
        return DropdownCheckList.Items.Count;
    }

    protected void OkButton_Click(object sender, EventArgs e)
    {
        setVariables();
        if(AutoPostBack==true)
        {
            OkButtonClicked(sender, e);
        }
    }
}