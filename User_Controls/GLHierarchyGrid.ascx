﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GLHierarchyGrid.ascx.cs" Inherits="User_Controls_GLHierarchyGrid" %>
<!--Warning - page has been rebuilt for Axiom - do not copy to Prod-->
<div id="parent">
    <!--Header and Filter Criteria-->
    <div id="resizable1" class="resizable ui-widget-content">
        <br />
        <asp:Label ID="lblHeader" Text="GL Hierarchy" runat="server" CssClass="page_header" />
        <br />

    </div>

    <!--Search and Grid-->
    <div id="resizable2" class="ui-widget-content">
        <br />
                    <asp:Label ID="lblSearch" Text="Search:" runat="server" Visible="false"></asp:Label>
                    <asp:TextBox ID="txtSearch" Runat="server" Width="120px" Visible="false"/>
                    <asp:Button ID="cmdGo" Text="Go" Runat="server" CausesValidation="false" Visible="false" />

        <!--Data Source - required to take advantage of sorting and paging functionality of the GridView-->
        <asp:SqlDataSource ID="sds" runat="server" 
            ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
            FilterExpression="CostElementName Like '%{0}%'" >
            <FilterParameters>
                <asp:ControlParameter ControlID="txtSearch" PropertyName="Text" />
            </FilterParameters>
        </asp:SqlDataSource>

        <!--GLHierarchy Grid-->
        <div id="project_grid">
            <asp:GridView ID="grd" runat="server" width="100%"
                AutoGenerateColumns="False" GridLines="Vertical"
                RowStyle-Wrap="false" EmptyDataText="No Records Found" 
                HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
                SelectedRowStyle-Wrap="false"
                AllowSorting="True" onsorted="grd_Sorted" onpageindexchanged="grd_PageIndexChanged"
                onselectedindexchanged="grd_SelectedIndexChanged" 
                OnRowDataBound="grd_RowDataBound" 
                DataKeyNames="GLHierarchyID" ShowFooter="False" PageSize="25">
                <Columns>
                    <asp:BoundField DataField="GLHierarchyID" ReadOnly="True" Visible="false" />
                    <asp:BoundField DataField="GLCode" HeaderText="GL Code" SortExpression="GLCode" />
                    <asp:BoundField DataField="CostElementName" HeaderText="Cost Element" SortExpression="CostElementName" />
                    <asp:BoundField DataField="GLLevel1Name" HeaderText="Level 1" SortExpression="GLLevel1Name" />
                    <asp:BoundField DataField="GLLevel2Name" HeaderText="Level 2" SortExpression="GLLevel2Name" />
                    <asp:BoundField DataField="GLLevel3Name" HeaderText="Level 3" SortExpression="GLLevel3Name" />
                    <asp:BoundField DataField="ActiveIndicatorName" HeaderText="Active" SortExpression="ActiveIndicatorName" />
                </Columns>
                <RowStyle CssClass="grid_row" Wrap="False" />
                <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
                <HeaderStyle CssClass="grid_header" Wrap="False" />
                <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
            </asp:GridView>
        </div>
    </div>
</div>
