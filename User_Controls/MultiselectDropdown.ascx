﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultiselectDropdown.ascx.cs" Inherits="User_Controls_MultiselectDropdown" %>

<script type="text/javascript" >

    function OpenDropdown(ctlID) {
        var checkList = document.getElementById(ctlID + 'dropdown');
        checkList.className = "show_multiselect";
        return false;
    }

    function CloseDropdown(ctlID) {
        var checkList = document.getElementById(ctlID + 'dropdown');
        checkList.className = "hidden_multiselect";
        return false;
    }

    function SetSameTab() {
        document.forms[0].target = "";
    }

    //function FocusLost(ctlID, cnt) {
    //    var checkList = document.getElementById(ctlID + 'DropdownCheckList');
    //    var selectAll = document.getElementById(ctlID + 'SelectAll');
    //    var OkButton = document.getElementById(ctlID + 'OkButton');
    //    var CancelButton = document.getElementById(ctlID + 'CancelButton');
    //    var DropdownImage = document.getElementById(ctlID + 'DropdownImage');
    //    var DisplayBox = document.getElementById(ctlID + 'DisplayBox');
    //    var dropdown = document.getElementById(ctlID + 'dropdown');
    //    var listitems = document.getElementById(ctlID + 'ListItems');
    //    var active = document.activeElement;

    //    var stillActive=0;
    //    if (active != checkList && active != selectAll && active != OkButton && active != CancelButton
    //        && active != DropdownImage && active != DisplayBox && active != dropdown && active!=listitems) {
            
    //        for (var i = 0; i < cnt; i++) {
    //            var items = document.getElementById(ctlID + 'DropdownCheckList_'+i);
    //            if (active == items)
    //                stillActive = 1;
    //        }
    //        if (stillActive == 0) {
    //            alert(active.id);
    //        }
    //    }
    //    var list = document.getElementById(ctlID + 'PanelContainer').getElementsByTagName('*');
    //    for (var iter; i < list.length;iter++)
    //        alert(document.getElementById(ctlID + 'PanelContainer_'+list[i]));
            
    //}

    function ToggleDropdown(ctlID)
    {
        var checkList = document.getElementById(ctlID + 'dropdown');
        if (checkList.className == "hidden_multiselect") {
            OpenDropdown(ctlID);
        }
        else {
            CloseDropdown(ctlID);
        }
        return false;
    }

    function retainPreviousSelections(ctlID, cnt)
    {
        var checkList = document.getElementById(ctlID + 'dropdown');
        var hiddenValue = document.getElementById(ctlID + 'HiddenValue');
        if (hiddenValue.value != "") {
            var IDS = hiddenValue.value.split(",");
            var checkList = document.getElementById(ctlID + 'DropdownCheckList');
            for (var i = 0; i < cnt; i++) {
                var checkBox = document.getElementById(ctlID + 'DropdownCheckList_' + i);
                checkBox.checked = false;
            }
            for (var i = 0; i < IDS.length; i++) {
                var checkBox = document.getElementById(ctlID + 'DropdownCheckList_' + IDS[i]);
                checkBox.checked = true;
            }

            var selectAllCheck = document.getElementById(ctlID + 'SelectAll');
            if (cnt == IDS.length) {
                selectAllCheck.checked = true;
            }
            else {
                selectAllCheck.checked = false;
            }
        }
        return true;
    }

    function ToggleCheck(ctlID, cnt) 
    {
        var selectall = document.getElementById(ctlID + 'SelectAll');
        var checkList = document.getElementById(ctlID + 'DropdownCheckList');
        if(selectall.checked)
        {
            for (var i = 0; i < cnt; i++) 
            {
                var checkBox = document.getElementById(ctlID + 'DropdownCheckList_' + i);
                checkBox.checked=true;
            }
        }
        else
        {
            for (var i = 0; i < cnt; i++) 
            {
                var checkBox = document.getElementById(ctlID + 'DropdownCheckList_' + i);
                checkBox.checked=false;
            }
        }
        return true;
    }

    function ValidateSelectAll(ctlID, cnt)
    {
        var selectall = document.getElementById(ctlID + 'SelectAll');
        var checkList = document.getElementById(ctlID + 'DropdownCheckList');
        var checkcount = 0;
        for (var i = 0; i < cnt; i++) {
            var checkBox = document.getElementById(ctlID + 'DropdownCheckList_' + i);
            if (checkBox.checked == true)
            {
                checkcount++;
            }
        }
        if (checkcount == cnt) {
            selectall.checked = true;
        }
        else {
            selectall.checked = false;
        }
        return true;
    }
    </script>

<asp:Panel ID="MainContainer" runat="server" height="20px" Width="200px">
<asp:TextBox ID="DisplayBox" runat="server" ReadOnly="true" Height="15px" BorderStyle="Solid" BorderWidth="1px" Width ="180px" BorderColor="gray" ForeColor="Black" style="position:relative;"></asp:TextBox>
<asp:ImageButton ID="DropdownImage" runat="server" Height="13px" ImageUrl="~/Images/DDImage_IE8.jpg" Width="16px" style="position:relative;left:-21px;"/>
    <asp:Panel class="hidden_multiselect" id="dropdown" style="z-index: 9999; position: relative; height: 200px; background:white;" 
        runat="server" BorderStyle="Groove">
        <asp:HiddenField ID="HiddenValue" runat="server"/>
        <asp:CheckBox ID="SelectAll" Text="Select All" runat="server" ForeColor="Black"/>
        <asp:Panel ID="ListItems" Style="overflow-y: auto; height:72%;" runat="server">
            <asp:CheckBoxList ID="DropdownCheckList" runat="server" Height="100%" ForeColor="Black">
            </asp:CheckBoxList>
        </asp:Panel>
        <asp:Panel ID="ButtonContainer" runat="server" style="position:relative; top:10px">
            <asp:Button ID="OkButton" Text="OK" runat="server" Width="40%" OnClick="OkButton_Click" OnClientClick="SetSameTab();" CausesValidation="false"/>
            <asp:Button ID="CancelButton" Text="Cancel" runat="server" Width="40%"/>
        </asp:Panel>
    </asp:Panel>
</asp:Panel>