﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProjectEdit_rs.ascx.cs" Inherits="User_Controls_ProjectEdit_rs" %>

<%@ Register src="~/User_Controls/MultiselectDropdown.ascx" tagname="dropdown" tagprefix="uc2" %>

<script>
    function calcProportion() {
        document.all.ctl00$ContentPlaceHolder1$ProjectEdit1$txtProportion.value = 100 - document.all.ctl00$ContentPlaceHolder1$ProjectEdit1$txtTotalProportion.value;
    }
</script>
 
        <!--Project Edit Area-->
        <table style="width:1080px"><tr><td style="vertical-align:top">
            <table border="0">
                <tr><td style="vertical-align:top">Project Name</td><td>
                    <asp:TextBox ID="ProjectID" runat="server" Visible="False"></asp:TextBox>
                    <asp:TextBox ID="ProjectName" runat="server" Width="280px" MaxLength="40"  ForeColor="#000000"
                        Enabled="False"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ErrorMessage="*Required" ControlToValidate="ProjectName" 
                        Display="Dynamic" ValidationGroup="Project"></asp:RequiredFieldValidator>
                </td></tr>
                <tr><td>Description</td><td>
                    <asp:TextBox ID="ProjectDescription" runat="server" Width="280px" Enabled="False" MaxLength="255" TextMode="MultiLine" Height="40px" ForeColor="#000000"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ErrorMessage="*Required" ControlToValidate="ProjectDescription" 
                        Display="Dynamic" ValidationGroup="Project"></asp:RequiredFieldValidator>
                </td></tr>
                <tr><td>Objective</td><td>
                    <asp:TextBox ID="ProjectObjective" runat="server" Width="280px" Enabled="False" MaxLength="255" TextMode="MultiLine" Height="40px" ForeColor="#000000"></asp:TextBox>
                </td></tr>
                <tr><td>Pulsing Plan Timing</td><td>
                    <asp:TextBox ID="PulsingPlanTiming" runat="server" Width="280px" Enabled="False" MaxLength="255"></asp:TextBox>
                    <asp:Panel ID="uc_PulsingPlanTiming" runat="server">
                        <uc2:dropdown ID="cboPulsingPlanTiming" runat="server"></uc2:dropdown>
                        <br /><asp:Label ID="lblPulsingPlanError" runat="server" ForeColor ="Red"/>
                    </asp:Panel>
                    
                </td></tr>
                <tr><td>KPIs</td><td>
                    <asp:TextBox ID="KPIs" runat="server" Width="280px" Enabled="False" MaxLength="255" TextMode="MultiLine" Height="40px" ForeColor="#000000"></asp:TextBox>
                </td></tr>
                <tr><td>Responsibility Area</td><td>
                    <asp:DropDownList ID="BudgetResponsibilityAreaID" runat="server" Width="170px" Enabled="False" DataTextField="EntityName" DataValueField="ID" ForeColor="#000000" />
                    <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="*Required" 
                        MinimumValue="1" MaximumValue="1000000" ControlToValidate="BudgetResponsibilityAreaID" Display="Dynamic" Type="Integer" validationgroup="Project"/>
                </td></tr>
                <tr><td>Year</td><td>
                    <asp:DropDownList ID="ProjectYear" runat="server" Width="100px" Enabled="False" DataTextField="EntityName" DataValueField="ID" ForeColor="#000000" />
                    <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="*Required" 
                        MinimumValue="2000" MaximumValue="2050" ControlToValidate="ProjectYear" Display="Dynamic" validationgroup="Project" />
                </td></tr>
                <tr><td>Pulsing Plan Priority</td><td>
                    <asp:DropDownList ID="PulsingPlanPriorityID" runat="server" Width="170px" Enabled="False" DataTextField="EntityName" DataValueField="ID" ForeColor="#000000" />
                    <asp:RangeValidator ID="RangeValidator4" runat="server" ErrorMessage="*Required" validationgroup="Project"
                        MinimumValue="1" MaximumValue="1000000" ControlToValidate="PulsingPlanPriorityID" Display="Dynamic" Type="Integer" />
                </td></tr>
                <tr><td>Project Owner</td><td>
                    <asp:TextBox ID="ProjectOwner" runat="server" Width="165px" Enabled="False" MaxLength="50" ForeColor="#000000"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                        ErrorMessage="*Required" ControlToValidate="ProjectOwner" 
                        Display="Dynamic" ValidationGroup="Project"></asp:RequiredFieldValidator>
                </td></tr>
                <tr><td>Created By</td><td>
                    <asp:TextBox ID="CreatedBy" runat="server" Width="165px" enabled="false" />
                </td></tr>                
                <tr><td>Created Date</td><td>
                    <asp:TextBox ID="CreatedDate" runat="server" Width="165px" enabled="false" />
                </td></tr>                
                <tr><td>Last Updated By</td><td>
                    <asp:TextBox ID="LastUpdatedBy" runat="server" Width="165px" enabled="false" />
                </td></tr>                
                <tr><td>Last Updated Date</td><td>
                    <asp:TextBox ID="LastUpdatedDate" runat="server" Width="165px" enabled="false" />
                </td></tr>  
            </table>
        </td>
        <td style="vertical-align:top">
            <table border="0">
                <!--Brand-Market entry area-->
                <tr><td colspan="2">Allocation To Brand-Markets</td></tr>
                <tr><td colspan="2">
                    <asp:GridView ID="grdSubBrandID" runat="server" width="280px"
                        AutoGenerateColumns="False" GridLines="Vertical" RowStyle-Wrap="false" EmptyDataText="[Not Allocated]" 
                        HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false" SelectedRowStyle-Wrap="false"
                        OnRowDataBound="grdSubBrandID_RowDataBound" DataKeyNames="SubBrandID" ShowFooter="True">
                        <Columns>
                            <asp:BoundField DataField="SubBrandID" ReadOnly="True" Visible="false" />
                            <asp:BoundField DataField="SubBrandName" HeaderText="Brand-Market" SortExpression="SubBrandName" />
                            <asp:BoundField DataField="PropnPerCent" HeaderText="(%)" SortExpression="PropnPerCent" DataFormatString="{0:0}" />
                        </Columns>
                        <RowStyle CssClass="grid_alternating_row" Wrap="False" />
                        <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
                        <HeaderStyle CssClass="grid_header" Wrap="False" />
                    </asp:GridView>
                </td></tr><tr><td colspan="2" style="font-size:2px">&nbsp;</td></tr><tr><td>
                    <asp:Button ID="cmdRemoveSubBrand" runat="server" Text="Remove" causesvalidation="false"
                        onclick="cmdRemoveSubBrand_Click" CssClass="form_button" Enabled="false" />
                    <asp:TextBox ID="txtTotalProportion" runat="server" Width="30px" Visible="true" 
                        CSSClass="hidden_text" Enabled="false" />
                    <asp:RangeValidator ID="RangeValidator7" runat="server" ErrorMessage="*Proportions must add up to 100"
                        validationgroup="Project"
                        controltovalidate="txtTotalProportion" MinimumValue="100" MaximumValue="100" Display="Dynamic" Type="Integer" />
                </td></tr>
                
                <tr><td colspan="2">
                    <asp:DropDownList ID="cboAddSubBrandID" runat="server" Width="178px" Enabled="false"
                        DataTextField="EntityName" DataValueField="ID" ForeColor="#000000" />
                    <asp:RangeValidator ID="RangeValidator6" ControlToValidate="cboAddSubBrandID" MaximumValue="10000000" MinimumValue="1"
                        runat="server" ErrorMessage="Required" Type="Integer" Display="Dynamic" ValidationGroup="SubBrand" />
                    <asp:TextBox ID="txtProportion" runat="server" Width="30px" enabled="false" ForeColor="#000000"/>%
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtProportion" 
                        ErrorMessage="*Required" Display="Dynamic" ValidationGroup="SubBrand" />
                    <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="txtProportion" 
                        MaximumValue="100" MinimumValue="1"
                        ErrorMessage="*Must be between 10 and 100" Type="Double" Display="Dynamic" ValidationGroup="SubBrand" />
                    <asp:Button ID="cmdAddSubBrand" runat="server" Text="Add" causesvalidation="true"
                        onclick="cmdAddSubBrand_Click" CssClass="form_button" Enabled="false" Width="50px" ValidationGroup="SubBrand" />
                </td></tr>
            </table>
        </td>
        <td valign="top">
            <table border="0">
                <!--Customer entry area-->
                <tr><td colspan="2">Allocation To Customers</td></tr>
                <tr><td colspan="2">
                    <asp:GridView ID="grdCustomerID" runat="server" width="350px"
                        AutoGenerateColumns="False" GridLines="Vertical" RowStyle-Wrap="false" EmptyDataText="[Not Allocated]" 
                        HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false" SelectedRowStyle-Wrap="false"
                        OnRowDataBound="grdCustomerID_RowDataBound" DataKeyNames="ID" ShowFooter="true">
                        <Columns>
                            <asp:BoundField DataField="ID" ReadOnly="True" Visible="false" />
                            <asp:BoundField DataField="EntityName" HeaderText="Customer" />
                            <asp:BoundField DataField="IONumber" HeaderText="IO" />
                            <asp:BoundField DataField="AllocatedAPBudget" HeaderText="A&P Budget" DataFormatString="{0:0}" />
                            <asp:BoundField DataField="AllocatedTCCBudget" HeaderText="TCC Budget" DataFormatString="{0:0}" />
                        </Columns>
                        <RowStyle CssClass="grid_alternating_row" Wrap="False" />
                        <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
                        <HeaderStyle CssClass="grid_header" Wrap="False" />
                    </asp:GridView>
                </td></tr>
                <tr><td colspan="2" style="font-size:2px">&nbsp;</td></tr><tr><td>
                    <asp:Button ID="cmdRemoveCustomer" runat="server" Text="Remove" causesvalidation="false"
                        onclick="cmdRemoveCustomer_Click" CssClass="form_button" Enabled="False" />
                </td></tr>
                <tr><td colspan="2">
                    <asp:DropDownList ID="cboAddCustomerID" runat="server" Width="158px" Enabled="false" DataTextField="EntityName" DataValueField="ID" ForeColor="#000000" />
                    <asp:RangeValidator ID="RangeValidator8" ControlToValidate="cboAddCustomerID" MaximumValue="10000000" MinimumValue="1"
                        runat="server" ErrorMessage="Required" Type="Integer" Display="Dynamic" ValidationGroup="Customer" />&nbsp;IO
                    <asp:TextBox ID="txtIONumber" runat="server" Width="90px" enabled="false" maxlength="12" ForeColor="#000000"/>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtIONumber" 
                        ErrorMessage="*Required" Display="Dynamic" ValidationGroup="Customer" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtIONumber"
                        Display="Dynamic" ErrorMessage="A 12 digit IO Number must be entered" ValidationExpression="^.{12,12}$" ValidationGroup="Customer" />
                    <asp:Button ID="cmdAddCustomer" runat="server" Text="Add" causesvalidation="true"
                        onclick="cmdAddCustomer_Click" CssClass="form_button" Enabled="False" Width="50px" ValidationGroup="Customer" />
                </td></tr>
            </table>
        </td></tr></table>
