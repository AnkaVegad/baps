﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Controls_RiskTrackerGrid : System.Web.UI.UserControl
{
    int intTotal = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //populate selection dropdowns
            FillCustomer();
            FillYear();
            FillMonth();
            //FillBrand();
            //FillCategory();
            FillSpendType();
            FillBudgetResponsibilityArea();

            //get current year from session variable set in global.asax
            cboYear.SelectedValue = Session["SelectedYear"].ToString();

            DataTable tbl = UserDataAccess.SelectUser(Session["CurrentUserName"].ToString());
            //set area 
            if (Session["SelectedBudgetResponsibilityAreaID"] == null)
            {
                if (tbl.Rows[0]["LastBudgetResponsibilityAreaID"] == DBNull.Value)
                //allows for user's first login when table entry will be blank
                {
                    Session["SelectedBudgetResponsibilityAreaID"] = Common.ADOLookup("BudgetResponsibilityAreaID", "qryUserAuthorisations", "UserName = '" + Session["CurrentUserName"].ToString().Replace("'", "''") + "'");
                }
                else
                {
                    Session["SelectedBudgetResponsibilityAreaID"] = tbl.Rows[0]["LastBudgetResponsibilityAreaID"].ToString();
                }
            }
            cboBudgetResponsibilityArea.SelectedValue = Session["SelectedBudgetResponsibilityAreaID"].ToString();

            if (Session["SelectedCustomerID"] == null)
            {
                Session["SelectedCustomerID"] = tbl.Rows[0]["LastCustomerID"].ToString();
            }
            cboCustomer.SelectedValue = Session["SelectedCustomerID"].ToString();
        }        
        //populate the RiskTracker grid 
        string strSQL;
        strSQL = "SELECT * FROM qryRiskTracker ";
        //strSQL += "ORDER BY BusinessGroupName, BusinessUnitName, EuropeanCategoryName, UKCategoryName, BrandName, IHOOHName, SubBrandName;";
        strSQL += "WHERE ProjectYear = '" + cboYear.SelectedValue + "' ";
        if (cboMonth.SelectedValue != "0")
        {
            strSQL += "AND ProjectMonth = '" + cboMonth.SelectedValue + "' ";
        }
        if (cboCustomer.SelectedValue != "0")
        {
            strSQL += "AND CustomerID = " + cboCustomer.SelectedValue + " ";
        }
        if (cboSpendType.SelectedValue != "0")
        {
            strSQL += "AND SpendTypeID = " + cboSpendType.SelectedValue + " ";
        }
        if (cboBudgetResponsibilityArea.SelectedValue != "0")
        {
            strSQL += "AND BudgetResponsibilityAreaID = " + cboBudgetResponsibilityArea.SelectedValue + " ";
        } 
        sds.SelectCommand = strSQL;
        grd.DataSourceID = "sds";
        grd.DataBind();
        //select first row
        if (!IsPostBack)
        {
            grd.SelectedIndex = 0;
        }
        else
        {
            intTotal = 0;
        }
    }

#region properties

    public string SelectedValue
    {
        get { 
            try
            {
                return grd.SelectedValue.ToString(); 
            }
            catch { return "0"; }
        }
    }

    public string HeaderText
    {
        set { lblHeader.Text = value; }
    }

#endregion

#region methods

    public void RefreshGrid()
    {
        grd.DataBind();
        grd.SelectedIndex = 0;
    }

    public void RefreshData()
    {
    }

#endregion
    
#region gridevents

    protected void grd_SelectedIndexChanged(object sender, EventArgs e)
    {
        intTotal = 0;
    }

    protected void grd_PageIndexChanged(object sender, EventArgs e)
    {
        grd.SelectedIndex = 0;
        intTotal = 0;
    }

    protected void grd_Sorted(object sender, EventArgs e)
    {
        grd.SelectedIndex = 0;
    }

    protected void grd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            int intCurrent = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "FinancialImpact"));
            //increment the totals
            intTotal += intCurrent;
            //highlight on hover
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            e.Row.Attributes["onclick"] = cs.GetPostBackClientHyperlink(this.grd, "Select$" + e.Row.RowIndex);
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[2].Text = "Totals";
            //display the totals
            e.Row.Cells[7].Text = intTotal.ToString();
        }
    }


#endregion

#region dropdowns

    protected void FillBudgetResponsibilityArea()
    //fill BudgetResponsibilityArea dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillBudgetResponsibilityArea";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        //cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboBudgetResponsibilityArea;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillCustomer()
    //fill customer dropdownlist for current user
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillCustomer";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        //cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        cmd.Parameters.AddWithValue("@IncludeInactive", 1);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboCustomer;
        d.DataSource = tbl;
        d.DataBind();
    }

    //protected void FillBrand()
    ////fill brand dropdownlist
    //{
    //    SqlCommand cmd = DataAccess.CreateCommand();
    //    cmd.CommandType = CommandType.StoredProcedure;
    //    cmd.CommandText = "sp_ap_FillBrandActive";
    //    cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
    //    //cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
    //    cmd.Parameters.AddWithValue("@IncludeInactive", 0);
    //    DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
    //    DropDownList d = cboBrand;
    //    d.DataSource = tbl;
    //    d.DataBind();
    //}

    protected void FillYear()
    //fill year dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillYear";
        cmd.Parameters.AddWithValue("@ZeroValueText", "[None]");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboYear;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillMonth()
    //fill months dropdownlist
    {
        //Get a data table object containing the values
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillMonth";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboMonth;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillSpendType()
    //populate the cboSpendType Filter dropdown
    {
        cboSpendType.Items.Add(new ListItem("<All>", "0"));
        cboSpendType.Items.Add(new ListItem("A&P", "1"));
        cboSpendType.Items.Add(new ListItem("TCC", "2"));
        cboSpendType.Items.Add(new ListItem("Coupons", "3"));
    }

    protected void cboBudgetResponsibilityArea_TextChanged(object sender, EventArgs e)
    {
        Session["SelectedBudgetResponsibilityAreaID"] = cboBudgetResponsibilityArea.SelectedValue;
        //UpdateUserSelections();
        //SelectFirstRow();
    }

    protected void cboCustomer_TextChanged(object sender, EventArgs e)
    {
        Session["SelectedCustomerID"] = cboCustomer.SelectedValue;
        //UpdateUserSelections();
        //SelectFirstRow();
    }

    protected void cboYear_TextChanged(object sender, EventArgs e)
    {
        Session["SelectedYear"] = cboYear.SelectedValue;
        //SelectFirstRow();
    }

#endregion
}
