﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddBudgetLevel1.ascx.cs" Inherits="User_Controls_AddBudgetLevel1" %>


<!--Header and Filter Criteria-->
<div id="parent">
    <!--Header and Filter Criteria-->
 
        <asp:Label ID="lblHeaderText" Text="New Budgets" runat="server" CssClass="page_header"></asp:Label>
        <br />
        <br />
        <table border="0" style="vertical-align">
                    <tr><td>Budget Year</td><td>
                        <asp:DropDownList ID="BudgetYear" runat="server" Width="185px"  DataTextField="EntityName" 
                            DataValueField="ID" />
                    </td></tr>  
                    <tr><td>Spend Type</td><td>
                        <asp:DropDownList ID="SpendType" runat="server" Width="185px"  DataTextField="EntityName" 
                            DataValueField="ID" />
                    </td></tr>  
                   <tr>
                       
                    

                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td>GL Level1</td>
                                    <td><asp:DropDownList ID="GLLevel1" runat="server" AutoPostBack = "true" OnSelectedIndexChanged = "GLLevel1_Changed">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>GL Level2</td>
                                    <td>
                                        <asp:DropDownList ID="GLLevel2" runat="server" AutoPostBack = "true" >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    </tr>
                    <tr><td>Central Budget Indicator</td><td>
                        <asp:DropDownList ID="CentralBudgetIndicator" runat="server" Width="185px"  DataTextField="EntityName"
                            DataValueField ="ID" />
                    <tr><td>Department Code</td><td>
                        <asp:DropDownList ID="DepartmentCode" runat="server" Width="185px"  DataTextField="EntityName"
                            DataValueField ="ID" />
                    </td></tr>
                     <tr><td>Budget Value</td><td>
                        <asp:TextBox ID="BudgetValue" runat="server" Width="175px"  DataTextField="EntityName" />
                        <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="*Required" display="Dynamic"
                            ControlToValidate="BudgetValue" MaximumValue="999999999" MinimumValue="0" Type="Integer" />
                    </td></tr>
           </table>
                    <asp:Button ID="cmdAddBudget" runat="server" Text="Add" causesvalidation="true"
                            onclick="cmdAddBudget_Click" CssClass="form_button" Enabled="false" Width="60px" />

 </div>
