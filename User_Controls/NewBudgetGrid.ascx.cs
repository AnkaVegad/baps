﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Controls_NewBudgetGrid : System.Web.UI.UserControl
{

    String Conn = APConfiguration.DbConnectionString;
 
    //String strBRAText;
    String strBRACodes;
    String strDeptCode;
    //String strCBI;
    String strTCCRelevant;
    String strSpendType;

    float Target;

    //String strUyear;
    int intUBRA;
    int intUCategory;
    int intUBrand;
    int intUCustomer;

    public static String strBL;
    public static String strCBI;

    public static int intBRA = 0;
    public static int intCategory = 0;
    public static int intSpendType = 0;
    public static int intBrand = 0;
    public static int intCustomer = 0;


    public static int? intBRAID;
    public static int? intCategoryID;
    public static int? intSpendTypeID;
    public static int? intBrandID;
    public static int? intCustomerID;
    public static int  intRunTypeID;

    
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
         {
             
             FillYear();
             FillBudgetLevel();
             FillBudgetResponsibilityArea();
             FillCategory();
             FillSpendType();
             FillBrand();
             FillCustomer();
             PopulateLastValues();
             //get current year from session variable set in global.asax
             cboYear.SelectedValue = Session["SelectedYear"].ToString();

             if (strBL == "1")
             {
                 ScriptManager.RegisterStartupScript(
                 this,
                 GetType(),
                 "cboBudgetLevel",
                 "FiltersBudgetLevel1();",
                 true);
                 grdBudgetL2.Visible = false;
                 grdBudgetL3.Visible = false;
                 grdBudgetL4.Visible = false;
                 grdBudgetL1.Visible = true;
                 FillBudgetGrid();
             }
             else if (strBL == "2")
             {
                 ScriptManager.RegisterStartupScript(
                this,
                GetType(),
                "cboBudgetLevel",
                "FiltersBudgetLevel2();",
                true);
                 grdBudgetL2.Visible = true;
                 grdBudgetL3.Visible = false;
                 grdBudgetL4.Visible = false;
                 grdBudgetL1.Visible = false;
                 FillBudgetGrid2();
             }
             else if (strBL == "3")
             {
                 FillBudgetGrid3();
             }

             else if (strBL == "4")
             {
                 FillLevel4Filters();
             }
          
         }
  
    }

 #region methods

    public void FillBudgetGrid()
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillBudgetLevel1";
        cmd.Parameters.AddWithValue("@BudgetYear", cboYear.SelectedValue);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
            GridView grd = grdBudgetL1;
            grd.DataSource = tbl;
            grd.DataBind();

            foreach (GridViewRow row in this.grdBudgetL1.Rows)
            {
                row.Cells[4].BackColor = Color.FromName(row.Cells[5].Text);

            }


            grdBudgetL1.Columns[5].Visible = false;
            grdBudgetL1.Columns[6].Visible = false;
        
    }

    public void FillBudgetGrid2()
    {
        
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillBudgetLevel2";
        cmd.Parameters.AddWithValue("@BudgetYear", cboYear.SelectedValue);
        cmd.Parameters.AddWithValue("@UKCategoryID", Convert.ToInt32(cboCategory.SelectedValue));
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        GridView grd = grdBudgetL2;
        grd.DataSource = tbl;
        grd.DataBind();

        foreach (GridViewRow row in this.grdBudgetL2.Rows)
        {
            row.Cells[4].BackColor = Color.FromName(row.Cells[5].Text);

        }
        grdBudgetL2.Columns[5].Visible = false;
        grdBudgetL2.Columns[6].Visible = false;
    }

    public void FillBudgetGrid3()
    {

        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillBudgetLevel3";
        cmd.Parameters.AddWithValue("@BudgetYear", cboYear.SelectedValue);
        cmd.Parameters.AddWithValue("@UKCategoryID", Convert.ToInt32(cboCategory.SelectedValue));
        cmd.Parameters.AddWithValue("@BudgetResponsibilityAreaID", Convert.ToInt32(cboBudgetResponsibilityArea.SelectedValue));
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        GridView grd = grdBudgetL3;
        grd.DataSource = tbl;
        grd.DataBind();

        foreach (GridViewRow row in this.grdBudgetL3.Rows)
        {
            row.Cells[3].BackColor = Color.FromName(row.Cells[4].Text);

        }
        grdBudgetL3.Columns[4].Visible = false;
        grdBudgetL3.Columns[5].Visible = false;
    }

    public void FillBudgetGrid4(int? CategoryID,int? BRAID,int? SpendTypeID, int? BrandID, int? CustomerID, int RunTypeID)
    {

        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillBudgetLevel4";
        cmd.Parameters.AddWithValue("@BudgetYear", cboYear.SelectedValue);
        cmd.Parameters.AddWithValue("@UKCategoryID", CategoryID);
        cmd.Parameters.AddWithValue("@BudgetResponsibilityAreaID", BRAID);
        cmd.Parameters.AddWithValue("@SpendTypeID", SpendTypeID);
        cmd.Parameters.AddWithValue("@BrandID", BrandID);
        cmd.Parameters.AddWithValue("@CustomerID", CustomerID);
        cmd.Parameters.AddWithValue("@RunType", RunTypeID);


       foreach (SqlParameter Parameter in cmd.Parameters)
         {
            if (Parameter.Value == null)
            {
            Parameter.Value = DBNull.Value;
            }
         }

        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        GridView grd = grdBudgetL4;
        grd.DataSource = tbl;
        grd.DataBind();

        foreach (GridViewRow row in this.grdBudgetL4.Rows)
        {
            row.Cells[5].BackColor = Color.FromName(row.Cells[6].Text);

        }
        grdBudgetL4.Columns[6].Visible = false;
        grdBudgetL4.Columns[7].Visible = false;

        string strConString = APConfiguration.DbConnectionString;
        SqlConnection con = new SqlConnection(strConString);
        SqlCommand cmd2 = con.CreateCommand();
        cmd.CommandType = CommandType.Text;
        cmd2.CommandText =  "SELECT OptionValue FROM tbloption WHERE OptionName = 'UserName';";
        String contactName = DataAccess.ExecuteScalar(cmd2).ToString();
        lblContact.Text = "If your project is not shown please contact " + contactName + " to request it";
    }

 #endregion

 #region gridevents

    /** Row Editing for Budget (L1,L2,L3,L4) **/
    protected void grdBudgetL1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        
        grdBudgetL1.EditIndex = e.NewEditIndex;
        grdBudgetL1.Columns[5].Visible = true;
        grdBudgetL1.Columns[6].Visible = true;
        FillBudgetGrid();
    }

    protected void grdBudgetL2_RowEditing(object sender, GridViewEditEventArgs e)
    {

        grdBudgetL2.EditIndex = e.NewEditIndex;
        grdBudgetL2.Columns[5].Visible = true;
        grdBudgetL2.Columns[6].Visible = true;
        FillBudgetGrid2();
        ScriptManager.RegisterStartupScript(
                this,
                GetType(),
                "cboBudgetLevel",
                "FiltersBudgetLevel2();",
                true);
    }

    protected void grdBudgetL3_RowEditing(object sender, GridViewEditEventArgs e)
    {

        grdBudgetL3.EditIndex = e.NewEditIndex;
        grdBudgetL3.Columns[4].Visible = true;
        grdBudgetL3.Columns[5].Visible = true;
        FillBudgetGrid3();
        ScriptManager.RegisterStartupScript(
                this,
                GetType(),
                "cboBudgetLevel",
                "FiltersBudgetLevel3();",
                true);
    }

    protected void grdBudgetL4_RowEditing(object sender, GridViewEditEventArgs e)
    {

        grdBudgetL4.EditIndex = e.NewEditIndex;
        grdBudgetL4.Columns[6].Visible = true;
        grdBudgetL4.Columns[7].Visible = true;
        FillBudgetGrid4(intCategoryID, intBRAID, intSpendTypeID, intBrandID, intCustomerID, intRunTypeID);
        ScriptManager.RegisterStartupScript(
            this,
            GetType(),
            "cboBudgetLevel",
            "FiltersBudgetLevel4(" + intBRA + "," + intCategory + "," + intSpendType + "," + intBrand + "," + intCustomer + ");",
            true);
    }


    /** Page Index Change for Budget (L1,L2,L3,L4) **/
    protected void grdBudgetL1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdBudgetL1.PageIndex = e.NewPageIndex;
        grdBudgetL1.Columns[5].Visible = true;
        grdBudgetL1.Columns[6].Visible = true;
        FillBudgetGrid();
    }

    protected void grdBudgetL2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdBudgetL2.PageIndex = e.NewPageIndex;
        grdBudgetL2.Columns[5].Visible = true;
        grdBudgetL2.Columns[6].Visible = true;
        FillBudgetGrid2();
        ScriptManager.RegisterStartupScript(
                this,
                GetType(),
                "cboBudgetLevel",
                "FiltersBudgetLevel2();",
                true);
    }

    protected void grdBudgetL3_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdBudgetL3.PageIndex = e.NewPageIndex;
        grdBudgetL3.Columns[4].Visible = true;
        grdBudgetL3.Columns[5].Visible = true;
        FillBudgetGrid3();
        ScriptManager.RegisterStartupScript(
                this,
                GetType(),
                "cboBudgetLevel",
                "FiltersBudgetLevel3();",
                true);
    }

    protected void grdBudgetL4_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdBudgetL4.PageIndex = e.NewPageIndex;
        grdBudgetL4.Columns[6].Visible = true;
        grdBudgetL4.Columns[7].Visible = true;
        FillBudgetGrid4(intCategoryID, intBRAID, intSpendTypeID, intBrandID, intCustomerID, intRunTypeID);
        ScriptManager.RegisterStartupScript(
                    this,
                    GetType(),
                    "cboBudgetLevel",
                    "FiltersBudgetLevel4(" + intBRA + "," + intCategory + "," + intSpendType + "," + intBrand + "," + intCustomer + ");",
                    true);
    }

    /** Page Cancel Edit for Budget (L1,L2,L3,L4) **/
    protected void grdBudgetL1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

        grdBudgetL1.EditIndex = -1;
        grdBudgetL1.Columns[5].Visible = true;
        grdBudgetL1.Columns[6].Visible = true;
        FillBudgetGrid();
    }

    protected void grdBudgetL2_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

        grdBudgetL2.EditIndex = -1;
        grdBudgetL2.Columns[5].Visible = true;
        grdBudgetL2.Columns[6].Visible = true;
        FillBudgetGrid2();
        ScriptManager.RegisterStartupScript(
                this,
                GetType(),
                "cboBudgetLevel",
                "FiltersBudgetLevel2();",
                true);
    }


    protected void grdBudgetL3_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

        grdBudgetL3.EditIndex = -1;
        grdBudgetL3.Columns[4].Visible = true;
        grdBudgetL3.Columns[5].Visible = true;
        FillBudgetGrid3();
        ScriptManager.RegisterStartupScript(
                this,
                GetType(),
                "cboBudgetLevel",
                "FiltersBudgetLevel3();",
                true);
    }

    protected void grdBudgetL4_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

        grdBudgetL4.EditIndex = -1;
        grdBudgetL4.Columns[6].Visible = true;
        grdBudgetL4.Columns[7].Visible = true;
        FillBudgetGrid4(intCategoryID, intBRAID, intSpendTypeID, intBrandID, intCustomerID, intRunTypeID);
        ScriptManager.RegisterStartupScript(
            this,
            GetType(),
            "cboBudgetLevel",
            "FiltersBudgetLevel4(" + intBRA + "," + intCategory + "," + intSpendType + "," + intBrand + "," + intCustomer + ");",
            true);
    }

    /** Row Data Bound for Budget (L1,L2,L3,L4) **/
    protected void grdBudgetL1_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        grdBudgetL1.Columns[5].Visible = true;
        grdBudgetL1.Columns[6].Visible = true;
        // Get a ClientScriptManager reference from the Page class.
        ClientScriptManager cs = Page.ClientScript;
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //e.Row.Attributes["onmouseover"] = "this.style.cursor='hand'";
            //;this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            //e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            //e.Row.Attributes["onclick"] = cs.GetPostBackClientHyperlink(this.grdBudgetL1, "Select$" + e.Row.RowIndex);
            
            e.Row.Attributes.Add("class", e.Row.Cells[6].Text);
            if (e.Row.Cells[6].Text == "balance")
            {
                e.Row.Cells[7].Text = "";
            }
            else if (e.Row.Cells[6].Text == "table" && e.Row.RowIndex != grdBudgetL1.EditIndex)
            {
                e.Row.Attributes["onclick"] = cs.GetPostBackClientHyperlink(this.grdBudgetL1, "Select$" + e.Row.RowIndex);
                e.Row.ToolTip = "Click to select this row.";
            }
            
        }

    }

    protected void grdBudgetL2_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        grdBudgetL2.Columns[5].Visible = true;
        grdBudgetL2.Columns[6].Visible = true;
        // Get a ClientScriptManager reference from the Page class.
        ClientScriptManager cs = Page.ClientScript;
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //e.Row.Attributes["onmouseover"] = "this.style.cursor='hand'";
            //;this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            //e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            //e.Row.Attributes["onclick"] = cs.GetPostBackClientHyperlink(this.grdBudgetL1, "Select$" + e.Row.RowIndex);

            e.Row.Attributes.Add("class", e.Row.Cells[6].Text);
            if (e.Row.Cells[6].Text == "balance")
            {
                e.Row.Cells[7].Text = "";
            }
            //else if (e.Row.Cells[6].Text == "table" && e.Row.RowIndex != grdBudgetL2.EditIndex)
            //{
            //    e.Row.Attributes["onclick"] = cs.GetPostBackClientHyperlink(this.grdBudgetL2, "Select$" + e.Row.RowIndex);
            //    e.Row.ToolTip = "Click to select this row.";
            //}

        }

    }

    protected void grdBudgetL3_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        grdBudgetL3.Columns[4].Visible = true;
        grdBudgetL3.Columns[5].Visible = true;
        // Get a ClientScriptManager reference from the Page class.
        ClientScriptManager cs = Page.ClientScript;
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //e.Row.Attributes["onmouseover"] = "this.style.cursor='hand'";
            //;this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            //e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            //e.Row.Attributes["onclick"] = cs.GetPostBackClientHyperlink(this.grdBudgetL1, "Select$" + e.Row.RowIndex);

            e.Row.Attributes.Add("class", e.Row.Cells[5].Text);
            if (e.Row.Cells[5].Text == "balance" || e.Row.Cells[5].Text == "table total")
            {
                e.Row.Cells[6].Text = "";
            }
            //else if (e.Row.Cells[6].Text == "table" && e.Row.RowIndex != grdBudgetL2.EditIndex)
            //{
            //    e.Row.Attributes["onclick"] = cs.GetPostBackClientHyperlink(this.grdBudgetL2, "Select$" + e.Row.RowIndex);
            //    e.Row.ToolTip = "Click to select this row.";
            //}

        }

    }

    protected void grdBudgetL4_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        grdBudgetL4.Columns[6].Visible = true;
        grdBudgetL4.Columns[7].Visible = true;
        // Get a ClientScriptManager reference from the Page class.
        ClientScriptManager cs = Page.ClientScript;
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //e.Row.Attributes["onmouseover"] = "this.style.cursor='hand'";
            //;this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            //e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            //e.Row.Attributes["onclick"] = cs.GetPostBackClientHyperlink(this.grdBudgetL1, "Select$" + e.Row.RowIndex);

            e.Row.Attributes.Add("class", e.Row.Cells[7].Text);
            if (e.Row.Cells[7].Text == "balance" || e.Row.Cells[7].Text == "table total")
            {
                e.Row.Cells[8].Text = "";
            }
            //else if (e.Row.Cells[6].Text == "table" && e.Row.RowIndex != grdBudgetL2.EditIndex)
            //{
            //    e.Row.Attributes["onclick"] = cs.GetPostBackClientHyperlink(this.grdBudgetL2, "Select$" + e.Row.RowIndex);
            //    e.Row.ToolTip = "Click to select this row.";
            //}

        }

    }

    /** Row Update for Budget (L1,L2,L3,L4) **/
    protected void grdBudgetL1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        grdBudgetL1.Columns[5].Visible = true;
        grdBudgetL1.Columns[6].Visible = true;
        SqlConnection sqlConn = new SqlConnection(Conn);
        GridViewRow row = grdBudgetL1.Rows[e.RowIndex];
        string Budget = grdBudgetL1.DataKeys[e.RowIndex].Values["BudgetID"].ToString();
        if (((TextBox)(row.Cells[4].Controls[0])).Text == "")
        {
             Target = 0;

        }
        else
        {
             Target = float.Parse(((TextBox)(row.Cells[4].Controls[0])).Text);
        }
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "UpdateBudget";
        cmd.Parameters.AddWithValue("@BudgetID", Budget);
        cmd.Parameters.AddWithValue("@Target", Convert.ToInt32(Target));
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataAccess.ExecuteSelectCommand(cmd);
        grdBudgetL1.EditIndex = -1;
        FillBudgetGrid();
        
    }

    protected void grdBudgetL2_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        grdBudgetL2.Columns[5].Visible = true;
        grdBudgetL2.Columns[6].Visible = true;
        SqlConnection sqlConn = new SqlConnection(Conn);
        GridViewRow row = grdBudgetL2.Rows[e.RowIndex];
        string Budget = grdBudgetL2.DataKeys[e.RowIndex].Values["BudgetID"].ToString();
        if (((TextBox)(row.Cells[4].Controls[0])).Text == "")
        {
            Target = 0;

        }
        else
        {
            Target = float.Parse(((TextBox)(row.Cells[4].Controls[0])).Text);
        }
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "UpdateBudget";
        cmd.Parameters.AddWithValue("@BudgetID", Budget);
        cmd.Parameters.AddWithValue("@Target", Convert.ToInt32(Target));
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataAccess.ExecuteSelectCommand(cmd);
        grdBudgetL2.EditIndex = -1;
        FillBudgetGrid2();
        ScriptManager.RegisterStartupScript(
                this,
                GetType(),
                "cboBudgetLevel",
                "FiltersBudgetLevel2();",
                true);
    }

    protected void grdBudgetL3_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        grdBudgetL3.Columns[4].Visible = true;
        grdBudgetL3.Columns[5].Visible = true;
        SqlConnection sqlConn = new SqlConnection(Conn);
        GridViewRow row = grdBudgetL3.Rows[e.RowIndex];
        string Budget = grdBudgetL3.DataKeys[e.RowIndex].Values["BudgetID"].ToString();
        if (((TextBox)(row.Cells[3].Controls[0])).Text == "")
        {
            Target = 0;

        }
        else
        {
            Target = float.Parse(((TextBox)(row.Cells[3].Controls[0])).Text);
        }
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "UpdateBudget";
        cmd.Parameters.AddWithValue("@BudgetID", Budget);
        cmd.Parameters.AddWithValue("@Target", Convert.ToInt32(Target));
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataAccess.ExecuteSelectCommand(cmd);
        grdBudgetL3.EditIndex = -1;
        FillBudgetGrid3();
        ScriptManager.RegisterStartupScript(
                this,
                GetType(),
                "cboBudgetLevel",
                "FiltersBudgetLevel3();",
                true);
    }

    protected void grdBudgetL4_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        grdBudgetL4.Columns[6].Visible = true;
        grdBudgetL4.Columns[7].Visible = true;
        SqlConnection sqlConn = new SqlConnection(Conn);
        GridViewRow row = grdBudgetL4.Rows[e.RowIndex];
        string Budget = grdBudgetL4.DataKeys[e.RowIndex].Values["BudgetID"].ToString();
        if (((TextBox)(row.Cells[5].Controls[0])).Text == "")
        {
            Target = 0;

        }
        else
        {
            Target = float.Parse(((TextBox)(row.Cells[5].Controls[0])).Text);
        }
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "UpdateBudget";
        cmd.Parameters.AddWithValue("@BudgetID", Budget);
        cmd.Parameters.AddWithValue("@Target", Convert.ToInt32(Target));
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataAccess.ExecuteSelectCommand(cmd);
        grdBudgetL4.EditIndex = -1;
        FillBudgetGrid4(intCategoryID, intBRAID, intSpendTypeID, intBrandID, intCustomerID, intRunTypeID);
        ScriptManager.RegisterStartupScript(
            this,
            GetType(),
            "cboBudgetLevel",
            "FiltersBudgetLevel4(" + intBRA + "," + intCategory + "," + intSpendType + "," + intBrand + "," + intCustomer + ");",
            true);
    }

    protected void grdBudgetL1_SelectedIndexChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow row in grdBudgetL1.Rows)
        {
            if (row.RowIndex == grdBudgetL1.SelectedIndex)
            {
                row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
            }
            else
            {
                row.BackColor = ColorTranslator.FromHtml("#FFFFFF");
            }
        }

    }

    protected void grdBudgetL2_SelectedIndexChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow row in grdBudgetL2.Rows)
        {
            if (row.RowIndex == grdBudgetL2.SelectedIndex)
            {
                row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
            }
            else
            {
                row.BackColor = ColorTranslator.FromHtml("#FFFFFF");
            }
        }

    }

    protected void grdBudgetL3_SelectedIndexChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow row in grdBudgetL3.Rows)
        {
            if (row.RowIndex == grdBudgetL3.SelectedIndex)
            {
                row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
            }
            else
            {
                row.BackColor = ColorTranslator.FromHtml("#FFFFFF");
            }
        }

    }

    protected void grdBudgetL4_SelectedIndexChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow row in grdBudgetL4.Rows)
        {
            if (row.RowIndex == grdBudgetL4.SelectedIndex)
            {
                row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
            }
            else
            {
                row.BackColor = ColorTranslator.FromHtml("#FFFFFF");
            }
        }

    }
 

    public string SelectedValue
    {
        get { return grdBudgetL1.SelectedValue.ToString(); }
        
    }

#endregion

#region dropdowns

    protected void FillBudgetLevel()
    //populate the Budget Level dropdown
    {
        cboBudgetLevel.Items.Clear();
        if (Convert.ToInt16(Session["CurrentUserLevelID"]) == 1)
        {
            cboBudgetLevel.Items.Add(new ListItem("Budget Level 1", "1"));
            cboBudgetLevel.Items.Add(new ListItem("Budget Level 2", "2"));
            cboBudgetLevel.Items.Add(new ListItem("Budget Level 3", "3"));
            cboBudgetLevel.Items.Add(new ListItem("Budget Level 4", "4"));
        }
        else if (Convert.ToInt16(Session["CurrentUserLevelID"]) <= 10)
        {
            cboBudgetLevel.Items.Add(new ListItem("Budget Level 3", "3"));
            cboBudgetLevel.Items.Add(new ListItem("Budget Level 4", "4"));
            ScriptManager.RegisterStartupScript(
                            this,
                            GetType(),
                            "cboBudgetLevel",
                            "FiltersBudgetLevel3();",
                            true);
            grdBudgetL2.Visible = false;
            grdBudgetL3.Visible = true;
            grdBudgetL4.Visible = false;
            grdBudgetL1.Visible = false;
            //FillBudgetResponsibilityArea();
            //FillCategory();
            //PopulateLastValues();
            //get current year from session variable set in global.asax
            cboYear.SelectedValue = Session["SelectedYear"].ToString();
            //FillBudgetGrid3();
        }
        else if (Convert.ToInt16(Session["CurrentUserLevelID"]) <= 25)
        {
            cboBudgetLevel.Items.Add(new ListItem("Budget Level 4", "4"));
            grdBudgetL2.Visible = false;
            grdBudgetL3.Visible = false;
            grdBudgetL4.Visible = true;
            grdBudgetL1.Visible = false;
            //FillBudgetResponsibilityArea();
            //FillCategory();
            //FillSpendType();
            //FillBrand();
            //FillCustomer();
            //PopulateLastValues();
            //get current year from session variable set in global.asax
            cboYear.SelectedValue = Session["SelectedYear"].ToString();
            //FillLevel4Filters();
        }



        strBL = cboBudgetLevel.SelectedValue;
        //ScriptManager.RegisterStartupScript(this, GetType(), "error", "alert('" + strBL + "');", true);
        //ScriptManager.RegisterStartupScript(this, GetType(), "error", "alert('" + Session["BudgetLevel"] + "');", true);
        
    }

    protected void FillYear()
    //fill year dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillYearBudgets";
        cmd.Parameters.AddWithValue("@ZeroValueText", "[None]");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboYear;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillBudgetResponsibilityArea()
    //fill BudgetResponsibilityArea dropdownlist
    {
            cboBudgetResponsibilityArea.Items.Clear();
            SqlCommand cmd = DataAccess.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "FillBudgetResponsibilityAreaBudgets";
            cmd.Parameters.AddWithValue("@ZeroValueText", "");
            cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
            cmd.Parameters.AddWithValue("@BudgetLevelID", Convert.ToInt32(strBL));
            DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
            DropDownList d = cboBudgetResponsibilityArea;
            d.DataSource = tbl;
            d.DataBind();
            
       
    }

    protected void FillCategory()
    //fill category dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillCategory";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        //cmd.Parameters.AddWithValue("@IncludeInactive", 0);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboCategory;
        d.DataSource = tbl;
        d.DataBind();
    }

    public void FillSpendType()
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillSpendType";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboSpendType;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillBrand()
    //fill brand dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillBrand";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboBrand;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillCustomer()
    //fill customer dropdownlist for current user
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillCustomer";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        cmd.Parameters.AddWithValue("@IncludeInactive", 0);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboCustomer;
        d.DataSource = tbl;
        d.DataBind();
    }

   
    protected void cboBudgetLevel_TextChanged(object sender, EventArgs e)
    {
        strBL = cboBudgetLevel.SelectedValue;
        if (strBL == "1")
        {
            ScriptManager.RegisterStartupScript(
                            this,
                            GetType(),
                            "cboBudgetLevel",
                            "FiltersBudgetLevel1();",
                            true);
            grdBudgetL2.Visible = false;
            grdBudgetL3.Visible = false;
            grdBudgetL4.Visible = false;
            grdBudgetL1.Visible = true;
            FillBudgetGrid();
        }
        else if (strBL == "2")
        {
            ScriptManager.RegisterStartupScript(
                            this,
                            GetType(),
                            "cboBudgetLevel",
                            "FiltersBudgetLevel2();",
                            true);
            grdBudgetL2.Visible = true;
            grdBudgetL3.Visible = false;
            grdBudgetL4.Visible = false;
            grdBudgetL1.Visible = false;
            //FillCategory();
            //PopulateLastValues();
            FillBudgetGrid2();

        } 

        else if (strBL == "3")
        {
            ScriptManager.RegisterStartupScript(
                           this,
                           GetType(),
                           "cboBudgetLevel",
                           "FiltersBudgetLevel3();",
                           true);
            grdBudgetL2.Visible = false;
            grdBudgetL3.Visible = true;
            grdBudgetL4.Visible = false;
            grdBudgetL1.Visible = false;
            FillBudgetResponsibilityArea();
            //FillCategory();
            PopulateLastValues();
            FillBudgetGrid3();
        }

        else if (strBL == "4")
        {

            grdBudgetL2.Visible = false;
            grdBudgetL3.Visible = false;
            grdBudgetL4.Visible = true;
            grdBudgetL1.Visible = false;
            FillBudgetResponsibilityArea();
            PopulateLastValues();
            FillLevel4Filters();
        }

        //ScriptManager.RegisterStartupScript(this, GetType(), "error", "alert('" + strBL + "');", true);
        //UpdateUserSelections();
        //SelectFirstRow();
    }

    public int getBudgetLevel
    {

        get { return Convert.ToInt32(strBL); }
    }


    protected string GetBRAValues(int BRAID)
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "GetBRAAttributeValues";
        cmd.Parameters.AddWithValue("@BudgetResponsibilityAreaID", BRAID);
        String strBRAValues = DataAccess.ExecuteScalar(cmd).ToString();
        return strBRAValues;
    }
    protected void FillLevel4Filters()
    {
        
        if (cboBudgetResponsibilityArea.SelectedValue == "0")
        {
            intBRA = 1;
            intCategory = 0;
            intSpendType = 0;
            intBrand = 0;
            intCustomer = 0;
            ScriptManager.RegisterStartupScript(
              this,
              GetType(),
              "cboBudgetLevel",
              "FiltersBudgetLevel4(" + intBRA + "," + intCategory + "," + intSpendType + "," + intBrand + "," + intCustomer + ");",
              true);
        }
        else
        {
            strBRACodes = GetBRAValues(Convert.ToInt32(cboBudgetResponsibilityArea.SelectedValue));
            strSpendType = cboSpendType.SelectedItem.Text;
            String[] codes = strBRACodes.Split('_');
            //strDeptCode = codes[0];
            strCBI = codes[1];
            strTCCRelevant = codes[2];

            //TCCRelevant and SpendType NUll
            if (strTCCRelevant == "1" && strSpendType == "")
            {

                intBRA = 1;
                intCategory = 0;
                intSpendType = 1;
                intBrand = 0;
                intCustomer = 0;
                ScriptManager.RegisterStartupScript(
                  this,
                  GetType(),
                  "cboBudgetLevel",
                  "FiltersBudgetLevel4(" + intBRA + "," + intCategory + "," + intSpendType + "," + intBrand + "," + intCustomer + ");",
                  true);
                intBRAID = null;
                intCategoryID = null;
                intSpendTypeID = null;
                intBrandID = null;
                intCustomerID = null; 
                intRunTypeID = 0; //No Run
                FillBudgetGrid4(intCategoryID, intBRAID, intSpendTypeID, intBrandID, intCustomerID, intRunTypeID);
            }

            else if (strCBI == "1")
            {
                if (strTCCRelevant == "1")
                {
                    if (strSpendType.Contains("A&P"))
                    {
                        intBRA = 1;
                        intCategory = 0;
                        intSpendType = 1;
                        intBrand = 0;
                        intCustomer = 0;
                        ScriptManager.RegisterStartupScript(
                          this,
                          GetType(),
                          "cboBudgetLevel",
                          "FiltersBudgetLevel4(" + intBRA + "," + intCategory + "," + intSpendType + "," + intBrand + "," + intCustomer + ");",
                          true);
                        intBRAID = Convert.ToInt32(cboBudgetResponsibilityArea.SelectedValue);
                        intCategoryID = null;
                        intSpendTypeID = Convert.ToInt32(cboSpendType.SelectedValue);
                        intBrandID = null;
                        intCustomerID = 0; // Pass all Customer values
                        intRunTypeID = 2; //Central A&P Spend Type
                        FillBudgetGrid4(intCategoryID, intBRAID, intSpendTypeID, intBrandID, intCustomerID, intRunTypeID);
                    }
                    else if (strSpendType.Contains("TCC"))
                    {
                        intBRA = 1;
                        intCategory = 0;
                        intSpendType = 1;
                        intBrand = 0;
                        intCustomer = 0;
                        ScriptManager.RegisterStartupScript(
                          this,
                          GetType(),
                          "cboBudgetLevel",
                          "FiltersBudgetLevel4(" + intBRA + "," + intCategory + "," + intSpendType + "," + intBrand + "," + intCustomer + ");",
                          true);
                        intBRAID = Convert.ToInt32(cboBudgetResponsibilityArea.SelectedValue);
                        intCategoryID = null;
                        intSpendTypeID = Convert.ToInt32(cboSpendType.SelectedValue);
                        intBrandID = null;
                        intCustomerID = 0; // Pass all Customer values
                        intRunTypeID = 3; //Central TCC Spend Type
                        FillBudgetGrid4(intCategoryID, intBRAID, intSpendTypeID, intBrandID, intCustomerID, intRunTypeID);
                    }
                }
                else
                {
                    intBRA = 1;
                    intCategory = 0;
                    intSpendType = 0;
                    intBrand = 0;
                    intCustomer = 0;
                    ScriptManager.RegisterStartupScript(
                      this,
                      GetType(),
                      "cboBudgetLevel",
                      "FiltersBudgetLevel4(" + intBRA + "," + intCategory + "," + intSpendType + "," + intBrand + "," + intCustomer + ");",
                      true);

                    intBRAID = Convert.ToInt32(cboBudgetResponsibilityArea.SelectedValue);
                    intCategoryID = null;
                    intSpendTypeID = null; // Pass all Spend Type values
                    intBrandID = null;
                    intCustomerID = 0;  // Pass all Customer values
                    intRunTypeID = 2; //Central Spend Type
                    FillBudgetGrid4(intCategoryID, intBRAID, intSpendTypeID, intBrandID, intCustomerID, intRunTypeID);
                }
               }
                else if (strCBI == "2")
                {
                    if (strTCCRelevant == "1")
                    {
                        if (strSpendType.Contains("A&P"))
                        {
                            intBRA = 1;
                            intCategory = 1;
                            intSpendType = 1;
                            intBrand = 1;
                            intCustomer = 0;
                            ScriptManager.RegisterStartupScript(
                              this,
                              GetType(),
                              "cboBudgetLevel",
                              "FiltersBudgetLevel4(" + intBRA + "," + intCategory + "," + intSpendType + "," + intBrand + "," + intCustomer + ");",
                              true);
                            intBRAID = Convert.ToInt32(cboBudgetResponsibilityArea.SelectedValue);
                            intCategoryID = Convert.ToInt32(cboCategory.SelectedValue);
                            intSpendTypeID = Convert.ToInt32(cboSpendType.SelectedValue);
                            intBrandID = Convert.ToInt32(cboBrand.SelectedValue); ;
                            intCustomerID = 0; // Pass all Customer values
                            intRunTypeID = 1; //Controllable A&P Spend Type
                            FillBudgetGrid4(intCategoryID, intBRAID, intSpendTypeID, intBrandID, intCustomerID, intRunTypeID);
                        }
                        else if (strSpendType.Contains("TCC"))
                        {
                            intBRA = 1;
                            intCategory = 1;
                            intSpendType = 1;
                            intBrand = 1;
                            intCustomer = 0;
                            ScriptManager.RegisterStartupScript(
                              this,
                              GetType(),
                              "cboBudgetLevel",
                              "FiltersBudgetLevel4(" + intBRA + "," + intCategory + "," + intSpendType + "," + intBrand + "," + intCustomer + ");",
                              true);
                            intBRAID = Convert.ToInt32(cboBudgetResponsibilityArea.SelectedValue);
                            intCategoryID = Convert.ToInt32(cboCategory.SelectedValue);
                            intSpendTypeID = Convert.ToInt32(cboSpendType.SelectedValue);
                            intBrandID = Convert.ToInt32(cboBrand.SelectedValue);
                            intCustomerID = 0; // Pass all Customer values
                            intRunTypeID = 3; //Controllable TCC Spend Type
                            FillBudgetGrid4(intCategoryID, intBRAID, intSpendTypeID, intBrandID, intCustomerID, intRunTypeID);
                        }
                    }
                    else
                        {
                            intBRA = 1;
                            intCategory = 1;
                            intSpendType = 0;
                            intBrand = 1;
                            intCustomer = 0;
                            ScriptManager.RegisterStartupScript(
                              this,
                              GetType(),
                              "cboBudgetLevel",
                              "FiltersBudgetLevel4(" + intBRA + "," + intCategory + "," + intSpendType + "," + intBrand + "," + intCustomer + ");",
                              true);

                            intBRAID = Convert.ToInt32(cboBudgetResponsibilityArea.SelectedValue);
                            intCategoryID = Convert.ToInt32(cboCategory.SelectedValue);
                            intSpendTypeID = null; // Pass all Spend Type values
                            intBrandID = Convert.ToInt32(cboBrand.SelectedValue);
                            intCustomerID = 0;  // Pass all Customer values
                            intRunTypeID = 1; //Controllable Spend Type
                            FillBudgetGrid4(intCategoryID, intBRAID, intSpendTypeID, intBrandID, intCustomerID, intRunTypeID);
                        }    
                }
                    else if (strCBI == "0")
                    {
                        intBRA = 1;
                        intCategory = 1;
                        intSpendType = 0;
                        intBrand = 1;
                        intCustomer = 0;
                        ScriptManager.RegisterStartupScript(
                          this,
                          GetType(),
                          "cboBudgetLevel",
                          "FiltersBudgetLevel4(" + intBRA + "," + intCategory + "," + intSpendType + "," + intBrand + "," + intCustomer + ");",
                          true);

                        intBRAID = Convert.ToInt32(cboBudgetResponsibilityArea.SelectedValue);
                        intCategoryID = Convert.ToInt32(cboCategory.SelectedValue);
                        intSpendTypeID = null; // Pass all Spend Type values
                        intBrandID = Convert.ToInt32(cboBrand.SelectedValue);
                        intCustomerID = 0;  // Pass all Customer values
                        intRunTypeID = 3; //Export,FoodSolutions..
                        FillBudgetGrid4(intCategoryID, intBRAID, intSpendTypeID, intBrandID, intCustomerID, intRunTypeID);
                    }
         } 
    }

    /* 
     * old code to handle filter in Budget L4 screen
     * 
    protected void FillLevel4Filters()
    {
        if (cboBudgetResponsibilityArea.SelectedValue == "0")
        {
            intBRA = 1;
            intCategory = 0;
            intSpendType = 0;
            intBrand = 0;
            intCustomer = 0;
            ScriptManager.RegisterStartupScript(
              this,
              GetType(),
              "cboBudgetLevel",
              "FiltersBudgetLevel4(" + intBRA + "," + intCategory + "," + intSpendType + "," + intBrand + "," + intCustomer + ");",
              true);
        }
        else
        {
            strBRACodes = GetBRAValues(Convert.ToInt32(cboBudgetResponsibilityArea.SelectedValue));
            strSpendType = cboSpendType.SelectedItem.Text;
            String[] codes = strBRACodes.Split('_');
            strDeptCode = codes[0];
            strCBI = codes[1];
            strTCCRelevant = codes[2];

            //TCCRelevant and SpendType NUll
            if (strTCCRelevant == "1" && strSpendType == "")
            {

                intBRA = 1;
                intCategory = 0;
                intSpendType = 1;
                intBrand = 0;
                intCustomer = 0;
                ScriptManager.RegisterStartupScript(
                  this,
                  GetType(),
                  "cboBudgetLevel",
                  "FiltersBudgetLevel4(" + intBRA + "," + intCategory + "," + intSpendType + "," + intBrand + "," + intCustomer + ");",
                  true);
                intBRAID = null;
                intCategoryID = null;
                intSpendTypeID = null;
                intBrandID = null;
                intCustomerID = null; 
                intRunTypeID = 0; //No Run
                FillBudgetGrid4(intCategoryID, intBRAID, intSpendTypeID, intBrandID, intCustomerID, intRunTypeID);
            }

            //TCCRelevant
            else if (strTCCRelevant == "1")
            {
                if (strSpendType.Contains("A&P"))
                {
                    if (strCBI == "1")
                    {
                        if (strDeptCode.Contains("BB") || strDeptCode.Contains("BD"))
                        {
                            intBRA = 1;
                            intCategory = 0;
                            intSpendType = 1;
                            intBrand = 0;
                            intCustomer = 0;
                            ScriptManager.RegisterStartupScript(
                              this,
                              GetType(),
                              "cboBudgetLevel",
                              "FiltersBudgetLevel4(" + intBRA + "," + intCategory + "," + intSpendType + "," + intBrand + "," + intCustomer + ");",
                              true);

                            intBRAID = Convert.ToInt32(cboBudgetResponsibilityArea.SelectedValue);
                            intCategoryID = null;
                            intSpendTypeID = Convert.ToInt32(cboSpendType.SelectedValue);
                            intBrandID = null;
                            intCustomerID = 0; // Pass all Customer values
                            intRunTypeID = 2; //Central Spend Type
                            FillBudgetGrid4(intCategoryID, intBRAID, intSpendTypeID, intBrandID, intCustomerID, intRunTypeID);

                        }
                        else if (strDeptCode.Contains("SM") || strDeptCode.Contains("CD"))
                        {
                            intBRA = 1;
                            intCategory = 0;
                            intSpendType = 1;
                            intBrand = 0;
                            intCustomer = 1;

                            ScriptManager.RegisterStartupScript(
                              this,
                              GetType(),
                              "cboBudgetLevel",
                              "FiltersBudgetLevel4(" + intBRA + "," + intCategory + "," + intSpendType + "," + intBrand + "," + intCustomer + ");",
                              true);

                            intBRAID = Convert.ToInt32(cboBudgetResponsibilityArea.SelectedValue);
                            intCategoryID = null;
                            intSpendTypeID = Convert.ToInt32(cboSpendType.SelectedValue);
                            intBrandID = null;
                            intCustomerID = Convert.ToInt32(cboCustomer.SelectedValue);
                            intRunTypeID = 2; //Central Spend Type
                            FillBudgetGrid4(intCategoryID, intBRAID, intSpendTypeID, intBrandID, intCustomerID, intRunTypeID);
                        }
                    }
                    else if (strCBI == "2")
                    {
                        if (strDeptCode.Contains("BB") || strDeptCode.Contains("BD") || strDeptCode.Contains("FS") || strDeptCode.Contains("EX"))
                        {
                            intBRA = 1;
                            intCategory = 0;
                            intSpendType = 1;
                            intBrand = 1;
                            intCustomer = 0;
                            ScriptManager.RegisterStartupScript(
                              this,
                              GetType(),
                              "cboBudgetLevel",
                              "FiltersBudgetLevel4(" + intBRA + "," + intCategory + "," + intSpendType + "," + intBrand + "," + intCustomer + ");",
                              true);

                            intBRAID = Convert.ToInt32(cboBudgetResponsibilityArea.SelectedValue);
                            intCategoryID = null;
                            intSpendTypeID = Convert.ToInt32(cboSpendType.SelectedValue);
                            intBrandID = Convert.ToInt32(cboBrand.SelectedValue);
                            intCustomerID = 0; // Pass all Customer values
                            intRunTypeID = 1; //Controllable Spend Type
                            FillBudgetGrid4(intCategoryID, intBRAID, intSpendTypeID, intBrandID, intCustomerID, intRunTypeID);
                        }
                        else if (strDeptCode.Contains("SM") || strDeptCode.Contains("CD"))
                        {
                            intBRA = 1;
                            intCategory = 0;
                            intSpendType = 1;
                            intBrand = 1;
                            intCustomer = 1;
                            ScriptManager.RegisterStartupScript(
                              this,
                              GetType(),
                              "cboBudgetLevel",
                              "FiltersBudgetLevel4(" + intBRA + "," + intCategory + "," + intSpendType + "," + intBrand + "," + intCustomer + ");",
                              true);

                            intBRAID = Convert.ToInt32(cboBudgetResponsibilityArea.SelectedValue);
                            intCategoryID = null;
                            intSpendTypeID = Convert.ToInt32(cboSpendType.SelectedValue);
                            intBrandID = Convert.ToInt32(cboBrand.SelectedValue);
                            intCustomerID = Convert.ToInt32(cboCustomer.SelectedValue);
                            intRunTypeID = 1; //Controllable Spend Type
                            FillBudgetGrid4(intCategoryID, intBRAID, intSpendTypeID, intBrandID, intCustomerID, intRunTypeID);

                        }
                    }
                }

                else if (strSpendType.Contains("TCC"))
                {
                    intBRA = 1;
                    intCategory = 1;
                    intSpendType = 1;
                    intBrand = 0;
                    intCustomer = 1;
                    ScriptManager.RegisterStartupScript(
                      this,
                      GetType(),
                      "cboBudgetLevel",
                      "FiltersBudgetLevel4(" + intBRA + "," + intCategory + "," + intSpendType + "," + intBrand + "," + intCustomer + ");",
                      true);

                    intBRAID = Convert.ToInt32(cboBudgetResponsibilityArea.SelectedValue);
                    intCategoryID = Convert.ToInt32(cboCategory.SelectedValue);
                    intSpendTypeID = Convert.ToInt32(cboSpendType.SelectedValue);
                    intBrandID = null;
                    intCustomerID = Convert.ToInt32(cboCustomer.SelectedValue);
                    intRunTypeID = 3; //TCC SpendType
                    FillBudgetGrid4(intCategoryID, intBRAID, intSpendTypeID, intBrandID, intCustomerID, intRunTypeID);

                }

            }

            else if (strCBI == "1")
            {
                if (strDeptCode.Contains("BB") || strDeptCode.Contains("BD"))
                {
                    intBRA = 1;
                    intCategory = 0;
                    intSpendType = 0;
                    intBrand = 0;
                    intCustomer = 0;
                    ScriptManager.RegisterStartupScript(
                      this,
                      GetType(),
                      "cboBudgetLevel",
                      "FiltersBudgetLevel4(" + intBRA + "," + intCategory + "," + intSpendType + "," + intBrand + "," + intCustomer + ");",
                      true);

                    intBRAID = Convert.ToInt32(cboBudgetResponsibilityArea.SelectedValue);
                    intCategoryID = null;
                    intSpendTypeID = null; // Pass all Spend Type values
                    intBrandID = null;
                    intCustomerID = 0;  // Pass all Customer values
                    intRunTypeID = 2; //Central Spend Type
                    FillBudgetGrid4(intCategoryID, intBRAID, intSpendTypeID, intBrandID, intCustomerID, intRunTypeID);

                }
                else if (strDeptCode.Contains("SM") || strDeptCode.Contains("CD"))
                {
                    intBRA = 1;
                    intCategory = 0;
                    intSpendType = 0;
                    intBrand = 0;
                    intCustomer = 1;

                    ScriptManager.RegisterStartupScript(
                      this,
                      GetType(),
                      "cboBudgetLevel",
                      "FiltersBudgetLevel4(" + intBRA + "," + intCategory + "," + intSpendType + "," + intBrand + "," + intCustomer + ");",
                      true);

                    intBRAID = Convert.ToInt32(cboBudgetResponsibilityArea.SelectedValue);
                    intCategoryID = null;
                    intSpendTypeID = null; // Pass all Spend Type values
                    intBrandID = null;
                    intCustomerID = Convert.ToInt32(cboCustomer.SelectedValue);
                    intRunTypeID = 2; //Central Spend Type
                    FillBudgetGrid4(intCategoryID, intBRAID, intSpendTypeID, intBrandID, intCustomerID, intRunTypeID);
                }
            }
            else if (strCBI == "2")
            {
                if (strDeptCode.Contains("BB") || strDeptCode.Contains("BD") || strDeptCode.Contains("FS") || strDeptCode.Contains("EX"))
                {
                    intBRA = 1;
                    intCategory = 0;
                    intSpendType = 0;
                    intBrand = 1;
                    intCustomer = 0;
                    ScriptManager.RegisterStartupScript(
                      this,
                      GetType(),
                      "cboBudgetLevel",
                      "FiltersBudgetLevel4(" + intBRA + "," + intCategory + "," + intSpendType + "," + intBrand + "," + intCustomer + ");",
                      true);

                    intBRAID = Convert.ToInt32(cboBudgetResponsibilityArea.SelectedValue);
                    intCategoryID = null;
                    intSpendTypeID = null;
                    intBrandID = Convert.ToInt32(cboBrand.SelectedValue);
                    intCustomerID = 0; // Pass all Customer values
                    intRunTypeID = 1; //Controllable Spend Type
                    FillBudgetGrid4(intCategoryID, intBRAID, intSpendTypeID, intBrandID, intCustomerID, intRunTypeID);
                }
                else if (strDeptCode.Contains("SM") || strDeptCode.Contains("CD"))
                {
                    intBRA = 1;
                    intCategory = 0;
                    intSpendType = 0;
                    intBrand = 1;
                    intCustomer = 1;
                    ScriptManager.RegisterStartupScript(
                      this,
                      GetType(),
                      "cboBudgetLevel",
                      "FiltersBudgetLevel4(" + intBRA + "," + intCategory + "," + intSpendType + "," + intBrand + "," + intCustomer + ");",
                      true);

                    intBRAID = Convert.ToInt32(cboBudgetResponsibilityArea.SelectedValue);
                    intCategoryID = null;
                    intSpendTypeID = null;
                    intBrandID = Convert.ToInt32(cboBrand.SelectedValue);
                    intCustomerID = Convert.ToInt32(cboCustomer.SelectedValue);
                    intRunTypeID = 1; //Controllable Spend Type
                    FillBudgetGrid4(intCategoryID, intBRAID, intSpendTypeID, intBrandID, intCustomerID, intRunTypeID);

                }
            }
        }
        
    }
     
     */
    protected void cboYear_TextChanged(object sender, EventArgs e)
    {
        Session["SelectedYear"] = cboYear.SelectedValue;
        strBL = cboBudgetLevel.SelectedValue;
        if (strBL == "1")
        {
            ScriptManager.RegisterStartupScript(
                this,
                GetType(),
                "cboBudgetLevel",
                "FiltersBudgetLevel1();",
                true);
            FillBudgetGrid();
        }
        else if (strBL == "2")
        {
            ScriptManager.RegisterStartupScript(
          this,
          GetType(),
          "cboBudgetLevel",
          "FiltersBudgetLevel2();",
          true);
            FillBudgetGrid2();
        }
        else if (strBL == "3")
        {
            ScriptManager.RegisterStartupScript(
          this,
          GetType(),
          "cboBudgetLevel",
          "FiltersBudgetLevel3();",
          true);
            FillBudgetGrid3();
        }
        else if (strBL == "4")
        {
            FillLevel4Filters();
        }

        //UpdateUserSelections();
        //SelectFirstRow();
    }


    protected void cboBudgetResponsibilityArea_TextChanged(object sender, EventArgs e)
    {
        Session["SelectedBudgetResponsibilityAreaID"] = cboBudgetResponsibilityArea.SelectedValue;
        //strBRAText = cboBudgetResponsibilityArea.SelectedItem.Text;
        strBL = cboBudgetLevel.SelectedValue;

        if (strBL == "3")
        {
            ScriptManager.RegisterStartupScript(
                            this,
                            GetType(),
                            "cboBudgetLevel",
                            "FiltersBudgetLevel3();",
                            true);
             FillBudgetGrid3();
        }

        else if (strBL == "4")
        {
            FillLevel4Filters();
        }
         
        UpdateUserSelections();
        //SelectFirstRow();
    }


    protected void cboCategory_TextChanged(object sender, EventArgs e)
    {
        strBL = cboBudgetLevel.SelectedValue;
        //strSpendType = cboSpendType.SelectedItem.Text;
        Session["SelectedCategoryID"] = cboCategory.SelectedValue;
        
        if (strBL == "2")
        {
            ScriptManager.RegisterStartupScript(
                            this,
                            GetType(),
                            "cboBudgetLevel",
                            "FiltersBudgetLevel2();",
                            true);
            FillBudgetGrid2();
        }

        else if (strBL == "3")
        {
            ScriptManager.RegisterStartupScript(
                            this,
                            GetType(),
                            "cboBudgetLevel",
                            "FiltersBudgetLevel3();",
                            true);
            FillBudgetGrid3();
        }

        else if (strBL == "4")
        {
            FillLevel4Filters();
        }

        UpdateUserSelections();
    }

    protected void cboSpendType_TextChanged(object sender, EventArgs e)
    {
        Session["SelectedSpendTypeID"] = cboSpendType.SelectedValue;
        //strBRAText = cboBudgetResponsibilityArea.SelectedItem.Text;
        strSpendType = cboSpendType.SelectedItem.Text;
        strBL = cboBudgetLevel.SelectedValue;

        if (strBL == "4" && (strSpendType.Contains("A&P") || strSpendType.Contains("TCC")))
        {
            FillLevel4Filters();
        }

        else if (strBL == "4" && (strSpendType.Contains("Cpns") || strSpendType.Contains("")))
        {
            intBRA = 1;
            intCategory = 0;
            intSpendType = 1;
            intBrand = 0;
            intCustomer = 0;

            ScriptManager.RegisterStartupScript(
              this,
              GetType(),
              "cboBudgetLevel",
              "FiltersBudgetLevel4(" + intBRA + "," + intCategory + "," + intSpendType + "," + intBrand + "," + intCustomer + ");",
              true);
            intBRAID = null;
            intCategoryID = null;
            intSpendTypeID = null;
            intBrandID = null;
            intCustomerID = null;
            intRunTypeID = 0; //No Run
            FillBudgetGrid4(intCategoryID, intBRAID, intSpendTypeID, intBrandID, intCustomerID, intRunTypeID);

        }
        
    }

    protected void cboBrand_TextChanged(object sender, EventArgs e)
    {
        Session["SelectedBrandID"] = cboBrand.SelectedValue;
        //strBRAText = cboBudgetResponsibilityArea.SelectedItem.Text;
        //strSpendType = cboSpendType.SelectedItem.Text;
        strBL = cboBudgetLevel.SelectedValue;
        
        if (strBL == "4")
        {
            FillLevel4Filters();
        }

        UpdateUserSelections();
    }

    protected void cboCustomer_TextChanged(object sender, EventArgs e)
    {
        Session["SelectedCustomerID"] = cboCustomer.SelectedValue;
        //strBRAText = cboBudgetResponsibilityArea.SelectedItem.Text;
        //strSpendType = cboSpendType.SelectedItem.Text;
        strBL = cboBudgetLevel.SelectedValue;

        if (strBL == "4")
        {
            FillLevel4Filters();
        }
         
        UpdateUserSelections();
    }


    protected void PopulateLastValues()
    {

        DataTable tbl = UserDataAccess.SelectUser(Session["CurrentUserName"].ToString());
        // set area 
        if (Session["SelectedBudgetResponsibilityAreaID"] == null)
        {
            if (tbl.Rows[0]["LastBudgetResponsibilityAreaID"] == DBNull.Value)
            //allows for user's first login when table entry will be blank
            {
                Session["SelectedBudgetResponsibilityAreaID"] = Common.ADOLookup("BudgetResponsibilityAreaID", "qryUserAuthorisations", "UserName = '" + Session["CurrentUserName"].ToString().Replace("'", "''") + "'");
            }
            else
            {
                Session["SelectedBudgetResponsibilityAreaID"] = tbl.Rows[0]["LastBudgetResponsibilityAreaID"].ToString();
            }
            strBRACodes = GetBRAValues(Convert.ToInt32(Session["SelectedBudgetResponsibilityAreaID"].ToString()));
            String[] codes = strBRACodes.Split('_');
            strCBI = codes[1];
        }
        if (strBL == "3" && strCBI == "2")
        {
             cboBudgetResponsibilityArea.SelectedValue = Session["SelectedBudgetResponsibilityAreaID"].ToString();
            
        }
        else if (strBL == "4")
        {
            cboBudgetResponsibilityArea.SelectedValue = Session["SelectedBudgetResponsibilityAreaID"].ToString();
            
        }
        else
        {
            cboBudgetResponsibilityArea.SelectedValue = "0";  
        }
        //strBRA = Session["SelectedBudgetResponsibilityAreaID"].ToString();

        if (Session["SelectedCustomerID"] == null)
        {
            Session["SelectedCustomerID"] = tbl.Rows[0]["LastCustomerID"].ToString();
        }
        cboCustomer.SelectedValue = Session["SelectedCustomerID"].ToString();
        //strCustomer = Session["SelectedCustomerID"].ToString();

        if (Session["SelectedBrandID"] == null)
        {
            Session["SelectedBrandID"] = tbl.Rows[0]["LastBrandID"].ToString();
        }
        cboBrand.SelectedValue = Session["SelectedBrandID"].ToString();
        //strBrand = Session["SelectedBrandID"].ToString();

        if (Session["SelectedCategoryID"] == null)
        {
            Session["SelectedCategoryID"] = tbl.Rows[0]["LastCategoryID"].ToString();
        }
        cboCategory.SelectedValue = Session["SelectedCategoryID"].ToString();
        //strCategory = Session["SelectedCategoryID"].ToString();
    }


    protected void UpdateUserSelections()
    {
        strBL = cboBudgetLevel.SelectedValue;
   /* Enable Year, only if user wants to store last year selected     
        if (cboYear.SelectedValue == "")
        {
            strUyear = "0";

        }
        else
        {
            strUyear = cboYear.SelectedValue;
        }
  */       
         if (cboBudgetResponsibilityArea.SelectedValue == "")
         {
             intUBRA = 0;
         }
         else
         {

            intUBRA = Convert.ToInt32(cboBudgetResponsibilityArea.SelectedValue);
             
         }

         if (cboCategory.SelectedValue == "")
         {
             intUCategory = 0;
         }
         else
         {
             intUCategory = Convert.ToInt32(cboCategory.SelectedValue);
         }

         if (cboBrand.SelectedValue == "")
         {
             intUBrand = 0;
         }
         else
         {
             intUBrand = Convert.ToInt32(cboBrand.SelectedValue);
         }

         if (cboCustomer.SelectedValue == "")
         {
             intUCustomer = 0;
         }
         else
         {
             intUCustomer = Convert.ToInt32(cboCustomer.SelectedValue);
         }
        
        UserDataAccess.UpdateBudgetUserSelections(Convert.ToInt32(strBL),intUBRA,intUCategory,intUBrand,intUCustomer,Session["CurrentUserName"].ToString());
    }


    #endregion

   
}