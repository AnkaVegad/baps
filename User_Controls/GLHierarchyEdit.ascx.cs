﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Controls_GLHierarchyEdit : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //populate the activity indicator dropdown
            ActiveIndicatorName.Items.Add(new ListItem("", "0"));
            ActiveIndicatorName.Items.Add(new ListItem("Active", "1"));
            ActiveIndicatorName.Items.Add(new ListItem("Inactive", "2"));
            //populate other dropdowns - remember you can't do this on Postbacks
            FillGLLevels();
        }
    }

#region properties

    public GLHierarchy SelectGLHierarchy
    {
        get
        {
            GLHierarchy g = new GLHierarchy();
            g.GLHierarchyID = Convert.ToInt32(GLHierarchyID.Text);
            g.GLCode = GLCode.Text;
            g.GLLevel1ID = Convert.ToInt32(GLLevel1ID.SelectedValue);
            g.GLLevel2ID = Convert.ToInt32(GLLevel2ID.SelectedValue);
            g.GLLevel3ID = Convert.ToInt32(GLLevel3ID.SelectedValue);
            g.ActiveIndicator = ActiveIndicatorName.SelectedValue;
            g.CreatedBy = Session["CurrentUserName"].ToString();
            return g;
        }
    }

    public string HeaderText
    {
        set { lblHeader.Text = value; }
    }

#endregion

#region methods

    public void PopulateForm(GLHierarchy g)
    {
        GLHierarchyID.Text = g.GLHierarchyID.ToString();
        GLCode.Text = g.GLCode;
        GLLevel1ID.SelectedValue = g.GLLevel1ID.ToString();
        GLLevel2ID.SelectedValue = g.GLLevel2ID.ToString();
        GLLevel3ID.SelectedValue = g.GLLevel3ID.ToString();
        ActiveIndicatorName.SelectedValue = g.ActiveIndicator;
        GLCode.Focus();
        //enable controls
        if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 15)
        {
            ToggleControlState(true);
        }
    }

    public string UpdateGLHierarchy()
    {
        if (Session["CurrentUserLevelID"] == null) //check for session expiry
        {
            Response.Redirect("ProjectView.aspx");
            return "";
        }
        else
        {
            ////Main data
            GLHierarchy g = SelectGLHierarchy;
            int intResult = GLHierarchyDataAccess.UpdateGLHierarchy(g);
            if (intResult == 0)
            {
                return "Duplicate GL Hierarchy values.";
            }
            else
            {
                //Clear
                ClearForm();
                return "";
            }
        }
    }

    public string InsertGLHierarchy()
    {
        if (Session["CurrentUserLevelID"] == null) //check for session expiry
        {
            Response.Redirect("ProjectView.aspx");
            return "";
        }
        else
        {
            //Main data
            GLHierarchy g = SelectGLHierarchy;
            g.CreatedBy = Session["CurrentUserName"].ToString();
            int intGLHierarchyID = GLHierarchyDataAccess.InsertGLHierarchy(g);
            //trap duplicate GLHierarchy values
            if (intGLHierarchyID == 0)
            {
                return "Duplicate GL Hierarchy values.";
            }
            else
            {
                //Clear
                ClearForm();
                return "";
            }
        }
    }

    protected void ToggleControlState(bool state)
    {
        //enable or disable textboxes and dropdowns
        foreach (Control c in Controls)
        {
            if (c is TextBox) { ((TextBox)c).Enabled = state; }
            if (c is DropDownList) { ((DropDownList)c).Enabled = state; }
        }
    }

    public void ClearControls()
    {
        //enable or disable textboxes and dropdowns
        foreach (Control c in Controls)
        {
            if (c is TextBox) { ((TextBox)c).Text = ""; }
            if (c is DropDownList) { ((DropDownList)c).SelectedValue = "0"; }
        }
    }

    public void ClearForm()
    {
        ClearControls();
        ToggleControlState(false);
    }

    public void NewRecord()
    {
        ClearControls();
        GLHierarchyID.Text = "0";
        GLCode.Focus();
        //enable controls
        ToggleControlState(true);
    }

    public void FillGLLevels()
    {
        FillGLLevel("1", GLLevel1ID);
        FillGLLevel("2", GLLevel2ID);
        FillGLLevel("3", GLLevel3ID);
    }

#endregion

#region filldropdowns

    protected void FillGLLevel(string strGLHierarchyLevel, DropDownList d)
    //fill GL Level dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillGLLevel";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        cmd.Parameters.AddWithValue("@GLHierarchyLevel", strGLHierarchyLevel);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        d.DataSource = tbl;
        d.DataBind();
    }

#endregion
}
