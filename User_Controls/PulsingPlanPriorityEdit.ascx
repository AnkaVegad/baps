﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PulsingPlanPriorityEdit.ascx.cs" Inherits="User_Controls_PulsingPlanPriorityEdit" %>

    <!--PulsingPlanPriority Edit Area-->
    <table style="width:1100px">
        <tr><td style="font-size:2px">&nbsp;</td></tr>
        <tr>
            <td>
                <asp:Label ID="lblHeader" runat="server" CssClass="page_header"></asp:Label><br />
            </td>
        </tr><tr><td style="font-size:2px">&nbsp;</td></tr>
    </table>
    
    <div id="product_edit_area">
        <table style="width:1000px">
            <tr><td style="vertical-align:top">
                <table><tr>
                    <td>Pulsing Plan Priority</td><td>
                        <asp:Label ID="PulsingPlanPriorityID" runat="server" Visible="false" width="40px"/>
                        <asp:TextBox ID="PulsingPlanPriorityName" runat="server" Width="280px" MaxLength="35" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                            ControlToValidate="PulsingPlanPriorityName" ValidationGroup="PulsingPlanPriority"
                            Display="Dynamic" ErrorMessage="*Required" />
                    </td></tr>
                </table>            
            </td></tr>
        </table>
    </div>
    