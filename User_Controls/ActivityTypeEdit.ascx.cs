﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Controls_ActivityTypeEdit : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //populate the drop-downs in edit area
        if (!IsPostBack)
        {
            FillSpendType();
            FillBudgetResponsibilityArea();
            FillActivityTypeGroup();
        }
    }

#region properties

    public ActivityType SelectActivityType
    {
        get
        {
            ActivityType a = new ActivityType();
            a.ActivityTypeID = Convert.ToInt32(ActivityTypeID.Text);
            a.ActivityTypeName = ActivityTypeName.Text;
            a.GLCode = GLCode.Text;
            a.NeedsFinanceApprovalFlag = CheckNeedsFinanceApprovalFlag.Checked?"1":"0";
            a.CreatedBy = Session["CurrentUserName"].ToString();
            return a;
        }
    }

    public string ActivityTypeIDValue
    {
        get { return ActivityTypeID.Text; }
    }

    public string HeaderText
    {
        set { lblHeader.Text = value; }
    }

#endregion

#region gridevents

    double dblTotal = 0;

    protected void grdSubBrandID_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            //highlight on hover
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            e.Row.Attributes["onclick"] = cs.GetPostBackClientHyperlink(this.grdSubBrandID, "Select$" + e.Row.RowIndex);
        }
    }

#endregion

#region methods

    public void PopulateForm(ActivityType a)
    {
        //populate fields
        ActivityTypeID.Text = a.ActivityTypeID.ToString();
        ActivityTypeName.Text = a.ActivityTypeName;
        GLCode.Text = a.GLCode;
        CheckNeedsFinanceApprovalFlag.Checked = a.NeedsFinanceApprovalFlag=="1"?true:false;
       
        //populate grdSubBrandID
        DataTable tbl = ActivityTypeDataAccess.FillActivityTypeToSpendType(Convert.ToInt32(a.ActivityTypeID));
        Session["SelectedSubBrandItems"] = tbl;
        grdSubBrandID.DataSource = tbl;
        grdSubBrandID.DataBind();

        //enable controls (must be after ActivityTypeID is populated)
        if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 15)
        {
            ToggleControlState(true);
        }
    }

    public void UpdateActivityType()
    {
        if (Session["SelectedSubBrandItems"] == null) //check for session expiry
        {
            Response.Redirect("ProjectView.aspx");
        }
        else
        {
            //Main data
            int intRowsAffected = ActivityTypeDataAccess.UpdateActivityType(SelectActivityType);
            //SubBrands - clear and recreate
            ActivityTypeDataAccess.ClearActivityTypeToSpendType(Convert.ToInt32(ActivityTypeID.Text));
            DataTable tbl = (DataTable)Session["SelectedSubBrandItems"];
            foreach (DataRow r in tbl.Rows)
            {
                ActivityTypeDataAccess.InsertActivityTypeToSpendType(Convert.ToInt32(ActivityTypeID.Text), Convert.ToInt32(r["BudgetResponsibilityAreaID"]), Convert.ToInt32(r["SpendTypeID"]), Convert.ToInt32(r["ActivityTypeGroupID"]), Convert.ToInt16(r["ActiveIndicator"]), Session["CurrentUserName"].ToString());
            }
            //Clear
            ClearForm();
        }
    }

    public string InsertActivityType()
    {
        if (Session["SelectedSubBrandItems"] == null) //check for session expiry
        {
            Response.Redirect("ProjectView.aspx");
            return "";
        }
        else
        {
            DataTable tbl = (DataTable)Session["SelectedSubBrandItems"];
            //validation check at least one allocation is selected
            if (tbl.Rows.Count == 0)
            {
                return "You must select at least one allocation.";
            }
            else
            {
                //Main data
                int intActivityTypeID = ActivityTypeDataAccess.InsertActivityType(SelectActivityType);
                //trap duplicate ActivityType name
                if (intActivityTypeID == 0)
                {
                    return "Duplicate Activity Type name.";
                }
                else
                {
                    //Allocation
                    DataTable tblSubBrand = (DataTable)Session["SelectedSubBrandItems"];
                    foreach (DataRow r in tblSubBrand.Rows)
                    {
                        ActivityTypeDataAccess.InsertActivityTypeToSpendType(intActivityTypeID, Convert.ToInt32(r["BudgetResponsibilityAreaID"]), Convert.ToInt32(r["SpendTypeID"]), Convert.ToInt32(r["ActivityTypeGroupID"]), 1, Session["CurrentUserName"].ToString());
                    }
                    //Clear
                    ClearForm();
                    return "";
                }
            }
        }
    }

    protected void ToggleControlState(bool state)
    {
        //enable or disable textboxes and dropdowns
        foreach (Control c in Controls)
        {
            if (c is TextBox) { ((TextBox)c).Enabled = state; }
            if (c is DropDownList) { ((DropDownList)c).Enabled = state; }
        }
        //other controls
        grdSubBrandID.Enabled = state;
        cmdAddSubBrand.Enabled = state;
        cmdRemoveSubBrand.Enabled = state;
    }

    protected void ClearControls()
    {
        //remove data from controls
        ActivityTypeID.Text = "";
        ActivityTypeName.Text = "";
        GLCode.Text = "";
        CheckNeedsFinanceApprovalFlag.Checked = false;
        //Clear Allocation
        grdSubBrandID.DataSource = "";
        grdSubBrandID.DataBind();
        Session["SelectedSubBrandItems"] = null;
        cboSpendTypeID.SelectedValue = "0";
        cboBudgetResponsibilityAreaID.SelectedValue = "0";
        cboActivityTypeGroupID.SelectedValue = "0";
    }

    public void ClearForm()
    {
        ClearControls();
        ToggleControlState(false);
    }

    public void NewRecord()
    {
        ClearControls();
         //initialise allocations table
        DataTable tbl = new DataTable();
        tbl.Columns.Add("UniqueKey");
        tbl.Columns.Add("BudgetResponsibilityAreaID");
        tbl.Columns.Add("BudgetResponsibilityAreaName");
        tbl.Columns.Add("SpendTypeID");
        tbl.Columns.Add("SpendTypeName");
        tbl.Columns.Add("ActivityTypeGroupID");
        tbl.Columns.Add("ActivityTypeGroupName");
        tbl.Columns.Add("ActiveIndicator");
        tbl.Columns.Add("ActiveIndicatorName");
        Session["SelectedSubBrandItems"] = tbl;
 
        ActivityTypeID.Text = "0";
        ActivityTypeName.Focus();
        //enable controls (must be after ActivityTypeID is populated)
        ToggleControlState(true);
    }

    protected void AddSelectedSubBrand(int intBudgetResponsibilityAreaID, int intSpendTypeID, int intActivityTypeGroupID, int intActiveIndicator)
    {
        DataTable tbl = (DataTable)Session["SelectedSubBrandItems"];

        //loop through tbl to see if record with this SubBrandID has already been added (to prevent duplication)
        bool duplicate = false;
        for (int i = tbl.Rows.Count - 1; i >= 0; i--)
        {
            DataRow dr = tbl.Rows[i];
            string dtitem1 = dr["SpendTypeID"].ToString();
            string dtitem2 = dr["BudgetResponsibilityAreaID"].ToString();
            if (dtitem1 == intSpendTypeID.ToString() && dtitem2 == intBudgetResponsibilityAreaID.ToString())
            {
                duplicate = true;
            }
        }

        if (!duplicate)
        {
            //add the selected item to the table
            AddRow(tbl, intBudgetResponsibilityAreaID, intSpendTypeID, intActivityTypeGroupID, intActiveIndicator); 
        }

        grdSubBrandID.DataSource = tbl;
        grdSubBrandID.DataBind();
        //store basket table in session variable
        Session["SelectedSubBrandItems"] = tbl;
        //clear fields
        cboSpendTypeID.SelectedValue = "0";
        cboBudgetResponsibilityAreaID.SelectedValue = "0";
        cboActivityTypeGroupID.SelectedValue = "0";
    }

    protected void RemoveSubBrand()
    //remove the selected line item from the temporary table and grid
    {
        try //because an item may not be selected in the grid or the Session variable may not be valid
        {
            //loop through the table and delete if criteria found
            DataTable tbl = (DataTable)Session["SelectedSubBrandItems"];

            //mark record for deletion
            for (int i = tbl.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = tbl.Rows[i];
                string dtitem = dr["UniqueKey"].ToString();
                if (dtitem == grdSubBrandID.SelectedValue.ToString())
                {
                    tbl.Rows[i].Delete();
                }
            }

            //delete
            tbl.AcceptChanges();

            grdSubBrandID.DataSource = tbl;
            grdSubBrandID.DataBind();
            //store basket table in session variable
            Session["SelectedSubBrandItems"] = tbl;
        }
        catch { }
    }

    protected void ToggleActiveIndicator()
    //toggle ActivityIndicator for selected ActivityType_SpendType_BudgetResponsibilityArea in temporary table and grid 
    {
        try //because an item may not be selected in the grid or the Session variable may not be valid
        {
            //loop through the table and if criteria met toggle ActiveIndicator
            DataTable tbl = (DataTable)Session["SelectedSubBrandItems"];

            //find record, remove it, replace with new one
            for (int i = tbl.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = tbl.Rows[i];
                string dtitem = dr["UniqueKey"].ToString();
                if (dtitem == grdSubBrandID.SelectedValue.ToString())
                {
                    int intActiveIndicator = 0;
                    int intBudgetResponsibilityAreaID = Convert.ToInt32(dr["BudgetResponsibilityAreaID"]);
                    int intSpendTypeID = Convert.ToInt32(dr["SpendTypeID"]);
                    int intActivityTypeGroupID = Convert.ToInt32(dr["ActivityTypeGroupID"]);
                    if (Convert.ToInt16(dr["ActiveIndicator"]) == 0) { intActiveIndicator = 1; }
                    //remove existing row from table    
                    tbl.Rows[i].Delete();
                    tbl.AcceptChanges();
                    //add new row to table
                    AddRow(tbl, intBudgetResponsibilityAreaID, intSpendTypeID, intActivityTypeGroupID, intActiveIndicator);
                }
            }
            //refresh grid
            grdSubBrandID.DataSource = tbl;
            grdSubBrandID.DataBind();
            //store basket table in session variable
            Session["SelectedSubBrandItems"] = tbl;
        }
        catch { }
    }

    protected void AddRow(DataTable tbl, int intBudgetResponsibilityAreaID, int intSpendTypeID, int intActivityTypeGroupID, int intActiveIndicator)
    {                    
        //add the selected item to the table
        DataRow dr = tbl.NewRow();
        dr["UniqueKey"] = intBudgetResponsibilityAreaID.ToString() + "." + intSpendTypeID.ToString();
        dr["BudgetResponsibilityAreaID"] = intBudgetResponsibilityAreaID;
        dr["BudgetResponsibilityAreaName"] = Common.ADOLookup("BudgetResponsibilityAreaName", "tblBudgetResponsibilityArea", "BudgetResponsibilityAreaID = " + intBudgetResponsibilityAreaID.ToString());
        dr["SpendTypeID"] = intSpendTypeID;
        dr["SpendTypeName"] = Common.ADOLookup("SpendTypeName", "tblSpendType", "SpendTypeID = " + intSpendTypeID.ToString());
        dr["ActivityTypeGroupID"] = intActivityTypeGroupID;
        dr["ActivityTypeGroupName"] = Common.ADOLookup("ActivityTypeGroupName", "tblActivityTypeGroup", "ActivityTypeGroupID = " + intActivityTypeGroupID.ToString());
        dr["ActiveIndicator"] = intActiveIndicator;
        if (intActiveIndicator == 1)
        {
            dr["ActiveIndicatorName"] = "Active";
        }
        else
        {
            dr["ActiveIndicatorName"] = "Inactive";
        }
        tbl.Rows.Add(dr);
    }

#endregion

#region dropdowns

    protected void FillBudgetResponsibilityArea()
    //Fill BudgetResponsibilityArea dropdownlist for which current user has permissions
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillBudgetResponsibilityArea";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboBudgetResponsibilityAreaID;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillSpendType()
    //Fill dropdownlist of SpendTypes ***to do: "for selected budget responsibility area"***
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillSpendType";
        //cmd.Parameters.AddWithValue("@ZeroValueText", "");
        //cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboSpendTypeID;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillActivityTypeGroup()
    //Fill dropdownlist of ActivityTypeGroups
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ge_FillActivityTypeGroup";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboActivityTypeGroupID;
        d.DataSource = tbl;
        d.DataBind();
    }

#endregion

#region subformevents

    protected void cmdAddSubBrand_Click(object sender, EventArgs e)
    {
        AddSelectedSubBrand(Convert.ToInt32(cboBudgetResponsibilityAreaID.SelectedValue), Convert.ToInt32(cboSpendTypeID.SelectedValue), Convert.ToInt32(cboActivityTypeGroupID.SelectedValue), 1);
    }

    protected void cmdRemoveSubBrand_Click(object sender, EventArgs e)
    {
        RemoveSubBrand();
    }

    protected void cmdActive_Click(object sender, EventArgs e)
    {
        ToggleActiveIndicator();
    }

#endregion
}
