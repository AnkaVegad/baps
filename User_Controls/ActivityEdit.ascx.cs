﻿using System;
using System.Data.SqlClient;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Controls_ActivityEdit : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //check for session expired
        if (Session["SelectedProjectToCustomerID"] == null | Session["SelectedBudgetResponsibilityAreaID"] == null)
        {
            Response.Redirect("ProjectView.aspx");
        }

        if (!IsPostBack)
        {
            //add onchange events
            ProjectMonth.Attributes.Add("onchange", "Javascript:showPhasing();");
            //ActivityTypeID.Attributes.Add("onchange", "Javascript:setNeedToConfirm(true);");
            ActivityName.Attributes.Add("onblur", "Javascript:copyToShortText();");
            //populate the drop-downs in edit area
            FillSpendType();
            FillMonth();
            FillBudgetResponsibilityArea();
        }
    }

    protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
    //if ProjectMonth is 'Phased' check that the monthly breakdown matches the quantity * price entered
    //seems to fire even if requiredfieldvalidation has failed (because of IsChanged() function), so check for correct entries in fields first
    //amended 27/03/12 with big number check
    {
        if (ItemQuantity.Text == String.Empty || UnitPrice.Text == String.Empty)
        {
            args.IsValid = false;
        }
        else
        {
            if ((Convert.ToInt32(ItemQuantity.Text) * Convert.ToDecimal(UnitPrice.Text)) > 100000000)
            {
                args.IsValid = false;
            }
            else
            {
                if (ProjectMonth.SelectedValue == "13")
                {
                    if (Convert.ToInt32(Convert.ToInt32(ItemQuantity.Text) * Convert.ToDecimal(UnitPrice.Text)) == SumMonthlyBreakdown())
                    {
                        args.IsValid = true;
                    }
                    else
                    {
                        args.IsValid = false;
                    }
                }
                else
                {
                    args.IsValid = true;
                }
            }
        }
    }

#region properties

    public string BudgetResponsibilityAreaIDSelected
    {
        set { BudgetResponsibilityAreaID.SelectedValue = value; }
    }

    public string ChangedValue
    {
        set { txtChanged.Value = value; }
    }

    public string SpendTypeIDSelected
    {
        set 
        { 
            SpendTypeID.SelectedValue = value;
            //Show only active ActivityTypes; amended 05/03/12 to correct ActivityTypes issue ***remmed out 22/02/13 for new Activity Type (Multi)View ***
            //FillActivityType(Convert.ToInt32(value), Convert.ToInt32(BudgetResponsibilityAreaID.SelectedValue), 1);
        }
    }

    public string ProjectMonthValue
    {
        get { return ProjectMonth.SelectedValue; }
    }

    public string SupplierIDValue
    {
        set
        {
            SupplierID.Enabled = false;
            SupplierID.Text = value;
            txtChanged.Value = "1";
            if (value != "")
            {
                DataTable tbl = SupplierDataAccess.SelectSupplier(Convert.ToInt32(value));
                if (tbl.Rows.Count > 0) { SupplierName.Text = tbl.Rows[0]["SupplierName"].ToString(); }
            }
        }
    }

    public string SpendTypeIDValue
    {
        set { SpendTypeID.SelectedValue = value; }
        get { return SpendTypeID.SelectedValue; }
    }

    public string ActivityTypeIDValue
    {
        get
        {
            return ActivityTypeID.Text;
        }
        set
        {
            ActivityTypeID.Enabled = false;
            ActivityTypeID.Text = value;
            txtChanged.Value = "1";
            if (value != "")
            {
                ActivityType a = ActivityTypeDataAccess.SelectActivityType(Convert.ToInt32(value));
                ActivityTypeName.Text = a.ActivityTypeName;
            }
        }
    }

    public bool SpendTypeIDEnabled
    {
        set
        {
            SpendTypeID.Enabled = value;
        }
    }

    public string POIDText
    {
        get { return POID.Text; } 
    }

    public string RebateIDText
    {
        get { return RebateID.Text; }
    }

    public Activity SelectActivity
    //create an Activity object from the values in the controls on the page
    {
        get {
            Activity a = new Activity();
            a.ActivityID = Convert.ToInt32(ActivityID.Text);
            a.ProjectToCustomerID = Convert.ToInt32(Session["SelectedProjectToCustomerID"]);
            a.ActivityName = ActivityName.Text;
            a.ShortText = ShortText.Text;
            a.ItemQuantity = Convert.ToInt32(ItemQuantity.Text);
            a.UnitPrice = Decimal.Parse(UnitPrice.Text);
            a.ProjectMonth = ProjectMonth.SelectedValue;
            a.MonthRequired = ProjectMonth.SelectedValue;
            if (ProjectMonth.SelectedValue != "13")
            {
                a.MonthShortName = ProjectMonth.SelectedItem.ToString();
            }
            a.POID = POID.Text;
            a.RebateID = RebateID.Text;
            a.DocumentNumber = DocumentNumber.Text;
            a.ActivityTypeID = Convert.ToInt32(ActivityTypeID.Text);
            a.ActivityTypeName = ActivityTypeName.Text;
            a.SpendTypeID = int.Parse(SpendTypeID.SelectedValue);
            if (SupplierID.Text == String.Empty) { a.SupplierID = String.Empty; } else { a.SupplierID = SupplierID.Text; }
            a.SupplierRefNo = SupplierRefNo.Text;
            a.Comments = Comments.Text;
            a.LastUpdatedBy = Session["CurrentUserName"].ToString();
            a.CreatedBy = Session["CurrentUserName"].ToString();
            if (Month01.Text != "") { a.Month01 = Convert.ToInt32(Convert.ToDecimal(Month01.Text)).ToString(); } else { a.Month01 = ""; }
            if (Month02.Text != "") { a.Month02 = Convert.ToInt32(Convert.ToDecimal(Month02.Text)).ToString(); } else { a.Month02 = ""; }
            if (Month03.Text != "") { a.Month03 = Convert.ToInt32(Convert.ToDecimal(Month03.Text)).ToString(); } else { a.Month03 = ""; }
            if (Month04.Text != "") { a.Month04 = Convert.ToInt32(Convert.ToDecimal(Month04.Text)).ToString(); } else { a.Month04 = ""; }
            if (Month05.Text != "") { a.Month05 = Convert.ToInt32(Convert.ToDecimal(Month05.Text)).ToString(); } else { a.Month05 = ""; }
            if (Month06.Text != "") { a.Month06 = Convert.ToInt32(Convert.ToDecimal(Month06.Text)).ToString(); } else { a.Month06 = ""; }
            if (Month07.Text != "") { a.Month07 = Convert.ToInt32(Convert.ToDecimal(Month07.Text)).ToString(); } else { a.Month07 = ""; }
            if (Month08.Text != "") { a.Month08 = Convert.ToInt32(Convert.ToDecimal(Month08.Text)).ToString(); } else { a.Month08 = ""; }
            if (Month09.Text != "") { a.Month09 = Convert.ToInt32(Convert.ToDecimal(Month09.Text)).ToString(); } else { a.Month09 = ""; }
            if (Month10.Text != "") { a.Month10 = Convert.ToInt32(Convert.ToDecimal(Month10.Text)).ToString(); } else { a.Month10 = ""; }
            if (Month11.Text != "") { a.Month11 = Convert.ToInt32(Convert.ToDecimal(Month11.Text)).ToString(); } else { a.Month11 = ""; }
            if (Month12.Text != "") { a.Month12 = Convert.ToInt32(Convert.ToDecimal(Month12.Text)).ToString(); } else { a.Month12 = ""; }

            return a;
        }
    }

#endregion

#region methods

    public void PopulateForm(Activity a)
    //prepare form for editing an existing record
    {
        //enable controls unless read only - amended 05/12/12 to check user's permissions here too
        if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 35 && Common.CheckAuthorisations(Convert.ToInt32(Session["SelectedProjectToCustomerID"]), Session["CurrentUserName"].ToString()) > 0)
        {
            ToggleControlState(true);
        }

        
        //if Coupons, enable Document number field - updated 19/12/12
        if (a.SpendTypeID == 3) { DocumentNumber.Enabled = true; } else { DocumentNumber.Enabled = false; }
        //populate fields
        ActivityID.Text = a.ActivityID.ToString();
        ActivityName.Text = a.ActivityName;
        ShortText.Text = a.ShortText;
        ItemQuantity.Text = a.ItemQuantity.ToString();
        UnitPrice.Text = String.Format("{0:0.00}", a.UnitPrice);
        ProjectMonth.SelectedValue = a.ProjectMonth;
        POID.Text = a.POID;
        RebateID.Text = a.RebateID;
        DocumentNumber.Text = a.DocumentNumber;
        BudgetResponsibilityAreaID.SelectedValue = a.BudgetResponsibilityAreaID;
        SpendTypeID.SelectedValue = a.SpendTypeID.ToString();
        //fill the ActivityType dependant on the SpendType and the BRA; include inactive records; amended 05/03/12 to correct ActivityTypes issue ***remmed out 22/02/13 for new Activity Type (Multi)View ***
        //FillActivityType(a.SpendTypeID, Convert.ToInt32(a.BudgetResponsibilityAreaID), 0);
        ActivityTypeID.Text = a.ActivityTypeID.ToString();
        ActivityTypeName.Text = a.ActivityTypeName;
        SupplierID.Text = a.SupplierID;
        if (a.SupplierID != "")
        { SupplierName.Text = Common.ADOLookup("SupplierName", "tblSupplier", "SupplierID = " + a.SupplierID); }
        else { SupplierName.Text = ""; }
        SupplierRefNo.Text = a.SupplierRefNo;
        Comments.Text = a.Comments;
        Month01.Text = a.Month01;
        Month02.Text = a.Month02;
        Month03.Text = a.Month03;
        Month04.Text = a.Month04;
        Month05.Text = a.Month05;
        Month06.Text = a.Month06;
        Month07.Text = a.Month07;
        Month08.Text = a.Month08;
        Month09.Text = a.Month09;
        Month10.Text = a.Month10;
        Month11.Text = a.Month11;
        Month12.Text = a.Month12;

        txtChanged.Value = "0";
        Session["Mode"] = "Edit";
    }

    public void UpdateActivity()
    {
        if (Page.IsValid)
        {
            double dblOldValue = 0;
            double dblOldPOValue = 0;
            //if Spend Type is A&P and PONumber already allocated, get current value of activity and PO - before they are updated
            if (DocumentNumber.Text != String.Empty && SpendTypeID.SelectedValue == "1")
            {
                DataTable tblActivity = ActivityDataAccess.SelectActivityToTable(Convert.ToInt32(ActivityID.Text));
                dblOldValue = Convert.ToDouble(tblActivity.Rows[0]["UnitPrice"]) * Convert.ToInt32(tblActivity.Rows[0]["ItemQuantity"]);
                dblOldPOValue = Convert.ToDouble(POHeaderDataAccess.GetPOValue(Convert.ToInt32(POID.Text)));
            }
            //update activity
            int intRowsAffected = ActivityDataAccess.UpdateActivity(SelectActivity, 1);
            //if Spend Type is A&P, POValue has changed and PONumber already allocated, create a PO change record
            //***want to move to SP***
            if (DocumentNumber.Text != String.Empty && SpendTypeID.SelectedValue == "1")
            {
                //get new value of activity
                double dblNewValue = Convert.ToDouble(UnitPrice.Text) * Convert.ToInt32(ItemQuantity.Text);
                //insert POChange record
                if (dblOldValue != dblNewValue) 
                {
                    //get new value of PO
                    double dblChange = dblNewValue - dblOldValue;
                    double dblNewPOValue = dblOldPOValue + dblChange;
                    POHeaderDataAccess.InsertPOChange(Convert.ToInt32(POID.Text), dblOldPOValue, dblNewPOValue, Session["CurrentUserName"].ToString());
                }                
            }
            ClearForm();
        }
    }

    public void CreateActivity()
    {
        if (Page.IsValid)
        {
            ActivityDataAccess.InsertActivity(SelectActivity);
            ClearForm();
        }
    }

    protected void ToggleControlState(bool blnState)
    //enable/disable controls
    {
        foreach (Control c in Controls)
        {
            if (c != ActivityID && c != POID && c != SpendTypeID && c != DocumentNumber && c != BudgetResponsibilityAreaID && c != ActivityTypeName && c != SupplierName)
            {
                if (c is TextBox) { ((TextBox)c).Enabled = blnState; } else { if (c is DropDownList) { ((DropDownList)c).Enabled = blnState; } }
            }
        }
        //disable SpendType in case mode is "New"
        if (blnState == false) { SpendTypeID.Enabled = false; }
        //required to ensure "readers" cannot change phased values
        Month01.Enabled = blnState;
        Month02.Enabled = blnState;
        Month03.Enabled = blnState;
        Month04.Enabled = blnState;
        Month05.Enabled = blnState;
        Month06.Enabled = blnState;
        Month07.Enabled = blnState;
        Month08.Enabled = blnState;
        Month09.Enabled = blnState;
        Month10.Enabled = blnState;
        Month11.Enabled = blnState;
        Month12.Enabled = blnState;
    }

    protected void ClearControls()
    {
        foreach (Control c in Controls)
        {
            if (c is TextBox) { ((TextBox)c).Text = ""; } 
            if (c is DropDownList) { ((DropDownList)c).SelectedValue = "0"; }    
        }
        //data labels
        SupplierID.Text = String.Empty;
        SupplierName.Text = String.Empty;
        ActivityTypeName.Text = String.Empty;
        //these are not at the top level so need clearing explicitly 
        Month01.Text = "";
        Month02.Text = "";
        Month03.Text = "";
        Month04.Text = "";
        Month05.Text = "";
        Month06.Text = "";
        Month07.Text = "";
        Month08.Text = "";
        Month09.Text = "";
        Month10.Text = "";
        Month11.Text = "";
        Month12.Text = "";
    }

    public void ClearForm()
    {
        ClearControls();
        ToggleControlState(false);
        //tblMonthlyBreakdown.Visible = false;
        ActivityName.Focus();
    }

    public void NewRecord()
    //prepare form for new record entry
    {
        ClearControls();
        ToggleControlState(true);
        ActivityID.Text = "0";
    }

    protected int SumMonthlyBreakdown()
    //sum the values in the month boxes allowing for blanks
    {
        int intTotal = 0;
        if (Month01.Text != "")
        {
            intTotal += Convert.ToInt32(Convert.ToDecimal(Month01.Text));
        }
        if (Month02.Text != "")
        {
            intTotal += Convert.ToInt32(Convert.ToDecimal(Month02.Text));
        }
        if (Month03.Text != "")
        {
            intTotal += Convert.ToInt32(Convert.ToDecimal(Month03.Text));
        }
        if (Month04.Text != "")
        {
            intTotal += Convert.ToInt32(Convert.ToDecimal(Month04.Text));
        }
        if (Month05.Text != "")
        {
            intTotal += Convert.ToInt32(Convert.ToDecimal(Month05.Text));
        }
        if (Month06.Text != "")
        {
            intTotal += Convert.ToInt32(Convert.ToDecimal(Month06.Text));
        }
        if (Month07.Text != "")
        {
            intTotal += Convert.ToInt32(Convert.ToDecimal(Month07.Text));
        }
        if (Month08.Text != "")
        {
            intTotal += Convert.ToInt32(Convert.ToDecimal(Month08.Text));
        }
        if (Month09.Text != "")
        {
            intTotal += Convert.ToInt32(Convert.ToDecimal(Month09.Text));
        }
        if (Month10.Text != "")
        {
            intTotal += Convert.ToInt32(Convert.ToDecimal(Month10.Text));
        }
        if (Month11.Text != "")
        {
            intTotal += Convert.ToInt32(Convert.ToDecimal(Month11.Text));
        }
        if (Month12.Text != "")
        {
            intTotal += Convert.ToInt32(Convert.ToDecimal(Month12.Text));
        }
        return intTotal;
    }

#endregion

#region dropdowns

    public DataTable SelectActivityTypes(int intBudgetResponsibilityAreaID)
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "SelectActivityTypes";
        cmd.Parameters.AddWithValue("@BudgetResponsibilityAreaID", intBudgetResponsibilityAreaID);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    protected void FillSpendType()
    //populate the SpendTypeID dropdownlist
    {
        DataTable tbl = Common.FillDropDown("FillSpendType");
        DropDownList d = SpendTypeID;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillMonth()
    //fill months dropdownlist
    {
        //Get a data table object containing the values
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillMonth";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = ProjectMonth;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillBudgetResponsibilityArea()
    //fill BudgetResponsibilityArea dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillBudgetResponsibilityArea";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = BudgetResponsibilityAreaID;
        d.DataSource = tbl;
        d.DataBind();
    }

#endregion

}
