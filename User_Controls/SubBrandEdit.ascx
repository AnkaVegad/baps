﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SubBrandEdit.ascx.cs" Inherits="User_Controls_SubBrandEdit" %>

    <!--SubBrand Edit Area-->
    <table width="1100px">
    <tr><td style="font-size:2px">&nbsp;</td></tr>
    <tr>
        <td>
            <asp:Label ID="lblHeader" runat="server" CssClass="page_header"></asp:Label><br />
        </td>
    </tr><tr><td style="font-size:2px">&nbsp;</td></tr>
    </table>

    <div id="project_edit_area">
        <table border="0" width="500px">
            <tr>
                <td>Brand-Market Name</td>
                <td>
                    <asp:TextBox ID="SubBrandID" runat="server" Visible="False"></asp:TextBox>
                    <asp:TextBox ID="SubBrandName" runat="server" Width="280px" MaxLength="40" 
                        Enabled="False"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ErrorMessage="*Required" ControlToValidate="SubBrandName" 
                        Display="Dynamic" ValidationGroup="SubBrand"></asp:RequiredFieldValidator>
                </td>
            </tr>
        </table>
        <table width="680px">
            <!--Allocation entry area-->
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr><td colspan="2">Allocation To PPH L2</td></tr>
            <tr><td colspan="2">
                <asp:GridView ID="grdSubBrandID" runat="server" width="430px"
                    AutoGenerateColumns="False" GridLines="Vertical" RowStyle-Wrap="false" EmptyDataText="[Not Allocated]" 
                    HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false" SelectedRowStyle-Wrap="false"
                    OnRowDataBound="grdSubBrandID_RowDataBound" DataKeyNames="PPHL2" ShowFooter="false">
                    <Columns>
                        <asp:BoundField DataField="PPHL2" HeaderText="PPH L2" SortExpression="PPHL2" />
                        <asp:BoundField DataField="VTEXT" HeaderText="Description" SortExpression="VTEXT" />
                    </Columns>
                    <RowStyle CssClass="grid_alternating_row" Wrap="False" />
                    <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
                    <HeaderStyle CssClass="grid_header" Wrap="False" />
                </asp:GridView>
            </td></tr><tr><td colspan="2" style="font-size:2px">&nbsp;</td></tr><tr><td>
                <asp:Button ID="cmdRemoveSubBrand" runat="server" Text="Remove" causesvalidation="false"
                    onclick="cmdRemoveSubBrand_Click" CssClass="form_button" Enabled="false" />
            </td></tr>
                
            <tr><td colspan="2">PPH L2&nbsp;
                <asp:DropDownList ID="cboPPHL2" runat="server" Width="400px" Enabled="false"
                    DataTextField="EntityName" DataValueField="ID" />
                <asp:Button ID="cmdAddSubBrand" runat="server" Text="Add" causesvalidation="true"
                    onclick="cmdAddSubBrand_Click" CssClass="form_button" Enabled="false" Width="60px" ValidationGroup="SubBrand" />
            </td></tr>

        </table>
    </div>
    <br />
