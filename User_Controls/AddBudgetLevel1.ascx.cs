﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class User_Controls_AddBudgetLevel1 : System.Web.UI.UserControl
{
    String Conn = APConfiguration.DbConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            
            cmdAddBudget.Enabled = true;
            FillYear();
            FillSpendType();
            FillGLLevel1();
            //FillGLLevel2();
            FillCentralBudgetIndicator();
            FillDeparmentCode();
        }
    }

    protected void FillYear()
    //fill year dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillYear";
        cmd.Parameters.AddWithValue("@ZeroValueText", "[None]");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = BudgetYear;
        d.DataSource = tbl;
        d.DataBind();
    }

    public void FillSpendType()
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillSpendType";
        cmd.Parameters.AddWithValue("@ZeroValueText", "[None]");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = SpendType;
        d.DataSource = tbl;
        d.DataBind();
    }

    public void FillGLLevel1()
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillGLLevel1";
        cmd.Parameters.AddWithValue("@ZeroValueText", "[None]");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        GLLevel1.DataSource = tbl;
        GLLevel1.DataTextField = "EntityName";
        GLLevel1.DataValueField = "ID";
        GLLevel1.DataBind();
        GLLevel2.Enabled = false;
        GLLevel2.Items.Insert(0, new ListItem("Select GLLevel1", "0"));
        

        //BindDropDownList(GLLevel1, tbl, "CountryName", "CountryId", "Select Country");
        //DropDownList d = GLLevel1;
        //d.DataSource = tbl;
        //d.DataBind();
    }

    public void FillGLLevel2()
    {
        
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillGLLevel2";
        cmd.Parameters.AddWithValue("@ZeroValueText", "[None]");
        cmd.Parameters.AddWithValue("@GLLevel1ID", Convert.ToInt32(GLLevel1.SelectedValue.ToString()));
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        GLLevel2.DataSource = tbl;
        GLLevel2.DataTextField = "EntityName";
        GLLevel2.DataValueField = "ID";
        GLLevel2.DataBind();
    }

    protected void GLLevel1_Changed(object sender, EventArgs e)
    {
        GLLevel2.Enabled = false;
        GLLevel2.Items.Clear();
        GLLevel2.Items.Insert(0, new ListItem("Select State", "0"));

        int GLLevelID = int.Parse(GLLevel1.SelectedItem.Value);
        if (GLLevelID > 0)
        {
            FillGLLevel2();
            GLLevel2.Enabled = true;
        }
    }

    public void FillCentralBudgetIndicator()
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillCentralBudgetIndicator";
        cmd.Parameters.AddWithValue("@ZeroValueText", "[None]");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = CentralBudgetIndicator;
        d.DataSource = tbl;
        d.DataBind();
    }

    public void FillDeparmentCode()
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillDepartmentCode";
        cmd.Parameters.AddWithValue("@ZeroValueText", "[None]");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = DepartmentCode;
        d.DataSource = tbl;
        d.DataBind();
    }


    protected void cmdAddBudget_Click(object sender, EventArgs e)
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "InsertBudget";
        cmd.Parameters.AddWithValue("@BudgetYear", BudgetYear.SelectedValue);
        cmd.Parameters.AddWithValue("@CurrencyCode", "EUR");
        cmd.Parameters.AddWithValue("@SpendTypeID", Convert.ToInt32(SpendType.SelectedValue));
        cmd.Parameters.AddWithValue("@BudgetLevelID", 1);
        cmd.Parameters.AddWithValue("@BudgetResponsibilityAreaID", DBNull.Value);
        cmd.Parameters.AddWithValue("@CategoryID", DBNull.Value);
        cmd.Parameters.AddWithValue("@BrandID", DBNull.Value);
        cmd.Parameters.AddWithValue("@GLLevel1ID", Convert.ToInt32(GLLevel1.SelectedValue));
        cmd.Parameters.AddWithValue("@GLLevel2ID", Convert.ToInt32(GLLevel2.SelectedValue));
        cmd.Parameters.AddWithValue("@DepartmentCode", DepartmentCode.SelectedValue);
        cmd.Parameters.AddWithValue("@CentralBudgetIndicator",Convert.ToByte(CentralBudgetIndicator.SelectedValue) );
        cmd.Parameters.AddWithValue("@ProjectToCustomerID", DBNull.Value);
        cmd.Parameters.AddWithValue("@BudgetValue", Convert.ToInt32(BudgetValue.Text));
        DataAccess.ExecuteSelectCommand(cmd);
        

    }

    public void  DeleteBudget(int BudgetId)
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "DeleteBudget";
        cmd.Parameters.AddWithValue("BudgetID", BudgetId);
        DataAccess.ExecuteSelectCommand(cmd);
    }



}