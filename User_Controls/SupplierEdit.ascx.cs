﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Controls_SupplierEdit : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //populate the activity indicator dropdown
            ActiveIndicator.Items.Add(new ListItem("", "0"));
            ActiveIndicator.Items.Add(new ListItem("Active", "1"));
            ActiveIndicator.Items.Add(new ListItem("Inactive", "2"));
        }
    }

#region properties

    public string HeaderText
    {
        set { lblHeader.Text = value; }
    }

#endregion

#region methods

    public void PopulateForm(int intSupplierID)
    {
        SupplierID.Text = intSupplierID.ToString();
        DataTable tbl = SupplierDataAccess.SelectSupplier(intSupplierID);
        DataRow r = tbl.Rows[0];
        //autopopulate text boxes except for comparators
        foreach (Control c in Controls)
        {
            if (c is TextBox)
            {
                ((TextBox)c).Text = r[c.ID].ToString();
            }
        }
        //drop down
        ActiveIndicator.SelectedValue = r["ActiveIndicator"].ToString();
        SupplierName.Focus();
    }

    public void ClearControls()
    {
        foreach (Control c in Controls)
        {
            if (c is TextBox) { ((TextBox)c).Text = ""; }
            if (c is DropDownList) { ((DropDownList)c).SelectedValue = "0"; }
        }
    }

    public void NewRecord()
    {
        ClearControls();
        SupplierID.Text = "0";
        SupplierName.Focus();
    }

    public int CreateRecord()
    {
        int intNewID = SupplierDataAccess.InsertSupplier(SupplierName.Text, SAPVendorCode.Text, ActiveIndicator.SelectedValue, Comments.Text, Session["CurrentUserName"].ToString());
        ClearControls();
        return intNewID;
    }

    public int UpdateRecord()
    {
        int intResult = SupplierDataAccess.UpdateSupplier(Convert.ToInt32(SupplierID.Text), SupplierName.Text, SAPVendorCode.Text, ActiveIndicator.SelectedValue, Comments.Text, Session["CurrentUserName"].ToString());
        ClearControls();
        return intResult;
    }

#endregion

#region dropdowns
    

#endregion
}
