﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PopupCalendar.ascx.cs" Inherits="User_Controls_PopupCalendar" %>

<asp:TextBox ID="txtValue" runat="server" Width="75px" MaxLength="10" 
    Enabled="False" />
<asp:ImageButton ID="imgCalendar" runat="server" 
    ImageUrl="~/Images/Calendar.bmp" onclick="imgCalendar_Click" />
<asp:Panel ID="divCalPopup" runat="server" style="position: absolute; z-index: 20;" Visible="False">
    <asp:Calendar ID="calPopup" runat="server" 
        onselectionchanged="calPopup_SelectionChanged"></asp:Calendar>
</asp:Panel>
<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
    ControlToValidate="txtValue" ValidationGroup="CustomerCycle"
    Display="Dynamic" ErrorMessage="*Required" />
    