﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NewBudgetGrid.ascx.cs" Inherits="User_Controls_NewBudgetGrid" %>
<script type="text/javascript" src="/Scripts/BAPS.js"></script>

<!--Header and Filter Criteria-->
<div id="parent">
    <!--Header and Filter Criteria-->
    <div id="resizable1" class="resizable ui-widget-content" style="height:101.5%">
        <br />
        <asp:Label ID="lblHeaderText" Text="Budgets" runat="server" CssClass="page_header"></asp:Label>
        <br />
        <br />
        <table style="width:100%" border="0">
            <tr id="trBudgetLevel">
                <td>Budget Level</td>
                <td>
                    <asp:DropDownList id="cboBudgetLevel" runat="server" ontextchanged="cboBudgetLevel_TextChanged" width="170px"
                        DataTextField="EntityName" DataValueField="ID" AutoPostBack="true" forecolor="Black"/>
                </td>
            </tr>
            <tr id="trYear">
                <td>Year</td>
                <td>
                    <asp:DropDownList id="cboYear" runat="server" ontextchanged="cboYear_TextChanged" AutoPostBack="true" 
                        DataTextField="EntityName" DataValueField="ID" forecolor="Black"/>
                </td>
            </tr>   
             <tr id="trCategory" style="display:none">
                <td>Category</td>
                <td>
                    <asp:DropDownList id="cboCategory" runat="server" ontextchanged="cboCategory_TextChanged" width="170px" AutoPostBack="true" 
                        DataTextField="EntityName" DataValueField="ID" forecolor="Black"/>
                </td>
            </tr>   
            <tr id="trBrand" style="display:none">
                <td>Brand</td>
                <td>
                    <asp:DropDownList id="cboBrand" runat="server" ontextchanged="cboBrand_TextChanged" width="170px" AutoPostBack="true" 
                        DataTextField="EntityName" DataValueField="ID" forecolor="Black"/>
                </td>
            </tr>  
            <tr id="trBudgetResponsbilityAera" style="display:none">
                <td>Area</td>
                <td>
                    <asp:DropDownList id="cboBudgetResponsibilityArea" runat="server" ontextchanged="cboBudgetResponsibilityArea_TextChanged" width="170px" AutoPostBack="true" 
                        DataTextField="EntityName" DataValueField="ID" forecolor="Black"/>
                </td>
            </tr>   
            <tr id="trSpendType" style="display:none">
                <td>Spend Type</td>
                <td>
                    <asp:DropDownList id="cboSpendType" runat="server" ontextchanged="cboSpendType_TextChanged" width="170px" AutoPostBack="true" 
                        DataTextField="EntityName" DataValueField="ID" forecolor="Black"/>
                </td>
            </tr>   
            <tr id="trCustomer" style="display:none">
                <td>Customer</td>
                <td>
                    <asp:DropDownList id="cboCustomer" runat="server" ontextchanged="cboCustomer_TextChanged" width="170px" AutoPostBack="true" 
                        DataTextField="EntityName" DataValueField="ID" forecolor="Black"/>
                </td>
            </tr>  
        </table><br /> 
        <div ID="trContact" style="padding-left:20px;padding-top:50px">
                    <asp:Label ID="lblContact" runat="server" Width="270px"/>
        </div>
    </div>

    <!--Search and Grid-->
    <div id="resizable2" class="ui-widget-content" style="height:101.5%">

        <!--Data Source - required to take advantage of sorting and paging functionality of the GridView' parameter removed 28/02/14 to solve filter problem with SQLDataSource when >101 records-->
        <asp:SqlDataSource ID="sds" runat="server" 
            ConnectionString="<%$ ConnectionStrings:APConnectionString %>">
             <UpdateParameters>
                 <asp:Parameter Type="Int32" 
                  Name="Target"></asp:Parameter>
            </UpdateParameters>
        </asp:SqlDataSource>

        <div id="Budget_Level1_grid" class="budgetgrid" style="padding-top:10px; padding-right:10px">
            <asp:GridView ID="grdBudgetL1" runat="server" Width ="100%"  
                AllowSorting="false" AutoGenerateColumns="False"  RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                EmptyDataText="No Records Found" EnableModelValidation="True" GridLines="None" AlternatingRowStyle-Wrap="False"
                OnSelectedIndexChanged="grdBudgetL1_SelectedIndexChanged" SelectedRowStyle-Wrap="false" DataKeyNames="BudgetID"
                OnRowDataBound="grdBudgetL1_RowDataBound" CssClass="budgettable"
                OnRowUpdating="grdBudgetL1_RowUpdating" OnRowEditing="grdBudgetL1_RowEditing"  OnRowCancelingEdit="grdBudgetL1_RowCancelingEdit"
                ShowFooter="False"  >
                <HeaderStyle BackColor="Gray" ForeColor="White" Wrap="False" />
                <AlternatingRowStyle Wrap="False"></AlternatingRowStyle>
                 <Columns>
                                <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="BudgetID" ReadOnly="True"  Visible="false" />
                                <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="Spend Type/GL Hierarchy Level 1" HeaderText="SpendType\GL Hierarchy Level 1" SortExpression="SpendType\GL Hierarchy Level 1" ReadOnly="True" />
                                <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="GL Hierarchy Level 2" HeaderText="GL Hierarchy Level 2" SortExpression="GL Hierarchy Level 2" ReadOnly="True" />
                                <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="Currency" HeaderText="Currency" SortExpression="Currency" ReadOnly="True"/>
                                <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="Target" HeaderText="Target" 
                                    SortExpression="Target"   ApplyFormatInEditMode ="true" ControlStyle-Width ="99px"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N0}">
                                </asp:BoundField>
                                 <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="ColourCoding" Visible="true"  ReadOnly="True"/>
                                <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="CssTag" Visible="true" ReadOnly="True"/>
                                <asp:CommandField  HeaderStyle-CssClass="GridHeader" HeaderText="Actions" ShowEditButton="True"/>
                                
                </Columns>
                
                <EditRowStyle BackColor="#6699FF" />
                
            </asp:GridView>
            </div>

         <div id="Budget_Level2_grid" class="budgetgrid" style="padding-top:10px; padding-right:10px">
            <asp:GridView ID="grdBudgetL2" runat="server" Width ="100%" 
                AllowSorting="false" AutoGenerateColumns="False"  RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                EmptyDataText="No Records Found" EnableModelValidation="True" GridLines="None" AlternatingRowStyle-Wrap="False"
                OnSelectedIndexChanged="grdBudgetL2_SelectedIndexChanged" SelectedRowStyle-Wrap="false" DataKeyNames="BudgetID"
                OnRowDataBound="grdBudgetL2_RowDataBound" CssClass="budgettable"
                OnRowUpdating="grdBudgetL2_RowUpdating" OnRowEditing="grdBudgetL2_RowEditing"  OnRowCancelingEdit="grdBudgetL2_RowCancelingEdit"
                ShowFooter="False"  >
                <HeaderStyle BackColor="Gray" CssClass="grid_header" ForeColor="White" Wrap="False" />
                <AlternatingRowStyle Wrap="False"></AlternatingRowStyle>
                 <Columns>
                                <asp:BoundField DataField="BudgetID" ReadOnly="True"  Visible="false" />
                                <asp:BoundField DataField="Spend Type/GL Hierarchy Level 1" HeaderText="SpendType\GL Hierarchy Level 1" SortExpression="SpendType\GL Hierarchy Level 1" ReadOnly="True" HeaderStyle-CssClass="GridHeader"/>
                                <asp:BoundField DataField="GL Hierarchy Level 2" HeaderText="GL Hierarchy Level 2" SortExpression="GL Hierarchy Level 2" ReadOnly="True" HeaderStyle-CssClass="GridHeader"/>
                                <asp:BoundField DataField="Currency" HeaderText="Currency" SortExpression="Currency" ReadOnly="True" HeaderStyle-CssClass="GridHeader"/>
                                <asp:BoundField DataField="Target" HeaderText="Target" 
                                    SortExpression="Target" ApplyFormatInEditMode ="true" ControlStyle-Width ="99px"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N0}" HeaderStyle-CssClass="GridHeader">
                                </asp:BoundField>
                                 <asp:BoundField DataField="ColourCoding" Visible="true"  ReadOnly="True" HeaderStyle-CssClass="GridHeader"/>
                                <asp:BoundField DataField="CssTag" Visible="true" ReadOnly="True" HeaderStyle-CssClass="GridHeader"/>
                                <asp:CommandField  HeaderText="Actions" ShowEditButton="True" HeaderStyle-CssClass="GridHeader"/>
                                
                </Columns>
                
                <EditRowStyle BackColor="#6699FF" />
            </asp:GridView>
            </div>

            <div id="Budget_Level3_grid" class="budgetgrid" style="padding-top:10px; padding-right:10px">
            <asp:GridView ID="grdBudgetL3" runat="server" Width ="100%" 
                AllowSorting="false" AutoGenerateColumns="False"  RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                EmptyDataText="No Records Found" EnableModelValidation="True" GridLines="None" AlternatingRowStyle-Wrap="False"
                OnSelectedIndexChanged="grdBudgetL3_SelectedIndexChanged" SelectedRowStyle-Wrap="false" DataKeyNames="BudgetID"
                OnRowDataBound="grdBudgetL3_RowDataBound" CssClass="budgettable"
                OnRowUpdating="grdBudgetL3_RowUpdating" OnRowEditing="grdBudgetL3_RowEditing"  OnRowCancelingEdit="grdBudgetL3_RowCancelingEdit"
                ShowFooter="False"  >
                <AlternatingRowStyle Wrap="False"></AlternatingRowStyle>
                 <Columns>
                                <asp:BoundField DataField="BudgetID" ReadOnly="True"  Visible="false" />  
                                <asp:BoundField DataField="Brand" HeaderText="Brand" SortExpression="Brand" ReadOnly="True" HeaderStyle-CssClass="GridHeader"/>
                                <asp:BoundField DataField="Currency" HeaderText="Currency" SortExpression="Currency" ReadOnly="True" HeaderStyle-CssClass="GridHeader"/>
                                <asp:BoundField DataField="Target" HeaderText="Target" 
                                    SortExpression="Target"  ApplyFormatInEditMode ="true" ControlStyle-Width ="99px"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N0}"  HeaderStyle-CssClass="GridHeader">
                                </asp:BoundField>
                                 <asp:BoundField DataField="ColourCoding" Visible="true"  ReadOnly="True" HeaderStyle-CssClass="GridHeader"/>
                                <asp:BoundField DataField="CssTag" Visible="true" ReadOnly="True" HeaderStyle-CssClass="GridHeader"/>
                                <asp:CommandField  HeaderText="Actions" ShowEditButton="True" HeaderStyle-CssClass="GridHeader"/>
                                
                </Columns>
                
                <EditRowStyle BackColor="#6699FF" />
                <HeaderStyle BackColor="Gray" CssClass="grid_header" ForeColor="White" Wrap="False" />
            </asp:GridView>
            </div>

            <div id="Budget_Level4_grid" class="budgetgrid" style="padding-top:10px; padding-right:10px">
            <asp:GridView ID="grdBudgetL4" runat="server" Width ="100%" 
                AllowSorting="false" AutoGenerateColumns="False"  RowStyle-Wrap="false" HeaderStyle-Wrap="false"
                EmptyDataText="No Records Found" EnableModelValidation="True" GridLines="None" AlternatingRowStyle-Wrap="False"
                OnSelectedIndexChanged="grdBudgetL4_SelectedIndexChanged" SelectedRowStyle-Wrap="false" DataKeyNames="BudgetID"
                OnRowDataBound="grdBudgetL4_RowDataBound" CssClass="budgettable"
                OnRowUpdating="grdBudgetL4_RowUpdating" OnRowEditing="grdBudgetL4_RowEditing"  OnRowCancelingEdit="grdBudgetL4_RowCancelingEdit"
                ShowFooter="False"  >
                <AlternatingRowStyle Wrap="False"></AlternatingRowStyle>
                 <Columns>
                                <asp:BoundField DataField="BudgetID" ReadOnly="True"  Visible="false" />  
                                <asp:BoundField DataField="Project" HeaderText="Project" SortExpression="Project" ReadOnly="True" HeaderStyle-CssClass="GridHeader" />
                                <asp:BoundField DataField="Customer" HeaderText="Customer" SortExpression="Customer" ReadOnly="True" HeaderStyle-CssClass="GridHeader"/>
                                <asp:BoundField DataField="Spend Type" HeaderText="SpendType" SortExpression="SpendType" ReadOnly="True" HeaderStyle-CssClass="GridHeader"/>
                                <asp:BoundField DataField="Currency" HeaderText="Currency" SortExpression="Currency" ReadOnly="True" HeaderStyle-CssClass="GridHeader"/>
                                <asp:BoundField DataField="Target" HeaderText="Target" 
                                    SortExpression="Target" ApplyFormatInEditMode ="true" ControlStyle-Width ="99px"
                                    ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N0}"  HeaderStyle-CssClass="GridHeader">
                                </asp:BoundField>
                                <asp:BoundField DataField="ColourCoding" Visible="true"  ReadOnly="True" HeaderStyle-CssClass="GridHeader"/>
                                <asp:BoundField DataField="CssTag" Visible="true" ReadOnly="True" HeaderStyle-CssClass="GridHeader"/>
                                <asp:CommandField  HeaderText="Actions" ShowEditButton="True" HeaderStyle-CssClass="GridHeader"/>
                                
                </Columns>
                
                <EditRowStyle BackColor="#6699FF" />
                <HeaderStyle BackColor="Gray" CssClass="grid_header" ForeColor="White" Wrap="False" />
            </asp:GridView>
            </div>
    </div>
 </div>



