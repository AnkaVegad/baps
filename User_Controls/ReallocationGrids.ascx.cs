﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Controls_ReallocationGrids : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void RefreshGrids(int intProjectToCustomerID)
    {
        //populate grids
        grdReallocationFrom.DataSource = ReallocationDataAccess.SelectReallocationFrom(intProjectToCustomerID);
        grdReallocationFrom.DataBind();
        grdReallocationTo.DataSource = ReallocationDataAccess.SelectReallocationTo(intProjectToCustomerID);
        grdReallocationTo.DataBind();
    }
}
