﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SupplierGrid.ascx.cs" Inherits="User_Controls_SupplierGrid" %>

<asp:Panel ID="Panel1" runat="server" DefaultButton="cmdGo">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <!--Page Header-->
            <table border="0" style="width:400px">
                <tr>
                    <td style="text-align:left">
                        <asp:Label ID="lblHeader" Text="Select Supplier" runat="server" CssClass="page_header" />
                    </td>
                    <td style="text-align:right">
                        <asp:Label ID="lblSearch" Text="Search:" runat="server"></asp:Label>
                        <asp:TextBox ID="txtSearch" Runat="server" Width="120px"></asp:TextBox>
                        <asp:Button ID="cmdGo" Text="Go" Runat="server" CausesValidation="false" AccessKey="g" OnClick="cmdGo_Click"/>
                    </td>
                </tr>
                <tr><td style="font-size:3px">&nbsp;</td></tr>
            </table>

            <!--Data Source - required to take advantage of sorting and paging functionality of the GridView-->
            <asp:SqlDataSource ID="sds" runat="server" 
                ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
                FilterExpression="SupplierName Like '%{0}%' OR SAPVendorCode Like '%{0}%'" >
                <FilterParameters>
                    <asp:ControlParameter ControlID="txtSearch" PropertyName="Text" />
                </FilterParameters>
            </asp:SqlDataSource>

            <!--Activity Detail Grid-->
            <div id="project_grid">
                <asp:GridView ID="grd" runat="server" width="400px"
                    AutoGenerateColumns="False" GridLines="Vertical"
                    RowStyle-Wrap="false" EmptyDataText="No Records Found" 
                    HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
                    SelectedRowStyle-Wrap="false"
                    AllowSorting="True" onsorted="grd_Sorted" AllowPaging="True" onpageindexchanged="grd_PageIndexChanged"
                    onselectedindexchanged="grd_SelectedIndexChanged" 
                    OnRowDataBound="grd_RowDataBound" 
                    DataKeyNames="SupplierID" ShowFooter="false" PageSize="20">
                    <Columns>
                        <asp:BoundField DataField="SupplierID" ReadOnly="True" Visible="false" />
                        <asp:BoundField DataField="SupplierName" HeaderText="Supplier Name" SortExpression="SupplierName" />
                        <asp:BoundField DataField="SAPVendorCode" HeaderText="SAP Code" SortExpression="SAPVendorCode" />
                        <asp:BoundField DataField="StatusName" HeaderText="Status" SortExpression="StatusName" />
                    </Columns>
                    <RowStyle CssClass="grid_row" Wrap="False" />
                    <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
                    <HeaderStyle CssClass="grid_header" Wrap="False" />
                    <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
                </asp:GridView>
            </div><br />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
