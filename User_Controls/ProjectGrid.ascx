﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProjectGrid.ascx.cs" Inherits="User_Controls_ProjectGrid" %>

<!--Header and Filter Criteria-->
<table style="width:1100px" border="0">
    <tr>
        <td>
            <asp:Label ID="lblHeaderText" Text="Projects" runat="server" CssClass="page_header"></asp:Label>
        </td>
        <td>Area</td>
        <td>
            <asp:DropDownList id="cboBudgetResponsibilityArea" runat="server" ontextchanged="cboBudgetResponsibilityArea_TextChanged" 
                DataTextField="EntityName" DataValueField="ID" AutoPostBack="True"/>
        </td>
        <td>Customer</td>
        <td>
            <asp:DropDownList id="cboCustomer" runat="server" ontextchanged="cboCustomer_TextChanged" 
                DataTextField="EntityName" DataValueField="ID" AutoPostBack="True" Width="200px" />
        </td>
        <td>Brand</td>
        <td>
            <asp:DropDownList id="cboBrand" runat="server" ontextchanged="cboBrand_TextChanged" 
                DataTextField="EntityName" DataValueField="ID" AutoPostBack="True"/>
        </td>
        <td>Category</td>
        <td>
            <asp:DropDownList id="cboCategory" runat="server" ontextchanged="cboCategory_TextChanged" 
                DataTextField="EntityName" DataValueField="ID" AutoPostBack="True" Width="180px" />
        </td>
        <td>Year</td>
        <td style="text-align:right">
            <asp:DropDownList id="cboYear" runat="server" ontextchanged="cboYear_TextChanged" AutoPostBack="True" 
                DataTextField="EntityName" DataValueField="ID" />
        </td>
    </tr>
</table>
<table style="width:1100px" border="0">
    <tr>
        <td>&nbsp;Priority&nbsp;
            <asp:DropDownList id="cboPulsingPlanPriorityMatchType" runat="server" ontextchanged="cboPulsingPlanPriorityMatchType_TextChanged"
                AutoPostBack="True" Width="110px" />
            <asp:DropDownList id="cboPulsingPlanPriority" runat="server" ontextchanged="cboPulsingPlanPriority_TextChanged" 
                DataTextField="EntityName" DataValueField="ID" AutoPostBack="True" Width="180px" />
        </td>
        <td style="text-align:right">
            <asp:Label ID="lblSearch" Text="Search Project Name" runat="server"></asp:Label>
            <asp:TextBox ID="txtSearch" Runat="server" Width="150px"></asp:TextBox>
            <asp:Button ID="cmdGo" Text="Go" Runat="server" CausesValidation="false" />
        </td>
    </tr>
</table><br /> 

<!--Data Source - required to take advantage of sorting and paging functionality of the GridView' parameter removed 28/02/14 to solve filter problem with SQLDataSource when >101 records-->
<asp:SqlDataSource ID="sds" runat="server" 
    ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
    >
    


</asp:SqlDataSource>

<!--Grid-->
<div id="project_grid">
    <asp:GridView ID="grd" runat="server" width="1100px"
        AutoGenerateColumns="False" GridLines="Vertical" RowStyle-Wrap="false" EmptyDataText="No Records Found" 
        HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false" SelectedRowStyle-Wrap="false"
        AllowPaging="true" PageSize="20" onpageindexchanged="grd_PageIndexChanged" 
        onselectedindexchanged="grd_SelectedIndexChanged" AllowSorting="True" onsorted="grd_Sorted"
        DataKeyNames="ProjectToCustomerID" DataSourceID="sds" 
        onrowdatabound="grd_RowDataBound" ShowFooter="True">
        <Columns>
            <asp:BoundField DataField="ProjectToCustomerID" ReadOnly="True" Visible="false" />
            <asp:BoundField DataField="ProjectID" ReadOnly="True" Visible="false" />
            <asp:BoundField DataField="ProjectName" HeaderText="Project" SortExpression="ProjectName" />
            <asp:BoundField DataField="CustomerName" HeaderText="Customer" SortExpression="CustomerName" />
            <asp:BoundField DataField="IONumber" HeaderText="IO" SortExpression="IONumber" />
            <asp:BoundField DataField="SumOfAPBudget" HeaderText="A&P Bdgt" 
                SortExpression="SumOfAPBudget" DataFormatString="{0:0}" 
                ItemStyle-HorizontalAlign="Right" >
            </asp:BoundField>
            <asp:BoundField DataField="SumOfAPFYView" HeaderText="A&P FY Vw" 
                SortExpression="SumOfAPFYView" DataFormatString="{0:0}" 
                ItemStyle-HorizontalAlign="Right" >
            </asp:BoundField>
            <asp:BoundField DataField="SumOfAPVariance" HeaderText="A&P Var" 
                SortExpression="SumOfAPVariance" DataFormatString="{0:0}" 
                ItemStyle-HorizontalAlign="Right" >
            </asp:BoundField>
            <asp:BoundField DataField="SumOfTCCBudget" HeaderText="TCC Bdgt" 
                SortExpression="SumOfTCCBudget" DataFormatString="{0:0}" 
                ItemStyle-HorizontalAlign="Right" >
            </asp:BoundField>
            <asp:BoundField DataField="SumOfTCCFYView" HeaderText="TCC FY Vw" 
                SortExpression="SumOfTCCFYView" DataFormatString="{0:0}" 
                ItemStyle-HorizontalAlign="Right" >
            </asp:BoundField>
            <asp:BoundField DataField="SumOfTCCVariance" HeaderText="TCC Var" 
                SortExpression="SumOfTCCVariance" DataFormatString="{0:0}" 
                ItemStyle-HorizontalAlign="Right" >
            </asp:BoundField>
            <asp:BoundField DataField="SumOfFYView" HeaderText="Total FY Vw" 
                SortExpression="SumOfFYView" DataFormatString="{0:0}" 
                ItemStyle-HorizontalAlign="Right" >
            </asp:BoundField>
            <asp:BoundField DataField="SumOfVariance" HeaderText="Total Var" 
                SortExpression="SumOfVariance" DataFormatString="{0:0}" 
                ItemStyle-HorizontalAlign="Right" >
            </asp:BoundField>
            <asp:CommandField ShowSelectButton="True" Visible="False" />
        </Columns>
        <RowStyle CssClass="grid_row" Wrap="False" />
        <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
        <HeaderStyle CssClass="grid_header" Wrap="False" />
        <FooterStyle CssClass="grid_footer" Wrap="False" />
        <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
    </asp:GridView>
    <asp:Label ID="txtMessage" runat="server" />
</div>
<br />
