﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReallocationGrid.ascx.cs" Inherits="User_Controls_ReallocationGrid"  %>
<%@ Register src="~/User_Controls/MultiselectDropdown.ascx" tagname="dropdown" tagprefix="uc1" %>

<style type="text/css">
    .auto-style1 {
        width: 131px;
    }
    .auto-style2 {
        width: 246px;
    }

</style>

<script type="text/javascript">
    $(function () {
        $("[id*=grd] td").bind("click", function () {
            var row = $(this).parent();
            var GridSelection = document.getElementById('ctl00$ContentPlaceHolder1$Grid1$SelectedProjectToCustomerID');
            var index = -1;

            $("[id*=grd] tr").each(function () {
                if ($(this)[0] != row[0]) {
                    if (row[0].innerHTML.indexOf(">Totals<") < 0) {
                        index = index + 1;
                        $("td", this).removeClass("grid_selected");
                    }
                }
                else {
                    if (row[0].innerHTML.indexOf(">Totals<") < 0)
                        GridSelection.value = index;
                }
            });
            $("td", row).each(function () {
                if (row[0].innerHTML.indexOf(">Totals<") < 0) {
                    if (!$(this).hasClass("grid_selected")) {
                        $(this).addClass("grid_selected");
                    } else {
                        //Don't do anything. Code below to deselect row
                        //$(this).removeClass("grid_selected");
                    }
                }
            });
        });
    });
</script>

<script type="text/javascript">

    function CopySpendTypeToDestination() {
        var FromSpendType = document.getElementById('ctl00_ContentPlaceHolder1_Grid1_cboFromSpendTypeID');
        var ToSpendType = document.getElementById('ctl00_ContentPlaceHolder1_Grid1_cboToSpendTypeID');
        ToSpendType.value = FromSpendType.value;
        return false;
    }
</script>

<script type="text/javascript">
    function setscroll(ctlID) {
        var panel = document.getElementById(ctlID + 'grid');
        var div_value = document.cookie.substring(document.cookie.indexOf("=") + 1, document.cookie.length);
        panel.scrollTop = div_value.substring(0, div_value.indexOf(","));
        panel.scrollLeft = div_value.substring(div_value.indexOf(",") + 1, div_value.length);
    }
</script>
<script type="text/javascript">
    function savescroll(ctlID) {
        var panel = document.getElementById(ctlID + 'grid');
        document.cookie = "div_position=" + panel.scrollTop.toString() + ',' + panel.scrollLeft.toString() + ";";
    }
</script>
<asp:HiddenField ID="SelectedProjectToCustomerID" runat="server" />
<asp:HiddenField ID="FromProjectToCustomerID" runat="server" />
<asp:HiddenField ID="ToProjectToCustomerID" runat="server" />
<asp:HiddenField ID="ReallocationProgress" runat="server" />
<div id="bodymain" runat="server" >
    <!--Header and Filter Criteria-->
    <div id="resizable1" class="resizable ui-widget-content">
        <br />
        <asp:Label ID="lblHeaderText" Text="Projects" runat="server" CssClass="page_header"></asp:Label>
        <br />
        <br />
        <table style="width:100%" border="0">
            <tr>
                <td>Area</td>
                <td>
                    <asp:DropDownList id="cboBudgetResponsibilityArea" runat="server" ontextchanged="cboBudgetResponsibilityArea_TextChanged" width="170px"
                        DataTextField="EntityName" DataValueField="ID" AutoPostBack="True" ForeColor="Black"/>
                </td>
            </tr>
            <tr>
                <td>Customer</td>
                <td>
                    <asp:DropDownList id="cboCustomer" runat="server" ontextchanged="cboCustomer_TextChanged"  width="170px"
                        DataTextField="EntityName" DataValueField="ID" AutoPostBack="True" ForeColor="Black"/>
                </td>
            </tr>
            <tr>
                <td>Category</td>
                <td>
                    <asp:DropDownList id="cboCategory" runat="server" ontextchanged="cboCategory_TextChanged"  width="170px"
                        DataTextField="EntityName" DataValueField="ID" AutoPostBack="True" ForeColor="Black"/>
                </td>
            </tr>
            <tr>
                <td>Brand</td>
                <td>
                    <asp:DropDownList id="cboBrand" runat="server" ontextchanged="cboBrand_TextChanged"  width="170px"
                        DataTextField="EntityName" DataValueField="ID" AutoPostBack="True" ForeColor="Black"/>
                </td>
            </tr>
            <tr>
                <td>Year</td>
                <td>
                    <asp:DropDownList id="cboYear" runat="server" ontextchanged="cboYear_TextChanged" AutoPostBack="True" 
                        DataTextField="EntityName" DataValueField="ID" ForeColor="Black"/>
                </td>
            </tr>
            <tr>
                <td>Priority</td>
                <td>
                    <uc1:DropDown id="cboPulsingPlanPriority" runat="server"/>
                </td>
            </tr><tr>
                <td colspan="2">
                    <asp:Button ID="cmdReallocateFrom" runat="server" Text="Reallocate From" Width="120px"
                     onclick="cmdReallocateFrom_Click" CssClass="form_button" Enabled="true" CausesValidation="False" />
                </td>
            </tr>
            <tr><td>Project:</td>
            </tr>
            <tr>
                <td colspan ="2">
                <asp:Label ID="txtFromProject" runat="server" Width="304px" CssClass="label_text" Font-Bold="True"/>
                </td>
            </tr>
            <tr><td>Customer:</td><td>
                <asp:label ID="txtFromCustomer" runat="server" Width="170px" CssClass="label_text" Font-Bold="True"/>
            </td>
                </tr>
            <tr><td>IO Number:</td><td>
                <asp:Label ID="txtFromIONumber" runat="server" CssClass="label_text" Width="100px" Font-Bold="True"/>
            </td>
                </tr>
            <tr><td>Spend Type:</td><td>
                <asp:DropDownList ID="cboFromSpendTypeID" runat="server" DataTextField="EntityName" DataValueField="ID" onchange = "CopySpendTypeToDestination();" ForeColor="Black"/>
                <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="*Required" MaximumValue="3" MinimumValue="1" ControlToValidate="cboFromSpendTypeID"></asp:RangeValidator>
            </td>
                </tr>
            <tr>
            <td colspan="2">
                <asp:Button ID="cmdReallocateTo" runat="server" Text="Reallocate To" Width="120px"
                    onclick="cmdReallocateTo_Click" CssClass="form_button" Enabled="true" CausesValidation="False" />
            </td></tr>
            <tr>
                <td>Project:</td>
                </tr>
            <tr><td colspan="2">
                <asp:Label ID="txtToProject" runat="server" Width="304px" CssClass="label_text" Font-Bold="True"/>
                <asp:CustomValidator ID="CustomValidator2" runat="server" ErrorMessage="*Please select a valid project" 
                    Display="Dynamic" onservervalidate="CustomValidator2_ServerValidate" />
            </td></tr>
            <tr>    
                <td>Customer:</td><td>
                <asp:Label ID="txtToCustomer" runat="server" Width="170px" CssClass="label_text" Font-Bold="True"/>
            </td></tr>
            <tr>
                <td>IO Number:</td><td>
                <asp:Label ID="txtToIONumber" runat="server" CssClass="label_text" Width="100px" Font-Bold="True"/>
                <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="*Source and Destination must be different." 
                    Display="Dynamic" onservervalidate="CustomValidator1_ServerValidate" />
            </td></tr>
            <tr>
                <td>Spend Type:</td><td>
                <asp:DropDownList ID="cboToSpendTypeID" runat="server" DataTextField="EntityName" DataValueField="ID" ForeColor="Black"/>
                <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="*Required" MaximumValue="3" MinimumValue="1" 
                    ControlToValidate="cboToSpendTypeID" />
            </td></tr>
            <tr><td>Amount To Reallocate:</td><td>
                <asp:TextBox ID="txtBudgetAmount" runat="server" Width="75px" ForeColor="Black"/>
                <asp:Label ID="lblDefaultCurrency" runat="server" Text="EUR" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*Required" 
                    ControlToValidate="txtBudgetAmount" Display="Dynamic"/>
                <asp:RangeValidator ID="RangeValidator3" runat="server" ErrorMessage="*Enter a numeric value between 1 and 10,000,000" 
                    MaximumValue="10000000" MinimumValue="1" ControlToValidate="txtBudgetAmount" Display="Dynamic" Type="Integer" />
            </td>
            </tr>
            <tr>
            <td>Approver:</td>
            <td>
                <asp:DropDownList ID="cboApprover" runat="server" DataTextField="UserName" DataValueField="UserName" ForeColor="Black"/>
                <asp:Label ID ="ApproverMessage" Text="" runat="server" ForeColor ="Red" />
            </td>
            </tr>
             <tr>
            <td rowspan="2">Comments:</td><td rowspan="2">
                <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" width="170px" Height="40px" ForeColor="Black"></asp:TextBox>
            </td>
            </tr>
        </table>
    </div>


    <!--Search and Grid-->
    <div id="resizable2" class="ui-widget-content">
        <!--Data Source - required to take advantage of sorting and paging functionality of the GridView; parameter removed 28/02/14 to solve filter problem with SQLDataSource when >101 records-->
        <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:APConnectionString %>" >
        </asp:SqlDataSource>

        <div style="padding-top:10px">
            <asp:Label ID="lblSearch" Text="Search Project Name" runat="server"></asp:Label>
            <asp:TextBox ID="txtSearch" Runat="server" Width="150px" OnTextChanged="txtSearch_TextChanged"></asp:TextBox>
            <asp:Button ID="cmdGo" Text="Go" Runat="server" CausesValidation="false" OnClick="cmdGo_Click"/>
            <asp:Label ID="txtMessage" runat="server" />
            </div>
        <div id="project_grid_rs" style="padding-top:10px; overflow-x:hidden; overflow-y:hidden; height:92%" >
<!--Grid-->
<asp:Panel id="grid" style="overflow-x:auto; overflow-y:auto; height:97%; position:relative;" runat="server">
    <asp:GridView ID="grd" runat="server" width="98.6%"
        AutoGenerateColumns="False" GridLines="Vertical" RowStyle-Wrap="false" EmptyDataText="No Records Found" 
        AlternatingRowStyle-Wrap="false" SelectedRowStyle-Wrap="false"
        DataKeyNames="ProjectToCustomerID" HeaderStyle-Wrap="false"
        onrowdatabound="grd_RowDataBound" ShowFooter="True" >

        <HeaderStyle BackColor="Gray" CssClass="GVFixedHeader" ForeColor="White" Wrap="false"  Height="15px"></HeaderStyle>
        <Columns>
            <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="ProjectToCustomerID" ReadOnly="True" Visible="false" />
            <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="ProjectID" ReadOnly="True" Visible="false" />
            <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="ProjectName" HeaderText="Project" SortExpression="ProjectName" />
            <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="CustomerName" HeaderText="Customer" SortExpression="CustomerName" />
            <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="IONumber" HeaderText="IO" SortExpression="IONumber" />
            <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="SumOfAPBudget" HeaderText="A&P Bdgt" 
                SortExpression="SumOfAPBudget" DataFormatString="{0:0}" 
                ItemStyle-HorizontalAlign="Right" >
            </asp:BoundField>
            <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="SumOfAPFYView" HeaderText="A&P FY Vw" 
                SortExpression="SumOfAPFYView" DataFormatString="{0:0}" 
                ItemStyle-HorizontalAlign="Right" >
            </asp:BoundField>
            <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="SumOfAPVariance" HeaderText="A&P Var" 
                SortExpression="SumOfAPVariance" DataFormatString="{0:0}" 
                ItemStyle-HorizontalAlign="Right" >
            </asp:BoundField>
            <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="SumOfTCCBudget" HeaderText="TCC Bdgt" 
                SortExpression="SumOfTCCBudget" DataFormatString="{0:0}" 
                ItemStyle-HorizontalAlign="Right" >
            </asp:BoundField>
            <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="SumOfTCCFYView" HeaderText="TCC FY Vw" 
                SortExpression="SumOfTCCFYView" DataFormatString="{0:0}" 
                ItemStyle-HorizontalAlign="Right" >
            </asp:BoundField>
            <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="SumOfTCCVariance" HeaderText="TCC Var" 
                SortExpression="SumOfTCCVariance" DataFormatString="{0:0}" 
                ItemStyle-HorizontalAlign="Right" >
            </asp:BoundField>
            <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="SumOfFYView" HeaderText="Total FY Vw" 
                SortExpression="SumOfFYView" DataFormatString="{0:0}" 
                ItemStyle-HorizontalAlign="Right" >
            </asp:BoundField>
            <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="SumOfVariance" HeaderText="Total Var" 
                SortExpression="SumOfVariance" DataFormatString="{0:0}" 
                ItemStyle-HorizontalAlign="Right" >
            </asp:BoundField>
            <asp:CommandField ShowSelectButton="True" Visible="False" />
        </Columns>
        <RowStyle CssClass="grid_row" Wrap="False" BackColor="#EFF3FB" />
                <SelectedRowStyle CssClass="grid_selected" Wrap="False" BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <EditRowStyle BackColor="#2461BF" />
                <HeaderStyle BackColor="Gray" CssClass="GVFixedHeader" ForeColor="White" Wrap="false" Height="15px"></HeaderStyle>           
                <FooterStyle CssClass="grid_footer" Wrap="False" />
                <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" BackColor="White" Height="15px"/>
    </asp:GridView>
</asp:Panel>
</div>
</div>

</div>
