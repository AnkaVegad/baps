﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VarianceGrid.ascx.cs" Inherits="User_Controls_VarianceGrid" %>

    <!--Page Header-->
    <table border="0" width="1000px">
        <tr>
            <td align="left">
                <asp:Label ID="lblHeader" Text="Variances" runat="server" CssClass="page_header" />
            </td>
        <td>Area</td>
        <td>
            <asp:DropDownList id="cboBudgetResponsibilityArea" runat="server" ontextchanged="cboBudgetResponsibilityArea_TextChanged" 
                DataTextField="EntityName" DataValueField="ID" AutoPostBack="True"/>
        </td>
        <td>Year</td>
        <td>
            <asp:DropDownList id="cboYear" runat="server" ontextchanged="cboYear_TextChanged" AutoPostBack="True" 
                DataTextField="EntityName" DataValueField="ID" />
        <tr><td style="font-size:3px">&nbsp;</td></tr>
        <tr>
            <td align="left" colspan="5">
                <asp:Label ID="Label12" Text="Variance of Actuals from Forecast Snapshot; 100 is a perfect score; < 100 is an over-forecast; > 100 is an under-forecast; 0 means no actuals or no forecast" runat="server" Width="1050px" />
            </td>
        </tr>    
    </table>
    <br />
    <!--Data Source - required to take advantage of sorting and paging functionality of the GridView-->
    <asp:SqlDataSource ID="sds" runat="server" 
        ConnectionString="<%$ ConnectionStrings:APConnectionString %>">
    </asp:SqlDataSource>

    <!--Variance Grid-->
    <div id="project_grid">
        <asp:GridView ID="grd" runat="server" width="1000px"
            AutoGenerateColumns="False" GridLines="Vertical"
            RowStyle-Wrap="false" EmptyDataText="No Records Found" 
            HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
            SelectedRowStyle-Wrap="false"
            AllowSorting="True" onsorted="grd_Sorted" AllowPaging="True" onpageindexchanged="grd_PageIndexChanged"
            onselectedindexchanged="grd_SelectedIndexChanged" 
            OnRowDataBound="grd_RowDataBound" 
            DataKeyNames="BusinessGrouping" ShowFooter="False" PageSize="35">
            <Columns>
                <asp:BoundField DataField="BusinessGrouping" HeaderText="Customer/Category" SortExpression="BusinessGrouping" />
                <asp:BoundField DataField="SpendTypeName" HeaderText="Spend" SortExpression="SpendTypeName" />
                <asp:BoundField DataField="Month01" HeaderText="Jan" SortExpression="Month01" />
                <asp:BoundField DataField="Month02" HeaderText="Feb" SortExpression="Month02" />
                <asp:BoundField DataField="Month03" HeaderText="Mar" SortExpression="Month03" />
                <asp:BoundField DataField="Month04" HeaderText="Apr" SortExpression="Month04" />
                <asp:BoundField DataField="Month05" HeaderText="May" SortExpression="Month05" />
                <asp:BoundField DataField="Month06" HeaderText="Jun" SortExpression="Month06" />
                <asp:BoundField DataField="Month07" HeaderText="Jul" SortExpression="Month07" />
                <asp:BoundField DataField="Month08" HeaderText="Aug" SortExpression="Month08" />
                <asp:BoundField DataField="Month09" HeaderText="Sep" SortExpression="Month09" />
                <asp:BoundField DataField="Month10" HeaderText="Oct" SortExpression="Month10" />
                <asp:BoundField DataField="Month11" HeaderText="Nov" SortExpression="Month11" />
                <asp:BoundField DataField="Month12" HeaderText="Dec" SortExpression="Month12" />
            </Columns>
            <RowStyle CssClass="grid_row" Wrap="False" />
            <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
            <HeaderStyle CssClass="grid_header" Wrap="False" />
            <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
        </asp:GridView>
    </div><br />
