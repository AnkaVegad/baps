﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PageHeader.ascx.cs" Inherits="User_Controls_PageHeader" %>

    <!--Page Header-->
    <table border="0" style="width:1095px">
        <tr>
            <td style="text-align:left">
                <asp:Label ID="Label11" Text="" runat="server" CssClass="page_header" Width="75px" />
            </td><td>
                <asp:Label ID="ProjectToCustomerID" runat="server" visible="false" Width="30px" CssClass="label_text" />
                <asp:Label ID="BudgetResponsibilityAreaID" runat="server" visible="false" Width="30px" CssClass="label_text" />
                <asp:Label ID="ProjectID" runat="server" visible="false" Width="30px" />
                <asp:Label ID="CustomerID" runat="server" visible="false" Width="30px" />
            </td>
            <td style="text-align:right">
                <asp:Label ID="Label901" Text="Project" runat="server" CssClass="page_header" />
            </td>
            <td style="text-align:left">
                <asp:Label ID="ProjectName" Text="" runat="server" CssClass="page_header_text" />
            </td>
            <td style="text-align:right">
                <asp:Label ID="Label902" Text="Customer" runat="server" CssClass="page_header" />
            </td>
            <td style="text-align:left">
                <asp:Label ID="CustomerName" Text="" runat="server" CssClass="page_header_text" />
            </td>
            <td style="text-align:right">
                <asp:Label ID="Label903" Text="IO Number" runat="server" CssClass="page_header"></asp:Label>
            </td>
            <td style="text-align:left">
                <asp:Label ID="IONumber" Text="" runat="server" CssClass="page_header_text" />
            </td>
            <td style="text-align:right">
                <asp:Label ID="Label997" Text="Year" runat="server" CssClass="page_header"></asp:Label>
            </td>
            <td style="text-align:left">
                <asp:Label ID="ProjectYear" Text="" runat="server" CssClass="page_header_text" Width="50px" />
            </td>
        </tr>
    </table>
