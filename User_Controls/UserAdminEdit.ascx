﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserAdminEdit.ascx.cs" Inherits="User_Controls_UserAdminEdit" %>

<!--User Edit--> 
<br /><br />
<!--Page Header-->
<table border="0" style="width:1100px">
    <tr>
        <td style="text-align:left">
            <asp:Label ID="lblHeader" Text="User Admin" runat="server" CssClass="page_header"/>
        </td>
    </tr>
</table>
<!--User Edit Area-->
<table style="width:1080px"><tr><td style="vertical-align:top">
    <table border="0">
        <tr><td>User Name</td><td>
            <asp:TextBox ID="UserID" runat="server" Visible="False"></asp:TextBox>
            <asp:TextBox ID="UserName" runat="server" Width="180px" MaxLength="50" Enabled="False" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*Required" 
                ControlToValidate="UserName" Display="Dynamic" ValidationGroup="User" />
        </td></tr>
        <tr><td>Level</td><td>
            <asp:DropDownList ID="UserLevelID" runat="server" Width="120px" Enabled="true" DataTextField="EntityName" DataValueField="ID" />
            <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="*Required" 
                MinimumValue="0" MaximumValue="1000" ControlToValidate="UserLevelID" Display="Dynamic" Type="Integer" ValidationGroup="User" />
        </td></tr>
        <tr><td>Active</td><td>
            <asp:DropDownList ID="ActiveIndicator" runat="server" Width="120px" Enabled="true" DataTextField="EntityName" DataValueField="ID" />
            <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="*Required" 
                MinimumValue="1" MaximumValue="2" ControlToValidate="ActiveIndicator" Display="Dynamic" Type="Integer" ValidationGroup="User" />
        </td></tr>
        <tr><td>Delegate User Name</td><td>
            <asp:TextBox ID="DelegateUserName" runat="server" Width="180px" MaxLength="50" />
        </td></tr>
    </table>
</td>
<td style="vertical-align:top">
    <table border="0">
        <tr><td colspan="2">Authorisations</td></tr>
        <tr><td colspan="2">
            <asp:GridView ID="grdAuth" runat="server" width="550px" AutoGenerateColumns="False" 
                GridLines="Vertical" RowStyle-Wrap="false" EmptyDataText="No records found" 
                HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false" SelectedRowStyle-Wrap="false"
                AllowSorting="false" AllowPaging="false" OnRowDataBound="grdAuth_RowDataBound" 
                DataKeyNames="UserAuthorisationsKey" PageSize="25">
                <Columns>
                    <asp:BoundField DataField="UserAuthorisationsKey" visible="false" />
                    <asp:BoundField DataField="BudgetResponsibilityAreaID" visible="false" />
                    <asp:BoundField DataField="BudgetResponsibilityAreaName" HeaderText="Area" />
                    <asp:BoundField DataField="CustomerID" visible="false" />
                    <asp:BoundField DataField="CustomerName" HeaderText="Customer(s)" />
                    <asp:BoundField DataField="BrandID" visible="false" />
                    <asp:BoundField DataField="BrandName" HeaderText="Product Hierarchy" />
                </Columns>
                <RowStyle CssClass="grid_row" Wrap="False" />
                <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
                <HeaderStyle CssClass="grid_header" Wrap="False" />
                <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
            </asp:GridView>
        </td></tr><tr><td colspan="2" style="font-size:2px">&nbsp;</td></tr><tr><td>
            <asp:Button ID="cmdRemoveUserAuth" runat="server" Text="Remove" causesvalidation="true"
                onclick="cmdRemoveUserAuth_Click" CssClass="form_button" Enabled="true" />
        </td></tr>
                
        <tr><td colspan="2">
            <asp:DropDownList ID="cboBudgetResponsibilityAreaID" runat="server" Width="178px" Enabled="true" DataTextField="EntityName" DataValueField="ID" />
            <asp:RangeValidator ID="RangeValidator3" ControlToValidate="cboBudgetResponsibilityAreaID" MaximumValue="10000000" MinimumValue="1"
                runat="server" ErrorMessage="Required" Type="Integer" Display="Dynamic" ValidationGroup="Auth" />
            <asp:DropDownList ID="cboCustomerID" runat="server" Width="178px" Enabled="true" DataTextField="EntityName" DataValueField="ID" />
            <asp:RangeValidator ID="RangeValidator6" ControlToValidate="cboCustomerID" MaximumValue="10000000" MinimumValue="0"
                runat="server" ErrorMessage="Required" Type="Integer" Display="Dynamic" ValidationGroup="Auth" />
            <asp:DropDownList ID="cboBrandID" runat="server" Width="178px" Enabled="true" DataTextField="EntityName" DataValueField="ID" />
            <asp:RangeValidator ID="RangeValidator4" ControlToValidate="cboBrandID" MaximumValue="10000000" MinimumValue="0"
                runat="server" ErrorMessage="Required" Type="Integer" Display="Dynamic" ValidationGroup="Auth" />
            <asp:Button ID="cmdAddUserAuth" runat="server" Text="Add" causesvalidation="true"
                onclick="cmdAddUserAuth_Click" CssClass="form_button" Enabled="true" Width="60px" ValidationGroup="Auth" />
        </td></tr>
    </table>
</td></tr>
</table>
  