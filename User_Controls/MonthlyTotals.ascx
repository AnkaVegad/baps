﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MonthlyTotals.ascx.cs" Inherits="User_Controls_MonthlyTotals" %>

    <!--Activity Monthly Summary Grid-->
    <table border="0" width="895px" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left"><b>MONTHLY
                </b></td>
        </tr>
    </table>

    <asp:Label ID="ProjectID" runat="server" visible="false" Width="30px" />
    <asp:Label ID="CustomerID" runat="server" visible="false" Width="30px" />

    <asp:SqlDataSource ID="sds" runat="server" 
    ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
    SelectCommand="SelectActivityMonthlySummary" 
    SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:ControlParameter ControlID="ProjectID" DefaultValue="" Name="ProjectID" 
                PropertyName="Text" Type="Int32" />
            <asp:ControlParameter ControlID="CustomerID" Name="CustomerID" 
                PropertyName="Text" Type="Int32" />
        </SelectParameters>

    </asp:SqlDataSource>
                
    <div id="grid_monthly">
        <asp:GridView ID="grdMonthly" runat="server" width="1070px"
            AutoGenerateColumns="False" GridLines="Vertical"
            RowStyle-Wrap="false" EmptyDataText="No Records Found" 
            HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
            SelectedRowStyle-Wrap="false"
            AllowSorting="True" onsorted="grdMonthly_Sorted" AllowPaging="True" onpageindexchanged="grdMonthly_PageIndexChanged"
            DataKeyNames="MeasureOrder" DataSourceID="sds" PagerSettings-PageButtonCount="10" PageSize="20">
            <Columns>
                <asp:BoundField DataField="MeasureOrder" InsertVisible="False" ReadOnly="True" Visible="False" />
                <asp:BoundField DataField="MeasureName" HeaderText="Measure" SortExpression="MeasureName" />
                <asp:BoundField DataField="SpendTypeName" HeaderText="Spend Type" SortExpression="SpendTypeName" />
                <asp:BoundField DataField="Month01" HeaderText="Jan" 
                    SortExpression="Month01" ItemStyle-HorizontalAlign="Right" 
                    DataFormatString="{0:0}" ItemStyle-Width="65px" >
                </asp:BoundField>
                <asp:BoundField DataField="Month02" HeaderText="Feb" 
                    SortExpression="Month02" ItemStyle-HorizontalAlign="Right" 
                    DataFormatString="{0:0}" ItemStyle-Width="65px" >
                </asp:BoundField>
                <asp:BoundField DataField="Month03" HeaderText="Mar" 
                    SortExpression="Month03" ItemStyle-HorizontalAlign="Right" 
                    DataFormatString="{0:0}" ItemStyle-Width="65px" >
                </asp:BoundField>
                <asp:BoundField DataField="Month04" HeaderText="Apr" 
                    SortExpression="Month04" ItemStyle-HorizontalAlign="Right" 
                    DataFormatString="{0:0}" ItemStyle-Width="65px" >
                </asp:BoundField>
                <asp:BoundField DataField="Month05" HeaderText="May" 
                    SortExpression="Month05" ItemStyle-HorizontalAlign="Right" 
                    DataFormatString="{0:0}" ItemStyle-Width="65px" >
                </asp:BoundField>
                <asp:BoundField DataField="Month06" HeaderText="Jun" 
                    SortExpression="Month06" ItemStyle-HorizontalAlign="Right" 
                    DataFormatString="{0:0}" ItemStyle-Width="65px" >
                </asp:BoundField>
                <asp:BoundField DataField="Month07" HeaderText="Jul" 
                    SortExpression="Month07" ItemStyle-HorizontalAlign="Right" 
                    DataFormatString="{0:0}" ItemStyle-Width="65px" >
                </asp:BoundField>
                <asp:BoundField DataField="Month08" HeaderText="Aug" 
                    SortExpression="Month08" ItemStyle-HorizontalAlign="Right" 
                    DataFormatString="{0:0}" ItemStyle-Width="65px" >
                </asp:BoundField>
                <asp:BoundField DataField="Month09" HeaderText="Sep" 
                    SortExpression="Month09" ItemStyle-HorizontalAlign="Right" 
                    DataFormatString="{0:0}" ItemStyle-Width="65px" >
                </asp:BoundField>
                <asp:BoundField DataField="Month10" HeaderText="Oct" 
                    SortExpression="Month10" ItemStyle-HorizontalAlign="Right" 
                    DataFormatString="{0:0}" ItemStyle-Width="65px" >
                </asp:BoundField>
                <asp:BoundField DataField="Month11" HeaderText="Nov" 
                    SortExpression="Month11" ItemStyle-HorizontalAlign="Right" 
                    DataFormatString="{0:0}" ItemStyle-Width="65px" >
                </asp:BoundField>
                <asp:BoundField DataField="Month12" HeaderText="Dec" 
                    SortExpression="Month12" ItemStyle-HorizontalAlign="Right" 
                    DataFormatString="{0:0}" ItemStyle-Width="65px" >
                </asp:BoundField>
            </Columns>
            <RowStyle CssClass="grid_row" Wrap="False" />
            <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
            <HeaderStyle CssClass="grid_header" Wrap="False" />
            <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
        </asp:GridView>
    </div><br /><br />
