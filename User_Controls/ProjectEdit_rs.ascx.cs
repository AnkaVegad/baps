﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Controls_ProjectEdit_rs : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        cboPulsingPlanTiming.OkButtonClicked += new EventHandler(cboPulsingPlanTiming_OkButtonClicked);
        //populate the drop-downs in edit area
        if (!IsPostBack)
        {
            //ProjectID.Text = Session["SelectedProjectID"].ToString();
            FillProjectYear();
            FillPulsingPlanPriority();
            FillBudgetResponsibilityArea(); 
            FillSubBrand(); 
            FillCustomer();
            FillPulsingPlanTiming();
            cboAddSubBrandID.Attributes.Add("onchange", "Javascript:calcProportion();");
        }
    }

#region properties

    public Project SelectProject
    {
        get
        {
            Project a = new Project();
            a.ProjectID = Convert.ToInt32(ProjectID.Text);
            a.ProjectName = ProjectName.Text;
            //a.CountryID = int.Parse(CountryID.SelectedValue);
            a.BudgetResponsibilityAreaID = int.Parse(BudgetResponsibilityAreaID.SelectedValue);
            a.ProjectYear = ProjectYear.SelectedValue;
            a.ProjectDescription = ProjectDescription.Text;
            a.PulsingPlanPriorityID = int.Parse(PulsingPlanPriorityID.SelectedValue);
            a.ProjectOwner = ProjectOwner.Text;
            a.PulsingPlanTiming = PulsingPlanTiming.Text;
            string[] items = PulsingPlanTiming.Text.Split(',');
            a.ProjectStartMonth = items[0].Trim();
            a.ProjectEndMonth = items[items.Length - 1].Trim();
            a.ProjectObjective = ProjectObjective.Text;
            a.KPIs = KPIs.Text;
            a.LastUpdatedBy = Session["CurrentUserName"].ToString();
            a.CreatedBy = Session["CurrentUserName"].ToString();
            return a;
        }
    }

    public string ProjectIDValue
    {
        get { return ProjectID.Text; }
    }

#endregion

#region gridevents

    double dblTotal = 0;

    protected void grdSubBrandID_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            //calculate total SubBrand proportion
            double dblCurrent = Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "PropnPerCent"));
            //colour code the cell 
            //if (intCurrent < 0) { e.Row.Cells[11].BackColor = Color.PapayaWhip; } else { e.Row.Cells[11].BackColor = Color.LightGreen; }
            //increment the total
            dblTotal += dblCurrent;

            //highlight on hover
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            e.Row.Attributes["onclick"] = cs.GetPostBackClientHyperlink(this.grdSubBrandID, "Select$" + e.Row.RowIndex);
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[1].Text = "Total";
            //display the total in grid and in hidden control
            e.Row.Cells[2].Text = dblTotal.ToString();
            txtTotalProportion.Text = dblTotal.ToString();
            //colour code the cell 
            if (dblTotal == 100) { e.Row.Cells[2].BackColor = Color.LightGreen; } else { e.Row.Cells[2].BackColor = Color.PapayaWhip; }
        }
    }

    double dblTotalAP = 0;
    double dblTotalTCC = 0;

    protected void grdCustomerID_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            //calculate total A&P Budget
            double dblCurrent = Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "AllocatedAPBudget"));
            //increment the total
            dblTotalAP += dblCurrent;
            //calculate total TCC Budget
            dblCurrent = Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "AllocatedTCCBudget"));
            //increment the total
            dblTotalTCC += dblCurrent;

            //highlight on hover
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            e.Row.Attributes["onclick"] = cs.GetPostBackClientHyperlink(this.grdCustomerID, "Select$" + e.Row.RowIndex);
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[1].Text = "Totals";
            //display the total in grid and in hidden control
            e.Row.Cells[3].Text = dblTotalAP.ToString();
            e.Row.Cells[4].Text = dblTotalTCC.ToString();
            //colour code the cell 
            //if (dblTotal == 100) { e.Row.Cells[2].BackColor = Color.LightGreen; } else { e.Row.Cells[2].BackColor = Color.PapayaWhip; }
        }
    }

#endregion

#region methods

    public void PopulateForm(Project a)
    {
        //populate fields
        ProjectID.Text = a.ProjectID.ToString();
        ProjectName.Text = a.ProjectName;
        BudgetResponsibilityAreaID.SelectedValue = a.BudgetResponsibilityAreaID.ToString();
        ProjectYear.SelectedValue = a.ProjectYear.ToString();
        ProjectDescription.Text = a.ProjectDescription;
        PulsingPlanPriorityID.SelectedValue = a.PulsingPlanPriorityID.ToString();
        ProjectOwner.Text = a.ProjectOwner.ToString();
        PulsingPlanTiming.Text = a.ProjectStartMonth.Trim() + ", " + a.ProjectEndMonth;
        ProjectObjective.Text = a.ProjectObjective;
        KPIs.Text = a.KPIs;
        CreatedBy.Text = a.CreatedBy;
        CreatedDate.Text = a.CreatedDate;
        LastUpdatedBy.Text = a.LastUpdatedBy;
        LastUpdatedDate.Text = a.LastUpdatedDate;


    //Changed Vidya
        //Fill values in Pulsing plan timing multiselect dropdown
        string SelectedValues = a.ProjectStartMonth.Trim().PadLeft(2,'0');
        if (a.ProjectStartMonth.Trim().Length > 0 && a.ProjectEndMonth.Trim().Length > 0)
        {
            for (int i = int.Parse(a.ProjectStartMonth) + 1; i <= int.Parse(a.ProjectEndMonth); i++)
            {
                SelectedValues = SelectedValues + ',' + i.ToString().Trim().PadLeft(2,'0');
            }
            cboPulsingPlanTiming.DeselectAllValues();
            cboPulsingPlanTiming.SetSelectedValue(SelectedValues);
        }

        //populate grdSubBrandID
        DataTable tbl = ProjectDataAccess.FillProjectToSubBrand(Convert.ToInt32(a.ProjectID));
        Session["SelectedSubBrandItems"] = tbl;
        grdSubBrandID.DataSource = tbl;
        grdSubBrandID.DataBind();

        //populate grdCustomerID
        tbl = ProjectDataAccess.FillCustomersForSelectedProject(Convert.ToInt32(a.ProjectID), Session["CurrentUserName"].ToString());
        Session["SelectedCustomerItems"] = tbl;
        grdCustomerID.DataSource = tbl;
        grdCustomerID.DataBind();
        
        //enable controls (must be after ProjectID is populated)
        if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 15)
        {
            ToggleControlState(true);
        }

        //ClearControls();
    }

    public void PopulateCopy(Project a)
    //populate selected fields from the project to be copied from
    {
        //populate fields
        BudgetResponsibilityAreaID.SelectedValue = a.BudgetResponsibilityAreaID.ToString();
        ProjectYear.SelectedValue = a.ProjectYear.ToString();
        PulsingPlanPriorityID.SelectedValue = a.PulsingPlanPriorityID.ToString();
        ProjectOwner.Text = a.ProjectOwner.ToString();

        //populate grdSubBrandID
        DataTable tbl = ProjectDataAccess.FillProjectToSubBrand(Convert.ToInt32(a.ProjectID));
        Session["SelectedSubBrandItems"] = tbl;
        grdSubBrandID.DataSource = tbl;
        grdSubBrandID.DataBind();

        //initialise selected customer table
        tbl = new DataTable();
        tbl.Columns.Add("ID");
        tbl.Columns.Add("EntityName");
        tbl.Columns.Add("IONumber");
        tbl.Columns.Add("AllocatedAPBudget");
        tbl.Columns.Add("AllocatedTCCBudget");
        Session["SelectedCustomerItems"] = tbl;

        ProjectID.Text = "0";
        ProjectName.Focus();
        //enable controls (must be after ProjectID is populated)
        if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 15)
        {
            ToggleControlState(true);
        }
    }

    public string CheckPulsingPlanTiming()
    {
        string errorMessage = "";
        if (cboPulsingPlanTiming.getSelectedCount() == 0)
        {
            errorMessage = "*Required";
        }
        else if (!cboPulsingPlanTiming.CheckContinuous())
        {
            errorMessage = "PulsingPlanTiming should be continuous values";
        }
        lblPulsingPlanError.Text = errorMessage;
        return errorMessage;
    }

    public string UpdateProject()
    {
        if (Session["SelectedSubBrandItems"] == null) //check for session expiry
        {
            Response.Redirect("CalendarView.aspx");
            return "";
        }
        else
        {
            //Main data
            int intMessage = ProjectDataAccess.UpdateProject(SelectProject);
            if (intMessage == 0)
            {
                return "Duplicate project name";
            }
            else
            {
                if (intMessage == -1)
                {
                    return "Invalid project owner";
                }
                else
                {
                    DataTable tbl = (DataTable)Session["SelectedSubBrandItems"];
                    ProjectDataAccess.MergeProjectToSubBrand(ProjectID.Text, tbl, Session["CurrentUserName"].ToString());
                    tbl = (DataTable)Session["SelectedCustomerItems"];

                    DataColumn clm = new DataColumn("ProjectOwner");
                    clm.DefaultValue = ProjectOwner.Text;
                    tbl.Columns.Add(clm);
                    clm = new DataColumn("InStoreStartDate");
                    clm.DefaultValue = "0";
                    tbl.Columns.Add(clm);
                    clm = new DataColumn("CustomerPulsingPlanPriorityID");
                    clm.DefaultValue = "0";
                    tbl.Columns.Add(clm);
                    clm = new DataColumn("AllocatedCpnBudget");
                    clm.DefaultValue = "0";
                    tbl.Columns.Add(clm);
                    clm = new DataColumn("comments");
                    clm.DefaultValue = "";
                    tbl.Columns.Add(clm);
                    ProjectDataAccess.MergeProjectToCustomer(ProjectID.Text, tbl, Session["CurrentUserName"].ToString());
                    return "";
                }
            }
        }
    }

    //public string UpdateProject()
    //{
    //    if (Session["SelectedSubBrandItems"] == null) //check for session expiry
    //    {
    //        Response.Redirect("Project.aspx");
    //        return "";
    //    }
    //    else
    //    {
    //        //Main data
    //        int intMessage = ProjectDataAccess.UpdateProject(SelectProject);
    //        if (intMessage == 0)
    //        {
    //            return "Duplicate project name";
    //        }
    //        else
    //        {
    //            if (intMessage == -1)
    //            {
    //                return "Invalid project owner";
    //            }
    //            else
    //            {
    //                //SubBrands - clear and recreate
    //                ProjectDataAccess.ClearProjectToSubBrand(Convert.ToInt32(ProjectID.Text));
    //                DataTable tbl = (DataTable)Session["SelectedSubBrandItems"];
    //                foreach (DataRow r in tbl.Rows)
    //                {
    //                    ProjectDataAccess.InsertProjectToSubBrand(Convert.ToInt32(ProjectID.Text), Convert.ToInt32(r["SubBrandID"]), Convert.ToDouble(r["PropnPerCent"]) / 100, Session["CurrentUserName"].ToString());
    //                }
    //                //Customers - insert if not already existing (no deletion initially) 
    //                tbl = (DataTable)Session["SelectedCustomerItems"];
    //                foreach (DataRow r in tbl.Rows)
    //                {
    //                    ProjectDataAccess.InsertProjectToCustomer(Convert.ToInt32(ProjectID.Text), Convert.ToInt32(r["ID"]), r["IONumber"].ToString(), "", 0, 0, 0, 0, "", "", Session["CurrentUserName"].ToString());
    //                }
    //                //Clear
    //                ClearForm();
    //                return "";
    //            }
    //        }
    //    }
    //}

    public string CreateProject()
    {
        if (Session["SelectedSubBrandItems"] == null) //check for session expiry
        {
            Response.Redirect("Project.aspx");
            return "";
        }
        else
        {
            DataTable tblCustomer = (DataTable)Session["SelectedCustomerItems"];
            //validation check at least one customer is selected
            if (tblCustomer.Rows.Count == 0)
            {
                return "You must select at least 1 customer";
            }
            else
            {
                //Main data
                int intProjectID = ProjectDataAccess.InsertProject(SelectProject);
                ProjectID.Text = intProjectID.ToString();
                //trap duplicate project name
                if (intProjectID == 0)
                {
                    return "Duplicate project name.";
                }
                else
                {

                    if (intProjectID == -1)
                    {
                        return "Invalid project owner";
                    }
                    else
                    {
                        DataTable tbl = (DataTable)Session["SelectedSubBrandItems"];
                        ProjectDataAccess.MergeProjectToSubBrand(ProjectID.Text, tbl, Session["CurrentUserName"].ToString());
                        tbl = (DataTable)Session["SelectedCustomerItems"];

                        DataColumn clm = new DataColumn("ProjectOwner");
                        clm.DefaultValue = ProjectOwner.Text;
                        tbl.Columns.Add(clm);
                        clm = new DataColumn("InStoreStartDate");
                        clm.DefaultValue = "0";
                        tbl.Columns.Add(clm);
                        clm = new DataColumn("CustomerPulsingPlanPriorityID");
                        clm.DefaultValue = "0";
                        tbl.Columns.Add(clm);
                        clm = new DataColumn("AllocatedCpnBudget");
                        clm.DefaultValue = "0";
                        tbl.Columns.Add(clm);
                        clm = new DataColumn("comments");
                        clm.DefaultValue = "";
                        tbl.Columns.Add(clm);
                        String Message = ProjectDataAccess.MergeProjectToCustomer(ProjectID.Text, tbl, Session["CurrentUserName"].ToString());
                        //Clear
                        ClearForm();
                        return Message;
                    }

                }
            }
        }
    }

    public void ToggleControlState(bool state)
    {
        //enable or disable textboxes and dropdowns
        foreach (Control c in Controls)
        {
            if (c is TextBox && c != LastUpdatedDate && c != LastUpdatedBy && c != CreatedDate && c != CreatedBy && c != PulsingPlanTiming)//Added PulsingPlanTiming
            {
                ((TextBox)c).Enabled = state; 
            }
            if (c is DropDownList) { ((DropDownList)c).Enabled = state; }
        }
        //other controls
        grdCustomerID.Enabled = state;
        grdSubBrandID.Enabled = state;
        cmdAddSubBrand.Enabled = state;
        cmdRemoveSubBrand.Enabled = state;
        cmdAddCustomer.Enabled = state;
        //Akira Changes to add multiselect dropdown
        //If enabled is true, do not show PulsingPlanTiming text box, instead show multiselect dropdown and vice-versa
        PulsingPlanTiming.Visible = !state;
        uc_PulsingPlanTiming.Visible = state;
        //End of Akira changes
        if (ProjectID.Text == "0") //New
        {
            cmdRemoveCustomer.Enabled = state;
        }
        if (ProjectID.Text != "0" && state == false) //Edit
        {
            cmdRemoveCustomer.Enabled = false;
        }
    }

    protected void ClearControls()
    {
        //remove data from controls
        ProjectID.Text = "";
        ProjectName.Text = "";
        //CountryID.SelectedValue = "0";
        BudgetResponsibilityAreaID.SelectedValue = "0";
        ProjectYear.SelectedValue = "0";
        ProjectDescription.Text = "";
        PulsingPlanPriorityID.SelectedValue = "0";
        ProjectOwner.Text = "";
        PulsingPlanTiming.Text = "";
        ProjectObjective.Text = "";
        KPIs.Text = "";
        LastUpdatedBy.Text = String.Empty;
        LastUpdatedDate.Text = String.Empty;
        CreatedBy.Text = String.Empty;
        CreatedDate.Text = String.Empty;
        //Clear brand-markets
        grdSubBrandID.DataSource = "";
        grdSubBrandID.DataBind();
        Session["SelectedSubBrandItems"] = null;
        txtTotalProportion.Text = "0";
        cboAddSubBrandID.SelectedValue = "0";
        txtProportion.Text = "";
        //Clear customers
        grdCustomerID.DataSource = "";
        grdCustomerID.DataBind();
        Session["SelectedCustomerItems"] = null;
        cboAddCustomerID.SelectedValue = "0";
        txtIONumber.Text = "";
        cboPulsingPlanTiming.DeselectAllValues();
    }

    public void ClearForm()
    {
        ClearControls();
        ToggleControlState(false);
    }

    public void NewRecord()
    {
        ClearControls();
        //initialise selected subbrand table
        DataTable tbl = new DataTable();
        tbl.Columns.Add("SubBrandID");
        tbl.Columns.Add("SubBrandName");
        tbl.Columns.Add("PropnPerCent");
        Session["SelectedSubBrandItems"] = tbl;
        //initialise selected customer table
        tbl = new DataTable();
        tbl.Columns.Add("ID");
        tbl.Columns.Add("EntityName");
        tbl.Columns.Add("IONumber");
        tbl.Columns.Add("AllocatedAPBudget");
        tbl.Columns.Add("AllocatedTCCBudget");
        Session["SelectedCustomerItems"] = tbl;

        ProjectID.Text = "0";
        ProjectName.Focus();
        //enable controls (must be after ProjectID is populated)
        ToggleControlState(true);
    }


    protected void AddSelectedSubBrand(int intSubBrandID, double dblProportion)
    {
        DataTable tbl = (DataTable)Session["SelectedSubBrandItems"];

        //loop through tbl to see if record with this SubBrandID has already been added (to prevent duplication)
        bool duplicate = false;
        for (int i = tbl.Rows.Count - 1; i >= 0; i--)
        {
            DataRow dr = tbl.Rows[i];
            string dtitem = dr["SubBrandID"].ToString();
            if (dtitem == intSubBrandID.ToString())
            {
                duplicate = true;
            }
        }

        if (!duplicate)
        {
            //add the selected item to the table
            DataRow dr = tbl.NewRow();
            dr["SubBrandID"] = intSubBrandID;
            dr["SubBrandName"] = Common.ADOLookup("SubBrandName", "tblSubBrand", "SubBrandID = " + intSubBrandID.ToString());
            dr["PropnPerCent"] = dblProportion;
            tbl.Rows.Add(dr);
        }

        grdSubBrandID.DataSource = tbl;
        grdSubBrandID.DataBind();
        //store basket table in session variable
        Session["SelectedSubBrandItems"] = tbl;
        //clear fields
        cboAddSubBrandID.SelectedValue = "0";
        txtProportion.Text = "";
    }

    protected void AddSelectedCustomer(int intCustomerID, string strIONumber)
    {
        DataTable tbl = (DataTable)Session["SelectedCustomerItems"];

        //loop through tbl to see if record with this CustomerID has already been added (to prevent duplication)
        bool duplicate = false;
        for (int i = tbl.Rows.Count - 1; i >= 0; i--)
        {
            DataRow dr = tbl.Rows[i];
            string dtitem = dr["ID"].ToString();
            if (dtitem == intCustomerID.ToString())
            {
                duplicate = true;
            }
        }

        if (!duplicate)
        {
            //add the selected item to the table
            DataRow dr = tbl.NewRow();
            dr["ID"] = intCustomerID;
            dr["EntityName"] = Common.ADOLookup("CustomerName", "tblCustomer", "CustomerID = " + intCustomerID.ToString());
            dr["IONumber"] = strIONumber;
            dr["AllocatedAPBudget"] = "0";
            dr["AllocatedTCCBudget"] = "0";
            tbl.Rows.Add(dr);
        }

        grdCustomerID.DataSource = tbl;
        grdCustomerID.DataBind();
        //store basket table in session variable
        Session["SelectedCustomerItems"] = tbl;
        //clear form fields
        cboAddCustomerID.SelectedValue = "0";
        txtIONumber.Text = "";
    }

    protected void RemoveSubBrand()
    //remove the selected line item from the grid
    {
        try //because an item may not be selected in the grid or the Session variable may not be valid
        {
            //loop through the table and delete if criteria found
            DataTable tbl = (DataTable)Session["SelectedSubBrandItems"];

            //mark record for deletion
            for (int i = tbl.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = tbl.Rows[i];
                string dtitem = dr["SubBrandID"].ToString();
                if (dtitem == grdSubBrandID.SelectedValue.ToString())
                {
                    tbl.Rows[i].Delete();
                }
            }

            //delete
            tbl.AcceptChanges();

            grdSubBrandID.DataSource = tbl;
            grdSubBrandID.DataBind();
            //recalculate remaining proportion if no rows left (if there are rows this is calculated by the onrowdatabound event
            if (grdSubBrandID.Rows.Count == 0)
            {
                txtTotalProportion.Text = "0";
            }
            //store basket table in session variable
            Session["SelectedSubBrandItems"] = tbl;
        }
        catch { }
    }

    protected void RemoveCustomer()
    //remove the selected item from the grid
    {
        try //because an item may not be selected in the grid or the Session variable may not be valid
        {
            //loop through the table and delete if criteria found
            DataTable tbl = (DataTable)Session["SelectedCustomerItems"];

            //mark record for deletion
            for (int i = tbl.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = tbl.Rows[i];
                string dtitem = dr["ID"].ToString();
                if (dtitem == grdCustomerID.SelectedValue.ToString())
                {
                    tbl.Rows[i].Delete();
                }
            }

            //delete
            tbl.AcceptChanges();

            grdCustomerID.DataSource = tbl;
            grdCustomerID.DataBind();
            //store basket table in session variable
            Session["SelectedCustomerItems"] = tbl;
        }
        catch { }
    }

#endregion

#region dropdowns

    protected void FillProjectYear()
    //fill year dropdownlist allowing entry of the first project of a new year 
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillYearPlusOne";
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = ProjectYear;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillPulsingPlanPriority()
    //fill PulsingPlanPriority dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillPulsingPlanPriority";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = PulsingPlanPriorityID;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillBudgetResponsibilityArea()
    //Fill BudgetResponsibilityArea dropdownlist checking permissions
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillBudgetResponsibilityArea";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = BudgetResponsibilityAreaID;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillSubBrand()
    //Fill dropdownlist of SubBrands for which current user has permissions
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillSubBrand";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboAddSubBrandID;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillCustomer()
    //Fill dropdownlist of Customers for which user has permissions
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillCustomer";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        cmd.Parameters.AddWithValue("@IncludeInactive", 0);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboAddCustomerID;
        d.DataSource = tbl;
        d.DataBind();
    }

    //Changed
    protected void FillPulsingPlanTiming()
    //fill year dropdownlist allowing entry of the first project of a new year 
    {
        DataTable dt1 = new DataTable();
        dt1.Columns.Add(new DataColumn("EntityName", typeof(System.String)));
        dt1.Columns.Add(new DataColumn("ID", typeof(System.String)));
        dt1.Rows.Add(new string[] { "Jan", "01" });
        dt1.Rows.Add(new string[] { "Feb", "02" });
        dt1.Rows.Add(new string[] { "Mar", "03" });
        dt1.Rows.Add(new string[] { "Apr", "04" });
        dt1.Rows.Add(new string[] { "May", "05" });
        dt1.Rows.Add(new string[] { "Jun", "06" });
        dt1.Rows.Add(new string[] { "Jul", "07" });
        dt1.Rows.Add(new string[] { "Aug", "08" });
        dt1.Rows.Add(new string[] { "Sep", "09" });
        dt1.Rows.Add(new string[] { "Oct", "10" });
        dt1.Rows.Add(new string[] { "Nov", "11" });
        dt1.Rows.Add(new string[] { "Dec", "12" });
        cboPulsingPlanTiming.AddDataToList(dt1, "EntityName", "ID");
    }

#endregion

#region subformevents
    protected void cboPulsingPlanTiming_OkButtonClicked(object sender, EventArgs e)
    {
        string[] items = cboPulsingPlanTiming.getSelectedID().Split(',');
        PulsingPlanTiming.Text = items[0] + ',' + items[items.Length - 1];
    }

    protected void cmdAddSubBrand_Click(object sender, EventArgs e)
    {
        AddSelectedSubBrand(Convert.ToInt32(cboAddSubBrandID.SelectedValue), Convert.ToDouble(txtProportion.Text));
    }

    protected void cmdAddCustomer_Click(object sender, EventArgs e)
    {
        AddSelectedCustomer(Convert.ToInt32(cboAddCustomerID.SelectedValue), txtIONumber.Text);
    }

    protected void cmdRemoveSubBrand_Click(object sender, EventArgs e)
    {
        RemoveSubBrand();
    }

    protected void cmdRemoveCustomer_Click(object sender, EventArgs e)
    {
        RemoveCustomer();
    }

#endregion

}
