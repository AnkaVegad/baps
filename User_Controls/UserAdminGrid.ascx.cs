﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

public partial class User_Controls_UserAdminGrid : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //populate selection dropdowns
            FillActiveIndicatorFilter();
            FillUserLevelFilter();
            FillBudgetResponsibilityAreaFilter();
        }
        //populate the user grid
        string strSQL = "SELECT TOP (100) PERCENT * FROM qryUser ";
        strSQL += "WHERE UserID > 0 ";
        if (ActiveIndicatorFilter.SelectedValue != "0")
        {
            strSQL += "AND ActiveIndicator = " + ActiveIndicatorFilter.SelectedValue + " ";
        }
        if (BudgetResponsibilityAreaID.SelectedValue != "0")
        {
            strSQL += "AND EXISTS(SELECT 'X' FROM qryUserAuthorisations WHERE qryUserAuthorisations.UserName = qryUser.UserName AND qryUserAuthorisations.BudgetResponsibilityAreaID = " + BudgetResponsibilityAreaID.SelectedValue + ") ";
        }
        if (UserLevelIDFilter.SelectedValue != "-1")
        {
            strSQL += "AND UserLevelID = " + UserLevelIDFilter.SelectedValue + " ";
        }
        //amended 08/12/11: only show where current user has BRA in common with target user
        strSQL += "AND EXISTS(SELECT 'X' FROM qryUserAuthorisations WHERE qryUserAuthorisations.UserName = '" + Session["CurrentUserName"] + "' AND qryUserAuthorisations.BudgetResponsibilityAreaID IN (SELECT BudgetResponsibilityAreaID FROM qryUserAuthorisations WHERE qryUserAuthorisations.UserName=qryUser.UserName))";
        //amended 08/12/11: only show where current user is higher or equal user level to target user - reverted 19/01/12
        //strSQL += "AND UserLevelID >= " + Session["CurrentUserLevelID"] + " ";
        strSQL += "ORDER BY UserName;";
        sds.SelectCommand = strSQL;
        grd.DataBind();
    }

#region gridevents

    protected void grd_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Session["SelectedProjectToCustomerID"] = Convert.ToInt32(grd.SelectedValue); remmed out 19/01/12 as not relevant
    }

    protected void grd_PageIndexChanged(object sender, EventArgs e)
    {
        grd.SelectedIndex = -1;
    }

    protected void grd_Sorted(object sender, EventArgs e)
    {
        grd.SelectedIndex = -1;
    }

    protected void grd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;
            //highlight on hover
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            e.Row.Attributes["onclick"] = cs.GetPostBackClientHyperlink(this.grd, "Select$" + e.Row.RowIndex);
        }
    }

#endregion

#region properties

    public string SelectedValue
    {
        get
        {
            try
            {
                return grd.SelectedValue.ToString();
            }
            catch { return "0"; }
        }
    }

    //public string HeaderText
    //{
    //    set { lblHeader.Text = value; }
    //}

#endregion

#region dropdowns
    protected void FillActiveIndicatorFilter()
    //populate the ActiveIndicator Filter dropdown
    {
        ActiveIndicatorFilter.Items.Add(new ListItem("<All>", "0"));
        ActiveIndicatorFilter.Items.Add(new ListItem("Yes", "1"));
        ActiveIndicatorFilter.Items.Add(new ListItem("No", "2"));
    }

    protected void FillUserLevelFilter()
    //populate the UserLevelID dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "FillUserLevel";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        cmd.Parameters.AddWithValue("@CurrentUserLevelID", Session["CurrentUserlevelID"]);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = UserLevelIDFilter;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillBudgetResponsibilityAreaFilter()
    //fill BudgetResponsibilityAreaID filter dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "FillBudgetResponsibilityArea";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = BudgetResponsibilityAreaID;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void BudgetResponsibilityAreaID_TextChanged(object sender, EventArgs e)
    {
        Session["SelectedBudgetResponsibilityAreaID"] = BudgetResponsibilityAreaID.SelectedValue;
    }

#endregion
}