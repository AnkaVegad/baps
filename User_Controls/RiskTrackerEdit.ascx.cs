﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Controls_RiskTrackerEdit : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //populate the activity indicator dropdown
            OpportunityOrRisk.Items.Add(new ListItem("", "0"));
            OpportunityOrRisk.Items.Add(new ListItem("Risk", "1"));
            OpportunityOrRisk.Items.Add(new ListItem("Opportunity", "2"));
            //populate other dropdowns
            FillSpendType();
            FillMonth();
        }
    }

#region properties

    public RiskTracker SelectRiskTracker
    {
        get
        {
            RiskTracker h = new RiskTracker();
            h.RiskTrackerID = Convert.ToInt32(RiskTrackerID.Text);
            //h.ProjectToCustomerID = Convert.ToInt32(ProjectToCustomerID.Text);
            h.SpendTypeID = Convert.ToInt32(SpendTypeID.SelectedValue);
            h.ProjectMonth = ProjectMonth.SelectedValue;
            h.OpportunityOrRisk = Convert.ToInt16(OpportunityOrRisk.SelectedValue);
            h.KeyDrivers = KeyDrivers.Text;
            h.FinancialImpact = Convert.ToInt32(FinancialImpact.Text);
            h.CreatedBy = Session["CurrentUserName"].ToString();
            return h;
        }
    }

//    public string HeaderText
//    {
//        set { lblHeader.Text = value; }
//    }

#endregion

#region methods

    public void PopulateForm(int intRiskTrackerID)
    {
        RiskTrackerID.Text = intRiskTrackerID.ToString();
        DataTable tbl = VarianceDataAccess.SelectRiskTracker(intRiskTrackerID);
        DataRow dr = tbl.Rows[0];
        ProjectToCustomerID.Text = dr["ProjectToCustomerID"].ToString();
        CustomerName.Text = dr["CustomerName"].ToString();
        ProjectName.Text = dr["ProjectName"].ToString();
        SpendTypeID.SelectedValue = dr["SpendTypeID"].ToString();
        ProjectMonth.SelectedValue = dr["ProjectMonth"].ToString();
        OpportunityOrRisk.SelectedValue = dr["OpportunityOrRisk"].ToString();
        KeyDrivers.Text = dr["KeyDrivers"].ToString();
        FinancialImpact.Text = dr["FinancialImpact"].ToString();
        SpendTypeID.Focus();
        //enable controls
        if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 15)
        {
            ToggleControlState(true);
        }
    }

    public string UpdateRiskTracker()
    {
        if (Session["CurrentUserLevelID"] == null) //check for session expiry
        {
            Response.Redirect("ProjectView.aspx");
            return "";
        }
        else
        {
            //Main data
            RiskTracker h = SelectRiskTracker;
            int intResult = VarianceDataAccess.UpdateRiskTracker(h);
            if (intResult == 0)
            {
                return "Duplicate Risk Tracker values.";
            }
            else
            {
                //Clear
                ClearForm();
                return "";
            }
        }
    }

    public RiskTracker InsertRiskTracker()
    {
        //if (Session["CurrentUserLevelID"] == null) //check for session expiry
        //{
        //    Response.Redirect("ProjectView.aspx");
        //    //return "";
        //}
        //else
        //{
            //Main data
            RiskTracker h = SelectRiskTracker;
            
            //h.CreatedBy = Session["CurrentUserName"].ToString();
            //int intRiskTrackerID = VarianceDataAccess.InsertRiskTracker(h);
            ////trap duplicate RiskTracker values
            //if (intRiskTrackerID == 0)
            //{
            //    return "Duplicate Risk Tracker values.";
            //}
            //else
            //{
                //Clear
                //ClearForm();
                return h;
            //}
        //}
    }

    protected void ToggleControlState(bool state)
    {
        //enable or disable textboxes and dropdowns
        foreach (Control c in Controls)
        {
            if (c is TextBox) { ((TextBox)c).Enabled = state; }
            if (c is DropDownList) { ((DropDownList)c).Enabled = state; }
        }
    }

    public void ClearControls()
    {
        //enable or disable textboxes and dropdowns
        foreach (Control c in Controls)
        {
            if (c is TextBox) { ((TextBox)c).Text = ""; }
            if (c is DropDownList) { ((DropDownList)c).SelectedValue = "0"; }
        }
    }

    public void ClearForm()
    {
        ClearControls();
        ToggleControlState(false);
    }

    public void NewRecord()
    {
        ClearControls();
        RiskTrackerID.Text = "0";
        SpendTypeID.Focus();
        //enable controls
        ToggleControlState(true);
    }

#endregion

#region dropdowns

    protected void FillSpendType()
    //populate the spendtype dropdownlists
    {
        DataTable tbl = Common.FillDropDown("FillSpendType");
        DropDownList d = SpendTypeID;
        d.DataSource = tbl;
        d.DataBind();
        d = SpendTypeID;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillMonth()
    //fill months dropdownlist
    {
        //Get a data table object containing the values
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillMonth";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = ProjectMonth;
        d.DataSource = tbl;
        d.DataBind();
    }

#endregion
}
