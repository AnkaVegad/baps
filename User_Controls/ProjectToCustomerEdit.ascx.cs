﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Controls_ProjectToCustomerEdit : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //fill dropdowns
            FillPulsingPlanPriority();
            //FillCustomer(); moved to methods 06/12/12
        }
        //enable/disable controls
        IONumber.Enabled = false;
        AllocatedAPBudget.Enabled = false;
        AllocatedTCCBudget.Enabled = false;
        AllocatedCpnBudget.Enabled = false;
        Comments.Enabled = false;
        InStoreStartDate.Enabled = false;
        PulsingPlanPriorityID.Enabled = false;
        ProjectOwner.Enabled = false;
        txtIONumberSecondary.Enabled = false;
        //txtAllocatedBudget.Enabled = false;    
        txtProportion.Enabled = false;
//        txtTotalProportion.Enabled = false;
        cmdAddSecondaryIO.Enabled = false;
        //if (Common.CheckAuthorisations(Convert.ToInt32(Session["SelectedProjectToCustomerID"]), Session["CurrentUserName"].ToString()) > 0)
        //{
        //    //superusers may edit secondary IOs (added AH 03/03/13)
        //    if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 5)
        //    {
        //        txtIONumberSecondary.Enabled = true;
        //        //txtAllocatedBudget.Enabled = true;
        //        txtProportion.Enabled = true;
        //        cmdAddSecondaryIO.Enabled = true;
        //        cmdRemoveSecondaryIO.Enabled = true;
        //    }
        //    //administrators and above may edit budgets (added AH 06/12/12 - and IONumber)
        //    if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 15)
        //    {
        //        IONumber.Enabled = true;
        //        AllocatedAPBudget.Enabled = true;
        //        AllocatedTCCBudget.Enabled = true;
        //        AllocatedCpnBudget.Enabled = true;
        //        Comments.Enabled = true;
        //        ProjectOwner.Enabled = true;
        //    }
        //    //all except read-only may edit other attributes
        //    if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 35)
        //    {
        //        InStoreStartDate.Enabled = true;
        //        PulsingPlanPriorityID.Enabled = true;
        //    }
        //}
        //txtIONumberSecondary.Attributes.Add("onchange", "Javascript:calcProportion();");
    }

#region properties

    public string ProjectToCustomerIDValue
    {
        get { return ProjectToCustomerID.Text; }
    }

#endregion

#region methods

    protected void MsgBox(string strTitle, string strMessage)
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), strTitle, "alert('" + Common.Left(strMessage, 35) + "');", true);
    }

    public void PopulateForm(int intProjectToCustomerID)
    {
        FillCustomerAll();
        ProjectToCustomerID.Text = intProjectToCustomerID.ToString();
        DataTable tbl = ProjectDataAccess.SelectProjectToCustomer(intProjectToCustomerID);
        DataRow r = tbl.Rows[0];
        //text boxes (excluding comment which always starts blank)
//        foreach (Control c in Controls)
//        {
//            if (c is TextBox && c != Comments) { ((TextBox)c).Text = r[c.ID].ToString(); }
//        }
        IONumber.Text = r["IONumber"].ToString();
        InStoreStartDate.Text = r["InStoreStartDate"].ToString();
        ProjectOwner.Text = r["ProjectOwner"].ToString();
        AllocatedAPBudget.Text = r["AllocatedAPBudget"].ToString();
        AllocatedTCCBudget.Text = r["AllocatedTCCBudget"].ToString();
        AllocatedCpnBudget.Text = r["AllocatedCpnBudget"].ToString();
        
        //Label
        ProjectName.Text = r["ProjectName"].ToString();
        //dropdowns
        CustomerID.Enabled = false;
        CustomerID.SelectedValue = r["CustomerID"].ToString();
        if (r["CustomerPulsingPlanPriorityID"].ToString() == String.Empty) { PulsingPlanPriorityID.SelectedValue = "0"; }
        else { PulsingPlanPriorityID.SelectedValue = r["CustomerPulsingPlanPriorityID"].ToString(); }
        lblHeader.Text = "Edit Project Details for Customer";
        lblComments.Visible = true;
        Comments.Visible = true;
        
        //populate grdSecondaryIO
        DataTable tbl2 = ProjectDataAccess.FillSecondaryIO(r["IONumber"].ToString());
        Session["SelectedSecondaryIOItems"] = tbl2;
        grdSecondaryIO.DataSource = tbl2;
        grdSecondaryIO.DataBind();
    }

    public void ClearControls()
    {
        foreach (Control c in Controls)
        {
            if (c is TextBox) { ((TextBox)c).Text = ""; }
            if (c is DropDownList) { ((DropDownList)c).SelectedValue = "0"; }
        }
        //Clear Secondary IOs
        grdSecondaryIO.DataSource = "";
        grdSecondaryIO.DataBind();
        Session["SelectedSecondaryIOItems"] = null;
        txtTotalProportion.Text = "0";
    }

    public void NewRecord(int intProjectID)
    {
        ClearControls();
        ProjectToCustomerID.Text = "0";
        ProjectID.Text = intProjectID.ToString();
        ProjectName.Text = Common.ADOLookup("ProjectName", "tblProject", "ProjectID = " + ProjectID.Text);
        FillCustomer();
        CustomerID.Enabled = true;
        lblHeader.Text = "New Project Details for Customer";
        lblComments.Visible = false;
        Comments.Visible = false;
        //initialise selected Secondary IO table
        DataTable tbl = new DataTable();
        tbl.Columns.Add("IONumberPrimary");
        tbl.Columns.Add("IONumberSecondary");
        tbl.Columns.Add("AllocatedBudget");
        tbl.Columns.Add("Proportion");
        Session["SelectedSecondaryIOItems"] = tbl;
    }

    public string CreateRecord()
    {
        string strMessage = ProjectDataAccess.InsertProjectToCustomer(Convert.ToInt32(ProjectID.Text), Convert.ToInt32(CustomerID.SelectedValue), IONumber.Text, InStoreStartDate.Text, Convert.ToInt32(PulsingPlanPriorityID.SelectedValue), Convert.ToInt32(AllocatedAPBudget.Text), Convert.ToInt32(AllocatedTCCBudget.Text), Convert.ToInt32(AllocatedCpnBudget.Text), ProjectOwner.Text, Comments.Text, Session["CurrentUserName"].ToString());
        if (strMessage == "[none]")
        {
            //Secondary IOs - create
            DataTable tbl = (DataTable)Session["SelectedSecondaryIOItems"];
            foreach (DataRow r in tbl.Rows)
            {
                ProjectDataAccess.InsertSecondaryIO(IONumber.Text, r["IONumberSecondary"].ToString(), 0, Convert.ToDouble(r["Proportion"]) / 100, Session["CurrentUserName"].ToString());
                //ProjectDataAccess.InsertSecondaryIO(IONumber.Text, r["IONumberSecondary"].ToString(), Convert.ToInt32(r["AllocatedBudget"]), Convert.ToDouble(r["Proportion"]) / 100, Session["CurrentUserName"].ToString());
            }
            ClearControls();
        }
        return strMessage;
    }

    public string UpdateRecord()
    {
        string strMessage = ProjectDataAccess.UpdateProjectToCustomer(Convert.ToInt32(ProjectToCustomerID.Text), IONumber.Text, InStoreStartDate.Text, PulsingPlanPriorityID.SelectedValue, Convert.ToInt32(AllocatedAPBudget.Text), Convert.ToInt32(AllocatedTCCBudget.Text), Convert.ToInt32(AllocatedCpnBudget.Text), ProjectOwner.Text, Comments.Text, Session["CurrentUserName"].ToString());
        if (strMessage == "[none]")
        {
            //Secondary IOs - clear and recreate
            ProjectDataAccess.ClearSecondaryIOs(IONumber.Text);
            DataTable tbl = (DataTable)Session["SelectedSecondaryIOItems"];
            foreach (DataRow r in tbl.Rows)
            {
                ProjectDataAccess.InsertSecondaryIO(IONumber.Text, r["IONumberSecondary"].ToString(), 0, Convert.ToDouble(r["Proportion"]) / 100, Session["CurrentUserName"].ToString());
                //ProjectDataAccess.InsertSecondaryIO(IONumber.Text, r["IONumberSecondary"].ToString(), Convert.ToInt32(r["AllocatedBudget"]), Convert.ToDouble(r["Proportion"]) / 100, Session["CurrentUserName"].ToString());
            }
            ClearControls();
        }
        return strMessage;
    }

    protected string AddSecondaryIO(string strIONumberPrimary, string strIONumberSecondary, double dblAllocatedBudget, double dblProportion)
    {
        bool duplicate = false;
        string strMessage = "OK";
        DataTable tbl = (DataTable)Session["SelectedSecondaryIOItems"];

        //Check IO does not already exist as a primary
        if (Common.ADOLookupSafe("IONumber", "tblProjectToCustomer", "IONumber = '" + strIONumberSecondary + "'") != String.Empty)
        {
            duplicate = true;
            strMessage = "Already exists as Primary";
        }
        
        //loop through tbl to see if record with this SecondaryIO has already been added (to prevent duplication)
        for (int i = tbl.Rows.Count - 1; i >= 0; i--)
        {
            DataRow dr = tbl.Rows[i];
            string dtitem = dr["IONumberSecondary"].ToString();
            if (dtitem == strIONumberSecondary.ToString())
            {
                duplicate = true;
            }
        }

        if (!duplicate)
        {
            //add the selected item to the table
            DataRow dr = tbl.NewRow();
            dr["IONumberPrimary"] = strIONumberPrimary;
            dr["IONumberSecondary"] = strIONumberSecondary;
            //dr["AllocatedBudget"] = dblAllocatedBudget;
            dr["Proportion"] = dblProportion;
            tbl.Rows.Add(dr);
        }

        grdSecondaryIO.DataSource = tbl;
        grdSecondaryIO.DataBind();
        //store basket table in session variable
        Session["SelectedSecondaryIOItems"] = tbl;
        //clear fields
        txtIONumberSecondary.Text = "";
        //txtAllocatedBudget.Text = "";
        txtProportion.Text = "";
        return strMessage;
    }

    protected void RemoveSecondaryIO()
    //remove the selected line item from the grid
    {
        try //because an item may not be selected in the grid or the Session variable may not be valid
        {
            //loop through the table and delete if criteria found
            DataTable tbl = (DataTable)Session["SelectedSecondaryIOItems"];

            //mark record for deletion
            for (int i = tbl.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = tbl.Rows[i];
                string dtitem = dr["IONumberSecondary"].ToString();
                if (dtitem == grdSecondaryIO.SelectedValue.ToString())
                {
                    tbl.Rows[i].Delete();
                }
            }

            //delete
            tbl.AcceptChanges();

            grdSecondaryIO.DataSource = tbl;
            grdSecondaryIO.DataBind();
            //recalculate remaining proportion if no rows left (if there are rows this is calculated by the onrowdatabound event
            if (grdSecondaryIO.Rows.Count == 0)
            {
                txtTotalProportion.Text = "0";
            }
            //store basket table in session variable
            Session["SelectedSecondaryIOItems"] = tbl;
        }
        catch { }
    }
#endregion

#region filldropdowns

    protected void FillPulsingPlanPriority()
    //fill PulsingPlanPriority dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillPulsingPlanPriority";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = PulsingPlanPriorityID;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillCustomer()
    //fill customer dropdownlist for current user
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillCustomer";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        cmd.Parameters.AddWithValue("@IncludeInactive", 1);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = CustomerID;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillCustomerAll()
    //fill customer dropdownlist for current user
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillCustomer";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        //cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        cmd.Parameters.AddWithValue("@IncludeInactive", 1);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = CustomerID;
        d.DataSource = tbl;
        d.DataBind();
    }

#endregion

#region gridevents

    double dblTotal = 0;

    protected void grdSecondaryIO_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            //calculate total SecondaryIO proportion
            double dblCurrent = Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "Proportion"));
            //increment the total
            dblTotal += dblCurrent;

            //highlight on hover
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            e.Row.Attributes["onclick"] = cs.GetPostBackClientHyperlink(this.grdSecondaryIO, "Select$" + e.Row.RowIndex);
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[1].Text = "Total";
            //display the total in grid and in hidden control
            e.Row.Cells[2].Text = dblTotal.ToString();
            txtTotalProportion.Text = dblTotal.ToString();
            //colour code the cell 
            if (dblTotal == 100) { e.Row.Cells[2].BackColor = Color.LightGreen; } else { e.Row.Cells[2].BackColor = Color.PapayaWhip; }
        }
    }

#endregion
    
#region subformevents

    protected void cmdAddSecondaryIO_Click(object sender, EventArgs e)
    {
        string strMessage = AddSecondaryIO(IONumber.Text, txtIONumberSecondary.Text, 0, Convert.ToDouble(txtProportion.Text));
        if (strMessage != "OK")
        {
            MsgBox("Error", strMessage);
        }
//        AddSecondaryIO(IONumber.Text, txtIONumberSecondary.Text, Convert.ToDouble(txtAllocatedBudget.Text), Convert.ToDouble(txtProportion.Text));
    }

    protected void cmdRemoveSecondaryIO_Click(object sender, EventArgs e)
    {
        RemoveSecondaryIO();
    }

    #endregion
}
