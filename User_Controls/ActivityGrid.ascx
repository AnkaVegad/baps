﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActivityGrid.ascx.cs" Inherits="User_Controls_ActivityGrid" %>
<!--Warning - page has been rebuilt for Axiom - do not copy to Prod-->

<%@ Register src="~/User_Controls/MultiselectDropdown.ascx" tagname="dropdown" tagprefix="uc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<style type="text/css">
    .auto-style1 {
        width: 131px;
    }
    .auto-style2 {
        width: 246px;
    }

</style>

<script type="text/javascript">
    $(function () {
        $("[id*=grd] td").bind("click", function () {
            var row = $(this).parent();
            var GridSelection = document.getElementById('ctl00$ContentPlaceHolder1$Grid1$SelectedActivityID');
            var index = -1;

            $("[id*=grd] tr").each(function () {
                if ($(this)[0] != row[0]) {
                    if (row[0].innerHTML.indexOf(">Total Forecast<") < 0 && row[0].innerHTML.indexOf(">Actuals<") < 0) {
                        index = index + 1;
                        $("td", this).removeClass("grid_selected");
                    }
                }
                else {
                    if (row[0].innerHTML.indexOf(">Total Forecast<") < 0 && row[0].innerHTML.indexOf(">Actuals<") < 0)
                        GridSelection.value = index;
                }
            });
            $("td", row).each(function () {
                if (row[0].innerHTML.indexOf(">Total Forecast<") < 0 && row[0].innerHTML.indexOf(">Actuals<") < 0) {
                    if (!$(this).hasClass("grid_selected")) {
                        $(this).addClass("grid_selected");
                    } else {
                        //Don't do anything. Code below to deselect row
                        //$(this).removeClass("grid_selected");
                    }
                }
            });
        });
    });
</script>

<script type="text/javascript">
    function setscroll(ctlID) {
        var panel = document.getElementById(ctlID + 'grid');
        var div_value = document.cookie.substring(document.cookie.indexOf("div_position=") + 13, document.cookie.length);
        panel.scrollTop = div_value.substring(0, div_value.indexOf(","));
        panel.scrollLeft = div_value.substring(div_value.indexOf(",") + 1, div_value.length);
    }
</script>
<script type="text/javascript">
    function savescroll(ctlID) {
        var panel = document.getElementById(ctlID + 'grid');
        document.cookie = "div_position=" + panel.scrollTop + ',' + panel.scrollLeft.toString() + ";";
    }
</script>

<asp:HiddenField ID="SelectedActivityID" runat="server" />
<div id="bodymain" runat="server">
    <!--Header and Filter Criteria-->
    <div id="resizable1" class="resizable ui-widget-content">
        <br />
        <asp:Label ID="lblHeader" Text="Activities" runat="server" CssClass="page_header" />
        <br />
        <br />
        <table border="0">
            <tr>
                <td colspan="2">
                    <asp:HiddenField id="ProjectID" runat="server"/>
                    <asp:HiddenField id="CustomerID" runat="server"/>
                    <asp:HiddenField id="ProjectToCustomerID" runat="server"/>
                </td>
            </tr><tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr><tr>
                <td>Project: </td>
                
            </tr>
            <tr><td colspan="2">
                    <asp:DropDownList id="cboProject" runat="server" width="317px" OnSelectedIndexChanged="cboProject_SelectedIndexChanged"
                        DataTextField="EntityName" DataValueField="ID" AutoPostBack="True" ForeColor="Black" Font-Bold="true" />
             
                </td></tr>
            <tr>
                <td>Area: </td>
                <td>
                    <asp:Label id="lblBudgetResponsibilityAreaName" runat="server" width="220px" CssClass="label_text"
                        Text="" Font-Bold="True" />
                    <asp:HiddenField id="BudgetResponsibilityAreaID" runat="server"/>
                </td>
            </tr><tr>
                <td>Currency: </td>
                <td>
                    <asp:Label id="lblCurrencyCode" runat="server" width="220px" CssClass="label_text"
                        Text="" Font-Bold="True" />
                </td>
            </tr><tr>
                <td>Internal Order: </td>
                <td>
                    <asp:Label id="lblIONumber" runat="server" width="220px" CssClass="label_text"
                        Text="" Font-Bold="True" />
                </td>
            </tr><tr>
                <td>Project Year: </td>
                <td>
                    <asp:Label id="lblProjectYear" runat="server" width="220px" CssClass="label_text"
                        Text="" Font-Bold="True" />
                </td>
            </tr><tr>
                <td>PP Priority: </td>
                <td>
                    <asp:Label id="lblPulsingPlanPriorityName" runat="server" width="220px" CssClass="label_text"
                        Text="" Font-Bold="True" />
                </td>
            </tr><tr>
                <td>Customer: </td>
                <td>
                    <asp:DropDownList id="cboCustomer" runat="server" width="226px" OnTextChanged="cboCustomer_TextChanged"
                        DataTextField="EntityName" DataValueField="ID" AutoPostBack="True" ForeColor="Black"/>
                </td>
            </tr><tr>
                <td>Activity Type:</td>
                <td>
                    <asp:DropDownList id="cboActivityType" runat="server" width="226px"
                        DataTextField="EntityName" DataValueField="ID" AutoPostBack="True" OnSelectedIndexChanged="cboActivityType_SelectedIndexChanged" ForeColor="Black" />
                </td>
            </tr><tr>
                <td>Spend Type:</td>
                <td>
                    <asp:DropDownList id="cboSpendType" runat="server" width="226px" OnSelectedIndexChanged="cboSpendType_SelectedIndexChanged"
                        DataTextField="EntityName" DataValueField="ID" AutoPostBack="True" ForeColor="Black"/>
                    <asp:HiddenField ID="SpendTypeID" runat="server" />
                </td>
            </tr><tr>
                <td><asp:Label id="Budget" runat="server" text="Budget:"/></td>
                <td>
                    <asp:Label id="lblBudget" runat="server" width="80px" CssClass="label_text" />
                </td>
            </tr><tr>
                <td><asp:Label id="Forecast" runat="server" text="Forecast:"/></td>
                <td>
                    <asp:Label id="lblForecast" runat="server" width="80px" CssClass="label_text" />
                </td>
            </tr><tr>
                <td><asp:Label id="Balance" runat="server" text="Balance:"/></td>
                <td>
                    <asp:Label id="lblBalance" runat="server" width="80px" CssClass="label_text" />
                </td>
            </tr><tr>
                <td><asp:Label id="FYView" runat="server" text="Full Year View:"/></td>
                <td>
                    <asp:Label id="lblFYView" runat="server" width="80px" CssClass="label_text" />
                </td>
            </tr><tr>
                <td><asp:Label id="Variance" runat="server" text="Variance:"/></td>
                <td>
                    <asp:Label id="lblVariance" runat="server" width="80px" CssClass="label_text" />
                </td>
            </tr>
        </table>
    </div>        

    <!--Search and Grid-->
    <div id="resizable2" class="ui-widget-content">
        
        <!--Data Source - required to take advantage of sorting and paging functionality of the GridView-->
        <asp:SqlDataSource ID="sds" runat="server" 
            ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
            FilterExpression="ActivityName Like '%{0}%'">
            <FilterParameters>
                <asp:ControlParameter ControlID="txtSearch" PropertyName="Text" />
            </FilterParameters>
        </asp:SqlDataSource>

        <div style="padding-top:10px">
        <!--Search-->
            <table>
                <tr>
                    <td class="auto-style1">
                        <asp:Label ID="Label1" Text="Select Grid Columns:" runat="server"></asp:Label>
                    </td>
                    <td class="auto-style2">
                        <uc1:dropdown ID="cboColumn" runat="server"/>
                        <asp:HiddenField ID="cboColumnHiddenField" runat="server" />
                        <asp:HiddenField ID="cboColumnIDHiddenField" runat="server" />
                    </td>
                    <td style="text-align:right">
                        <asp:Label ID="lblSearch" Text="Search Activity Name:" runat="server"></asp:Label>
                        <asp:TextBox ID="txtSearch" Runat="server" Width="180px" placeholder="Search Activity Name" OnTextChanged="txtSearch_TextChanged"/>
                        <asp:Button ID="cmdGo" Text="Go" Runat="server" CausesValidation="false"/>
                    </td>
                </tr>
            </table>
        </div>
        <div id="project_grid_rs" style="padding-top:10px; overflow-x:hidden; overflow-y:hidden; height:92%" >
        <!--Grid-->
        <asp:Panel id="grid" style="overflow-x:auto; overflow-y:auto; height:97%; position:relative; top: 1px; left: 0px;" runat="server">
           
             <asp:GridView ID="grd" runat="server" Height="20px"   width="98.6%"
                AutoGenerateColumns="False" GridLines="Vertical"
                RowStyle-Wrap="false" EmptyDataText="No Records Found" 
                HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
                SelectedRowStyle-Wrap="false"
                OnRowDataBound="grd_RowDataBound" 
                DataKeyNames="ActivityID" DataSourceID="sds" ShowFooter="true">
                <Columns>
                    <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="ActivityID" ReadOnly="True" Visible="false" />
                     <asp:TemplateField HeaderStyle-CssClass="GridHeader"  HeaderText="Files" SortExpression="Files">
                         <ItemTemplate>
                            <asp:ImageButton runat="server" ID="ImageButton1" height="20px" Width="20px" 
                                ImageUrl='<%# GetImageType((Eval("POID") == DBNull.Value?-1:(int)Eval("POID")), (int)Eval("Filecount")) %>' 
                                OnCommand="ImageButton1_Click" Visible='<%# IsFilePresent((int)Eval("Filecount")) %>' ImageAlign="Left" />
                         </ItemTemplate>
                     </asp:TemplateField>                  
                    <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="ActivityName" HeaderText="Activity Name" SortExpression="ActivityName" />
                    <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="SpendTypeName" HeaderText="Spend" SortExpression="SpendTypeName" />
                    <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="ActivityTypeName" HeaderText="Activity Type" SortExpression="ActivityTypeName" />
                    <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="DocumentNumber" HeaderText="Document" SortExpression="DocumentNumber" />
                    <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="ActivityStatusName" HeaderText="Status" SortExpression="ActivityStatusName" />
                    <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="SupplierName" HeaderText="Supplier" SortExpression="SupplierName" />
                    <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="POValue" HeaderText="Total" SortExpression="POValue" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N0}" />
                    <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="Month01" HeaderText="Jan" SortExpression="Month01" 
                        ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N0}" >
                    </asp:BoundField>
                    <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="Month02" HeaderText="Feb" SortExpression="Month02" 
                        ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N0}" >
                    </asp:BoundField>
                    <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="Month03" HeaderText="Mar" SortExpression="Month03" 
                        ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N0}" >
                    </asp:BoundField>
                    <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="Month04" HeaderText="Apr" SortExpression="Month04" 
                        ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N0}" >
                    </asp:BoundField>
                    <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="Month05" HeaderText="May" SortExpression="Month05" 
                        ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N0}" >
                    </asp:BoundField>
                    <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="Month06" HeaderText="Jun" SortExpression="Month06" 
                        ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N0}" >
                    </asp:BoundField>
                    <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="Month07" HeaderText="Jul" SortExpression="Month07" 
                        ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N0}" >
                    </asp:BoundField>
                    <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="Month08" HeaderText="Aug" SortExpression="Month08" 
                        ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N0}" >
                    </asp:BoundField>
                    <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="Month09" HeaderText="Sep" SortExpression="Month09" 
                        ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N0}" >
                    </asp:BoundField>
                    <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="Month10" HeaderText="Oct" SortExpression="Month10" 
                        ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N0}" >
                    </asp:BoundField>
                    <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="Month11" HeaderText="Nov" SortExpression="Month11" 
                        ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N0}" >
                    </asp:BoundField>
                    <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="Month12" HeaderText="Dec" SortExpression="Month12" 
                        ItemStyle-HorizontalAlign="Right" DataFormatString="{0:N0}" >
                    </asp:BoundField>
                </Columns>
                <RowStyle CssClass="grid_row" Wrap="false" BackColor="#EFF3FB"/>
                <HeaderStyle BackColor="Gray" CssClass="GVFixedHeader" ForeColor="White" Wrap="True" Height="15px"></HeaderStyle>           
                <FooterStyle CssClass="grid_footer" Wrap="False" />
                <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="false"  BackColor="White" />
            </asp:GridView></asp:Panel>
       </div>
    </div>
    </div>

      <!-- Project Creation message ModalPopupExtender -->
        <asp:Button id="cmdDummy" runat="server" style="display:none;" />
        <ajaxToolkit:ModalPopupExtender ID="ShowFileList" runat="server" PopupControlID="Panel1" 
            TargetControlID="cmdDummy" BackgroundCssClass="modalBackground" CancelControlID="cmdCancel"/>
        
            <asp:Panel ID="Panel1" runat="server" CssClass="smallModalPopup" style = "display:none; width:650px; height: 250px" HorizontalAlign="Left">    
            <div style="font-size:medium;">
                <label runat="server"><b>List of uploaded file</b></label>
            <hr/>
            </div>

            <div style="height:150px;padding:15px">
            <asp:GridView ID="GridViewDocuments" runat="server"
                 AutoGenerateColumns="False" DataKeyNames ="FileID"
            EmptyDataText = "No files uploaded" CellPadding="4" Width="90%"
            EnableModelValidation="True" ForeColor="#333333"  GridLines="Vertical" style="border-left-style:inset;">
                <AlternatingRowStyle BackColor="White" CssClass="grid_alternating_row" Wrap="false" ForeColor="#284775"  />
    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
               <Columns>
         <asp:BoundField DataField="FileID" HeaderText="FileID" SortExpression="FileID" Visible="false" ItemStyle-HorizontalAlign="Left" />
         
         <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="FileName" HeaderText="File Name" ItemStyle-HorizontalAlign="Left"  />
                   
             
       <asp:TemplateField>
            <ItemTemplate>
                <asp:LinkButton ID="linkDownload" Text = "Download" CommandArgument = '<%# Eval("FileName") %>' runat="server" OnCommand = "LinkDownload_click" ForeColor="Black"></asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>
         
  
            </Columns>
               
                <FooterStyle CssClass="grid_footer" Wrap="False" />
                <HeaderStyle BackColor="Gray" ForeColor="White" Height="15px" Wrap="True" />
    
                <RowStyle CssClass="grid_row" Wrap="False" BackColor="#EFF3FB" /> 

            </asp:GridView>
            </div>
            <!--Buttons-->
           
            <div style="text-align:center">
                <asp:Button ID="cmdCancel" runat="server" Text="Cancel" CssClass  ="form_button" Width="80px"/>
            </div>
          
        </asp:Panel>
          
        <!-- ModalPopupExtender -->