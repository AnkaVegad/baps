﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SupplierEdit.ascx.cs" Inherits="User_Controls_SupplierEdit" %>

    <!--Supplier Edit Area-->
    <table style="width:1100px">
        <tr><td style="font-size:2px">&nbsp;</td></tr>
        <tr>
            <td>
                <asp:Label ID="lblHeader" runat="server" CssClass="page_header"></asp:Label><br />
            </td>
        </tr><tr><td style="font-size:2px">&nbsp;</td></tr>
    </table>
    
    <div id="product_edit_area">
        <table style="width:1000px">
            <tr><td style="vertical-align:top">
                <table><tr>
                    <td>Supplier Name</td><td>
                        <asp:Label ID="SupplierID" runat="server" Visible="false" width="40px"/>
                        <asp:TextBox ID="SupplierName" runat="server" Width="280px" MaxLength="35" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                            ControlToValidate="SupplierName" ValidationGroup="Supplier"
                            Display="Dynamic" ErrorMessage="*Required" />
                    </td></tr>
                    <tr><td>SAP Vendor Code</td><td>
                        <asp:TextBox ID="SAPVendorCode" runat="server" Width="100px" MaxLength="10" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ValidationGroup="Supplier"
                            ControlToValidate="SAPVendorCode" Display="Dynamic" ErrorMessage="*Required" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="SAPVendorCode"
                            Display="Dynamic" ErrorMessage="A numeric value must be entered" ValidationExpression="^\d+$" ValidationGroup="Supplier" />
                    </td></tr>
                    <tr><td>Status</td><td>
                        <asp:DropDownList ID="ActiveIndicator" runat="server" Width="80px"
                            DataTextField="EntityName" DataValueField="ID" />
                        <asp:RangeValidator ID="RangeValidator6" runat="server" 
                            ErrorMessage="*Required" MaximumValue="2" MinimumValue="1" 
                            ControlToValidate="ActiveIndicator" Display="Dynamic" ValidationGroup="Supplier" />
                    </td></tr>
                    <tr><td>Comments</td><td>
                        <asp:TextBox ID="Comments" runat="server" Width="280px" MaxLength="255" />
                    </td></tr>
                    <tr><td>Last Updated Date</td><td>
                        <asp:TextBox ID="LastUpdatedDate" runat="server" Width="176px" Visible="true" Enabled="false" />
                    </td></tr>
                    <tr><td>Last Updated By</td><td>
                        <asp:TextBox ID="LastUpdatedBy" runat="server" Width="176px" Visible="true" Enabled="false" />
                    </td></tr>
                    <tr><td>Created Date</td><td>
                        <asp:TextBox ID="CreatedDate" runat="server" Width="176px" Visible="true" Enabled="false" />
                    </td></tr>
                    <tr><td>Created By</td><td>
                        <asp:TextBox ID="CreatedBy" runat="server" Width="176px" Visible="true" Enabled="false" />
                    </td></tr>
                </table>            
            </td></tr>
        </table>
    </div>
    