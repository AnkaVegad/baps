﻿using System;
using System.Data;
using System.Data.SqlClient;

public partial class FilterDropDown : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    public string SelectedValue
    //set or return selected value property
    {
        get { return cboFilterDropDown.SelectedValue; }

        set { cboFilterDropDown.SelectedValue = value; }
    }

    public string SelectedItem
    //return selected item property
    {
        get { return cboFilterDropDown.SelectedItem.Text; }
    }

    public DataTable DataSource
    {
        set
        {
            //string strSQL = value;
            //SqlCommand cmd = DataAccess.CreateCommand();
            //cmd.CommandType = CommandType.Text;
            //cmd.CommandText = strSQL;
            //DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
            cboFilterDropDown.DataSource = value;
            cboFilterDropDown.DataTextField = "EntityName";
            cboFilterDropDown.DataValueField = "ID";
            cboFilterDropDown.DataBind();
        }
    }

    public int Width
    {
        //get { return cboFilterDropDown.Width; }

        set { cboFilterDropDown.Width = value; }
    }

    public string Text
    {
        set { Label1.Text = value; }
    }
}

