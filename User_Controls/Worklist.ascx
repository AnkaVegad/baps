﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Worklist.ascx.cs" Inherits="User_Controls_Worklist" %>
<%@ Register src="WorklistSummary_rs.ascx" tagname="WorklistSummary_rs" tagprefix="uc1" %>

    <script type="text/javascript">
        function setscroll(ctlID) {
            //var panel = document.getElementById(ctlID + 'project_grid');
            var panel = document.getElementById('<%= project_grid.ClientID %>');
            var div_value = document.cookie.substring(document.cookie.indexOf("div_position=") + 13, document.cookie.length);
            panel.scrollTop = div_value.substring(0, div_value.indexOf(","));
            panel.scrollLeft = div_value.substring(div_value.indexOf(",") + 1, div_value.length);
        }

        function savescroll(ctlID) {
            var panel = document.getElementById(ctlID + 'project_grid');
            document.cookie = "div_position=" + panel.scrollTop.toString() + ',' + panel.scrollLeft.toString() + ";";
        }
    </script>

<script type="text/javascript">
    function OnLoad() {
        var CurrentWorklist = document.getElementById('<%= HiddenCurrentWorklist.ClientID %>');

        var Worklist01 = document.getElementById('<%= HiddenGridSelection1.ClientID %>');
        var Worklist2 = document.getElementById('<%= HiddenGridSelection2.ClientID %>');
        var Worklist3 = document.getElementById('<%= HiddenGridSelection3.ClientID %>');
        var Worklist4 = document.getElementById('<%= HiddenGridSelection4.ClientID %>');
        var Worklist5 = document.getElementById('<%= HiddenGridSelection5.ClientID %>');
        var Worklist6 = document.getElementById('<%= HiddenGridSelection6.ClientID %>');
        var Worklist7 = document.getElementById('<%= HiddenGridSelection7.ClientID %>');
        var Worklist8 = document.getElementById('<%= HiddenGridSelection8.ClientID %>');
        var Worklist9 = document.getElementById('<%= HiddenGridSelection9.ClientID %>');
        var Worklist10 = document.getElementById('<%= HiddenGridSelection10.ClientID %>');
        var Worklist11 = document.getElementById('<%= HiddenGridSelection11.ClientID %>');
        var Worklist12 = document.getElementById('<%= HiddenGridSelection12.ClientID %>');
        
        if (document.cookie.indexOf("CurrentWorklist=Value=") > -1 && document.cookie.indexOf(";") > document.cookie.indexOf("CurrentWorklist=Value=")) {
            CurrentWorklist.value = document.cookie.substring(document.cookie.indexOf("CurrentWorklist=Value=") + 22, document.cookie.indexOf(";"));
            ChangePage(parseInt(CurrentWorklist.value), 0);
        }
        else if (document.cookie.indexOf("CurrentWorklist=Value=") > -1) {
            CurrentWorklist.value = document.cookie.substring(document.cookie.indexOf("CurrentWorklist=Value=") + 22, document.cookie.length);
            ChangePage(parseInt(CurrentWorklist.value), 0);
        }
    }
</script>

<script type="text/javascript">
    function ChangePage(id, resetscroll)
    {
        if (resetscroll == 1) {
            var panel = document.getElementById('<%= project_grid.ClientID %>');
            panel.scrollTop = 0;
            panel.scrollLeft = 0;
        }

        var view1 = document.getElementById('<%= vw1.ClientID %>');
        var view2 = document.getElementById('<%= vw2.ClientID %>');
        var view3 = document.getElementById('<%= vw3.ClientID %>');
        var view4 = document.getElementById('<%= vw4.ClientID %>');
        var view5 = document.getElementById('<%= vw5.ClientID %>');
        var view6 = document.getElementById('<%= vw6.ClientID %>');
        var view7 = document.getElementById('<%= vw7.ClientID %>');
        var view8 = document.getElementById('<%= vw8.ClientID %>');
        var view9 = document.getElementById('<%= vw9.ClientID %>');
        var view10 = document.getElementById('<%= vw10.ClientID %>');
        var view11 = document.getElementById('<%= vw11.ClientID %>');
        var view12 = document.getElementById('<%= vw12.ClientID %>');

        var grd1 = document.getElementById('<%= grdWorklist01.ClientID %>');
        var grd2 = document.getElementById('<%= grdWorklist2.ClientID %>');
        var grd3 = document.getElementById('<%= grdWorklist3.ClientID %>');
        var grd4 = document.getElementById('<%= grdWorklist4.ClientID %>');
        var grd5 = document.getElementById('<%= grdWorklist5.ClientID %>');
        var grd6 = document.getElementById('<%= grdWorklist6.ClientID %>');
        var grd7 = document.getElementById('<%= grdWorklist7.ClientID %>');
        var grd8 = document.getElementById('<%= grdWorklist8.ClientID %>');
        var grd9 = document.getElementById('<%= grdWorklist9.ClientID %>');
        var grd10 = document.getElementById('<%= grdWorklist10.ClientID %>');
        var grd11 = document.getElementById('<%= grdWorklist11.ClientID %>');
        var grd12 = document.getElementById('<%= grdWorklist12.ClientID %>');

        var footer1 = document.getElementById('<%= FooterWorklist1.ClientID %>');
        var footer2 = document.getElementById('<%= FooterWorklist2.ClientID %>');
        var footer3 = document.getElementById('<%= FooterWorklist3.ClientID %>');
        var footer4 = document.getElementById('<%= FooterWorklist4.ClientID %>');
        var footer5 = document.getElementById('<%= FooterWorklist5.ClientID %>');
        var footer6 = document.getElementById('<%= FooterWorklist6.ClientID %>');
        var footer7 = document.getElementById('<%= FooterWorklist7.ClientID %>');
        var footer8 = document.getElementById('<%= FooterWorklist8.ClientID %>');
        var footer9 = document.getElementById('<%= FooterWorklist9.ClientID %>');
        var footer10 = document.getElementById('<%= FooterWorklist10.ClientID %>');
        var footer11 = document.getElementById('<%= FooterWorklist11.ClientID %>');
        var footer12 = document.getElementById('<%= FooterWorklist12.ClientID %>');

        var lbtnActionReceipt = document.getElementById('<%= lbtnActionReceipt.ClientID %>');
        var lbtnApproveReallocation = document.getElementById('<%= lbtnApproveReallocation.ClientID %>');
        var lbtnApproveRebate = document.getElementById('<%= lbtnApproveRebate.ClientID %>');
        var lbtnAwaitingPONo = document.getElementById('<%= lbtnAwaitingPONo.ClientID %>');
        var lbtnChangeToPo = document.getElementById('<%= lbtnChangeToPo.ClientID %>');
        var lbtnChangeToRebate = document.getElementById('<%= lbtnChangeToRebate.ClientID %>');
        var lbtnCreatePR = document.getElementById('<%= lbtnCreatePR.ClientID %>');
        var lbtnCreateRebate = document.getElementById('<%= lbtnCreateRebate.ClientID %>');
        var lbtnRequestPR_Rbte = document.getElementById('<%= lbtnRequestPR_Rbte.ClientID %>');
        var lbtnRequestReceipt = document.getElementById('<%= lbtnRequestReceipt.ClientID %>');
        var lbtnReallocationAwaitApproval = document.getElementById('<%= lbtnReallocationAwaitApproval.ClientID %>');
        var lbtnFinanceApproval = document.getElementById('<%= lbtnFinanceApproval.ClientID %>');
        

        var CurrentWorklist = document.getElementById('<%= HiddenCurrentWorklist.ClientID %>');

        view1.style.display = "none";
        view2.style.display = "none";
        view3.style.display = "none";
        view4.style.display = "none";
        view5.style.display = "none";
        view6.style.display = "none";
        view7.style.display = "none";
        view8.style.display = "none";
        view9.style.display = "none";
        view10.style.display = "none";
        view11.style.display = "none";
        view12.style.display = "none";

        footer1.style.display = "none";
        footer2.style.display = "none";
        footer3.style.display = "none";
        footer4.style.display = "none";
        footer5.style.display = "none";
        footer6.style.display = "none";
        footer7.style.display = "none";
        footer8.style.display = "none";
        footer9.style.display = "none";
        footer10.style.display = "none";
        footer11.style.display = "none";
        footer12.style.display = "none";

        if (lbtnActionReceipt != null)
        {lbtnActionReceipt.style.fontWeight = "normal";}
        if (lbtnApproveReallocation!=null)
        {lbtnApproveReallocation.style.fontWeight = "normal";}
        if (lbtnApproveRebate!=null)
        {lbtnApproveRebate.style.fontWeight = "normal";}
        if (lbtnAwaitingPONo!=null)
        {lbtnAwaitingPONo.style.fontWeight = "normal";}
        if (lbtnChangeToPo!=null)
        {lbtnChangeToPo.style.fontWeight = "normal";}
        if (lbtnChangeToRebate!=null)
        {lbtnChangeToRebate.style.fontWeight = "normal";}
        if (lbtnCreatePR!=null)
        {lbtnCreatePR.style.fontWeight = "normal";}
        if (lbtnCreateRebate!=null)
        {lbtnCreateRebate.style.fontWeight = "normal";}
        if (lbtnRequestPR_Rbte!=null)
        {lbtnRequestPR_Rbte.style.fontWeight = "normal";}
        if (lbtnRequestReceipt!=null)
        { lbtnRequestReceipt.style.fontWeight = "normal"; }
        if (lbtnReallocationAwaitApproval != null)
        { lbtnReallocationAwaitApproval.style.fontWeight = "normal"; }
        if (lbtnFinanceApproval != null)
        { lbtnFinanceApproval.style.fontWeight = "normal"; }

        switch (id) {
            case 1:
                view1.style.display = "inline";
                footer1.style.display = "inline";
                lbtnAwaitingPONo.style.fontWeight = "bold";
                break;
            case 2:
                view2.style.display = "inline";
                footer2.style.display = "inline";
                lbtnRequestReceipt.style.fontWeight = "bold";
                
                break;
            case 3:
                view3.style.display = "inline";
                footer3.style.display = "inline";
                lbtnChangeToPo.style.fontWeight = "bold";
                
                break;
            case 4:
                view4.style.display = "inline";
                footer4.style.display = "inline";
                lbtnActionReceipt.style.fontWeight = "bold";
                break;
            case 5:
                view5.style.display = "inline";
                footer5.style.display = "inline";
                lbtnCreatePR.style.fontWeight = "bold";
                
                break;
            case 6:
                view6.style.display = "inline";
                footer6.style.display = "inline";
                lbtnChangeToRebate.style.fontWeight = "bold";
                
                break;
            case 7:
                view7.style.display = "inline";
                footer7.style.display = "inline";
                lbtnApproveRebate.style.fontWeight = "bold";
                
                break;
            case 8:
                view8.style.display = "inline";
                footer8.style.display = "inline";
                lbtnCreateRebate.style.fontWeight = "bold";
                
                break;
            case 9:
                view9.style.display = "inline";
                footer9.style.display = "inline";
                lbtnApproveReallocation.style.fontWeight = "bold";
                
                break;
            case 10:
                view10.style.display = "inline";
                footer10.style.display = "inline";
                lbtnReallocationAwaitApproval.style.fontWeight = "bold";
                
                break;
            case 11:
                view11.style.display = "inline";
                footer11.style.display = "inline";
                lbtnRequestPR_Rbte.style.fontWeight = "bold";

                break;
            case 12:
                view12.style.display = "inline";
                footer12.style.display = "inline";
                lbtnFinanceApproval.style.fontWeight = "bold";

                break;
        }
        CurrentWorklist.value = id;
        document.cookie = "CurrentWorklist=Value=" + id.toString();
    }
</script>

<script type="text/javascript">
    $(function () {
        $("[id*=grdWorklist01] td").bind("click", function () {
            var row = $(this).parent();
            var GridSelection = document.getElementById('<%= HiddenGridSelection1.ClientID %>');
            var index = -1;

            $("[id*=grdWorklist01] tr").each(function () {
                if ($(this)[0] != row[0]) {
                    index = index + 1;
                    $("td", this).removeClass("grid_selected");
                }
                else {
                    GridSelection.value = index;
                }
            });
            $("td", row).each(function () {
                if (!$(this).hasClass("grid_selected")) {
                    $(this).addClass("grid_selected");
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $("[id*=grdWorklist2] td").bind("click", function () {
            var row = $(this).parent();
            var GridSelection = document.getElementById('<%= HiddenGridSelection2.ClientID %>');
            var index = -1;

            $("[id*=grdWorklist2] tr").each(function () {
                if ($(this)[0] != row[0]) {
                    index = index + 1;
                    $("td", this).removeClass("grid_selected");
                }
                else {
                    GridSelection.value = index;
                }
            });
            $("td", row).each(function () {
                if (!$(this).hasClass("grid_selected")) {
                    $(this).addClass("grid_selected");
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(function () {
    
        $("[id*=grdWorklist3] td").bind("click", function () {
            var row = $(this).parent();
            var GridSelection = document.getElementById('<%= HiddenGridSelection3.ClientID %>');
            var index = -1;

            $("[id*=grdWorklist3] tr").each(function () {
                if ($(this)[0] != row[0]) {
                    index = index + 1;
                    $("td", this).removeClass("grid_selected");
                }
                else {
                    GridSelection.value = index;
                }
            });
            $("td", row).each(function () {
                if (!$(this).hasClass("grid_selected")) {
                    $(this).addClass("grid_selected");
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $("[id*=grdWorklist4] td").bind("click", function () {
            var row = $(this).parent();
            var GridSelection = document.getElementById('<%= HiddenGridSelection4.ClientID %>');
            var index = -1;

            $("[id*=grdWorklist4] tr").each(function () {
                if ($(this)[0] != row[0]) {
                    index = index + 1;
                    $("td", this).removeClass("grid_selected");
                }
                else {
                    GridSelection.value = index;
                }
            });
            $("td", row).each(function () {
                if (!$(this).hasClass("grid_selected")) {
                    $(this).addClass("grid_selected");
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $("[id*=grdWorklist5] td").bind("click", function () {
            var row = $(this).parent();
            var GridSelection = document.getElementById('<%= HiddenGridSelection5.ClientID %>');
            var index = -1;

            $("[id*=grdWorklist5] tr").each(function () {
                if ($(this)[0] != row[0]) {
                    index = index + 1;
                    $("td", this).removeClass("grid_selected");
                }
                else {
                    GridSelection.value = index;
                }
            });
            $("td", row).each(function () {
                if (!$(this).hasClass("grid_selected")) {
                    $(this).addClass("grid_selected");
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $("[id*=grdWorklist6] td").bind("click", function () {
            var row = $(this).parent();
            var GridSelection = document.getElementById('<%= HiddenGridSelection6.ClientID %>');
            var index = -1;


            $("[id*=grdWorklist6] tr").each(function () {
                if ($(this)[0] != row[0]) {
                    index = index + 1;
                    $("td", this).removeClass("grid_selected");
                }
                else {
                    GridSelection.value = index;
                }
            });
            $("td", row).each(function () {
                if (!$(this).hasClass("grid_selected")) {
                    $(this).addClass("grid_selected");
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $("[id*=grdWorklist7] td").bind("click", function () {
            var row = $(this).parent();
            var GridSelection = document.getElementById('<%= HiddenGridSelection7.ClientID %>');
            var index = -1;

            $("[id*=grdWorklist7] tr").each(function () {
                if ($(this)[0] != row[0]) {
                    index = index + 1;
                    $("td", this).removeClass("grid_selected");
                }
                else {
                    GridSelection.value = index;
                }
            });
            $("td", row).each(function () {
                if (!$(this).hasClass("grid_selected")) {
                    $(this).addClass("grid_selected");
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $("[id*=grdWorklist8] td").bind("click", function () {
            var row = $(this).parent();
            var GridSelection = document.getElementById('<%= HiddenGridSelection8.ClientID %>');
            var index = -1;

            $("[id*=grdWorklist8] tr").each(function () {
                if ($(this)[0] != row[0]) {
                    index = index + 1;
                    $("td", this).removeClass("grid_selected");
                }
                else {
                    GridSelection.value = index;
                }
            });
            $("td", row).each(function () {
                if (!$(this).hasClass("grid_selected")) {
                    $(this).addClass("grid_selected");
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $("[id*=grdWorklist9] td").bind("click", function () {
            var row = $(this).parent();
            var GridSelection = document.getElementById('<%= HiddenGridSelection9.ClientID %>');
            var index = -1;

            $("[id*=grdWorklist9] tr").each(function () {
                if ($(this)[0] != row[0]) {
                    index = index + 1;
                    $("td", this).removeClass("grid_selected");
                }
                else {
                    GridSelection.value = index;
                }
            });
            $("td", row).each(function () {
                if (!$(this).hasClass("grid_selected")) {
                    $(this).addClass("grid_selected");
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $("[id*=grdWorklist10] td").bind("click", function () {
            var row = $(this).parent();
            var GridSelection = document.getElementById('<%= HiddenGridSelection10.ClientID %>');
            var index = -1;

            $("[id*=grdWorklist10] tr").each(function () {
                if ($(this)[0] != row[0]) {
                    index = index + 1;
                    $("td", this).removeClass("grid_selected");
                }
                else {
                    GridSelection.value = index;
                }
            });
            $("td", row).each(function () {
                if (!$(this).hasClass("grid_selected")) {
                    $(this).addClass("grid_selected");
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $("[id*=grdWorklist11] td").bind("click", function () {
            var row = $(this).parent();
            var GridSelection = document.getElementById('<%= HiddenGridSelection11.ClientID %>');
            var index = -1;

            $("[id*=grdWorklist11] tr").each(function () {
                if ($(this)[0] != row[0]) {
                    index = index + 1;
                    $("td", this).removeClass("grid_selected");
                }
                else {
                    GridSelection.value = index;
                }
            });
            $("td", row).each(function () {
                if (!$(this).hasClass("grid_selected")) {
                    $(this).addClass("grid_selected");
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $("[id*=grdWorklist12] td").bind("click", function () {
            var row = $(this).parent();
            var GridSelection = document.getElementById('<%= HiddenGridSelection12.ClientID %>');
            var index = -1;

            $("[id*=grdWorklist12] tr").each(function () {
                if ($(this)[0] != row[0]) {
                    index = index + 1;
                    $("td", this).removeClass("grid_selected");
                }
                else {
                    GridSelection.value = index;
                }
            });
            $("td", row).each(function () {
                if (!$(this).hasClass("grid_selected")) {
                    $(this).addClass("grid_selected");
                }
            });
        });
    });
</script>




<style type="text/css">
    .hideGridColumn
    {
        display:none;
    }
 </style>
<div id="bodymain" runat="server">
    <div id="resizable1" class="resizable ui-widget-content" style="width:330px">
        <asp:HiddenField ID="HiddenCurrentWorklist" runat="server"/>
        <asp:HiddenField ID="HiddenGridSelection1" runat="server"/>
        <asp:HiddenField ID="HiddenGridSelection2" runat="server"/>
        <asp:HiddenField ID="HiddenGridSelection3" runat="server"/>
        <asp:HiddenField ID="HiddenGridSelection4" runat="server"/>
        <asp:HiddenField ID="HiddenGridSelection5" runat="server"/>
        <asp:HiddenField ID="HiddenGridSelection6" runat="server"/>
        <asp:HiddenField ID="HiddenGridSelection7" runat="server"/>
        <asp:HiddenField ID="HiddenGridSelection8" runat="server"/>
        <asp:HiddenField ID="HiddenGridSelection9" runat="server"/>
        <asp:HiddenField ID="HiddenGridSelection10" runat="server"/>
        <asp:HiddenField ID="HiddenGridSelection11" runat="server"/>
        <asp:HiddenField ID="HiddenGridSelection12" runat="server"/>
        <asp:Label ID="lblHeading" Text="" runat="server" CssClass="page_header"></asp:Label>
        <%--        <!--Filter Criteria-->
        <table>
           <!-- <tr>
                <td colspan="2">
                    <asp:Label ID="lblHeading" Text="" runat="server" CssClass="page_header"></asp:Label>
                    <br />
                </td>
            </tr>-->
           <tr><td><br /></td></tr> 
            <tr>
                <td>Select Worklist</td>
                <td>
            <asp:DropDownList id="cboWorklistType" runat="server" AutoPostBack="True" ForeColor="Black"
                ontextchanged="cboWorklistType_TextChanged" visible="false"/>
        </td></tr></table><table>
            <tr>
                <td colspan="2"><uc1:WorklistSummary_rs ID="WorklistSummary_rs1" runat="server" /></td>
            </tr>
            
       </table> --%>
        <div id="spacer"></div>
    <table border="0">
    <tr><td>
        <asp:Label ID="lblTitle" runat="server" Text="Worklist Name" Font-Bold="true"></asp:Label>
    </td><td>
        <asp:Label ID="lblWorklistCount" runat="server" style="text-align:right" Width="100px" Text="Pending Items" Font-Bold="true"/>&nbsp;&nbsp;
    </td></tr>
        <%if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 8)
{ %>
    <tr><td>
        <asp:LinkButton ID="lbtnCreatePR" runat="server" Text="Create PR" OnClientClick="ChangePage(5,1);return false;"></asp:LinkButton>
    </td><td>
        <asp:Label ID="txtWorklist5" runat="server" CssClass="label_text" Borderstyle="None" style="text-align:right" Width="100px"/>&nbsp;&nbsp;
    </td></tr>
        <tr><td>
        <asp:LinkButton ID="lbtnAwaitingPONo" runat="server" Text="Awaiting PO No" OnClientClick="ChangePage(1,1);return false;"></asp:LinkButton>
    </td><td>
        <asp:Label ID="txtWorklist1" runat="server" CssClass="label_text" Borderstyle="None" style="text-align:right" Width="100px"/>&nbsp;&nbsp;
    </td></tr><tr><td>
        <asp:LinkButton ID="lbtnChangeToPo" runat="server" Text="Change To PO" OnClientClick="ChangePage(3,1);return false;"></asp:LinkButton>
    </td><td>   
        <asp:Label ID="txtWorklist3" runat="server" CssClass="label_text" Borderstyle="None" style="text-align:right" Width="100px"/>&nbsp;&nbsp;
    </td></tr><tr>
            <% } %>
    <td>
        <asp:LinkButton ID="lbtnRequestReceipt" runat="server" Text="Request Receipt" OnClientClick="ChangePage(2,1);return false;"></asp:LinkButton>
    </td><td>
        <asp:Label ID="txtWorklist2" runat="server" CssClass="label_text" Borderstyle="None" style="text-align:right" Width="100px"/>&nbsp;&nbsp;
    </td></tr><tr>
            <%if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 8)
{ %>
    <td>
        <asp:LinkButton ID="lbtnActionReceipt" runat="server" Text="Action Receipt" OnClientClick="ChangePage(4,1);return false;"></asp:LinkButton>
    </td><td>
        <asp:Label ID="txtWorklist4" runat="server" CssClass="label_text" Borderstyle="None" style="text-align:right" Width="100px"/>&nbsp;&nbsp;
    </td></tr><tr>
            <% } %>
    <td>
        <asp:LinkButton ID="lbtnApproveRebate" runat="server" Text="Approve Rebate" OnClientClick="ChangePage(7,1);return false;"></asp:LinkButton>
    </td><td>
        <asp:Label ID="txtWorklist7" runat="server" CssClass="label_text" Borderstyle="None" style="text-align:right" Width="100px"/>&nbsp;&nbsp;
    </td></tr><tr><td>
        <asp:LinkButton ID="lbtnApproveReallocation" runat="server" Text="Approve Reallocation" OnClientClick="ChangePage(9,1);return false;"></asp:LinkButton>
    </td><td>
        <asp:Label ID="txtWorklist9" runat="server" CssClass="label_text" Borderstyle="None" style="text-align:right" Width="100px"/>&nbsp;&nbsp;
    </td></tr>
        <tr><td>
        <asp:LinkButton ID="lbtnReallocationAwaitApproval" runat="server" Text="Reallocation Awaiting Approval" OnClientClick="ChangePage(10,1);return false;"></asp:LinkButton>
    </td>
    <td>
        <asp:Label ID="txtWorklist10" runat="server" CssClass="label_text" Borderstyle="None" style="text-align:right" Width="100px"/>&nbsp;&nbsp;
    </td>
        </tr>
        <tr><td>
            <%if (Convert.ToInt16(Session["CurrentUserLevelID"]) == 35)
{ %>
    
        <asp:LinkButton ID="lbtnCreateRebate" runat="server" Text="Create Rebate" OnClientClick="ChangePage(8,1);return false;"></asp:LinkButton>
    </td><td>
        <asp:Label ID="txtWorklist8" runat="server" CssClass="label_text" Borderstyle="None" style="text-align:right" Width="100px"/>&nbsp;&nbsp;
    </td></tr><tr><td>
        <asp:LinkButton ID="lbtnChangeToRebate" runat="server" Text="Change To Rebate" OnClientClick="ChangePage(6,1);return false;"></asp:LinkButton>
    </td><td>
        <asp:Label ID="txtWorklist6" runat="server" CssClass="label_text" Borderstyle="None" style="text-align:right" Width="100px"/>&nbsp;&nbsp;
    </td>
            <% } %>
    </tr>
        <tr><td>
        <asp:LinkButton ID="lbtnRequestPR_Rbte" runat="server" Text="Request PR/Rbte" OnClientClick="ChangePage(11,1);return false;"></asp:LinkButton>
    </td><td>
        <asp:Label ID="txtWorklist11" runat="server" CssClass="label_text" Borderstyle="None" style="text-align:right" Width="100px"/>&nbsp;&nbsp;
    </td></tr>
        <tr><td>
        <asp:LinkButton ID="lbtnFinanceApproval" runat="server" Text="Finance Approval" OnClientClick="ChangePage(12,1);return false;"></asp:LinkButton>
    </td><td>
        <asp:Label ID="txtWorklist12" runat="server" CssClass="label_text" Borderstyle="None" style="text-align:right" Width="100px"/>&nbsp;&nbsp;
    </td></tr>
    </table>

        <br />
        

    </div>
    <div id="resizable2" class="ui-widget-content">
        <!--Header-->
     
        <br />
        <div id="multiview" runat="server" style="overflow-x:auto; overflow-y:auto; height:100%; width:100%;">
            <div id="project_grid_rs">
            <asp:Panel id="project_grid" style="overflow-x:auto; overflow-y:auto; height:100%; width:100%; position:relative; top: 2px; left: 0px;" runat="server">
                <!--Worklist 5: Create PR-->
                <div ID="vw5" runat="server" style="display:none">
                    <asp:SqlDataSource ID="sds5" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
                        SelectCommandType="StoredProcedure" SelectCommand="sp_ap_SelectWorklist5">
                        <SelectParameters>
                            <asp:SessionParameter Name="UserName" 
                                SessionField="CurrentUserName" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:GridView ID="grdWorklist5" runat="server" width="97%"
                        AutoGenerateColumns="False" GridLines="Vertical"
                        Rowstyle-Wrap="false" EmptyDataText="No Records Found" ShowFooter="false"
                        Headerstyle-Wrap="false" AlternatingRowstyle-Wrap="false" 
                        SelectedRowstyle-Wrap="false" AllowSorting="true" onRowDataBound="grdWorklist5_RowDataBound"             
                        DataSourceID="sds5" DataKeyNames="POID" >
                        <Columns>
                            <asp:BoundField DataField="POID" InsertVisible="False" ReadOnly="True" Visible="false" />
                            <asp:BoundField DataField="RequisitionBy" HeaderText="Requisition By" SortExpression="RequisitionBy" />
                            <asp:BoundField DataField="BudgetResponsibilityAreaName" HeaderText="Area" SortExpression="BudgetResponsibilityAreaName" />
                            <asp:BoundField DataField="CreatedDate" HeaderText="Requisition Date" SortExpression="CreatedDate" DataFormatString="{0:dd/MM/yy}" />
                            <asp:BoundField DataField="ProjectMonthName" HeaderText="Month Required" SortExpression="ProjectMonthYear" />
                            <asp:BoundField DataField="SupplierName" HeaderText="Supplier" SortExpression="SupplierName" />
                            <asp:BoundField DataField="SAPVendorCode" HeaderText="SAP Vendor" SortExpression="SAPVendorCode" />
                            <asp:BoundField DataField="POValue" HeaderText="Value" SortExpression="POValue" Itemstyle-HorizontalAlign="Right" DataFormatString="{0:0.00}" />
                        </Columns>
                        <Pagerstyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <Rowstyle CssClass="grid_row" Wrap="False" BackColor="#EFF3FB" />
                        <SelectedRowstyle CssClass="grid_selected" Wrap="False" BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <Headerstyle CssClass="grid_header" Wrap="False" />            
                        <EditRowstyle BackColor="#2461BF" />
                        <Footerstyle CssClass="grid_footer" Wrap="False" />
                        <AlternatingRowstyle CssClass="grid_alternating_row" Wrap="False" BackColor="White" />
                    </asp:GridView>
                </div>    
                <!--Worklist 1: Awaiting PO Number-->
                <div ID="vw1" runat="server" style="display:none">
                    <asp:SqlDataSource ID="sds1" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
                        SelectCommandType="StoredProcedure" SelectCommand="SelectWorklist1">
                        <SelectParameters>
                            <asp:SessionParameter Name="UserName" 
                                SessionField="CurrentUserName" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:GridView ID="grdWorklist01" runat="server" width="97%"
                        AutoGenerateColumns="False" GridLines="Vertical"
                        Rowstyle-Wrap="false" EmptyDataText="No Records Found" 
                        Headerstyle-Wrap="false" AlternatingRowstyle-Wrap="false"
                        SelectedRowstyle-Wrap="false" 
                        AllowSorting="True" onRowDataBound="grdWorklist1_RowDataBound" 
                        DataSourceID="sds1" DataKeyNames="POID" >
                        <Columns>
                            <asp:BoundField DataField="POID" ReadOnly="True" Visible="false" />
                            <asp:BoundField DataField="PRNumber" HeaderText="PR Number" SortExpression="PRNumber" />
                            <asp:BoundField DataField="RequisitionBy" HeaderText="Requisition By" SortExpression="RequisitionBy" />
                            <asp:BoundField DataField="CreatedDate" HeaderText="Requisition Date" SortExpression="CreatedDate" />
                            <asp:BoundField DataField="SupplierName" HeaderText="Supplier" SortExpression="SupplierName" />
                            <asp:BoundField DataField="SAPVendorCode" HeaderText="SAP Vendor" SortExpression="SAPVendorCode" />
                            <asp:BoundField DataField="POValue" HeaderText="Value" 
                                SortExpression="POValue" Itemstyle-HorizontalAlign="Right" >
                            </asp:BoundField>
                        </Columns>
                        <Pagerstyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <Rowstyle CssClass="grid_row" Wrap="False" BackColor="#EFF3FB" />
                        <SelectedRowstyle CssClass="grid_selected" Wrap="False" BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <Headerstyle CssClass="grid_header" Wrap="False" />            
                        <EditRowstyle BackColor="#2461BF" />
                        <Footerstyle CssClass="grid_footer" Wrap="False" />
                        <AlternatingRowstyle CssClass="grid_alternating_row" Wrap="False" BackColor="White" />
                    </asp:GridView>
                </div>    
                <!--Worklist 3: Change To PO-->
                <div ID="vw3" runat="server" style="display:none">
                    <asp:SqlDataSource ID="sds3" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
                        SelectCommandType="StoredProcedure" SelectCommand="SelectWorklist3">
                        <SelectParameters>
                            <asp:SessionParameter Name="UserName" 
                                SessionField="CurrentUserName" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:GridView ID="grdWorklist3" runat="server" width="97%"
                        AutoGenerateColumns="False" GridLines="Vertical"
                        Rowstyle-Wrap="false" EmptyDataText="No Records Found" 
                        Headerstyle-Wrap="false" AlternatingRowstyle-Wrap="false"
                        SelectedRowstyle-Wrap="false"
                        AllowSorting="True" onRowDataBound="grdWorklist3_RowDataBound"
                        DataSourceID="sds3" DataKeyNames="POChangeID" >
                        <Columns>
                            <asp:BoundField DataField="POChangeID" ReadOnly="True" Visible="true" />
                            <asp:BoundField DataField="ChangedBy" HeaderText="Changed By" SortExpression="ChangedBy" />
                            <asp:BoundField DataField="ChangedDate" HeaderText="Date Changed" SortExpression="ChangedDate" DataFormatString="{0:dd/MM/yy}" />
                            <asp:BoundField DataField="PONumber" HeaderText="PO Number" SortExpression="PONumber" />
                            <asp:BoundField DataField="SupplierName" HeaderText="Supplier" SortExpression="SupplierName" />
                            <asp:BoundField DataField="SAPVendorCode" HeaderText="SAP Vendor" SortExpression="SAPVendorCode" />
                            <asp:BoundField DataField="POOldValue" HeaderText="Old Value" SortExpression="POOldValue" />
                            <asp:BoundField DataField="PONewValue" HeaderText="New Value" SortExpression="PONewValue" />
                        </Columns>
                        <Pagerstyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <Rowstyle CssClass="grid_row" Wrap="False" BackColor="#EFF3FB" />
                        <SelectedRowstyle CssClass="grid_selected" Wrap="False" BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <Headerstyle CssClass="grid_header" Wrap="False" />            
                        <EditRowstyle BackColor="#2461BF" />
                        <Footerstyle CssClass="grid_footer" Wrap="False" />
                        <AlternatingRowstyle CssClass="grid_alternating_row" Wrap="False" BackColor="White" />
                    </asp:GridView>
                </div>    
                <!--Worklist 2: Request Receipt; IO Number added again 15/07/12-->
                <div ID="vw2" runat="server" style="display:none"><!--Default View-->
                    <asp:SqlDataSource ID="sds2" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
                        SelectCommandType="StoredProcedure" SelectCommand="SelectWorklist2">
                        <SelectParameters>
                            <asp:SessionParameter Name="UserName" 
                                SessionField="CurrentUserName" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:GridView ID="grdWorklist2" runat="server" width="97%"
                        AutoGenerateColumns="False" GridLines="Vertical"
                        Rowstyle-Wrap="false" EmptyDataText="No Records Found" 
                        Headerstyle-Wrap="false" AlternatingRowstyle-Wrap="false"
                        SelectedRowstyle-Wrap="false"
                        AllowSorting="True" onRowDataBound="grdWorklist2_RowDataBound"
                        DataSourceID="sds2" DataKeyNames="ActivityDetailID" >
                        <Columns>
                            <asp:BoundField DataField="ActivityDetailID" ReadOnly="True" Visible="false" />
                            <asp:BoundField DataField="ProjectName" HeaderText="Project" SortExpression="ProjectName" />
                            <asp:BoundField DataField="CustomerName" HeaderText="Customer" SortExpression="CustomerName" />
                            <asp:BoundField DataField="MonthShortName" HeaderText="Month" SortExpression="MonthShortName" />
                            <asp:BoundField DataField="ActivityName" HeaderText="Activity" SortExpression="ActivityName" />
                            <asp:BoundField DataField="IONumber" HeaderText="IO Number" SortExpression="IONumber" />
                            <asp:BoundField DataField="PONumber" HeaderText="PO Number" SortExpression="PONumber" />
                            <asp:BoundField DataField="SupplierName" HeaderText="Supplier" SortExpression="SupplierName" />
                            <asp:BoundField DataField="ActivityValue" HeaderText="Value" 
                                SortExpression="ActivityValue" Itemstyle-HorizontalAlign="Right" />
                        </Columns>
                        <Pagerstyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <Rowstyle CssClass="grid_row" Wrap="False" BackColor="#EFF3FB" />
                        <SelectedRowstyle CssClass="grid_selected" Wrap="False" BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <Headerstyle CssClass="grid_header" Wrap="False" />            
                        <EditRowstyle BackColor="#2461BF" />
                        <Footerstyle CssClass="grid_footer" Wrap="False" />
                        <AlternatingRowstyle CssClass="grid_alternating_row" Wrap="False" BackColor="White" />
                    </asp:GridView>
                </div>    
                <!--Worklist 4: Action Receipt-->
                <div ID="vw4" runat="server" style="display:none">
                    <asp:SqlDataSource ID="sds4" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
                        SelectCommandType="StoredProcedure" SelectCommand="SelectWorklist4">
                        <SelectParameters>
                            <asp:SessionParameter Name="UserName" 
                                SessionField="CurrentUserName" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:GridView ID="grdWorklist4" runat="server" width="97%"
                        AutoGenerateColumns="False" GridLines="Vertical"
                        Rowstyle-Wrap="false" EmptyDataText="No Records Found" 
                        Headerstyle-Wrap="false" AlternatingRowstyle-Wrap="false"
                        SelectedRowstyle-Wrap="false"
                        AllowSorting="True" onRowDataBound="grdWorklist4_RowDataBound"
                        DataSourceID="sds4" DataKeyNames="ActivityDetailID" >
                        <Columns>
                            <asp:BoundField DataField="ActivityDetailID" ReadOnly="True" Visible="false" />
                            <asp:BoundField DataField="ProjectName" HeaderText="Project" SortExpression="ProjectName" />
                            <asp:BoundField DataField="CustomerName" HeaderText="Customer" SortExpression="CustomerName" />
                            <asp:BoundField DataField="ActivityName" HeaderText="Activity" SortExpression="ActivityName" />
                            <asp:BoundField DataField="IONumber" HeaderText="IO Number" SortExpression="IONumber" />
                            <asp:BoundField DataField="PONumber" HeaderText="PO Number" SortExpression="PONumber" />
                            <asp:BoundField DataField="SupplierName" HeaderText="Supplier" SortExpression="SupplierName" />
                            <asp:BoundField DataField="ReceiptRequestedBy" HeaderText="Requested By" SortExpression="ReceiptRequestedBy" />
                            <asp:BoundField DataField="MonthShortName" HeaderText="Month" SortExpression="MonthShortName" />
                            <asp:BoundField DataField="ActivityValue" HeaderText="Rcpt Value" SortExpression="ActivityValue" Itemstyle-HorizontalAlign="Right" />
                        </Columns>
                        <Pagerstyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <Rowstyle CssClass="grid_row" Wrap="False" BackColor="#EFF3FB" />
                        <SelectedRowstyle CssClass="grid_selected" Wrap="False" BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <Headerstyle CssClass="grid_header" Wrap="False" />            
                        <EditRowstyle BackColor="#2461BF" />
                        <Footerstyle CssClass="grid_footer" Wrap="False" />
                        <AlternatingRowstyle CssClass="grid_alternating_row" Wrap="False" BackColor="White" />
                    </asp:GridView>
                </div>    
                <!--Worklist 7: Request Rebate Authorisation-->
                <div ID="vw7" runat="server" style="display:none">
                    <asp:SqlDataSource ID="sds7" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
                        SelectCommandType="StoredProcedure" SelectCommand="sp_ap_SelectWorklist7">
                        <SelectParameters>
                            <asp:SessionParameter Name="UserName" 
                                SessionField="CurrentUserName" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:GridView ID="grdWorklist7" runat="server" width="97%"
                        AutoGenerateColumns="False" GridLines="Vertical"
                        Rowstyle-Wrap="false" EmptyDataText="No Records Found" 
                        Headerstyle-Wrap="false" AlternatingRowstyle-Wrap="false"
                        SelectedRowstyle-Wrap="false" AllowSorting="true" onRowDataBound="grdWorklist7_RowDataBound"             
                        DataSourceID="sds7" DataKeyNames="RebateID" >
                        <Columns>
                            <asp:BoundField DataField="RebateID" InsertVisible="False" ReadOnly="True" Visible="false" />
                            <asp:BoundField DataField="RequestedBy" HeaderText="Requested By" SortExpression="RequestedBy" />
                            <asp:BoundField DataField="CreatedDate" HeaderText="Requested Date" SortExpression="CreatedDate" DataFormatString="{0:dd/MM/yy}" />
                            <asp:BoundField DataField="InStoreStartDate" HeaderText="Valid From" SortExpression="InStoreStartDate" DataFormatString="{0:dd/MM/yy}" />
                            <asp:BoundField DataField="CustomerName" HeaderText="Customer Name" SortExpression="CustomerName" />
                            <asp:BoundField DataField="SAPCustomerCode" HeaderText="SAP Customer" SortExpression="SAPCustomerCode" />
                            <asp:BoundField DataField="RebateValue" HeaderText="Rebate Value" SortExpression="RebateValue" Itemstyle-HorizontalAlign="Right" />
                        </Columns>
                        <Pagerstyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <Rowstyle CssClass="grid_row" Wrap="False" BackColor="#EFF3FB" />
                        <SelectedRowstyle CssClass="grid_selected" Wrap="False" BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <Headerstyle CssClass="grid_header" Wrap="False" />            
                        <EditRowstyle BackColor="#2461BF" />
                        <Footerstyle CssClass="grid_footer" Wrap="False" />
                        <AlternatingRowstyle CssClass="grid_alternating_row" Wrap="False" BackColor="White" />
                    </asp:GridView>
                </div>    
                <!--Worklist 8: Create Rebate-->
                <div ID="vw8" runat="server" style="display:none">
                    <asp:SqlDataSource ID="sds8" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
                        SelectCommandType="StoredProcedure" SelectCommand="sp_ap_SelectWorklist8">
                        <SelectParameters>
                            <asp:SessionParameter Name="UserName" 
                                SessionField="CurrentUserName" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:GridView ID="grdWorklist8" runat="server" width="97%"
                        AutoGenerateColumns="False" GridLines="Vertical"
                        Rowstyle-Wrap="false" EmptyDataText="No Records Found" 
                        Headerstyle-Wrap="false" AlternatingRowstyle-Wrap="false"
                        SelectedRowstyle-Wrap="false" AllowSorting="true" onRowDataBound="grdWorklist8_RowDataBound"             
                        DataSourceID="sds8" DataKeyNames="RebateID" >
                        <Columns>
                            <asp:BoundField DataField="RebateID" InsertVisible="False" ReadOnly="True" Visible="false" />
                            <asp:BoundField DataField="RequestedBy" HeaderText="Requested By" SortExpression="RequestedBy" />
                            <asp:BoundField DataField="CreatedDate" HeaderText="Requested Date" SortExpression="CreatedDate" DataFormatString="{0:dd/MM/yy}" />
                            <asp:BoundField DataField="InStoreStartDate" HeaderText="Valid From" SortExpression="InStoreStartDate" DataFormatString="{0:dd/MM/yy}" />
                            <asp:BoundField DataField="CustomerName" HeaderText="Customer Name" SortExpression="CustomerName" />
                            <asp:BoundField DataField="SAPCustomerCode" HeaderText="SAP Customer" SortExpression="SAPCustomerCode" />
                            <asp:BoundField DataField="RebateValue" HeaderText="Rebate Value" SortExpression="RebateValue" Itemstyle-HorizontalAlign="Right" />
                        </Columns>
                        <Pagerstyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <Rowstyle CssClass="grid_row" Wrap="False" BackColor="#EFF3FB" />
                        <SelectedRowstyle CssClass="grid_selected" Wrap="False" BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <Headerstyle CssClass="grid_header" Wrap="False" />            
                        <EditRowstyle BackColor="#2461BF" />
                        <Footerstyle CssClass="grid_footer" Wrap="False" />
                        <AlternatingRowstyle CssClass="grid_alternating_row" Wrap="False" BackColor="White" />
                    </asp:GridView>
                </div>    
                <!--Worklist 6: Change To Rebate-->
                <div ID="vw6" runat="server" style="display:none">
                    <asp:SqlDataSource ID="sds6" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
                        SelectCommandType="StoredProcedure" SelectCommand="sp_ap_SelectWorklist6">
                        <SelectParameters>
                            <asp:SessionParameter Name="UserName" 
                                SessionField="CurrentUserName" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:GridView ID="grdWorklist6" runat="server" width="97%"
                        AutoGenerateColumns="False" GridLines="Vertical"
                        Rowstyle-Wrap="false" EmptyDataText="No Records Found" 
                        Headerstyle-Wrap="false" AlternatingRowstyle-Wrap="false"
                        SelectedRowstyle-Wrap="false" AllowSorting="true" onRowDataBound="grdWorklist6_RowDataBound"             
                        DataSourceID="sds6" DataKeyNames="RebateChangeID" >
                        <Columns>
                            <asp:BoundField DataField="RebateChangeID" ReadOnly="True" Visible="false" />
                            <asp:BoundField DataField="ChangedBy" HeaderText="Changed By" SortExpression="ChangedBy" />
                            <asp:BoundField DataField="ChangedDate" HeaderText="Date Changed" SortExpression="ChangedDate" DataFormatString="{0:dd/MM/yy}" />
                            <asp:BoundField DataField="AgreementNumber" HeaderText="Agreement" SortExpression="AgreementNumber" />
                            <asp:BoundField DataField="CustomerName" HeaderText="Customer" SortExpression="CustomerName" />
                            <asp:BoundField DataField="SAPCustomerCode" HeaderText="SAP Customer" SortExpression="SAPCustomerCode" />
                            <asp:BoundField DataField="RebateOldValue" HeaderText="Old Value" SortExpression="RebateOldValue" />
                            <asp:BoundField DataField="RebateNewValue" HeaderText="New Value" SortExpression="RebateNewValue" />
                        </Columns>
                        <Pagerstyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <Rowstyle CssClass="grid_row" Wrap="False" BackColor="#EFF3FB" />
                        <SelectedRowstyle CssClass="grid_selected" Wrap="False" BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <Headerstyle CssClass="grid_header" Wrap="False" />            
                        <EditRowstyle BackColor="#2461BF" />
                        <Footerstyle CssClass="grid_footer" Wrap="False" />
                        <AlternatingRowstyle CssClass="grid_alternating_row" Wrap="False" BackColor="White" />
                    </asp:GridView>
                </div>    
                <!--Worklist9: Approve Budget Reallocation-->
                <div ID="vw9" runat="server" style="display:none">
                    <asp:SqlDataSource ID="sds9" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
                        SelectCommandType="StoredProcedure" SelectCommand="SelectWorklist9">
                        <SelectParameters>
                            <asp:SessionParameter Name="UserName" 
                                SessionField="CurrentUserName" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:GridView ID="grdWorklist9" runat="server" width="97%" 
                        AutoGenerateColumns="False" GridLines="Vertical"
                        Rowstyle-Wrap="false" EmptyDataText="No Records Found" 
                        AlternatingRowstyle-Wrap="false"
                        SelectedRowstyle-Wrap="false" AllowSorting="true" onRowDataBound="grdWorklist9_RowDataBound"             
                        DataSourceID="sds9" DataKeyNames="ReallocationID" >
                        <Headerstyle BackColor="Gray" ForeColor="White" Wrap="True"></Headerstyle>
                        <Columns>
                            <asp:BoundField DataField="ReallocationID" ReadOnly="True" Visible="false" />
                            <asp:BoundField DataField="RequestedBy" HeaderText="Requested By" SortExpression="RequestedBy" />
                            <asp:BoundField DataField="RequestedDate" HeaderText="Requested Date" SortExpression="RequestedDate" DataFormatString="{0:dd/MM/yy}" />
                            <asp:BoundField DataField="FromProject" HeaderText="From Project" SortExpression="FromProject" />
                            <asp:BoundField DataField="FromCustomer" HeaderText="From Customer" SortExpression="FromCustomer" />
                            <asp:BoundField DataField="ToProject" HeaderText="To Project" SortExpression="ToProject" />
                            <asp:BoundField DataField="ToCustomer" HeaderText="To Customer" SortExpression="ToCustomer" />
                            <asp:BoundField DataField="Value" HeaderText="Value" SortExpression="Value" />
                        </Columns>
                        <Pagerstyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <Rowstyle CssClass="grid_row" Wrap="False" BackColor="#EFF3FB" />
                        <SelectedRowstyle CssClass="grid_selected" Wrap="False" BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <Headerstyle CssClass="grid_header" Wrap="False" />            
                        <EditRowstyle BackColor="#2461BF" />
                        <Footerstyle CssClass="grid_footer" Wrap="False" />
                        <AlternatingRowstyle CssClass="grid_alternating_row" Wrap="False" BackColor="White" />
                    </asp:GridView>
                </div>
                <!--Worklist10: Budget Reallocation Awaiting Approval-->
                <div ID="vw10" runat="server" style="display:none">
                    <asp:SqlDataSource ID="sds10" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
                        SelectCommandType="StoredProcedure" SelectCommand="SelectWorklist10">
                        <SelectParameters>
                            <asp:SessionParameter Name="UserName" 
                                SessionField="CurrentUserName" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:GridView ID="grdWorklist10" runat="server" width="97%" 
                        AutoGenerateColumns="False" GridLines="Vertical"
                        Rowstyle-Wrap="false" EmptyDataText="No Records Found" 
                        Headerstyle-Wrap="false" AlternatingRowstyle-Wrap="false"
                        SelectedRowstyle-Wrap="false" AllowSorting="true" onRowDataBound="grdWorklist10_RowDataBound"             
                        DataSourceID="sds10" DataKeyNames="ReallocationID" >
                        <Headerstyle BackColor="Gray" ForeColor="White" Wrap="True"></Headerstyle>
                        <Columns>
                            <asp:BoundField DataField="ReallocationID" ReadOnly="True" Visible="false" />
                            <asp:BoundField DataField="RequestedBy" HeaderText="Requested By" SortExpression="RequestedBy" />
                            <asp:BoundField DataField="RequestedDate" HeaderText="Requested Date" SortExpression="RequestedDate" DataFormatString="{0:dd/MM/yy}" />
                            <asp:BoundField DataField="FromProject" HeaderText="From Project" SortExpression="FromProject" />
                            <asp:BoundField DataField="FromCustomer" HeaderText="From Customer" SortExpression="FromCustomer" />
                            <asp:BoundField DataField="ToProject" HeaderText="To Project" SortExpression="ToProject" />
                            <asp:BoundField DataField="ToCustomer" HeaderText="To Customer" SortExpression="ToCustomer" />
                            <asp:BoundField DataField="Value" HeaderText="Value" SortExpression="Value" />
                            <asp:BoundField DataField="ApproverUserName" HeaderText="Approver" SortExpression="ApproverUserName" />
                            <asp:BoundField DataField="ApprovalStatus" HeaderText="Approval Status" SortExpression="ApprovalStatus" />
                        </Columns>
                        <Rowstyle CssClass="grid_row" Wrap="False" BackColor="#EFF3FB" />
                        <SelectedRowstyle CssClass="grid_selected" Wrap="False" BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <Headerstyle CssClass="grid_header" Wrap="False" />            
                        <AlternatingRowstyle CssClass="grid_alternating_row" Wrap="False" BackColor="White" />
                    </asp:GridView>
                </div>
                <!--Worklist11: Worklist for Purchase Requisition/Rebate Requests-->
                <div ID="vw11" runat="server" style="display:none">
                    <asp:SqlDataSource ID="sds11" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
                        SelectCommandType="StoredProcedure" SelectCommand="SelectWorklist11">
                        <SelectParameters>
                            <asp:SessionParameter Name="UserName" 
                                SessionField="CurrentUserName" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:GridView ID="grdWorklist11" runat="server" width="95%" 
                        AutoGenerateColumns="False" GridLines="Vertical"
                        Rowstyle-Wrap="false" EmptyDataText="No Records Found" 
                        Headerstyle-Wrap="false" AlternatingRowstyle-Wrap="false"
                        SelectedRowstyle-Wrap="false" AllowSorting="true" onRowDataBound="grdWorklist11_RowDataBound"             
                        DataSourceID="sds11" DataKeyNames="ActivityID" >
                        <Headerstyle BackColor="Gray" ForeColor="White" Wrap="True"></Headerstyle>
                        <Columns>
                            <asp:BoundField DataField="ActivityID" ReadOnly="True" Visible="false" />
                            <asp:BoundField DataField="SpendTypeName" HeaderText="SpendTypeName" ReadOnly="true" Visible="false" />
                            <asp:BoundField DataField="ShortText" HeaderText="Activity" SortExpression="Purch_Req/Rebate_Req" />
                            <asp:BoundField DataField="ProjectName" HeaderText="Project" SortExpression="Project"/>
                            <asp:BoundField DataField="CustomerName" HeaderText="Customer" SortExpression="Customer"/>
                            <asp:BoundField DataField="BudgetResponsibilityAreaName" HeaderText="Area" SortExpression="BudgetResponsibilityArea"/>
                            <asp:BoundField DataField="Creator" HeaderText="Created By" SortExpression="Creator"/>
                            <asp:BoundField DataField="CreatedDate" HeaderText="Created Date" DataFormatString="{0:dd/MM/yy}" SortExpression="CreatedDate"/>
                            <asp:BoundField DataField="MonthRequired" HeaderText="Required Month" SortExpression="RequiredMonth"/>
                            <asp:BoundField DataField="SupplierName" HeaderText="Supplier" SortExpression="Supplier"/>
                            <asp:BoundField DataField="Value" HeaderText="Value" SortExpression="Value"/>
                        </Columns>
                        <Rowstyle CssClass="grid_row" Wrap="False" BackColor="#EFF3FB" />
                        <SelectedRowstyle CssClass="grid_selected" Wrap="False" BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <Headerstyle CssClass="grid_header" Wrap="False" />            
                        <AlternatingRowstyle CssClass="grid_alternating_row" Wrap="False" BackColor="White" />
                    </asp:GridView>
                </div>

                <!--Worklist12: Finance Approval-->
               <div ID="vw12" runat="server" style="display:none">
                    <asp:SqlDataSource ID="sds12" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
                        SelectCommandType="StoredProcedure" SelectCommand="sp_ap_SelectWorklist12">
                        <SelectParameters>
                            <asp:SessionParameter Name="UserName" 
                                SessionField="CurrentUserName" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:GridView ID="grdWorklist12" runat="server" width="97%"
                        AutoGenerateColumns="False" GridLines="Vertical"
                        Rowstyle-Wrap="false" EmptyDataText="No Records Found" ShowFooter="false"
                        Headerstyle-Wrap="false" AlternatingRowstyle-Wrap="false" 
                        SelectedRowstyle-Wrap="false" AllowSorting="true" onRowDataBound="grdWorklist12_RowDataBound"             
                        DataSourceID="sds12" DataKeyNames="POID" >
                        <Columns>
                            <asp:BoundField DataField="POID" InsertVisible="False" ReadOnly="True" Visible="false" />
                            <asp:BoundField DataField="RequisitionBy" HeaderText="Requisition By" SortExpression="RequisitionBy" />
                            <asp:BoundField DataField="BudgetResponsibilityAreaName" HeaderText="Area" SortExpression="BudgetResponsibilityAreaName" />
                            <asp:BoundField DataField="CreatedDate" HeaderText="Requisition Date" SortExpression="CreatedDate" DataFormatString="{0:dd/MM/yy}" />
                            <asp:BoundField DataField="ProjectMonthName" HeaderText="Month Required" SortExpression="ProjectMonthYear" />
                            <asp:BoundField DataField="SupplierName" HeaderText="Supplier" SortExpression="SupplierName" />
                            <asp:BoundField DataField="SAPVendorCode" HeaderText="SAP Vendor" SortExpression="SAPVendorCode" />
                            <asp:BoundField DataField="POValue" HeaderText="Value" SortExpression="POValue" Itemstyle-HorizontalAlign="Right" DataFormatString="{0:0.00}" />
                            <asp:BoundField DataField="FinanceApprovalFlag" HeaderText="Status" SortExpression="FinanceApprovalFlag" />

                        </Columns>
                        <Pagerstyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <Rowstyle CssClass="grid_row" Wrap="False" BackColor="#EFF3FB" />
                        <SelectedRowstyle CssClass="grid_selected" Wrap="False" BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <Headerstyle CssClass="grid_header" Wrap="False" />            
                        <EditRowstyle BackColor="#2461BF" />
                        <Footerstyle CssClass="grid_footer" Wrap="False" />
                        <AlternatingRowstyle CssClass="grid_alternating_row" Wrap="False" BackColor="White" />
                    </asp:GridView>
                </div>  
            </asp:panel>
        </div>
        </div>
    </div>
    <div id="footer">
        <br />
        <div id="FooterWorklist1" runat="server">
            &nbsp;
            <asp:Button ID="cmdSelect1" runat="server" Text="Select" onclick="cmdSelect_Click" CssClass="form_button" />
            <asp:Button ID="cmdBack1" runat="server" Text="Back To Projects" causesvalidation="false" onclick="cmdBack_Click" CssClass="form_button" Width="120px" />
        </div>
        <div id="FooterWorklist2" runat="server">
            &nbsp;
            <asp:Button ID="cmdActioned2" runat="server" Text="Actioned" onclick="cmdActioned_Click" CssClass="form_button" />   
            <asp:Button ID="cmdBack2" runat="server" Text="Back To Projects" causesvalidation="false" onclick="cmdBack_Click" CssClass="form_button" Width="120px" />
        </div>
        <div id="FooterWorklist3" runat="server">
            &nbsp;
            <asp:Button ID="cmdSelect3" runat="server" Text="Select" onclick="cmdSelect_Click" CssClass="form_button" />
            <asp:Button ID="cmdActioned3" runat="server" Text="Actioned" onclick="cmdActioned_Click" CssClass="form_button" />   
            <asp:Button ID="cmdBack3" runat="server" Text="Back To Projects" causesvalidation="false" onclick="cmdBack_Click" CssClass="form_button" Width="120px" />
        </div>
        <div id="FooterWorklist4" runat="server">
            &nbsp;
            <asp:Button ID="cmdSelect4" runat="server" Text="Select" onclick="cmdSelect_Click" CssClass="form_button" />
            <asp:Button ID="cmdActioned4" runat="server" Text="Actioned" onclick="cmdActioned_Click" CssClass="form_button" />  
            <asp:Button ID="cmdCancel4" runat="server" Text="Cancel" causesvalidation="false" onclick="cmdCancel_Click" CssClass="form_button" 
                OnClientClick="return confirm('Are you sure you want to delete the request and reset status to unreceipted?');" /> 
            <asp:Button ID="cmdBack4" runat="server" Text="Back To Projects" causesvalidation="false" onclick="cmdBack_Click" CssClass="form_button" Width="120px" />
        </div>
        <div id="FooterWorklist5" runat="server">
            &nbsp;
            <asp:Button ID="cmdSelect5" runat="server" Text="Select" onclick="cmdSelect_Click" CssClass="form_button" />
            <asp:Button ID="cmdBack5" runat="server" Text="Back To Projects" causesvalidation="false" onclick="cmdBack_Click" CssClass="form_button" Width="120px" />
        </div>
        <div id="FooterWorklist6" runat="server">
            &nbsp;
            <asp:Button ID="cmdSelect6" runat="server" Text="Select" onclick="cmdSelect_Click" CssClass="form_button" />
            <asp:Button ID="cmdActioned6" runat="server" Text="Actioned" onclick="cmdActioned_Click" CssClass="form_button" />   
            <asp:Button ID="cmdBack6" runat="server" Text="Back To Projects" causesvalidation="false" onclick="cmdBack_Click" CssClass="form_button" Width="120px" />
        </div>
        <div id="FooterWorklist7" runat="server">
            &nbsp;
            <asp:Button ID="cmdSelect7" runat="server" Text="Select" onclick="cmdSelect_Click" CssClass="form_button" />
            <asp:Button ID="cmdBack7" runat="server" Text="Back To Projects" causesvalidation="false" onclick="cmdBack_Click" CssClass="form_button" Width="120px" />
        </div>
        <div id="FooterWorklist8" runat="server">
            &nbsp;
            <asp:Button ID="cmdSelect8" runat="server" Text="Select" onclick="cmdSelect_Click" CssClass="form_button" />
            <asp:Button ID="cmdBack8" runat="server" Text="Back To Projects" causesvalidation="false" onclick="cmdBack_Click" CssClass="form_button" Width="120px" />
        </div>
        <div id="FooterWorklist9" runat="server">
            &nbsp;
            <asp:Button ID="cmdApprove9" runat="server" Text="Approve Reallocation" onclick="cmdApprove_Click" CssClass="form_button" Width="120px" />
            <asp:Button ID="cmdReject9" runat="server" Text="Reject Reallocation" onclick="cmdReject_Click" CssClass="form_button" Width="120px" />
            <asp:Button ID="cmdBack9" runat="server" Text="Back To Projects" causesvalidation="false" onclick="cmdBack_Click" CssClass="form_button" Width="120px" />
        </div>
        <div id="FooterWorklist10" runat="server">
            &nbsp;
            <asp:Button ID="cmdDelete10" runat="server" Text="Delete Reallocation" onclick="cmdDelete_Click" CssClass="form_button" Width="120px" 
                OnClientClick="return confirm('Are you sure you want to delete the request?');"/>
            <asp:Button ID="cmdBack10" runat="server" Text="Back To Projects" causesvalidation="false" onclick="cmdBack_Click" CssClass="form_button" Width="120px" />
        </div>
        <div id="FooterWorklist11" runat="server">
            &nbsp;
            <asp:Button ID="cmdCreatePR_Rbte11" runat="server" Text="Request PR/Rbte" causesvalidation="false" onclick="cmdCreatePR_Rbte_Click" CssClass="form_button" Width="120px" />
            <asp:Button ID="cmdBack11" runat="server" Text="Back To Projects" causesvalidation="false" onclick="cmdBack_Click" CssClass="form_button" Width="120px" />
        </div>
        <div id="FooterWorklist12" runat="server">
            &nbsp;
            <asp:Button ID="cmdSelect12" runat="server" Text="Select" onclick="cmdSelect_Click" CssClass="form_button" />
            <asp:Button ID="cmdBack12" runat="server" Text="Back To Projects" causesvalidation="false" onclick="cmdBack_Click" CssClass="form_button" Width="120px" />
            <br />
            <br />
        </div>
        <asp:Label ID="lblMessage" runat="server" Width="500px" CSSClass="error_message"/>    
        <asp:TextBox ID="txtMessage" width="1000" runat="server" TextMode="MultiLine" Height="300" visible="false"></asp:TextBox>

    </div>
</div>
