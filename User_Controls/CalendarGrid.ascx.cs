﻿using System;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.IO;
using System.Configuration;

public partial class User_Controls_CalendarGrid : System.Web.UI.UserControl
{
    public event EventHandler ProjectEditView;
    protected void Page_Load(object sender, EventArgs e)
    {
        cboBudgetResponsibilityArea.OkButtonClicked += new EventHandler(cboBudgetResponsibilityArea_OkButtonClicked);
        cboCategory.OkButtonClicked += new EventHandler(cboCategory_OkButtonClicked);
        cboBusinessUnit.OkButtonClicked += new EventHandler(cboBusinessUnit_OkButtonClicked);
        cboPulsingPlanPriority.OkButtonClicked += new EventHandler(cboPulsingPlanPriority_OkButtonClicked);
        cboMonth.OkButtonClicked += new EventHandler(cboMonth_OkButtonClicked);
        cboBrand.OkButtonClicked += new EventHandler(cboBrand_OkButtonClicked);
        cboYear.Attributes.Add("OnClick", "SetSameTab();");
        if (!IsPostBack)
        {
            String UID = this.UniqueID.Replace("$", "_") + "_";
            calendar_grid.Attributes.Add("OnScroll", "savescroll('" + UID + "'); return false;");

            //populate selection dropdowns
            FillBusinessUnit();
            FillYear();
            FillBrand();
            FillCategory();
            FillBudgetResponsibilityArea();
            FillPulsingPlanPriority();
            FillMonth();

            PopulateLastValues();

            //get current year from session variable set in global.asax
            cboYear.SelectedValue = Session["SelectedYear"].ToString();
            FillCalendarGrid();
            SetScroll();
        }
    }

    public void ExportGrid()
    {
        for (int rowIndex = 0; rowIndex < grdCalendar.Rows.Count; rowIndex++)
        {
            for (int i = 1; i <= 12; i++)
            {
                //Disable link button
                ((LinkButton)grdCalendar.Rows[rowIndex].Cells[i].FindControl("LinkButton" + i)).Enabled = false;
            }
        }
        string attachment = "inline; filename=CalendarPrint.html";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        //Response.ContentType = "application/ms-excel";
        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        Label lblFilterNameValue = new Label();

        lblFilterNameValue.Text = "<b>BudgetResponsibilityArea: </b>" + cboBudgetResponsibilityArea.getSelectedValues(Session["BudgetResponsibilityArea"].ToString());
        lblFilterNameValue.RenderControl(htw);
        lblFilterNameValue.Text = "<br/>" + "<b>BusinessUnit: </b>" + cboBusinessUnit.getSelectedValues(Session["BusinessUnit"].ToString());
        lblFilterNameValue.RenderControl(htw);
        lblFilterNameValue.Text = "<br/>" + "<b>Category: </b>" + cboCategory.getSelectedValues(Session["Category"].ToString());
        lblFilterNameValue.RenderControl(htw);
        lblFilterNameValue.Text = "<br/>" + "<b>Brand: </b>" + cboBrand.getSelectedValues(Session["Brand"].ToString());
        lblFilterNameValue.RenderControl(htw);
        lblFilterNameValue.Text = "<br/>" + "<b>Year: </b>" + cboYear.SelectedValue;
        lblFilterNameValue.RenderControl(htw);
        lblFilterNameValue.Text = "<br/>" + "<b>PulsingPlanPriority: </b>" + cboPulsingPlanPriority.getSelectedValues(Session["PulsingPlanPriority"].ToString());
        lblFilterNameValue.RenderControl(htw);
        grdCalendar.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
        for (int rowIndex = 0; rowIndex < grdCalendar.Rows.Count; rowIndex++)
        {
            for (int i = 1; i <= 12; i++)
            {
                if (grdCalendar.Rows[rowIndex].Cells[i].Text != "-1")
                {
                    //Disable link button
                    ((LinkButton)grdCalendar.Rows[rowIndex].Cells[i].FindControl("LinkButton" + i)).Enabled = true;
                }
            }
        }
    }

    #region GridMethods

    public void SetScroll()
    {
        String UID = this.UniqueID.Replace("$", "_") + "_";
        Page page = HttpContext.Current.CurrentHandler as Page;
        page.ClientScript.RegisterStartupScript(typeof(Page), "setscroll", "<script type='text/javascript'>setscroll('" + UID + "');</script>");
    }

    public void FillCalendarGrid()
    {
        foreach (DataControlField col in grdCalendar.Columns)
        {
            col.Visible = true;
        }

        //Check if session timed out
        if (Session["SelectedYear"] == null || Session["BudgetResponsibilityArea"] == null || Session["BusinessUnit"] == null
            || Session["Category"] == null || Session["Brand"] == null || Session["PulsingPlanPriority"] == null
            || Session["CurrentUserName"] == null || Session["Month"] == null)
        {
            lblErrorMessage.Visible = true;
            lblErrorMessage.Text = "Session timed out, please select calendar page from menu to continue";
            grdCalendar.DataSource = null;
            grdCalendar.DataBind();
        }
        else
        {
            if (Session["SelectedYear"].ToString() != "" && Session["BudgetResponsibilityArea"].ToString() != "" && Session["BusinessUnit"].ToString() != ""
                && Session["Category"].ToString() != "" && Session["Brand"].ToString() != "" && Session["PulsingPlanPriority"].ToString() != ""
                && Session["CurrentUserName"].ToString() != "" && Session["Month"].ToString() != "")
            {
                lblErrorMessage.Text = "";
                lblErrorMessage.Visible = false;
                SqlCommand cmd = DataAccess.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "FillCalendar";
                cmd.Parameters.AddWithValue("@Year", Session["SelectedYear"].ToString());
                cmd.Parameters.AddWithValue("@AreaID", Session["BudgetResponsibilityArea"].ToString());
                cmd.Parameters.AddWithValue("@BusinessUnitID", Session["BusinessUnit"].ToString());
                cmd.Parameters.AddWithValue("@CategoryID", Session["Category"].ToString());
                cmd.Parameters.AddWithValue("@BrandID", Session["Brand"].ToString());
                cmd.Parameters.AddWithValue("@PulsingPlanPriorityID", Session["PulsingPlanPriority"].ToString());
                cmd.Parameters.AddWithValue("@UserID", Session["CurrentUserName"].ToString());
                CalendarDisplayLogic objCalendarDisplay = new CalendarDisplayLogic();
                objCalendarDisplay.SetCalendarInput(DataAccess.ExecuteSelectCommand(cmd));
                DataTable tbl = objCalendarDisplay.ReturnResult();
                grdCalendar.DataSource = tbl;
                grdCalendar.DataBind();

                String[] items = Session["Month"].ToString().Split(',');
                foreach (DataControlField col in grdCalendar.Columns)
                {
                    int removeColumn = 1;
                    foreach (string item in items)
                    {
                        if (col.HeaderText.Contains(item))
                        {
                            removeColumn = 0;
                        }
                    }
                    if (removeColumn == 1 && col.HeaderText != "Brand")
                    {
                        col.Visible = false;
                    }
                    else
                        col.Visible = true;
                }

                UserDataAccess.UpdateCalendarUserSelections(Session["BudgetResponsibilityArea"].ToString(), Session["Category"].ToString(), Session["Brand"].ToString()
                , Session["SelectedYear"].ToString(), Session["PulsingPlanPriority"].ToString()
                , Session["CurrentUserName"].ToString());

                DisableEmptyLinkButtons();
                AddColourCoding();
                MergeColumns();
                RemoveEmptyRows();
            }
            else
            {
                lblErrorMessage.Visible = true;
                lblErrorMessage.Text = "Please select values for all filters";
            }
        }
        foreach (DataControlField col in grdCalendar.Columns)
        {
            if (col.HeaderText.Contains("ID") || col.HeaderText.Contains("Colour"))
            {
                col.Visible = false;
            }
        }
    }

    public void AddColourCoding()
    {
        for (int rowIndex = 0; rowIndex < grdCalendar.Rows.Count; rowIndex++)
        {
            for (int i = 1; i <= 12; i++)
            {
                if (grdCalendar.Columns[i].Visible == true)
                {
                    int colourPos = i + 24;
                    int projectPos = i + 12;
                    if (Color.FromName(grdCalendar.Rows[rowIndex].Cells[colourPos].Text).IsKnownColor)
                    {
                        grdCalendar.Rows[rowIndex].Cells[projectPos].BackColor = Color.FromName(grdCalendar.Rows[rowIndex].Cells[colourPos].Text.ToString());
                    }
                }
            }
        }
    }

    public void MergeColumns()
    {
        for (int rowIndex = grdCalendar.Rows.Count - 1; rowIndex >= 0; rowIndex--)
        {
            GridViewRow row = grdCalendar.Rows[rowIndex];
            for (int i = 2; i <= 12; i++)
            {
                if (grdCalendar.Columns[i].Visible == true && grdCalendar.Columns[i-1].Visible == true)
                {
                    int j = i - 1;
                    int ipos = i + 12;
                    int jpos = j + 12;
                    if (row.Cells[i].Text != "-1" && row.Cells[j].Text != "-1" && row.Cells[i].Text == row.Cells[j].Text)
                    {
                        row.Cells[ipos].ColumnSpan = row.Cells[jpos].ColumnSpan < 2 ? 2 :
                                               row.Cells[jpos].ColumnSpan + 1;
                        row.Cells[jpos].Visible = false;
                    }
                }
            }
        }
    }

    public void RemoveEmptyRows()
    {
        int totalCount = grdCalendar.Rows.Count;
        int removeCount = 0;
        for (int rowIndex = grdCalendar.Rows.Count - 1; rowIndex >= 0; rowIndex--)
        {
            GridViewRow row = grdCalendar.Rows[rowIndex];
            int remove = 1;
            for (int i = 1; i <= 12; i++)
            {
                if (grdCalendar.Columns[i].Visible == true && row.Cells[i].Text != "-1")
                {
                    remove = 0;
                }
            }
            if (remove == 1)
            {
                row.Visible = false;
                removeCount++;
            }
        }
        if (removeCount == totalCount)
        {
            grdCalendar.DataSource = null;
            grdCalendar.DataBind();
        }
    }

    public void DisableEmptyLinkButtons()
    {
        for (int rowIndex = 0; rowIndex < grdCalendar.Rows.Count; rowIndex++)
        {
            for (int i = 1; i <= 12; i++)
            {
                if (grdCalendar.Rows[rowIndex].Cells[i].Text == "-1")
                {
                    //Disable link button
                    ((LinkButton)grdCalendar.Rows[rowIndex].Cells[i].FindControl("LinkButton"+i)).Enabled=false;
                }
            }
        }
    }

    protected void PopulateLastValues()
    {
        DataTable tbl = UserDataAccess.SelectUser(Session["CurrentUserName"].ToString());
        if (Session["BudgetResponsibilityArea"] == null)
        {
            if (tbl.Rows[0]["LastBudgetResponsibilityAreaID"] == DBNull.Value)
            {
                Session["BudgetResponsibilityArea"] = Common.ADOLookup("BudgetResponsibilityAreaID", "qryUserAuthorisations", "UserName = '" + Session["CurrentUserName"].ToString().Replace("'", "''") + "'");
                cboBudgetResponsibilityArea.SetSelectedValue(Session["BudgetResponsibilityArea"].ToString());
            }
            else
            {
                cboBudgetResponsibilityArea.SetSelectedValue(tbl.Rows[0]["LastBudgetResponsibilityAreaID"].ToString());
                Session["BudgetResponsibilityArea"] = cboBudgetResponsibilityArea.getSelectedID();
            }
        }
        else
        {
            cboBudgetResponsibilityArea.SetSelectedValue(Session["BudgetResponsibilityArea"].ToString());
        }

        if (Session["Brand"] == null)
        {
            if (tbl.Rows[0]["LastBrandID"] == DBNull.Value)
            {
                cboBrand.SetSelectedValue("-1");
                Session["Brand"] = cboBrand.getSelectedID();
            }
            else
            {
                cboBrand.SetSelectedValue(tbl.Rows[0]["LastBrandID"].ToString());
                Session["Brand"] = cboBrand.getSelectedID();
            }
        }
        else
        {
            cboBrand.SetSelectedValue(Session["Brand"].ToString());
        }

        if (Session["Category"] == null)
        {
            if (tbl.Rows[0]["LastCategoryID"] == DBNull.Value)
            {
                cboCategory.SetSelectedValue("-1");
                Session["Category"] = cboCategory.getSelectedID();
            }
            else
            {
                cboCategory.SetSelectedValue(tbl.Rows[0]["LastCategoryID"].ToString());
                Session["Category"] = cboCategory.getSelectedID();
            }
        }
        else
        {
            cboCategory.SetSelectedValue(Session["Category"].ToString());
        }

        if (Session["BusinessUnit"] == null)
        {
            //if (tbl.Rows[0]["LastBUCode"] == DBNull.Value)
            //{
                cboBusinessUnit.SetSelectedValue("-1");
                Session["BusinessUnit"] = cboBusinessUnit.getSelectedID();
            //}
            //else
            //{
            //    Session["BusinessUnit"] = tbl.Rows[0]["LastBUCode"].ToString();
            //    cboBusinessUnit.SetSelectedValue(tbl.Rows[0]["LastBUCode"].ToString());
            //}
        }
        else
        {
            cboBusinessUnit.SetSelectedValue(Session["BusinessUnit"].ToString());
        }

        if (Session["PulsingPlanPriority"] == null)
        {
            if (tbl.Rows[0]["LastPulsingPlanPriorityID"] == DBNull.Value)
            {
                cboPulsingPlanPriority.SetSelectedValue("-1");
                Session["PulsingPlanPriority"] = cboPulsingPlanPriority.getSelectedID();
            }
            else
            {
                cboPulsingPlanPriority.SetSelectedValue(tbl.Rows[0]["LastPulsingPlanPriorityID"].ToString());
                Session["PulsingPlanPriority"] = cboPulsingPlanPriority.getSelectedID();
            }
        }
        else
        {
            cboPulsingPlanPriority.SetSelectedValue(Session["PulsingPlanPriority"].ToString());
        }

        if (Session["SelectedYear"] == null)
        {
            if (tbl.Rows[0]["LastYear"] == DBNull.Value)
            {
                cboYear.SelectedIndex = 0;
                Session["SelectedYear"] = cboYear.SelectedValue;
            }
            else
            {
                cboYear.SelectedValue = tbl.Rows[0]["LastYear"].ToString();
                Session["SelectedYear"] = cboYear.SelectedValue;
            }
        }
        else
        {
            cboYear.SelectedValue = Session["SelectedYear"].ToString();
        }

        if (Session["Month"] == null)
        {
            cboMonth.SetSelectedValue("-1");
            Session["Month"] = cboMonth.getSelectedValues();
        }
        else
        {
            cboMonth.SetSelectedName(Session["Month"].ToString());
        }
    }

    #endregion

    #region dropdowns

    protected void FillBudgetResponsibilityArea()
    //fill BudgetResponsibilityArea dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillBudgetResponsibilityArea";
        cmd.Parameters.AddWithValue("@ZeroValueText", "[None]");
        //cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        cboBudgetResponsibilityArea.AddDataToList(tbl, "EntityName", "ID");
    }

    protected void FillBusinessUnit()
    //fill BusinessUnit dropdownlist for current user
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillBusinessUnit";
        cmd.Parameters.AddWithValue("@ZeroValueText", "[None]");
        //cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        cboBusinessUnit.AddDataToList(tbl, "EntityName", "ID");
    }

    protected void FillBrand()
    //fill brand dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillBrandActive";
        cmd.Parameters.AddWithValue("@ZeroValueText", "[None]");
        //cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        cmd.Parameters.AddWithValue("@IncludeInactive", 0);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        cboBrand.AddDataToList(tbl, "EntityName", "ID");
    }

    protected void FillCategory()
    //fill category dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillCategoryActive";
        cmd.Parameters.AddWithValue("@ZeroValueText", "[None]");
        //cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        cmd.Parameters.AddWithValue("@IncludeInactive", 0);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        cboCategory.AddDataToList(tbl, "EntityName", "ID");
    }

    protected void FillYear()
    //fill year dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillYear";
        cmd.Parameters.AddWithValue("@ZeroValueText", "[None]");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboYear;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillPulsingPlanPriority()
    //fill project type dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillPulsingPlanPriority";
        cmd.Parameters.AddWithValue("@ZeroValueText", "[None]");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        cboPulsingPlanPriority.AddDataToList(tbl, "EntityName", "ID");
    }

    protected void FillMonth()
    {
        DataTable dt1 = new DataTable();
        dt1.Columns.Add(new DataColumn("EntityName", typeof(System.String)));
        dt1.Columns.Add(new DataColumn("ID", typeof(System.String)));
        dt1.Rows.Add(new string[] { "Jan", "1" });
        dt1.Rows.Add(new string[] { "Feb", "2" });
        dt1.Rows.Add(new string[] { "Mar", "3" });
        dt1.Rows.Add(new string[] { "Apr", "4" });
        dt1.Rows.Add(new string[] { "May", "5" });
        dt1.Rows.Add(new string[] { "Jun", "6" });
        dt1.Rows.Add(new string[] { "Jul", "7" });
        dt1.Rows.Add(new string[] { "Aug", "8" });
        dt1.Rows.Add(new string[] { "Sep", "9" });
        dt1.Rows.Add(new string[] { "Oct", "10" });
        dt1.Rows.Add(new string[] { "Nov", "11" });
        dt1.Rows.Add(new string[] { "Dec", "12" });
        cboMonth.AddDataToList(dt1, "EntityName", "ID");
    }

    protected void cboYear_TextChanged(object sender, EventArgs e)
    {
        Session["SelectedYear"] = cboYear.SelectedValue;
        FillCalendarGrid();
    }

    protected void cboBudgetResponsibilityArea_OkButtonClicked(object sender, EventArgs e)
    {
        Session["BudgetResponsibilityArea"] = cboBudgetResponsibilityArea.getSelectedID();
        FillCalendarGrid();
    }

    protected void cboPulsingPlanPriority_OkButtonClicked(object sender, EventArgs e)
    {
        Session["PulsingPlanPriority"] = cboPulsingPlanPriority.getSelectedID();
        FillCalendarGrid();
    }

    protected void cboMonth_OkButtonClicked(object sender, EventArgs e)
    {
        Session["Month"] = cboMonth.getSelectedValues();
        FillCalendarGrid();
    }

    protected void cboCategory_OkButtonClicked(object sender, EventArgs e)
    {
        Session["Category"] = cboCategory.getSelectedID();
        FillCalendarGrid();
    }

    protected void cboBusinessUnit_OkButtonClicked(object sender, EventArgs e)
    {
        Session["BusinessUnit"] = cboBusinessUnit.getSelectedID();
        FillCalendarGrid();
    }

    protected void cboBrand_OkButtonClicked(object sender, EventArgs e)
    {
        Session["Brand"] = cboBrand.getSelectedID();
        FillCalendarGrid();
    }

    #endregion

    protected void LinkButton_Click(object sender, EventArgs e)
    {
        string LinkButtonID = ((LinkButton)sender).ID;
        GridViewRow selectedRow = (GridViewRow)((LinkButton)sender).NamingContainer;
        int columnIndex = int.Parse(LinkButtonID.Replace("LinkButton", ""));
        int rowIndex = selectedRow.RowIndex;
        if (grdCalendar.Rows[rowIndex].Cells[columnIndex].Text != "-1")
        {
            Session["SelectedProjectID"] = grdCalendar.Rows[rowIndex].Cells[columnIndex].Text;
            ProjectEditView(sender, e);
        }
    }

    protected void cmdExport_Click(object sender, EventArgs e)
    {
        ExportGrid();
    }
}
