﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WorklistSummary_rs : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //txtUser.Text = Session["CurrentUserName"].ToString();
        //Page.User.Identity.Name.ToString();
        Refresh();
    }

    public void Refresh()
    {
        //summarise Worklists
        //DataTable tbl = WorklistDataAccess.SelectWorklist5(Session["CurrentUserName"].ToString());
        //txtWorklist5.Text = tbl.Rows.Count.ToString();

        //tbl = WorklistDataAccess.SelectWorklist1(Session["CurrentUserName"].ToString());
        //txtWorklist1.Text = tbl.Rows.Count.ToString();

        //tbl = WorklistDataAccess.SelectWorklist3(Session["CurrentUserName"].ToString());
        //txtWorklist3.Text = tbl.Rows.Count.ToString();

        //tbl = WorklistDataAccess.SelectWorklist2(Session["CurrentUserName"].ToString());
        //txtWorklist2.Text = tbl.Rows.Count.ToString();

        //tbl = WorklistDataAccess.SelectWorklist4(Session["CurrentUserName"].ToString());
        //txtWorklist4.Text = tbl.Rows.Count.ToString();

        //tbl = WorklistDataAccess.SelectWorklist7(Session["CurrentUserName"].ToString());
        //txtWorklist7.Text = tbl.Rows.Count.ToString();

        //tbl = WorklistDataAccess.SelectWorklist8(Session["CurrentUserName"].ToString());
        //txtWorklist8.Text = tbl.Rows.Count.ToString();

        //tbl = WorklistDataAccess.SelectWorklist6(Session["CurrentUserName"].ToString());
        //txtWorklist6.Text = tbl.Rows.Count.ToString();

        //tbl = WorklistDataAccess.SelectWorklist9(Session["CurrentUserName"].ToString());
        //txtWorklist9.Text = tbl.Rows.Count.ToString();

        DataTable tbl = WorklistDataAccess.WorklistSummary(Session["CurrentUserName"].ToString());
        txtWorklist1.Text = tbl.Rows[0].ItemArray[0].ToString();
        txtWorklist2.Text = tbl.Rows[0].ItemArray[1].ToString();
        txtWorklist3.Text = tbl.Rows[0].ItemArray[2].ToString();
        txtWorklist4.Text = tbl.Rows[0].ItemArray[3].ToString();
        txtWorklist5.Text = tbl.Rows[0].ItemArray[4].ToString();
        txtWorklist6.Text = tbl.Rows[0].ItemArray[5].ToString();
        txtWorklist7.Text = tbl.Rows[0].ItemArray[6].ToString();
        txtWorklist8.Text = tbl.Rows[0].ItemArray[7].ToString();
        txtWorklist9.Text = tbl.Rows[0].ItemArray[8].ToString();
        txtWorklist10.Text =tbl.Rows[0].ItemArray[9].ToString();
        txtWorklist11.Text = tbl.Rows[0].ItemArray[10].ToString();
        txtWorklist12.Text = tbl.Rows[0].ItemArray[11].ToString();
    }
}
