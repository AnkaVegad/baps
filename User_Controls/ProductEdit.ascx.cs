﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Controls_ProductEdit : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillCategory();
            FillBrand();
            FillBU();
            FillSubCategory();            
        }
    }

    //protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
    ////check that the weekly breakdown matches the ItemQuantity entered
    ////seems to fire even if requiredfieldvalidation has failed, so use try to check for correct entries in fields first
    //{
    //    try
    //    {
    //        if (Convert.ToInt16(Phasing01.Text) + Convert.ToInt16(Phasing02.Text) + Convert.ToInt16(Phasing03.Text) + Convert.ToInt16(Phasing04.Text) + Convert.ToInt16(Phasing05.Text) + Convert.ToInt16(Phasing06.Text) + Convert.ToInt16(Phasing07.Text) + Convert.ToInt16(Phasing08.Text) + Convert.ToInt16(Phasing09.Text) + Convert.ToInt16(Phasing10.Text) + Convert.ToInt16(Phasing11.Text) + Convert.ToInt16(Phasing12.Text) + Convert.ToInt16(Phasing13.Text) == Convert.ToInt16(ItemQuantity.Text))
    //        {
    //            args.IsValid = true;
    //        }
    //        else
    //        {
    //            args.IsValid = false;
    //        }
    //    }
    //    catch
    //    { }
    //}

#region properties

    //public string HeaderText
    //{
    //    set { lblHeader.Text = value; }
    //}

#endregion

#region events

    //protected void cmdReset_Click(object sender, EventArgs e)
    //{
    //    int intResult = SOPActivityDataAccess.ResetAutoPhasing(Convert.ToInt32(ActivityID.Text));
    //}

#endregion

#region methods

    public void PopulateForm(string strTUEAN)
    {
        DataTable tbl = ProductDataAccess.SelectProductByTUEAN(strTUEAN);
        DataRow r = tbl.Rows[0];
        TUEAN.Text = r["TUEAN"].ToString();
        NoInPack.Text = r["NoInPack"].ToString();
        ProductName.Text = r["ProductName"].ToString();
        VariantName.Text = r["VariantName"].ToString();
        ContainerCode.Text = r["ContainerCode"].ToString();
        WeightVolume.Text = r["WeightVolume"].ToString();
        WeightVolumeUnit.Text = r["WeightVolumeUnit"].ToString();
        PacksPerCase.Text = r["PacksPerCase"].ToString();
        PRODH.Text = r["PRODH"].ToString();
        VTEXT.Text = r["VTEXT"].ToString();
        VMSTA.Text = r["VMSTA"].ToString();
        //BUCode.SelectedValue = r["BUCode"].ToString();
        PRODH_1160_20.Text = r["PRODH_1160_20"].ToString();
        VTEXT_1160_20.Text = r["VTEXT_1160_20"].ToString();
        VMSTA_1160_20.Text = r["VMSTA_1160_20"].ToString();
        CategoryID.SelectedValue = r["CategoryID"].ToString();
        SubCategoryID.SelectedValue = r["SubCategoryID"].ToString();
        BrandID.SelectedValue = r["BrandID"].ToString();
        SPART.Text = r["SPART"].ToString();
        PromoName.Text = r["PromoName"].ToString();
    }

    public void ClearControls()
    {
        foreach (Control c in Controls)
        {
            if (c is TextBox) { ((TextBox)c).Text = ""; }
            if (c is DropDownList) { ((DropDownList)c).SelectedValue = "0"; }
        }
    }

    public void EnableControls(bool mode)
    {
        //BUCode.Enabled = mode;
        SubCategoryID.Enabled = mode;
        BrandID.Enabled = mode;
        ContainerCode.Enabled = mode;
        PromoName.Enabled = mode;
    }

    //public void NewRecord()
    //{
    //    ClearControls();
    //    CustomerID.Enabled = true;
    //    CategoryID.Enabled = true;
    //    PromoMechanicGroupID.Enabled = true;
    //    PromoLengthWeeks.Enabled = true;
    //}

    //public int CreateRecord()
    //{
    //    if (Page.IsValid)
    //    {
    //        int intNewID = SOPPhasingBreakdownDataAccess.InsertPhasingBreakdown(Convert.ToInt32(CustomerID.SelectedValue), Convert.ToInt32(CategoryID.SelectedValue), Convert.ToInt16(PromoMechanicGroupID.SelectedValue), Convert.ToInt16(PromoLengthWeeks.SelectedValue), Convert.ToInt16(Phasing01.Text), Convert.ToInt16(Phasing02.Text), Convert.ToInt16(Phasing03.Text), Convert.ToInt16(Phasing04.Text), Convert.ToInt16(Phasing05.Text), Convert.ToInt16(Phasing06.Text), Convert.ToInt16(Phasing07.Text), Convert.ToInt16(Phasing08.Text), Convert.ToInt16(Phasing09.Text), Convert.ToInt16(Phasing10.Text), Convert.ToInt16(Phasing11.Text), Convert.ToInt16(Phasing12.Text), Convert.ToInt16(Phasing13.Text), Session["CurrentUserName"].ToString());
    //        ClearControls();
    //        return intNewID;
    //    }
    //    else { return 99; }
    //}

    public int UpdateRecord()
    {
        //update product
        int intResult = ProductDataAccess.UpdateProduct(TUEAN.Text, Convert.ToInt32(CategoryID.SelectedValue), Convert.ToInt32(BrandID.SelectedValue), ContainerCode.Text, PromoName.Text, Convert.ToInt32(SubCategoryID.SelectedValue));
        return intResult;
    }


#endregion

#region dropdowns

    protected void FillBrand()
    //fill brand dropdownlist for current user
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_md_FillBrand";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = BrandID;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillCategory()
    //fill category dropdownlist for current user
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_sop_FillCategory";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = CategoryID;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillSubCategory()
    //fill category dropdownlist for current user
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_sop_FillSubCategory";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        cmd.Parameters.AddWithValue("@IncludeInactive", 1);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = SubCategoryID;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillBU()
    //fill business unit dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_sop_FillBU";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = BUCode;
        d.DataSource = tbl;
        d.DataBind();
    }

#endregion

}
