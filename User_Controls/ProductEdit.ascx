﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductEdit.ascx.cs" Inherits="User_Controls_ProductEdit" %>
    
    <div id="project_edit_area">
    <table><tr><td>
        <table><tr><td>
            <table border="0">
                <tr><td>Case EAN</td><td>
                    <asp:TextBox ID="TUEAN" runat="server" Width="100px" Enabled="False" MaxLength="20" />
                </td></tr>
                <tr><td>No In Pack</td><td>
                    <asp:TextBox ID="NoInPack" runat="server" Width="50px" Enabled="False" MaxLength="5"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </td></tr>
                <tr><td>Product</td><td>
                    <asp:TextBox ID="ProductName" runat="server" Width="100px" Enabled="False" MaxLength="40"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </td></tr>
                <tr><td>Variant</td><td>
                    <asp:TextBox ID="VariantName" runat="server" Width="100px" Enabled="False" MaxLength="40"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </td></tr>
                <tr><td>Container</td><td>
                    <asp:TextBox ID="ContainerCode" runat="server" Width="50px" Enabled="False" MaxLength="40"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </td></tr>
                <tr><td>Units Per Case</td><td>
                    <asp:TextBox ID="PacksPerCase" runat="server" Width="50px" MaxLength="12" Enabled="False"
                        onkeyup="Javascript:setNeedToConfirm(true);"/>
                </td></tr>
                <tr><td>Weight/Volume</td><td>
                    <asp:TextBox ID="WeightVolume" runat="server" Width="50px" Enabled="False" MaxLength="10"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </td></tr>
                <tr><td>Wt/Vol Unit</td><td>
                    <asp:TextBox ID="WeightVolumeUnit" runat="server" Width="50px" Enabled="False" MaxLength="10"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </td></tr>
                <tr><td>Promo</td><td>
                    <asp:TextBox ID="PromoName" runat="server" Width="100px" MaxLength="50" Enabled="False"
                        onkeyup="Javascript:setNeedToConfirm(true);"/>
                </td></tr>
            </table>
        </td>

        <td valign="top">
            <table style="width:600px;" border="0">
                <tr><td>Pricing Hierarchy GB</td><td>
                    <asp:TextBox ID="PRODH" runat="server" Width="90px" MaxLength="12" Enabled="False"
                        onkeyup="Javascript:setNeedToConfirm(true);"/>
                    <asp:TextBox ID="VTEXT" runat="server" Width="270px" MaxLength="100" Enabled="False"
                        onkeyup="Javascript:setNeedToConfirm(true);"/>
                <tr><td>Pricing Hierarchy IE</td><td>
                    <asp:TextBox ID="PRODH_1160_20" runat="server" Width="90px" MaxLength="12" Enabled="False"
                        onkeyup="Javascript:setNeedToConfirm(true);"/>
                    <asp:TextBox ID="VTEXT_1160_20" runat="server" Width="270px" MaxLength="100" Enabled="False"
                        onkeyup="Javascript:setNeedToConfirm(true);"/>
                </td></tr>
                <tr><td>Status GB</td><td>
                    <asp:TextBox ID="VMSTA" runat="server" Width="40px" Enabled="False" MaxLength="2"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </td></tr>
                <tr><td>Status IE</td><td>
                    <asp:TextBox ID="VMSTA_1160_20" runat="server" Width="40px" Enabled="False" MaxLength="2"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </td></tr>
                <tr><td>Business Unit</td><td>
                    <asp:DropDownList ID="BUCode" runat="server" Width="50px" Enabled="False" 
                        DataTextField="EntityName" DataValueField="ID" />
                </td></tr>
                <tr><td>Category</td><td>
                    <asp:DropDownList ID="CategoryID" runat="server" Width="150px" Enabled="False" 
                        DataTextField="EntityName" DataValueField="ID" />
                    <asp:RangeValidator ID="RangeValidator6" runat="server" 
                        ErrorMessage="*Required" MaximumValue="Z" MinimumValue="1" 
                        ControlToValidate="CategoryID" Display="Dynamic" ValidationGroup="Product" />
                </td></tr>
                <tr><td>Sub-Category</td><td>
                    <asp:DropDownList ID="SubCategoryID" runat="server" Width="150px" Enabled="False" 
                        DataTextField="EntityName" DataValueField="ID" />
                    <asp:RangeValidator ID="RangeValidator2" runat="server" 
                        ErrorMessage="*Required" MaximumValue="Z" MinimumValue="1" 
                        ControlToValidate="SubCategoryID" Display="Dynamic" ValidationGroup="Product" />
                </td></tr>
                <tr><td>Brand</td><td>
                    <asp:DropDownList ID="BrandID" runat="server" Width="150px" 
                        Enabled="False" DataTextField="EntityName" DataValueField="ID" />
                    <asp:RangeValidator ID="RangeValidator1" runat="server" 
                        ErrorMessage="*Required" MaximumValue="Z" MinimumValue="1" 
                        ControlToValidate="BrandID" Display="Dynamic" ValidationGroup="Product" />
                </td></tr>
                <tr><td>Division</td><td>
                    <asp:TextBox ID="SPART" runat="server" Width="40px" Enabled="False" MaxLength="2"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </td></tr>
            </table>
        </td></tr></table>
    </td></tr></table>
    </div>