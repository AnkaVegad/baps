﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReallocationGrids.ascx.cs" Inherits="User_Controls_ReallocationGrids" %>
<style>
.grid_row{
height:50px;
grid-row-sizing:50px;
line-height:50px;
padding:10px;

}
.grid_alternating_row{
height:50px;
grid-row-sizing:50px;
line-height:50px;
padding:10px;

}
</style>

    <!--Reallocations From Grid-->
    Reallocations From This Project/Customer:<br /><br />
    <div id="activity_grid">
        <asp:GridView ID="grdReallocationFrom" runat="server" width="1070px"
            AutoGenerateColumns="False" GridLines="Vertical"
            RowStyle-Wrap="false" EmptyDataText="No Records Found" 
            HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
            SelectedRowStyle-Wrap="false"
            AllowSorting="false" AllowPaging="false"
            DataKeyNames="ReallocationID" >
            <Columns>
                <asp:BoundField DataField="ReallocationID" InsertVisible="False" ReadOnly="True" Visible="False" />
                <asp:BoundField DataField="CreatedBy" HeaderText="User" SortExpression="CreatedDate" ItemStyle-Width="100px" />
                <asp:BoundField DataField="CreatedDate" HeaderText="Date" SortExpression="CreatedDate" ItemStyle-Width="50px" DataFormatString="{0:dd/MM/yy}" />
                <asp:BoundField DataField="BudgetAmount" HeaderText="Amount" SortExpression="CreatedDate" ItemStyle-Width="50px" />
                <asp:BoundField DataField="ToProjectName" HeaderText="To Project" SortExpression="CreatedDate" ItemStyle-Width="250px" />
                <asp:BoundField DataField="ToCustomerName" HeaderText="To Customer" SortExpression="CreatedDate" ItemStyle-Width="100px" />
                <asp:BoundField DataField="FromSpendTypeName" HeaderText="From" SortExpression="CreatedDate" ItemStyle-Width="40px" />
                <asp:BoundField DataField="ToSpendTypeName" HeaderText="To" SortExpression="CreatedDate" ItemStyle-Width="40px" />
                <asp:BoundField DataField="Comments" HeaderText="Comments" SortExpression="CreatedDate" ItemStyle-Width="200px" />
            </Columns>
            <RowStyle CssClass="grid_row" Wrap="False"/>
            <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
            <HeaderStyle CssClass="grid_header" Wrap="False" />
            <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
        </asp:GridView>
        <br />
        <br />

        <!--Reallocations To Grid-->
        Reallocations To This Project/Customer:<br /><br />
        <asp:GridView ID="grdReallocationTo" runat="server" width="1070px"
            AutoGenerateColumns="False" GridLines="Vertical"
            RowStyle-Wrap="false" EmptyDataText="No Records Found" 
            HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
            SelectedRowStyle-Wrap="false"
            AllowSorting="false" AllowPaging="false"
            DataKeyNames="ReallocationID" >
            <Columns>
                <asp:BoundField DataField="ReallocationID" InsertVisible="False" ReadOnly="True" Visible="False" />
                <asp:BoundField DataField="CreatedBy" HeaderText="User" SortExpression="CreatedDate" ItemStyle-Width="100px" />
                <asp:BoundField DataField="CreatedDate" HeaderText="Date" SortExpression="CreatedDate" ItemStyle-Width="50px" DataFormatString="{0:dd/MM/yy}" />
                <asp:BoundField DataField="BudgetAmount" HeaderText="Amount" SortExpression="CreatedDate" ItemStyle-Width="50px" />
                <asp:BoundField DataField="FromProjectName" HeaderText="From Project" SortExpression="CreatedDate" ItemStyle-Width="250px" />
                <asp:BoundField DataField="FromCustomerName" HeaderText="From Customer" SortExpression="CreatedDate" ItemStyle-Width="100px" />
                <asp:BoundField DataField="FromSpendTypeName" HeaderText="From" SortExpression="CreatedDate" ItemStyle-Width="40px" />
                <asp:BoundField DataField="ToSpendTypeName" HeaderText="To" SortExpression="CreatedDate" ItemStyle-Width="40px" />
                <asp:BoundField DataField="Comments" HeaderText="Comments" SortExpression="CreatedDate" ItemStyle-Width="200px" />
            </Columns>
            <RowStyle CssClass="grid_row" Wrap="False" />
            <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
            <HeaderStyle CssClass="grid_header" Wrap="False" />
            <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
        </asp:GridView>
    </div><br />
    <br />
