﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Controls_ActivityTypeGrid : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillActiveIndicatorFilter();
            FillBudgetResponsibilityArea();
            FillSpendTypeFilter();
            FillGLLevels();
            grd.SelectedIndex = 0;
        }
    }

#region properties

    public string SelectedValue
    {
        get { 
            try
            {
                return grd.SelectedValue.ToString(); 
            }
            catch { return "0"; }
        }
    }

    public string HeaderText
    {
        set { lblHeader.Text = value; }
    }

#endregion

#region methods

    public void FillDropdowns()
    {
        FillActiveIndicatorFilter();
        FillBudgetResponsibilityArea();
        FillSpendTypeFilter();
        FillGLLevels();
    }

    public void PopulateGrid()
    {
        //populate the grid 
        string strSQL = "SELECT DISTINCT ActivityTypeID, ActivityTypeName, GLCode, GLLevel1Name, GLLevel2Name, GLLevel3Name FROM vw_ap_ActivityType b WHERE ActivityTypeID > 0 ";
        if (cboBudgetResponsibilityArea.SelectedValue != "0")
        {
            strSQL += "AND BudgetResponsibilityAreaID = " + cboBudgetResponsibilityArea.SelectedValue + " ";
        }
        if (ActiveIndicatorFilter.SelectedValue != "-1")
        {
            strSQL += "AND ActiveIndicator = " + ActiveIndicatorFilter.SelectedValue + " ";
        }
        if (SpendTypeFilter.SelectedValue != "0")
        {
            strSQL += "AND SpendTypeID = " + SpendTypeFilter.SelectedValue + " ";
        }
        if (GLLevel1Filter.SelectedValue != "0")
        {
            strSQL += "AND GLLevel1ID = " + GLLevel1Filter.SelectedValue + " ";
        }
        if (GLLevel2Filter.SelectedValue != "0")
        {
            strSQL += "AND GLLevel2ID = " + GLLevel2Filter.SelectedValue + " ";
        }
        if (GLLevel3Filter.SelectedValue != "0")
        {
            strSQL += "AND GLLevel3ID = " + GLLevel3Filter.SelectedValue + " ";
        }
        //next line remmed out because all users in this screen are superusers - but problem because superusers will be able to see Jolin ActivityTypes in this screen *** so temp ***
        //Readded this line by Vidya 29-Jan-16 as same function reused in SelectFilters
        strSQL += "AND EXISTS(SELECT 'X' FROM qryUserAuthorisations a WHERE a.UserName = '" + Session["CurrentUserName"].ToString().Replace("'", "''") + "' AND (a.BudgetResponsibilityAreaID = b.BudgetResponsibilityAreaID)) ";
        strSQL += "ORDER BY ActivityTypeName;";
        sds.SelectCommand = strSQL;
        grd.DataSourceID = "sds";
        grd.DataBind();
    }

    public void RefreshGrid()
    {
        grd.DataBind();
        grd.SelectedIndex = 0;
    }

    // (added 24/02/13)
    public void SelectFilters(string strSpendTypeID, string strBudgetResponsibilityAreaID)
    {
        //added 29/10/14:
        txtSearch.Text = String.Empty;

        SpendTypeFilter.SelectedValue = strSpendTypeID;
        cboBudgetResponsibilityArea.SelectedValue = strBudgetResponsibilityAreaID;
        ActiveIndicatorFilter.SelectedValue = "1";
        SpendTypeFilter.Enabled = false;
        cboBudgetResponsibilityArea.Enabled = false;
        ActiveIndicatorFilter.Enabled = false;
        PopulateGrid();
    }

    public void FillGLLevels()
    {
        FillGLLevel("1", GLLevel1Filter);
        FillGLLevel("2", GLLevel2Filter);
        FillGLLevel("3", GLLevel3Filter);
    }

#endregion
    
#region gridevents

    protected void grd_SelectedIndexChanged(object sender, EventArgs e)
    {
    }

    protected void grd_PageIndexChanged(object sender, EventArgs e)
    {
        grd.SelectedIndex = 0;
    }

    protected void grd_Sorted(object sender, EventArgs e)
    {
        grd.SelectedIndex = 0;
    }

    protected void grd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(this.grd, "Select$" + e.Row.RowIndex);
        }
    }

#endregion

#region dropdowns

    protected void FillBudgetResponsibilityArea()
    //fill BudgetResponsibilityArea dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillBudgetResponsibilityArea";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboBudgetResponsibilityArea;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillActiveIndicatorFilter()
    //populate the ActiveIndicator Filter dropdown
    {
        ActiveIndicatorFilter.Items.Add(new ListItem("<All>", "-1"));
        ActiveIndicatorFilter.Items.Add(new ListItem("Active", "1"));
        ActiveIndicatorFilter.Items.Add(new ListItem("Inactive", "0"));
    }

    protected void FillSpendTypeFilter()
    //populate the SpendTypeID Filter dropdown
    {
        SpendTypeFilter.Items.Add(new ListItem("<All>", "0"));
        SpendTypeFilter.Items.Add(new ListItem("A&P", "1"));
        SpendTypeFilter.Items.Add(new ListItem("TCC", "2"));
        SpendTypeFilter.Items.Add(new ListItem("Coupons", "3"));
    }

    protected void FillGLLevel(string strGLHierarchyLevel, DropDownList d)
    //fill GL Level dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillGLLevel";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        cmd.Parameters.AddWithValue("@GLHierarchyLevel", strGLHierarchyLevel);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        d.DataSource = tbl;
        d.DataBind();
    }

#endregion
    protected void cmdGo_Click(object sender, EventArgs e)
    {
        PopulateGrid();
    }
    protected void cboBudgetResponsibilityArea_SelectedIndexChanged(object sender, EventArgs e)
    {
        PopulateGrid();
    }
    protected void SpendTypeFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        PopulateGrid();
    }
    protected void ActiveIndicatorFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        PopulateGrid();
    }
    protected void GLLevel1Filter_SelectedIndexChanged(object sender, EventArgs e)
    {
        PopulateGrid();
    }
    protected void GLLevel2Filter_SelectedIndexChanged(object sender, EventArgs e)
    {
        PopulateGrid();
    }
    protected void GLLevel3Filter_SelectedIndexChanged(object sender, EventArgs e)
    {
        PopulateGrid();
    }
}
