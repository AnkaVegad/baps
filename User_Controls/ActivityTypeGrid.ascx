﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActivityTypeGrid.ascx.cs" Inherits="User_Controls_ActivityTypeGrid" %>
<!--Warning - page has been rebuilt for Axiom - do not copy to Prod-->
<asp:Panel ID="Panel1" runat="server" DefaultButton="cmdGo">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="parent">
    <!--Header and Filter Criteria-->
    <div id="resizable1" class="resizable ui-widget-content">
        <br />
        <asp:Label ID="lblHeader" Text="Activity Types" runat="server" CssClass="page_header" />
        <br />
        <br />
        <table border="0" style="width:100%">
            <tr>
                <td>Area</td>
                <td>
                    <asp:DropDownList id="cboBudgetResponsibilityArea" runat="server" width="170px" ForeColor="Black"
                        DataTextField="EntityName" DataValueField="ID" AutoPostBack="True"
                        OnSelectedIndexChanged="cboBudgetResponsibilityArea_SelectedIndexChanged"/>
                </td>
            </tr>
            <tr>
                <td>Spend Type</td><td>
                    <asp:DropDownList id="SpendTypeFilter" runat="server" width="170px" ForeColor="Black"
                        DataTextField="EntityName" DataValueField="ID" AutoPostBack="True" OnSelectedIndexChanged="SpendTypeFilter_SelectedIndexChanged"/>
                </td>
            </tr>
            <tr>
                <td>Status</td><td>
                    <asp:DropDownList id="ActiveIndicatorFilter" runat="server" ForeColor="Black"  
                        DataTextField="EntityName" DataValueField="ID" AutoPostBack="True" OnSelectedIndexChanged="ActiveIndicatorFilter_SelectedIndexChanged"/>
                </td>
            </tr>
            <tr>
                <td colspan="2">GL Hierarchy </td>
            </tr>
            <tr>
            <td>Level 1</td><td>
                    <asp:DropDownList id="GLLevel1Filter" runat="server"  width="170px" ForeColor="Black"
                        DataTextField="EntityName" DataValueField="ID" AutoPostBack="True" OnSelectedIndexChanged="GLLevel1Filter_SelectedIndexChanged"/>
                </td>
            </tr>
            <tr>
                <td>Level 2</td><td>
                    <asp:DropDownList id="GLLevel2Filter" runat="server" width="170px" ForeColor="Black"
                        DataTextField="EntityName" DataValueField="ID" AutoPostBack="True" OnSelectedIndexChanged="GLLevel2Filter_SelectedIndexChanged"/>
                </td>
            </tr>
            <tr>
                <td>Level 3</td><td>
                    <asp:DropDownList id="GLLevel3Filter" runat="server"  width="170px" ForeColor="Black"
                        DataTextField="EntityName" DataValueField="ID" AutoPostBack="True" OnSelectedIndexChanged="GLLevel3Filter_SelectedIndexChanged"/>
                </td>
            </tr>
        </table>
        
    </div>        

    <!--Search and Grid-->
    <div id="resizable2" class="ui-widget-content">
        
        <!--Data Source - required to take advantage of sorting and paging functionality of the GridView-->
        <asp:SqlDataSource ID="sds" runat="server" 
            ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
            FilterExpression="ActivityTypeName Like '%{0}%'">
            <FilterParameters>
                <asp:ControlParameter ControlID="txtSearch" PropertyName="Text" />
            </FilterParameters>
        </asp:SqlDataSource>

        <!--Search-->
        <table style="width:100%"><tr><td style="text-align:right">
            <asp:Label ID="lblSearch" Text="Search Activity Type Name:" runat="server"></asp:Label>
            <asp:TextBox ID="txtSearch" Runat="server" Width="180px" placeholder="Search Activity Type Name" />
            <asp:Button ID="cmdGo" Text="Go" Runat="server" CausesValidation="false" OnClick="cmdGo_Click"/>
        </td></tr></table>

        <!--Grid-->
        <div id="project_grid_rs">
            <asp:GridView ID="grd" runat="server" width="100%"
                AutoGenerateColumns="False" GridLines="Vertical"
                RowStyle-Wrap="false" EmptyDataText="No records found" 
                HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
                SelectedRowStyle-Wrap="false"
                onselectedindexchanged="grd_SelectedIndexChanged" 
                OnRowDataBound="grd_RowDataBound" onsorted="grd_Sorted" AllowSorting="True" 
                DataKeyNames="ActivityTypeID" DataSourceID="sds" PageSize="20">
                <Columns>
                    <asp:BoundField DataField="ActivityTypeID" ReadOnly="True" Visible="False" />
                    <asp:BoundField DataField="ActivityTypeName" HeaderText="Activity Type Name" SortExpression="ActivityTypeName" />
                    <asp:BoundField DataField="GLCode" HeaderText="GL Code" SortExpression="GLCode" />
                    <asp:BoundField DataField="GLLevel1Name" HeaderText="Level 1" SortExpression="GLLevel1Name" />
                    <asp:BoundField DataField="GLLevel2Name" HeaderText="Level 2" SortExpression="GLLevel2Name" />
                    <asp:BoundField DataField="GLLevel3Name" HeaderText="Level 3" SortExpression="GLLevel3Name" />
                </Columns>
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle CssClass="grid_row" Wrap="False" BackColor="#EFF3FB" />
                <SelectedRowStyle CssClass="grid_selected" Wrap="False" BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle CssClass="grid_header" Wrap="False" />            
                <EditRowStyle BackColor="#2461BF" />
                <FooterStyle CssClass="grid_footer" Wrap="False" />
                <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" BackColor="White" />
            </asp:GridView>
            <asp:Label ID="txtMessage" runat="server" />
       </div>
    </div>
</div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
