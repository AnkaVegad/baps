﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Controls_PulsingPlanPriorityEdit : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        //{
        //    //populate the activity indicator dropdown
        //    ActiveIndicator.Items.Add(new ListItem("", "0"));
        //    ActiveIndicator.Items.Add(new ListItem("Active", "1"));
        //    ActiveIndicator.Items.Add(new ListItem("Inactive", "2"));
        //}
    }

#region properties

    public string HeaderText
    {
        set { lblHeader.Text = value; }
    }

#endregion

#region methods

    public void PopulateForm(int intPulsingPlanPriorityID)
    {
        PulsingPlanPriorityID.Text = intPulsingPlanPriorityID.ToString();
        DataTable tbl = BAPSMiscDataAccess.SelectPulsingPlanPriority(intPulsingPlanPriorityID);
        DataRow r = tbl.Rows[0];
        //autopopulate text boxes except for comparators
        foreach (Control c in Controls)
        {
            if (c is TextBox)
            {
                ((TextBox)c).Text = r[c.ID].ToString();
            }
        }
        //drop down
        //ActiveIndicator.SelectedValue = r["ActiveIndicator"].ToString();
        PulsingPlanPriorityName.Focus();
    }

    public void ClearControls()
    {
        foreach (Control c in Controls)
        {
            if (c is TextBox) { ((TextBox)c).Text = ""; }
            if (c is DropDownList) { ((DropDownList)c).SelectedValue = "0"; }
        }
    }

    public void NewRecord()
    {
        ClearControls();
        PulsingPlanPriorityID.Text = "0";
        PulsingPlanPriorityName.Focus();
    }

    public int CreateRecord()
    {
        int intNewID = BAPSMiscDataAccess.InsertPulsingPlanPriority(PulsingPlanPriorityName.Text);
        ClearControls();
        return intNewID;
    }

    public int UpdateRecord()
    {
        string strResult = BAPSMiscDataAccess.UpdatePulsingPlanPriority(Convert.ToInt32(PulsingPlanPriorityID.Text), PulsingPlanPriorityName.Text);
        ClearControls();
        return 99;
    }

#endregion

#region dropdowns
    

#endregion
}
