﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActivityTypeEdit.ascx.cs" Inherits="User_Controls_ActivityTypeEdit" %>

<!--Activity Type Edit Area-->
<br />    
<div id="parent">
    <div id="resizable1" class="resizable ui-widget-content">
        <br />
        <asp:Label ID="lblHeader" Text="Edit GL Hierarchy" runat="server" CssClass="page_header"></asp:Label><br /><br />
    </div>

    <div id="resizable2" class="ui-widget-content">
        <table border="0" style="width:500px">
            <tr>
                <td>Activity Type Name</td>
                <td>
                    <asp:TextBox ID="ActivityTypeID" runat="server" Visible="False" ForeColor="black"></asp:TextBox>
                    <asp:TextBox ID="ActivityTypeName" runat="server" Width="300px" MaxLength="50" 
                        Enabled="False" ForeColor="black"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ErrorMessage="*Required" ControlToValidate="ActivityTypeName" 
                        Display="Dynamic" ValidationGroup="ActivityType"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>GL Code</td>
                <td>
                    <asp:TextBox ID="GLCode" runat="server" Width="80px" MaxLength="8" 
                        Enabled="False" ForeColor ="Black"></asp:TextBox>
                </td>
                
                        </tr>
            
        </table>
        <table style="width:750px">
            <!--Allocation entry area-->
            <tr><td colspan="2">
                </td></tr>
            <tr><td colspan="2">Allocation To Area and Spend Type</td></tr>
            <tr><td colspan="2">
                <asp:GridView ID="grdSubBrandID" runat="server" width="380px"
                    AutoGenerateColumns="False" GridLines="Vertical" RowStyle-Wrap="false" EmptyDataText="[Not Allocated]" 
                    HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false" SelectedRowStyle-Wrap="false"
                    OnRowDataBound="grdSubBrandID_RowDataBound" DataKeyNames="UniqueKey" ShowFooter="false">
                    <Columns>
                        <asp:BoundField DataField="UniqueKey" ReadOnly="false" Visible="false" />
                        <asp:BoundField DataField="BudgetResponsibilityAreaID" ReadOnly="false" Visible="false" />
                        <asp:BoundField DataField="BudgetResponsibilityAreaName" HeaderText="Area" SortExpression="BudgetResponsibilityAreaName" />
                        <asp:BoundField DataField="SpendTypeID" ReadOnly="false" Visible="false" />
                        <asp:BoundField DataField="SpendTypeName" HeaderText="Spend Type" SortExpression="SpendTypeName" />
                        <asp:BoundField DataField="ActivityTypeGroupID" ReadOnly="false" Visible="false" />
                        <asp:BoundField DataField="ActivityTypeGroupName" HeaderText="Group" SortExpression="ActivityTypeGroup" />
                        <asp:BoundField DataField="ActiveIndicator" ReadOnly="false" Visible="false" />
                        <asp:BoundField DataField="ActiveIndicatorName" HeaderText="Status" SortExpression="ActiveIndicator" />
                    </Columns>
                    <RowStyle CssClass="grid_alternating_row" Wrap="False" />
                    <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
                    <HeaderStyle CssClass="grid_header" Wrap="False" />
                </asp:GridView>
            </td></tr><tr><td colspan="2" style="font-size:2px">&nbsp;</td></tr><tr><td>
                <asp:Button ID="cmdRemoveSubBrand" runat="server" Text="Remove" causesvalidation="false"
                    onclick="cmdRemoveSubBrand_Click" CssClass="form_button" Enabled="false" />
                <asp:Button ID="cmdActive" runat="server" Text="Toggle Status" causesvalidation="false"
                    onclick="cmdActive_Click" CssClass="form_button" Enabled="true" Width="107px" />
            </td></tr>
                
            <tr><td colspan="2">Area&nbsp;
                <asp:DropDownList ID="cboBudgetResponsibilityAreaID" runat="server" Width="140px" Enabled="false" ForeColor="black"
                    DataTextField="EntityName" DataValueField="ID" />
                <asp:RangeValidator ID="RangeValidator1" ControlToValidate="cboBudgetResponsibilityAreaID" MaximumValue="10000000" MinimumValue="1"
                    runat="server" ErrorMessage="Required" Type="Integer" Display="Dynamic" ValidationGroup="SubBrand" />
                Spend Type&nbsp;<asp:DropDownList ID="cboSpendTypeID" runat="server" Width="100px" Enabled="false" ForeColor="black"
                    DataTextField="EntityName" DataValueField="ID" />
                <asp:RangeValidator ID="RangeValidator6" ControlToValidate="cboSpendTypeID" MaximumValue="10000000" MinimumValue="1"
                    runat="server" ErrorMessage="Required" Type="Integer" Display="Dynamic" ValidationGroup="SubBrand" />
                Activity Type Group&nbsp;<asp:DropDownList ID="cboActivityTypeGroupID" runat="server" Width="150px" Enabled="false" ForeColor="black"
                    DataTextField="EntityName" DataValueField="ID" />
                <asp:RangeValidator ID="RangeValidator2" ControlToValidate="cboActivityTypeGroupID" MaximumValue="10000000" MinimumValue="1"
                    runat="server" ErrorMessage="Required" Type="Integer" Display="Dynamic" ValidationGroup="SubBrand" />
                <asp:Button ID="cmdAddSubBrand" runat="server" Text="Add" causesvalidation="true"
                    onclick="cmdAddSubBrand_Click" CssClass="form_button" Enabled="false" Width="60px" ValidationGroup="SubBrand"/>
                <asp:CheckBox ID="CheckNeedsFinanceApprovalFlag" runat="server" Text="Needs Finance Approval"/>
                <br />
            </td></tr>

        </table>
    </div>
</div>


