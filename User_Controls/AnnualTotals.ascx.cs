﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

public partial class User_Controls_AnnualTotals : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public bool AllowEditingBudgets
    {
        set
        {
            AllocatedAPBudget.Enabled = value;
            AllocatedTCCBudget.Enabled = value;
        }
    }

    public void PopulateAnnualValues(DataRow r)
    {
        AllocatedAPBudget.Text = String.Format("{0:0}", r["AllocatedAPBudget"]);
        AllocatedAPBudget.Style.Add("text-align", "right");
        //ChangeToAPBudget.Text = String.Format("{0:0}", r["ChangeToAPBudget"]);
        //ChangeToAPBudget.Style.Add("text-align", "right");
        //CurrentAPWorkingBudget.Text = String.Format("{0:0}", r["CurrentAPWorkingBudget"]);
        //CurrentAPWorkingBudget.Style.Add("text-align", "right");
        APCommitted.Text = String.Format("{0:0}", r["APCommitted"]);
        APCommitted.Style.Add("text-align", "right");
        APUncommitted.Text = String.Format("{0:0}", r["APUncommitted"]);
        APUncommitted.Style.Add("text-align", "right");
        APForecast.Text = String.Format("{0:0}", r["APForecast"]);
        APForecast.Style.Add("text-align", "right");
        APBalance.Text = String.Format("{0:0}", r["APBalance"]);
        APBalance.Style.Add("text-align", "right");
        if (Convert.ToInt32(r["APBalance"]) < 0) { APBalance.BackColor = Color.PapayaWhip; } else { APBalance.BackColor = Color.LightGreen; }
        APFYView.Text = String.Format("{0:0}", r["APFYView"]);
        APFYView.Style.Add("text-align", "right");
        APVariance.Text = String.Format("{0:0}", r["APVariance"]);
        APVariance.Style.Add("text-align", "right");
        if (Convert.ToInt32(r["APVariance"]) < 0) { APVariance.BackColor = Color.PapayaWhip; } else { APVariance.BackColor = Color.LightGreen; }

        AllocatedTCCBudget.Text = String.Format("{0:0}", r["AllocatedTCCBudget"]);
        AllocatedTCCBudget.Style.Add("text-align", "right");
        //ChangeToTCCBudget.Text = String.Format("{0:0}", r["ChangeToTCCBudget"]);
        //ChangeToTCCBudget.Style.Add("text-align", "right");
        //CurrentTCCWorkingBudget.Text = String.Format("{0:0}", r["CurrentTCCWorkingBudget"]);
        //CurrentTCCWorkingBudget.Style.Add("text-align", "right");
        TCCCommitted.Text = String.Format("{0:0}", r["TCCCommitted"]);
        TCCCommitted.Style.Add("text-align", "right");
        TCCUncommitted.Text = String.Format("{0:0}", r["TCCUncommitted"]);
        TCCUncommitted.Style.Add("text-align", "right");
        TCCForecast.Text = String.Format("{0:0}", r["TCCForecast"]);
        TCCForecast.Style.Add("text-align", "right");
        TCCBalance.Text = String.Format("{0:0}", r["TCCBalance"]);
        TCCBalance.Style.Add("text-align", "right");
        if (Convert.ToInt32(r["TCCBalance"]) < 0) { TCCBalance.BackColor = Color.PapayaWhip; } else { TCCBalance.BackColor = Color.LightGreen; }
        TCCFYView.Text = String.Format("{0:0}", r["TCCFYView"]);
        TCCFYView.Style.Add("text-align", "right");
        TCCVariance.Text = String.Format("{0:0}", r["TCCVariance"]);
        TCCVariance.Style.Add("text-align", "right");
        if (Convert.ToInt32(r["TCCVariance"]) < 0) { TCCVariance.BackColor = Color.PapayaWhip; } else { TCCVariance.BackColor = Color.LightGreen; }

        FYView.Text = String.Format("{0:0}", r["FYView"]);
        FYView.Style.Add("text-align", "right");
        Variance.Text = String.Format("{0:0}", r["Variance"]);
        Variance.Style.Add("text-align", "right");
        if (Convert.ToInt32(r["Variance"]) < 0) { Variance.BackColor = Color.PapayaWhip; } else { Variance.BackColor = Color.LightGreen; }

        Forecast.Text = String.Format("{0:0}", r["Forecast"]);
        Forecast.Style.Add("text-align", "right");
        Balance.Text = String.Format("{0:0}", r["Balance"]);
        Balance.Style.Add("text-align", "right");
        if (Convert.ToInt32(r["Balance"]) < 0) { Balance.BackColor = Color.PapayaWhip; } else { Balance.BackColor = Color.LightGreen; }

        AllocatedCpnBudget.Text = String.Format("{0:0}", r["AllocatedCpnBudget"]);
        AllocatedCpnBudget.Style.Add("text-align", "right");
        CpnForecast.Text = String.Format("{0:0}", r["CpnForecast"]);
        CpnForecast.Style.Add("text-align", "right");
        CpnBalance.Text = String.Format("{0:0}", r["CpnBalance"]);
        CpnBalance.Style.Add("text-align", "right");
    }
}
