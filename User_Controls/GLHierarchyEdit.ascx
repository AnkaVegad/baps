﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GLHierarchyEdit.ascx.cs" Inherits="User_Controls_GLHierarchyEdit" %>

    <!--GLHierarchy Edit Area-->
<br />    
<div id="parent">
    <div id="resizable1" class="resizable ui-widget-content">
        <br />
        <asp:Label ID="lblHeader" Text="Edit GL Hierarchy" runat="server" CssClass="page_header"></asp:Label><br /><br />
    </div>
    
    <div id="resizable2" class="ui-widget-content">
    
            <table>
                <tr><td style="vertical-align:top">
                    <table>
                        <tr><td>GL Code</td><td>
                            <asp:Label ID="GLHierarchyID" runat="server" Visible="false" width="40px"/>
                            <asp:TextBox ID="GLCode" runat="server" Width="80px" maxlength="8" />
                            <asp:RangeValidator ID="RangeValidator1" runat="server" 
                                ErrorMessage="*Required" MaximumValue="ZZZZZZZZ" MinimumValue="0" Type="String"
                                ControlToValidate="GLCode" Display="Dynamic" ValidationGroup="GLHierarchy" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*Required" 
                                ControlToValidate="GLCode" Display="Dynamic" ValidationGroup="GLHierarchy" />
                        </td></tr>
                        <tr><td>GL Level 1</td><td>
                            <asp:DropDownList ID="GLLevel1ID" runat="server" Width="200px"
                                DataTextField="EntityName" DataValueField="ID" />
                            <asp:RangeValidator ID="RangeValidator6" runat="server" 
                                ErrorMessage="*Required" MaximumValue="1000000" MinimumValue="1"  Type="Integer"
                                ControlToValidate="GLLevel1ID" Display="Dynamic" ValidationGroup="GLHierarchy" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*Required" 
                                ControlToValidate="GLLevel1ID" Display="Dynamic" ValidationGroup="GLHierarchy" />
                        </td></tr>
                        <tr><td>GL Level 2</td><td>
                            <asp:DropDownList ID="GLLevel2ID" runat="server" Width="200px"
                                DataTextField="EntityName" DataValueField="ID" />
                            <asp:RangeValidator ID="RangeValidator2" runat="server" 
                                ErrorMessage="*Required" MaximumValue="1000000" MinimumValue="1"  Type="Integer"
                                ControlToValidate="GLLevel2ID" Display="Dynamic" ValidationGroup="GLHierarchy" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*Required" 
                                ControlToValidate="GLLevel2ID" Display="Dynamic" ValidationGroup="GLHierarchy" />
                        </td></tr>
                        <tr><td>GL Level 3</td><td>
                            <asp:DropDownList ID="GLLevel3ID" runat="server" Width="200px"
                                DataTextField="EntityName" DataValueField="ID" />
                            <asp:RangeValidator ID="RangeValidator3" runat="server" 
                                ErrorMessage="*Required" MaximumValue="1000000" MinimumValue="1"  Type="Integer"
                                ControlToValidate="GLLevel3ID" Display="Dynamic" ValidationGroup="GLHierarchy" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*Required" 
                                ControlToValidate="GLLevel3ID" Display="Dynamic" ValidationGroup="GLHierarchy" />
                        </td></tr>
                        <tr><td>Status</td><td>
                            <asp:DropDownList ID="ActiveIndicatorName" runat="server" Width="80px"
                                DataTextField="EntityName" DataValueField="ID" />
                            <asp:RangeValidator ID="RangeValidator7" runat="server" 
                                ErrorMessage="*Required" MaximumValue="2" MinimumValue="1"  Type="Integer"
                                ControlToValidate="ActiveIndicatorName" Display="Dynamic" ValidationGroup="GLHierarchy" />
                        </td></tr>
                     </table>
                </td></tr>
            </table>

    </div>
</div>
    