﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Controls_PopupCalendar : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

#region properties

    public DateTime SelectedValue
    {
        get { return calPopup.SelectedDate; }
        set { txtValue.Text = value.ToString("dd/MM/yy"); calPopup.SelectedDate = value; }
    }

#endregion

#region events

    protected void imgCalendar_Click(object sender, ImageClickEventArgs e)
    {
        divCalPopup.Visible = !divCalPopup.Visible;
        //following remmed because causes "not recognised as valid datetime" error
//        calPopup.SelectedDate = Convert.ToDateTime(txtValue.Text);
    }

    protected void calPopup_SelectionChanged(object sender, EventArgs e)
    {
        txtValue.Text = calPopup.SelectedDate.ToString("dd/MM/yy");
        divCalPopup.Visible = false;
    }

#endregion
}
