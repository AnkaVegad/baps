﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Controls_SubBrandEdit : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //populate the drop-downs in edit area
        if (!IsPostBack)
        {
            FillPPHL2();
        }
    }

#region properties

    public SubBrand SelectSubBrand
    {
        get
        {
            SubBrand a = new SubBrand();
            a.SubBrandID = Convert.ToInt32(SubBrandID.Text);
            a.SubBrandName = SubBrandName.Text;
            a.CreatedBy = Session["CurrentUserName"].ToString();
            return a;
        }
    }

    public string SubBrandIDValue
    {
        get { return SubBrandID.Text; }
    }

    public string HeaderText
    {
        set { lblHeader.Text = value; }
    }

#endregion

#region gridevents

    double dblTotal = 0;

    protected void grdSubBrandID_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            //highlight on hover
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            e.Row.Attributes["onclick"] = cs.GetPostBackClientHyperlink(this.grdSubBrandID, "Select$" + e.Row.RowIndex);
        }
    }

#endregion

#region methods

    public void PopulateForm(SubBrand a)
    {
        //populate fields
        SubBrandID.Text = a.SubBrandID.ToString();
        SubBrandName.Text = a.SubBrandName;

        //populate grdSubBrandID
        DataTable tbl = SubBrandDataAccess.FillSubBrandToPPH(Convert.ToInt32(a.SubBrandID));
        Session["SelectedSubBrandItems"] = tbl;
        grdSubBrandID.DataSource = tbl;
        grdSubBrandID.DataBind();

        //enable controls (must be after SubBrandID is populated)
        if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 15)
        {
            ToggleControlState(true);
        }
    }

    public void UpdateSubBrand()
    {
        if (Session["SelectedSubBrandItems"] == null) //check for session expiry
        {
            Response.Redirect("ProjectView.aspx");
        }
        else
        {
            //Main data
            int intRowsAffected = SubBrandDataAccess.UpdateSubBrand(SelectSubBrand);
            //SubBrands - clear and recreate
            SubBrandDataAccess.ClearSubBrandToPPH(Convert.ToInt32(SubBrandID.Text));
            DataTable tbl = (DataTable)Session["SelectedSubBrandItems"];
            foreach (DataRow r in tbl.Rows)
            {
                SubBrandDataAccess.InsertSubBrandToPPH(Convert.ToInt32(SubBrandID.Text), r["PPHL2"].ToString(), Session["CurrentUserName"].ToString());
            }
            //Clear
            ClearForm();
        }
    }

    public string InsertSubBrand()
    {
        if (Session["SelectedSubBrandItems"] == null) //check for session expiry
        {
            Response.Redirect("ProjectView.aspx");
            return "";
        }
        else
        {
            DataTable tbl = (DataTable)Session["SelectedSubBrandItems"];
            //validation check at least one allocation is selected
            if (tbl.Rows.Count == 0)
            {
                return "You must select at least one allocation.";
            }
            else
            {
                //Main data
                int intSubBrandID = SubBrandDataAccess.InsertSubBrand(SelectSubBrand);
                //trap duplicate SubBrand name
                if (intSubBrandID == 0)
                {
                    return "Duplicate Brand-Market name.";
                }
                else
                {
                    //Allocation
                    DataTable tblSubBrand = (DataTable)Session["SelectedSubBrandItems"];
                    foreach (DataRow r in tblSubBrand.Rows)
                    {
                        SubBrandDataAccess.InsertSubBrandToPPH(intSubBrandID, r["PPHL2"].ToString(), Session["CurrentUserName"].ToString());
                    }
                    //Clear
                    ClearForm();
                    return "";
                }
            }
        }
    }

    protected void ToggleControlState(bool state)
    {
        //enable or disable textboxes and dropdowns
        foreach (Control c in Controls)
        {
            if (c is TextBox) { ((TextBox)c).Enabled = state; }
            if (c is DropDownList) { ((DropDownList)c).Enabled = state; }
        }
        //other controls
        grdSubBrandID.Enabled = state;
        cmdAddSubBrand.Enabled = state;
        cmdRemoveSubBrand.Enabled = state;
    }

    protected void ClearControls()
    {
        //remove data from controls
        SubBrandID.Text = "";
        SubBrandName.Text = "";
        //Clear Allocation
        grdSubBrandID.DataSource = "";
        grdSubBrandID.DataBind();
        Session["SelectedSubBrandItems"] = null;
        cboPPHL2.SelectedValue = "0";
    }

    public void ClearForm()
    {
        ClearControls();
        ToggleControlState(false);
    }

    public void NewRecord()
    {
        ClearControls();
         //initialise allocations table
        DataTable tbl = new DataTable();
        tbl.Columns.Add("PPHL2");
        tbl.Columns.Add("VTEXT");
        Session["SelectedSubBrandItems"] = tbl;
 
        SubBrandID.Text = "0";
        SubBrandName.Focus();
        //enable controls (must be after SubBrandID is populated)
        ToggleControlState(true);
    }

    protected void AddSelectedSubBrand(string strPPHL2)
    {
        DataTable tbl = (DataTable)Session["SelectedSubBrandItems"];

        //loop through tbl to see if record with this PPHL2 has already been added (to prevent duplication)
        bool duplicate = false;
        for (int i = tbl.Rows.Count - 1; i >= 0; i--)
        {
            DataRow dr = tbl.Rows[i];
            string dtitem1 = dr["PPHL2"].ToString();
            if (dtitem1 == strPPHL2)
            {
                duplicate = true;
            }
        }

        if (!duplicate)
        {
            //add the selected item to the table
            AddRow(tbl, strPPHL2); 
        }

        grdSubBrandID.DataSource = tbl;
        grdSubBrandID.DataBind();
        //store basket table in session variable
        Session["SelectedSubBrandItems"] = tbl;
        //clear fields
        cboPPHL2.SelectedValue = "0";
    }

    protected void RemoveSubBrand()
    //remove the selected line item from the temporary table and grid
    {
        try //because an item may not be selected in the grid or the Session variable may not be valid
        {
            //loop through the table and delete if criteria found
            DataTable tbl = (DataTable)Session["SelectedSubBrandItems"];

            //mark record for deletion
            for (int i = tbl.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = tbl.Rows[i];
                string dtitem = dr["PPHL2"].ToString();
                if (dtitem == grdSubBrandID.SelectedValue.ToString())
                {
                    tbl.Rows[i].Delete();
                }
            }

            //delete
            tbl.AcceptChanges();

            grdSubBrandID.DataSource = tbl;
            grdSubBrandID.DataBind();
            //store basket table in session variable
            Session["SelectedSubBrandItems"] = tbl;
        }
        catch { }
    }

    protected void AddRow(DataTable tbl, string strPPHL2)
    {                    
        //add the selected item to the table
        DataRow dr = tbl.NewRow();
        dr["PPHL2"] = strPPHL2;
        dr["VTEXT"] = Common.ADOLookup("VTEXT", "tblPPH", "PRODH = '" + strPPHL2.ToString() + "'");
        tbl.Rows.Add(dr);
    }

#endregion

#region filldropdowns

    protected void FillPPHL2()
    //Fill dropdownlist of PPHL2 
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillPPHL2";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        //cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboPPHL2;
        d.DataSource = tbl;
        d.DataBind();
    }

#endregion

#region subformevents

    protected void cmdAddSubBrand_Click(object sender, EventArgs e)
    {
        AddSelectedSubBrand(cboPPHL2.SelectedValue);
    }

    protected void cmdRemoveSubBrand_Click(object sender, EventArgs e)
    {
        RemoveSubBrand();
    }

#endregion

}
