﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Controls_Worklist : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        String UID = this.UniqueID.Replace("$", "_") + "_";
        project_grid.Attributes.Add("OnScroll", "savescroll('" + UID + "'); return false;");

        Page page = HttpContext.Current.CurrentHandler as Page;
        page.ClientScript.RegisterStartupScript(typeof(Page), "setworklist", "<script type='text/javascript'>OnLoad();</script>");

        if (!IsPostBack)
        {
            if (Session["Worklist1"] == null)
                HiddenGridSelection1.Value = "0";
            else
                HiddenGridSelection1.Value = Session["Worklist1"].ToString();

            if (Session["Worklist2"] == null)
                HiddenGridSelection2.Value = "0";
            else
                HiddenGridSelection2.Value = Session["Worklist2"].ToString();
            
            if (Session["Worklist3"] == null)
                HiddenGridSelection3.Value = "0";
            else
                HiddenGridSelection3.Value = Session["Worklist3"].ToString();
            
            if (Session["Worklist4"] == null)
                HiddenGridSelection4.Value = "0";
            else
                HiddenGridSelection4.Value = Session["Worklist4"].ToString();
            
            if (Session["Worklist5"] == null)
                HiddenGridSelection5.Value = "0";
            else
                HiddenGridSelection5.Value = Session["Worklist5"].ToString();
            
            if (Session["Worklist6"] == null)
                HiddenGridSelection6.Value = "0";
            else
                HiddenGridSelection6.Value = Session["Worklist6"].ToString();
            
            if (Session["Worklist7"] == null)
                HiddenGridSelection7.Value = "0";
            else
                HiddenGridSelection7.Value = Session["Worklist7"].ToString();
            
            if (Session["Worklist8"] == null)
                HiddenGridSelection8.Value = "0";
            else
                HiddenGridSelection8.Value = Session["Worklist8"].ToString();
            
            if (Session["Worklist9"] == null)
                HiddenGridSelection9.Value = "0";
            else
                HiddenGridSelection9.Value = Session["Worklist9"].ToString();
            
            if (Session["Worklist10"] == null)
                HiddenGridSelection10.Value = "0";
            else
                HiddenGridSelection10.Value = Session["Worklist10"].ToString();
            
            if (Session["Worklist11"] == null)
                HiddenGridSelection11.Value = "0";

            else
                HiddenGridSelection11.Value = Session["Worklist11"].ToString();
            
            if (Session["Worklist12"] == null)
                HiddenGridSelection12.Value = "0";

            else
                HiddenGridSelection12.Value = Session["Worklist12"].ToString();

            //Populate dropdown and select a worklist 
            if (HiddenCurrentWorklist.Value == "")
            {
                if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 15)
                {
                    //Administrator defaults to Awaiting PR
                    HiddenCurrentWorklist.Value = "5";
                }
                else if (Convert.ToInt16(Session["CurrentUserLevelID"]) == 35)
                {
                    //Rebate Creator defaults to Create Rebate
                    HiddenCurrentWorklist.Value = "8";
                }
                else
                {
                    //User defaults to Request Receipt
                    HiddenCurrentWorklist.Value = "2";
                }
            }
            //populate grid
            PopulateGrid();
            UserAccess();
            Refresh();

            //show/hide relevant grid and buttons
            switch (HiddenCurrentWorklist.Value)
            {
                case "1":
                    //lblHeading.Text = "Awaiting PO Number Worklist";
                    vw1.Style.Add("display", "inline");
                    vw2.Style.Add("display", "none");
                    vw3.Style.Add("display", "none");
                    vw4.Style.Add("display", "none");
                    vw5.Style.Add("display", "none");
                    vw6.Style.Add("display", "none");
                    vw7.Style.Add("display", "none");
                    vw8.Style.Add("display", "none");
                    vw9.Style.Add("display", "none");
                    vw10.Style.Add("display", "none");
                    vw11.Style.Add("display", "none");
                    vw12.Style.Add("display", "none");

                    FooterWorklist1.Style.Add("display", "inline");
                    FooterWorklist2.Style.Add("display", "none");
                    FooterWorklist3.Style.Add("display", "none");
                    FooterWorklist4.Style.Add("display", "none");
                    FooterWorklist5.Style.Add("display", "none");
                    FooterWorklist6.Style.Add("display", "none");
                    FooterWorklist7.Style.Add("display", "none");
                    FooterWorklist8.Style.Add("display", "none");
                    FooterWorklist9.Style.Add("display", "none");
                    FooterWorklist10.Style.Add("display", "none");
                    FooterWorklist11.Style.Add("display", "none");
                    FooterWorklist12.Style.Add("display", "none");

                    lbtnActionReceipt.Font.Bold = false;
                    lbtnApproveReallocation.Font.Bold = false;
                    lbtnApproveRebate.Font.Bold = false;
                    lbtnAwaitingPONo.Font.Bold = true;
                    lbtnChangeToPo.Font.Bold = false;
                    lbtnChangeToRebate.Font.Bold = false;
                    lbtnCreatePR.Font.Bold = false;
                    lbtnCreateRebate.Font.Bold = false;
                    lbtnReallocationAwaitApproval.Font.Bold = false;
                    lbtnRequestPR_Rbte.Font.Bold = false;
                    lbtnRequestReceipt.Font.Bold = false;
                    lbtnFinanceApproval.Font.Bold = false;
                    break;
                case "2":
                    //lblHeading.Text = "Request Receipt Worklist";
                    vw1.Style.Add("display", "none");
                    vw2.Style.Add("display", "inline");
                    vw3.Style.Add("display", "none");
                    vw4.Style.Add("display", "none");
                    vw5.Style.Add("display", "none");
                    vw6.Style.Add("display", "none");
                    vw7.Style.Add("display", "none");
                    vw8.Style.Add("display", "none");
                    vw9.Style.Add("display", "none");
                    vw10.Style.Add("display", "none");
                    vw11.Style.Add("display", "none");
                    vw12.Style.Add("display", "none");

                    FooterWorklist1.Style.Add("display", "none");
                    FooterWorklist2.Style.Add("display", "inline");
                    FooterWorklist3.Style.Add("display", "none");
                    FooterWorklist4.Style.Add("display", "none");
                    FooterWorklist5.Style.Add("display", "none");
                    FooterWorklist6.Style.Add("display", "none");
                    FooterWorklist7.Style.Add("display", "none");
                    FooterWorklist8.Style.Add("display", "none");
                    FooterWorklist9.Style.Add("display", "none");
                    FooterWorklist10.Style.Add("display", "none");
                    FooterWorklist11.Style.Add("display", "none");
                    FooterWorklist12.Style.Add("display", "none");

                    lbtnActionReceipt.Font.Bold = false;
                    lbtnApproveReallocation.Font.Bold = false;
                    lbtnApproveRebate.Font.Bold = false;
                    lbtnAwaitingPONo.Font.Bold = false;
                    lbtnChangeToPo.Font.Bold = false;
                    lbtnChangeToRebate.Font.Bold = false;
                    lbtnCreatePR.Font.Bold = false;
                    lbtnCreateRebate.Font.Bold = false;
                    lbtnReallocationAwaitApproval.Font.Bold = false;
                    lbtnRequestPR_Rbte.Font.Bold = false;
                    lbtnRequestReceipt.Font.Bold = true;
                    lbtnFinanceApproval.Font.Bold = false;
                    break;
                case "3":
                    //lblHeading.Text = "Change To PO Number Worklist";
                    vw1.Style.Add("display", "none");
                    vw2.Style.Add("display", "none");
                    vw3.Style.Add("display", "inline");
                    vw4.Style.Add("display", "none");
                    vw5.Style.Add("display", "none");
                    vw6.Style.Add("display", "none");
                    vw7.Style.Add("display", "none");
                    vw8.Style.Add("display", "none");
                    vw9.Style.Add("display", "none");
                    vw10.Style.Add("display", "none");
                    vw11.Style.Add("display", "none");
                    vw12.Style.Add("display", "none");


                    FooterWorklist1.Style.Add("display", "none");
                    FooterWorklist2.Style.Add("display", "none");
                    FooterWorklist3.Style.Add("display", "inline");
                    FooterWorklist4.Style.Add("display", "none");
                    FooterWorklist5.Style.Add("display", "none");
                    FooterWorklist6.Style.Add("display", "none");
                    FooterWorklist7.Style.Add("display", "none");
                    FooterWorklist8.Style.Add("display", "none");
                    FooterWorklist9.Style.Add("display", "none");
                    FooterWorklist10.Style.Add("display", "none");
                    FooterWorklist11.Style.Add("display", "none");
                    FooterWorklist12.Style.Add("display", "none");

                    lbtnActionReceipt.Font.Bold = false;
                    lbtnApproveReallocation.Font.Bold = false;
                    lbtnApproveRebate.Font.Bold = false;
                    lbtnAwaitingPONo.Font.Bold = false;
                    lbtnChangeToPo.Font.Bold = true;
                    lbtnChangeToRebate.Font.Bold = false;
                    lbtnCreatePR.Font.Bold = false;
                    lbtnCreateRebate.Font.Bold = false;
                    lbtnReallocationAwaitApproval.Font.Bold = false;
                    lbtnRequestPR_Rbte.Font.Bold = false;
                    lbtnRequestReceipt.Font.Bold = false;
                    lbtnFinanceApproval.Font.Bold = false;

                    break;
                case "4":
                    //lblHeading.Text = "Action Receipt Worklist";
                    vw1.Style.Add("display", "none");
                    vw2.Style.Add("display", "none");
                    vw3.Style.Add("display", "none");
                    vw4.Style.Add("display", "inline");
                    vw5.Style.Add("display", "none");
                    vw6.Style.Add("display", "none");
                    vw7.Style.Add("display", "none");
                    vw8.Style.Add("display", "none");
                    vw9.Style.Add("display", "none");
                    vw10.Style.Add("display", "none");
                    vw11.Style.Add("display", "none");
                    vw12.Style.Add("display", "none");


                    FooterWorklist1.Style.Add("display", "none");
                    FooterWorklist2.Style.Add("display", "none");
                    FooterWorklist3.Style.Add("display", "none");
                    FooterWorklist4.Style.Add("display", "inline");
                    FooterWorklist5.Style.Add("display", "none");
                    FooterWorklist6.Style.Add("display", "none");
                    FooterWorklist7.Style.Add("display", "none");
                    FooterWorklist8.Style.Add("display", "none");
                    FooterWorklist9.Style.Add("display", "none");
                    FooterWorklist10.Style.Add("display", "none");
                    FooterWorklist11.Style.Add("display", "none");
                    FooterWorklist12.Style.Add("display", "none");

                    lbtnActionReceipt.Font.Bold = true;
                    lbtnApproveReallocation.Font.Bold = false;
                    lbtnApproveRebate.Font.Bold = false;
                    lbtnAwaitingPONo.Font.Bold = false;
                    lbtnChangeToPo.Font.Bold = false;
                    lbtnChangeToRebate.Font.Bold = false;
                    lbtnCreatePR.Font.Bold = false;
                    lbtnCreateRebate.Font.Bold = false;
                    lbtnReallocationAwaitApproval.Font.Bold = false;
                    lbtnRequestPR_Rbte.Font.Bold = false;
                    lbtnRequestReceipt.Font.Bold = false;
                    lbtnFinanceApproval.Font.Bold = false;
                    break;
                case "5":
                    //lblHeading.Text = "Create PR Worklist";
                    vw1.Style.Add("display", "none");
                    vw2.Style.Add("display", "none");
                    vw3.Style.Add("display", "none");
                    vw4.Style.Add("display", "none");
                    vw5.Style.Add("display", "inline");
                    vw6.Style.Add("display", "none");
                    vw7.Style.Add("display", "none");
                    vw8.Style.Add("display", "none");
                    vw9.Style.Add("display", "none");
                    vw10.Style.Add("display", "none");
                    vw11.Style.Add("display", "none");
                    vw12.Style.Add("display", "none");


                    FooterWorklist1.Style.Add("display", "none");
                    FooterWorklist2.Style.Add("display", "none");
                    FooterWorklist3.Style.Add("display", "none");
                    FooterWorklist4.Style.Add("display", "none");
                    FooterWorklist5.Style.Add("display", "inline");
                    FooterWorklist6.Style.Add("display", "none");
                    FooterWorklist7.Style.Add("display", "none");
                    FooterWorklist8.Style.Add("display", "none");
                    FooterWorklist9.Style.Add("display", "none");
                    FooterWorklist10.Style.Add("display", "none");
                    FooterWorklist11.Style.Add("display", "none");
                    FooterWorklist12.Style.Add("display", "none");


                    lbtnActionReceipt.Font.Bold = false;
                    lbtnApproveReallocation.Font.Bold = false;
                    lbtnApproveRebate.Font.Bold = false;
                    lbtnAwaitingPONo.Font.Bold = false;
                    lbtnChangeToPo.Font.Bold = false;
                    lbtnChangeToRebate.Font.Bold = false;
                    lbtnCreatePR.Font.Bold = true;
                    lbtnCreateRebate.Font.Bold = false;
                    lbtnReallocationAwaitApproval.Font.Bold = false;
                    lbtnRequestPR_Rbte.Font.Bold = false;
                    lbtnRequestReceipt.Font.Bold = false;
                    lbtnFinanceApproval.Font.Bold = false;

                    break;
                case "6":
                    //lblHeading.Text = "Change To Rebate Worklist";
                    vw1.Style.Add("display", "none");
                    vw2.Style.Add("display", "none");
                    vw3.Style.Add("display", "none");
                    vw4.Style.Add("display", "none");
                    vw5.Style.Add("display", "none");
                    vw6.Style.Add("display", "inline");
                    vw7.Style.Add("display", "none");
                    vw8.Style.Add("display", "none");
                    vw9.Style.Add("display", "none");
                    vw10.Style.Add("display", "none");
                    vw11.Style.Add("display", "none");
                    vw12.Style.Add("display", "none");


                    FooterWorklist1.Style.Add("display", "none");
                    FooterWorklist2.Style.Add("display", "none");
                    FooterWorklist3.Style.Add("display", "none");
                    FooterWorklist4.Style.Add("display", "none");
                    FooterWorklist5.Style.Add("display", "none");
                    FooterWorklist6.Style.Add("display", "inline");
                    FooterWorklist7.Style.Add("display", "none");
                    FooterWorklist8.Style.Add("display", "none");
                    FooterWorklist9.Style.Add("display", "none");
                    FooterWorklist10.Style.Add("display", "none");
                    FooterWorklist11.Style.Add("display", "none");
                    FooterWorklist12.Style.Add("display", "none");


                    lbtnActionReceipt.Font.Bold = false;
                    lbtnApproveReallocation.Font.Bold = false;
                    lbtnApproveRebate.Font.Bold = false;
                    lbtnAwaitingPONo.Font.Bold = false;
                    lbtnChangeToPo.Font.Bold = false;
                    lbtnChangeToRebate.Font.Bold = true;
                    lbtnCreatePR.Font.Bold = false;
                    lbtnCreateRebate.Font.Bold = false;
                    lbtnReallocationAwaitApproval.Font.Bold = false;
                    lbtnRequestPR_Rbte.Font.Bold = false;
                    lbtnRequestReceipt.Font.Bold = false;
                    lbtnFinanceApproval.Font.Bold = false;
                    break;
                case "7":
                    //lblHeading.Text = "Approve Rebate Request Worklist";
                    vw1.Style.Add("display", "none");
                    vw2.Style.Add("display", "none");
                    vw3.Style.Add("display", "none");
                    vw4.Style.Add("display", "none");
                    vw5.Style.Add("display", "none");
                    vw6.Style.Add("display", "none");
                    vw7.Style.Add("display", "inline");
                    vw8.Style.Add("display", "none");
                    vw9.Style.Add("display", "none");
                    vw10.Style.Add("display", "none");
                    vw11.Style.Add("display", "none");
                    vw12.Style.Add("display", "none");


                    FooterWorklist1.Style.Add("display", "none");
                    FooterWorklist2.Style.Add("display", "none");
                    FooterWorklist3.Style.Add("display", "none");
                    FooterWorklist4.Style.Add("display", "none");
                    FooterWorklist5.Style.Add("display", "none");
                    FooterWorklist6.Style.Add("display", "none");
                    FooterWorklist7.Style.Add("display", "inline");
                    FooterWorklist8.Style.Add("display", "none");
                    FooterWorklist9.Style.Add("display", "none");
                    FooterWorklist10.Style.Add("display", "none");
                    FooterWorklist11.Style.Add("display", "none");
                    FooterWorklist12.Style.Add("display", "none");


                    lbtnActionReceipt.Font.Bold = false;
                    lbtnApproveReallocation.Font.Bold = false;
                    lbtnApproveRebate.Font.Bold = true;
                    lbtnAwaitingPONo.Font.Bold = false;
                    lbtnChangeToPo.Font.Bold = false;
                    lbtnChangeToRebate.Font.Bold = false;
                    lbtnCreatePR.Font.Bold = false;
                    lbtnCreateRebate.Font.Bold = false;
                    lbtnReallocationAwaitApproval.Font.Bold = false;
                    lbtnRequestPR_Rbte.Font.Bold = false;
                    lbtnRequestReceipt.Font.Bold = false;
                    lbtnFinanceApproval.Font.Bold = false;
                    break;
                case "8":
                    //lblHeading.Text = "Create Rebate Worklist";
                    vw1.Style.Add("display", "none");
                    vw2.Style.Add("display", "none");
                    vw3.Style.Add("display", "none");
                    vw4.Style.Add("display", "none");
                    vw5.Style.Add("display", "none");
                    vw6.Style.Add("display", "none");
                    vw7.Style.Add("display", "none");
                    vw8.Style.Add("display", "inline");
                    vw9.Style.Add("display", "none");
                    vw10.Style.Add("display", "none");
                    vw11.Style.Add("display", "none");
                    vw12.Style.Add("display", "none");


                    FooterWorklist1.Style.Add("display", "none");
                    FooterWorklist2.Style.Add("display", "none");
                    FooterWorklist3.Style.Add("display", "none");
                    FooterWorklist4.Style.Add("display", "none");
                    FooterWorklist5.Style.Add("display", "none");
                    FooterWorklist6.Style.Add("display", "none");
                    FooterWorklist7.Style.Add("display", "none");
                    FooterWorklist8.Style.Add("display", "inline");
                    FooterWorklist9.Style.Add("display", "none");
                    FooterWorklist10.Style.Add("display", "none");
                    FooterWorklist11.Style.Add("display", "none");
                    FooterWorklist12.Style.Add("display", "none");

                    lbtnActionReceipt.Font.Bold = false;
                    lbtnApproveReallocation.Font.Bold = false;
                    lbtnApproveRebate.Font.Bold = false;
                    lbtnAwaitingPONo.Font.Bold = false;
                    lbtnChangeToPo.Font.Bold = false;
                    lbtnChangeToRebate.Font.Bold = false;
                    lbtnCreatePR.Font.Bold = false;
                    lbtnCreateRebate.Font.Bold = true;
                    lbtnReallocationAwaitApproval.Font.Bold = false;
                    lbtnRequestPR_Rbte.Font.Bold = false;
                    lbtnRequestReceipt.Font.Bold = false;
                    lbtnFinanceApproval.Font.Bold = false;


                    break;
                case "9":
                    //lblHeading.Text = "Approve Budget Reallocation Worklist";
                    vw1.Style.Add("display", "none");
                    vw2.Style.Add("display", "none");
                    vw3.Style.Add("display", "none");
                    vw4.Style.Add("display", "none");
                    vw5.Style.Add("display", "none");
                    vw6.Style.Add("display", "none");
                    vw7.Style.Add("display", "none");
                    vw8.Style.Add("display", "none");
                    vw9.Style.Add("display", "inline");
                    vw10.Style.Add("display", "none");
                    vw11.Style.Add("display", "none");
                    vw12.Style.Add("display", "none");


                    FooterWorklist1.Style.Add("display", "none");
                    FooterWorklist2.Style.Add("display", "none");
                    FooterWorklist3.Style.Add("display", "none");
                    FooterWorklist4.Style.Add("display", "none");
                    FooterWorklist5.Style.Add("display", "none");
                    FooterWorklist6.Style.Add("display", "none");
                    FooterWorklist7.Style.Add("display", "none");
                    FooterWorklist8.Style.Add("display", "none");
                    FooterWorklist9.Style.Add("display", "inline");
                    FooterWorklist10.Style.Add("display", "none");
                    FooterWorklist11.Style.Add("display", "none");
                    FooterWorklist12.Style.Add("display", "none");


                    lbtnActionReceipt.Font.Bold = false;
                    lbtnApproveReallocation.Font.Bold = true;
                    lbtnApproveRebate.Font.Bold = false;
                    lbtnAwaitingPONo.Font.Bold = false;
                    lbtnChangeToPo.Font.Bold = false;
                    lbtnChangeToRebate.Font.Bold = false;
                    lbtnCreatePR.Font.Bold = false;
                    lbtnCreateRebate.Font.Bold = false;
                    lbtnReallocationAwaitApproval.Font.Bold = false;
                    lbtnRequestPR_Rbte.Font.Bold = false;
                    lbtnRequestReceipt.Font.Bold = false;
                    break;
                case "10":
                    //lblHeading.Text = "Open Budget Reallocation Worklist";
                    vw1.Style.Add("display", "none");
                    vw2.Style.Add("display", "none");
                    vw3.Style.Add("display", "none");
                    vw4.Style.Add("display", "none");
                    vw5.Style.Add("display", "none");
                    vw6.Style.Add("display", "none");
                    vw7.Style.Add("display", "none");
                    vw8.Style.Add("display", "none");
                    vw9.Style.Add("display", "none");
                    vw10.Style.Add("display", "inline");
                    vw11.Style.Add("display", "none");
                    vw12.Style.Add("display", "none");

                    FooterWorklist1.Style.Add("display", "none");
                    FooterWorklist2.Style.Add("display", "none");
                    FooterWorklist3.Style.Add("display", "none");
                    FooterWorklist4.Style.Add("display", "none");
                    FooterWorklist5.Style.Add("display", "none");
                    FooterWorklist6.Style.Add("display", "none");
                    FooterWorklist7.Style.Add("display", "none");
                    FooterWorklist8.Style.Add("display", "none");
                    FooterWorklist9.Style.Add("display", "none");
                    FooterWorklist10.Style.Add("display", "inline");
                    FooterWorklist11.Style.Add("display", "none");
                    FooterWorklist12.Style.Add("display", "none");


                    lbtnActionReceipt.Font.Bold = false;
                    lbtnApproveReallocation.Font.Bold = false;
                    lbtnApproveRebate.Font.Bold = false;
                    lbtnAwaitingPONo.Font.Bold = false;
                    lbtnChangeToPo.Font.Bold = false;
                    lbtnChangeToRebate.Font.Bold = false;
                    lbtnCreatePR.Font.Bold = false;
                    lbtnCreateRebate.Font.Bold = false;
                    lbtnReallocationAwaitApproval.Font.Bold = true;
                    lbtnRequestPR_Rbte.Font.Bold = false;
                    lbtnRequestReceipt.Font.Bold = false;
                    lbtnFinanceApproval.Font.Bold = false;
                    break;
                case "11":
                    vw1.Style.Add("display", "none");
                    vw2.Style.Add("display", "none");
                    vw3.Style.Add("display", "none");
                    vw4.Style.Add("display", "none");
                    vw5.Style.Add("display", "none");
                    vw6.Style.Add("display", "none");
                    vw7.Style.Add("display", "none");
                    vw8.Style.Add("display", "none");
                    vw9.Style.Add("display", "none");
                    vw10.Style.Add("display", "none");
                    vw11.Style.Add("display", "inline");
                    vw12.Style.Add("display", "none");


                    FooterWorklist1.Style.Add("display", "none");
                    FooterWorklist2.Style.Add("display", "none");
                    FooterWorklist3.Style.Add("display", "none");
                    FooterWorklist4.Style.Add("display", "none");
                    FooterWorklist5.Style.Add("display", "none");
                    FooterWorklist6.Style.Add("display", "none");
                    FooterWorklist7.Style.Add("display", "none");
                    FooterWorklist8.Style.Add("display", "none");
                    FooterWorklist9.Style.Add("display", "none");
                    FooterWorklist10.Style.Add("display", "none");
                    FooterWorklist11.Style.Add("display", "inline");
                    FooterWorklist12.Style.Add("display", "none");


                    lbtnActionReceipt.Font.Bold = false;
                    lbtnApproveReallocation.Font.Bold = false;
                    lbtnApproveRebate.Font.Bold = false;
                    lbtnAwaitingPONo.Font.Bold = false;
                    lbtnChangeToPo.Font.Bold = false;
                    lbtnChangeToRebate.Font.Bold = false;
                    lbtnCreatePR.Font.Bold = false;
                    lbtnCreateRebate.Font.Bold = false;
                    lbtnReallocationAwaitApproval.Font.Bold = false;
                    lbtnRequestPR_Rbte.Font.Bold = true;
                    lbtnRequestReceipt.Font.Bold = false;
                    lbtnFinanceApproval.Font.Bold = false;
                    break;

                case "12":
                    vw1.Style.Add("display", "none");
                    vw2.Style.Add("display", "none");
                    vw3.Style.Add("display", "none");
                    vw4.Style.Add("display", "none");
                    vw5.Style.Add("display", "none");
                    vw6.Style.Add("display", "none");
                    vw7.Style.Add("display", "none");
                    vw8.Style.Add("display", "none");
                    vw9.Style.Add("display", "none");
                    vw10.Style.Add("display", "none");
                    vw11.Style.Add("display", "none");
                    vw12.Style.Add("display", "inline");


                    FooterWorklist1.Style.Add("display", "none");
                    FooterWorklist2.Style.Add("display", "none");
                    FooterWorklist3.Style.Add("display", "none");
                    FooterWorklist4.Style.Add("display", "none");
                    FooterWorklist5.Style.Add("display", "none");
                    FooterWorklist6.Style.Add("display", "none");
                    FooterWorklist7.Style.Add("display", "none");
                    FooterWorklist8.Style.Add("display", "none");
                    FooterWorklist9.Style.Add("display", "none");
                    FooterWorklist10.Style.Add("display", "none");
                    FooterWorklist11.Style.Add("display", "none");
                    FooterWorklist12.Style.Add("display", "inline");


                    lbtnActionReceipt.Font.Bold = false;
                    lbtnApproveReallocation.Font.Bold = false;
                    lbtnApproveRebate.Font.Bold = false;
                    lbtnAwaitingPONo.Font.Bold = false;
                    lbtnChangeToPo.Font.Bold = false;
                    lbtnChangeToRebate.Font.Bold = false;
                    lbtnCreatePR.Font.Bold = false;
                    lbtnCreateRebate.Font.Bold = false;
                    lbtnReallocationAwaitApproval.Font.Bold = false;
                    lbtnRequestPR_Rbte.Font.Bold = false;
                    lbtnRequestReceipt.Font.Bold = false;
                    lbtnFinanceApproval.Font.Bold = true;
                    
                    break;
            }
        }
        SetScroll();
     
    }

    #region gridevents

    protected void grdWorklist1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //highlight on hover
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            //e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(this.grdWorklist1, "Select$" + e.Row.RowIndex);

            if (e.Row.RowIndex == Convert.ToInt32(HiddenGridSelection1.Value))
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].CssClass = "grid_selected";
                }
            }
        }
    }

    protected void grdWorklist2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //highlight on hover
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            //e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(this.grdWorklist2, "Select$" + e.Row.RowIndex);

            if (e.Row.RowIndex == Convert.ToInt32(HiddenGridSelection2.Value))
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].CssClass = "grid_selected";
                }
            }
        }
    }

    protected void grdWorklist3_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //highlight on hover
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            //e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(this.grdWorklist3, "Select$" + e.Row.RowIndex);

            if (e.Row.RowIndex == Convert.ToInt32(HiddenGridSelection3.Value))
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].CssClass = "grid_selected";
                }
            }
        }
    }

    protected void grdWorklist4_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //highlight on hover
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            //e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(this.grdWorklist4, "Select$" + e.Row.RowIndex);

            if (e.Row.RowIndex == Convert.ToInt32(HiddenGridSelection4.Value))
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].CssClass = "grid_selected";
                }
            }
        }
    }

    protected void grdWorklist5_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //highlight on hover
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            //e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(this.grdWorklist5, "Select$" + e.Row.RowIndex);

            if (e.Row.RowIndex == Convert.ToInt32(HiddenGridSelection5.Value))
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].CssClass = "grid_selected";
                }
            }

        }
    }

    protected void grdWorklist6_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //highlight on hover
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            //e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(this.grdWorklist6, "Select$" + e.Row.RowIndex);

            if (e.Row.RowIndex == Convert.ToInt32(HiddenGridSelection6.Value))
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].CssClass = "grid_selected";
                }
            }
        }
    }

    protected void grdWorklist7_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //highlight on hover
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            //e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(this.grdWorklist7, "Select$" + e.Row.RowIndex);

            if (e.Row.RowIndex == Convert.ToInt32(HiddenGridSelection7.Value))
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].CssClass = "grid_selected";
                }
            }
        }
    }

    protected void grdWorklist8_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //highlight on hover
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            //e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(this.grdWorklist8, "Select$" + e.Row.RowIndex);

            if (e.Row.RowIndex == Convert.ToInt32(HiddenGridSelection8.Value))
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].CssClass = "grid_selected";
                }
            }
        }
    }

    protected void grdWorklist9_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //highlight on hover
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            //e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(this.grdWorklist9, "Select$" + e.Row.RowIndex);

            if (e.Row.RowIndex == Convert.ToInt32(HiddenGridSelection9.Value))
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].CssClass = "grid_selected";
                }
            }
        }
    }

    protected void grdWorklist10_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //highlight on hover
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            //e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(this.grdWorklist10, "Select$" + e.Row.RowIndex);

            if (e.Row.RowIndex == Convert.ToInt32(HiddenGridSelection10.Value))
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].CssClass = "grid_selected";
                }
            }
        }
    }

    protected void grdWorklist11_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //highlight on hover
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            //e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(this.grdWorklist11, "Select$" + e.Row.RowIndex);

            if (e.Row.RowIndex == Convert.ToInt32(HiddenGridSelection11.Value))
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].CssClass = "grid_selected";
                }
            }
        }
    }

    protected void grdWorklist12_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //highlight on hover
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            //e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(this.grdWorklist12, "Select$" + e.Row.RowIndex);

            if (e.Row.RowIndex == Convert.ToInt32(HiddenGridSelection10.Value))
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].CssClass = "grid_selected";
                }
            }
        }
    }


    #endregion

    #region events

    protected void cmdSelect_Click(object sender, EventArgs e)
    //switch to PO Header/Rebate Header view
    {
        RememberGridSelections();
        Session["ReturnToScreen"] = "Worklist";
       
        
        try
        {
            switch (HiddenCurrentWorklist.Value)
            {
                case "1":
                    grdWorklist01.SelectedIndex = Int32.Parse(HiddenGridSelection1.Value);
                    if (grdWorklist01.Rows.Count != 0 && grdWorklist01.SelectedValue != null)
                    {
                        grdWorklist01.SelectedIndex = -1; 
                        RedirectToPOHeader(Convert.ToInt32(grdWorklist01.SelectedValue));
                        SqlCommand cmd = DataAccess.CreateCommand();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "SelectWorklistDetailsByActivity";
                        string ActivityID = DataAccess.ADOLookup("ActivityID", "tblActivity", "POID = " + grdWorklist01.SelectedValue);
                        cmd.Parameters.AddWithValue("@ActivityID", Convert.ToInt32(ActivityID));
                        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
                        if (tbl.Rows.Count > 0)
                        {
                            string SpendTypeName = tbl.Rows[0]["SpendTypeName"].ToString();
                            Session["SelectedProjectToCustomerID"] = tbl.Rows[0]["ProjectToCustomerID"].ToString();
                            Session["SelectedBudgetResponsibilityAreaID"] = tbl.Rows[0]["BudgetResponsibilityAreaID"].ToString();
                            Response.Redirect("POHeader.aspx?Mode=ExistingPOID");
                        }
                    }
                   
                    break;
                case "3":
                    grdWorklist3.SelectedIndex = Int32.Parse(HiddenGridSelection3.Value);
                    if (grdWorklist3.Rows.Count != 0 && grdWorklist3.SelectedValue != null) {
                        //Response.Write("Grid selection" + grdWorklist3.SelectedValue);
                        int intPOID = Convert.ToInt32(Common.ADOLookup("POID", "tblPOChange", "POChangeID = " + grdWorklist3.SelectedValue));
                        //Response.Write("POID Selected" + intPOID.ToString());
                       grdWorklist3.SelectedIndex = -1; 
                        RedirectToPOHeader(intPOID);
                        SqlCommand cmd = DataAccess.CreateCommand();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "SelectWorklistDetailsByActivity";
                        //There is no activity ID - Error
                        Response.Write("PTC Selected" + Session["SelectedProjectToCustomerID"].ToString());
                        string ActivityID = DataAccess.ADOLookup("ActivityID", "tblActivity", "ProjectToCustomerID = " + Session["SelectedProjectToCustomerID"].ToString());
                        Response.Write("Activity ID Selected" + ActivityID);
                        cmd.Parameters.AddWithValue("@ActivityID", Convert.ToInt32(ActivityID));
                        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
                        if (tbl.Rows.Count > 0)
                        {
                            string SpendTypeName = tbl.Rows[0]["SpendTypeName"].ToString();
                            //Session["SelectedProjectToCustomerID"] = tbl.Rows[0]["ProjectToCustomerID"].ToString();
                            //Session["SelectedBudgetResponsibilityAreaID"] = tbl.Rows[0]["BudgetResponsibilityAreaID"].ToString();
                            ////Response.Redirect("POHeader.aspx?Mode=ExistingPOID");
                        }
                    }
                   // grdWorklist3.SelectedIndex = -1; 
                    break;
                case "4":
                    grdWorklist4.SelectedIndex = Int32.Parse(HiddenGridSelection4.Value);
                    if (grdWorklist4.Rows.Count != 0 && grdWorklist4.SelectedValue != null) {
                        int intPOID = Convert.ToInt32(Common.ADOLookup("POID", "qryWorklist4", "ActivityDetailID = " + grdWorklist4.SelectedValue));
                        grdWorklist4.SelectedIndex = -1; 
                        RedirectToPOHeader(intPOID);
                        SqlCommand cmd = DataAccess.CreateCommand();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "SelectWorklistDetailsByActivity";
                        string ActivityID = DataAccess.ADOLookup("ActivityID", "tblActivityDetail", "ActivityDetailID = " + grdWorklist4.SelectedValue);
                        cmd.Parameters.AddWithValue("@ActivityID", Convert.ToInt32(ActivityID));
                        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
                        if (tbl.Rows.Count > 0)
                        {
                            string SpendTypeName = tbl.Rows[0]["SpendTypeName"].ToString();
                            Session["SelectedProjectToCustomerID"] = tbl.Rows[0]["ProjectToCustomerID"].ToString();
                            Session["SelectedBudgetResponsibilityAreaID"] = tbl.Rows[0]["BudgetResponsibilityAreaID"].ToString();
                            Response.Redirect("POHeader.aspx?Mode=ExistingPOID");
                        }
                    }
                  
                    break;
                case "5":
                    grdWorklist5.SelectedIndex = Int32.Parse(HiddenGridSelection5.Value);
                    if (grdWorklist5.Rows.Count != 0 && grdWorklist5.SelectedValue != null)
                    {
                        grdWorklist5.SelectedIndex = -1; 
                        RedirectToPOHeader(Convert.ToInt32(grdWorklist5.SelectedValue));
                        SqlCommand cmd = DataAccess.CreateCommand();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "SelectWorklistDetailsByActivity";
                        string ActivityID = DataAccess.ADOLookup("ActivityID", "tblActivity", "POID = " + grdWorklist5.SelectedValue);
                        cmd.Parameters.AddWithValue("@ActivityID", Convert.ToInt32(ActivityID));
                        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
                        if (tbl.Rows.Count > 0)
                        {
                            string SpendTypeName = tbl.Rows[0]["SpendTypeName"].ToString();
                            Session["SelectedProjectToCustomerID"] = tbl.Rows[0]["ProjectToCustomerID"].ToString();
                            Session["SelectedBudgetResponsibilityAreaID"] = tbl.Rows[0]["BudgetResponsibilityAreaID"].ToString();
                            Response.Redirect("POHeader.aspx?Mode=ExistingPOID");
                        }
                    }
                   
                    break;
                case "6":
                    grdWorklist6.SelectedIndex = Int32.Parse(HiddenGridSelection6.Value);
                    if (grdWorklist6.Rows.Count != 0 && grdWorklist6.SelectedValue != null) {
                        grdWorklist6.SelectedIndex = -1; 
                        SqlCommand cmd = DataAccess.CreateCommand();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "SelectWorklistDetailsByActivity";
                        string ActivityID = DataAccess.ADOLookup("ActivityID", "tblActivity", "RebateID = " + grdWorklist6.SelectedValue);
                        cmd.Parameters.AddWithValue("@ActivityID", Convert.ToInt32(ActivityID));
                        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
                        if (tbl.Rows.Count > 0)
                        {
                            string SpendTypeName = tbl.Rows[0]["SpendTypeName"].ToString();
                            Session["SelectedProjectToCustomerID"] = tbl.Rows[0]["ProjectToCustomerID"].ToString();
                            Session["SelectedBudgetResponsibilityAreaID"] = tbl.Rows[0]["BudgetResponsibilityAreaID"].ToString();
                            Response.Redirect("RebateHeader.aspx?Mode=ExistingRebateID"); 
                        }
                    }
                   
                    break;
                case "7":
                    grdWorklist7.SelectedIndex = Int32.Parse(HiddenGridSelection7.Value);
                    if (grdWorklist7.Rows.Count != 0 && grdWorklist7.SelectedValue != null) {
                        grdWorklist7.SelectedIndex = -1; 
                        RedirectToRebateHeader(Convert.ToInt32(grdWorklist7.SelectedValue));
                        SqlCommand cmd = DataAccess.CreateCommand();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "SelectWorklistDetailsByActivity";
                        string ActivityID = DataAccess.ADOLookup("ActivityID", "tblActivity", "RebateID = " + grdWorklist6.SelectedValue);
                        cmd.Parameters.AddWithValue("@ActivityID", Convert.ToInt32(ActivityID));
                        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
                        if (tbl.Rows.Count > 0)
                        {
                            string SpendTypeName = tbl.Rows[0]["SpendTypeName"].ToString();
                            Session["SelectedProjectToCustomerID"] = tbl.Rows[0]["ProjectToCustomerID"].ToString();
                            Session["SelectedBudgetResponsibilityAreaID"] = tbl.Rows[0]["BudgetResponsibilityAreaID"].ToString();
                            Response.Redirect("RebateHeader.aspx?Mode=ExistingRebateID"); 
                        }
                    }
                   
                    break;
                case "8":
                    grdWorklist8.SelectedIndex = Int32.Parse(HiddenGridSelection8.Value);
                    if (grdWorklist8.Rows.Count != 0 && grdWorklist8.SelectedValue != null)
                    {
                        grdWorklist8.SelectedIndex = -1; 
                        RedirectToRebateHeader(Convert.ToInt32(grdWorklist8.SelectedValue));
                        SqlCommand cmd = DataAccess.CreateCommand();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "SelectWorklistDetailsByActivity";
                        string ActivityID = DataAccess.ADOLookup("ActivityID", "tblActivity", "RebateID = " + grdWorklist6.SelectedValue);
                        cmd.Parameters.AddWithValue("@ActivityID", Convert.ToInt32(ActivityID));
                        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
                        if (tbl.Rows.Count > 0)
                        {
                            string SpendTypeName = tbl.Rows[0]["SpendTypeName"].ToString();
                            Session["SelectedProjectToCustomerID"] = tbl.Rows[0]["ProjectToCustomerID"].ToString();
                            Session["SelectedBudgetResponsibilityAreaID"] = tbl.Rows[0]["BudgetResponsibilityAreaID"].ToString();
                            Response.Redirect("RebateHeader.aspx?Mode=ExistingRebateID");
                        }
                    }
                
                    break;
            }
        }
        catch (Exception ex)
        {
            MsgBox("Message", ex.Message);
            //MsgBox("Message", "Error - please retry (" + HiddenCurrentWorklist.Value + ").");
        }
    }
    
    protected void cmdCancel_Click(object sender, EventArgs e)
    //only available for Worklist4; cancel the request for receipting and revert to ReceiptingStatus = 0
    {
        RememberGridSelections();
        grdWorklist4.SelectedIndex = Int32.Parse(HiddenGridSelection4.Value);
        int intPOID = Convert.ToInt32(Common.ADOLookup("POID", "qryWorklist4", "ActivityDetailID = " + grdWorklist4.SelectedValue));
        RedirectToPOHeader(intPOID);
        WorklistDataAccess.UpdateReceiptedStatus(Convert.ToInt32(grdWorklist4.SelectedValue), "0", Session["CurrentUserName"].ToString());
        grdWorklist4.DataBind();
        //populate grid
        PopulateGrid();
        Refresh();
    }

    protected void cmdApprove_Click(object sender, EventArgs e)
    {
        //Call Approve procedure passing ReallocationID
        RememberGridSelections();
        grdWorklist9.SelectedIndex = Int32.Parse(HiddenGridSelection9.Value);
        int ReallocationID = Convert.ToInt32(grdWorklist9.SelectedValue);
        String strMessage = WorklistDataAccess.UpdateWorklist9(ReallocationID, 1);
        if (strMessage != "")
            MsgBox("Message", strMessage);
        grdWorklist9.DataBind();
        //populate grid
        PopulateGrid();
        Refresh();
    }

    protected void cmdReject_Click(object sender, EventArgs e)
    {
        RememberGridSelections();
        grdWorklist9.SelectedIndex = Int32.Parse(HiddenGridSelection9.Value);
        //Call Reject procedure passing ReallocationID
        int ReallocationID = Convert.ToInt32(grdWorklist9.SelectedValue);
        WorklistDataAccess.UpdateWorklist9(ReallocationID, 2);
        grdWorklist9.DataBind();
        //populate grid
        PopulateGrid();
        Refresh();
    }

    protected void cmdDelete_Click(object sender, EventArgs e)
    {
        RememberGridSelections();
        grdWorklist10.SelectedIndex = Int32.Parse(HiddenGridSelection10.Value);
        //Call Delete procedure passing ReallocationID
        int ReallocationID = Convert.ToInt32(grdWorklist10.SelectedValue);
        WorklistDataAccess.DeleteWorklist10(ReallocationID);
        grdWorklist10.DataBind();
        //populate grid
        PopulateGrid();
        Refresh();
    }

    protected void cmdCreatePR_Rbte_Click(object sender, EventArgs e)
    {
        RememberGridSelections();
        grdWorklist11.SelectedIndex = Int32.Parse(HiddenGridSelection11.Value);
        try
        {
            Session["SelectedActivityID"] = grdWorklist11.SelectedValue;
            Session["ReturnToScreen"] = "Worklist";
            SqlCommand cmd = DataAccess.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SelectWorklistDetailsByActivity";
            cmd.Parameters.AddWithValue("@ActivityID", grdWorklist11.SelectedValue);
            DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
            if(tbl.Rows.Count > 0)
            {
                string SpendTypeName = tbl.Rows[0]["SpendTypeName"].ToString();
                Session["SelectedProjectToCustomerID"] = tbl.Rows[0]["ProjectToCustomerID"].ToString();
                Session["SelectedBudgetResponsibilityAreaID"] = tbl.Rows[0]["BudgetResponsibilityAreaID"].ToString();
                if (SpendTypeName == "A&P")
                {
                    if (Common.ADOLookup("POID", "tblActivity", "ActivityID = " + grdWorklist11.SelectedValue) == "")
                    {
                        //redirect to PRF with supplier (if known) and line item already populated
                        Session["Mode"] = "New";
                        Response.Redirect("POHeader.aspx?Mode=NewPRSelectedItem");
                    }
                    else
                    {
                        //redirect to PRF with all header details and line items populated
                        Session["Mode"] = "Edit";
                        Response.Redirect("POHeader.aspx?Mode=ExistingPR");
                    }
                }
                else //(TCC or Coupons)
                {
                    if (Common.ADOLookup("RebateID", "tblActivity", "ActivityID = " + grdWorklist11.SelectedValue) == "")
                    {
                        //redirect to Rebate Request with activity already populated
                        Session["Mode"] = "New";
                        Response.Redirect("RebateHeader.aspx?Mode=NewRebateSelectedItem");
                    }
                    else
                    {
                        //redirect to Rebate Request with all header details and activities populated
                        Session["Mode"] = "Edit";
                        Response.Redirect("RebateHeader.aspx?Mode=ExistingRebate");
                    }
                }
            }
            else
            {
                MsgBox("Message", "Error!, The selection is invalid. Please contact the system administrator.");
            }
        }
        catch (Exception ex)
        {
            MsgBox("Message", "Error!, Select a row");
            //MsgBox("Message", "Error - please retry (" + HiddenCurrentWorklist.Value + ").");
        }
     }

    int GetColumnIndexByName(GridViewRow row, string SearchColumnName)
    {
        int columnIndex = 0;
        foreach (DataControlFieldCell cell in row.Cells)
        {
            if (cell.ContainingField is BoundField)
            {
                if (((BoundField)cell.ContainingField).DataField.Equals(SearchColumnName))
                {
                    break;
                }
            }
            columnIndex++;
        }
        return columnIndex;
    }
    protected void cmdBack_Click(object sender, EventArgs e)
    //return to Projects
    {
        RememberGridSelections();
        Response.Redirect("ProjectView.aspx");
    }

    protected void cmdActioned_Click(object sender, EventArgs e)
    {
        RememberGridSelections();
        //general error trapping routine - testbed
        try //in case no rows
        {
            switch (HiddenCurrentWorklist.Value)
            {
                case "2":
                    grdWorklist2.SelectedIndex = Int32.Parse(HiddenGridSelection2.Value);
                    WorklistDataAccess.UpdateReceiptedStatus(Convert.ToInt32(grdWorklist2.SelectedValue), "1", Session["CurrentUserName"].ToString());
                    grdWorklist2.DataBind();
                    //***select new row?***
                    break;
                case "3":
                    grdWorklist3.SelectedIndex = Int32.Parse(HiddenGridSelection3.Value);
                    grdWorklist3.SelectedIndex = -1; 
                    int intPOID = Convert.ToInt32(Common.ADOLookup("POID", "tblPOChange", "POChangeID = " + grdWorklist3.SelectedValue));
                    RedirectToPOHeader(intPOID);
                    WorklistDataAccess.UpdatePOChange(Convert.ToInt32(grdWorklist3.SelectedValue));
                    //                    grdWorklist3.DataBind(); - moved to later to prevent error
                    //int intPOID = Convert.ToInt32(Common.ADOLookup("POID", "tblPOChange", "POChangeID = " + grdWorklist3.SelectedValue));
                    grdWorklist3.DataBind(); //moved from earlier
                    RedirectToPOHeader(intPOID);
                    break;
                case "4":
                    grdWorklist4.SelectedIndex = Int32.Parse(HiddenGridSelection4.Value);
                    grdWorklist4.SelectedIndex = -1; 
                    int intPOID2 = Convert.ToInt32(Common.ADOLookup("POID", "qryWorklist4", "ActivityDetailID = " + grdWorklist4.SelectedValue));
                    RedirectToPOHeader(intPOID2);
                    WorklistDataAccess.UpdateReceiptedStatus(Convert.ToInt32(grdWorklist4.SelectedValue), "2", Session["CurrentUserName"].ToString());
                    grdWorklist4.DataBind();
                    //***select new row?***
                    break;
                case "6":
                    grdWorklist6.SelectedIndex = Int32.Parse(HiddenGridSelection6.Value);
                    int intSelectedValue = Convert.ToInt32(grdWorklist6.SelectedValue);
                    WorklistDataAccess.UpdateRebateChange(intSelectedValue);
                    grdWorklist6.DataBind();
                    RedirectToRebateHeader(intSelectedValue);
                    break;
            }
            Refresh();
        }
        catch (Exception ex)
        {
            //Common.LogError(ex);
            //            MsgBox("Message", "Error");
            txtMessage.Text = ex.ToString();
            txtMessage.Visible = true;
        }
    }

    #endregion

    #region methods
    protected void UserAccess()
    {
        //Get a data table object containing the LibraryImages
        SqlCommand cmd2 = DataAccess.CreateCommand();
        cmd2.CommandType = CommandType.Text;
        string strSQL = "SELECT UserName " +
                            " FROM " +
                                " (SELECT OptionValue AS UserName" +
                                " FROM tblOption " +
                                " WHERE OptionName = 'FinanceApproverUserName' " +
                                " UNION " +
                                " SELECT DelegateUserName AS UserName" +
                                " FROM tblUser " +
                                " WHERE UserName = (SELECT OptionValue " +
                                "                   FROM tblOption " +
                                "                   WHERE OptionName = 'FinanceApproverUserName')) t" +
                            " WHERE UserName = \'" + Session["CurrentUserName"] + "\';";
        cmd2.CommandText = strSQL;
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd2);
        //string UserName = Session["CurrentUserName"].ToString();

        if (tbl.Rows.Count > 0)
        {
            lbtnFinanceApproval.Visible= true;
            txtWorklist12.Visible = true;
           
        }
        else
        {
            lbtnFinanceApproval.Visible = false;
            txtWorklist12.Visible = false;
           
        }
    }

    public void Refresh()
    {
        DataTable tbl = WorklistDataAccess.WorklistSummary(Session["CurrentUserName"].ToString());
        txtWorklist1.Text = tbl.Rows[0].ItemArray[0].ToString();
        txtWorklist2.Text = tbl.Rows[0].ItemArray[1].ToString();
        txtWorklist3.Text = tbl.Rows[0].ItemArray[2].ToString();
        txtWorklist4.Text = tbl.Rows[0].ItemArray[3].ToString();
        txtWorklist5.Text = tbl.Rows[0].ItemArray[4].ToString();
        txtWorklist6.Text = tbl.Rows[0].ItemArray[5].ToString();
        txtWorklist7.Text = tbl.Rows[0].ItemArray[6].ToString();
        txtWorklist8.Text = tbl.Rows[0].ItemArray[7].ToString();
        txtWorklist9.Text = tbl.Rows[0].ItemArray[8].ToString();
        txtWorklist10.Text = tbl.Rows[0].ItemArray[9].ToString();
        txtWorklist11.Text = tbl.Rows[0].ItemArray[10].ToString();
        txtWorklist12.Text = tbl.Rows[0].ItemArray[11].ToString();

    }

    protected void PopulateGrid()
    {
        grdWorklist01.DataBind();
        grdWorklist2.DataBind();
        grdWorklist3.DataBind();
        grdWorklist4.DataBind();
        grdWorklist5.DataBind();
        grdWorklist6.DataBind();
        grdWorklist7.DataBind();
        grdWorklist8.DataBind();
        grdWorklist9.DataBind();
        grdWorklist10.DataBind();
        grdWorklist11.DataBind();
        grdWorklist12.DataBind();
    }

    public void SetScroll()
    {
        String UID = this.UniqueID.Replace("$", "_") + "_";
        Page page = HttpContext.Current.CurrentHandler as Page;
        page.ClientScript.RegisterStartupScript(typeof(Page), "setscroll", "<script type='text/javascript'>setscroll('" + UID + "');</script>");
    }

    protected void RedirectToPOHeader(int intPOID)
    {
        //get ProjectToCustomerID for this POID
        DataTable tbl = POHeaderDataAccess.SelectProjectForPOID(intPOID);
        Session["SelectedProjectToCustomerID"] = tbl.Rows[0]["ProjectToCustomerID"];
        Session["ReturnToScreen"] = "Worklist";
        Session["SelectedPOID"] = intPOID;
    }

    protected void RedirectToRebateHeader(int intRebateID)
    {
        //get ProjectToCustomerID for this RebateID
        DataTable tbl = RebateHeaderDataAccess.SelectProjectForRebateID(intRebateID);
        Session["SelectedProjectToCustomerID"] = tbl.Rows[0]["ProjectToCustomerID"];
        Session["ReturnToScreen"] = "Worklist";
        Session["SelectedRebateID"] = intRebateID;
    }

    protected void MsgBox(string strTitle, string strMessage)
    {
        Page.ClientScript.RegisterStartupScript(this.GetType(), strTitle, "alert('" + Common.Left(strMessage, 150) + "');", true);
    }

    public void RememberGridSelections()
    {
        Session["Worklist1"] = HiddenGridSelection1.Value;
        Session["Worklist2"] = HiddenGridSelection2.Value;
        Session["Worklist3"] = HiddenGridSelection3.Value;
        Session["Worklist4"] = HiddenGridSelection4.Value;
        Session["Worklist5"] = HiddenGridSelection5.Value;
        Session["Worklist6"] = HiddenGridSelection6.Value;
        Session["Worklist7"] = HiddenGridSelection7.Value;
        Session["Worklist8"] = HiddenGridSelection8.Value;
        Session["Worklist9"] = HiddenGridSelection9.Value;
        Session["Worklist10"] = HiddenGridSelection10.Value;
        Session["Worklist11"] = HiddenGridSelection11.Value;
        Session["Worklist12"] = HiddenGridSelection12.Value;
    }

    #endregion
}