﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SubBrandGrid.ascx.cs" Inherits="User_Controls_SubBrandGrid" %>

        <!--Page Header-->
        <table border="0" width="1100px">
            <tr>
                <td align="left">
                    <asp:Label ID="lblHeader" Text="Brand-Markets" runat="server" CssClass="page_header"/>
                </td>
                <td align="right">
                    <asp:Label ID="lblSearch" Text="Search:" runat="server"></asp:Label>
                    <asp:TextBox ID="txtSearch" Runat="server" Width="120px"></asp:TextBox>
                    <asp:Button ID="cmdGo" Text="Go" Runat="server" CausesValidation="false" />
                </td>
            </tr>
            <tr><td style="font-size:3px">&nbsp;</td></tr>
        </table>
        
        <!--Data Source - required to take advantage of sorting and paging functionality of the GridView-->
        <asp:SqlDataSource ID="sds" runat="server" 
            ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
            FilterExpression="SubBrandName Like '%{0}%'">
            <FilterParameters>
                <asp:ControlParameter ControlID="txtSearch" PropertyName="Text" />
            </FilterParameters>
        </asp:SqlDataSource>

        <!--Project Grid-->
        <div id="project_grid">
            <asp:GridView ID="grd" runat="server" width="495px"
                AutoGenerateColumns="False" GridLines="Vertical"
                RowStyle-Wrap="false" EmptyDataText="No records found" 
                HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
                SelectedRowStyle-Wrap="false"
                AllowSorting="True" AllowPaging="true"
                onselectedindexchanged="grd_SelectedIndexChanged" 
                OnRowDataBound="grd_RowDataBound" 
                onpageindexchanged="grd_PageIndexChanged"  onsorted="grd_Sorted"
                DataKeyNames="SubBrandID" DataSourceID="sds" PageSize="20">
                <Columns>
                    <asp:BoundField DataField="SubBrandID" ReadOnly="True" Visible="False" />
                    <asp:BoundField DataField="SubBrandName" HeaderText="Brand-Market Name" SortExpression="SubBrandName" />
                </Columns>
                <RowStyle CssClass="grid_row" Wrap="False" />
                <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
                <HeaderStyle CssClass="grid_header" Wrap="False" />
                <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
            </asp:GridView>
            <asp:Label ID="txtMessage" runat="server" />
       </div><br />

