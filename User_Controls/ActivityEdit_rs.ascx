﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActivityEdit_rs.ascx.cs" Inherits="User_Controls_ActivityEdit_rs" %>

<%@ Register src="SupplierGrid.ascx" tagname="SupplierGrid" tagprefix="uc5" %>
<%@ Register src="ActivityTypeGrid.ascx" tagname="ActivityTypeGrid" tagprefix="uc6" %>

<style type="text/css">
    .auto-style1 {
        height: 23px;
    }
</style>

<script type="text/javascript">
    function setNeedToConfirm(bool) {
        //set isChanged flag
        var ctl = document.all.ctl00$ContentPlaceHolder1$ActivityEdit_rs1$txtChanged
        if (bool) { ctl.value = "1"; }
        else { ctl.value = "0"; }
    }
    function showPhasing() {
        if (document.all.ctl00$ContentPlaceHolder1$ActivityEdit_rs1$ProjectMonth.value == "13") {
            divPhasing.style.display = "block";
        } else {
            divPhasing.style.display = "none";
        }
        setNeedToConfirm(true);
    }

    function copyToShortText() {
        if (document.all.ctl00$ContentPlaceHolder1$ActivityEdit_rs1$ShortText.value == "") {
        document.all.ctl00$ContentPlaceHolder1$ActivityEdit_rs1$ShortText.value = document.all.ctl00$ContentPlaceHolder1$ActivityEdit_rs1$ActivityName.value;
            //alert(document.all.ctl00$ContentPlaceHolder1$ActivityEdit1$ActivityName.value);
        }
        
    }

    function SupplierEnableOnSpendType() {
        if (document.getElementById('ctl00_ContentPlaceHolder1_ActivityEdit_rs1_cboSpendType').value == "1") {
            document.getElementById('ctl00_ContentPlaceHolder1_ActivityEdit_rs1_cmdSupplierLookup').disabled = false;
        }
        else {
            document.getElementById('ctl00_ContentPlaceHolder1_ActivityEdit_rs1_cmdSupplierLookup').disabled = true;
        }
    }

</script>
 
<!--Activity Type Edit Area-->   
<div id="parent">
    <div id="resizable1" class="resizable ui-widget-content" style="height:101.5%">
        <br />
        <asp:Label ID="lblHeader" Text="Edit Activity" runat="server" CssClass="page_header"></asp:Label>
        <br /><br/>
        <table border="0" style="width:100%">
            <tr>
                <td>Project: </td><td></td>
            </tr><tr>
                <td colspan="2">
                    <asp:Label id="lblProjectName" runat="server" width="270px" CSSClass="label_text" Text="" Font-Bold="True" />
                </td>
            </tr>
            <tr>
                <td>Area: </td>
                <td>
                    <asp:Label id="lblBudgetResponsibilityAreaName" runat="server" width="164px" Text="" Font-Bold="True" CSSClass="label_text" />
                    <asp:Label ID="lblBudgetResponsibilityAreaID" runat="server" Visible="false" Width="20px" />
                </td>
            </tr>
            <tr>
                <td>Currency: </td>
                <td>
                    <asp:Label id="lblCurrencyCode" runat="server" width="164px" CSSClass="label_text" Text="" Font-Bold="True" />
                </td>
            </tr>
            <tr>
                <td>Internal Order: </td>
                <td>
                    <asp:Label id="lblIONumber" runat="server" width="164px" CSSClass="label_text" Text="" Font-Bold="True" />
                </td>
            </tr><tr>
                <td>Project Year: </td>
                <td>
                    <asp:Label id="lblProjectYear" runat="server" width="164px" CSSClass="label_text" Text="" Font-Bold="True" />
                </td>
            </tr>
            <tr>
                <td>Customer: </td>
                <td>
                    <asp:Label id="lblCustomerName" runat="server" width="164px" Text="" Font-Bold="True" CSSClass="label_text" />
                </td>
            </tr><tr>
                <td>Spend Type: </td>
                <td>
                    <!--<asp:Label id="SpendTypeName" runat="server" width="164px" Text="" Font-Bold="True" CSSClass="label_text" />-->
                    <asp:DropDownList ID="cboSpendType" runat="server" Width="164px" Enabled="False" 
                            DataTextField="EntityName" DataValueField="ID" ForeColor="Black" onchange = "SupplierEnableOnSpendType();"/>
                    <!--<asp:TextBox ID="SpendTypeID" runat="server" Width="1px" visible="false" />-->
                </td>
            </tr>
        </table>    
    </div>

    <div id="resizable2" class="ui-widget-content" style="height:101.5%">    
        <table><tr><td>
            <table border="0"><tr><td>
                <table border="0">
                    <tr><td>Activity Name</td><td>
                        <asp:TextBox ID="ActivityID" runat="server" Visible="false" Width="40px" Enabled="false" />
                        <asp:HiddenField ID="txtChanged" runat="server" />
                        <asp:TextBox ID="ActivityName" runat="server" Width="280px" MaxLength="100" 
                            Enabled="False" onkeyup="Javascript:setNeedToConfirm(true);" ForeColor="Black"/>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                            ErrorMessage="*Required" ControlToValidate="ActivityName" 
                            Display="Dynamic" ValidationGroup="Activity"></asp:RequiredFieldValidator>
                    </td></tr>
                    <tr><td>Short Text</td><td>
                        <asp:TextBox ID="ShortText" runat="server" Width="280px" Enabled="False" MaxLength="100"
                            onkeyup="Javascript:setNeedToConfirm(true);" ForeColor="Black"/>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                            ErrorMessage="*Required" ControlToValidate="ShortText" 
                            Display="Dynamic" ValidationGroup="Activity"></asp:RequiredFieldValidator>
                    </td></tr>
                    <tr><td>Supplier<asp:TextBox ID="SupplierID" runat="server" Width="30px" visible="false" /></td><td>
                        <asp:TextBox ID="SupplierName" runat="server" Width="280px" enabled="false" ForeColor="Black"/>
                        <asp:Button ID="cmdSupplierLookup" Text="Supplier" Runat="server" onclientclick="Javascript:setNeedToConfirm(false);"
                            onclick="cmdSupplierLookup_Click" CssClass="form_button" CausesValidation="false" enabled="true" width="85px" />
                    </td></tr>
                    <tr><td>Supplier Job Ref</td><td>
                        <asp:TextBox ID="SupplierRefNo" runat="server" Width="80px" MaxLength="20" Enabled="False"
                            onkeyup="Javascript:setNeedToConfirm(true);" ForeColor="Black"/>
                    </td></tr>
                    <tr><td>Comments</td><td>
                        <asp:TextBox ID="Comments" runat="server" Width="280px" MaxLength="255" Enabled="False"
                            onkeyup="Javascript:setNeedToConfirm(true);" ForeColor="Black"/>
                    </td></tr>
                    <tr><td>Activity Type</td><td>
                        <asp:TextBox ID="ActivityTypeID" runat="server" Width="30px" visible="false" />
                        <asp:TextBox ID="ActivityTypeName" runat="server" Width="280px" enabled="false" ForeColor="Black"/>
                        <asp:Button ID="cmdActivityTypeLookup" Text="Activity Type" Runat="server" 
                            onclick="cmdActivityTypeLookup_Click" onclientclick="Javascript:setNeedToConfirm(false);"
                            CssClass="form_button" CausesValidation="false" enabled="true" width="85px" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                            ControlToValidate="ActivityTypeID" Display="Dynamic" ErrorMessage="*Required Field"
                            ValidationGroup="Activity" />
                    </td></tr>
                    <tr><td>Committed Overrider</td><td>
                        <asp:DropDownList ID="CommittedOverrider" runat="server" Width="50px" Enabled="False" 
                            DataTextField="EntityName" DataValueField="ID" ForeColor="Black"/>
                        <asp:RangeValidator ID="RangeValidator1" runat="server" 
                            ErrorMessage="*Required" MaximumValue="2" MinimumValue="1" 
                            ControlToValidate="CommittedOverrider" Display="Dynamic" ValidationGroup="Activity" />
                    </td></tr>
                    <tr><td>Promo Indicator</td><td>
                        <asp:DropDownList ID="PromoIndicator" runat="server" Width="50px" Enabled="False" 
                            DataTextField="EntityName" DataValueField="ID" ForeColor="Black"/>
                        <asp:RangeValidator ID="RangeValidator3" runat="server" 
                            ErrorMessage="*Required" MaximumValue="2" MinimumValue="1" 
                            ControlToValidate="PromoIndicator" Display="Dynamic" ValidationGroup="Activity" />
                    </td></tr>                
                    <tr><td>Document Number</td><td>
                        <asp:TextBox ID="POID" runat="server" Width="30px" visible="false" />
                        <asp:TextBox ID="RebateID" runat="server" Width="30px" visible="false" />
                        <asp:TextBox ID="DocumentNumber" runat="server" Width="80px" Enabled="False" ForeColor="Black"
                            onkeyup="Javascript:setNeedToConfirm(true);" />
                    </td></tr>
                    <tr><td>Quantity</td><td>
                        <asp:TextBox ID="ItemQuantity" runat="server" Width="80px" Enabled="False"
                            onkeyup="Javascript:setNeedToConfirm(true);" ForeColor="Black"/>
                        <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="*Must be between 1 and 10,000,000" 
                            ControlToValidate="ItemQuantity" Type="Integer" MinimumValue="1" MaximumValue="10000000" ValidationGroup="Activity" Display="Dynamic"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                            ErrorMessage="*Required" ControlToValidate="ItemQuantity" 
                            Display="Dynamic" ValidationGroup="Activity"></asp:RequiredFieldValidator>
                    </td></tr>
                    <tr><td>Unit Price</td><td>
                        <asp:TextBox ID="UnitPrice" runat="server" Width="80px" Enabled="False" 
                            onkeyup="Javascript:setNeedToConfirm(true);" ForeColor="Black"/>
                       <asp:RangeValidator ID="RangeValidator4" runat="server" ErrorMessage="*Must be between -10,000,000 and 10,000,000" 
                            ControlToValidate="UnitPrice" Type="Double" MinimumValue="-9999999.9999" MaximumValue="9999999.9999" ValidationGroup="Activity" Display="Dynamic"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                            ErrorMessage="*Required" ControlToValidate="UnitPrice" 
                            Display="Dynamic" ValidationGroup="Activity"></asp:RequiredFieldValidator>
                        
                    </td></tr>
                    <tr><td>Month</td><td>
                        <asp:DropDownList ID="ProjectMonth" runat="server" Width="86px" Enabled="False" 
                            DataValueField="ID" DataTextField="EntityName" ForeColor="Black"/>
                        <asp:RangeValidator ID="RangeValidator5" runat="server" 
                            ErrorMessage="*Required" MaximumValue="13" MinimumValue="01" 
                            ControlToValidate="ProjectMonth" Display="Dynamic" ValidationGroup="Activity" Type="String"></asp:RangeValidator>
                    </td></tr>
                    <tr><td colspan="2" class="auto-style1">
                        <asp:CustomValidator ID="CustomValidator1" runat="server" controltovalidate="ItemQuantity"
                            ErrorMessage="*Monthly breakdown does not add up to Quantity x Unit Price or values too large" 
                            onservervalidate="CustomValidator1_ServerValidate" ValidationGroup="Activity" Display="Dynamic" />
                    </td></tr>    
                </table>
            </td>
        </tr></table>
    </td></tr>
    <tr><td>
    </div>
    <%if (ProjectMonthValue == "13") { %>
        <div id="divPhasing" style="display:block"> </div>
    <% } else {%>
        <div id="divPhasing" style="display:none">
    <% } %>
        <asp:Table ID="tblMonthlyBreakdown" runat="server" Visible="true">
            <asp:TableRow>
                <asp:TableCell>
                    &nbsp;
                </asp:TableCell>
                <asp:TableCell>
                    Jan
                </asp:TableCell>
                <asp:TableCell>
                    Feb
                </asp:TableCell>
                <asp:TableCell>
                    Mar
                </asp:TableCell>
                <asp:TableCell>
                    Apr
                </asp:TableCell>
                <asp:TableCell>
                    May
                </asp:TableCell>
                <asp:TableCell>
                    Jun
                </asp:TableCell>
                <asp:TableCell>
                    Jul
                </asp:TableCell>
                <asp:TableCell>
                    Aug
                </asp:TableCell>
                <asp:TableCell>
                    Sep
                </asp:TableCell>
                <asp:TableCell>
                    Oct
                </asp:TableCell>
                <asp:TableCell>
                    Nov
                </asp:TableCell>
                <asp:TableCell>
                    Dec
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    Monthly Breakdown (whole numbers only)
                </asp:TableCell>
                <asp:TableCell>
                    <asp:TextBox ID="Month01" runat="server" Width="60px" Enabled="true" ForeColor="Black"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </asp:TableCell><asp:TableCell>
                    <asp:TextBox ID="Month02" runat="server" Width="60px" Enabled="true" ForeColor="Black"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </asp:TableCell><asp:TableCell>
                    <asp:TextBox ID="Month03" runat="server" Width="60px" Enabled="true" ForeColor="Black"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </asp:TableCell><asp:TableCell>
                    <asp:TextBox ID="Month04" runat="server" Width="60px" Enabled="true" ForeColor="Black"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </asp:TableCell><asp:TableCell>
                    <asp:TextBox ID="Month05" runat="server" Width="60px" Enabled="true" ForeColor="Black"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </asp:TableCell><asp:TableCell>
                    <asp:TextBox ID="Month06" runat="server" Width="60px" Enabled="true" ForeColor="Black"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </asp:TableCell><asp:TableCell>
                    <asp:TextBox ID="Month07" runat="server" Width="60px" Enabled="true" ForeColor="Black"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </asp:TableCell><asp:TableCell>
                    <asp:TextBox ID="Month08" runat="server" Width="60px" Enabled="true" ForeColor="Black"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </asp:TableCell><asp:TableCell>
                    <asp:TextBox ID="Month09" runat="server" Width="60px" Enabled="true" ForeColor="Black"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </asp:TableCell><asp:TableCell>
                    <asp:TextBox ID="Month10" runat="server" Width="60px" Enabled="true" ForeColor="Black"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </asp:TableCell><asp:TableCell>
                    <asp:TextBox ID="Month11" runat="server" Width="60px" Enabled="true" ForeColor="Black"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </asp:TableCell><asp:TableCell>
                    <asp:TextBox ID="Month12" runat="server" Width="60px" Enabled="true" ForeColor="Black"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        </div>
    <br />
    <asp:Label ID="Title" runat="server" Text="Select File:">
    </asp:Label>&nbsp; 
    <asp:FileUpload ID="FileUploadActivity" runat="server" Width="500px" />
    <asp:Button ID="btnUpload" runat="server" Text="Upload" enabled="true"  CssClass="form_button" OnClick="btnUpload_Click" 
        />&nbsp; &nbsp; &nbsp; <asp:Label ID="lblUploadFile" runat="server" Text="" CssClass="page_header" ></asp:Label>
    
    <asp:GridView ID="GridViewFiles" runat="server"
                 AutoGenerateColumns="False"
            EmptyDataText = "No files uploaded" CellPadding="4"
            EnableModelValidation="True" ForeColor="#333333"  GridLines="Vertical" Width="492px">
           <AlternatingRowStyle BackColor="White" CssClass="grid_alternating_row" Wrap="False"  />
    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    <Columns>
       
          <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="FileName" HeaderText="File Name" ItemStyle-HorizontalAlign="Left"  />
         <asp:TemplateField>
            <ItemTemplate>
                <asp:LinkButton ID="Download" Text = "Download" CommandArgument = '<%# Eval("FileName") %>' runat="server" OnCommand ="DownloadLink_click"></asp:LinkButton></ItemTemplate></asp:TemplateField><asp:TemplateField>
            <ItemTemplate>
                <asp:LinkButton ID = "Delete" Text = "Delete" CommandArgument = '<%# Eval("FileName") %>' runat = "server" OnCommand ="DeleteLink_Click" OnClientClick="return confirm('Are you sure you want to delete this record?');" />
            </ItemTemplate>
        </asp:TemplateField>
       
       
    </Columns>        
                <FooterStyle CssClass="grid_footer" Wrap="False" />
                <HeaderStyle BackColor="Gray" ForeColor="White" Height="15px" Wrap="false" />
    
                <RowStyle CssClass="grid_row" Wrap="False" BackColor="#EFF3FB" />
            </asp:GridView>
            
            
            
           <asp:Button id="dummyButton1" runat="server" style="display:none;" />
        <asp:ModalPopupExtender ID="mp1" runat="server" PopupControlID="Panel1" TargetControlID="dummyButton1" BackgroundCssClass="modalBackground" />
        <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" style = "display:none">  
            
           
     
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <!--Supplier Selection-->
                    <uc5:SupplierGrid ID="SupplierGrid1" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
            <!--Supplier Buttons-->
            <asp:Button ID="cmdSaveSupplier" runat="server" Text="Save" onclick="cmdSaveSupplier_Click" 
                CssClass="form_button" Enabled="true" CausesValidation="true" Visible="true" />
            <asp:Button ID="cmdCancelSupplier" runat="server" Text="Cancel" onclick="cmdCancelSupplier_Click"
                CssClass="form_button" Enabled="true" CausesValidation="false" Visible="true" />        
        </asp:Panel>
        <!-- ModalPopupExtender -->

        <!-- ActivityType ModalPopupExtender -->
        <asp:Button id="dummyButton2" runat="server" style="display:none;" />
        <asp:ModalPopupExtender ID="mp2" runat="server" PopupControlID="Panel2" TargetControlID="dummyButton2" BackgroundCssClass="modalBackground" />
        <asp:Panel ID="Panel2" runat="server" CssClass="modalPopup" style = "display:none">    
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <!--Activity Type Selection-->
                    <uc6:ActivityTypeGrid ID="ActivityTypeGrid1" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
            <!--ActivityType Buttons-->
            <asp:Button ID="cmdSaveActivityType" runat="server" Text="Save" onclick="cmdSaveActivityType_Click" 
                CssClass="form_button" Enabled="true" CausesValidation="true"/>
            <asp:Button ID="cmdCancelActivityType" runat="server" Text="Cancel" onclick="cmdCancelSupplier_Click" 
                CssClass="form_button" Enabled="true" CausesValidation="false"/>
        </asp:Panel>
        </div>