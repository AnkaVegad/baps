﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Controls_ProjectGrid : System.Web.UI.UserControl
{
    int intCol1 = 0;
    int intCol2 = 0;
    int intCol3 = 0;
    int intCol4 = 0;
    int intCol5 = 0;
    int intCol6 = 0;
    int intCol7 = 0;
    int intTotal = 0;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        //This is additionally set here because otherwise a "Data Key Names must be set for Grid View" error occurs
        grd.DataKeyNames = new string[] { "ProjectToCustomerID" };

        if (!IsPostBack)
        {
            //populate selection dropdowns
            FillCustomer();
            FillYear();
            FillBrand();
            FillCategory();
            FillBudgetResponsibilityArea();
            //added AH 21/08/12
            FillPulsingPlanPriority();
            FillPulsingPlanPriorityMatchType();

            //get current year from session variable set in global.asax
            cboYear.SelectedValue = Session["SelectedYear"].ToString();

            DataTable tbl = UserDataAccess.SelectUser(Session["CurrentUserName"].ToString());
            //set area 
            if (Session["SelectedBudgetResponsibilityAreaID"] == null)
            {
                if (tbl.Rows[0]["LastBudgetResponsibilityAreaID"] == DBNull.Value)
                //allows for user's first login when table entry will be blank
                {
                    Session["SelectedBudgetResponsibilityAreaID"] = Common.ADOLookup("BudgetResponsibilityAreaID", "qryUserAuthorisations", "UserName = '" + Session["CurrentUserName"].ToString().Replace("'", "''") + "'");
                }
                else
                {
                    Session["SelectedBudgetResponsibilityAreaID"] = tbl.Rows[0]["LastBudgetResponsibilityAreaID"].ToString();
                }
            }
            cboBudgetResponsibilityArea.SelectedValue = Session["SelectedBudgetResponsibilityAreaID"].ToString();

            if (Session["SelectedCustomerID"] == null)
            {
                Session["SelectedCustomerID"] = tbl.Rows[0]["LastCustomerID"].ToString();
            }
            cboCustomer.SelectedValue = Session["SelectedCustomerID"].ToString();

            if (Session["SelectedBrandID"] == null)
            {
                Session["SelectedBrandID"] = tbl.Rows[0]["LastBrandID"].ToString();
            }
            cboBrand.SelectedValue = Session["SelectedBrandID"].ToString();

            if (Session["SelectedCategoryID"] == null)
            {
                Session["SelectedCategoryID"] = tbl.Rows[0]["LastCategoryID"].ToString();
            }
            cboCategory.SelectedValue = Session["SelectedCategoryID"].ToString();

            //Added AH 16/12/12 - ISNULLs in qryUser should deal with "first use" issue
            cboPulsingPlanPriorityMatchType.SelectedValue = tbl.Rows[0]["LastPulsingPlanPriorityMatchType"].ToString();
            cboPulsingPlanPriority.SelectedValue = tbl.Rows[0]["LastPulsingPlanPriorityID"].ToString();

        }

        string strSQL = "SELECT TOP 101 dbo.qryProjectViewInput.ProjectID, dbo.qryProjectViewInput.CustomerID, dbo.qryProjectViewInput.ProjectToCustomerID, dbo.qryProjectViewInput.ProjectName, ";
        strSQL += "dbo.qryProjectViewInput.CustomerName, dbo.qryProjectViewInput.IONumber, dbo.qryProjectViewInput.ProjectYear, SUM(dbo.qryProjectViewInput.APBudget) ";
        strSQL += "AS SumOfAPBudget, SUM(dbo.qryProjectViewInput.TCCBudget) AS SumOfTCCBudget, SUM(dbo.qryProjectViewInput.APFYView) AS SumOfAPFYView, ";
        strSQL += "SUM(dbo.qryProjectViewInput.TCCFYView) AS SumOfTCCFYView, SUM(dbo.qryProjectViewInput.APVariance) AS SumOfAPVariance, ";
        strSQL += "SUM(dbo.qryProjectViewInput.TCCVariance) AS SumOfTCCVariance, ";
        strSQL += "SUM(dbo.qryProjectViewInput.FYView) AS SumOfFYView, ";
        strSQL += "SUM(dbo.qryProjectViewInput.Variance) AS SumOfVariance ";
        strSQL += "FROM dbo.qryProjectViewInput LEFT OUTER JOIN dbo.tblProductHierarchy ON dbo.qryProjectViewInput.SubBrandID = dbo.tblProductHierarchy.SubBrandID ";
        strSQL += "WHERE ProjectYear = '" + cboYear.SelectedValue + "' ";
        if (cboCustomer.SelectedValue != "0")
        {
            strSQL += "AND CustomerID = " + cboCustomer.SelectedValue + " ";
        }
        if (cboBrand.SelectedValue != "0")
        {
            strSQL += "AND tblProductHierarchy.BrandID = " + cboBrand.SelectedValue + " ";
        }
        if (cboCategory.SelectedValue != "0")
        {
            strSQL += "AND UKCategoryID = " + cboCategory.SelectedValue + " ";
        }
        if (cboBudgetResponsibilityArea.SelectedValue != "0")
        {
            strSQL += "AND BudgetResponsibilityAreaID = " + cboBudgetResponsibilityArea.SelectedValue + " ";
        }
        //added AH 21/08/12
        if (cboPulsingPlanPriority.SelectedValue != "0")
        {
            if (cboPulsingPlanPriorityMatchType.SelectedValue == "1")
            {
                strSQL += "AND PulsingPlanPriorityID = " + cboPulsingPlanPriority.SelectedValue + " ";
            }
            else
            {
                strSQL += "AND NOT PulsingPlanPriorityID = " + cboPulsingPlanPriority.SelectedValue + " ";
            }
        }
        //Added 28/02/14 to solve filter problem with SQLDataSource when >101 records
        if (txtSearch.Text != String.Empty)
        {
            strSQL += "AND ProjectName Like '%" + txtSearch.Text + "%' ";
        }
        //amended 06/12/12 - show all projects unless in the Reallocate and Paste views
        if (lblHeaderText.Text == "Paste To" || lblHeaderText.Text == "Reallocate To/From" || lblHeaderText.Text == "Select Project")
        {
            strSQL += "AND EXISTS(SELECT 'X' FROM qryUserAuthorisations WHERE UserName = '" + Session["CurrentUserName"].ToString().Replace("'", "''") + "' AND ((qryUserAuthorisations.BrandID = dbo.tblProductHierarchy.BrandID AND qryUserAuthorisations.CategoryID = 1) OR (qryUserAuthorisations.BrandID = dbo.tblProductHierarchy.UKCategoryID AND qryUserAuthorisations.CategoryID = 2) OR (qryUserAuthorisations.BrandID = 0)) AND (qryUserAuthorisations.CustomerID = dbo.qryProjectViewInput.CustomerID OR qryUserAuthorisations.CustomerID = 0) AND (qryUserAuthorisations.BudgetResponsibilityAreaID = qryProjectViewInput.BudgetResponsibilityAreaID)) ";
        }
        strSQL += "GROUP BY dbo.qryProjectViewInput.ProjectID, dbo.qryProjectViewInput.CustomerID, dbo.qryProjectViewInput.ProjectToCustomerID, dbo.qryProjectViewInput.ProjectName, ";
        strSQL += "dbo.qryProjectViewInput.CustomerName, dbo.qryProjectViewInput.IONumber, dbo.qryProjectViewInput.ProjectYear ";
        strSQL += "ORDER BY ProjectName;";
        sds.SelectCommand = strSQL;
        grd.DataBind();
        //max rows retrieved message
        if (grd.PageCount == 6 && grd.PageSize == 20)
        {
            txtMessage.Text = "The maximum number of rows has been retrieved; to see other projects, change your selection criteria.";
        }
        else
        {
            if (grd.PageCount == 11 && grd.PageSize == 10)
            {
                txtMessage.Text = "The maximum number of rows has been retrieved; to see other projects, change your selection criteria.";
            }
            else
            {
                txtMessage.Text = String.Empty;
            }
        }

        if (!IsPostBack)
        //ensure a row is always selected
        {
            if (Session["SelectedProjectToCustomerID"] == null)
            {
                //select first row
                grd.SelectedIndex = 0;
                Session["SelectedProjectToCustomerID"] = grd.SelectedValue;
            }
            else
            {
                //reselect remembered row
                for (int i = 0; i < grd.Rows.Count; i++)
                {
                    if (grd.DataKeys[grd.Rows[i].RowIndex].Value.ToString() == Session["SelectedProjectToCustomerID"].ToString())
                    {
                        grd.SelectedIndex = i;
                    }
                }
            }
        }        
    }

#region properties

    public int GridPageSize
    {
        set { grd.PageSize = value; }
    }
    
    public string SelectedValue
    {
        get { return grd.SelectedValue.ToString(); }
    }

    public string SelectedProjectID
    {
        get {
            string strProjectID = Common.ADOLookup("ProjectID", "tblProjectToCustomer", "ProjectToCustomerID = " + grd.SelectedValue.ToString());
            return strProjectID; 
        }
    }

    public string HeaderText
    {
        set { lblHeaderText.Text = value; }
    }

#endregion

#region methods

    protected void UpdateUserSelections()
    {
        UserDataAccess.UpdateUserSelections(Convert.ToInt32(cboCustomer.SelectedValue), Convert.ToInt32(cboBudgetResponsibilityArea.SelectedValue), Convert.ToInt32(cboBrand.SelectedValue), Convert.ToInt32(cboCategory.SelectedValue), cboPulsingPlanPriorityMatchType.SelectedValue.ToString(), Convert.ToInt32(cboPulsingPlanPriority.SelectedValue), Session["CurrentUserName"].ToString());
    }

    public void Refresh()
    {
        grd.DataBind();
    }

    protected void SelectFirstRow()
    {
        try
        {
            grd.SelectedIndex = 0;
            Session["SelectedProjectToCustomerID"] = grd.SelectedValue;
        }
        catch { }
    }

#endregion

#region gridevents

    protected void grd_SelectedIndexChanged(object sender, EventArgs e)
    {
        //amended 06/12/12 - remember the selected project unless in Paste or Reallocate views
        if (lblHeaderText.Text != "Paste To" && lblHeaderText.Text != "Reallocate To/From" && lblHeaderText.Text != "Select Project")
        {
            Session["SelectedProjectToCustomerID"] = Convert.ToInt32(grd.SelectedValue);
        }
    }

    protected void grd_PageIndexChanged(object sender, EventArgs e)
    {
        intCol1 = 0;
        intCol2 = 0;
        intCol3 = 0;
        intCol4 = 0;
        intCol5 = 0;
        intCol6 = 0;
        intCol7 = 0;
        intTotal = 0;
        grd.SelectedIndex = -1;
    }

    protected void grd_Sorted(object sender, EventArgs e)
    {
        grd.SelectedIndex = -1;
    }

    protected void grd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            int intCurrent = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "SumOfVariance"));
            //colour code the cell 
            if (intCurrent < 0) { e.Row.Cells[12].BackColor = Color.PapayaWhip; } else { e.Row.Cells[12].BackColor = Color.LightGreen; }
            //increment the totals
            intCol1 += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "SumOfAPBudget"));
            intCol2 += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "SumOfAPFYView"));
            intCol3 += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "SumOfAPVariance"));
            intCol4 += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "SumOfTCCBudget"));
            intCol5 += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "SumOfTCCFYView"));
            intCol6 += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "SumOfTCCVariance"));
            intCol7 += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "SumOfFYView"));
            intTotal += intCurrent;
            //highlight on hover
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            e.Row.Attributes["onclick"] = cs.GetPostBackClientHyperlink(this.grd, "Select$" + e.Row.RowIndex);
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[2].Text = "Totals";
            //display the totals
            e.Row.Cells[5].Text = intCol1.ToString();
            e.Row.Cells[6].Text = intCol2.ToString();
            e.Row.Cells[7].Text = intCol3.ToString();
            e.Row.Cells[8].Text = intCol4.ToString();
            e.Row.Cells[9].Text = intCol5.ToString();
            e.Row.Cells[10].Text = intCol6.ToString();
            e.Row.Cells[11].Text = intCol7.ToString();
            e.Row.Cells[12].Text = intTotal.ToString();
            //colour code the cell 
            if (intTotal < 0) { e.Row.Cells[12].BackColor = Color.PapayaWhip; } else { e.Row.Cells[12].BackColor = Color.LightGreen; }
        }
        
        ////the following Akira lines are to increase the height of the top row so it does not disappear behind the fixed header
        //if (e.Row.RowType == DataControlRowType.Header)
        //{
        //    e.Row.Attributes.Add("Id", "GridHeader");
        //}
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    if (e.Row.RowIndex == 0)
        //        /*e.Row.Height = x.HeaderRow.Height;*/
        //        e.Row.Attributes.Add("Id", "GridFirstRow");
        //        /*e.Row.Style.Add("height", headerHeight);*/
        //        e.Row.Style.Add("vertical-align", "bottom");
        //        /*e.Row.Style.Add("position", "relative");*/
        //}
    }

#endregion

#region dropdowns

    protected void FillBudgetResponsibilityArea()
    //fill BudgetResponsibilityArea dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillBudgetResponsibilityArea";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        //cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboBudgetResponsibilityArea;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillCustomer()
    //fill customer dropdownlist for current user
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillCustomer";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        //cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        cmd.Parameters.AddWithValue("@IncludeInactive", 1);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboCustomer;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillBrand()
    //fill brand dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillBrandActive";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        //cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        cmd.Parameters.AddWithValue("@IncludeInactive", 0);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboBrand;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillCategory()
    //fill category dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillCategoryActive";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        //cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        cmd.Parameters.AddWithValue("@IncludeInactive", 0);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboCategory;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillYear()
    //fill year dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillYear";
        cmd.Parameters.AddWithValue("@ZeroValueText", "[None]");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboYear;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillPulsingPlanPriority()
    //fill project type dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillPulsingPlanPriority";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboPulsingPlanPriority;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillPulsingPlanPriorityMatchType()
    //populate the view mode dropdown
    {
        cboPulsingPlanPriorityMatchType.Items.Add(new ListItem("Equal To", "1"));
        cboPulsingPlanPriorityMatchType.Items.Add(new ListItem("Not Equal To", "2"));
    }

    protected void cboBudgetResponsibilityArea_TextChanged(object sender, EventArgs e)
    {
        Session["SelectedBudgetResponsibilityAreaID"] = cboBudgetResponsibilityArea.SelectedValue;
        UpdateUserSelections();
        SelectFirstRow();
    }

    protected void cboCustomer_TextChanged(object sender, EventArgs e)
    {
        Session["SelectedCustomerID"] = cboCustomer.SelectedValue;
        UpdateUserSelections();
        SelectFirstRow();
    }

    protected void cboBrand_TextChanged(object sender, EventArgs e)
    {
        Session["SelectedBrandID"] = cboBrand.SelectedValue;
        UpdateUserSelections();
        SelectFirstRow();
    }

    protected void cboCategory_TextChanged(object sender, EventArgs e)
    {
        Session["SelectedCategoryID"] = cboCategory.SelectedValue;
        UpdateUserSelections();
        SelectFirstRow();
    }

    protected void cboYear_TextChanged(object sender, EventArgs e)
    {
        Session["SelectedYear"] = cboYear.SelectedValue;
        SelectFirstRow();
    }

    protected void cboPulsingPlanPriority_TextChanged(object sender, EventArgs e)
    {
        UpdateUserSelections();
        SelectFirstRow();
    }

    protected void cboPulsingPlanPriorityMatchType_TextChanged(object sender, EventArgs e)
    {
        UpdateUserSelections();
        SelectFirstRow();
    }

#endregion
}
