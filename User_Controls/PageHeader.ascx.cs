﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Controls_PageHeader : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

   /* public void Populate(int intActivityID)
    {
        DataTable tbl = SOPActivityDataAccess.SelectActivity(intActivityID);
        DataRow r = tbl.Rows[0];
        ProjectName.Text = r["ProjectName"].ToString();
        CustomerName.Text = r["CustomerName"].ToString();
        IONumber.Text = r["IONumber"].ToString();
        ProjectYear.Text = r["ProjectYear"].ToString();
        Label11.Text = "Edit Promo";
    }*/

    public void PopulateFromProject(int intProjectToCustomerID)
    {
        DataTable tbl = ProjectDataAccess.SelectProjectToCustomer(Convert.ToInt32(Session["SelectedProjectToCustomerID"]));
        DataRow r = tbl.Rows[0];
        ProjectName.Text = r["ProjectName"].ToString();
        CustomerName.Text = r["CustomerName"].ToString();
        IONumber.Text = r["IONumber"].ToString();
        ProjectYear.Text = r["ProjectYear"].ToString();
    }

}