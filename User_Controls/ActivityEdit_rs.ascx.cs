﻿using System;
using System.Data.SqlClient;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.IO;
using System.Drawing;

public partial class User_Controls_ActivityEdit_rs : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //add onchange events
            ProjectMonth.Attributes.Add("onchange", "Javascript:showPhasing();");
            ActivityName.Attributes.Add("onblur", "Javascript:copyToShortText();");
            //populate the drop-downs in edit area
            FillMonth();
            FillYesNo(CommittedOverrider);
            FillYesNo(PromoIndicator);
            FillSpendType();
        }
        else
        {
            //populate the header values
            DataTable tbl = ProjectDataAccess.SelectProjectToCustomer(Convert.ToInt32(Session["SelectedProjectToCustomerID"]));
            DataRow r = tbl.Rows[0];
            lblIONumber.Text = r["IONumber"].ToString();
            lblProjectName.Text = r["ProjectName"].ToString();
            lblCustomerName.Text = r["CustomerName"].ToString();
            lblProjectYear.Text = r["ProjectYear"].ToString();
            lblBudgetResponsibilityAreaName.Text = r["BudgetResponsibilityAreaName"].ToString();
            lblBudgetResponsibilityAreaID.Text = r["BudgetResponsibilityAreaID"].ToString();
            lblCurrencyCode.Text = r["CurrencyCode"].ToString().ToUpper().Replace("NULL", "");
        
            String selectedSpendType = cboSpendType.SelectedValue;
            cboSpendType.Items.Clear();
            FillSpendType();
            //selectedSpendType = "1"; (added by AH 13/07/15 to get over runtime error on first use)
            cboSpendType.SelectedValue = selectedSpendType;
        }
        //FillData();
    }

    private void FillData()
    {
        DataTable dt = new DataTable();
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "GetFileDetailsUsingPOID";
        cmd.Parameters.AddWithValue("@POID", Int32.Parse(POID.Text));
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        dt = DataAccess.ExecuteSelectCommand(cmd);

        //if (dt.Rows.Count > 0)
        //{
            GridViewFiles.DataSource = dt;
            GridViewFiles.DataBind();
        //}
    }



    protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
    //if ProjectMonth is 'Phased' check that the monthly breakdown matches the quantity * price entered
    //seems to fire even if requiredfieldvalidation has failed (because of IsChanged() function), so check for correct entries in fields first
    //amended 27/03/12 with big number check
    {
        if (ItemQuantity.Text == String.Empty || UnitPrice.Text == String.Empty)
        {
            args.IsValid = false;
        }
        else
        {
            if ((Convert.ToInt32(ItemQuantity.Text) * Convert.ToDecimal(UnitPrice.Text)) > 100000000)
            {
                args.IsValid = false;
            }
            else
            {
                if (ProjectMonth.SelectedValue == "13")
                {
                    if (Convert.ToInt32(Convert.ToInt32(ItemQuantity.Text) * Convert.ToDecimal(UnitPrice.Text)) == SumMonthlyBreakdown())
                    {
                        args.IsValid = true;
                    }
                    else
                    {
                        args.IsValid = false;
                    }
                }
                else
                {
                    args.IsValid = true;
                }
            }
        }
    }

    string filename = string.Empty;

    protected void DownloadLink_click(object sender, CommandEventArgs e)
    {
        //Response.Write(e.CommandArgument);
        Download(e.CommandArgument.ToString());
    }

    private void Download(string fileName)
    {

        //string strFilename = FileUpload1.FileName;
        string strFilePath = Server.MapPath("Uploads/" + fileName);
        //  FileUpload1.SaveAs(strFilePath);
        // lblFilePath.Text = strFilePath;
        Response.Clear();
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);
        Response.WriteFile(strFilePath);
        Response.Flush();
        Response.End();
    }


    #region properties

    public string ActivityIDValue
    {
        get
        {
            return ActivityID.Text;
        }
    }

    public string ChangedValue
    {
        set { txtChanged.Value = value; }
    }

    public string ProjectMonthValue
    {
        get { return ProjectMonth.SelectedValue; }
    }

    public string SupplierIDValue
    {
        set
        {
            SupplierID.Enabled = false;
            SupplierID.Text = value;
            txtChanged.Value = "1";
            if (value != "")
            {
                DataTable tbl = SupplierDataAccess.SelectSupplier(Convert.ToInt32(value));
                if (tbl.Rows.Count > 0) { SupplierName.Text = tbl.Rows[0]["SupplierName"].ToString(); }
            }
        }
    }

    public string ActivityTypeIDValue
    {
        get
        {
            return ActivityTypeID.Text;
        }
        set
        {
            ActivityTypeID.Enabled = false;
            ActivityTypeID.Text = value;
            txtChanged.Value = "1";
            if (value != "")
            {
                ActivityType a = ActivityTypeDataAccess.SelectActivityType(Convert.ToInt32(value));
                ActivityTypeName.Text = a.ActivityTypeName;
            }
        }
    }

    public string POIDText
    {
        get { return POID.Text; }
    }

    public string RebateIDText
    {
        get { return RebateID.Text; }
    }

    public Activity SelectActivity
    //create an Activity object from the values in the controls on the page
    {
        get
        {
            Activity a = new Activity();
            a.ActivityID = Convert.ToInt32(ActivityID.Text);
            a.ProjectToCustomerID = Convert.ToInt32(Session["SelectedProjectToCustomerID"]);
            a.ActivityName = ActivityName.Text;
            a.ShortText = ShortText.Text;
            a.ItemQuantity = Convert.ToInt32(ItemQuantity.Text);
            a.UnitPrice = Decimal.Parse(UnitPrice.Text);
            //a.UoM = UoM.Text;
            a.ProjectMonth = ProjectMonth.SelectedValue;
            a.MonthRequired = ProjectMonth.SelectedValue;
            if (ProjectMonth.SelectedValue != "13")
            {
                a.MonthShortName = ProjectMonth.SelectedItem.ToString();
            }
            a.POID = POID.Text;
            a.RebateID = RebateID.Text;
            a.DocumentNumber = DocumentNumber.Text;
            //a.POItemNo = POItemNo.Text;
            a.ActivityTypeID = Convert.ToInt32(ActivityTypeID.Text);
            a.ActivityTypeName = ActivityTypeName.Text;
            a.SpendTypeID = int.Parse(cboSpendType.SelectedValue);
            if (SupplierID.Text == String.Empty) { a.SupplierID = String.Empty; } else { a.SupplierID = SupplierID.Text; }
            a.SupplierName = SupplierName.Text;
            a.SupplierRefNo = SupplierRefNo.Text;
            if (CommittedOverrider.SelectedValue == "2")
            {
                a.CommittedOverrider = "X";
            }
            else
            {
                a.CommittedOverrider = "";
            }
            if (PromoIndicator.SelectedValue == "2")
            {
                a.PromoIndicator = "X";
            }
            else
            {
                a.PromoIndicator = "";
            }
            a.Comments = Comments.Text;
            a.LastUpdatedBy = Session["CurrentUserName"].ToString();
            a.CreatedBy = Session["CurrentUserName"].ToString();
            if (Month01.Text != "") { a.Month01 = Convert.ToInt32(Convert.ToDecimal(Month01.Text)).ToString(); } else { a.Month01 = ""; }
            if (Month02.Text != "") { a.Month02 = Convert.ToInt32(Convert.ToDecimal(Month02.Text)).ToString(); } else { a.Month02 = ""; }
            if (Month03.Text != "") { a.Month03 = Convert.ToInt32(Convert.ToDecimal(Month03.Text)).ToString(); } else { a.Month03 = ""; }
            if (Month04.Text != "") { a.Month04 = Convert.ToInt32(Convert.ToDecimal(Month04.Text)).ToString(); } else { a.Month04 = ""; }
            if (Month05.Text != "") { a.Month05 = Convert.ToInt32(Convert.ToDecimal(Month05.Text)).ToString(); } else { a.Month05 = ""; }
            if (Month06.Text != "") { a.Month06 = Convert.ToInt32(Convert.ToDecimal(Month06.Text)).ToString(); } else { a.Month06 = ""; }
            if (Month07.Text != "") { a.Month07 = Convert.ToInt32(Convert.ToDecimal(Month07.Text)).ToString(); } else { a.Month07 = ""; }
            if (Month08.Text != "") { a.Month08 = Convert.ToInt32(Convert.ToDecimal(Month08.Text)).ToString(); } else { a.Month08 = ""; }
            if (Month09.Text != "") { a.Month09 = Convert.ToInt32(Convert.ToDecimal(Month09.Text)).ToString(); } else { a.Month09 = ""; }
            if (Month10.Text != "") { a.Month10 = Convert.ToInt32(Convert.ToDecimal(Month10.Text)).ToString(); } else { a.Month10 = ""; }
            if (Month11.Text != "") { a.Month11 = Convert.ToInt32(Convert.ToDecimal(Month11.Text)).ToString(); } else { a.Month11 = ""; }
            if (Month12.Text != "") { a.Month12 = Convert.ToInt32(Convert.ToDecimal(Month12.Text)).ToString(); } else { a.Month12 = ""; }

            return a;
        }
    }

    #endregion

    #region methods

    public void PopulateForm(Activity a)
    //prepare form for editing an existing record
    {
        lblHeader.Text = "Edit Activity";
        //enable controls unless read only - amended 05/12/12 to check user's permissions here too
        if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 35 && Common.CheckAuthorisations(Convert.ToInt32(Session["SelectedProjectToCustomerID"]), Session["CurrentUserName"].ToString()) > 0)
        {
            ToggleControlState(true);
        }

        //if Coupons, enable Document number field - updated 19/12/12
        if (a.SpendTypeID == 3) { DocumentNumber.Enabled = true; } else { DocumentNumber.Enabled = false; }
        //populate fields
        ActivityID.Text = a.ActivityID.ToString();
        ActivityName.Text = a.ActivityName;
        ShortText.Text = a.ShortText;
        ItemQuantity.Text = a.ItemQuantity.ToString();
        UnitPrice.Text = String.Format("{0:0.0000}", a.UnitPrice);
        //UoM.Text = a.UoM;
        ProjectMonth.SelectedValue = a.ProjectMonth;
        POID.Text = a.POID;
        RebateID.Text = a.RebateID;
        DocumentNumber.Text = a.DocumentNumber;
        //POItemNo.Text = a.POItemNo;
        cboSpendType.SelectedValue = a.SpendTypeID.ToString();
        SpendTypeID.Text = cboSpendType.SelectedValue.ToString();
        SpendTypeName.Text = cboSpendType.SelectedItem.ToString();
        cboSpendType.Enabled = false;
        ActivityTypeID.Text = a.ActivityTypeID.ToString();
        ActivityTypeName.Text = a.ActivityTypeName;
        SupplierID.Text = a.SupplierID;
        if (a.SupplierID != "")
        { SupplierName.Text = a.SupplierName; }
        else { SupplierName.Text = ""; }
        cmdSupplierLookup.Enabled = (a.SpendTypeID == 1 && a.POID == String.Empty);
        SupplierRefNo.Text = a.SupplierRefNo;
        if (a.CommittedOverrider == "X")
        {
            CommittedOverrider.SelectedValue = "2";
        }
        else
        {
            CommittedOverrider.SelectedValue = "1";
        }
        if (a.PromoIndicator == "X")
        {
            PromoIndicator.SelectedValue = "2";
        }
        else
        {
            PromoIndicator.SelectedValue = "1";
        }
        Comments.Text = a.Comments;
        Month01.Text = a.Month01;
        Month02.Text = a.Month02;
        Month03.Text = a.Month03;
        Month04.Text = a.Month04;
        Month05.Text = a.Month05;
        Month06.Text = a.Month06;
        Month07.Text = a.Month07;
        Month08.Text = a.Month08;
        Month09.Text = a.Month09;
        Month10.Text = a.Month10;
        Month11.Text = a.Month11;
        Month12.Text = a.Month12;

        txtChanged.Value = "0";
        Session["Mode"] = "Edit";
        UploadItemsVisiblity();
    }

    public void UploadItemsVisiblity()
    {
        string POIDCheck = "Invalid";
        try
        {
            POIDCheck = DataAccess.ADOLookup("POID", "tblPOHeader", "POID = " + POID.Text);
        }
        catch (Exception ex)
        {

        }

        if (POIDCheck.Equals("Invalid"))
        {
            Title.Visible = false;
            FileUploadActivity.Visible = false;
            btnUpload.Visible = false;
            GridViewFiles.Visible = false;
            lblUploadFile.Visible = false;
        }
        else
        {
            Title.Visible = true;
            FileUploadActivity.Visible = true;
            btnUpload.Visible = true;
            GridViewFiles.Visible = true;
            lblUploadFile.Visible = true;

            FillData();
        }

        lblUploadFile.Text = "";
    }

    public string UpdateRecord()
    {
        if (Page.IsValid)
        {
            double dblOldValue = 0;
            double dblOldPOValue = 0;
            //if Spend Type is A&P and PONumber already allocated, get current value of activity and PO - before they are updated
            if (DocumentNumber.Text != String.Empty && SpendTypeID.Text == "1")
            {
                DataTable tblActivity = ActivityDataAccess.SelectActivityToTable(Convert.ToInt32(ActivityID.Text));
                dblOldValue = Convert.ToDouble(tblActivity.Rows[0]["UnitPrice"]) * Convert.ToInt32(tblActivity.Rows[0]["ItemQuantity"]);
                dblOldPOValue = Convert.ToDouble(POHeaderDataAccess.GetPOValue(Convert.ToInt32(POID.Text)));
            }
            //update activity
            int intRowsAffected = ActivityDataAccess.UpdateActivity_rs(SelectActivity, 1);
            //if Spend Type is A&P, POValue has changed and PONumber already allocated, create a PO change record
            //***want to move to SP***
            if (DocumentNumber.Text != String.Empty && SpendTypeID.Text == "1")
            {
                //get new value of activity
                double dblNewValue = Convert.ToDouble(UnitPrice.Text) * Convert.ToInt32(ItemQuantity.Text);
                //insert POChange record
                if (dblOldValue != dblNewValue)
                {
                    //get new value of PO
                    double dblChange = dblNewValue - dblOldValue;
                    double dblNewPOValue = dblOldPOValue + dblChange;
                    POHeaderDataAccess.InsertPOChange(Convert.ToInt32(POID.Text), dblOldPOValue, dblNewPOValue, Session["CurrentUserName"].ToString());
                }
            }
            ClearForm();
            return "[none]";
        }
        else
        {
            return "Page Invalid";
        }
    }

    public string CreateRecord()
    {
        if (Page.IsValid)
        {
            ActivityDataAccess.InsertActivity_rs(SelectActivity);
            ClearForm();
            return "[none]";
        }
        else
        {
            return "Page Invalid";
        }
    }

    protected void ToggleControlState(bool blnState)
    //enable/disable controls
    {
        foreach (Control c in Controls)
        {
            if (c != ActivityID && c != POID && c != SpendTypeName && c != DocumentNumber && c != ActivityTypeName && c != SupplierName)
            {
                if (c is TextBox) { ((TextBox)c).Enabled = blnState; } else { if (c is DropDownList) { ((DropDownList)c).Enabled = blnState; } }
            }
        }
        //required to ensure "readers" cannot change phased values
        Month01.Enabled = blnState;
        Month02.Enabled = blnState;
        Month03.Enabled = blnState;
        Month04.Enabled = blnState;
        Month05.Enabled = blnState;
        Month06.Enabled = blnState;
        Month07.Enabled = blnState;
        Month08.Enabled = blnState;
        Month09.Enabled = blnState;
        Month10.Enabled = blnState;
        Month11.Enabled = blnState;
        Month12.Enabled = blnState;
    }

    public void ClearControls()
    {
        foreach (Control c in Controls)
        {
            if (c is TextBox) { ((TextBox)c).Text = ""; }
            //if (c is DropDownList) { ((DropDownList)c).SelectedValue = "0"; }    remmed out 08/10/14 to solve value does not exist in list error, replaced by ProjectMonth specific line below
        }
        ProjectMonth.SelectedValue = "0";
        //data labels
        SupplierID.Text = String.Empty;
        SupplierName.Text = String.Empty;
        ActivityTypeID.Text = String.Empty;
        ActivityTypeName.Text = String.Empty;
        //these are not at the top level so need clearing explicitly 
        Month01.Text = "";
        Month02.Text = "";
        Month03.Text = "";
        Month04.Text = "";
        Month05.Text = "";
        Month06.Text = "";
        Month07.Text = "";
        Month08.Text = "";
        Month09.Text = "";
        Month10.Text = "";
        Month11.Text = "";
        Month12.Text = "";
    }

    public void ClearForm()
    {
        ClearControls();
        ToggleControlState(false);
        ActivityName.Focus();
    }

    public void NewRecord(string strSpendTypeID, string strSpendTypeName)
    //prepare form for new record entry
    {
        lblHeader.Text = "New Activity";
        ClearControls();
        ToggleControlState(true);
        ActivityID.Text = "0";
        cmdSupplierLookup.Enabled = true;
        if (cboSpendType.Items.Count == 1)
        {
            cboSpendType.Enabled = false;
        }
        else
        {
            cboSpendType.Enabled = true;
        }
        UploadItemsVisiblity();
    }

    public void NewRecord()
    //prepare form for new record entry
    {
        lblHeader.Text = "New Activity";
        ClearControls();
        ToggleControlState(true);
        cmdSupplierLookup.Enabled = true;
        ActivityID.Text = "0";
        cboSpendType.Enabled = true;
        UploadItemsVisiblity();
    }

    protected int SumMonthlyBreakdown()
    //sum the values in the month boxes allowing for blanks
    {
        int intTotal = 0;
        if (Month01.Text != "")
        {
            intTotal += Convert.ToInt32(Convert.ToDecimal(Month01.Text));
        }
        if (Month02.Text != "")
        {
            intTotal += Convert.ToInt32(Convert.ToDecimal(Month02.Text));
        }
        if (Month03.Text != "")
        {
            intTotal += Convert.ToInt32(Convert.ToDecimal(Month03.Text));
        }
        if (Month04.Text != "")
        {
            intTotal += Convert.ToInt32(Convert.ToDecimal(Month04.Text));
        }
        if (Month05.Text != "")
        {
            intTotal += Convert.ToInt32(Convert.ToDecimal(Month05.Text));
        }
        if (Month06.Text != "")
        {
            intTotal += Convert.ToInt32(Convert.ToDecimal(Month06.Text));
        }
        if (Month07.Text != "")
        {
            intTotal += Convert.ToInt32(Convert.ToDecimal(Month07.Text));
        }
        if (Month08.Text != "")
        {
            intTotal += Convert.ToInt32(Convert.ToDecimal(Month08.Text));
        }
        if (Month09.Text != "")
        {
            intTotal += Convert.ToInt32(Convert.ToDecimal(Month09.Text));
        }
        if (Month10.Text != "")
        {
            intTotal += Convert.ToInt32(Convert.ToDecimal(Month10.Text));
        }
        if (Month11.Text != "")
        {
            intTotal += Convert.ToInt32(Convert.ToDecimal(Month11.Text));
        }
        if (Month12.Text != "")
        {
            intTotal += Convert.ToInt32(Convert.ToDecimal(Month12.Text));
        }
        return intTotal;
    }

    #endregion

    #region supplierlookup

    protected void cmdSaveSupplier_Click(object sender, EventArgs e)
    {
        //Populate supplier ID and supplier name in ActivityEdit screen (and set txtChanged flag to true)
        SupplierIDValue = SupplierGrid1.SelectedValue;
    }

    protected void cmdCancelSupplier_Click(object sender, EventArgs e)
    {
        //temporary fix - isChanged flag always set to true when exiting this screen (in case another change has already been made)
        ChangedValue = "1";
    }

    protected void cmdSupplierLookup_Click(object sender, EventArgs e)
    {
        //Prepare the grid
        //SupplierGrid1.SelectFilters();
        SupplierGrid1.PopulateGrid(); 
        mp1.Show();
    }

    #endregion

    #region activitytypelookup

    protected void cmdSaveActivityType_Click(object sender, EventArgs e)
    {
        //Populate activitytype ID and activitytype name in ActivityEdit screen (and set txtChanged flag to true)
        ActivityTypeIDValue = ActivityTypeGrid1.SelectedValue;
    }

    protected void cmdCancelActivityType_Click(object sender, EventArgs e)
    {
        //temporary fix - isChanged flag always set to true when exiting this screen
        ChangedValue = "1";
    }

    protected void cmdActivityTypeLookup_Click(object sender, EventArgs e)
    {
        //Select the spend type and BRA (added 24/02/13)
        SpendTypeID.Text = cboSpendType.SelectedValue.ToString();
        SpendTypeName.Text = cboSpendType.SelectedItem.ToString();
        ActivityTypeGrid1.SelectFilters(SpendTypeID.Text, lblBudgetResponsibilityAreaID.Text);
        mp2.Show();
    }

    #endregion

    #region dropdowns

    public DataTable SelectActivityTypes(int intBudgetResponsibilityAreaID)
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "SelectActivityTypes";
        cmd.Parameters.AddWithValue("@BudgetResponsibilityAreaID", intBudgetResponsibilityAreaID);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    protected void FillSpendType()
    //populate the SpendType dropdownlist
    {
        //Get a data table object containing the values
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillSpendType_ActivityEdit";
        cmd.Parameters.AddWithValue("@ZeroValueText", "[None]");
        cmd.Parameters.AddWithValue("@BudgetResponsibilityAreaID", lblBudgetResponsibilityAreaID.Text);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboSpendType;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillMonth()
    //fill months dropdownlist
    {
        //Get a data table object containing the values
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillMonth";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = ProjectMonth;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillYesNo(DropDownList d)
    //populate generic Yes/No dropdown with values 1=No and 2=Yes
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ge_FillYesNo";
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        d.DataSource = tbl;
        d.DataBind();
    }

    #endregion


    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (FileUploadActivity.HasFile)
        {
            string POIDCheck = "Invalid";
            try
            {
                POIDCheck = DataAccess.ADOLookup("POID", "tblPOHeader", "POID = " + POID.Text);
            }
            catch (Exception ex)
            {

            }
            try
            {
                if (POIDCheck.Equals("Invalid"))
                {
                    lblUploadFile.Text = "POID is Invalid";
                    lblUploadFile.ForeColor = Color.Red;
                }
                else
                {
                    string strFiletype = FileUploadActivity.PostedFile.ContentType;
                    int intFilelength = FileUploadActivity.PostedFile.ContentLength;
                    int FileCount = 0;

                    SqlCommand cmd = DataAccess.CreateCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "FileCountFromPOID";
                    // Response.Write(POID.Text);
                    cmd.Parameters.AddWithValue("@POID", Int32.Parse(POID.Text));
                    DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
                    FileCount = Int32.Parse(tbl.Rows[0]["FileCount"].ToString());
                    // Response.Write(FileCount.ToString());

                    if (FileCount < 5)
                    {
                        if (intFilelength > 1024 * 1024 * 10)
                        {
                            lblUploadFile.Text = "Maximum file size 10MB.";
                            lblUploadFile.ForeColor = Color.Red;
                        }
                        else
                        {
                            filename = Path.GetFileName(FileUploadActivity.PostedFile.FileName);
                            string filename_wo_extension = filename.Substring(0, filename.LastIndexOf(".") - 1);
                            string extension = filename.Substring(filename.LastIndexOf("."));
                            filename = filename_wo_extension + "_" + POID.Text + "_" + DateTime.Now.ToString().Replace(":", "_").Replace(" ", "_").Replace("/", "_") + extension;
                            FileUploadActivity.SaveAs(Server.MapPath("Uploads/" + filename));
                            
                            //SqlCommand cmd2 = DataAccess.CreateCommand();
                            //cmd2.CommandType = CommandType.StoredProcedure;
                            //cmd2.CommandText = "SaveFileDetailsUsingPOID";
                            //cmd2.Parameters.AddWithValue("@FileName", filename);
                            //cmd2.Parameters.AddWithValue("@POID", Int32.Parse(POID.Text));
                            //int fileID = DataAccess.ExecuteScalarReturnIdentity(cmd2);
                            //GridViewFiles.DataBind();

                            SqlCommand cmd3 = DataAccess.CreateCommand();
                            cmd3.CommandType = CommandType.StoredProcedure;
                            cmd3.CommandText = "SaveFileDetailsTempUsingPOID";
                            cmd3.Parameters.AddWithValue("@FileName", filename);
                            cmd3.Parameters.AddWithValue("@POID", Int32.Parse(POID.Text));
                            cmd3.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());

                            DataAccess.ExecuteScalar(cmd3);

                            lblUploadFile.Text = "File uploaded sucessfully.";
                            lblUploadFile.ForeColor = Color.Green;
                        }
                    }
                    else
                    {
                        lblUploadFile.Text = "Maximum of 5 file uploads allowed Or Other users might have unsaved changes on this PO.";
                        lblUploadFile.ForeColor = Color.Red;
                    }
                }

            }
            catch (Exception ex)
            {
                lblUploadFile.Text = "ERROR: " + ex.Message.ToString();
                lblUploadFile.ForeColor = Color.Red;
            }
        }
        FillData();

    }



    protected void DeleteLink_Click(object sender, CommandEventArgs e)
    {
        try
        {
            //Delete file from Server
            string fileName = Server.MapPath("Uploads/" + e.CommandArgument.ToString());
           // File.Delete(fileName);

            //Procedure to delete from table
            SqlCommand cmd = DataAccess.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "DeleteFileDetailsFromPOID";
            cmd.Parameters.AddWithValue("@FileName", e.CommandArgument.ToString());
            cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());

            DataTable dtf = DataAccess.ExecuteSelectCommand(cmd);
            string filename = dtf.Rows[0]["FileName"].ToString();
            if (!filename.Equals("[none]"))
            {
                fileName = Server.MapPath("Uploads/" + filename);
                File.Delete(fileName);
            }
            lblUploadFile.Text = "File deleted successfully";
            lblUploadFile.ForeColor = Color.Green;
            FillData();
        }
        catch (Exception ex)
        {
            lblUploadFile.Text = "Delete failed with the following error" + ex.Message.ToString();
            lblUploadFile.ForeColor = Color.Red;
        }

    }
}