﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WorklistSummary_rs.ascx.cs" Inherits="WorklistSummary_rs" %>
<div id="spacer"></div>
    <table border="0">
    <tr><td>
        <asp:Label ID="lblTitle" runat="server" Text="Worklist Name" Font-Bold="true"></asp:Label>
    </td><td>
        <asp:Label ID="lblWorklistCount" runat="server" style="text-align:right" Width="100px" Text="Pending Items" Font-Bold="true"/>&nbsp;&nbsp;
    </td></tr>
<%if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 8)
{ %>
    <tr><td>
        <asp:Label ID="lblCreatePR" runat="server" Text="Create PR"></asp:Label>
    </td><td>
        <asp:Label ID="txtWorklist5" runat="server" CssClass="label_text" BorderStyle="None" style="text-align:right" Width="100px"/>&nbsp;&nbsp;
    </td></tr>
        <tr><td>
        <asp:Label ID="lblAwaitingPONo" runat="server" Text="Awaiting PO No"></asp:Label>
    </td><td>
        <asp:Label ID="txtWorklist1" runat="server" CssClass="label_text" BorderStyle="None" style="text-align:right" Width="100px"/>&nbsp;&nbsp;
    </td></tr><tr><td>
        <asp:Label ID="lblChangeToPo" runat="server" Text="Change To PO"></asp:Label>
    </td><td>   
        <asp:Label ID="txtWorklist3" runat="server" CssClass="label_text" BorderStyle="None" style="text-align:right" Width="100px"/>&nbsp;&nbsp;
    </td></tr><tr>
<% } %>
    <td>
        <asp:Label ID="lblRequestReceipt" runat="server" Text="Request Receipt"></asp:Label>
    </td><td>
        <asp:Label ID="txtWorklist2" runat="server" CssClass="label_text" BorderStyle="None" style="text-align:right" Width="100px"/>&nbsp;&nbsp;
    </td></tr><tr>
<%if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 8)
{ %>
    <td>
        <asp:Label ID="lblActionReceipt" runat="server" Text="Action Receipt"></asp:Label>
    </td><td>
        <asp:Label ID="txtWorklist4" runat="server" CssClass="label_text" BorderStyle="None" style="text-align:right" Width="100px"/>&nbsp;&nbsp;
    </td></tr><tr>
<% } %>
    <td>
        <asp:Label ID="lblApproveRebate" runat="server" Text="Approve Rebate"></asp:Label>
    </td><td>
        <asp:Label ID="txtWorklist7" runat="server" CssClass="label_text" BorderStyle="None" style="text-align:right" Width="100px"/>&nbsp;&nbsp;
    </td></tr><tr><td>
        <asp:Label ID="lblApproveReallocation" runat="server" Text="Approve Reallocation"></asp:Label>
    </td><td>
        <asp:Label ID="txtWorklist9" runat="server" CssClass="label_text" BorderStyle="None" style="text-align:right" Width="100px"/>&nbsp;&nbsp;
    </td></tr>

        <tr><td>
        <asp:Label ID="lblReallocationAwaitApproval" runat="server" Text="Reallocation Awaiting Approval"></asp:Label>
    </td><td>
        <asp:Label ID="txtWorklist10" runat="server" CssClass="label_text" BorderStyle="None" style="text-align:right" Width="100px"/>&nbsp;&nbsp;
    </td></tr>
        <tr><td>
<%if (Convert.ToInt16(Session["CurrentUserLevelID"]) == 35)
{ %>
    
        <asp:Label ID="lblCreateRebate" runat="server" Text="Create Rebate"></asp:Label>
    </td><td>
        <asp:Label ID="txtWorklist8" runat="server" CssClass="label_text" BorderStyle="None" style="text-align:right" Width="100px"/>&nbsp;&nbsp;
    </td></tr><tr><td>
        <asp:Label ID="lblChangeToRebate" runat="server" Text="Change To Rebate"></asp:Label>
    </td><td>
        <asp:Label ID="txtWorklist6" runat="server" CssClass="label_text" BorderStyle="None" style="text-align:right" Width="100px"/>&nbsp;&nbsp;
    </td>
<% } %>
    </tr>
        <tr><td>
        <asp:Label ID="lblRequestPR_Rbte" runat="server" Text="Request PR/Rbte"></asp:Label>
    </td><td>
        <asp:Label ID="txtWorklist11" runat="server" CssClass="label_text" BorderStyle="None" style="text-align:right" Width="100px"/>&nbsp;&nbsp;
    </td></tr>
  

        <tr><td>
        <asp:Label ID="lblFinanceApproval" runat="server" Text="Finance Approval"></asp:Label>
    </td><td>
        <asp:Label ID="txtWorklist12" runat="server" CssClass="label_text" BorderStyle="None" style="text-align:right" Width="100px"/>&nbsp;&nbsp;
    </td></tr>
    </table>
