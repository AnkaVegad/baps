﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Controls_MonthlyTotals : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void PopulateGrid(int intProjectID, int intCustomerID)
    {
        //populate the monthly totals grid
        //grdMonthly.DataSource = ActivityDataAccess.SelectActivityMonthlySummary(intProjectID, intCustomerID);
        ProjectID.Text = intProjectID.ToString();
        CustomerID.Text = intCustomerID.ToString();
        grdMonthly.DataBind();
    }

    protected void grdMonthly_PageIndexChanged(object sender, EventArgs e)
    {

    }

    protected void grdMonthly_Sorted(object sender, EventArgs e)
    {

    }
}

