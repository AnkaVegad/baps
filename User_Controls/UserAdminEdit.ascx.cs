﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Controls_UserAdminEdit : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //populate selection dropdowns
            FillActiveIndicator();
            FillUserLevel();
            FillBudgetResponsibilityArea();
            FillCustomer();
            FillBrand();
        }
    }

#region properties

    public string HeaderText
    {
        set { lblHeader.Text = value; }
    }

#endregion

#region gridevents

    protected void grdAuth_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            e.Row.Attributes["onclick"] = cs.GetPostBackClientHyperlink(this.grdAuth, "Select$" + e.Row.RowIndex);
        }
    }

#endregion
    
#region methods

    public void PopulateForm(int intUserID)
    {
        //get data for selected user
        string strUserName = Common.ADOLookup("UserName", "tblUser", "UserID = " + intUserID);
        DataTable tbl = UserDataAccess.SelectUser(strUserName);
        DataRow r = tbl.Rows[0];
        //populate fields
        UserID.Text = r["UserID"].ToString();
        UserName.Text = r["UserName"].ToString();
        ActiveIndicator.SelectedValue = r["ActiveIndicator"].ToString();
        UserLevelID.SelectedValue = r["UserLevelID"].ToString();
        DelegateUserName.Text = r["DelegateUserName"].ToString(); 
        //populate authorisations grid
        tbl = UserDataAccess.SelectUserAuthorisations(intUserID);
        grdAuth.DataSource = tbl;
        grdAuth.DataBind();
        Session["SelectedUserAuthItems"] = tbl;
        Session["Mode"] = "Edit";
        UserName.Enabled = false;
    }

    public string UpdateUser()
    //amended 07/12/12 added CategoryID; 18/06/14 added DelegateUserName
    {
        DataTable tbl = (DataTable)Session["SelectedUserAuthItems"];
        //validation check at least one auth item selected
        if (tbl.Rows.Count == 0)
        {
            return "Enter authorisation items.";
        }
        else
        {
            //Main data
            int intMessage = UserDataAccess.UpdateUser(Convert.ToInt32(UserID.Text), Convert.ToInt16(ActiveIndicator.SelectedValue), Convert.ToInt16(UserLevelID.SelectedValue), DelegateUserName.Text, Session["CurrentUserName"].ToString());
            if (intMessage == -1)
            {
                return "Delegate user does not exist.";
            }
            else
            {
                //Authorisations - clear and recreate
                UserDataAccess.ClearUserAuthorisations(Convert.ToInt32(UserID.Text));
                //DataTable tbl = (DataTable)Session["SelectedUserAuthItems"]; ; *** remmed out 19/06/14 as unnecessary ***
                foreach (DataRow r in tbl.Rows)
                {
                    UserDataAccess.InsertUserAuthorisation(Convert.ToInt32(UserID.Text), Convert.ToInt32(r["BudgetResponsibilityAreaID"]), Convert.ToInt32(r["CustomerID"]), Convert.ToInt32(r["BrandID"]), Convert.ToInt32(r["CategoryID"]), Session["CurrentUserName"].ToString());
                }
                //Clear
                ClearControls();
                return "";
            }
        }
    }

    public string CreateUser()
    //amended 07/12/12 added CategoryID; 18/06/14 added DelegateUserName
    {
        DataTable tbl = (DataTable)Session["SelectedUserAuthItems"];
        //validation check at least one auth item selected
        if (tbl.Rows.Count == 0)
        {
            return "Enter authorisation items.";
        }
        else
        {
            //Main data
            int intUserID = UserDataAccess.InsertUser(UserName.Text, Convert.ToInt16(ActiveIndicator.SelectedValue), Convert.ToInt16(UserLevelID.SelectedValue), DelegateUserName.Text, Session["CurrentUserName"].ToString());
            if (intUserID == 0)
            {
                return "User name already exists.";
            }
            else
            {
                if (intUserID == -1)
                {
                    return "Delegate user does not exist.";
                }
                else
                {
                    //Authorisations
                    //DataTable tblSubBrand = (DataTable)Session["SelectedUserAuthItems"]; *** remmed out 18/06/14 as unnecessary ***
                    foreach (DataRow r in tbl.Rows)
                    {
                        UserDataAccess.InsertUserAuthorisation(intUserID, Convert.ToInt32(r["BudgetResponsibilityAreaID"]), Convert.ToInt32(r["CustomerID"]), Convert.ToInt32(r["BrandID"]), Convert.ToInt32(r["CategoryID"]), Session["CurrentUserName"].ToString());
                    }
                    //Clear
                    ClearControls();
                    return "";
                }
            }
        }
    }

    protected void AddUserAuth(int intBudgetResponsibilityAreaID, int intCustomerID, int intBrandID, int intCategoryID)
    //amended 01/02/12 to support new "blank" row for non-Superusers
    //amended 07/12/12 added CategoryID parameter (which in reality indicates whether Brand or Category level)
    {
        DataTable tbl = (DataTable)Session["SelectedUserAuthItems"];

        //loop through tbl to see if record with this key has already been added (to prevent duplication)
        bool duplicate = false;
        string strUserAuthorisationsKey = intBudgetResponsibilityAreaID.ToString() + '-' + intCustomerID.ToString() + '-' + intBrandID.ToString() + intCategoryID.ToString();
        for (int i = tbl.Rows.Count - 1; i >= 0; i--)
        {
            DataRow dr = tbl.Rows[i];
            string dt = dr["UserAuthorisationsKey"].ToString();
            if (dt == strUserAuthorisationsKey)
            {
                duplicate = true;
            }
        }

        if (!duplicate)
        {
            //add the selected item to the table
            DataRow dr = tbl.NewRow();
            dr["UserAuthorisationsKey"] = strUserAuthorisationsKey;
            dr["BudgetResponsibilityAreaID"] = intBudgetResponsibilityAreaID;
            dr["BudgetResponsibilityAreaName"] = Common.ADOLookup("BudgetResponsibilityAreaName", "tblBudgetResponsibilityArea", "BudgetResponsibilityAreaID = " + intBudgetResponsibilityAreaID.ToString());
            dr["CustomerID"] = intCustomerID;
            if (intCustomerID == 0) { dr["CustomerName"] = "<All>"; }
            else
            {
                dr["CustomerName"] = Common.ADOLookup("CustomerName", "tblCustomer", "CustomerID = " + intCustomerID.ToString());
            }
            dr["BrandID"] = intBrandID;
            if (intBrandID == 0) { dr["BrandName"] = "<All>"; }
            else
            {
                if (intCategoryID == 1)
                {
                    dr["BrandName"] = "Brand: " + Common.ADOLookup("BrandName", "tblBrand", "BrandID = " + intBrandID.ToString());
                }
                else
                {
                    dr["BrandName"] = "Category: " + Common.ADOLookup("UKCategoryName", "tblUKCategory", "UKCategoryID = " + intBrandID.ToString());
                }
            }
            dr["CategoryID"] = intCategoryID;
            tbl.Rows.Add(dr);
        }

        grdAuth.DataSource = tbl;
        grdAuth.DataBind();

        //store basket table in session variable
        Session["SelectedUserAuthItems"] = tbl;

        //clear 
        cboBudgetResponsibilityAreaID.SelectedValue = "0";
        //        cboCustomerID.SelectedValue = "0";
        //        cboBrandID.SelectedValue = "0";
        cboCustomerID.SelectedIndex = -1;
        cboBrandID.SelectedIndex = -1;
    }

    protected void RemoveUserAuth()
    //remove the selected user authorisation item from the grid
    {
        try //because an item may not be selected in the grid or the Session variable may not be valid
        {
            //loop through the table and delete if criteria found
            DataTable tbl = (DataTable)Session["SelectedUserAuthItems"];

            //mark record for deletion
            for (int i = tbl.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = tbl.Rows[i];
                string dtitem = dr["UserAuthorisationsKey"].ToString();
                if (dtitem == grdAuth.SelectedValue.ToString())
                {
                    tbl.Rows[i].Delete();
                }
            }

            //delete
            tbl.AcceptChanges();

            grdAuth.DataSource = tbl;
            grdAuth.DataBind();

            //store basket table in session variable
            Session["SelectedUserAuthItems"] = tbl;
        }
        catch { }
    }

    public void ClearControls()
    //remove data from controls
    //amended 01/02/12 to support new "blank" row for non-Superusers
    {
        UserID.Text = "";
        UserName.Text = "";
        UserLevelID.SelectedValue = "-1";
        ActiveIndicator.SelectedValue = "0";

        //Clear authorisations
        grdAuth.DataSource = "";
        grdAuth.DataBind();
        Session["SelectedUserAuthItems"] = null;
        cboBudgetResponsibilityAreaID.SelectedValue = "0";

        
        cboCustomerID.SelectedIndex = -1;
        cboBrandID.SelectedIndex = -1;
    }

    public void NewRecord()
    //amended 07/12/12 added CategoryID
    {
        ClearControls();
        //initialise selected user auth table
        Session["SelectedUserAuthItems"] = null;
        DataTable tbl = new DataTable();
        tbl.Columns.Add("UserAuthorisationsKey");
        tbl.Columns.Add("BudgetResponsibilityAreaID");
        tbl.Columns.Add("BudgetResponsibilityAreaName");
        tbl.Columns.Add("CustomerID");
        tbl.Columns.Add("CustomerName");
        tbl.Columns.Add("BrandID");
        tbl.Columns.Add("BrandName");
        tbl.Columns.Add("CategoryID");
        Session["SelectedUserAuthItems"] = tbl;

        //UserID.Text = "0";

        Session["Mode"] = "New";
        UserName.Enabled = true;
        UserName.Focus();
    }

#endregion

#region dropdowns

    protected void FillActiveIndicator()
    //populate the ActiveIndicator dropdown
    {
        ActiveIndicator.Items.Add(new ListItem("", "0"));
        ActiveIndicator.Items.Add(new ListItem("Yes", "1"));
        ActiveIndicator.Items.Add(new ListItem("No", "2"));
    }

    protected void FillUserLevel()
    //populate the UserLevelID dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "FillUserLevel";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        cmd.Parameters.AddWithValue("@CurrentUserLevelID", Session["CurrentUserlevelID"]);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = UserLevelID;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillBudgetResponsibilityArea()
    //fill BudgetResponsibilityArea dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "FillBudgetResponsibilityArea";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboBudgetResponsibilityAreaID;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillCustomer()
    //fill customer dropdownlist for current user
    //amended 01/02/12 changed from FillCustomer to new sp_ge_FillCustomerUserAdmin
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ge_FillCustomerUserAdmin";
        //cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        //cmd.Parameters.AddWithValue("@IncludeInactive", 0);
        cmd.Parameters.AddWithValue("@CurrentUserLevelID", Convert.ToInt16(Session["CurrentUserLevelID"]));
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboCustomerID;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillBrand()
    //fill brand dropdownlist for current user
    //amended 01/02/12 changed from FillBrand to new sp_ge_FillBrandUserAdmin
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ge_FillBrandUserAdmin";
        //cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        cmd.Parameters.AddWithValue("@CurrentUserLevelID", Convert.ToInt16(Session["CurrentUserLevelID"]));
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboBrandID;
        d.DataSource = tbl;
        d.DataBind();
    }

#endregion

#region subformbuttons

    protected void cmdAddUserAuth_Click(object sender, EventArgs e)
    {
        if (Convert.ToInt32(cboBrandID.SelectedValue) > 90000)
        {
            AddUserAuth(Convert.ToInt32(cboBudgetResponsibilityAreaID.SelectedValue), Convert.ToInt32(cboCustomerID.SelectedValue), Convert.ToInt32(cboBrandID.SelectedValue) - 90000, 2);
        }
        else
        {
            if (Convert.ToInt32(cboBrandID.SelectedValue) > 80000)
            {
                AddUserAuth(Convert.ToInt32(cboBudgetResponsibilityAreaID.SelectedValue), Convert.ToInt32(cboCustomerID.SelectedValue), Convert.ToInt32(cboBrandID.SelectedValue) - 80000, 2);
            }
            else
            {
                AddUserAuth(Convert.ToInt32(cboBudgetResponsibilityAreaID.SelectedValue), Convert.ToInt32(cboCustomerID.SelectedValue), Convert.ToInt32(cboBrandID.SelectedValue), 1);
            }
        }
    }

    protected void cmdRemoveUserAuth_Click(object sender, EventArgs e)
    {
        RemoveUserAuth();
    }

#endregion
}
