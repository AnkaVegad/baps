﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomerEdit.ascx.cs" Inherits="User_Controls_CustomerEdit" %>

    <!--Customer Edit Area-->
    <table width="1100px">
    <tr><td style="font-size:2px">&nbsp;</td></tr>
    <tr>
        <td>
            <asp:Label ID="lblHeader" runat="server" CssClass="page_header"></asp:Label><br />
        </td>
    </tr><tr><td style="font-size:2px">&nbsp;</td></tr>
    </table>
    
    <div id="project_edit_area">
        <table width="1000">
            <tr><td valign="top">
                <table>
                    <tr><td>Customer Name</td><td>
                        <asp:Label ID="CustomerID" runat="server" Visible="false" width="40px"/>
                        <asp:TextBox ID="CustomerName" runat="server" Width="280px" MaxLength="50" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                            ControlToValidate="CustomerName" ValidationGroup="Customer"
                            Display="Dynamic" ErrorMessage="*Required" />
                    </td></tr>
                    <tr><td>Status</td><td>
                        <asp:DropDownList ID="ActiveIndicator" runat="server" Width="80px"
                            DataTextField="EntityName" DataValueField="ID" />
                        <asp:RangeValidator ID="RangeValidator6" runat="server" 
                            ErrorMessage="*Required" MaximumValue="2" MinimumValue="1" 
                            ControlToValidate="ActiveIndicator" Display="Dynamic" ValidationGroup="Customer" />
                    </td></tr>
                    <tr><td>SAP Customer Code</td><td>
                        <asp:TextBox ID="SAPCustomerCode" runat="server" Width="100px" MaxLength="12" />
                    </td></tr>
                 </table>
            </td></tr>
        </table>
        <table width="680px">
            <!--Allocation entry area-->
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr><td colspan="2">Allocation To Area and Customer Group</td></tr>
            <tr><td colspan="2">
                <asp:GridView ID="grdSubBrandID" runat="server" width="350px"
                    AutoGenerateColumns="False" GridLines="Vertical" RowStyle-Wrap="false" EmptyDataText="[Not Allocated]" 
                    HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false" SelectedRowStyle-Wrap="false"
                    OnRowDataBound="grdSubBrandID_RowDataBound" DataKeyNames="UniqueKey" ShowFooter="false">
                    <Columns>
                        <asp:BoundField DataField="UniqueKey" ReadOnly="false" Visible="false" />
                        <asp:BoundField DataField="BudgetResponsibilityAreaID" ReadOnly="false" Visible="false" />
                        <asp:BoundField DataField="BudgetResponsibilityAreaName" HeaderText="Area" SortExpression="BudgetResponsibilityAreaName" />
                        <asp:BoundField DataField="CustomerGroupID" ReadOnly="false" Visible="false" />
                        <asp:BoundField DataField="CustomerGroupName" HeaderText="Customer Group" SortExpression="CustomerGroupName" />
                        <asp:BoundField DataField="CurrencyCode" HeaderText="Currency" SortExpression="CurrencyCode" />
                        <asp:BoundField DataField="ActiveIndicator" ReadOnly="false" Visible="false" />
                        <asp:BoundField DataField="ActiveIndicatorName" HeaderText="Status" SortExpression="ActiveIndicator" />
                    </Columns>
                    <RowStyle CssClass="grid_alternating_row" Wrap="False" />
                    <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
                    <HeaderStyle CssClass="grid_header" Wrap="False" />
                </asp:GridView>
            </td></tr><tr><td colspan="2" style="font-size:2px">&nbsp;</td></tr><tr><td>
                <asp:Button ID="cmdRemoveSubBrand" runat="server" Text="Remove" causesvalidation="false"
                    onclick="cmdRemoveSubBrand_Click" CssClass="form_button" Enabled="false" />
                <asp:Button ID="cmdActive" runat="server" Text="Toggle Status" causesvalidation="false"
                    onclick="cmdActive_Click" CssClass="form_button" Enabled="true" Width="107px" />
            </td></tr>
                
            <tr><td colspan="2">Area&nbsp;
                <asp:DropDownList ID="cboBudgetResponsibilityAreaID" runat="server" Width="100px" Enabled="false"
                    DataTextField="EntityName" DataValueField="ID" />
                <asp:RangeValidator ID="RangeValidator2" ControlToValidate="cboBudgetResponsibilityAreaID" MaximumValue="10000000" MinimumValue="1"
                    runat="server" ErrorMessage="Required" Type="Integer" Display="Dynamic" ValidationGroup="SubBrand" />
                Customer Group&nbsp;<asp:DropDownList ID="cboCustomerGroupID" runat="server" Width="150px" Enabled="false"
                    DataTextField="EntityName" DataValueField="ID" />
                <asp:RangeValidator ID="RangeValidator3" ControlToValidate="cboCustomerGroupID" MaximumValue="10000000" MinimumValue="1"
                    runat="server" ErrorMessage="Required" Type="Integer" Display="Dynamic" ValidationGroup="SubBrand" />
                Currency&nbsp;<asp:TextBox ID="txtCurrencyCode" runat="server" MaxLength="3" />
                <asp:Button ID="cmdAddSubBrand" runat="server" Text="Add" causesvalidation="true"
                    onclick="cmdAddSubBrand_Click" CssClass="form_button" Enabled="false" Width="60px" ValidationGroup="SubBrand" />
            </td></tr>

        </table>
    </div>
    