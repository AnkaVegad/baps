﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductHierarchyEdit.ascx.cs" Inherits="User_Controls_ProductHierarchyEdit" %>

    <!--ProductHierarchy Edit Area-->
    <table width="1100px">
    <tr><td style="font-size:2px">&nbsp;</td></tr>
    <tr>
        <td>
            <asp:Label ID="lblHeader" runat="server" CssClass="page_header"></asp:Label><br />
        </td>
    </tr><tr><td style="font-size:2px">&nbsp;</td></tr>
    </table>
    
    <div id="project_edit_area">
        <table width="1000">
            <tr><td valign="top">
                <table>
                    <tr><td>Business Group</td><td>
                        <asp:Label ID="ProductHierarchyID" runat="server" Visible="false" width="40px"/>
                        <asp:DropDownList ID="BusinessGroupID" runat="server" Width="200px"
                            DataTextField="EntityName" DataValueField="ID" />
                        <asp:RangeValidator ID="RangeValidator1" runat="server" 
                            ErrorMessage="*Required" MaximumValue="1000000" MinimumValue="1" Type="Integer"
                            ControlToValidate="BusinessGroupID" Display="Dynamic" ValidationGroup="ProductHierarchy" />
                    </td></tr>
                    <tr><td>Business Unit</td><td>
                        <asp:DropDownList ID="BusinessUnitID" runat="server" Width="200px"
                            DataTextField="EntityName" DataValueField="ID" />
                        <asp:RangeValidator ID="RangeValidator6" runat="server" 
                            ErrorMessage="*Required" MaximumValue="1000000" MinimumValue="1"  Type="Integer"
                            ControlToValidate="BusinessUnitID" Display="Dynamic" ValidationGroup="ProductHierarchy" />
                    </td></tr>
                    <tr><td>European Category</td><td>
                        <asp:DropDownList ID="EuropeanCategoryID" runat="server" Width="200px"
                            DataTextField="EntityName" DataValueField="ID" />
                        <asp:RangeValidator ID="RangeValidator2" runat="server" 
                            ErrorMessage="*Required" MaximumValue="1000000" MinimumValue="1"  Type="Integer"
                            ControlToValidate="EuropeanCategoryID" Display="Dynamic" ValidationGroup="ProductHierarchy" />
                    </td></tr>
                    <tr><td>UK Category</td><td>
                        <asp:DropDownList ID="UKCategoryID" runat="server" Width="200px"
                            DataTextField="EntityName" DataValueField="ID" />
                        <asp:RangeValidator ID="RangeValidator3" runat="server" 
                            ErrorMessage="*Required" MaximumValue="1000000" MinimumValue="1"  Type="Integer"
                            ControlToValidate="UKCategoryID" Display="Dynamic" ValidationGroup="ProductHierarchy" />
                    </td></tr>
                    <tr><td>Brand</td><td>
                        <asp:DropDownList ID="BrandID" runat="server" Width="200px"
                            DataTextField="EntityName" DataValueField="ID" />
                        <asp:RangeValidator ID="RangeValidator4" runat="server" 
                            ErrorMessage="*Required" MaximumValue="1000000" MinimumValue="1"  Type="Integer"
                            ControlToValidate="BrandID" Display="Dynamic" ValidationGroup="ProductHierarchy" />
                    </td></tr>
                    <tr><td>Brand-Market</td><td>
                        <asp:DropDownList ID="SubBrandID" runat="server" Width="250px"
                            DataTextField="EntityName" DataValueField="ID" />
                        <asp:RangeValidator ID="RangeValidator5" runat="server" 
                            ErrorMessage="*Required" MaximumValue="1000000" MinimumValue="1"  Type="Integer"
                            ControlToValidate="SubBrandID" Display="Dynamic" ValidationGroup="ProductHierarchy" />
                    </td></tr>
                    <tr><td>Status</td><td>
                        <asp:DropDownList ID="ActiveIndicator" runat="server" Width="80px"
                            DataTextField="EntityName" DataValueField="ID" />
                        <asp:RangeValidator ID="RangeValidator7" runat="server" 
                            ErrorMessage="*Required" MaximumValue="2" MinimumValue="1"  Type="Integer"
                            ControlToValidate="ActiveIndicator" Display="Dynamic" ValidationGroup="ProductHierarchy" />
                    </td></tr>
                 </table>
            </td></tr>
        </table>

    </div>
    