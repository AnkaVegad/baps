﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Controls_CustomerEdit : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //populate the activity indicator dropdown
            ActiveIndicator.Items.Add(new ListItem("", "0"));
            ActiveIndicator.Items.Add(new ListItem("Active", "1"));
            ActiveIndicator.Items.Add(new ListItem("Inactive", "2"));
            //fill other drop downs
            FillCustomerGroup();
            FillBudgetResponsibilityArea();
        }
    }

#region properties

    public Customer SelectCustomer
    {
        get
        {
            Customer c = new Customer();
            c.CustomerID = Convert.ToInt32(CustomerID.Text);
            c.CustomerName = CustomerName.Text;
            c.SAPCustomerCode = SAPCustomerCode.Text;
            c.ActiveIndicator = Convert.ToInt16(ActiveIndicator.SelectedValue);
            c.CreatedBy = Session["CurrentUserName"].ToString();
            return c;
        }
    }

    //public string ActivityTypeIDValue
    //{
    //    get { return CustomerID.Text; }
    //}

    public string HeaderText
    {
        set { lblHeader.Text = value; }
    }

#endregion

#region gridevents

    protected void grdSubBrandID_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            //highlight on hover
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            e.Row.Attributes["onclick"] = cs.GetPostBackClientHyperlink(this.grdSubBrandID, "Select$" + e.Row.RowIndex);
        }
    }

#endregion
    
#region methods

    public void PopulateForm(Customer c)
    {
        CustomerID.Text = c.CustomerID.ToString();
        CustomerName.Text = c.CustomerName.ToString();
        ActiveIndicator.SelectedValue = c.ActiveIndicator.ToString();
        SAPCustomerCode.Text = c.SAPCustomerCode.ToString();
        CustomerName.Focus();

        //populate grdSubBrandID
        DataTable tbl = CustomerDataAccess.FillCustomerHierarchy(Convert.ToInt32(c.CustomerID));
        Session["SelectedSubBrandItems"] = tbl;
        grdSubBrandID.DataSource = tbl;
        grdSubBrandID.DataBind();

        //enable controls (must be after ActivityTypeID is populated)
        if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 15)
        {
            ToggleControlState(true);
        }
    }

    public string UpdateCustomer()
    {
        if (Session["SelectedSubBrandItems"] == null) //check for session expiry
        {
            Response.Redirect("ProjectView.aspx");
            return "";
        }
        else
        {
            //Main data
            Customer c = SelectCustomer;
            c.LastUpdatedBy = Session["CurrentUserName"].ToString();
            int intResult = CustomerDataAccess.UpdateCustomer(c);
            if (intResult == 0)
            {
                return "Duplicate value entered";
            }
            else
            {
                //CustomerHierarchy - clear and recreate
                CustomerDataAccess.ClearCustomerHierarchy(Convert.ToInt32(CustomerID.Text));
                DataTable tbl = (DataTable)Session["SelectedSubBrandItems"];
                foreach (DataRow r in tbl.Rows)
                {
                    CustomerDataAccess.InsertCustomerHierarchy(Convert.ToInt32(CustomerID.Text), Convert.ToInt32(r["BudgetResponsibilityAreaID"]), Convert.ToInt32(r["CustomerGroupID"]), r["CurrencyCode"].ToString(), Convert.ToInt16(r["ActiveIndicator"]), Session["CurrentUserName"].ToString());
                }
                //Clear
                ClearForm();
                return "";
            }
        }
    }

    public string InsertCustomer()
    {
        if (Session["SelectedSubBrandItems"] == null) //check for session expiry
        {
            Response.Redirect("ProjectView.aspx");
            return "";
        }
        else
        {
            DataTable tbl = (DataTable)Session["SelectedSubBrandItems"];
            //validation check at least one allocation is selected
            if (tbl.Rows.Count == 0)
            {
                return "You must select at least one allocation.";
            }
            else
            {
                //Main data
                Customer c = SelectCustomer;
                c.CreatedBy = Session["CurrentUserName"].ToString();
                int intCustomerID = CustomerDataAccess.InsertCustomer(c);
                //trap duplicate Customer name
                if (intCustomerID == 0)
                {
                    return "Duplicate Customer name.";
                }
                else
                {
                    //Allocation
                    DataTable tblSubBrand = (DataTable)Session["SelectedSubBrandItems"];
                    foreach (DataRow r in tblSubBrand.Rows)
                    {
                        CustomerDataAccess.InsertCustomerHierarchy(intCustomerID, Convert.ToInt32(r["BudgetResponsibilityAreaID"]), Convert.ToInt32(r["CustomerGroupID"]), r["CurrencyCode"].ToString(), 1, Session["CurrentUserName"].ToString());
                    }
                    //Clear
                    ClearForm();
                    return "";
                }
            }
        }
    }

    protected void ToggleControlState(bool state)
    {
        //enable or disable textboxes and dropdowns
        foreach (Control c in Controls)
        {
            if (c is TextBox) { ((TextBox)c).Enabled = state; }
            if (c is DropDownList) { ((DropDownList)c).Enabled = state; }
        }
        //other controls
        grdSubBrandID.Enabled = state;
        cmdAddSubBrand.Enabled = state;
        cmdRemoveSubBrand.Enabled = state;
    }

    public void ClearControls()
    {
        //remove data from controls
        CustomerID.Text = "";
        CustomerName.Text = "";
        SAPCustomerCode.Text = "";
        ActiveIndicator.SelectedValue = "0";
        //Clear Allocation
        grdSubBrandID.DataSource = "";
        grdSubBrandID.DataBind();
        Session["SelectedSubBrandItems"] = null;
        cboCustomerGroupID.SelectedValue = "0";
        cboBudgetResponsibilityAreaID.SelectedValue = "0";
        txtCurrencyCode.Text = "";
    }

    public void ClearForm()
    {
        ClearControls();
        ToggleControlState(false);
    }

    public void NewRecord()
    {
        ClearControls();
        //initialise allocations table
        DataTable tbl = new DataTable();
        tbl.Columns.Add("UniqueKey");
        tbl.Columns.Add("BudgetResponsibilityAreaID");
        tbl.Columns.Add("BudgetResponsibilityAreaName");
        tbl.Columns.Add("CustomerGroupID");
        tbl.Columns.Add("CustomerGroupName");
        tbl.Columns.Add("CurrencyCode");
        tbl.Columns.Add("ActiveIndicator");
        tbl.Columns.Add("ActiveIndicatorName");
        Session["SelectedSubBrandItems"] = tbl;

        CustomerID.Text = "0";
        CustomerName.Focus();
        //enable controls (must be after ActivityTypeID is populated)
        ToggleControlState(true);
    }

    protected void AddSelectedSubBrand(int intBudgetResponsibilityAreaID, int intCustomerGroupID, string strCurrencyCode, int intActiveIndicator)
    {
        DataTable tbl = (DataTable)Session["SelectedSubBrandItems"];

        //loop through tbl to see if record with this BRA has already been added (to prevent duplication)
        //*** why does it check Customer Group too? ***
        bool duplicate = false;
        for (int i = tbl.Rows.Count - 1; i >= 0; i--)
        {
            DataRow dr = tbl.Rows[i];
            string dtitem1 = dr["CustomerGroupID"].ToString();
            string dtitem2 = dr["BudgetResponsibilityAreaID"].ToString();
            if (dtitem1 == intCustomerGroupID.ToString() && dtitem2 == intBudgetResponsibilityAreaID.ToString())
            {
                duplicate = true;
            }
        }

        if (!duplicate)
        {
            //add the selected item to the table
            AddRow(tbl, intBudgetResponsibilityAreaID, intCustomerGroupID, strCurrencyCode, intActiveIndicator);
        }

        grdSubBrandID.DataSource = tbl;
        grdSubBrandID.DataBind();
        //store basket table in session variable
        Session["SelectedSubBrandItems"] = tbl;
        //clear fields
        cboCustomerGroupID.SelectedValue = "0";
        cboBudgetResponsibilityAreaID.SelectedValue = "0";
        txtCurrencyCode.Text = "";
    }

    protected void RemoveSubBrand()
    //remove the selected line item from the temporary table and grid
    {
        try //because an item may not be selected in the grid or the Session variable may not be valid
        {
            //loop through the table and delete if criteria found
            DataTable tbl = (DataTable)Session["SelectedSubBrandItems"];

            //mark record for deletion
            for (int i = tbl.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = tbl.Rows[i];
                string dtitem = dr["UniqueKey"].ToString();
                if (dtitem == grdSubBrandID.SelectedValue.ToString())
                {
                    tbl.Rows[i].Delete();
                }
            }

            //delete
            tbl.AcceptChanges();

            grdSubBrandID.DataSource = tbl;
            grdSubBrandID.DataBind();
            //store basket table in session variable
            Session["SelectedSubBrandItems"] = tbl;
        }
        catch { }
    }

    protected void ToggleActiveIndicator()
    //toggle ActivityIndicator for selected ActivityType_SpendType_BudgetResponsibilityArea in temporary table and grid 
    {
        try //because an item may not be selected in the grid or the Session variable may not be valid
        {
            //loop through the table and if criteria met toggle ActiveIndicator
            DataTable tbl = (DataTable)Session["SelectedSubBrandItems"];

            //find record, remove it, replace with new one
            for (int i = tbl.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = tbl.Rows[i];
                string dtitem = dr["UniqueKey"].ToString();
                if (dtitem == grdSubBrandID.SelectedValue.ToString())
                {
                    int intActiveIndicator = 2;
                    int intBudgetResponsibilityAreaID = Convert.ToInt32(dr["BudgetResponsibilityAreaID"]);
                    int intCustomerGroupID = Convert.ToInt32(dr["CustomerGroupID"]);
                    string strCurrencyCode = dr["CurrencyCode"].ToString();
                    if (Convert.ToInt16(dr["ActiveIndicator"]) == 0) { intActiveIndicator = 1; }
                    //remove existing row from table    
                    tbl.Rows[i].Delete();
                    tbl.AcceptChanges();
                    //add new row to table
                    AddRow(tbl, intBudgetResponsibilityAreaID, intCustomerGroupID, strCurrencyCode, intActiveIndicator);
                }
            }
            //refresh grid
            grdSubBrandID.DataSource = tbl;
            grdSubBrandID.DataBind();
            //store basket table in session variable
            Session["SelectedSubBrandItems"] = tbl;
        }
        catch { }
    }

    protected void AddRow(DataTable tbl, int intBudgetResponsibilityAreaID, int intCustomerGroupID, string strCurrencyCode, int intActiveIndicator)
    {
        //add the selected item to the table
        DataRow dr = tbl.NewRow();
        dr["UniqueKey"] = intBudgetResponsibilityAreaID.ToString() + "." + intCustomerGroupID.ToString();
        dr["BudgetResponsibilityAreaID"] = intBudgetResponsibilityAreaID;
        dr["BudgetResponsibilityAreaName"] = Common.ADOLookup("BudgetResponsibilityAreaName", "tblBudgetResponsibilityArea", "BudgetResponsibilityAreaID = " + intBudgetResponsibilityAreaID.ToString());
        dr["CustomerGroupID"] = intCustomerGroupID;
        dr["CustomerGroupName"] = Common.ADOLookup("CustomerGroupName", "tblCustomerGroup", "CustomerGroupID = " + intCustomerGroupID.ToString());
        dr["CurrencyCode"] = strCurrencyCode;
        dr["ActiveIndicator"] = intActiveIndicator;
        if (intActiveIndicator == 1)
        {
            dr["ActiveIndicatorName"] = "Active";
        }
        else
        {
            dr["ActiveIndicatorName"] = "Inactive";
        }
        tbl.Rows.Add(dr);
    }

#endregion

#region filldropdowns

    protected void FillBudgetResponsibilityArea()
    //Fill BudgetResponsibilityArea dropdownlist for which current user has permissions
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillBudgetResponsibilityArea";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboBudgetResponsibilityAreaID;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillCustomerGroup()
    //Fill dropdownlist of CustomerGroups
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillCustomerGroup";
        //cmd.Parameters.AddWithValue("@ZeroValueText", "");
        //cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboCustomerGroupID;
        d.DataSource = tbl;
        d.DataBind();
    }

#endregion

#region subformevents

    protected void cmdAddSubBrand_Click(object sender, EventArgs e)
    {
        AddSelectedSubBrand(Convert.ToInt32(cboBudgetResponsibilityAreaID.SelectedValue), Convert.ToInt32(cboCustomerGroupID.SelectedValue), txtCurrencyCode.Text, 1);
    }

    protected void cmdRemoveSubBrand_Click(object sender, EventArgs e)
    {
        RemoveSubBrand();
    }

    protected void cmdActive_Click(object sender, EventArgs e)
    {
        ToggleActiveIndicator();
    }

#endregion
}
