﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActivityEdit.ascx.cs" Inherits="User_Controls_ActivityEdit" %>

<script type="text/javascript">
    function showPhasing() {
        if (document.all.ctl00$ContentPlaceHolder1$ActivityEdit1$ProjectMonth.value == "13") {
            divPhasing.style.display = "block";
        } else {
            divPhasing.style.display = "none";
        }
        setNeedToConfirm(true);
    }

    function copyToShortText() {
        if (document.all.ctl00$ContentPlaceHolder1$ActivityEdit1$ShortText.value == "") {
        document.all.ctl00$ContentPlaceHolder1$ActivityEdit1$ShortText.value = document.all.ctl00$ContentPlaceHolder1$ActivityEdit1$ActivityName.value;
            //alert(document.all.ctl00$ContentPlaceHolder1$ActivityEdit1$ActivityName.value);
        }
    }

</script>
 
    <!--Edit Area-->
    <table><tr><td>
        <table border="0"><tr><td>
            <table style="width:400px;" border="0">
                <tr><td style="width:100px">Activity Name</td><td>
                    <asp:TextBox ID="ActivityID" runat="server" Visible="false" Width="40px" Enabled="false" />
                    <asp:HiddenField ID="txtChanged" runat="server" />
                    <asp:TextBox ID="ActivityName" runat="server" Width="280px" MaxLength="100" 
                        Enabled="False" onkeyup="Javascript:setNeedToConfirm(true);" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ErrorMessage="*Required" ControlToValidate="ActivityName" 
                        Display="Dynamic" ValidationGroup="Activity"></asp:RequiredFieldValidator>
                </td></tr>
                <tr><td>Short Text</td><td>
                    <asp:TextBox ID="ShortText" runat="server" Width="280px" Enabled="False" MaxLength="100"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ErrorMessage="*Required" ControlToValidate="ShortText" 
                        Display="Dynamic" ValidationGroup="Activity"></asp:RequiredFieldValidator>
                </td></tr>
                <tr><td>Supplier<asp:Label ID="SupplierID" runat="server" Width="30px" visible="false" /></td><td>
                    <asp:TextBox ID="SupplierName" runat="server" Width="280px" enabled="false" />
                </td></tr>
                <tr><td>Comments</td><td>
                    <asp:TextBox ID="Comments" runat="server" Width="280px" MaxLength="255" Enabled="False"
                        onkeyup="Javascript:setNeedToConfirm(true);"/>
                </td></tr>
            </table>
        </td>
        <td style="vertical-align:top">
            <table style="width:200px;" border="0">
                <tr><td>Supplier Job Ref</td><td>
                    <asp:TextBox ID="SupplierRefNo" runat="server" Width="80px" MaxLength="20" Enabled="False"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </td></tr>
                <tr><td>Quantity</td><td>
                    <asp:TextBox ID="ItemQuantity" runat="server" Width="80px" Enabled="False"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                    <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="*Must be between 1 and 10,000,000" 
                        ControlToValidate="ItemQuantity" Type="Integer" MinimumValue="1" MaximumValue="10000000" ValidationGroup="Activity" Display="Dynamic"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ErrorMessage="*Required" ControlToValidate="ItemQuantity" 
                        Display="Dynamic" ValidationGroup="Activity"></asp:RequiredFieldValidator>
                </td></tr>
                <tr><td>Unit Price</td><td>
                    <asp:TextBox ID="UnitPrice" runat="server" Width="80px" Enabled="False" 
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                    <asp:RangeValidator ID="RangeValidator4" runat="server" ErrorMessage="*Must be between -10,000,000 and 10,000,000" 
                        ControlToValidate="UnitPrice" Type="Currency" MinimumValue="-10000000" MaximumValue="10000000.00" ValidationGroup="Activity" Display="Dynamic"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                        ErrorMessage="*Required" ControlToValidate="UnitPrice" 
                        Display="Dynamic" ValidationGroup="Activity"></asp:RequiredFieldValidator>
                </td></tr>
                <tr><td>Month</td><td>
                    <asp:DropDownList ID="ProjectMonth" runat="server" Width="86px" Enabled="False" 
                        DataValueField="ID" DataTextField="EntityName" />
                    <asp:RangeValidator ID="RangeValidator5" runat="server" 
                        ErrorMessage="*Required" MaximumValue="13" MinimumValue="01" 
                        ControlToValidate="ProjectMonth" Display="Dynamic" ValidationGroup="Activity" Type="String"></asp:RangeValidator>
                </td></tr>
            </table>
        </td>
        <td style="vertical-align:top">
            <table style="width:375px;" border="0">
                <tr><td>Document Number</td><td>
                    <asp:TextBox ID="POID" runat="server" Width="30px" visible="false" />
                    <asp:TextBox ID="RebateID" runat="server" Width="30px" visible="false" />
                    <asp:TextBox ID="DocumentNumber" runat="server" Width="80px" Enabled="False" 
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </td></tr>
                <tr><td>Area</td><td>
                    <asp:DropDownList ID="BudgetResponsibilityAreaID" runat="server" Width="150px" Enabled="False" 
                        DataTextField="EntityName" DataValueField="ID" />
                </td></tr>
                <tr><td>Spend Type</td><td>
                    <asp:DropDownList ID="SpendTypeID" runat="server" Width="150px" Enabled="False" 
                        DataTextField="EntityName" DataValueField="ID" />
                    <asp:RangeValidator ID="RangeValidator6" runat="server" 
                        ErrorMessage="*Required" MaximumValue="Z" MinimumValue="1" 
                        ControlToValidate="SpendTypeID" Display="Dynamic" ValidationGroup="Activity" />
                </td></tr>
                <tr><td>Activity Type</td><td>
                    <asp:TextBox ID="ActivityTypeID" runat="server" Width="30px" visible="false" />
                    <asp:TextBox ID="ActivityTypeName" runat="server" Width="240px" enabled="false" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                        ControlToValidate="ActivityTypeID" Display="Dynamic" ErrorMessage="*Required Field"
                        ValidationGroup="Activity" />
                </td></tr>
                <tr><td colspan="2">
                    <asp:CustomValidator ID="CustomValidator1" runat="server" controltovalidate="ItemQuantity"
                        ErrorMessage="*Monthly breakdown does not add up to Quantity x Unit Price or values too large" 
                        onservervalidate="CustomValidator1_ServerValidate" ValidationGroup="Activity" Display="Dynamic" />
                </td></tr>    
            </table>
        </td></tr></table>
    </td></tr>
    <tr><td>
    
    <%if (ProjectMonthValue == "13") { %>
        <div id="divPhasing" style="display:block">
    <% } else {%>
        <div id="divPhasing" style="display:none">
    <% } %>
        <asp:Table ID="tblMonthlyBreakdown" runat="server" Visible="true">
            <asp:TableRow>
                <asp:TableCell>
                    &nbsp;
                </asp:TableCell>
                <asp:TableCell>
                    Jan
                </asp:TableCell>
                <asp:TableCell>
                    Feb
                </asp:TableCell>
                <asp:TableCell>
                    Mar
                </asp:TableCell>
                <asp:TableCell>
                    Apr
                </asp:TableCell>
                <asp:TableCell>
                    May
                </asp:TableCell>
                <asp:TableCell>
                    Jun
                </asp:TableCell>
                <asp:TableCell>
                    Jul
                </asp:TableCell>
                <asp:TableCell>
                    Aug
                </asp:TableCell>
                <asp:TableCell>
                    Sep
                </asp:TableCell>
                <asp:TableCell>
                    Oct
                </asp:TableCell>
                <asp:TableCell>
                    Nov
                </asp:TableCell>
                <asp:TableCell>
                    Dec
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    Monthly Breakdown (whole numbers only)
                </asp:TableCell>
                <asp:TableCell>
                    <asp:TextBox ID="Month01" runat="server" Width="60px" Enabled="true"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </asp:TableCell>
                <asp:TableCell>
                    <asp:TextBox ID="Month02" runat="server" Width="60px" Enabled="true"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </asp:TableCell>
                <asp:TableCell>
                    <asp:TextBox ID="Month03" runat="server" Width="60px" Enabled="true"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </asp:TableCell>
                <asp:TableCell>
                    <asp:TextBox ID="Month04" runat="server" Width="60px" Enabled="true"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </asp:TableCell>
                <asp:TableCell>
                    <asp:TextBox ID="Month05" runat="server" Width="60px" Enabled="true"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </asp:TableCell>
                <asp:TableCell>
                    <asp:TextBox ID="Month06" runat="server" Width="60px" Enabled="true"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </asp:TableCell>
                <asp:TableCell>
                    <asp:TextBox ID="Month07" runat="server" Width="60px" Enabled="true"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </asp:TableCell>
                <asp:TableCell>
                    <asp:TextBox ID="Month08" runat="server" Width="60px" Enabled="true"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </asp:TableCell>
                <asp:TableCell>
                    <asp:TextBox ID="Month09" runat="server" Width="60px" Enabled="true"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </asp:TableCell>
                <asp:TableCell>
                    <asp:TextBox ID="Month10" runat="server" Width="60px" Enabled="true"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </asp:TableCell>
                <asp:TableCell>
                    <asp:TextBox ID="Month11" runat="server" Width="60px" Enabled="true"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </asp:TableCell>
                <asp:TableCell>
                    <asp:TextBox ID="Month12" runat="server" Width="60px" Enabled="true"
                        onkeyup="Javascript:setNeedToConfirm(true);" />
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>        
    </td></tr></table>