﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserAdminGrid.ascx.cs" Inherits="User_Controls_UserAdminGrid" %>

        <!--User List-->
        <!--Page Header-->
        <table border="0" width="1100px">
            <tr>
                <td align="left">
                    <asp:Label ID="Label900" Text="User Admin" runat="server" CssClass="page_header"/>
                </td>
                <td>Area&nbsp;
                    <asp:DropDownList id="BudgetResponsibilityAreaID" runat="server" ontextchanged="BudgetResponsibilityAreaID_TextChanged" 
                        DataTextField="EntityName" DataValueField="ID" AutoPostBack="True"/>
                </td>
                <td>Active&nbsp;
                    <asp:DropDownList id="ActiveIndicatorFilter" runat="server" 
                        DataTextField="EntityName" DataValueField="ID" AutoPostBack="True"/>
                </td>
                <td>Level&nbsp;
                    <asp:DropDownList id="UserLevelIDFilter" runat="server" 
                        DataTextField="EntityName" DataValueField="ID" AutoPostBack="True"/>
                </td>
                <td align="right">
                    <asp:Label ID="lblSearch" Text="Search:" runat="server" />
                    <asp:TextBox ID="txtSearch" Runat="server" Width="120px" />
                    <asp:Button ID="cmdGo" Text="Go" Runat="server" CausesValidation="false" />
                </td>
            </tr>
        </table>
        <div id="spacer"></div>
        <!--Data Source - required to take advantage of sorting and paging functionality of the GridView-->
        <asp:SqlDataSource ID="sds" runat="server" 
            ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
            FilterExpression="UserName Like '%{0}%'">
            <FilterParameters>
                <asp:ControlParameter ControlID="txtSearch" PropertyName="Text" />
            </FilterParameters>
        </asp:SqlDataSource>
        <!--User Grid-->
        <div id="project_grid">
            <asp:GridView ID="grd" runat="server" width="1095px"
                AutoGenerateColumns="False" GridLines="Vertical" RowStyle-Wrap="false" EmptyDataText="No records found" 
                HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false" SelectedRowStyle-Wrap="false" 
                AllowSorting="True" AllowPaging="true" onpageindexchanged="grd_PageIndexChanged"  onsorted="grd_Sorted"
                onselectedindexchanged="grd_SelectedIndexChanged" OnRowDataBound="grd_RowDataBound" 
                DataKeyNames="UserID" DataSourceID="sds" PageSize="25">
                <Columns>
                    <asp:BoundField DataField="UserID" ReadOnly="True" Visible="False" />
                    <asp:BoundField DataField="UserName" HeaderText="User Name" SortExpression="UserName" />
                    <asp:BoundField DataField="ActiveYesNo" HeaderText="Active" SortExpression="ActiveYesNo" />
                    <asp:BoundField DataField="UserLevelName" HeaderText="Level" SortExpression="UserLevelName" />
                    <asp:BoundField DataField="DelegateUserName" HeaderText="Delegated User" SortExpression="DelegateUserName" />
                    <asp:BoundField DataField="CreatedBy" HeaderText="Created By" SortExpression="CreatedBy" />
                    <asp:BoundField DataField="CreatedDate" HeaderText="Date Created" SortExpression="CreatedDate" />
                </Columns>
                <RowStyle CssClass="grid_row" Wrap="False" />
                <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
                <HeaderStyle CssClass="grid_header" Wrap="False" />
                <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
            </asp:GridView>
        </div><br />
        <br />
