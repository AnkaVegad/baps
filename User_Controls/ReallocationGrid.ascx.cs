﻿using System;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Controls_ReallocationGrid : System.Web.UI.UserControl
{
    int intCol1 = 0;
    int intCol2 = 0;
    int intCol3 = 0;
    int intCol4 = 0;
    int intCol5 = 0;
    int intCol6 = 0;
    int intCol7 = 0;
    int intTotal = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        //This is additionally set here because otherwise a "Data Key Names must be set for Grid View" error occurs
        grd.DataKeyNames = new string[] { "ProjectToCustomerID" };
        cboPulsingPlanPriority.OkButtonClicked += new EventHandler(cboPulsingPlanPriority_OkButtonClicked);
        if (!IsPostBack)
        {
            //String UID = this.UniqueID.Replace("$", "_") + "_";
            //grid.Attributes.Add("OnScroll", "savescroll('" + UID + "'); return false;");
            HeaderText = "Reallocate To/From";
            Session["FillType"] = "From";
            //populate selection dropdowns
            FillCustomer();
            FillYear();
            FillBrand();
            FillSpendType();
            FillApprover();
            FillCategory();
            FillBudgetResponsibilityArea();
            FillPulsingPlanPriority();
            //added AH 21/08/12
            GetPreviousColumnsSelected();
            ReallocateToEnable(false);
            txtMessage.Text = "Use filters and select a project to customer record, click <Reallocate From> button to reallocate from that entry";
            //get current year from session variable set in global.asax
            cboYear.SelectedValue = Session["SelectedYear"].ToString();
            DataTable tbl = UserDataAccess.SelectUserSelections(Session["CurrentUserName"].ToString());
            //set area 
            if (Session["SelectedBudgetResponsibilityAreaID"] == null)
            {
                if (tbl.Rows[0]["SelectionName"] == DBNull.Value)
                //allows for user's first login when table entry will be blank
                {
                    Session["SelectedBudgetResponsibilityAreaID"] = Common.ADOLookup("BudgetResponsibilityAreaID", "qryUserAuthorisations", "UserName = '" + Session["CurrentUserName"].ToString().Replace("'", "''") + "'");
                }
                else
                {
                    Session["SelectedBudgetResponsibilityAreaID"] = tbl.Rows[0]["LastBudgetResponsibilityAreaID"].ToString();
                }
            }
            cboBudgetResponsibilityArea.SelectedValue = Session["SelectedBudgetResponsibilityAreaID"].ToString();

            if (Session["SelectedCustomerID"] == null)
            {
                if (tbl.Rows[0]["SelectionName"] == DBNull.Value)
                //allows for user's first login when table entry will be blank
                {
                    Session["SelectedCustomerID"] = Common.ADOLookup("CustomerID", "qryUserAuthorisations", "UserName = '" + Session["CurrentUserName"].ToString().Replace("'", "''") + "'");
                }
                else
                {
                    Session["SelectedCustomerID"] = tbl.Rows[0]["LastCustomerID"].ToString();
                }

            }
            cboCustomer.SelectedValue = Session["SelectedCustomerID"].ToString();

            if (Session["SelectedBrandID"] == null)
            {
                if (tbl.Rows[0]["SelectionName"] == DBNull.Value)
                //allows for user's first login when table entry will be blank
                {
                    Session["SelectedBrandID"] = Common.ADOLookup("BrandID", "qryUserAuthorisations", "UserName = '" + Session["CurrentUserName"].ToString().Replace("'", "''") + "'");
                }
                else
                {
                    Session["SelectedBrandID"] = tbl.Rows[0]["LastBrandID"].ToString();
                }
            }
            cboBrand.SelectedValue = Session["SelectedBrandID"].ToString();

            if (Session["SelectedCategoryID"] == null)
            {
                if (tbl.Rows[0]["SelectionName"] == DBNull.Value)
                //allows for user's first login when table entry will be blank
                {
                    Session["SelectedCategoryID"] = Common.ADOLookup("CategoryID", "qryUserAuthorisations", "UserName = '" + Session["CurrentUserName"].ToString().Replace("'", "''") + "'");
                }
                else
                {
                    Session["SelectedCategoryID"] = tbl.Rows[0]["LastCategoryID"].ToString();
                }
            }
            cboCategory.SelectedValue = Session["SelectedCategoryID"].ToString();

            if (Session["SelectedPulsingPlanPriorityID"] == null)
            {
                if (tbl.Rows[0]["SelectionName"] == DBNull.Value)
                //allows for user's first login when table entry will be blank
                {
                    cboPulsingPlanPriority.SetSelectedValue("-1");
                    Session["SelectedPulsingPlanPriorityID"] = cboPulsingPlanPriority.getSelectedID();
                }
                else
                {
                    for (int i = 0; i < tbl.Rows.Count; i++)
                    {
                        if (tbl.Rows[i]["SelectionName"].ToString() == "LastPulsingPlanPriorityID" && tbl.Rows[i]["SelectionValue"].ToString() != "")
                        {
                            cboPulsingPlanPriority.SetSelectedValue(tbl.Rows[i]["SelectionValue"].ToString());
                            Session["SelectedPulsingPlanPriorityID"] = tbl.Rows[i]["SelectionValue"].ToString();
                        }
                        else
                        {
                            cboPulsingPlanPriority.SetSelectedValue("-1");
                            Session["SelectedPulsingPlanPriorityID"] = cboPulsingPlanPriority.getSelectedID();
                        }
                    }
                }
            }
            //cboPulsingPlanPriority.SetSelectedValue(Session["SelectedPulsingPlanPriorityID"].ToString());
            //Added AH 16/12/12 - ISNULLs in qryUser should deal with "first use" issue
            SelectedProjectToCustomerID.Value = "0";
            FillReallocationGrid();
            //grd.SelectedIndex = 0;
        }
        //SetScroll();
    }

    public void SetScroll()
    {
        String UID = this.UniqueID.Replace("$", "_") + "_";
        Page page = HttpContext.Current.CurrentHandler as Page;
        page.ClientScript.RegisterStartupScript(typeof(Page), "setscroll", "<script type='text/javascript'>setscroll('" + UID + "');</script>");
    }

    #region properties

    public string SelectedValue
    {
        get { return grd.SelectedValue.ToString(); }
    }

    public string SelectedProjectID
    {
        get
        {
            string strProjectID = Common.ADOLookup("ProjectID", "tblProjectToCustomer", "ProjectToCustomerID = " + grd.SelectedValue.ToString());
            return strProjectID;
        }
    }

    public string HeaderText
    {
        set { lblHeaderText.Text = value; }
    }

    #endregion

    #region methods

    //protected void UpdateUserSelections()
    //{
    //    UserDataAccess.UpdateUserSelections(Convert.ToInt32(cboCustomer.SelectedValue), Convert.ToInt32(cboBudgetResponsibilityArea.SelectedValue), Convert.ToInt32(cboBrand.SelectedValue), Convert.ToInt32(cboCategory.SelectedValue), cboPulsingPlanPriorityMatchType.SelectedValue.ToString(), Convert.ToInt32(cboPulsingPlanPriority.SelectedValue), Session["CurrentUserName"].ToString());
    //}

    public void Refresh()
    {
        grd.DataBind();
    }

    #endregion

    #region gridevents
    public void FillReallocationGrid()
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillReallocation_17mar";
        cmd.Parameters.AddWithValue("@Year", cboYear.SelectedValue);
        cmd.Parameters.AddWithValue("@BudgetResponsibilityAreaID", cboBudgetResponsibilityArea.SelectedValue);
        cmd.Parameters.AddWithValue("@CustomerID", cboCustomer.SelectedValue);
        cmd.Parameters.AddWithValue("@BrandID", cboBrand.SelectedValue);
        cmd.Parameters.AddWithValue("@PulsingPlanPriorityID", Session["SelectedPulsingPlanPriorityID"].ToString());
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        cmd.Parameters.AddWithValue("@SearchText", txtSearch.Text);
        cmd.Parameters.AddWithValue("@FillType", Session["FillType"].ToString());

        if (Session["FillType"].ToString().Equals("From"))
            cmd.Parameters.AddWithValue("@CategoryID", cboCategory.SelectedValue);
        else
            cmd.Parameters.AddWithValue("@CategoryID", FromProjectToCustomerID.Value);

        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        grd.DataSource = tbl;
        grd.DataBind();
    }

    //protected void grd_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    Session["SelectedProjectToCustomerID"] = Convert.ToInt32(grd.SelectedValue);
    //}

    //protected void grd_Sorted(object sender, EventArgs e)
    //{
    //    grd.SelectedIndex = -1;
    //}

    protected void grd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            int intCurrent = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "SumOfVariance"));
            //colour code the cell 
            if (intCurrent < 0) { e.Row.Cells[12].BackColor = Color.PapayaWhip; } else { e.Row.Cells[12].BackColor = Color.LightGreen; }
            //increment the totals
            intCol1 += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "SumOfAPBudget"));
            intCol2 += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "SumOfAPFYView"));
            intCol3 += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "SumOfAPVariance"));
            intCol4 += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "SumOfTCCBudget"));
            intCol5 += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "SumOfTCCFYView"));
            intCol6 += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "SumOfTCCVariance"));
            intCol7 += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "SumOfFYView"));
            intTotal += intCurrent;
            //highlight on hover
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            //e.Row.Attributes["onclick"] = cs.GetPostBackClientHyperlink(this.grd, "Select$" + e.Row.RowIndex);
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[2].Text = "Totals";
            //display the totals
            e.Row.Cells[5].Text = intCol1.ToString();
            e.Row.Cells[6].Text = intCol2.ToString();
            e.Row.Cells[7].Text = intCol3.ToString();
            e.Row.Cells[8].Text = intCol4.ToString();
            e.Row.Cells[9].Text = intCol5.ToString();
            e.Row.Cells[10].Text = intCol6.ToString();
            e.Row.Cells[11].Text = intCol7.ToString();
            e.Row.Cells[12].Text = intTotal.ToString();
            //colour code the cell 
            if (intTotal < 0) { e.Row.Cells[12].BackColor = Color.PapayaWhip; } else { e.Row.Cells[12].BackColor = Color.LightGreen; }
        }

        if (e.Row.RowIndex == Convert.ToInt32(SelectedProjectToCustomerID.Value))
        {
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                e.Row.Cells[i].CssClass = "grid_selected";
            }
        }

        ////the following Akira lines are to increase the height of the top row so it does not disappear behind the fixed header
        //if (e.Row.RowType == DataControlRowType.Header)
        //{
        //    e.Row.Attributes.Add("Id", "GridHeader");
        //}
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    if (e.Row.RowIndex == 0)
        //        /*e.Row.Height = x.HeaderRow.Height;*/
        //        e.Row.Attributes.Add("Id", "GridFirstRow");
        //    /*e.Row.Style.Add("height", headerHeight);*/
        //    e.Row.Style.Add("vertical-align", "bottom");
        //    /*e.Row.Style.Add("position", "relative");*/
        //}
    }

    #endregion

    #region dropdowns

    protected void FillBudgetResponsibilityArea()
    //fill BudgetResponsibilityArea dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillBudgetResponsibilityArea";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        //cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboBudgetResponsibilityArea;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillCustomer()
    //fill customer dropdownlist for current user
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillCustomer";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        //cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        cmd.Parameters.AddWithValue("@IncludeInactive", 1);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboCustomer;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillBrand()
    //fill brand dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillBrandActive";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        //cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        cmd.Parameters.AddWithValue("@IncludeInactive", 0);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboBrand;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillCategory()
    //fill category dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillCategoryActive";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        //cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        cmd.Parameters.AddWithValue("@IncludeInactive", 0);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboCategory;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillYear()
    //fill year dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillYear";
        cmd.Parameters.AddWithValue("@ZeroValueText", "[None]");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboYear;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillPulsingPlanPriority()
    //fill project type dropdownlist 
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillPulsingPlanPriority";
        cmd.Parameters.AddWithValue("@ZeroValueText", "[None]");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        cboPulsingPlanPriority.AddDataToList(tbl, "EntityName", "ID");
    }

    protected void FillSpendType()
    //populate the spendtype dropdownlists
    {
        DataTable tbl = Common.FillDropDown("FillSpendType");
        DropDownList d = cboFromSpendTypeID;
        d.DataSource = tbl;
        d.DataBind();
        d = cboToSpendTypeID;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillApprover()
    {
        if (Session["SourceProjectID"] != null && Session["DestinationProjectID"] != null)
        {
            SqlCommand cmd = DataAccess.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "FillApprover";

            cmd.Parameters.AddWithValue("@SourceProjectID", Session["SourceProjectID"].ToString());
            cmd.Parameters.AddWithValue("@DestinationProjectID", Session["DestinationProjectID"].ToString());

            DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
            DropDownList d = cboApprover;
            d.DataSource = tbl;
            d.DataBind();
            if (cboApprover.Items.Count > 0)
                cboApprover.SelectedIndex = 0;
        }
    }

    protected void cboBudgetResponsibilityArea_TextChanged(object sender, EventArgs e)
    {
        Session["SelectedBudgetResponsibilityAreaID"] = cboBudgetResponsibilityArea.SelectedValue;
        SetUserSelections("LastBudgetResponsibilityAreaID", Session["SelectedBudgetResponsibilityAreaID"].ToString());
        FillReallocationGrid();
    }

    protected void cboCustomer_TextChanged(object sender, EventArgs e)
    {
        Session["SelectedCustomerID"] = cboCustomer.SelectedValue;
        SetUserSelections("LastCustomerID", Session["SelectedCustomerID"].ToString());
        FillReallocationGrid();
    }

    protected void cboBrand_TextChanged(object sender, EventArgs e)
    {
        Session["SelectedBrandID"] = cboBrand.SelectedValue;
        SetUserSelections("LastBrandID", Session["SelectedBrandID"].ToString());
        FillReallocationGrid();
    }

    protected void cboCategory_TextChanged(object sender, EventArgs e)
    {
        Session["SelectedCategoryID"] = cboCategory.SelectedValue;
        SetUserSelections("LastCategoryID", Session["SelectedCategoryID"].ToString());
        FillReallocationGrid();
    }

    protected void cboYear_TextChanged(object sender, EventArgs e)
    {
        Session["SelectedYear"] = cboYear.SelectedValue;
        SetUserSelections("LastYear", Session["SelectedYear"].ToString());
        FillReallocationGrid();

    }

    protected void cboPulsingPlanPriority_OkButtonClicked(object sender, EventArgs e)
    {

        if (cboPulsingPlanPriority.getSelectedID() == "")
        {
            cboPulsingPlanPriority.SetSelectedValue("-1");
            Session["SelectedPulsingPlanPriorityID"] = cboPulsingPlanPriority.getSelectedID();

        }
        else
        {
            Session["SelectedPulsingPlanPriorityID"] = cboPulsingPlanPriority.getSelectedID();
        }
        SetUserSelections("LastPulsingPlanPriorityID", Session["SelectedPulsingPlanPriorityID"].ToString());
        FillReallocationGrid();
    }
    #endregion

    #region events
    public void CategoryFilterDisable()
    {
        cboCategory.Enabled = false;
    }

    public void CategoryFilterEnable()
    {
        cboCategory.Enabled = true;
    }

    protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (txtFromIONumber.Text == txtToIONumber.Text && cboFromSpendTypeID.SelectedValue == cboToSpendTypeID.SelectedValue)
        {
            args.IsValid = false;
        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void CustomValidator2_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (txtToProject.Text == "")
        {
            args.IsValid = false;
        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void cmdReallocateFrom_Click(object sender, EventArgs e)
    {
        try //in case no selection in grid
        {
            //CustomValidator1.Enabled = true;
            //check permissions (added 06/12/12)
            //grd.SelectedIndex = Convert.ToInt32(SelectedProjectToCustomerID.Value);
            grd.SelectedIndex = Convert.ToInt32(SelectedProjectToCustomerID.Value);
            FromProjectToCustomerID.Value = grd.SelectedValue.ToString();
            grd.SelectedIndex = -1;
            SelectedProjectToCustomerID.Value = "0";
            if (Common.CheckAuthorisations(Convert.ToInt32(FromProjectToCustomerID.Value), Session["CurrentUserName"].ToString()) > 0)
            {
                //retrieve the ProjectName, CustomerName and IOC for this ProjectToCustomer record
                DataTable tbl = ProjectDataAccess.SelectProjectToCustomer(Convert.ToInt32(FromProjectToCustomerID.Value));
                DataRow dr = tbl.Rows[0];
                //txtFromProjectToCustomerID.Text = dr["ProjectToCustomerID"].ToString();
                txtFromProject.Text = dr["ProjectName"].ToString();
                txtFromCustomer.Text = dr["CustomerName"].ToString();
                txtFromIONumber.Text = dr["IONumber"].ToString();
                Session["SourceProjectID"] = dr["ProjectID"].ToString();
                CategoryFilterDisable();
                Session["FillType"] = "To";
                cmdReallocateTo.Enabled = true;
                FillApprover();
                txtMessage.Text = "Use filters and select a project to customer record, click <Reallocate To> button to reallocate to that entry. Alternately, click <Reallocate From> button to change reallocate from selection";
                FillReallocationGrid();
            }
            else
            {
                MsgBox("BAPS", "No authorisation for project.");
            }
        }
        catch (Exception ex)
        {
            if (ex.Message.ToString() == "Object reference not set to an instance of an object.")
            {
                MsgBox("Message", "You must select a project.");
            }
            else
            {
                MsgBox("Message", ex.Message.ToString());
            }
        }
    }

    protected void cmdReallocateTo_Click(object sender, EventArgs e)
    {
        grd.SelectedIndex = Convert.ToInt32(SelectedProjectToCustomerID.Value);
        ToProjectToCustomerID.Value = grd.SelectedValue.ToString();
        grd.SelectedIndex = -1;
        SelectedProjectToCustomerID.Value = "0";
        try //in case no selection in grid
        {
            //retrieve the ProjectName, CustomerName and IOC for this ProjectToCustomer record
            DataTable tbl = ProjectDataAccess.SelectProjectToCustomer(Convert.ToInt32(ToProjectToCustomerID.Value));
            DataRow dr = tbl.Rows[0];
            txtToProject.Text = dr["ProjectName"].ToString();
            txtToCustomer.Text = dr["CustomerName"].ToString();
            txtToIONumber.Text = dr["IONumber"].ToString();
            Session["DestinationProjectID"] = dr["ProjectID"].ToString();
            ReallocateFromEnable(false);
            ReallocateToEnable(true);
            FillApprover();
            txtMessage.Text = "Change SpendTypes and amount, add an approver and click Save button to request approval for reallocation. Or, Click on Clear Form to start over";
            FillReallocationGrid();
            if (cboApprover.Items.Count == 0)
                ApproverMessage.Text = "Approver not available";
            else
                ApproverMessage.Text = "";
        }
        catch (Exception ex)
        {
            if (ex.Message.ToString() == "Object reference not set to an instance of an object.")
            {
                MsgBox("Message", "You must select a project.");
            }
            else
            {
                MsgBox("Message", ex.Message.ToString());
            }
        }
    }

    public void ReallocateToEnable(Boolean state)
    {
        cmdReallocateTo.Enabled = state;
        txtComments.Enabled = state;
        cboApprover.Enabled = state;
    }

    public void ReallocateFromEnable(Boolean state)
    {
        cmdReallocateFrom.Enabled = state;
    }
    protected void SetUserSelections(string name, string value)
    {
        DataTable tbl = new DataTable();
        DataColumn clm;

        clm = new DataColumn("UserName");
        clm.DefaultValue = "0";
        tbl.Columns.Add(clm);

        clm = new DataColumn("SelectionName");
        clm.DefaultValue = "0";
        tbl.Columns.Add(clm);

        clm = new DataColumn("SelectionValue");
        clm.DefaultValue = "0";
        tbl.Columns.Add(clm);

        DataRow dr = tbl.NewRow();
        dr[0] = Session["CurrentUserName"].ToString();
        dr[1] = name;
        dr[2] = value;
        tbl.Rows.Add(dr);
        UserDataAccess.UpdateUserSelections(tbl);
    }
    protected void GetPreviousColumnsSelected()
    {
        DataTable tbl = UserDataAccess.SelectUserSelections(Session["CurrentUserName"].ToString());
        for (int i = 0; i < tbl.Rows.Count; i++)
        {
            if (tbl.Rows[i]["SelectionName"].ToString() == "LastBudgetResponsibilityAreaID")
            {
                Session["SelectedBudgetResponsibilityAreaID"] = tbl.Rows[i]["SelectionValue"].ToString();
                cboBudgetResponsibilityArea.SelectedValue = Session["SelectedBudgetResponsibilityAreaID"].ToString();
            }
            if (tbl.Rows[i]["SelectionName"].ToString() == "LastCustomerID")
            {
                Session["SelectedCustomerID"] = tbl.Rows[i]["SelectionValue"].ToString();
                cboCustomer.SelectedValue = Session["SelectedCustomerID"].ToString();
            }
            if (tbl.Rows[i]["SelectionName"].ToString() == "LastBrandID")
            {
                Session["SelectedBrandID"] = tbl.Rows[i]["SelectionValue"].ToString();
                cboBrand.SelectedValue = Session["SelectedBrandID"].ToString();
            }
            if (tbl.Rows[i]["SelectionName"].ToString() == "LastCategoryID")
            {
                Session["SelectedCategoryID"] = tbl.Rows[i]["SelectionValue"].ToString();
                cboCategory.SelectedValue = Session["SelectedCategoryID"].ToString();
            }
            if (tbl.Rows[i]["SelectionName"].ToString() == "LastYear")
            {
                Session["SelectedYear"] = tbl.Rows[i]["SelectionValue"].ToString();
                cboYear.SelectedValue = Session["SelectedYear"].ToString();
            }
            if (tbl.Rows[i]["SelectionName"].ToString() == "LastPulsingPlanPriorityID")
            {
                Session["SelectedPulsingPlanPriorityID"] = tbl.Rows[i]["SelectionValue"].ToString();
                cboPulsingPlanPriority.SetSelectedValue(Session["SelectedPulsingPlanPriorityID"].ToString());
            }
        }
    }


    protected void MsgBox(string strTitle, string strMessage)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), strTitle, "alert('" + Common.Left(strMessage, 40) + "');", true);
    }

    public void ClearForm()
    {
        txtFromProject.Text = "";
        txtToProject.Text = "";
        txtFromCustomer.Text = "";
        txtToCustomer.Text = "";
        txtFromIONumber.Text = "";
        txtToIONumber.Text = "";
        cboFromSpendTypeID.SelectedValue = "0";
        cboToSpendTypeID.SelectedValue = "0";
        txtBudgetAmount.Text = "";
        txtComments.Text = "";
        CategoryFilterEnable();
        txtMessage.Text = "Use filters and select a project to customer record, click <Reallocate From> button to reallocate from that entry";
        Session["FillType"] = "From";
        ReallocateFromEnable(true);
        ReallocateToEnable(false);
        SelectedProjectToCustomerID.Value = "0";
        FillReallocationGrid();
        ApproverMessage.Text = "";
    }

    public void Save()
    {
        //check money exists in source budget
        string strMessage = ReallocationDataAccess.TransactReallocation(Convert.ToInt32(FromProjectToCustomerID.Value), Convert.ToInt32(ToProjectToCustomerID.Value), Convert.ToInt32(cboFromSpendTypeID.SelectedValue), Convert.ToInt32(cboToSpendTypeID.SelectedValue), Session["CurrentUserName"].ToString(), Convert.ToInt32(txtBudgetAmount.Text), txtComments.Text, cboApprover.SelectedValue);
        if (strMessage == "")
        {
            MsgBox("Message", "Reallocation request sent for approval.");
            CategoryFilterEnable();
            Session["FillType"] = "To";
            ReallocateFromEnable(true);
            ReallocateToEnable(false);
            ClearForm();
        }
        else
        {
            MsgBox("Message", strMessage);
        }
    }

    protected void cmdGo_Click(object sender, EventArgs e)
    {
        FillReallocationGrid();
    }
    protected void txtSearch_TextChanged(object sender, EventArgs e)
    {
        FillReallocationGrid();
    }
#endregion
   
}
