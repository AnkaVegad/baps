﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Controls_SupplierGrid : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            grd.SelectedIndex = 0;
        }
    }

#region properties

    public string SelectedValue
    {
        get { 
            try
            {
                return grd.SelectedValue.ToString(); 
            }
            catch { return "0"; }
        }
    }

    public string HeaderText
    {
        set { lblHeader.Text = value; }
    }

#endregion

#region methods
    public void PopulateGrid()
    {
        //populate the supplier grid 
        string strSQL;
        if (txtSearch.Text.Contains("'") == true)
        {
            txtSearch.Text = "Remove quote/apostrophe.";
        }
        else
        {
            if (txtSearch.Text.Trim() == String.Empty) //no search filter - show the top 10 active suppliers only, excluding "OTHER"
            {
                strSQL = "SELECT TOP 10 * FROM qrySupplierWithCountOfPO ";
                strSQL += "WHERE SupplierName <> 'OTHER' ";
                if (lblHeader.Text == "Select Supplier")
                {
                    strSQL += "AND ActiveIndicator = 1 ";
                }
                strSQL += "ORDER BY CountOfPO DESC;";
            }
            else //show all active suppliers meeting the criteria - don't put a "TOP n" because it's applied before the filter
            {
                strSQL = "SELECT * FROM qrySupplierWithCountOfPO ";
                if (lblHeader.Text == "Select Supplier")
                {
                    strSQL += "WHERE ActiveIndicator = 1 ";
                }
                strSQL += "ORDER BY CountOfPO DESC;";
            }
            sds.SelectCommand = strSQL;
            grd.DataSourceID = "sds";
            grd.DataBind();
            grd.Columns[3].Visible = (lblHeader.Text == "Suppliers");
           
        }
    }


    public void RefreshGrid()
    {
        grd.DataBind();
        grd.SelectedIndex = 0;
    }

    public void RefreshData()
    {
    }

#endregion
    
#region gridevents

    protected void grd_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void grd_PageIndexChanged(object sender, EventArgs e)
    {
        grd.SelectedIndex = 0;
    }

    protected void grd_Sorted(object sender, EventArgs e)
    {
        grd.SelectedIndex = 0;
    }

    protected void grd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(this.grd, "Select$" + e.Row.RowIndex);
        }
    }

#endregion

    protected void cmdGo_Click(object sender, EventArgs e)
    {
        PopulateGrid();
    }
}
