﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CalendarGrid.ascx.cs" Inherits="User_Controls_CalendarGrid" %>

<%@ Register src="~/User_Controls/MultiselectDropdown.ascx" tagname="dropdown" tagprefix="uc1" %>
<html>
    <head>
        <title></title>
        <script type="text/javascript">

            function SetNewTab() {
                document.forms[0].target = "_blank";
            }

            function setscroll(ctlID) {
                var panel = document.getElementById(ctlID + 'calendar_grid');
                var div_value = document.cookie.substring(document.cookie.indexOf("=")+1, document.cookie.length);
                panel.scrollTop = div_value.substring(0, div_value.indexOf(","));
                panel.scrollLeft = div_value.substring(div_value.indexOf(",") + 1, div_value.length);
            }

            function savescroll(ctlID) {
                var panel = document.getElementById(ctlID + 'calendar_grid');
                document.cookie = "div_position=" + panel.scrollTop.toString() + ',' + panel.scrollLeft.toString() +";";
            }

            function SetSameTab() {
                document.forms[0].target = "";
            }
        </script>
    </head>
<body>  

<div id="parent">
    
    <!--Header and Filter Criteria-->
    <div id="resizable1" class="resizable ui-widget-content" style="height:101.5%">
        <br />
        <asp:Label ID="lblHeaderText" Text="Calendar" runat="server" CssClass="page_header"></asp:Label>
        <br />
        <br />
        <table border="0">
            <tr>
                <td>Area</td>
                <td>
                    <uc1:dropdown ID="cboBudgetResponsibilityArea" runat="server" />
                </td>
            </tr>
            <tr>
                <td>Business Unit</td>
                <td>
                    <uc1:dropdown ID="cboBusinessUnit" runat="server" />
                </td>
            </tr>
            <tr>
                <td>Category</td>
                <td>
                    <uc1:dropdown ID="cboCategory" runat="server" />
                </td>
            </tr>
            <tr>
                <td>Brand</td>
                <td>
                    <uc1:dropdown ID="cboBrand" runat="server" />
                </td>
            </tr>
            <tr>
                <td>Year</td>
                <td>
                    <asp:DropDownList id="cboYear" runat="server" ontextchanged="cboYear_TextChanged" AutoPostBack="True" ForeColor="Black"
                        DataTextField="EntityName" DataValueField="ID"/>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">Priority</td>
                <td class="auto-style1">
                    <uc1:dropdown ID="cboPulsingPlanPriority" runat="server" />
                </td>
            </tr>
            <tr>
                <td>Month</td>
                <td>
                    <uc1:dropdown ID="cboMonth" runat="server" />
                </td>
                
            </tr>
        </table>
        <asp:Label ID="lblErrorMessage" runat="server" ForeColor="Red"></asp:Label>
        <br /> 
    </div>

    <!--Search and Grid-->
    <div id="resizable2" class="ui-widget-content" style="height:101.5%">
        <!--Data Source - required to take advantage of sorting and paging functionality of the GridView; parameter removed 28/02/14 to solve filter problem with SQLDataSource when >101 records-->
                <asp:SqlDataSource ID="sds" runat="server" 
            ConnectionString="<%$ ConnectionStrings:APConnectionString %>">
             <UpdateParameters>
                 <asp:Parameter Type="Int32" 
                  Name="Target"></asp:Parameter>
            </UpdateParameters>
        </asp:SqlDataSource>

        <br />
        <asp:Panel id="calendar_grid" style="overflow-x:auto; overflow-y:auto; height:98%; width:100%; position:relative" runat="server">
            <asp:GridView ID="grdCalendar" runat="server" CellPadding="3"
                AutoGenerateColumns="False" EmptyDataText="No Records Found" 
                EnableModelValidation="True">
                <PagerStyle HorizontalAlign="Center" />
                <RowStyle Wrap="True" Height ="50"/>
                <HeaderStyle BackColor="Gray" CssClass="GVFixedHeader" ForeColor="White" Wrap="True"></HeaderStyle>
                <Columns>
                    <asp:BoundField  HeaderStyle-CssClass="BrandHeader" DataField="Brand" ReadOnly="True"  Visible="True" HeaderText="Brand" SortExpression="Brand"/>

                    <asp:BoundField HeaderText="JanID" DataField="JanID" ReadOnly="True" />
                    <asp:BoundField HeaderText="FebID" DataField="FebID" ReadOnly="True" />
                    <asp:BoundField HeaderText="MarID" DataField="MarID" ReadOnly="True" />
                    <asp:BoundField HeaderText="AprID" DataField="AprID" ReadOnly="True" />
                    <asp:BoundField HeaderText="MayID" DataField="MayID" ReadOnly="True" />
                    <asp:BoundField HeaderText="JunID" DataField="JunID" ReadOnly="True" />
                    <asp:BoundField HeaderText="JulID" DataField="JulID" ReadOnly="True" />
                    <asp:BoundField HeaderText="AugID" DataField="AugID" ReadOnly="True" />
                    <asp:BoundField HeaderText="SepID" DataField="SepID" ReadOnly="True" />
                    <asp:BoundField HeaderText="OctID" DataField="OctID" ReadOnly="True" />
                    <asp:BoundField HeaderText="NovID" DataField="NovID" ReadOnly="True" />
                    <asp:BoundField HeaderText="DecID" DataField="DecID" ReadOnly="True" />

                    <asp:TemplateField HeaderStyle-CssClass="MonthHeader" HeaderText="Jan" SortExpression="Jan" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" Text='<%# Eval("Jan") %>' OnClick ="LinkButton_Click" OnClientClick="SetSameTab();" Width ="100px"></asp:LinkButton>
                        </ItemTemplate>
                        
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-CssClass="MonthHeader" HeaderText="Feb" SortExpression="Feb" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton2" runat="server" Text='<%# Eval("Feb") %>' OnClick ="LinkButton_Click" OnClientClick="SetSameTab();" Width ="100px"></asp:LinkButton>
                        </ItemTemplate>
                        
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-CssClass="MonthHeader"  HeaderText="Mar" SortExpression="Mar" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton3" runat="server" Text='<%# Eval("Mar") %>' OnClick ="LinkButton_Click" OnClientClick="SetSameTab();" Width ="100px"></asp:LinkButton>
                        </ItemTemplate>
                        
                    </asp:TemplateField>
                    <asp:TemplateField  HeaderStyle-CssClass="MonthHeader" HeaderText="Apr" SortExpression="Apr" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton4" runat="server" Text='<%# Eval("Apr") %>' OnClick ="LinkButton_Click" OnClientClick="SetSameTab();" Width ="100px"></asp:LinkButton>
                        </ItemTemplate>
                        
                    </asp:TemplateField>
                    <asp:TemplateField  HeaderStyle-CssClass="MonthHeader"  HeaderText="May" SortExpression="May" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton5" runat="server" Text='<%# Eval("May") %>' OnClick ="LinkButton_Click" OnClientClick="SetSameTab();" Width ="100px"></asp:LinkButton>
                        </ItemTemplate>
                        
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-CssClass="MonthHeader"  HeaderText="Jun" SortExpression="Jun" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton6" runat="server" Text='<%# Eval("Jun") %>' OnClick ="LinkButton_Click" OnClientClick="SetSameTab();" Width ="100px"></asp:LinkButton>
                        </ItemTemplate>
                        
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-CssClass="MonthHeader"  HeaderText="Jul" SortExpression="Jul" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton7" runat="server" Text='<%# Eval("Jul") %>' OnClick ="LinkButton_Click" OnClientClick="SetSameTab();" Width ="100px"></asp:LinkButton>
                        </ItemTemplate>
                        
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-CssClass="MonthHeader"  HeaderText="Aug" SortExpression="Aug" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton8" runat="server" Text='<%# Eval("Aug") %>' OnClick ="LinkButton_Click" OnClientClick="SetSameTab();" Width ="100px"></asp:LinkButton>
                        </ItemTemplate>
                        
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-CssClass="MonthHeader"  HeaderText="Sep" SortExpression="Sep" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton9" runat="server" Text='<%# Eval("Sep") %>' OnClick ="LinkButton_Click" OnClientClick="SetSameTab();" Width ="100px"></asp:LinkButton>
                        </ItemTemplate>
                        
                    </asp:TemplateField>
                    <asp:TemplateField  HeaderStyle-CssClass="MonthHeader" HeaderText="Oct" SortExpression="Oct" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton10" runat="server" Text='<%# Eval("Oct") %>' OnClick ="LinkButton_Click" OnClientClick="SetSameTab();" Width ="100px"></asp:LinkButton>
                        </ItemTemplate>
                        
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-CssClass="MonthHeader"  HeaderText="Nov" SortExpression="Nov" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton11" runat="server" Text='<%# Eval("Nov") %>'  OnClick ="LinkButton_Click" OnClientClick="SetSameTab();" Width ="100px"></asp:LinkButton>
                        </ItemTemplate>
                        
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-CssClass="MonthHeader"  HeaderText="Dec" SortExpression="Dec" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton12" runat="server" Text='<%# Eval("Dec") %>' OnClick ="LinkButton_Click" OnClientClick="SetSameTab();" Width ="100px"></asp:LinkButton>
                        </ItemTemplate>
                        
                    </asp:TemplateField>

                    <asp:BoundField HeaderText="JanColour" DataField="JanColour" ReadOnly="True" />
                    <asp:BoundField HeaderText="FebColour" DataField="FebColour" ReadOnly="True" />
                    <asp:BoundField HeaderText="MarColour" DataField="MarColour" ReadOnly="True" />
                    <asp:BoundField HeaderText="AprColour" DataField="AprColour" ReadOnly="True" />
                    <asp:BoundField HeaderText="MayColour" DataField="MayColour" ReadOnly="True" />
                    <asp:BoundField HeaderText="JunColour" DataField="JunColour" ReadOnly="True" />
                    <asp:BoundField HeaderText="JulColour" DataField="JulColour" ReadOnly="True" />
                    <asp:BoundField HeaderText="AugColour" DataField="AugColour" ReadOnly="True" />
                    <asp:BoundField HeaderText="SepColour" DataField="SepColour" ReadOnly="True" />
                    <asp:BoundField HeaderText="OctColour" DataField="OctColour" ReadOnly="True" />
                    <asp:BoundField HeaderText="NovColour" DataField="NovColour" ReadOnly="True" />
                    <asp:BoundField HeaderText="DecColour" DataField="DecColour" ReadOnly="True" />
                </Columns>
            </asp:GridView>
        </asp:Panel>
    </div>
</div>
<div id="footer">
    <br /> 
    &nbsp;
    <asp:Button ID="cmdExport" runat="server" Text="Export"
        onclick="cmdExport_Click" CssClass="form_button" OnClientClick="SetNewTab();" />
</div>
</body>
</html>
