﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RiskTrackerEdit.ascx.cs" Inherits="User_Controls_RiskTrackerEdit" %>

    <!--RiskTracker Edit Area-->
    <div id="activity_edit_area">
        <table width="1000">
            <tr><td valign="top">
                <table>
                    <%if (Session["Mode"].ToString() == "Edit") {%>
                    <tr><td>Customer</td><td>
                        <asp:Label ID="CustomerName" runat="server" Width="200px" CSSClass="pseudo_text_box" />
                    </td></tr>
                    <tr><td>Project</td><td>
                        <asp:Label ID="ProjectName" runat="server" Width="200px" CSSClass="pseudo_text_box" />
                    </td></tr><%}%>
                    <tr><td>Spend Type</td><td>
                        <asp:DropDownList ID="SpendTypeID" runat="server" Width="100px"
                            DataTextField="EntityName" DataValueField="ID" />
                        <asp:RangeValidator ID="RangeValidator2" runat="server" 
                            ErrorMessage="*Required" MaximumValue="1000000" MinimumValue="1"  Type="Integer"
                            ControlToValidate="SpendTypeID" Display="Dynamic" ValidationGroup="RiskTracker" />
                        <asp:Label ID="RiskTrackerID" runat="server" Visible="false" width="40px"/>
                        <asp:Label ID="ProjectToCustomerID" runat="server" Visible="false" width="40px"/>
                    </td></tr>
                    <tr><td>Month</td><td>
                        <asp:DropDownList ID="ProjectMonth" runat="server" Width="100px"
                            DataTextField="EntityName" DataValueField="ID" />
                        <asp:RangeValidator ID="RangeValidator1" runat="server" 
                            ErrorMessage="*Required" MaximumValue="13" MinimumValue="1"  Type="Integer"
                            ControlToValidate="ProjectMonth" Display="Dynamic" ValidationGroup="RiskTracker" />
                    </td></tr>
                    <tr><td>Risk/Opportunity</td><td>
                        <asp:DropDownList ID="OpportunityOrRisk" runat="server" Width="100px"
                            DataTextField="EntityName" DataValueField="ID" />
                        <asp:RangeValidator ID="RangeValidator4" runat="server" 
                            ErrorMessage="*Required" MaximumValue="2" MinimumValue="1"  Type="Integer"
                            ControlToValidate="OpportunityOrRisk" Display="Dynamic" ValidationGroup="RiskTracker" />
                    </td></tr>
                    <tr><td>Key Drivers</td><td>
                        <asp:TextBox ID="KeyDrivers" runat="server" Width="350px" TextMode="MultiLine" MaxLength="255" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator"
                            ControlToValidate="KeyDrivers" Display="Dynamic" ValidationGroup="RiskTracker" />
                    </td></tr>
                    <tr><td>Financial Impact</td><td>
                        <asp:TextBox ID="FinancialImpact" runat="server" Width="100px" />
                        <asp:RangeValidator ID="RangeValidator7" runat="server" 
                            ErrorMessage="*Required" MaximumValue="1000000000" MinimumValue="-1000000000"  Type="Integer"
                            ControlToValidate="FinancialImpact" Display="Dynamic" ValidationGroup="RiskTracker" />
                    </td></tr>
                 </table>
            </td></tr>
        </table>

    </div>
    