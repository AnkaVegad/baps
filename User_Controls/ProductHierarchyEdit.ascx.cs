﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Controls_ProductHierarchyEdit : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //populate the activity indicator dropdown
            ActiveIndicator.Items.Add(new ListItem("", "0"));
            ActiveIndicator.Items.Add(new ListItem("Active", "1"));
            ActiveIndicator.Items.Add(new ListItem("Inactive", "2"));
            //populate other dropdowns
            FillBusinessGroup();
            FillBusinessUnit();
            FillEuropeanCategory();
            FillCategory();
            FillBrand();
            FillSubBrand();
        }
    }

#region properties

    public ProductHierarchy SelectProductHierarchy
    {
        get
        {
            ProductHierarchy h = new ProductHierarchy();
            h.ProductHierarchyID = Convert.ToInt32(ProductHierarchyID.Text);
            h.BusinessGroupID = Convert.ToInt32(BusinessGroupID.Text);
            h.BusinessUnitID = Convert.ToInt32(BusinessUnitID.SelectedValue);
            h.EuropeanCategoryID = Convert.ToInt32(EuropeanCategoryID.SelectedValue);
            h.UKCategoryID = Convert.ToInt32(UKCategoryID.SelectedValue);
            h.BrandID = Convert.ToInt32(BrandID.SelectedValue);
            h.SubBrandID = Convert.ToInt32(SubBrandID.SelectedValue);
            h.ActiveIndicator = ActiveIndicator.SelectedValue;
            h.CreatedBy = Session["CurrentUserName"].ToString();
            return h;
        }
    }

    public string HeaderText
    {
        set { lblHeader.Text = value; }
    }

#endregion

#region methods

    public void PopulateForm(ProductHierarchy h)
    {
        ProductHierarchyID.Text = h.ProductHierarchyID.ToString();
        BusinessGroupID.SelectedValue = h.BusinessGroupID.ToString();
        BusinessUnitID.SelectedValue = h.BusinessUnitID.ToString();
        EuropeanCategoryID.SelectedValue = h.EuropeanCategoryID.ToString();
        UKCategoryID.SelectedValue = h.UKCategoryID.ToString();
        BrandID.SelectedValue = h.BrandID.ToString();
        SubBrandID.SelectedValue = h.SubBrandID.ToString();
        ActiveIndicator.SelectedValue = h.ActiveIndicator.ToString();
        BusinessGroupID.Focus();
        //enable controls
        if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 15)
        {
            ToggleControlState(true);
        }
    }

    public string UpdateProductHierarchy()
    {
        if (Session["CurrentUserLevelID"] == null) //check for session expiry
        {
            Response.Redirect("ProjectView.aspx");
            return "";
        }
        else
        {
            //Main data
            ProductHierarchy h = SelectProductHierarchy;
            int intResult = ProductHierarchyDataAccess.UpdateProductHierarchy(h);
            if (intResult == 0)
            {
                return "Duplicate Product Hierarchy values.";
            }
            else
            {
                //Clear
                ClearForm();
                return "";
            }
        }
    }

    public string InsertProductHierarchy()
    {
        if (Session["CurrentUserLevelID"] == null) //check for session expiry
        {
            Response.Redirect("ProjectView.aspx");
            return "";
        }
        else
        {
            //Main data
            ProductHierarchy h = SelectProductHierarchy;
            h.CreatedBy = Session["CurrentUserName"].ToString();
            int intProductHierarchyID = ProductHierarchyDataAccess.InsertProductHierarchy(h);
            //trap duplicate ProductHierarchy values
            if (intProductHierarchyID == 0)
            {
                return "Duplicate Product Hierarchy values.";
            }
            else
            {
                //Clear
                ClearForm();
                return "";
            }
        }
    }

    protected void ToggleControlState(bool state)
    {
        //enable or disable textboxes and dropdowns
        foreach (Control c in Controls)
        {
            if (c is TextBox) { ((TextBox)c).Enabled = state; }
            if (c is DropDownList) { ((DropDownList)c).Enabled = state; }
        }
    }

    public void ClearControls()
    {
        //enable or disable textboxes and dropdowns
        foreach (Control c in Controls)
        {
            if (c is TextBox) { ((TextBox)c).Text = ""; }
            if (c is DropDownList) { ((DropDownList)c).SelectedValue = "0"; }
        }
    }

    public void ClearForm()
    {
        ClearControls();
        ToggleControlState(false);
    }

    public void NewRecord()
    {
        ClearControls();
        ProductHierarchyID.Text = "0";
        BusinessGroupID.Focus();
        //enable controls
        ToggleControlState(true);
    }

#endregion

#region filldropdowns

    protected void FillBrand()
    //fill brand dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillBrand";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = BrandID;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillSubBrand()
    //fill subbrand dropdownlist for current user
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillSubBrand";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = SubBrandID;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillCategory()
    //fill category dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillCategory";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = UKCategoryID;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillBusinessUnit()
    //fill BusinessUnit dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillBusinessUnit";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = BusinessUnitID;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillBusinessGroup()
    //fill BusinessGroup dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillBusinessGroup";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = BusinessGroupID;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillEuropeanCategory()
    //fill EuropeanCategory dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillEuropeanCategory";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = EuropeanCategoryID;
        d.DataSource = tbl;
        d.DataBind();
    }

#endregion
}
