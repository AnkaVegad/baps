﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Controls_ProductHierarchyGrid : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //populate the ProductHierarchy grid 
        string strSQL;
        strSQL = "SELECT ProductHierarchyID, BusinessGroupName, BusinessUnitName, EuropeanCategoryName, UKCategoryName, BrandName, IHOOHName, SubBrandName, StatusName FROM qryProductHierarchy ";
        strSQL += "ORDER BY BusinessGroupName, BusinessUnitName, EuropeanCategoryName, UKCategoryName, BrandName, IHOOHName, SubBrandName;";
        sds.SelectCommand = strSQL;
        grd.DataSourceID = "sds";
        grd.DataBind();
        if (!IsPostBack)
        {
            grd.SelectedIndex = 0;
        }
    }

#region properties

    public string SelectedValue
    {
        get { 
            try
            {
                return grd.SelectedValue.ToString(); 
            }
            catch { return "0"; }
        }
    }

    public string HeaderText
    {
        set { lblHeader.Text = value; }
    }

#endregion

#region methods

    public void RefreshGrid()
    {
        grd.DataBind();
        grd.SelectedIndex = 0;
    }

    public void RefreshData()
    {
    }

#endregion
    
#region gridevents

    protected void grd_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void grd_PageIndexChanged(object sender, EventArgs e)
    {
        grd.SelectedIndex = 0;
    }

    protected void grd_Sorted(object sender, EventArgs e)
    {
        grd.SelectedIndex = 0;
    }

    protected void grd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(this.grd, "Select$" + e.Row.RowIndex);
        }
    }

#endregion

}
