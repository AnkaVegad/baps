﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProjectToCustomerEdit.ascx.cs" Inherits="User_Controls_ProjectToCustomerEdit" %>

<script>
    function calcProportion() {
        document.all.ctl00$ContentPlaceHolder1$ProjectEdit1$txtProportion.value = 100 - document.all.ctl00$ContentPlaceHolder1$ProjectEdit1$txtTotalProportion.value;
    }
</script>

    <!--ProjectToCustomer Edit Area-->
    <table>
        <tr><td style="font-size:2px">&nbsp;</td></tr>
        <tr>
            <td>
                <asp:Label ID="lblHeader" Text="Edit Project Details" runat="server" CssClass="page_header"></asp:Label><br />
            </td>
        </tr><tr><td style="font-size:2px">&nbsp;</td></tr>
    </table>
    
    <div id="project_edit_area">
        <table style="width:1080px" border="0">
            <tr><td style="vertical-align:top">
                <table><tr>
                    <td>Project Name</td><td>
                        <asp:Label ID="ProjectToCustomerID" runat="server" Visible="false" width="40px"/>
                        <asp:TextBox ID="ProjectID" runat="server" Visible="False" />
                        <asp:Label ID="ProjectName" runat="server" Width="380px" CSSClass="page_header" />
                    </td></tr>
                    <tr><td>Customer</td><td>
                        <asp:DropDownList ID="CustomerID" runat="server" Width="185px" Enabled="False" DataTextField="EntityName" DataValueField="ID" />
                        <asp:RangeValidator ID="RangeValidator3" runat="server" controltovalidate="CustomerID" ErrorMessage="*Required"
                            maximumvalue="10000000" MinimumValue="1" Type="Integer" Display="Dynamic" />
                    </td></tr>  
                
                    <tr><td>IO Number</td><td>
                        <asp:TextBox ID="IONumber" runat="server" Width="100px" MaxLength="12" enabled="false" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="IONumber"
                            Display="Dynamic" ErrorMessage="*Required" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="IONumber"
                            Display="Dynamic" ErrorMessage="A 12 digit IO Number must be entered" ValidationExpression="^.{12,12}$" />
                    </td></tr>
                    <tr><td>In Store Start Date</td><td>
                        <asp:TextBox ID="InStoreStartDate" runat="server" Width="100px" MaxLength="50" enabled="false" />
                    </td></tr>
                    <tr><td>Pulsing Plan Priority (Customer Level)</td><td>
                        <asp:DropDownList ID="PulsingPlanPriorityID" runat="server" Width="185px" enabled="false" 
                            DataTextField="EntityName" DataValueField="ID" />
                    </td></tr>
                    <tr><td>IO Owner</td><td>
                        <asp:TextBox ID="ProjectOwner" runat="server" Width="180px" MaxLength="50" enabled="false" />
                    </td></tr>
                    <tr><td>Allocated A&amp;P Budget</td><td>
                        <asp:TextBox ID="AllocatedAPBudget" runat="server" Width="80px" enabled="false" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="AllocatedAPBudget"
                            Display="Dynamic" ErrorMessage="*Required" />
                        <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="*Required" display="Dynamic"
                            ControlToValidate="AllocatedAPBudget" MaximumValue="999999999" MinimumValue="-999999999" Type="Integer" />
                    </td></tr>
                    <tr><td>Allocated TCC Budget</td><td>
                        <asp:TextBox ID="AllocatedTCCBudget" runat="server" Width="80px" enabled="false" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="AllocatedTCCBudget"
                            Display="Dynamic" ErrorMessage="*Required" />
                        <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="*Required" display="Dynamic"
                            ControlToValidate="AllocatedTCCBudget" MaximumValue="999999999" MinimumValue="-999999999" Type="Integer" />
                    </td></tr>
                    <tr><td>Allocated Coupon Budget</td><td>
                        <asp:TextBox ID="AllocatedCpnBudget" runat="server" Width="80px" enabled="false" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="AllocatedCpnBudget"
                            Display="Dynamic" ErrorMessage="*Required" />
                        <asp:RangeValidator ID="RangeValidator4" runat="server" ErrorMessage="*Required" display="Dynamic"
                            ControlToValidate="AllocatedCpnBudget" MaximumValue="999999999" MinimumValue="-999999999" Type="Integer" />
                    </td></tr>
                    <tr><td><asp:Label id="lblComments" runat="server" Text="Reason for budget change" Visible="false" /></td><td>
                        <asp:TextBox ID="Comments" runat="server" Width="180px" height="60px" visible="false" enabled="false" TextMode="MultiLine" />
                    </td></tr>


                </table>
                
                          
            </td><td style="vertical-align:top">
                <table>
                    <!--Secondary IO entry area--added 11/02/13---------------------------------------------------------------------------------------------->
                    <tr><td colspan="2">Secondary IOs</td></tr>
                    <tr><td colspan="2">
                        <asp:GridView ID="grdSecondaryIO" runat="server" width="280px"
                            AutoGenerateColumns="False" GridLines="Vertical" RowStyle-Wrap="false" EmptyDataText="[None]" 
                            HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false" SelectedRowStyle-Wrap="false"
                            OnRowDataBound="grdSecondaryIO_RowDataBound" DataKeyNames="IONumberSecondary" ShowFooter="True">
                            <Columns>
                                <asp:BoundField DataField="IONumberSecondary" HeaderText="Secondary IO" SortExpression="IONumberSecondary" />
                                <asp:BoundField DataField="AllocatedBudget" HeaderText="Budget" SortExpression="AllocatedBudget" />
                                <asp:BoundField DataField="Proportion" HeaderText="(%)" SortExpression="Proportion" DataFormatString="{0:0}" />
                            </Columns>
                            <RowStyle CssClass="grid_alternating_row" Wrap="False" />
                            <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
                            <HeaderStyle CssClass="grid_header" Wrap="False" />
                        </asp:GridView>
                    </td></tr><tr><td colspan="2" style="font-size:2px">&nbsp;</td></tr><tr><td>
                        <asp:Button ID="cmdRemoveSecondaryIO" runat="server" Text="Remove" causesvalidation="false"
                            onclick="cmdRemoveSecondaryIO_Click" CssClass="form_button" Enabled="false" />
                        <asp:TextBox ID="txtTotalProportion" runat="server" Width="30px" Visible="true" 
                            CSSClass="hidden_text" Enabled="false" />
                        <asp:RangeValidator ID="RangeValidator7" runat="server" ErrorMessage="*Proportions must add up to 100"
                            validationgroup="ProjectToCustomer"
                            controltovalidate="txtTotalProportion" MinimumValue="100" MaximumValue="100" Display="Dynamic" Type="Integer" />
                    </td></tr>
                
                    <tr><td colspan="2">
                        IO:&nbsp;<asp:TextBox ID="txtIONumberSecondary" runat="server" Width="85px" Enabled="false" MaxLength="12" />
                        <asp:RangeValidator ID="RangeValidator6" ControlToValidate="txtIONumberSecondary" MaximumValue="229999999999" MinimumValue="210000000000"
                            runat="server" ErrorMessage="Required" Type="String" Display="Dynamic" ValidationGroup="SecondaryIO" />
                        &nbsp;Bdgt:&nbsp;<asp:TextBox ID="txtAllocatedBudget" runat="server" Width="70px" Enabled="false" />
                        <asp:TextBox ID="txtProportion" runat="server" Width="30px" enabled="false" />%
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtProportion" 
                            ErrorMessage="*Required" Display="Dynamic" ValidationGroup="SecondaryIO" />
                        <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="txtProportion" 
                            MaximumValue="100" MinimumValue="1"
                            ErrorMessage="*Must be between 1 and 100" Type="Double" Display="Dynamic" ValidationGroup="SecondaryIO" />
                        <asp:Button ID="cmdAddSecondaryIO" runat="server" Text="Add" causesvalidation="true"
                            onclick="cmdAddSecondaryIO_Click" CssClass="form_button" Enabled="false" Width="60px" ValidationGroup="SecondaryIO" />
                    </td></tr>  
                    <!--End of Secondary IO entry area--added 11/02/13---------------------------------------------------------------------------------------------->
                </table>
            </td></tr>
        </table>
    </div>
    