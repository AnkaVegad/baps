﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RiskTrackerGrid.ascx.cs" Inherits="User_Controls_RiskTrackerGrid" %>

    <!--Page Header-->
    <table border="0" style="width:1000px">
        <tr>
            <td style="text-align:left">
                <asp:Label ID="lblHeader" Text="Risk Tracker" runat="server" CssClass="page_header" />
            </td>
        <td>Area</td>
        <td>
            <asp:DropDownList id="cboBudgetResponsibilityArea" runat="server" ontextchanged="cboBudgetResponsibilityArea_TextChanged" 
                DataTextField="EntityName" DataValueField="ID" AutoPostBack="True"/>
        </td>
        <td>Customer</td>
        <td>
            <asp:DropDownList id="cboCustomer" runat="server" ontextchanged="cboCustomer_TextChanged" 
                DataTextField="EntityName" DataValueField="ID" AutoPostBack="True" Width="200px" />
        </td>
        <td>Spend Type</td>
        <td>
            <asp:DropDownList id="cboSpendType" runat="server" AutoPostBack="True" 
                DataTextField="EntityName" DataValueField="ID" />
        </td>
        <td>Month</td>
        <td>
            <asp:DropDownList id="cboMonth" runat="server" AutoPostBack="True" 
                DataTextField="EntityName" DataValueField="ID" />
        </td>
        <td>Year</td>
        <td>
            <asp:DropDownList id="cboYear" runat="server" ontextchanged="cboYear_TextChanged" AutoPostBack="True" 
                DataTextField="EntityName" DataValueField="ID" />
        </td>
            <td style="text-align:left">
                <asp:Label ID="lblSearch" Text="Search:" runat="server" Visible="false"></asp:Label>
                <asp:TextBox ID="txtSearch" Runat="server" Width="120px" Visible="false"/>
                <asp:Button ID="cmdGo" Text="Go" Runat="server" CausesValidation="false" Visible="false" />
            </td>
        </tr>
        <tr><td style="font-size:3px">&nbsp;</td></tr>
    </table>

    <!--Data Source - required to take advantage of sorting and paging functionality of the GridView-->
    <asp:SqlDataSource ID="sds" runat="server" 
        ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
        FilterExpression="ProjectName Like '%{0}%'" >
        <FilterParameters>
            <asp:ControlParameter ControlID="txtSearch" PropertyName="Text" />
        </FilterParameters>
    </asp:SqlDataSource>

    <!--RiskTracker Grid-->
    <div id="project_grid">
        <asp:GridView ID="grd" runat="server" width="1000px"
            AutoGenerateColumns="False" GridLines="Vertical"
            RowStyle-Wrap="false" EmptyDataText="No Records Found" 
            HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
            SelectedRowStyle-Wrap="false"
            AllowSorting="True" onsorted="grd_Sorted" AllowPaging="True" onpageindexchanged="grd_PageIndexChanged"
            onselectedindexchanged="grd_SelectedIndexChanged" 
            OnRowDataBound="grd_RowDataBound" 
            DataKeyNames="RiskTrackerID" ShowFooter="True" PageSize="25">
            <Columns>
                <asp:BoundField DataField="RiskTrackerID" ReadOnly="True" Visible="false" />
                <asp:BoundField DataField="CustomerName" HeaderText="Customer" SortExpression="CustomerName" />
                <asp:BoundField DataField="ProjectName" HeaderText="Project" SortExpression="ProjectName" />
                <asp:BoundField DataField="SpendTypeName" HeaderText="Spend Type" SortExpression="SpendTypeName" />
                <asp:BoundField DataField="MonthShortName" HeaderText="Month" SortExpression="MonthShortName" />
                <asp:BoundField DataField="OpportunityOrRiskName" HeaderText="Opportunity/Risk" SortExpression="OpportunityOrRiskName" />
                <asp:BoundField DataField="KeyDrivers" HeaderText="Key Drivers" SortExpression="KeyDrivers" />
                <asp:BoundField DataField="FinancialImpact" HeaderText="Financial Impact" SortExpression="FinancialImpact" />
                <asp:BoundField DataField="CurrencyCode" HeaderText="Currency" SortExpression="CurrencyCode" />
            </Columns>
            <RowStyle CssClass="grid_row" Wrap="False" />
            <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
            <HeaderStyle CssClass="grid_header" Wrap="False" />
            <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
        </asp:GridView>
    </div><br />
