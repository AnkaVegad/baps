﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AnnualTotals.ascx.cs" Inherits="User_Controls_AnnualTotals" %>

<div id="annual_totals">
    <table border="0" width="1050px" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left"><b>ANNUAL</b></td>
            <td>Allocated Bdgt</td>
            <!--<td>Chg To Bdgt</td>
            <td>Curr Wkg Bdgt</td>-->
            <td>Committed</td>
            <td>Uncommitted</td>
            <td>Total Forecast</td>
            <td>Balance</td>
            <td>FY View*</td>
            <td>Variance</td>
            <td></td>
            <td>Allocated Bdgt</td>
            <td>Total Forecast</td>
            <td>Balance</td>
        </tr>
        <tr>
            <td align="left">A&amp;P</td>
            <td><asp:Label ID="AllocatedAPBudget" runat="server" Width="80px" CssClass="label_text" /></td>
            <!--<td><asp:Label ID="ChangeToAPBudget" runat="server" Width="80px" CssClass="label_text" /></td>
            <td><asp:Label ID="CurrentAPWorkingBudget" runat="server" Width="80px" CssClass="label_text" /></td>-->
            <td><asp:Label ID="APCommitted" runat="server" Width="80px" CssClass="label_text" /></td>
            <td><asp:Label ID="APUncommitted" runat="server" Width="80px" CssClass="label_text" /></td>
            <td><asp:Label ID="APForecast" runat="server" Width="80px" CssClass="label_text" /></td>
            <td><asp:Label ID="APBalance" runat="server" Width="80px" CssClass="label_text" /></td>
            <td><asp:Label ID="APFYView" runat="server" Width="80px" CssClass="label_text" /></td>
            <td><asp:Label ID="APVariance" runat="server" Width="80px" CssClass="label_text" /></td>
            <td align="left">Coupons</td>
            <td><asp:TextBox ID="AllocatedCpnBudget" runat="server" Width="80px" CssClass="label_text" /></td>
            <td><asp:Label ID="CpnForecast" runat="server" Width="80px" CssClass="label_text" /></td>
            <td><asp:Label ID="CpnBalance" runat="server" Width="80px" CssClass="label_text" /></td>
        </tr>
        <tr>
            <td align="left">TCC</td>
            <td><asp:TextBox ID="AllocatedTCCBudget" runat="server" Width="80px" CssClass="label_text" /></td>
            <!--<td><asp:Label ID="ChangeToTCCBudget" runat="server" Width="80px" CssClass="label_text" /></td>
            <td><asp:Label ID="CurrentTCCWorkingBudget" runat="server" Width="80px" CssClass="label_text" /></td>-->
            <td><asp:Label ID="TCCCommitted" runat="server" Width="80px" CssClass="label_text" /></td>
            <td><asp:Label ID="TCCUncommitted" runat="server" Width="80px" CssClass="label_text" /></td>
            <td><asp:Label ID="TCCForecast" runat="server" Width="80px" CssClass="label_text" /></td>
            <td><asp:Label ID="TCCBalance" runat="server" Width="80px" CssClass="label_text" /></td>
            <td><asp:Label ID="TCCFYView" runat="server" Width="80px" CssClass="label_text" /></td>
            <td><asp:Label ID="TCCVariance" runat="server" Width="80px" CssClass="label_text" /></td>
            <td></td>
            <td colspan="2">* Actual/Snapshot/Forecast</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td align="left">A&amp;P+TCC</td>
            <td></td>
            <!--<td></td>
            <td></td>-->
            <td></td>
            <td></td>
            <td><asp:Label ID="Forecast" runat="server" Width="80px" CssClass="label_text" /></td>
            <td><asp:Label ID="Balance" runat="server" Width="80px" CssClass="label_text" /></td>
            <td><asp:Label ID="FYView" runat="server" Width="80px" Wrap="True" CssClass="label_text" /></td>
            <td><asp:Label ID="Variance" runat="server" Width="80px" CssClass="label_text" /></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <!--<tr>
            <td align="left">Coupons</td>
            <td><asp:TextBox ID="AllocatedCpnBudget1" runat="server" Width="80px" CssClass="label_text" /></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><asp:Label ID="CpnForecast1" runat="server" Width="80px" CssClass="label_text" /></td>
            <td><asp:Label ID="CpnBalance1" runat="server" Width="80px" CssClass="label_text" /></td>
            <td colspan="2">* Actual/Snapshot/Forecast</td>
        </tr>-->
        <tr>
            <td style="font-size:3px">&nbsp;</td>
        </tr>
    </table>
</div>