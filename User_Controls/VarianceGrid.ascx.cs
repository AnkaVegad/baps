﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Controls_VarianceGrid : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //populate selection dropdowns
            FillYear();
            FillBudgetResponsibilityArea();

            //get current year from session variable set in global.asax
            cboYear.SelectedValue = Session["SelectedYear"].ToString();

            DataTable tbl = UserDataAccess.SelectUser(Session["CurrentUserName"].ToString());
            //set area 
            if (Session["SelectedBudgetResponsibilityAreaID"] == null)
            {
                if (tbl.Rows[0]["LastBudgetResponsibilityAreaID"] == DBNull.Value)
                //allows for user's first login when table entry will be blank
                {
                    Session["SelectedBudgetResponsibilityAreaID"] = Common.ADOLookup("BudgetResponsibilityAreaID", "qryUserAuthorisations", "UserName = '" + Session["CurrentUserName"].ToString().Replace("'", "''") + "'");
                }
                else
                {
                    Session["SelectedBudgetResponsibilityAreaID"] = tbl.Rows[0]["LastBudgetResponsibilityAreaID"].ToString();
                }
            }
            cboBudgetResponsibilityArea.SelectedValue = Session["SelectedBudgetResponsibilityAreaID"].ToString();

        }        
        //populate the Variance grid 
        string strSQL;
        if (cboBudgetResponsibilityArea.SelectedValue == "1" || cboBudgetResponsibilityArea.SelectedValue == "5")
        {
            strSQL = "SELECT * FROM qryVarianceGridByCustomer ";
        }
        else
        {
            strSQL = "SELECT * FROM qryVarianceGridByCategory ";
        }
        strSQL += "WHERE FAYear = '" + cboYear.SelectedValue + "' ";
        if (cboBudgetResponsibilityArea.SelectedValue != "0")
        {
            strSQL += "AND BudgetResponsibilityAreaID = '" + cboBudgetResponsibilityArea.SelectedValue + "' ";
        } 
        sds.SelectCommand = strSQL;
        grd.DataSourceID = "sds";
        grd.DataBind();
        //select first row
        //if (!IsPostBack)
        //{
        //    grd.SelectedIndex = 0;
        //}
    }

#region properties

    //public string SelectedValue
    //{
    //    get { 
    //        try
    //        {
    //            return grd.SelectedValue.ToString(); 
    //        }
    //        catch { return "0"; }
    //    }
    //}

    public string HeaderText
    {
        set { lblHeader.Text = value; }
    }

#endregion

#region methods

    public void RefreshGrid()
    {
        grd.DataBind();
        grd.SelectedIndex = 0;
    }

    public void RefreshData()
    {
    }

#endregion
    
#region gridevents

    protected void grd_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void grd_PageIndexChanged(object sender, EventArgs e)
    {
        //grd.SelectedIndex = 0;
    }

    protected void grd_Sorted(object sender, EventArgs e)
    {
        //grd.SelectedIndex = 0;
    }

    protected void grd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            int intCurrent = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Month01"));
            if (intCurrent != 0)
            {
                if (intCurrent < 70 || intCurrent > 130) { e.Row.Cells[2].BackColor = Color.LightCoral; }
                else
                {
                    if (intCurrent < 90 || intCurrent > 110) { e.Row.Cells[2].BackColor = Color.LemonChiffon; } else { e.Row.Cells[2].BackColor = Color.LightGreen; }
                }
            }
            intCurrent = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Month02"));
            if (intCurrent != 0)
            {
                if (intCurrent < 70 || intCurrent > 130) { e.Row.Cells[3].BackColor = Color.LightCoral; }
                else
                {
                    if (intCurrent < 90 || intCurrent > 110) { e.Row.Cells[3].BackColor = Color.LemonChiffon; } else { e.Row.Cells[3].BackColor = Color.LightGreen; }
                }
            }
            intCurrent = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Month03"));
            if (intCurrent != 0)
            {
                if (intCurrent < 70 || intCurrent > 130) { e.Row.Cells[4].BackColor = Color.LightCoral; }
                else
                {
                    if (intCurrent < 90 || intCurrent > 110) { e.Row.Cells[4].BackColor = Color.LemonChiffon; } else { e.Row.Cells[4].BackColor = Color.LightGreen; }
                }
            }
            intCurrent = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Month04"));
            if (intCurrent != 0)
            {
                if (intCurrent < 70 || intCurrent > 130) { e.Row.Cells[5].BackColor = Color.LightCoral; }
                else
                {
                    if (intCurrent < 90 || intCurrent > 110) { e.Row.Cells[5].BackColor = Color.LemonChiffon; } else { e.Row.Cells[5].BackColor = Color.LightGreen; }
                }
            }
            intCurrent = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Month05"));
            if (intCurrent != 0)
            {
                if (intCurrent < 70 || intCurrent > 130) { e.Row.Cells[6].BackColor = Color.LightCoral; }
                else
                {
                    if (intCurrent < 90 || intCurrent > 110) { e.Row.Cells[6].BackColor = Color.LemonChiffon; } else { e.Row.Cells[6].BackColor = Color.LightGreen; }
                }
            }
            intCurrent = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Month06"));
            if (intCurrent != 0)
            {
                if (intCurrent < 70 || intCurrent > 130) { e.Row.Cells[7].BackColor = Color.LightCoral; }
                else
                {
                    if (intCurrent < 90 || intCurrent > 110) { e.Row.Cells[7].BackColor = Color.LemonChiffon; } else { e.Row.Cells[7].BackColor = Color.LightGreen; }
                }
            }
            intCurrent = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Month07"));
            if (intCurrent != 0)
            {
                if (intCurrent < 70 || intCurrent > 130) { e.Row.Cells[8].BackColor = Color.LightCoral; }
                else
                {
                    if (intCurrent < 90 || intCurrent > 110) { e.Row.Cells[8].BackColor = Color.LemonChiffon; } else { e.Row.Cells[8].BackColor = Color.LightGreen; }
                }
            }
            intCurrent = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Month08"));
            if (intCurrent != 0)
            {
                if (intCurrent < 70 || intCurrent > 130) { e.Row.Cells[9].BackColor = Color.LightCoral; }
                else
                {
                    if (intCurrent < 90 || intCurrent > 110) { e.Row.Cells[9].BackColor = Color.LemonChiffon; } else { e.Row.Cells[9].BackColor = Color.LightGreen; }
                }
            }
            intCurrent = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Month09"));
            if (intCurrent != 0)
            {
                if (intCurrent < 70 || intCurrent > 130) { e.Row.Cells[10].BackColor = Color.LightCoral; }
                else
                {
                    if (intCurrent < 90 || intCurrent > 110) { e.Row.Cells[10].BackColor = Color.LemonChiffon; } else { e.Row.Cells[10].BackColor = Color.LightGreen; }
                }
            }
            intCurrent = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Month10"));
            if (intCurrent != 0)
            {
                if (intCurrent < 70 || intCurrent > 130) { e.Row.Cells[11].BackColor = Color.LightCoral; }
                else
                {
                    if (intCurrent < 90 || intCurrent > 110) { e.Row.Cells[11].BackColor = Color.LemonChiffon; } else { e.Row.Cells[11].BackColor = Color.LightGreen; }
                }
            }
            intCurrent = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Month11"));
            if (intCurrent != 0)
            {
                if (intCurrent < 70 || intCurrent > 130) { e.Row.Cells[12].BackColor = Color.LightCoral; }
                else
                {
                    if (intCurrent < 90 || intCurrent > 110) { e.Row.Cells[12].BackColor = Color.LemonChiffon; } else { e.Row.Cells[12].BackColor = Color.LightGreen; }
                }
            }
            intCurrent = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Month12"));
            if (intCurrent != 0)
            {
                if (intCurrent < 70 || intCurrent > 130) { e.Row.Cells[13].BackColor = Color.LightCoral; }
                else
                {
                    if (intCurrent < 90 || intCurrent > 110) { e.Row.Cells[13].BackColor = Color.LemonChiffon; } else { e.Row.Cells[13].BackColor = Color.LightGreen; }
                }
            }
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            //e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            //e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(this.grd, "Select$" + e.Row.RowIndex);
        }
    }

#endregion

#region dropdowns

    protected void FillBudgetResponsibilityArea()
    //fill BudgetResponsibilityArea dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillBudgetResponsibilityArea";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        //cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboBudgetResponsibilityArea;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillYear()
    //fill year dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillYear";
        cmd.Parameters.AddWithValue("@ZeroValueText", "[None]");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboYear;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void cboBudgetResponsibilityArea_TextChanged(object sender, EventArgs e)
    {
        Session["SelectedBudgetResponsibilityAreaID"] = cboBudgetResponsibilityArea.SelectedValue;
        //UpdateUserSelections();
        //SelectFirstRow();
    }

    protected void cboYear_TextChanged(object sender, EventArgs e)
    {
        Session["SelectedYear"] = cboYear.SelectedValue;
        //SelectFirstRow();
    }

#endregion
}
