﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProjectGrid.ascx.cs" Inherits="User_Controls_ProjectGrid" %>

<div id="parent">
    <!--Header and Filter Criteria-->
    <div id="resizable1" class="resizable ui-widget-content">
        <br />
        <asp:Label ID="lblHeaderText" Text="Projects" runat="server" CssClass="page_header"></asp:Label>
        <br />
        <br />
        <table style="width:100%" border="0">
            <tr>
                <td>Area</td>
                <td>
                    <asp:DropDownList id="cboBudgetResponsibilityArea" runat="server" ontextchanged="cboBudgetResponsibilityArea_TextChanged" width="170px"
                        DataTextField="EntityName" DataValueField="ID" AutoPostBack="True"/>
                </td>
            </tr>
            <tr>
                <td>Customer</td>
                <td>
                    <asp:DropDownList id="cboCustomer" runat="server" ontextchanged="cboCustomer_TextChanged"  width="170px"
                        DataTextField="EntityName" DataValueField="ID" AutoPostBack="True"/>
                </td>
            </tr>
            <tr>
                <td>Category</td>
                <td>
                    <asp:DropDownList id="cboCategory" runat="server" ontextchanged="cboCategory_TextChanged"  width="170px"
                        DataTextField="EntityName" DataValueField="ID" AutoPostBack="True"/>
                </td>
            </tr>
            <tr>
                <td>Brand</td>
                <td>
                    <asp:DropDownList id="cboBrand" runat="server" ontextchanged="cboBrand_TextChanged"  width="170px"
                        DataTextField="EntityName" DataValueField="ID" AutoPostBack="True"/>
                </td>
            </tr>
            <tr>
                <td>Year</td>
                <td>
                    <asp:DropDownList id="cboYear" runat="server" ontextchanged="cboYear_TextChanged" AutoPostBack="True" 
                        DataTextField="EntityName" DataValueField="ID"/>
                </td>
            </tr>
            <tr>
                <td>Priority</td>
                <td>
                    <asp:DropDownList id="cboPulsingPlanPriorityMatchType" runat="server" ontextchanged="cboPulsingPlanPriorityMatchType_TextChanged" width="170px"
                        AutoPostBack="True"/>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:DropDownList id="cboPulsingPlanPriority" runat="server" ontextchanged="cboPulsingPlanPriority_TextChanged"  width="170px"
                        DataTextField="EntityName" DataValueField="ID" AutoPostBack="True"/>
                </td>
            </tr>
        </table><br /> 
    </div>  

    <!--Search and Grid-->
    <div id="resizable2" class="ui-widget-content">
        <!--Data Source - required to take advantage of sorting and paging functionality of the GridView; parameter removed 28/02/14 to solve filter problem with SQLDataSource when >101 records-->
        <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:APConnectionString %>" >
        </asp:SqlDataSource>

        <!--Grid-->
        <br />
                    <asp:Label ID="lblSearch" Text="Search Project Name" runat="server"></asp:Label>
                    <asp:TextBox ID="txtSearch" Runat="server" Width="150px"></asp:TextBox>
                    <asp:Button ID="cmdGo" Text="Go" Runat="server" CausesValidation="false" />
        <br /><br />
        <div id="project_grid">
            <asp:GridView ID="grd" runat="server" width="100%"
                AutoGenerateColumns="False" GridLines="Vertical" RowStyle-Wrap="false" EmptyDataText="No Records Found" 
                HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false" SelectedRowStyle-Wrap="false"
                onpageindexchanged="grd_PageIndexChanged" 
                onselectedindexchanged="grd_SelectedIndexChanged" AllowSorting="True" onsorted="grd_Sorted"
                DataKeyNames="ProjectToCustomerID" DataSourceID="sds" 
                onrowdatabound="grd_RowDataBound" ShowFooter="True">
                <Columns>
                    <asp:BoundField DataField="ProjectToCustomerID" ReadOnly="True" Visible="false" />
                    <asp:BoundField DataField="ProjectID" ReadOnly="True" Visible="false" />
                    <asp:BoundField DataField="ProjectName" HeaderText="Project" SortExpression="ProjectName" />
                    <asp:BoundField DataField="CustomerName" HeaderText="Customer" SortExpression="CustomerName" />
                    <asp:BoundField DataField="IONumber" HeaderText="IO" SortExpression="IONumber" />
                    <asp:BoundField DataField="SumOfAPBudget" HeaderText="A&P Bdgt" 
                        SortExpression="SumOfAPBudget" DataFormatString="{0:0}" 
                        ItemStyle-HorizontalAlign="Center" >
                    </asp:BoundField>
                    <asp:BoundField DataField="SumOfAPFYView" HeaderText="A&P FY Vw" 
                        SortExpression="SumOfAPFYView" DataFormatString="{0:0}" 
                        ItemStyle-HorizontalAlign="Center" >
                    </asp:BoundField>
                    <asp:BoundField DataField="SumOfAPVariance" HeaderText="A&P Var" 
                        SortExpression="SumOfAPVariance" DataFormatString="{0:0}" 
                        ItemStyle-HorizontalAlign="Center" >
                    </asp:BoundField>
                    <asp:BoundField DataField="SumOfTCCBudget" HeaderText="TCC Bdgt" 
                        SortExpression="SumOfTCCBudget" DataFormatString="{0:0}" 
                        ItemStyle-HorizontalAlign="Center" >
                    </asp:BoundField>
                    <asp:BoundField DataField="SumOfTCCFYView" HeaderText="TCC FY Vw" 
                        SortExpression="SumOfTCCFYView" DataFormatString="{0:0}" 
                        ItemStyle-HorizontalAlign="Center" >
                    </asp:BoundField>
                    <asp:BoundField DataField="SumOfTCCVariance" HeaderText="TCC Var" 
                        SortExpression="SumOfTCCVariance" DataFormatString="{0:0}" 
                        ItemStyle-HorizontalAlign="Center" >
                    </asp:BoundField>
                    <asp:BoundField DataField="SumOfFYView" HeaderText="Total FY Vw" 
                        SortExpression="SumOfFYView" DataFormatString="{0:0}" 
                        ItemStyle-HorizontalAlign="Center" >
                    </asp:BoundField>
                    <asp:BoundField DataField="SumOfVariance" HeaderText="Total Var" 
                        SortExpression="SumOfVariance" DataFormatString="{0:0}" 
                        ItemStyle-HorizontalAlign="Center" >
                    </asp:BoundField>
                    <asp:CommandField ShowSelectButton="True" Visible="False" />
                </Columns>
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle CssClass="grid_row" Wrap="False" BackColor="#EFF3FB" />
                <SelectedRowStyle CssClass="grid_selected" Wrap="False" BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="Gray" CssClass="grid_header" ForeColor="White"></HeaderStyle>
                <EditRowStyle BackColor="#2461BF" />
                <FooterStyle CssClass="grid_footer" Wrap="False" />
                <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" BackColor="White" />
            </asp:GridView>
            <asp:Label ID="txtMessage" runat="server" />
        </div>
    </div>
</div>

