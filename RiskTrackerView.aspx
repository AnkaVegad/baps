﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AP.master" AutoEventWireup="true" EnableEventValidation="false" CodeFile="RiskTrackerView.aspx.cs" Inherits="RiskTrackerView" %>

<%@ Register src="User_Controls/RiskTrackerGrid.ascx" tagname="RiskTrackerGrid" tagprefix="uc1" %>
<%@ Register src="User_Controls/RiskTrackerEdit.ascx" tagname="RiskTrackerEdit" tagprefix="uc2" %>
<%@ Register src="User_Controls/ProjectGrid.ascx" tagname="ProjectGrid" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:MultiView ID="mvw" runat="server">
    <asp:View ID="vw1" runat="server">
        <uc1:RiskTrackerGrid ID="RiskTrackerGrid1" runat="server" />
        <!--Grid Buttons-->
        <asp:Button ID="cmdNew" runat="server" Text="New" causesvalidation="false"
            onclick="cmdNew_Click" CssClass="form_button" enabled="true" width="70px" />&nbsp;
        <asp:Button ID="cmdEdit" runat="server" Text="Edit" causesvalidation="false"
            onclick="cmdEdit_Click" CssClass="form_button" enabled="true" width="70px" />&nbsp;
        <asp:Button ID="cmdDelete" runat="server" Text="Delete" causesvalidation="false" width="70px" 
            onclick="cmdDelete_Click" OnClientClick="return confirm('Are you sure you want to delete this Risk Tracker record?');"
            CssClass="form_button" Enabled="true" />
    </asp:View>
    <asp:View ID="vw2" runat="server">
        <table width="1100px">
        <tr><td style="font-size:2px">&nbsp;</td></tr>
        <tr>
            <td>
                <asp:Label ID="lblHeader" runat="server" CssClass="page_header" Text="Risk Tracker Item"></asp:Label><br />
            </td>
        </tr><tr><td style="font-size:2px">&nbsp;</td></tr>
        </table>
        <%if (Session["Mode"].ToString() == "New") {%>
            <!--Select Project Grid-->
            <uc3:ProjectGrid ID="ProjectGrid1" runat="server" />        
        <%}%>
        <!--Edit Area-->
        <uc2:RiskTrackerEdit ID="RiskTrackerEdit1" runat="server" /><br />
        <asp:Button ID="cmdUpdate" runat="server" Text="Save" causesvalidation="true"
            onclick="cmdUpdate_Click" CssClass="form_button" Enabled="true" validationgroup="RiskTracker" />&nbsp;
        <asp:Button ID="cmdCancel" runat="server" Text="Cancel" causesvalidation="false"
            onclick="cmdCancel_Click" CssClass="form_button" Enabled="True" />
    </asp:View>
</asp:MultiView>
</asp:Content>

