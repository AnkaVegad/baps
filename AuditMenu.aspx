<%@ Page Title="" Language="C#" MasterPageFile="~/AP.master" AutoEventWireup="true" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<!--Page Header-->
<table border="0" width="1095px">
    <tr>
        <td align="left">
            <asp:Label ID="Label11" Text="Audit Menu" runat="server" CssClass="page_header" Width="175px" />
        </td>
    </tr>
    <tr><td style="font-size:3px">&nbsp;</td></tr>
</table>

<!-- Buttons-->
<table border="0" width="1100">
    <tr><td>
            <asp:Button ID="cmdHistory" runat="server" Text="Audit History" causesvalidation="false" Enabled="true"
                width="130px" CssClass="form_button" PostBackUrl="Audit.aspx" />&nbsp;
    </td></tr>
    <tr><td>
            <asp:Button ID="cmdSnapshot" runat="server" Text="Snapshots" causesvalidation="false" Enabled="true"
                width="130px" CssClass="form_button" PostBackUrl="ForecastSnapshot.aspx" />&nbsp;
    </td></tr>
</table><br />
    
</asp:Content>

