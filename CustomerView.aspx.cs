﻿using System;

public partial class CustomerView : System.Web.UI.Page

{
    string strSingular = "Customer";

    protected void Page_Load(object sender, EventArgs e)
    {
        //check session not expired and high enough user level
        if (Session["SelectedProjectToCustomerID"] == null || Convert.ToInt16(Session["CurrentUserLevelID"]) >= 5)
        {
            Response.Redirect("ProjectView.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                mvw.ActiveViewIndex = 0;
                CustomerGrid1.HeaderText = strSingular;
            }
        }
    }

#region events

    protected void cmdUpdate_Click(object sender, EventArgs e)
    {
        string strMessage;
        try 
        {
            if (Page.IsValid)
            {
                if (Session["Mode"].ToString() == "New")
                {
                    strMessage = CustomerEdit1.InsertCustomer();
                }
                else
                {
                    strMessage = CustomerEdit1.UpdateCustomer();
                }
                if (strMessage != "")
                {
                    MsgBox("Message", strMessage);
                }
                else
                {
                    CustomerGrid1.RefreshGrid();
                    mvw.ActiveViewIndex = 0;
                }
            }
        }
        catch 
        {
            MsgBox("Message", "Could not save " + strSingular + ".");
        }
    }

    protected void cmdNew_Click(object sender, EventArgs e)
    {
        mvw.ActiveViewIndex = 1;
        //enable controls for new record
        CustomerEdit1.NewRecord();
        Session["Mode"] = "New";
        CustomerEdit1.HeaderText = Session["Mode"].ToString() + " " + strSingular;
    }

    protected void cmdEdit_Click(object sender, EventArgs e)
    {
        if (CustomerGrid1.SelectedValue != null)
        {
            int intSelectedCustomer = Convert.ToInt32(CustomerGrid1.SelectedValue);
            Customer c = CustomerDataAccess.SelectCustomer(intSelectedCustomer);
            CustomerEdit1.PopulateForm(c);
            mvw.ActiveViewIndex = 1;
            Session["Mode"] = "Edit";
            CustomerEdit1.HeaderText = Session["Mode"].ToString() + " " + strSingular;
        }
        //catch { } //in case no selection in grid
    }

    protected void cmdDelete_Click(object sender, EventArgs e)
    {
        try //in case referential integrity prevents deletion
        {
            CustomerDataAccess.DeleteCustomer(Convert.ToInt32(CustomerGrid1.SelectedValue));
            CustomerEdit1.ClearControls();
            // update grid
            CustomerGrid1.DataBind();
        }
        catch
        {
            MsgBox("Message", "Could not delete " + strSingular); 
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        CustomerEdit1.ClearControls();
        mvw.ActiveViewIndex = 0;
    }
    
#endregion

#region methods

    protected void MsgBox(string strTitle, string strMessage)
    {
        ClientScript.RegisterStartupScript(this.GetType(), strTitle, "alert('" + Common.Left(strMessage, 40) + "');", true);
    }

#endregion

}
