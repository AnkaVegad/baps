﻿using System;

public partial class ProductHierarchyView : System.Web.UI.Page

{
    string strSingular = "Product Hierarchy";

    protected void Page_Load(object sender, EventArgs e)
    {
        //check session not expired and high enough user level
        if (Session["SelectedProjectToCustomerID"] == null || Convert.ToInt16(Session["CurrentUserLevelID"]) >= 5)
        {
            Response.Redirect("ProjectView.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                mvw.ActiveViewIndex = 0;
                ProductHierarchyGrid1.HeaderText = strSingular;
            }
        }
    }

#region events

    protected void cmdUpdate_Click(object sender, EventArgs e)
    {
        string strMessage;
        try 
        {
            if (Page.IsValid)
            {
                if (Session["Mode"].ToString() == "New")
                {
                    strMessage = ProductHierarchyEdit1.InsertProductHierarchy();
                }
                else
                {
                    strMessage = ProductHierarchyEdit1.UpdateProductHierarchy();
                }
                if (strMessage != "")
                {
                    MsgBox("Message", strMessage);
                }
                else
                {
                    ProductHierarchyGrid1.RefreshGrid();
                    mvw.ActiveViewIndex = 0;
                }
            }
        }
        catch 
        {
            MsgBox("Message", "Could not save " + strSingular + ".");
        }
    }

    protected void cmdNew_Click(object sender, EventArgs e)
    {
        mvw.ActiveViewIndex = 1;
        //enable controls for new record
        ProductHierarchyEdit1.NewRecord();
        Session["Mode"] = "New";
        ProductHierarchyEdit1.HeaderText = Session["Mode"].ToString() + " " + strSingular;
    }

    protected void cmdEdit_Click(object sender, EventArgs e)
    {
        if (ProductHierarchyGrid1.SelectedValue != null)
        {
            int intSelectedProductHierarchy = Convert.ToInt32(ProductHierarchyGrid1.SelectedValue);
            ProductHierarchy c = ProductHierarchyDataAccess.SelectProductHierarchy(intSelectedProductHierarchy);
            ProductHierarchyEdit1.PopulateForm(c);
            mvw.ActiveViewIndex = 1;
            Session["Mode"] = "Edit";
            ProductHierarchyEdit1.HeaderText = Session["Mode"].ToString() + " " + strSingular;
        }
        //catch { } //in case no selection in grid
    }

    protected void cmdDelete_Click(object sender, EventArgs e)
    {
        try //in case referential integrity prevents deletion
        {
            ProductHierarchyDataAccess.DeleteProductHierarchy(Convert.ToInt32(ProductHierarchyGrid1.SelectedValue));
            ProductHierarchyEdit1.ClearControls();
            // update grid
            ProductHierarchyGrid1.DataBind();
        }
        catch
        {
            MsgBox("Message", "Could not delete " + strSingular); 
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        ProductHierarchyEdit1.ClearControls();
        mvw.ActiveViewIndex = 0;
    }
    
#endregion

#region methods

    protected void MsgBox(string strTitle, string strMessage)
    {
        ClientScript.RegisterStartupScript(this.GetType(), strTitle, "alert('" + Common.Left(strMessage, 35) + "');", true);
    }

#endregion

}
