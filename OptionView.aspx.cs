﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class OptionView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentUserName"] == null) { Response.Redirect("ProjectView.aspx"); }

        //Load the grid only the first time the page is loaded
        if (!Page.IsPostBack)
        {
             //Load the grid
             BindGrid();
        }
    }

    //Populate the GridView with data
    private void BindGrid()
    {
        //Get a data table object containing the LibraryImages
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.Text;
        string strSQL = "SELECT * FROM tblOption";
        cmd.CommandText = strSQL;
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        grd.DataSource = tbl;
        //bind the databound controls to the datasource
        grd.DataBind();
    }
    


    protected void grd_RowEditing(object sender, GridViewEditEventArgs e)
    {
        //set the row to enable edit mode for
        grd.EditIndex = e.NewEditIndex;
        //reload the grid
        BindGrid();
    }
    protected void grd_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        grd.EditIndex = -1;
        BindGrid();
    }
    protected void grd_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        //Retrieve updated data
        string id = grd.DataKeys[e.RowIndex].Value.ToString();
        string value = ((TextBox)grd.Rows[e.RowIndex].FindControl("txtOptionValue")).Text;
        //execute the update command
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.Text;
        string strSQL = "UPDATE tblOption SET OptionValue = '" + value + "' WHERE OptionName = '" + id + "'";
        cmd.CommandText = strSQL;
        int RowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        //cancel edit mode
        grd.EditIndex = -1;
        //bind the databound controls to the datasource
        BindGrid();
    }


}
