<%@ Page Title="" Language="C#" MasterPageFile="~/AP.master" AutoEventWireup="true" EnableEventValidation="false" CodeFile="ForecastSnapshotView.aspx.cs" Inherits="ForecastSnapshotView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <!--Page Header-->
    <table border="0" width="650px">
        <tr>
            <td align="left">
                <asp:Label ID="Label901" Text="Forecast Snapshots" runat="server" CssClass="page_header" />
            </td>
            <td align="right">Year</td>
            <td>
             <asp:DropDownList id="cboYear" runat="server" ontextchanged="cboYear_TextChanged" AutoPostBack="True" 
                DataTextField="EntityName" DataValueField="ID" />
            </td>
        </tr>
        <tr><td style="font-size:3px">&nbsp;</td></tr>
    </table>

    <!--Data Source - required to take advantage of sorting and paging functionality of the GridView-->
    <asp:SqlDataSource ID="sds" runat="server" 
        ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
        SelectCommandType="StoredProcedure" SelectCommand="SelectForecastSnapshotByYear">
        <SelectParameters>
           <asp:ControlParameter Name="SnapshotYear" ControlID="cboYear" PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>

    <!--ForecastSnapshot Grid-->
    <div id="project_grid">
        <asp:GridView ID="grd" runat="server" width="680px"
            AutoGenerateColumns="False" GridLines="Vertical"
            RowStyle-Wrap="false" EmptyDataText="No Records Found" 
            HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
            SelectedRowStyle-Wrap="false"             
            AllowSorting="True" onsorted="grd_Sorted" AllowPaging="False"
            OnRowDataBound="grd_RowDataBound"  
            DataKeyNames="UniqueKey" DataSourceID="sds">
            <Columns>
                <asp:BoundField DataField="UniqueKey" Visible="false" ReadOnly="true"/>
                <asp:BoundField DataField="SnapshotYear" HeaderText="Snapshot Year" SortExpression="SnapshotYear" />
                <asp:BoundField DataField="SnapshotMonth" HeaderText="Snapshot Month" SortExpression="SnapshotMonth" />
                <asp:BoundField DataField="SnapshotValue" HeaderText="Snapshot Value" SortExpression="SnapshotValue" />
                <asp:BoundField DataField="CreatedBy" HeaderText="Created By" SortExpression="CreatedBy" />
                <asp:BoundField DataField="CreatedDate" HeaderText="Date" SortExpression="CreatedDate" />
            </Columns>
            <RowStyle CssClass="grid_row" Wrap="False" />
            <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
            <HeaderStyle CssClass="grid_header" Wrap="False" />
            <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
        </asp:GridView>
    </div>
<br />
    <!--Grid Header-->
    <table border="0" width="650px">
        <tr>
            <td align="left">
                <asp:Label ID="Label1" Text="Budget Snapshots" runat="server" CssClass="page_header" />
            </td>
        </tr>
        <tr><td style="font-size:3px">&nbsp;</td></tr>
    </table>

    <!--Data Source - required to take advantage of sorting and paging functionality of the GridView-->
    <asp:SqlDataSource ID="sdsBudget" runat="server" 
        ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
        SelectCommandType="StoredProcedure" SelectCommand="sp_ap_SelectBudgetSnapshotByYear">
        <SelectParameters>
           <asp:ControlParameter Name="SnapshotYear" ControlID="cboYear" PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>

    <!--BudgetSnapshot Grid-->
    <div id="budget_snapshot_grid">
        <asp:GridView ID="grdBudget" runat="server" width="680px"
            AutoGenerateColumns="False" GridLines="Vertical"
            RowStyle-Wrap="false" EmptyDataText="No Records Found" 
            HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
            SelectedRowStyle-Wrap="false"             
            AllowSorting="True" onsorted="grd_Sorted" AllowPaging="False"
            OnRowDataBound="grd_RowDataBound"  
            DataKeyNames="UniqueKey" DataSourceID="sdsBudget">
            <Columns>
                <asp:BoundField DataField="UniqueKey" Visible="false" ReadOnly="true"/>
                <asp:BoundField DataField="SnapshotYear" HeaderText="Snapshot Year" SortExpression="SnapshotYear" />
                <asp:BoundField DataField="SnapshotMonth" HeaderText="Snapshot Month" SortExpression="SnapshotMonth" />
                <asp:BoundField DataField="BudgetValue" HeaderText="Snapshot Value" SortExpression="BudgetValue" />
                <asp:BoundField DataField="CreatedBy" HeaderText="Created By" SortExpression="CreatedBy" />
                <asp:BoundField DataField="CreatedDate" HeaderText="Date" SortExpression="CreatedDate" />
            </Columns>
            <RowStyle CssClass="grid_row" Wrap="False" />
            <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
            <HeaderStyle CssClass="grid_header" Wrap="False" />
            <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
        </asp:GridView>
    </div>

    <br/>Create next forecast and budget snapshots for month:&nbsp;
    <asp:DropDownList id="cboSnapshotMonthYear" runat="server" 
        DataTextField="EntityName" DataValueField="ID" />
    <asp:Button ID="cmdNew" runat="server" Text="Create" enabled="true"
        OnClick="cmdNew_Click" CssClass="form_button"
        OnClientClick="return confirm('Are you sure you want to create this snapshot?');" />&nbsp;
    <asp:Button ID="cmdDelete" runat="server" Text="Delete Selected Monthly Snapshots" enabled="true"
        onclick="cmdDelete_Click" CssClass="form_button" width="210px"
        OnClientClick="return confirm('Are you sure you want to delete this record?');" />

</asp:Content>

