﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class NewBudgets : System.Web.UI.Page
{


    protected void Page_Load(object sender, EventArgs e)
    {

        if (Convert.ToInt16(Session["CurrentUserLevelID"]) > 25)
        {
            Response.Redirect("ProjectView_rs.aspx");
        }
        if (!IsPostBack)
        {
            mvw.ActiveViewIndex = 0;
            //int BL = BudgetGrid1.getBudgetLevel;
            //if (Convert.ToInt16(Session["CurrentUserLevelID"]) == 1 && (BL == 1 || BL == 0))
            if (Convert.ToInt16(Session["CurrentUserLevelID"]) == 1)
            {
                cmdNew.Enabled = false;
                cmdDelete.Enabled = false;
            }
            else
            {
                cmdNew.Visible = false;
                cmdDelete.Visible = false;
            }
        
           
            //ScriptManager.RegisterStartupScript(this, GetType(), "error", "alert('" + Convert.ToString(BL) + "');", true);

        }

    }

    protected void cmdNew_Click(object sender, EventArgs e)
    {
        try
        {

            mvw.ActiveViewIndex = 0;
            cmdCancel.Enabled = true;

        }
        catch { } //in case no selected item in grid
    }
    protected void cmdDelete_Click(object sender, EventArgs e)
    {
        int BudgetId = Convert.ToInt32(BudgetGrid1.SelectedValue);
  
     
        //ScriptManager.RegisterStartupScript(this, GetType(), "error", "alert('" + BudgetId + "');", true);

        //AddBudgetGrid1.DeleteBudget(BudgetId);
        BudgetGrid1.FillBudgetGrid();
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        BudgetGrid1.FillBudgetGrid();
        mvw.ActiveViewIndex = 0;
    }



}