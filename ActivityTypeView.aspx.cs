﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ActivityTypeView : System.Web.UI.Page
{
    string strSingular = "Activity Type";
    string strPlural = "Activity Types";
 
    protected void Page_Load(object sender, EventArgs e)
    {
        //check session not expired and high enough user level
        if (Session["SelectedProjectToCustomerID"] == null || Convert.ToInt16(Session["CurrentUserLevelID"]) >= 5)
        {
            Response.Redirect("ProjectView.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                mvw.ActiveViewIndex = 0;
                ActivityTypeGrid1.FillDropdowns();
                ActivityTypeGrid1.PopulateGrid();
                ActivityTypeGrid1.HeaderText = strPlural;
            }
        }
    }

#region events

    protected void cmdGo_Click(object sender, EventArgs e)
    {
        ActivityTypeEdit1.ClearForm();
    }

    protected void cmdEdit_Click(object sender, EventArgs e)
    {
        if (ActivityTypeGrid1.SelectedValue != null)
        {
            ActivityTypeEdit1.PopulateForm(ActivityTypeDataAccess.SelectActivityType(Convert.ToInt32(ActivityTypeGrid1.SelectedValue)));
            mvw.ActiveViewIndex = 1;
            Session["Mode"] = "Edit";
            ActivityTypeEdit1.HeaderText = Session["Mode"].ToString() + " " + strSingular;
        }
    }

    protected void cmdUpdate_Click(object sender, EventArgs e)
    {
        //try
        //{
            if (Session["Mode"].ToString() == "New")
            {
                string strMessage = ActivityTypeEdit1.InsertActivityType();
                if (strMessage != "")
                {
                    MsgBox("Message", strMessage) ;
                }
                else
                {
                    ActivityTypeGrid1.RefreshGrid();
                    mvw.ActiveViewIndex = 0;
                }
            }
            else
            {
                ActivityTypeEdit1.UpdateActivityType();
                ActivityTypeGrid1.RefreshGrid();
                mvw.ActiveViewIndex = 0;
            }
        //}
        //catch
        //{
        //    MsgBox("Message", "Could not save " + strSingular + ".");
        //}
    }

    protected void cmdNew_Click(object sender, EventArgs e)
    {
        mvw.ActiveViewIndex = 1;
        //enable controls for new record
        ActivityTypeEdit1.NewRecord();
        Session["Mode"] = "New";
        ActivityTypeEdit1.HeaderText = Session["Mode"].ToString() + " " + strSingular;
    }

    protected void cmdDelete_Click(object sender, EventArgs e)
    {
        try
        {
            if (ActivityTypeGrid1.SelectedValue != null)
            {
                ActivityTypeDataAccess.DeleteActivityType(Convert.ToInt32(ActivityTypeGrid1.SelectedValue));
                ActivityTypeEdit1.ClearForm();
                // update grid
                ActivityTypeGrid1.DataBind();
            }
        }
        catch //if item has non-cascaded dependencies
        {
            MsgBox("Error", "Could not delete " + strSingular);
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        ActivityTypeEdit1.ClearForm();
        //ResetButtons();
//        ActivityTypeGrid1.SelectedIndex = -1;
        mvw.ActiveViewIndex = 0;
    }

#endregion

#region methods

    protected void MsgBox(string strTitle, string strMessage)
    {
        ClientScript.RegisterStartupScript(this.GetType(), strTitle, "alert('" + Common.Left(strMessage, 35) + "');", true);
    }

#endregion
}
