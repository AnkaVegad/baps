﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paste : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //populate clipboard items grid
            DataTable tbl = (DataTable)Session["SelectedItems"];
            grd.DataSource = tbl;
            grd.DataBind();
            //project grid settings
            ProjectGrid1.HeaderText = "Paste To";
            ProjectGrid1.GridPageSize = 10;
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        if (ProjectGrid1.SelectedValue == "")
        {
            MsgBox("", "Select a Project");
        }
        else
        {
            if (grd.Rows.Count == 0)
            {
                MsgBox("", "No activities selected");
            }
            else
            {
                string strMessage = "";
                int intActivityID = 0;
                int intCount = 0;
                foreach (GridViewRow gr in grd.Rows)
                {
                    intCount += 1;
                    intActivityID = Convert.ToInt32(grd.DataKeys[gr.RowIndex].Value);
                    strMessage = ReallocationDataAccess.CopyActivityToProject(intActivityID, Convert.ToInt32(ProjectGrid1.SelectedValue), Session["CurrentUserName"].ToString());
                }
                if (strMessage == "")
                {
                    Response.Redirect("Default.aspx");    //leave screen
                    //MsgBox("", "Copy/Paste Successful"); can't get this to display
                }
                else
                {
                    MsgBox("", strMessage);
                }
            }
        }
    }

    protected void cmdBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx");
    }

    protected void MsgBox(string strTitle, string strMessage)
    {
        ClientScript.RegisterStartupScript(this.GetType(), strTitle, "alert('" + Common.Left(strMessage, 35) + "');", true);
    }


}
