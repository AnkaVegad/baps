<%@ Page Title="" Language="C#" MasterPageFile="~/AP.master" EnableEventValidation="false" AutoEventWireup="true" CodeFile="POHeader.aspx.cs" Inherits="POHeader" %>

<%@ Register src="User_Controls/ActivityEdit.ascx" tagname="ActivityEdit" tagprefix="uc1" %>
<%@ Register src="User_Controls/SupplierGrid.ascx" tagname="SupplierGrid" tagprefix="uc2" %>
<%@ Register src="User_Controls/ActivityTypeGrid.ascx" tagname="ActivityTypeGrid" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:MultiView ID="mvw" runat="server">
    <asp:View ID="vw1" runat="server">
        <script type="text/javascript" language="javascript">
            window.onbeforeunload = confirmExit;

            function setNeedToConfirm(bool) {
                //set isChanged flag
                var ctl = document.all.ctl00$ContentPlaceHolder1$ActivityEdit1$txtChanged
                if (bool) { ctl.value = "1"; }
                else { ctl.value = "0"; }
            }

            function isChanged() {
                //only save if some data have changed
                var isChanged = (document.all.ctl00$ContentPlaceHolder1$ActivityEdit1$txtChanged.value == "1");
                setNeedToConfirm(false);
                return isChanged;
            }

            function confirmExit() {
                //display a message if user has made changes and is navigating away without clicking Save
                if (document.all.ctl00$ContentPlaceHolder1$ActivityEdit1$txtChanged.value == "1") {
                    return "You will lose any unsaved changes."
                }
            }

            function openPopUp() {
                //open the print preview in a popup window
                {
                    window.open("POHeaderPrint.aspx", "Print");
                }
            }  
        </script>
        <!--Page Header-->
        <table border="0" width="1095px">
            <tr>
                <td align="left">
                    <asp:Label ID="Label1" Text="Purchase Requisition" runat="server" CssClass="page_header"></asp:Label>
                    <asp:Label ID="POID" runat="server" visible="false" Width="30px" />
                    <asp:Label ID="ProjectToCustomerID" runat="server" visible="false" Width="30px" CssClass="label_text" />
                    <asp:Label ID="BudgetResponsibilityAreaID" runat="server" visible="false" Width="30px" CssClass="label_text" />
                </td>
                <td align="left">
                    <asp:TextBox ID="ProjectID" runat="server" visible="false"></asp:TextBox>
                    <asp:Label ID="Label901" Text="Project" runat="server" CssClass="page_header" />
                </td>
                <td align="left">
                    <asp:Label ID="ProjectName" runat="server" CssClass="page_header_text" Width="250px" />
                </td>
                <td align="left">
                    <asp:TextBox ID="CustomerID" runat="server" visible="false"></asp:TextBox>
                    <asp:Label ID="Label902" Text="Customer" runat="server" CssClass="page_header"></asp:Label>
                </td>
                <td align="left">
                    <asp:Label ID="CustomerName" runat="server" CssClass="page_header_text" Width="150px" />
                </td>
                <td align="left">
                    <asp:Label ID="Label903" Text="IO Number" runat="server" CssClass="page_header"></asp:Label>
                </td>
                <td align="left">
                    <asp:Label ID="IONumber" runat="server" CssClass="page_header_text" Width="110px" />
                </td>
            </tr>
        </table>
        
        <!--Upper Edit Area-->
        <table border="0" width="1100px"><tr><td valign="top">
            <table>
                <tr><td valign="top">
                    <table border="0">
                        <tr><td>Supplier<asp:Label ID="SupplierID" runat="server" Width="30px" visible="false" /></td><td>
                            <asp:TextBox ID="SupplierName" runat="server" Width="208px" enabled="false" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*Required"
                                controltovalidate="SupplierName" Display="Dynamic"  ValidationGroup="POHeader" />
                            <asp:Button ID="cmdSupplierLookup" style="border:1px solid gray;width:50px" Text="Lookup" Runat="server" onclick="cmdSupplierLookup_Click" CausesValidation="false" />
                        </td></tr>
                        <tr><td>SAP Vendor Code</td><td>
                            <asp:TextBox ID="SAPVendorCode" runat="server" Width="80px" enabled="false" />
                        </td></tr>
                        <tr><td valign="top">Comments</td><td width="15">
                            <asp:TextBox ID="Comments" runat="server" Width="276px" MaxLength="255" 
                                Enabled="True" Height="58px" TextMode="MultiLine" />
                        </td></tr>
                    </table>
                </td>
                <td valign="top">
                    <table border="0">
                        <tr><td>Delivery Address</td><td width="180">
                            <asp:DropDownList ID="DeliveryAddressID" runat="server" Width="180px" 
                                AutoPostBack="true" DataTextField="EntityName" DataValueField="ID" />
                            <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="*Required" 
                                ControlToValidate="DeliveryAddressID" Display="Dynamic" MaximumValue="ZZZ" 
                                MinimumValue="1" ValidationGroup="POHeader" />
                        </td></tr>
                        <tr><td></td><td>
                            <asp:Label ID="DeliveryAddress" runat="server" Width="177px" CSSClass="pseudo_text_box" Height="80px" />
                        </td></tr>               
                    </table>
                </td>
                <td valign="top">
                    <table style="width:300px;" border="0">
                        <tr><td>Requisition By</td><td>
                            <asp:TextBox ID="RequisitionBy" runat="server" Width="144px" enabled="false" />
                        </td></tr>
                        <tr><td>NPI Coordinator</td><td>
                            <asp:DropDownList ID="NPICoordinator" runat="server" Width="150px" DataTextField="EntityName" DataValueField="ID" />
                            <asp:RangeValidator ID="RangeValidator3" runat="server" ErrorMessage="*Required" 
                                ControlToValidate="NPICoordinator" Display="Dynamic" MaximumValue="zzz" MinimumValue="1" 
                                ValidationGroup="POHeader" />
                        </td></tr>
                        <tr><td>Approver</td><td>
                            <asp:DropDownList ID="ReleasedBy" runat="server" Width="150px" DataTextField="EntityName" DataValueField="ID" />
                            <asp:RangeValidator ID="RangeValidator4" runat="server" ErrorMessage="*Required" 
                                ControlToValidate="ReleasedBy" Display="Dynamic" MaximumValue="zzz" MinimumValue="1" 
                                ValidationGroup="POHeader" />
                        </td></tr>
                        <tr><td>PR Number</td><td>
                            <asp:TextBox ID="PRNumber" runat="server" Width="80px" MaxLength="10" />
                        </td></tr>
                        <tr><td>PO Number</td><td>
                            <asp:TextBox ID="PONumber" runat="server" Width="80px" MaxLength="10" />
                            <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="*PR Number must be entered" 
                                Display="Dynamic" onservervalidate="CustomValidator1_ServerValidate" ValidationGroup="POHeader" />
                        </td></tr>
                    </table>
                </td>
                <td valign="top">
                    <table style="width:75px;" border="0">
                        <tr><td align="right">
                            <asp:Button ID="cmdUpdate" runat="server" Text="Save" causesvalidation="true"
                                onclick="cmdUpdate_Click" CssClass="form_button" Enabled="true" ValidationGroup="POHeader" />            
                        </td></tr><tr><td align="right">
                            <asp:Button ID="cmdCancel" runat="server" Text="Cancel" causesvalidation="false"
                                onclick="cmdCancel_Click" CssClass="form_button" Enabled="true" />
                        </td></tr><tr><td align="right">
                            <asp:Button ID="cmdPrint" runat="server" Text="Print" causesvalidation="false"
                                onclientclick="Javascript:openPopUp();" CssClass="form_button" Enabled="true" />
                        </td></tr>
                    </table>
                </td></tr>
                <tr>
                    <td>
                        Line Items
                    </td>
                </tr>

            </table>
        </td></tr></table>

        <!--Grid-->
        <div id="poheader_grid">
        <asp:GridView ID="grd" runat="server" width="950px"
            AutoGenerateColumns="False" GridLines="Vertical" RowStyle-Wrap="false" EmptyDataText="No Activities" 
            HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
            SelectedRowStyle-Wrap="false" AllowSorting="True" onsorting="grd_Sorting"
            onselectedindexchanged="grd_SelectedIndexChanged" onrowdatabound="grd_RowDataBound"
            DataKeyNames="ActivityID" ShowFooter="True">
            <Columns>
                <asp:BoundField DataField="ActivityID" InsertVisible="False" ReadOnly="True" Visible="False" />
                <asp:BoundField DataField="ShortText" HeaderText="Short Text" SortExpression="ShortText" />
                <asp:BoundField DataField="ActivityTypeName" HeaderText="Type" SortExpression="ActivityTypeName" />
                <asp:BoundField DataField="GLCode" HeaderText="G/L Code" SortExpression="GLCode" />
                <asp:BoundField DataField="CommodityCode" HeaderText="Commodity" SortExpression="CommodityCode" />
                <asp:BoundField DataField="SupplierRefNo" HeaderText="Supplier's Ref" SortExpression="SupplierRefNo" />
                <asp:BoundField DataField="ItemQuantity" HeaderText="Quantity" SortExpression="ItemQuantity" />
                <asp:BoundField DataField="MonthShortName" HeaderText="Month" SortExpression="MonthRequired" />
                <asp:BoundField DataField="UnitPrice" HeaderText="Unit Price" SortExpression="POItemNo" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:0.00}" />
                <asp:BoundField DataField="TotalCost" HeaderText="Total Cost" SortExpression="POItemNo" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:0.00}" />
            </Columns>
            <RowStyle CssClass="grid_row" Wrap="False" />
            <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
            <HeaderStyle CssClass="grid_header" Wrap="False" />
            <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
        </asp:GridView>
        </div>
        <table><tr>
            <td>Currency</td>
            <td>
                <asp:Label ID="CurrencyCode" runat="server" visible="true" Width="30px" CssClass="pseudo_text_box" />
            </td>
        </tr></table>

        <!--Activity Buttons-->
        <table style="width:850px;" border="0">
            <tr><td>Unallocated Activities:</td><td align="right">
                <asp:DropDownList ID="cboSelectItem" runat="server" Width="400px" DataTextField="EntityName" DataValueField="ID">
                </asp:DropDownList>            
            </td><td align="right">
                <asp:Button ID="cmdAdd" runat="server" Text="Add Item" 
                onclick="cmdAdd_Click" CssClass="form_button" Enabled="false" CausesValidation="false"/>
            </td><td align="right">
                <asp:Button ID="cmdEdit" runat="server" Text="Edit Item" 
                onclick="cmdEdit_Click" CssClass="form_button" Enabled="false" CausesValidation="false"/>
            </td><td align="right">
                <asp:Button ID="cmdNew" runat="server" Text="New Item" 
                onclick="cmdNew_Click" CssClass="form_button" Enabled="false" CausesValidation="false"/>
            </td><td align="right">
                <asp:Button ID="cmdRemove" runat="server" Text="Remove Item" Width="100px"
                onclick="cmdRemove_Click" CssClass="form_button" Enabled="false" CausesValidation="false"/>
            </td></tr>
        </table><br /><br />

        <!--Lower Edit Area-->
        <div id="edit_area">
            <table><tr><td valign="top">
                <uc1:ActivityEdit ID="ActivityEdit1" runat="server" />
            </td><td valign="top">
                <table style="width:85px;" border="0">
                    <tr><td align="right">
                        <asp:Button ID="cmdSaveActivity" runat="server" Text="Save" CausesValidation="true"
                        onclick="cmdSaveActivity_Click" CssClass="form_button" Enabled="False" 
                        onclientclick="return isChanged();" ValidationGroup="Activity" width="85px" />            
                    </td></tr>
                    <tr><td align="right">
                        <asp:Button ID="cmdCancelActivity" runat="server" Text="Cancel" causesvalidation="false"
                        onclick="cmdCancelActivity_Click" CssClass="form_button" Enabled="False" 
                        onclientclick="Javascript:setNeedToConfirm(false);" width="85px" />
                    </td></tr>
                    <tr><td align="right">
                        <asp:Button ID="cmdActivityTypeLookup" Text="Activity Type" Runat="server" 
                            onclick="cmdActivityTypeLookup_Click" onclientclick="Javascript:setNeedToConfirm(false);"
                            CssClass="form_button" CausesValidation="false" enabled="false" width="85px" />
                    </td></tr>                
                </table>
            </td></tr></table>
        </div><br /><br />    

        <!-- Supplier ModalPopupExtender -->
        <asp:Button id="dummyButton" runat="server" style="display:none;" />
        <!-- Supplier ModalPopupExtender -->
        <asp:ModalPopupExtender ID="mp1" runat="server" PopupControlID="Panel1" TargetControlID="dummyButton" BackgroundCssClass="modalBackground" />
        <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" style="display:none" >    
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <!--Supplier Selection-->
                    <uc2:SupplierGrid ID="SupplierGrid1" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
            <!--Supplier Buttons-->
            <asp:Button ID="cmdSaveSupplier" runat="server" Text="Save" onclick="cmdSaveSupplier_Click" CssClass="form_button" 
                Enabled="true" CausesValidation="true"/>
            <asp:Button ID="cmdCancelSupplier" runat="server" Text="Cancel" onclick="cmdCancelSupplier_Click" CssClass="form_button" 
                Enabled="true" CausesValidation="false"/>
            </asp:Panel>
        <!-- ModalPopupExtender -->

        <!-- ActivityType ModalPopupExtender -->
        <asp:ModalPopupExtender ID="mp2" runat="server" PopupControlID="Panel2" TargetControlID="dummyButton" 
            BackgroundCssClass="modalBackground" />
        <asp:Panel ID="Panel2" runat="server" CssClass="modalPopup" style = "display:none">    
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <!--Activity Type Selection-->
                    <uc3:ActivityTypeGrid ID="ActivityTypeGrid1" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
            <!--ActivityType Buttons-->
            <asp:Button ID="cmdSaveActivityType" runat="server" Text="Save" onclick="cmdSaveActivityType_Click" 
                CssClass="form_button" Enabled="true" CausesValidation="true"/>
            <asp:Button ID="cmdCancelActivityType" runat="server" Text="Cancel" onclick="cmdCancelSupplier_Click" 
                CssClass="form_button" Enabled="true" CausesValidation="false"/>
        </asp:Panel>
        <!-- ModalPopupExtender -->

    </asp:View>
    <asp:View ID="vw2" runat="server">
    </asp:View>
</asp:MultiView>
</asp:Content>

