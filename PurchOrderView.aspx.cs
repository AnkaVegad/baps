﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PurchOrderView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //check for session expired - changed from SelectedProjectToCustomerID 21/12/12
        if (Session["CurrentUserName"] == null)
        {
            Response.Redirect("ProjectView.aspx");
        }
        if (!IsPostBack)
        {
            //populate selection dropdowns
            FillCustomer();
            FillYear();
            FillBrand();
            FillCategory();
            FillBudgetResponsibilityArea();

            //get current year from session
            cboYear.SelectedValue = Session["SelectedYear"].ToString();
            //get user settings
            DataTable tbl = UserDataAccess.SelectUser(Session["CurrentUserName"].ToString());

            //set area - revised 
            if (Session["SelectedBudgetResponsibilityAreaID"] == null)
            {
                if (tbl.Rows[0]["LastBudgetResponsibilityAreaID"] == DBNull.Value)
                //allows for user's first login when table entry will be blank
                {
                    Session["SelectedBudgetResponsibilityAreaID"] = Common.ADOLookup("BudgetResponsibilityAreaID", "qryUserAuthorisations", "UserName = '" + Session["CurrentUserName"].ToString().Replace("'", "''") + "'");
                }
                else
                {
                    Session["SelectedBudgetResponsibilityAreaID"] = tbl.Rows[0]["LastBudgetResponsibilityAreaID"].ToString();
                }
            }
            try { cboBudgetResponsibilityArea.SelectedValue = Session["SelectedBudgetResponsibilityAreaID"].ToString(); }
            catch { cboBudgetResponsibilityArea.SelectedValue = "0"; }

            if (Session["SelectedCustomerID"] == null)
            {
                Session["SelectedCustomerID"] = tbl.Rows[0]["LastCustomerID"].ToString();
            }
            try { cboCustomer.SelectedValue = Session["SelectedCustomerID"].ToString(); }
            catch { cboCustomer.SelectedValue = "0"; }

            if (Session["SelectedBrandID"] == null)
            {
                Session["SelectedBrandID"] = tbl.Rows[0]["LastBrandID"].ToString();
            }
            try { cboBrand.SelectedValue = Session["SelectedBrandID"].ToString(); }
            catch { cboBrand.SelectedValue = "0"; }

            if (Session["SelectedCategoryID"] == null)
            {
                Session["SelectedCategoryID"] = tbl.Rows[0]["LastCategoryID"].ToString();
            }
            try { cboCategory.SelectedValue = Session["SelectedCategoryID"].ToString(); }
            catch { cboCategory.SelectedValue = "0"; }
        }

        string strSQL = "SELECT TOP 101 dbo.qryPurchOrderViewInput.POID, dbo.qryPurchOrderViewInput.PONumber, dbo.qryPurchOrderViewInput.ProjectID, dbo.qryPurchOrderViewInput.CustomerID, ";
        strSQL += "dbo.qryPurchOrderViewInput.ProjectToCustomerID, dbo.qryPurchOrderViewInput.ProjectName, ";
        strSQL += "dbo.qryPurchOrderViewInput.CustomerName, dbo.qryPurchOrderViewInput.IONumber, ";
        strSQL += "dbo.qryPurchOrderViewInput.ProjectYear, dbo.qryPurchOrderViewInput.SupplierName, ";
        strSQL += "SUM(dbo.qryPurchOrderViewInput.Receipted) AS SumOfReceipted, ";
        strSQL += "SUM(dbo.qryPurchOrderViewInput.RcptRequested) AS SumOfRcptRequested, ";
        strSQL += "SUM(dbo.qryPurchOrderViewInput.PtReceipted) AS SumOfPtReceipted, ";
        strSQL += "SUM(dbo.qryPurchOrderViewInput.POValue) AS SumOfPOValue ";
        strSQL += "FROM dbo.qryPurchOrderViewInput LEFT OUTER JOIN dbo.tblProductHierarchy ON dbo.qryPurchOrderViewInput.SubBrandID = dbo.tblProductHierarchy.SubBrandID ";
        strSQL += "WHERE ProjectYear = '" + cboYear.SelectedValue + "' ";
        if (cboCustomer.SelectedValue != "0")
        {
            strSQL += "AND CustomerID = " + cboCustomer.SelectedValue + " ";
        }
        if (cboBrand.SelectedValue != "0")
        {
            strSQL += "AND tblProductHierarchy.BrandID = " + cboBrand.SelectedValue + " ";
        }
        if (cboCategory.SelectedValue != "0")
        {
            strSQL += "AND UKCategoryID = " + cboCategory.SelectedValue + " ";
        }
        if (cboBudgetResponsibilityArea.SelectedValue != "0")
        {
            strSQL += "AND BudgetResponsibilityAreaID = " + cboBudgetResponsibilityArea.SelectedValue + " ";
        }
        strSQL += "AND EXISTS(SELECT 'X' FROM qryUserAuthorisations WHERE UserName = '" + Session["CurrentUserName"].ToString().Replace("'", "''") + "' AND (qryUserAuthorisations.BrandID = dbo.tblProductHierarchy.BrandID OR qryUserAuthorisations.BrandID = 0) AND (qryUserAuthorisations.CustomerID = dbo.qryPurchOrderViewInput.CustomerID OR qryUserAuthorisations.CustomerID = 0) AND (qryUserAuthorisations.BudgetResponsibilityAreaID = qryPurchOrderViewInput.BudgetResponsibilityAreaID)) ";
        strSQL += "GROUP BY dbo.qryPurchOrderViewInput.POID, dbo.qryPurchOrderViewInput.PONumber, dbo.qryPurchOrderViewInput.ProjectID, dbo.qryPurchOrderViewInput.CustomerID, dbo.qryPurchOrderViewInput.ProjectToCustomerID, dbo.qryPurchOrderViewInput.ProjectName, ";
        strSQL += "dbo.qryPurchOrderViewInput.CustomerName, dbo.qryPurchOrderViewInput.IONumber, dbo.qryPurchOrderViewInput.ProjectYear, dbo.qryPurchOrderViewInput.SupplierName ";
        strSQL += "ORDER BY PONumber;";
        sds.SelectCommand = strSQL;
        grd.DataBind();

        //added 24/12/12 - ensure POID selected after page load
        if (!IsPostBack)
        {
            //select first row
            grd.SelectedIndex = 0;
            try //in case no rows in grid
            {
                Session["SelectedPOID"] = grd.SelectedValue.ToString();
            }
            catch
            { }
        }
        
        //max rows retrieved message
        if (grd.PageCount == 5 && grd.PageSize == 25)
        {
            txtMessage.Text = "The maximum number of rows has been retrieved; to see other POs, change your selection criteria.";
        }
        else
        {
            txtMessage.Text = String.Empty;
        }
    }

#region gridevents

    protected void grd_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["SelectedPOID"] = grd.SelectedValue.ToString();
    }

    protected void grd_PageIndexChanged(object sender, EventArgs e)
    {
        grd.SelectedIndex = -1;
    }

    protected void grd_Sorted(object sender, EventArgs e)
    {
        grd.SelectedIndex = -1;
    }

    int intTotalReceipted = 0;
    int intTotalPOValue = 0;
    protected void grd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            int intCurrentReceipted = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "SumOfReceipted"));
            int intCurrentPOValue = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "SumOfPOValue"));
            //colour code the cell 
            if (Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "SumOfReceipted")) == Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "SumOfPOValue"))) { e.Row.Cells[7].BackColor = Color.LightGreen; } else { e.Row.Cells[7].BackColor = Color.PapayaWhip; }
            //increment the totals
            intTotalReceipted += intCurrentReceipted;
            intTotalPOValue += intCurrentPOValue;
            //highlight on hover
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            e.Row.Attributes["onclick"] = cs.GetPostBackClientHyperlink(this.grd, "Select$" + e.Row.RowIndex);
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[5].Text = "Total";
            //display the total
            e.Row.Cells[6].Text = intTotalPOValue.ToString();
            e.Row.Cells[7].Text = intTotalReceipted.ToString();
            //colour code the cell 
            if (intTotalPOValue == intTotalReceipted) { e.Row.Cells[7].BackColor = Color.LightGreen; } else { e.Row.Cells[7].BackColor = Color.PapayaWhip; }
        }
    }

#endregion

#region methods

    protected void SelectFirstRow()
    {
        try
        {
            grd.SelectedIndex = 0;
            Session["SelectedPOID"] = grd.SelectedValue;
        }
        catch { }
    }

#endregion

#region events

    protected void cmdPOHeader_Click(object sender, EventArgs e)
    {
        DataTable tbl = POHeaderDataAccess.SelectProjectForPOID(Convert.ToInt32(grd.SelectedValue));
        if (tbl.Rows.Count > 0)
        {
            int intProjectToCustomerID = Convert.ToInt32(tbl.Rows[0]["ProjectToCustomerID"]);
            Session["SelectedProjectToCustomerID"] = intProjectToCustomerID;
            Session["ReturnToScreen"] = "PurchOrderView";
            Response.Redirect("POHeader.aspx?Mode=ExistingPOID");
        }
    }

#endregion

#region dropdowns

    protected void FillBudgetResponsibilityArea()
    //fill BudgetResponsibilityArea dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillBudgetResponsibilityArea";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboBudgetResponsibilityArea;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillCustomer()
    //fill customer dropdownlist for current user
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillCustomer";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        cmd.Parameters.AddWithValue("@IncludeInactive", 1);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboCustomer;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillBrand()
    //fill brand dropdownlist for current user
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillBrand";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboBrand;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillCategory()
    //fill category dropdownlist for current user
    {
        //var ctl = cboCategory;
        //string strSQL = "SELECT '0' AS ID, '<All>' AS Name FROM tblUKCategory ";
        //strSQL += "UNION SELECT DISTINCT b.UKCategoryID AS ID, b.UKCategoryName AS Name FROM qryProductHierarchy b ";
        //strSQL += "WHERE EXISTS(SELECT 'X' FROM tblUserAuthorisations a WHERE a.UserID = 1 AND a.BrandID = b.BrandID OR a.BrandID = 0) ";
        //strSQL += "ORDER BY Name";
        //Get a data table object containing the values
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillCategory";
        cmd.Parameters.AddWithValue("@ZeroValueText", "<All>");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboCategory;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillYear()
    //fill year dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillYear";
        cmd.Parameters.AddWithValue("@ZeroValueText", "[None]");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboYear;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void cboBudgetResponsibilityArea_TextChanged(object sender, EventArgs e)
    {
        Session["SelectedBudgetResponsibilityAreaID"] = cboBudgetResponsibilityArea.SelectedValue;
        SelectFirstRow();
    }

    protected void cboCustomer_TextChanged(object sender, EventArgs e)
    {
        Session["SelectedCustomerID"] = cboCustomer.SelectedValue;
        SelectFirstRow();
    }

    protected void cboBrand_TextChanged(object sender, EventArgs e)
    {
        Session["SelectedBrandID"] = cboBrand.SelectedValue;
        SelectFirstRow();
    }

    protected void cboCategory_TextChanged(object sender, EventArgs e)
    {
        Session["SelectedCategoryID"] = cboCategory.SelectedValue;
        SelectFirstRow();
    }

    protected void cboYear_TextChanged(object sender, EventArgs e)
    {
        Session["SelectedYear"] = cboYear.SelectedValue;
        SelectFirstRow();
    }

#endregion

}



