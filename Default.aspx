<%@ Page Title="" Language="C#" MasterPageFile="~/AP.master" AutoEventWireup="true" EnableEventValidation="false" ValidateRequest="false" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<%@ Register src="User_Controls/FilterDropDown.ascx" tagname="FilterDropDown" tagprefix="uc1" %>
<%@ Register src="User_Controls/ActivityEdit.ascx" tagname="ActivityEdit" tagprefix="uc2" %>
<%@ Register src="User_Controls/AnnualTotals.ascx" tagname="AnnualTotals" tagprefix="uc3" %>
<%@ Register src="User_Controls/MonthlyTotals.ascx" tagname="MonthlyTotals" tagprefix="uc4" %>
<%@ Register src="User_Controls/SupplierGrid.ascx" tagname="SupplierGrid" tagprefix="uc5" %>
<%@ Register src="User_Controls/ActivityTypeGrid.ascx" tagname="ActivityTypeGrid" tagprefix="uc6" %>
<%@ Register src="User_Controls/ProjectEdit_rs.ascx" tagname="ProjectEdit" tagprefix="uc7" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript" language="javascript">
    //this will call the javascript method _SessionExpired after 30 minutes
    setTimeout( "_SessionExpired()", 30 * 60 * 1000 );
    //or 100 minutes:
    //setTimeout("_SessionExpired()", 100 * 60 * 1000);

    function _SessionExpired()
    //redirect to a landing page which runs Session.Abandon() and provides link to login page
    {
        location.href = "ProjectView.aspx";
    }
</script>

<asp:MultiView ID="mvw" runat="server">
    <asp:View ID="vw1" runat="server">
        <script type="text/javascript" language="javascript">
            window.onbeforeunload = confirmExit;

            function setNeedToConfirm(bool) {
                //set isChanged flag
                var ctl = document.all.ctl00$ContentPlaceHolder1$ActivityEdit1$txtChanged
                if (bool) { ctl.value = "1"; }
                else { ctl.value = "0"; }
            }

            function isChanged() {
                //only save if some data have changed
                var isChanged = (document.all.ctl00$ContentPlaceHolder1$ActivityEdit1$txtChanged.value == "1");
                setNeedToConfirm(false);
                return isChanged;
            }

            function confirmExit() {
                //display a message if user has made changes and is navigating away without clicking Save
                if (document.all.ctl00$ContentPlaceHolder1$ActivityEdit1$txtChanged.value == "1") {
                    return "You will lose any unsaved changes."
                }
            }  
        </script>

    <!--Page Header-->
    <table border="0" width="1095px">
        <tr>
            <td align="left">
                <asp:Label ID="Label11" Text="Activities" runat="server" CssClass="page_header" Width="75px" />
            </td><td>
                <asp:Label ID="ProjectToCustomerID" runat="server" visible="false" Width="30px" CssClass="label_text" />
                <asp:Label ID="BudgetResponsibilityAreaID" runat="server" visible="false" Width="30px" CssClass="label_text" />
                <asp:Label ID="ProjectID" runat="server" visible="false" Width="30px" />
                <asp:Label ID="CustomerID" runat="server" visible="false" Width="30px" />
            </td>
            <td align="right">
                <asp:Label ID="Label901" Text="Project" runat="server" CssClass="page_header" />
            </td>
            <td align="left">
                <asp:Label ID="ProjectName" Text="" runat="server" CssClass="page_header_text" Width="280px" />
            </td>
            <td align="right">
                <asp:Label ID="Label902" Text="Customer" runat="server" CssClass="page_header" />
            </td>
            <td align="left">
                <asp:Label ID="CustomerName" Text="" runat="server" CssClass="page_header_text" Width="170px" />
            </td>
            <td align="right">
                <asp:Label ID="Label903" Text="IO Number" runat="server" CssClass="page_header"></asp:Label>
            </td>
            <td align="left">
                <asp:Label ID="IONumber" Text="" runat="server" CssClass="page_header_text" Width="110px" />
            </td>
            <td align="right">
                <asp:Label ID="Label997" Text="Year" runat="server" CssClass="page_header"></asp:Label>
            </td>
            <td align="left">
                <asp:Label ID="ProjectYear" Text="" runat="server" CssClass="page_header_text" Width="50px" />
            </td>
        </tr>
        <tr><td style="font-size:3px">&nbsp;</td></tr>
    </table>

    <!--Project Annual Totals Area--> 
    <uc3:AnnualTotals ID="AnnualTotals1" runat="server" />

    <!--Navigation and filtering-->
    <table border="0" width="1100px">
        <tr>
            <td align="left">
                <asp:Label ID="Label801" runat="server" Text="Type" />
                <asp:DropDownList id="cboActivityType" runat="server" 
                    DataTextField="EntityName" DataValueField="ID" AutoPostBack="true" Width="200px" />
            </td>
            <td align="left">
                <asp:Label ID="Label701" runat="server" Text="Customers" />
                <asp:DropDownList id="cboCustomer" runat="server" OnTextChanged="cboCustomer_TextChanged" 
                    DataTextField="EntityName" DataValueField="ID" AutoPostBack="true" Width="160px" />
            </td>
            <td align="left">
                <asp:Label ID="Label703" runat="server" Text="Projects" />
                <asp:DropDownList id="cboProject" runat="server" ontextchanged="cboProject_TextChanged" 
                    DataTextField="EntityName" DataValueField="ID" AutoPostBack="true" Width="200px" />
            </td>
            <td align="right">
                <asp:Label ID="Label704" runat="server" Text="Rows" />
                <asp:DropDownList id="cboRows" runat="server" AutoPostBack="true" ontextchanged="cboRows_TextChanged" />
            </td>
            <td align="right">
                <asp:Label ID="Label705" runat="server" Text="View" />
                <asp:DropDownList id="cboView" runat="server" AutoPostBack="true" ontextchanged="cboView_TextChanged" />
            </td>
            <td align="right">
                <asp:Label ID="lblSearch" Text="Search" runat="server"></asp:Label>
                <asp:TextBox ID="txtSearch" Runat="server" Width="80px"></asp:TextBox>
                <asp:Button ID="cmdGo" Text="Go" Runat="server" onclick="cmdGo_Click" CausesValidation="false" />
            </td>
        </tr>
        <tr>
            <td style="font-size:3px">
            </td>
        </tr>
    </table>

    <!--Data Source - required to take advantage of sorting and paging functionality of the GridView-->
    <asp:SqlDataSource ID="sds" runat="server" 
        ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
        FilterExpression="ActivityName Like '%{0}%'" 
            SelectCommand="SelectActivityGrid" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:ControlParameter ControlID="CustomerID" Name="CustomerID" 
                PropertyName="Text" Type="Int32" />
            <asp:ControlParameter ControlID="ProjectID" Name="ProjectID" 
                PropertyName="Text" Type="Int32" />
            <asp:ControlParameter ControlID="cboActivityType" Name="ActivityTypeID" 
                PropertyName="SelectedValue" Type="Int16" />
        </SelectParameters>
        <FilterParameters>
            <asp:ControlParameter ControlID="txtSearch" PropertyName="Text" />
        </FilterParameters>
    </asp:SqlDataSource>

    <!--Activity Detail Grid-->
    <div id="project_grid">
        <asp:GridView ID="grd" runat="server" width="1100px"
            AutoGenerateColumns="False" GridLines="Vertical"
            RowStyle-Wrap="false" EmptyDataText="No Records Found" 
            HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
            SelectedRowStyle-Wrap="false"
            AllowSorting="True" onsorted="grd_Sorted" 
            OnRowDataBound="grd_RowDataBound" 
            DataKeyNames="ActivityID" DataSourceID="sds" ShowFooter="True" PageSize="20">
            <Columns>
                <asp:BoundField DataField="ActivityID" ReadOnly="True" Visible="false" />
                <asp:BoundField DataField="ActivityName" HeaderText="Activity Name" SortExpression="ActivityName" />
                <asp:BoundField DataField="SpendTypeName" HeaderText="Spend" SortExpression="SpendTypeName" />
                <asp:BoundField DataField="ActivityTypeName" HeaderText="Activity Type" SortExpression="ActivityTypeName" />
                <asp:BoundField DataField="DocumentNumber" HeaderText="Document" SortExpression="DocumentNumber" />
                <asp:BoundField DataField="ActivityStatusName" HeaderText="Status" SortExpression="ActivityStatusName" />
                <asp:BoundField DataField="SupplierName" HeaderText="Supplier" SortExpression="SupplierName" />
                <asp:BoundField DataField="Month01" HeaderText="Jan" SortExpression="Month01" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month02" HeaderText="Feb" SortExpression="Month02" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month03" HeaderText="Mar" SortExpression="Month03" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month04" HeaderText="Apr" SortExpression="Month04" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month05" HeaderText="May" SortExpression="Month05" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month06" HeaderText="Jun" SortExpression="Month06" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month07" HeaderText="Jul" SortExpression="Month07" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month08" HeaderText="Aug" SortExpression="Month08" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month09" HeaderText="Sep" SortExpression="Month09" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month10" HeaderText="Oct" SortExpression="Month10" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month11" HeaderText="Nov" SortExpression="Month11" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
                <asp:BoundField DataField="Month12" HeaderText="Dec" SortExpression="Month12" 
                    ItemStyle-HorizontalAlign="Right" >
                </asp:BoundField>
            </Columns>
            <RowStyle CssClass="grid_row" Wrap="False" />
            <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
            <HeaderStyle CssClass="grid_header" Wrap="False" />
            <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
        </asp:GridView>
    </div><br />

    <!--Grid Buttons-->
    <table width="1100">
        <tr>
            <td>
                <asp:Button ID="cmdNewAP" runat="server" Text="New A&P Activity" causesvalidation="false" Enabled="false"
                    width="125px" onclick="cmdNewAP_Click" CssClass="form_button" />&nbsp;
                <asp:Button ID="cmdNewTCC" runat="server" Text="New TCC Activity" causesvalidation="false" Enabled="false"
                    width="125px" onclick="cmdNewTCC_Click" CssClass="form_button" />&nbsp;
                <asp:Button ID="cmdNewCpn" runat="server" Text="New Cpn Activity" causesvalidation="false" Enabled="false"
                    width="125px" onclick="cmdNewCpn_Click" CssClass="form_button" />&nbsp;
                <asp:Button ID="cmdDelete" runat="server" Text="Delete Activity" causesvalidation="false"
                    onclick="cmdDelete_Click" OnClientClick="return confirm('Are you sure you want to delete this record?');"
                    width="100px" CssClass="form_button" Enabled="False" />&nbsp;
                <asp:Button ID="cmdCopy" runat="server" Text="Copy Activity" causesvalidation="false"
                    onclick="cmdCopy_Click" width="100px" CssClass="form_button" Enabled="False" />&nbsp;
                <asp:Button ID="cmdCopyAll" runat="server" Text="Copy All" causesvalidation="false"
                    onclick="cmdCopyAll_Click" width="70px" CssClass="form_button" Enabled="False" />&nbsp;
                <asp:Button ID="cmdPaste" runat="server" Text="Paste" causesvalidation="false"
                    width="70px" onclick="cmdPaste_Click" CssClass="form_button" Enabled="False" />&nbsp;
                <asp:Button ID="cmdNewPR" runat="server" Text="New Purch Req" causesvalidation="false"
                    width="100px" onclick="cmdNewPR_Click" CssClass="form_button" Enabled="false" />&nbsp;
                <asp:Button ID="cmdMaster" runat="server" Text="Master" causesvalidation="false"
                    width="70px" onclick="cmdMaster_Click" CssClass="form_button" Enabled="true" />&nbsp;
                <asp:Button ID="cmdBack" runat="server" Text="Back To Projects" causesvalidation="false"
                    width="120px" CssClass="form_button" Enabled="true" PostBackUrl="ProjectView.aspx" />

            </td>
        </tr>
    </table><br />
    
    <!--Edit Area-->
    <div id="edit_area">
        <table><tr><td valign="top">
            <uc2:ActivityEdit ID="ActivityEdit1" runat="server" />
        </td><td valign="top">
            <table style="width:85px;" border="0">
                <tr><td align="right">
                    <asp:Button ID="cmdUpdate" runat="server" Text="Save" causesvalidation="true" ValidationGroup="Activity"
                        onclick="cmdUpdate_Click" CssClass="form_button" Enabled="False" width="85px"
                        onclientclick="return isChanged();" />
                </td></tr><tr><td align="right">
                    <asp:Button ID="cmdCancel" runat="server" Text="Cancel" causesvalidation="false"
                        onclick="cmdCancel_Click" CssClass="form_button" Enabled="False" width="85px" 
                        onclientclick="Javascript:setNeedToConfirm(false);" />
                </td></tr><tr><td align="right">
                    <asp:Button ID="cmdPOHeader" runat="server" Text="PReq/Rebate" causesvalidation="true"
                        onclick="cmdPOHeader_Click" CssClass="form_button" Enabled="false" width="85px" />
                </td></tr><tr><td align="right">
                    <asp:Button ID="cmdReceipt" runat="server" Text="Receipt" causesvalidation="false"
                        onclick="cmdReceipt_Click" CssClass="form_button" Enabled="false" width="85px" />
                </td></tr>
                <tr><td align="right">
                    <asp:Button ID="cmdSupplierLookup" Text="Supplier" Runat="server" 
                        onclick="cmdSupplierLookup_Click" onclientclick="Javascript:setNeedToConfirm(false);"
                        CssClass="form_button" CausesValidation="false" enabled="false" width="85px" />
                </td></tr>                
                <tr><td align="right">
                    <asp:Button ID="cmdActivityTypeLookup" Text="Activity Type" Runat="server" 
                        onclick="cmdActivityTypeLookup_Click" onclientclick="Javascript:setNeedToConfirm(false);"
                        CssClass="form_button" CausesValidation="false" enabled="false" width="85px" />
                </td></tr>                
            </table>
        </td></tr></table>
    </div>

        <!-- Supplier ModalPopupExtender -->
        <asp:ModalPopupExtender ID="mp1" runat="server" PopupControlID="Panel1" TargetControlID="cmdSupplierLookup" BackgroundCssClass="modalBackground" />
        <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" style = "display:none">    
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <!--Supplier Selection-->
                    <uc5:SupplierGrid ID="SupplierGrid1" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
            <!--Supplier Buttons-->
            <asp:Button ID="cmdSaveSupplier" runat="server" Text="Save" onclick="cmdSaveSupplier_Click" 
                CssClass="form_button" Enabled="true" CausesValidation="true"/>
            <asp:Button ID="cmdCancelSupplier" runat="server" Text="Cancel" onclick="cmdCancelSupplier_Click"
                CssClass="form_button" Enabled="true" CausesValidation="false"/>        
        </asp:Panel>
        <!-- ModalPopupExtender -->

        <!-- ActivityType ModalPopupExtender -->
        <asp:Button id="dummyButton" runat="server" style="display:none;" />
        <asp:ModalPopupExtender ID="mp2" runat="server" PopupControlID="Panel2" TargetControlID="dummyButton" 
            BackgroundCssClass="modalBackground" />
        <asp:Panel ID="Panel2" runat="server" CssClass="modalPopup" style = "display:none">    
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <!--Activity Type Selection-->
                    <uc6:ActivityTypeGrid ID="ActivityTypeGrid1" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
            <!--ActivityType Buttons-->
            <asp:Button ID="cmdSaveActivityType" runat="server" Text="Save" onclick="cmdSaveActivityType_Click" 
                CssClass="form_button" Enabled="true" CausesValidation="true"/>
            <asp:Button ID="cmdCancelActivityType" runat="server" Text="Cancel" onclick="cmdCancelSupplier_Click" 
                CssClass="form_button" Enabled="true" CausesValidation="false"/>
        </asp:Panel>
        <!-- ModalPopupExtender -->

    </asp:View>
    <asp:View ID="vw2" runat="server">
        <!--Page Header-->
        <br />
        <table border="0" width="1100px">
            <tr>
                <td align="left">
                    <asp:Label ID="Label2" runat="server" CssClass="page_header" Text="Project Master" />
                </td>
            </tr>
        </table><br />
        <!--Edit Area-->
        <div id="project_edit_area">
            <uc7:ProjectEdit ID="ProjectEdit1" runat="server" /><br />
            <table width="1080px">
                <tr><td><asp:Button ID="cmdBackToActivities" runat="server" Text="Back To Activities" causesvalidation="false"
                    onclick="cmdBackToActivities_Click" CssClass="form_button" width="120px" />
                </td></tr>            
            </table>
        </div>     
    </asp:View>
    <asp:View ID="vw3" runat="server">
    <!--Receipting Page Header-->
        <br /><br /><table border="0" width="1095px">
            <tr>
                <td align="left">
                    <asp:Label ID="Label1" Text="Receipting" runat="server" CssClass="page_header" />
                </td>
            </tr>
        </table><br />
        <!--Data Source - required to take advantage of sorting and paging functionality of the GridView-->
        <asp:SqlDataSource ID="sds2" runat="server" 
            ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
            SelectCommand="SELECT * FROM [qryReceiptingView] WHERE ([ActivityID] = @ActivityID)" >
            <SelectParameters>
                <asp:ControlParameter ControlID="grd" Name="ActivityID" 
                    PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <!--Activity Detail list-->
        <div id="worklist_grid">
            <asp:GridView ID="grdWorklist2" runat="server" width="1100px"
                AutoGenerateColumns="False" GridLines="Vertical"
                RowStyle-Wrap="false" EmptyDataText="No Records Found" 
                HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
                SelectedRowStyle-Wrap="false" DataKeyNames="ActivityDetailID"
                OnSelectedIndexChanged="grdWorklist2_SelectedIndexChanged"
                AllowSorting="True" OnSorted="grdWorklist2_Sorted" 
                onRowDataBound="grdWorklist2_RowDataBound" DataSourceID="sds2">
                <Columns>
                    <asp:BoundField DataField="ActivityDetailID" ReadOnly="True" Visible="false" />
                    <asp:BoundField DataField="ActivityName" HeaderText="Activity" SortExpression="ActivityName" />
                    <asp:BoundField DataField="MonthShortName" HeaderText="Month" SortExpression="ProjectMonth" />
                    <asp:BoundField DataField="PONumber" HeaderText="PO Number" SortExpression="PONumber" />
                    <asp:BoundField DataField="SupplierName" HeaderText="Supplier" SortExpression="SupplierName" />
                    <asp:BoundField DataField="ActivityValue" HeaderText="Value" 
                        SortExpression="ActivityValue" ItemStyle-HorizontalAlign="Right" >
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ReceiptedStatusName" HeaderText="Status" SortExpression="ReceiptedStatus" />
                </Columns>
                <RowStyle CssClass="grid_row" Wrap="False" />
                <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
                <HeaderStyle CssClass="grid_header" Wrap="False" />
                <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
            </asp:GridView>
        </div><br /><br />
        <!--Receipt Buttons-->
        <asp:Button ID="cmdRequestReceipt" runat="server" Text="Receipt" onclick="cmdRequestReceipt_Click" 
            CssClass="form_button" enabled="false" CausesValidation="true" />
        <asp:Button ID="cmdResetReceipt" runat="server" Text="Reset" onclick="cmdResetReceipt_Click" 
            CssClass="form_button" enabled="false" CausesValidation="true" />
        <asp:Button ID="cmdCancelReceipt" runat="server" Text="Cancel" onclick="cmdCancelReceipt_Click" 
            CssClass="form_button" CausesValidation="false"/>
    </asp:View>
    <asp:View ID="vw4" runat="server">
    </asp:View>
</asp:MultiView>
</asp:Content>

