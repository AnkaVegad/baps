﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AP_rs.master" EnableEventValidation="false" AutoEventWireup="true" CodeFile="NewBudgets.aspx.cs" Inherits="NewBudgets" %>

<%@ Register src="User_Controls/NewBudgetGrid.ascx" tagname="NewBudgetGrid" tagprefix="uc1" %>
<%@ Register src="User_Controls/AddBudgetLevel1.ascx" tagname="AddBudgetGrid" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="mvw" runat="server">
    <asp:View ID="vw1" runat="server">
        <uc1:NewBudgetGrid ID="BudgetGrid1" runat="server" />
                  <div id="footer">
                      <br /> 
                       &nbsp;
                    <asp:Button ID="cmdNew" runat="server" Text="New" enabled="false"
                    onclick="cmdNew_Click" CssClass="form_button" ForeColor="Blue" />
                    <asp:Button ID="cmdDelete" runat="server" Text="Delete" enabled="false"
                    onclick="cmdDelete_Click" CssClass="form_button" 
                    OnClientClick="return confirm('Are you sure you want to delete this record?');" ForeColor="Blue" />
                  </div>
    </asp:View>
        <asp:View ID="vw2" runat="server">
            <uc1:AddBudgetGrid ID="AddBudgetGrid1" runat="server" />
    
            <asp:Button ID="cmdCancel" runat="server" Text="Back to Budgets" causesvalidation="true"
                            onclick="cmdCancel_Click" CssClass="form_button" Enabled="false" Width="120px" />
            
         
              </asp:View>
           
</asp:MultiView>

</asp:Content>
