<%@ Page Title="" Language="C#" MasterPageFile="~/AP.master" AutoEventWireup="true" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<!--Page Header-->
<table border="0" width="1095px">
    <tr>
        <td align="left">
            <asp:Label ID="Label11" Text="Reporting Menu" runat="server" CssClass="page_header" Width="175px" />
        </td>
    </tr>
    <tr><td style="font-size:3px">&nbsp;</td></tr>
</table>

<!-- Buttons-->
<table border="0" width="1100">
    <tr><td>
            <asp:Button ID="cmdRiskTracker" runat="server" Text="Risk Tracker" Enabled="true"
                width="130px" CssClass="form_button" PostBackUrl="RiskTrackerView.aspx" />
    </td></tr>
    <tr><td>
            <asp:Button ID="cmdVariances" runat="server" Text="Variances" enabled="true"
                width="130px" CssClass="form_button" PostBackUrl="VarianceView.aspx" />
    </td></tr>
    <tr><td>
            <asp:Button ID="cmdLinks" runat="server" Text="Links" Enabled="true"
                width="130px" CssClass="form_button" PostBackUrl="ReportingLinks.aspx" />
    </td></tr>
</table><br />
    
</asp:Content>

