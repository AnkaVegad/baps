﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SubBrandView : System.Web.UI.Page
{
    string strSingular = "Brand-Market";
    string strPlural = "Brand-Markets";
 
    protected void Page_Load(object sender, EventArgs e)
    {
        //check session not expired and high enough user level
        if (Session["SelectedProjectToCustomerID"] == null || Convert.ToInt16(Session["CurrentUserLevelID"]) >= 5)
        {
            Response.Redirect("ProjectView.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                mvw.ActiveViewIndex = 0;
                SubBrandGrid1.HeaderText = strPlural;
            }
        }
    }

#region events

    protected void cmdGo_Click(object sender, EventArgs e)
    {
        SubBrandEdit1.ClearForm();
    }

    protected void cmdEdit_Click(object sender, EventArgs e)
    {
        if (SubBrandGrid1.SelectedValue != null)
        {
            SubBrandEdit1.PopulateForm(SubBrandDataAccess.SelectSubBrand(Convert.ToInt32(SubBrandGrid1.SelectedValue)));
            mvw.ActiveViewIndex = 1;
            Session["Mode"] = "Edit";
            SubBrandEdit1.HeaderText = Session["Mode"].ToString() + " " + strSingular;
        }
    }

    protected void cmdUpdate_Click(object sender, EventArgs e)
    {
        //try
        //{
            if (Session["Mode"].ToString() == "New")
            {
                string strMessage = SubBrandEdit1.InsertSubBrand();
                if (strMessage != "")
                {
                    MsgBox("Message", strMessage) ;
                }
                else
                {
                    SubBrandGrid1.RefreshGrid();
                    mvw.ActiveViewIndex = 0;
                }
            }
            else
            {
                SubBrandEdit1.UpdateSubBrand();
                SubBrandGrid1.RefreshGrid();
                mvw.ActiveViewIndex = 0;
            }
        //}
        //catch
        //{
        //    MsgBox("Message", "Could not save " + strSingular + ".");
        //}
    }

    protected void cmdNew_Click(object sender, EventArgs e)
    {
        mvw.ActiveViewIndex = 1;
        //enable controls for new record
        SubBrandEdit1.NewRecord();
        Session["Mode"] = "New";
        SubBrandEdit1.HeaderText = Session["Mode"].ToString() + " " + strSingular;
    }

    protected void cmdDelete_Click(object sender, EventArgs e)
    {
        try
        {
            if (SubBrandGrid1.SelectedValue != null)
            {
                SubBrandDataAccess.DeleteSubBrand(Convert.ToInt32(SubBrandGrid1.SelectedValue));
                SubBrandEdit1.ClearForm();
                // update grid
                SubBrandGrid1.DataBind();
            }
        }
        catch //if item has non-cascaded dependencies
        {
            MsgBox("Error", "Could not delete " + strSingular);
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        SubBrandEdit1.ClearForm();
        //ResetButtons();
//        SubBrandGrid1.SelectedIndex = -1;
        mvw.ActiveViewIndex = 0;
    }

#endregion

#region methods

    protected void MsgBox(string strTitle, string strMessage)
    {
        ClientScript.RegisterStartupScript(this.GetType(), strTitle, "alert('" + Common.Left(strMessage, 35) + "');", true);
    }

#endregion
}
