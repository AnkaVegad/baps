﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Web.UI;
using System.Drawing;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;

public partial class ImportMenu : System.Web.UI.Page
{
 
    protected void Page_Load(object sender, EventArgs e)
    {

    }

#region events

    protected void cmdImport_Click(object sender, EventArgs e)
    {
//        {
            //try
            //{
                
        //fails with "Cannot find installable ISAM":
        //string xConnStr = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source=" + Server.MapPath("ExcelImport.xlsx") + ";" + "Extended Properties=Excel 12.0 XML;";

        //fails with "Cannot find installable ISAM":
        string xConnStr = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=" + Server.MapPath("ExcelImport.xlsx") + ";" + "Extended Properties=Excel 12.0 XML;HDR=YES;";

        //WORKS!!
        //string xConnStr = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source=" + Server.MapPath("ExcelImport.xls") + ";" + "Extended Properties=Excel 8.0;";
        
        //fails with "not a valid path":
        //            string xConnStr = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source=\\S2.ms.unilever.com\\dfs\\ES-GROUPS\\cor\\LHD\\CustomerDev\\CustMktg\\BAPS Admin\\ExcelImport.xls;" + "Extended Properties=Excel 8.0;";

        
        using (OleDbConnection connection = new OleDbConnection(xConnStr)) 
            {

                OleDbCommand command = new OleDbCommand("Select * FROM [Sheet1$]", connection); 
                connection.Open();

                // Create DbDataReader to Data Worksheet
                using (OleDbDataReader dr = command.ExecuteReader()) 
                {

                    // SQL Server Connection String

                    string sqlConnectionString = APConfiguration.DbConnectionString; 
                    // Bulk Copy to SQL Server

                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(sqlConnectionString)) 
                    {

                        bulkCopy.DestinationTableName = "dbo.tblExcelTest"; 
                        bulkCopy.WriteToServer(dr);

                    }

                }

            }

            
            
//                string strConn = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\S2.ms.unilever.com\\dfs\\ES-GROUPS\\cor\\fiw\\Operations\\CDOps\\2. S&OP\\Dunnes Team\\2012\\ImportTest.xls;Extended Properties=""Excel 11.0;HDR=YES;IMEX=1""";
//                OleDbConnection MyConnection;
//                DataSet DtSet;
//                OleDbDataAdapter cmd;
//                MyConnection = new OleDbConnection(strConn);
//                cmd = new OleDbDataAdapter("select * from [Input tab$]", MyConnection);
//                cmd.TableMappings.Add("Table", "TestTable");
                
//                DtSet = new DataSet();
//                //SqlDataReader rdr = cmd.ExecuteReader();
                    
//                cmd.Fill(DtSet);
//                //grdImport.DataSource = DtSet.Tables[0];
//            //}
//            //finally
//            //{
//                //MessageBox.Show(ex.ToString());
//                MyConnection.Close();
//            //}
//        }
    }
 
    protected void Button2_Click(object sender, EventArgs e)
    {
//        Label1.Text = "Hello World";
        if (FileUpload1.HasFile)
//            try
            {
                Label1.Text = FileUpload1.FileName;
                FileUpload1.SaveAs(Server.MapPath("Uploads/" + FileUpload1.FileName));
//                Label1.Text = "File name: " +
//                     FileUpload1.PostedFile.FileName + "<br>" +
//                     FileUpload1.PostedFile.ContentLength + " kb<br>" +
//                     "Content type: " +
//                     FileUpload1.PostedFile.ContentType;
//            }
//            catch (Exception ex)
//            {
//                Label1.Text = "ERROR: " + ex.Message.ToString();
            }
        else
        {
            Label1.Text = "You have not specified a file.";
        }
    }

#endregion
}
