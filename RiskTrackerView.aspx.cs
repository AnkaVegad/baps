﻿using System;

public partial class RiskTrackerView : System.Web.UI.Page

{
    string strSingular = "Risk Tracker";

    protected void Page_Load(object sender, EventArgs e)
    {
        //check session not expired and high enough user level
        if (Session["SelectedProjectToCustomerID"] == null || Convert.ToInt16(Session["CurrentUserLevelID"]) >= 38)
        {
            MsgBox("Message", "Session expired.");
            Response.Redirect("ProjectView.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                mvw.ActiveViewIndex = 0;
                RiskTrackerGrid1.HeaderText = strSingular;
                ProjectGrid1.HeaderText = "Select Project";
            }
        }
    }

#region events

    protected void cmdUpdate_Click(object sender, EventArgs e)
    {
        string strMessage;
        //try 
        //{
            if (Page.IsValid)
            {
                if (Session["Mode"].ToString() == "New")
                {
                    RiskTracker h = RiskTrackerEdit1.InsertRiskTracker();
                    h.ProjectToCustomerID = Convert.ToInt32(ProjectGrid1.SelectedValue);
                    h.CreatedBy = Session["CurrentUserName"].ToString();
                    int intRiskTrackerID = VarianceDataAccess.InsertRiskTracker(h);
                    //trap duplicate RiskTracker values
                    if (intRiskTrackerID == 0)
                    {
                        strMessage = "Duplicate Risk Tracker values.";
                    }
                    else
                    {
                        //Clear
                        RiskTrackerEdit1.ClearForm();
                        strMessage = "";
                    }
                }
                else
                {
                    strMessage = RiskTrackerEdit1.UpdateRiskTracker();
                }
                if (strMessage != "")
                {
                    MsgBox("Message", strMessage);
                }
                else
                {
                    RiskTrackerGrid1.RefreshGrid();
                    mvw.ActiveViewIndex = 0;
                }
            }
        //}
        //catch 
        //{
        //    MsgBox("Message", "Could not save " + strSingular + ".");
        //}
    }

    protected void cmdNew_Click(object sender, EventArgs e)
    {
        mvw.ActiveViewIndex = 1;
        //enable controls for new record
        RiskTrackerEdit1.NewRecord();
        Session["Mode"] = "New";
        //RiskTrackerEdit1.HeaderText = Session["Mode"].ToString() + " " + strSingular;
    }

    protected void cmdEdit_Click(object sender, EventArgs e)
    {
//        MsgBox("Message", RiskTrackerGrid1.SelectedValue);
        if (RiskTrackerGrid1.SelectedValue != null && RiskTrackerGrid1.SelectedValue != "0")
        {
            int intRiskTracker = Convert.ToInt32(RiskTrackerGrid1.SelectedValue);
            //RiskTracker c = VarianceDataAccess.SelectRiskTracker(intSelectedRiskTracker);
            RiskTrackerEdit1.PopulateForm(intRiskTracker);
            mvw.ActiveViewIndex = 1;
            Session["Mode"] = "Edit";
        }
        //catch { } //in case no selection in grid
    }

    protected void cmdDelete_Click(object sender, EventArgs e)
    {
        try //in case referential integrity prevents deletion
        {
            VarianceDataAccess.DeleteRiskTracker(Convert.ToInt32(RiskTrackerGrid1.SelectedValue));
            RiskTrackerEdit1.ClearControls();
            // update grid
            RiskTrackerGrid1.DataBind();
        }
        catch
        {
            MsgBox("Message", "Could not delete " + strSingular); 
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        RiskTrackerEdit1.ClearControls();
        mvw.ActiveViewIndex = 0;
    }
    
#endregion

#region methods

    protected void MsgBox(string strTitle, string strMessage)
    {
        ClientScript.RegisterStartupScript(this.GetType(), strTitle, "alert('" + Common.Left(strMessage, 35) + "');", true);
    }

#endregion

}
