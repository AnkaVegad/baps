USE BAPS
GO

/****** Object:  Table [dbo].[tblDeliveryAddress]    Script Date: 12/15/2015 11:41:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblDeliveryAddress](
	[DeliveryAddressID] [smallint] NOT NULL,
	[DeliveryAddressName] [nvarchar](50) NOT NULL,
	[DeliveryAddress1] [nvarchar](50) NOT NULL,
	[DeliveryAddress2] [nvarchar](50) NOT NULL,
	[DeliveryAddress3] [nvarchar](50) NULL,
	[DeliveryAddress4] [nvarchar](50) NULL,
	[DeliveryPostcode] [nvarchar](10) NOT NULL,
	[CountryCode] [nchar](2) NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_tblDeliveryAddress] PRIMARY KEY CLUSTERED 
(
	[DeliveryAddressID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblDeliveryAddress] ADD  CONSTRAINT [DF_tblDeliveryAddress_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO


