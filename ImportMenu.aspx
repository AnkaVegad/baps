<%@ Page Title="" Language="C#" MasterPageFile="~/AP.master" AutoEventWireup="true" EnableEventValidation="false" CodeFile="ImportMenu.aspx.cs" Inherits="ImportMenu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
    function DoOnclick() {
        var RetVal = "Test: The child.html page will change this value...";
        alert(RetVal);
        RetVal = window.showModalDialog("SupplierView.aspx", "", "dialogHeight: 150px; dialogWidth: 300px;");
        alert(RetVal);
    }
</script>

<!--Page Header-->
<table border="0" width="1095px">
    <tr>
        <td align="left">
            <asp:Label ID="Label11" Text="Import Menu" runat="server" CssClass="page_header" Width="175px" />
        </td>
    </tr>
    <tr><td style="font-size:3px">&nbsp;</td></tr>
</table>

<!-- Buttons-->
      <input id="Button1" type="button" value="Open Child..." onclick="DoOnclick();" />
    <table border="0" width="1100">
        <tr><td>
                <asp:Button ID="cmdImport" runat="server" Text="Import" causesvalidation="false" Enabled="true"
                    width="130px" onclick="cmdImport_Click" CssClass="form_button" Visible="true" />&nbsp;
        </td></tr>
    </table><br />

    <asp:GridView ID="grdImport" runat="server" width="500px" AutoGenerateColumns="True" 
        GridLines="Vertical" RowStyle-Wrap="false" EmptyDataText="No records found" 
        HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false" SelectedRowStyle-Wrap="false"
        AllowSorting="false" AllowPaging="false"  
        PageSize="25">
        <RowStyle CssClass="grid_row" Wrap="False" />
        <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
        <HeaderStyle CssClass="grid_header" Wrap="False" />
        <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
    </asp:GridView>

    <asp:FileUpload ID="FileUpload1" runat="server" Width="500px" />
    <asp:Button ID="Button2" runat="server" Text="Upload" onclick="Button2_Click" /><br />
    <asp:Label ID="Label1" runat="server" Text="" CssClass="page_header"></asp:Label>


</asp:Content>