﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AP.master" AutoEventWireup="true" EnableEventValidation="false" CodeFile="UserAdminView.aspx.cs" Inherits="UserAdminView" %>

<%@ Register src="User_Controls/UserAdminGrid.ascx" tagname="UserAdminGrid" tagprefix="uc1" %>
<%@ Register src="User_Controls/UserAdminEdit.ascx" tagname="UserAdminEdit" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:MultiView ID="mvw" runat="server">
    <asp:View ID="vw1" runat="server">
        <uc1:UserAdminGrid ID="UserAdminGrid1" runat="server" />
        <!--Grid Buttons-->
        <asp:Button ID="cmdNew" runat="server" Text="New User" causesvalidation="false"
            onclick="cmdNew_Click" CssClass="form_button" enabled="true" width="100px" />
        <asp:Button ID="cmdEdit" runat="server" Text="Edit User" causesvalidation="false"
            onclick="cmdEdit_Click" CssClass="form_button" enabled="true" width="100px" />
        <asp:Button ID="cmdDelete" runat="server" Text="Delete User" causesvalidation="true" width="100px" 
            onclick="cmdDelete_Click" OnClientClick="return confirm('Are you sure you want to delete this user?');"
            CssClass="form_button" Enabled="true" />&nbsp;
    </asp:View>
    <asp:View ID="vw2" runat="server">
        <!--Edit Area-->
        <uc2:UserAdminEdit ID="UserAdminEdit1" runat="server" /><br />
        <asp:Button ID="cmdUpdate" runat="server" Text="Save" causesvalidation="true"
            onclick="cmdUpdate_Click" CssClass="form_button" Enabled="true" validationgroup="Customer" />&nbsp;
        <asp:Button ID="cmdCancel" runat="server" Text="Cancel" causesvalidation="false"
            onclick="cmdCancel_Click" CssClass="form_button" Enabled="True" />
    </asp:View>
</asp:MultiView>
</asp:Content>

