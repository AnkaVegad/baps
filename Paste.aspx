﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AP.master" EnableEventValidation="false" AutoEventWireup="true" CodeFile="Paste.aspx.cs" Inherits="Paste" %>

<%@ Register src="User_Controls/ProjectGrid.ascx" tagname="ProjectGrid" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <!--Page Header-->
    <br /><br /><table border="0" width="1095px">
        <tr>
            <td align="left">
                <asp:Label ID="Label1" Text="Activities On Clipboard" runat="server" CssClass="page_header"></asp:Label>
                <asp:Label ID="ProjectToCustomerID" runat="server" visible="false" Width="30px" CssClass="label_text" />
            </td>
        </tr>
        <tr><td>&nbsp;</td></tr>
    </table>

    <!--Grid-->
    <div id="project_grid">
        <asp:GridView ID="grd" runat="server" width="850px"
            AutoGenerateColumns="False" GridLines="Vertical" RowStyle-Wrap="false" EmptyDataText="No Activities" 
            HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
            SelectedRowStyle-Wrap="false" DataKeyNames="ActivityID" >
            <Columns>
                <asp:BoundField DataField="ActivityID" ReadOnly="True" Visible="False" />
                <asp:BoundField DataField="ActivityName" HeaderText="Description" />
                <asp:BoundField DataField="SupplierRefNo" HeaderText="Supplier Job Ref" />
                <asp:BoundField DataField="ItemQuantity" HeaderText="Quantity" />
                <asp:BoundField DataField="MonthShortName" HeaderText="Month" />
                <asp:BoundField DataField="UnitPrice" HeaderText="Unit Price" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:0.00}" />
                <asp:BoundField DataField="TotalCost" HeaderText="Total Cost" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:0.00}" />
            </Columns>
            <RowStyle CssClass="grid_row" Wrap="False" />
            <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
            <HeaderStyle CssClass="grid_header" Wrap="False" />
            <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
        </asp:GridView>
    </div><br /><br />
    
    <uc1:ProjectGrid ID="ProjectGrid1" runat="server" />

    <asp:Button ID="cmdPaste" runat="server" Text="Paste" onclick="cmdSave_Click" CssClass="form_button" 
        Enabled="True" />
    <asp:Button ID="cmdBack" runat="server" Text="Back to Activities" onclick="cmdBack_Click" 
        CssClass="form_button" Enabled="True" width="120px" />

</asp:Content>

