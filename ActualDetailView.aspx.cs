﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ActualDetailView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //check session not expired and high enough user level
        //amended AH 03/10/12 to allow all users to view
        if (Session["SelectedProjectToCustomerID"] == null  || Convert.ToInt16(Session["CurrentUserLevelID"]) >= 15)
        {
            Response.Redirect("ProjectView.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                FillYear();
                //Populate header fields
                //ProjectToCustomerID.Text = Session["SelectedProjectToCustomerID"].ToString();
                //PageHeader1.PopulateFromProject(Convert.ToInt32(Session["SelectedProjectToCustomerID"]));
                BindGrid();
            }
            //Populate the actuals grid
        }
    }

#region methods

    private void BindGrid()
    //Populate the GridView with data
    {
        //Get a data table object
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.Text;
        string strSQL = "SELECT * FROM tblActual ";
        strSQL += "WHERE SpendTypeCode = 'A&P' ";
        strSQL += "AND ActualYear = " + cboYear.SelectedValue + " ";
        strSQL += "AND IONumber = '" + txtSearch.Text + "' ";
        strSQL += "ORDER BY ActualMonth;";
        cmd.CommandText = strSQL;
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        grd.DataSource = tbl;
        //bind the databound controls to the datasource
        grd.DataBind();
    }

#endregion

#region gridevents

    protected void grd_RowEditing(object sender, GridViewEditEventArgs e)
        {
            //set the row to enable edit mode for
            grd.EditIndex = e.NewEditIndex;
            //reload the grid
            BindGrid();
        }
    
    protected void grd_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grd.EditIndex = -1;
            BindGrid();
        }

    protected void grd_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        //Retrieve updated data
        string id = grd.DataKeys[e.RowIndex].Value.ToString();
        string value1 = ((TextBox)grd.Rows[e.RowIndex].FindControl("txtProjectYear")).Text;
        //execute the update command
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.Text;
        string strSQL = "UPDATE tblActual SET ProjectYear = '" + value1 + "' WHERE ActualID = " + id;
        cmd.CommandText = strSQL;
        int RowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        //cancel edit mode
        grd.EditIndex = -1;
        //bind the databound controls to the datasource
        BindGrid();
    }

#endregion
    
#region dropdowns

    protected void FillYear()
    //fill year dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_sop_FillYear";
        cmd.Parameters.AddWithValue("@ZeroValueText", "[None]");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboYear;
        d.DataSource = tbl;
        d.DataBind();
    }

#endregion

    protected void cmdGo_Click(object sender, EventArgs e)
    {
        BindGrid();
    }

}
