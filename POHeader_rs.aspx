 <%@ Page Title="" Language="C#" MasterPageFile="~/AP_rs.master" EnableEventValidation="false" AutoEventWireup="true" CodeFile="POHeader_rs.aspx.cs" Inherits="POHeader" %>

<%@ Register src="User_Controls/ActivityEdit.ascx" tagname="ActivityEdit" tagprefix="uc1" %>
<%@ Register src="User_Controls/SupplierGrid.ascx" tagname="SupplierGrid" tagprefix="uc2" %>
<%@ Register src="User_Controls/ActivityTypeGrid.ascx" tagname="ActivityTypeGrid" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:MultiView ID="mvw" runat="server">
    <asp:View ID="vw1" runat="server">
        <script type="text/javascript" language="javascript">
            window.onbeforeunload = confirmExit;

            function setNeedToConfirm(bool) {
                //set isChanged flag
                var ctl = document.all.ctl00$ContentPlaceHolder1$ActivityEdit1$txtChanged
                if (bool) { ctl.value = "1"; }
                else {
                    ctl.value = "0";
                }
            }

            function isChanged() {
                //only save if some data have changed
                var isChanged = (document.all.ctl00$ContentPlaceHolder1$ActivityEdit1$txtChanged.value == "1");
                setNeedToConfirm(false);
                return isChanged;
            }

            function confirmExit() {
                //display a message if user has made changes and is navigating away without clicking Save
                if (document.all.ctl00$ContentPlaceHolder1$ActivityEdit1$txtChanged.value == "1") {
                    return "You will lose any unsaved changes."
                }
            }

            function openPopUp() {
                //open the print preview in a popup window
                {
                    window.open("POHeaderPrint.aspx", "Print");
                }
            }
        </script>  
        
         <script type="text/javascript">
             function setscroll(ctlID) {
                 var panel = document.getElementById('<%=grd.ClientID %>');
        var div_value = document.cookie.substring(document.cookie.indexOf("div_position=") + 13, document.cookie.length);
        panel.scrollTop = div_value.substring(0, div_value.indexOf(","));
        panel.scrollLeft = div_value.substring(div_value.indexOf(",") + 1, div_value.length);
            }
        </script>
        <script type="text/javascript">
            function savescroll(ctlID) {
                var panel = document.getElementById('<%=grd.ClientID %>');
                        document.cookie = "div_position=" + panel.scrollTop + ',' + panel.scrollLeft.toString() + ";";
            }
       </script>    
        <div id="resizable1" class="resizable ui-widget-content" style="height:85%">

        <!--<asp:Label ID="Label1" Text="Purchase Requisition" runat="server" CssClass="page_header" ></asp:Label>-->
        <br />
        <asp:Label ID="lblHeaderText" Text="Projects" runat="server" CssClass="page_header"></asp:Label>
        <br />
        <br />
                  
        <table border="0" style="width:100%">
            <tr>
                <td align="left">
                     <asp:Label ID="POID" runat="server" visible="false" Width="30px" />
                    <asp:Label ID="ProjectToCustomerID" runat="server" visible="false" Width="30px" CssClass="label_text" />
                    <asp:Label ID="BudgetResponsibilityAreaID" runat="server" visible="false" Width="30px" CssClass="label_text" />
                </td>

                <td align="left">
                    <asp:TextBox ID="ProjectID" runat="server" Visible="false" ></asp:TextBox>
                   <!-- <asp:Label ID="Label1901" Text="Project" runat="server" CssClass="page_header" />-->
                </td>
                
                <td align="left">
                     <asp:TextBox ID="CustomerID" runat="server" Visible="false" ></asp:TextBox>
                    <!--<asp:Label ID="Label1902" Text="Customer" runat="server" CssClass="page_header" />-->
                </td>



             </tr>
            <tr>
                <td>Project: </td>
       
                <td>
                    <asp:Label id="ProjectName" runat="server" width="250px" CSSClass="label_text" Text="" Font-Bold="True" />
                </td>
            </tr>
            <tr>
                <td>Area: </td>
                <td>
                    <asp:Label id="BudgetResponsibilityAreaName" runat="server" width="250px" Text="" Font-Bold="True" CSSClass="label_text" />
                    <!--<asp:Label ID="lblBudgetResponsibilityAreaID" runat="server" Visible="false" Width="20px" />-->
                </td>
            </tr>
            <tr>
                <td>Currency: </td>
                <td>
                    <asp:Label id="CurrencyCode" runat="server" width="250px" CSSClass="label_text" Text="" Font-Bold="True" />
                </td>
            </tr>
            <tr>
                <td>Internal Order: </td>
                <td>
                    <asp:Label id="IONumber" runat="server" width="250px" CSSClass="label_text" Text="" Font-Bold="True" />
                </td>
            </tr><tr>
                <td>Project Year: </td>
                <td>
                    <asp:Label id="ProjectYear" runat="server" width="250px" CSSClass="label_text" Text="" Font-Bold="True" />
                </td>
            </tr>
            <tr>
                <td>PP Priority: </td>
                <td>
                    <asp:Label id="PulsingPlanPriority" runat="server" width="250px" CSSClass="label_text" Text="" Font-Bold="True" />
                </td>
            </tr>
            <tr>
                <td>Customer: </td>
                <td>
                    <asp:Label id="CustomerName" runat="server" width="250px" Text="" Font-Bold="True" CSSClass="label_text" />
                </td>
            </tr>
        </table>    
    </div>

     <div id="resizable2" class="ui-widget-content" style="height:85%; overflow:scroll;">
         <div id="div1" style="height:auto;">
           <br />
            <table border="0" width="1100px">
                <tr><td valign="top">
                    <table border="0">
                        <tr><td>Supplier<asp:Label ID="SupplierID" runat="server" Width="30px" visible="false" /></td><td>
                            <asp:TextBox ID="SupplierName" runat="server" Width="208px" enabled="false" ForeColor="Black"/>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*Required"
                                controltovalidate="SupplierName" Display="Dynamic"  ValidationGroup="POHeader" />
                            <asp:Button ID="cmdSupplierLookup" style="border:1px solid gray;width:50px" Text="Lookup" Runat="server" onclick="cmdSupplierLookup_Click" CausesValidation="false" />
                        </td></tr>
                        <tr><td>SAP Vendor Code</td><td>
                            <asp:TextBox ID="SAPVendorCode" runat="server" Width="80px" enabled="false" ForeColor="Black"/>
                        </td></tr>
                        <tr><td valign="top">Comments</td><td width="15">
                            <asp:TextBox ID="Comments" runat="server" Width="276px" MaxLength="255" 
                                Enabled="True" Height="58px" TextMode="MultiLine" ForeColor="Black" />
                        </td></tr>
                    </table>
                </td>
                <td valign="top">
                    <table border="0">
                        <tr><td>Delivery Address</td><td width="180">
                            <asp:DropDownList ID="DeliveryAddressID" runat="server" Width="180px" 
                                AutoPostBack="true" DataTextField="EntityName" DataValueField="ID" ForeColor="Black" />
                            <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="*Required" 
                                ControlToValidate="DeliveryAddressID" Display="Dynamic" MaximumValue="ZZZ" 
                                MinimumValue="1" ValidationGroup="POHeader" />
                        </td></tr>
                        <tr><td></td><td>
                            <asp:Label ID="DeliveryAddress" runat="server" Width="250px" CSSClass="pseudo_text_box" Height="80px" ForeColor="Black" />
                        </td></tr>               
                    </table>
                </td>
                <td valign="top">
                    <table style="width:300px;" border="0">
                        <tr><td>Requisition By</td><td>
                            <asp:TextBox ID="RequisitionBy" runat="server" Width="144px" enabled="false" ForeColor="Black"/>
                        </td></tr>
                        <tr><td>NPI Coordinator</td><td>
                            <asp:DropDownList ID="NPICoordinator" runat="server" Width="150px" DataTextField="EntityName" DataValueField="ID" ForeColor="Black"/>
                            <asp:RangeValidator ID="RangeValidator3" runat="server" ErrorMessage="*Required" 
                                ControlToValidate="NPICoordinator" Display="Dynamic" MaximumValue="zzz" MinimumValue="1" 
                                ValidationGroup="POHeader" />
                        </td></tr>
                        <tr><td>Approver</td><td>
                            <asp:DropDownList ID="ReleasedBy" runat="server" Width="150px" DataTextField="EntityName" DataValueField="ID" ForeColor="Black"/>
                            <asp:RangeValidator ID="RangeValidator4" runat="server" ErrorMessage="*Required" 
                                ControlToValidate="ReleasedBy" Display="Dynamic" MaximumValue="zzz" MinimumValue="1" 
                                ValidationGroup="POHeader" />
                        </td></tr>
                        <tr><td>PR Number</td><td>
                            <asp:TextBox ID="PRNumber" runat="server" Width="80px" MaxLength="10" ForeColor="Black"/>
                        </td></tr>
                        <tr><td>PO Number</td><td>
                            <asp:TextBox ID="PONumber" runat="server" Width="80px" MaxLength="10" ForeColor="Black"/>
                            <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="*PR Number must be entered" 
                                Display="Dynamic" onservervalidate="CustomValidator1_ServerValidate" ValidationGroup="POHeader" />
                        </td></tr>
                        <tr><td>
                                 <asp:Label ID="lblNewFinanceApproval" Text = "New Finance Approval" runat="server" Width="145px" /> </td>
                                <td><asp:CheckBox ID="CheckFinanceApproval" runat="server" OnCheckedChanged="CheckBox1_CheckedChanged"  Visible='<%# IsActivityPresent((int)Eval("CheckActivityCount")) %>' />
                            
                            </td>
                        </tr>
                    </table>
                </td>
               </tr>
                <tr>
                    <td>
                        Line Items
                    </td>
                </tr>

            </table>
        </td></tr></table>
             </div>

        <!--Grid-->
        <div id="poheader_grid" style="min-height: 50px; Height:auto;">
        <asp:GridView ID="grd" runat="server" width="950px"
            AutoGenerateColumns="False" GridLines="Vertical" RowStyle-Wrap="false" EmptyDataText="No Activities" 
            HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
            SelectedRowStyle-Wrap="false" AllowSorting="True" onsorting="grd_Sorting"
            onselectedindexchanged="grd_SelectedIndexChanged" onrowdatabound="grd_RowDataBound"
            DataKeyNames="ActivityID" ShowFooter="True">

            <Columns>
                <asp:BoundField DataField="ActivityID" InsertVisible="False" ReadOnly="True" Visible="False" />
                <asp:BoundField DataField="ShortText" HeaderText="Short Text" SortExpression="ShortText" />
                <asp:BoundField DataField="ActivityTypeName" HeaderText="Type" SortExpression="ActivityTypeName" />
                <asp:BoundField DataField="GLCode" HeaderText="G/L Code" SortExpression="GLCode" />
                <asp:BoundField DataField="CommodityCode" HeaderText="Commodity" SortExpression="CommodityCode" />
                <asp:BoundField DataField="SupplierRefNo" HeaderText="Supplier's Ref" SortExpression="SupplierRefNo" />
                <asp:BoundField DataField="ItemQuantity" HeaderText="Quantity" SortExpression="ItemQuantity" />
                <asp:BoundField DataField="MonthShortName" HeaderText="Month" SortExpression="MonthRequired" />
                <asp:BoundField DataField="UnitPrice" HeaderText="Unit Price" SortExpression="POItemNo" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:0.00}" />
                <asp:BoundField DataField="TotalCost" HeaderText="Total Cost" SortExpression="POItemNo" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:0.00}" />
            </Columns>
            <RowStyle CssClass="grid_row" Wrap="False" />
            <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
            <HeaderStyle CssClass="grid_header" Wrap="False" />
              <FooterStyle CssClass="grid_footer" Wrap="False" />
            <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
        </asp:GridView>
        </div>
            
       <div id="div2" style="height:auto;">
           <br />
            <br /> &nbsp;Select File:&nbsp;
    <asp:FileUpload ID="FileUpload1" runat="server" Width="500px" />
    <asp:Button ID="ButtonUpload" runat="server" Text="Upload" enabled="true"  CssClass="form_button" 
        OnClick="cmdUpload_Click" />&nbsp;
     <asp:Label ID="LabelUploadFile" runat="server" Text="" CssClass="page_header" ></asp:Label>
            &nbsp;&nbsp;<asp:Label ID="lblname" runat="server" CssClass="page_header" Text=""></asp:Label>
            &nbsp;<asp:Label ID="lblFileList" runat="server" CssClass="page_header"></asp:Label>

           
            <asp:GridView ID="GridViewDocuments" runat="server"
                 AutoGenerateColumns="False"
            EmptyDataText = "No files uploaded" CellPadding="4"
            EnableModelValidation="True" ForeColor="#333333"  GridLines="Vertical" Width="492px" >
                <AlternatingRowStyle BackColor="White" CssClass="grid_alternating_row" Wrap="False"  />
    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    <Columns>
       
          <asp:BoundField HeaderStyle-CssClass="GridHeader" DataField="FileName" HeaderText="File Name" ItemStyle-HorizontalAlign="Left"  />
            
       <asp:TemplateField>
            <ItemTemplate>
                <asp:LinkButton ID="lnkDownload" Text = "Download" CommandArgument = '<%# Eval("FileName") %>' runat="server" OnCommand = "LinkDownload_click"></asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:LinkButton ID = "lnkDelete" Text = "Delete" CommandArgument = '<%# Eval("FileName") %>' runat = "server" OnCommand="lnkDelete_Command" OnClientClick="return confirm('Are you sure you want to delete this record?');" />
            </ItemTemplate>
        </asp:TemplateField>
         
  
    </Columns>
    
               
                <FooterStyle CssClass="grid_footer" Wrap="False" />
                <HeaderStyle BackColor="Gray" ForeColor="White" Height="15px" Wrap="false" />
    
                <RowStyle CssClass="grid_row" Wrap="False" BackColor="#EFF3FB" />

            </asp:GridView>
             
            
             
            
            <br />
         

        <!--Activity Buttons-->
        <table style="width:850px;" border="0">
            <tr><td>Unallocated Activities:</td><td align="right">
                <asp:DropDownList ID="cboSelectItem" runat="server" Width="400px" DataTextField="EntityName" DataValueField="ID" ForeColor="Black" >
                </asp:DropDownList>            
            </td><td align="right">
                <asp:Button ID="cmdAdd" runat="server" Text="Add Item" 
                onclick="cmdAdd_Click" CssClass="form_button" Enabled="false" CausesValidation="false"/>
            </td><td align="right">
                <asp:Button ID="cmdEdit" runat="server" Text="Edit Item" 
                onclick="cmdEdit_Click" CssClass="form_button" Enabled="false" CausesValidation="false"/>
            </td><td align="right">
                <asp:Button ID="cmdNew" runat="server" Text="New Item" 
                onclick="cmdNew_Click" CssClass="form_button" Enabled="false" CausesValidation="false"/>
            </td><td align="right">
                <asp:Button ID="cmdRemove" runat="server" Text="Remove Item" Width="100px"
                onclick="cmdRemove_Click" CssClass="form_button" Enabled="false" CausesValidation="false"/>
            </td></tr>
        </table>
           
            <!--Lower Edit Area-->
        <div id="edit_area">
            <table><tr><td valign="top">
                <uc1:ActivityEdit ID="ActivityEdit1" runat="server" />
            </td><td valign="top">
                <table style="width:85px;" border="0">
                    <tr><td align="right">
                        <asp:Button ID="cmdSaveActivity" runat="server" Text="Save" CausesValidation="true"
                        onclick="cmdSaveActivity_Click" CssClass="form_button" Enabled="False" 
                        onclientclick="return isChanged();" ValidationGroup="Activity" width="85px" />            
                    </td></tr>
                    <tr><td align="right">
                        <asp:Button ID="cmdCancelActivity" runat="server" Text="Cancel" causesvalidation="false"
                        onclick="cmdCancelActivity_Click" CssClass="form_button" Enabled="False" 
                        onclientclick="Javascript:setNeedToConfirm(false);" width="85px" />
                    </td></tr>
                    <tr><td align="right">
                        <asp:Button ID="cmdActivityTypeLookup" Text="Activity Type" Runat="server" 
                            onclick="cmdActivityTypeLookup_Click" onclientclick="Javascript:setNeedToConfirm(false);"
                            CssClass="form_button" CausesValidation="false" enabled="false" width="85px" />
                    </td></tr>                
                </table>
            </td></tr></table>
        </div>
            </div>
         </div>

       
        <br /><br />    

         <div id="footer">
           <br />
           &nbsp;
            <!--Grid Buttons-->
                            <asp:Button ID="cmdUpdate" runat="server" Text="Save" causesvalidation="true"
                                onclick="cmdUpdate_Click" CssClass="form_button" Enabled="true" ValidationGroup="POHeader" />            
                       
                            <asp:Button ID="cmdCancel" runat="server" Text="Cancel" causesvalidation="false"
                                onclick="cmdCancel_Click" CssClass="form_button" Enabled="true" />
                       
                            <asp:Button ID="cmdPrint" runat="server" Text="Print" causesvalidation="false"
                                onclientclick="Javascript:openPopUp();" CssClass="form_button" Enabled="true" />
                      
       </div>
          
        <!-- Supplier ModalPopupExtender -->
       <asp:Button id="dummyButton" runat="server" style="display:none;" />
        <!-- Supplier ModalPopupExtender -->
        <asp:ModalPopupExtender ID="mp1" runat="server" PopupControlID="Panel1" TargetControlID="dummyButton" BackgroundCssClass="modalBackground" />
        <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" style="display:none" >    
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <!--Supplier Selection-->
                    <uc2:SupplierGrid ID="SupplierGrid1" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
            <!--Supplier Buttons-->
            <asp:Button ID="cmdSaveSupplier" runat="server" Text="Save" onclick="cmdSaveSupplier_Click" CssClass="form_button" 
                Enabled="true" CausesValidation="true"/>
            <asp:Button ID="cmdCancelSupplier" runat="server" Text="Cancel" onclick="cmdCancelSupplier_Click" CssClass="form_button" 
                Enabled="true" CausesValidation="false"/>
            </asp:Panel>
        <!-- ModalPopupExtender -->

        <!-- ActivityType ModalPopupExtender -->
        <asp:ModalPopupExtender ID="mp2" runat="server" PopupControlID="Panel2" TargetControlID="dummyButton" 
            BackgroundCssClass="modalBackground" />
        <asp:Panel ID="Panel2" runat="server" CssClass="modalPopup" style = "display:none">    
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <!--Activity Type Selection-->
                    <uc3:ActivityTypeGrid ID="ActivityTypeGrid1" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
            <!--ActivityType Buttons-->
            <asp:Button ID="cmdSaveActivityType" runat="server" Text="Save" onclick="cmdSaveActivityType_Click" 
                CssClass="form_button" Enabled="true" CausesValidation="true"/>
            <asp:Button ID="cmdCancelActivityType" runat="server" Text="Cancel" onclick="cmdCancelSupplier_Click" 
                CssClass="form_button" Enabled="true" CausesValidation="false"/>
        </asp:Panel>
        <!-- ModalPopupExtender -->

    </asp:View>
    <asp:View ID="vw2" runat="server">
    </asp:View>
</asp:MultiView>
</asp:Content>


