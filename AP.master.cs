﻿using System;
using System.Data;
using System.Web.UI.WebControls;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Response.Cookies["ShowWorklist"]["value"] != null)
        {
            String ShowWorklistPopup = Response.Cookies["ShowWorklist"]["value"].ToString();
            //Check for active worklist items
            if (ShowWorklistPopup.Equals("1"))
            {
                int worklistItems = WorklistDataAccess.WorklistSummary(Session["CurrentUserName"].ToString()).Rows[0].ItemArray.Length;
                int displayWorklist = 0;
                for (int i = 0; i < worklistItems; i++)
                {
                    if (WorklistDataAccess.WorklistSummary(Session["CurrentUserName"].ToString()).Rows[0].ItemArray[i].ToString() != "0")
                    {
                        displayWorklist = 1;
                    }
                }
                //redirect if present
                if (displayWorklist == 1)
                {
                    showWorklist.Show();
                }
            }
        }

        if (!IsPostBack)
        {
            //populate the search option dropdown
            cboSearchOptions.Items.Add(new ListItem("IO Number", "1"));
            cboSearchOptions.Items.Add(new ListItem("PO Number", "2"));
            cboSearchOptions.Items.Add(new ListItem("Rebate", "3"));

            //populate current user, current user level and current year in header
            txtCurrentUserLevelName.Text = Session["CurrentUserLevelName"].ToString();
            txtCurrentUserName.Text = Session["CurrentUserName"].ToString();
            txtCurrentYear.Text = Session["SelectedYear"].ToString();
        }
    }

    protected void cmdGo_Click(object sender, EventArgs e)
    {
        DataTable tbl = null;
        switch (cboSearchOptions.SelectedValue)
        {
            case "1":
                //Search for IONumber
                tbl = ProjectDataAccess.FindIONumber(txtSearch1.Text, Session["CurrentUserName"].ToString());
                break;
            case "2":
                //Search for PO Number
                tbl = ProjectDataAccess.FindPONumber(txtSearch1.Text, Session["CurrentUserName"].ToString());
                break;
            case "3":
                //Search for Rebate Number
                tbl = ProjectDataAccess.FindRebateNumber(txtSearch1.Text, Session["CurrentUserName"].ToString());
                break;
        }

//        txtSearch1.Text = tbl.Rows.Count.ToString();
        if (tbl.Rows.Count >= 1)
        {
            int intProjectToCustomerID = Convert.ToInt32(tbl.Rows[0]["ProjectToCustomerID"]);
            Session["SelectedProjectToCustomerID"] = intProjectToCustomerID;
            Response.Redirect("Default.aspx");
        }
        else
        {
            txtSearch1.Text = "Not found";
        }
    }

    public string CurrentUser
    {
        get
        {
            try
            {
                return txtCurrentUserName.Text;
            }
            catch { return "0"; }
        }
    }

    protected void cmdRedirect_Click(object sender, EventArgs e)
    {
        Response.Redirect("Worklist.aspx");
    }
}
