<%@ Page Title="" Language="C#" MasterPageFile="~/AP.master" AutoEventWireup="true" EnableEventValidation="false" ValidateRequest="false" CodeFile="ActualsView.aspx.cs" Inherits="ActualsView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <!--Page Header-->
    <table border="0" width="650px">
        <tr>
            <td align="left">
                <asp:Label ID="Label901" Text="Actuals" runat="server" CssClass="page_header" />
            </td>
            <td align="right">Year</td>
            <td>
             <asp:DropDownList id="cboYear" runat="server" ontextchanged="cboYear_TextChanged" AutoPostBack="True" 
                DataTextField="EntityName" DataValueField="ID" />
            </td>
        </tr>
        <tr><td style="font-size:3px">&nbsp;</td></tr>
    </table>

    <!--Data Source - required to take advantage of sorting and paging functionality of the GridView-->
    <asp:SqlDataSource ID="sds" runat="server" 
        ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
        SelectCommandType="StoredProcedure" SelectCommand="sp_ap_SelectActualsByYear">
        <SelectParameters>
           <asp:ControlParameter Name="ActualsYear" ControlID="cboYear" PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>

    <!--Actuals Grid-->
    <div id="project_grid">
        <asp:GridView ID="grd" runat="server" width="680px"
            AutoGenerateColumns="False" GridLines="Vertical"
            RowStyle-Wrap="false" EmptyDataText="No Records Found" 
            HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
            SelectedRowStyle-Wrap="false"             
            AllowSorting="True" onsorted="grd_Sorted" AllowPaging="False"
            OnRowDataBound="grd_RowDataBound"  
            DataKeyNames="UniqueKey" DataSourceID="sds">
            <Columns>
                <asp:BoundField DataField="UniqueKey" Visible="false" ReadOnly="true"/>
                <asp:BoundField DataField="ActualYear" HeaderText="Year" SortExpression="ActualYear" />
                <asp:BoundField DataField="ActualMonth" HeaderText="Month" SortExpression="ActualMonth" />
                <asp:BoundField DataField="SpendTypeCode" HeaderText="Spend Type" SortExpression="SpendTypeCode" />
                <asp:BoundField DataField="ActualValue" HeaderText="Actuals Value" SortExpression="ActualValue" />
                <asp:BoundField DataField="CreatedBy" HeaderText="Created By" SortExpression="CreatedBy" />
                <asp:BoundField DataField="CreatedDate" HeaderText="Date" SortExpression="CreatedDate" />
            </Columns>
            <RowStyle CssClass="grid_row" Wrap="False" />
            <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
            <HeaderStyle CssClass="grid_header" Wrap="False" />
            <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
        </asp:GridView>
    </div>
<br />

    <br/>Create TCC Actuals for month:&nbsp;
    <asp:DropDownList id="cboActualsMonthYear" runat="server" DataTextField="EntityName" DataValueField="ID" />
    &nbsp;Select File:&nbsp;
    <asp:FileUpload ID="uplTCC" runat="server" Width="500px" />
    <asp:Button ID="cmdUploadTCC" runat="server" Text="Upload" enabled="true"  CssClass="form_button"
        OnClick="cmdUploadTCC_Click" OnClientClick="return confirm('Are you sure you want to create these actuals?');" />&nbsp;
    <asp:Button ID="cmdBack" runat="server" Text="Back To Projects" enabled="true"  CssClass="form_button"
        PostBackURL="ProjectView.aspx" width="130px"/><br /><br />
    <asp:Label ID="Label70013" runat="server" Text="" CssClass="page_header"></asp:Label>

</asp:Content>

