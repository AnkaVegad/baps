<%@ Page Title="" Language="C#" MasterPageFile="~/AP.master" AutoEventWireup="true" CodeFile="SummaryView.aspx.cs" Inherits="SummaryView" %>

<%@ Register src="User_Controls/AnnualTotals.ascx" tagname="AnnualTotals" tagprefix="uc1" %>
<%@ Register src="User_Controls/MonthlyTotals.ascx" tagname="MonthlyTotals" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <!--Page Header-->
    <table border="0" width="1095px">
        <tr>
            <td align="left">
                <asp:Label ID="Label900" Text="Project Summary" runat="server" CssClass="page_header" />
                <asp:Label ID="ProjectToCustomerID" runat="server" visible="false" Width="30px" CssClass="label_text" />
            </td>
            <td align="left">
                <asp:Label ID="Label901" Text="Project" runat="server" CssClass="page_header" />
                <asp:Label ID="ProjectID" runat="server" visible="false" Width="30px" />
                <asp:Label ID="ProjectName" Text="" runat="server" CssClass="page_header_text" Width="250px" />
            </td>
            <td align="left">
                <asp:Label ID="Label902" Text="Customer" runat="server" CssClass="page_header" />
                <asp:Label ID="CustomerID" runat="server" visible="false" Width="30px" />
                <asp:Label ID="CustomerName" Text="" runat="server" CssClass="page_header_text" Width="150px" />
            </td>
            <td align="left">
                <asp:Label ID="Label903" Text="IO Number" runat="server" CssClass="page_header"></asp:Label>
                <asp:Label ID="IONumber" Text="" runat="server" CssClass="page_header_text" Width="110px" />
            </td>
        </tr>
        <tr><td style="font-size:3px">&nbsp;</td></tr>
    </table>

    <uc1:AnnualTotals ID="AnnualTotals1" runat="server" />
    <uc2:MonthlyTotals ID="MonthlyTotals1" runat="server" />
</asp:Content>

