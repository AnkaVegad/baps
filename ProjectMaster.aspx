﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AP.master" AutoEventWireup="true" EnableEventValidation="false" CodeFile="ProjectMaster.aspx.cs" Inherits="ProjectMaster" %>

<%@ Register src="User_Controls/ProjectEdit_rs.ascx" tagname="ProjectEdit" tagprefix="uc1" %>
<%@ Register src="User_Controls/FilterDropDown.ascx" tagname="FilterDropDown" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:MultiView ID="mvw" runat="server">
    <asp:View ID="vw1" runat="server">
        <!--Page Header-->
        <table border="0" width="1100px">
            <tr>
                <td align="left">
                    <asp:Label ID="Label900" Text="Project Master" runat="server" CssClass="page_header"/>
                </td>
                <td>
                    <asp:Label ID="Label902" Text="Area" runat="server"/>
                    <asp:DropDownList id="cboBudgetResponsibilityArea" runat="server" AutoPostBack="true" 
                        DataTextField="EntityName" DataValueField="ID" ontextchanged="cboBudgetResponsibilityArea_TextChanged" />
                </td>
                <td>
                    <asp:Label ID="Label910" Text="Brand" runat="server"/>
                    <asp:DropDownList id="cboBrand" runat="server" AutoPostBack="true" 
                        DataTextField="EntityName" DataValueField="ID" ontextchanged="cboBrand_TextChanged" />
                </td>
                <td>
                    <asp:Label ID="Label901" Text="Year" runat="server"/>
                    <asp:DropDownList id="cboProjectYear" runat="server" AutoPostBack="true" 
                        DataTextField="EntityName" DataValueField="ID" ontextchanged="cboProjectYear_TextChanged" />
                </td>
                <td>
                    <asp:Label ID="Label903" Text="Country" runat="server"/>
                    <asp:DropDownList id="cboCountry" runat="server" AutoPostBack="true" 
                        DataTextField="EntityName" DataValueField="ID" ontextchanged="cboCountry_TextChanged" />
                </td>
                <td>
                    <asp:Label ID="Label904" Text="Priority" runat="server"/>
                    <asp:DropDownList id="cboPulsingPlanPriority" runat="server" AutoPostBack="true" 
                        DataTextField="EntityName" DataValueField="ID" ontextchanged="cboPulsingPlanPriority_TextChanged" />
                </td>
                <td align="right">
                    <asp:Label ID="lblSearch" Text="Search:" runat="server"></asp:Label>
                    <asp:TextBox ID="txtSearch" Runat="server" Width="120px"></asp:TextBox>
                    <asp:Button ID="cmdGo" Text="Go" Runat="server" CausesValidation="false" onclick="cmdGo_Click" />
                </td>
            </tr>
            <tr><td style="font-size:3px">&nbsp;</td></tr>
        </table>
        
        <!--Data Source - required to take advantage of sorting and paging functionality of the GridView-->
        <asp:SqlDataSource ID="sds" runat="server" 
            ConnectionString="<%$ ConnectionStrings:APConnectionString %>">
        </asp:SqlDataSource>

        <!--Project Grid-->
        <div id="project_grid">
            <asp:GridView ID="grd" runat="server" width="1095px"
                AutoGenerateColumns="False" GridLines="Vertical"
                RowStyle-Wrap="false" EmptyDataText="No records found" 
                HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
                SelectedRowStyle-Wrap="false"
                AllowSorting="True" AllowPaging="true"
                onselectedindexchanged="grd_SelectedIndexChanged" 
                OnRowDataBound="grd_RowDataBound" 
                onpageindexchanged="grd_PageIndexChanged"  onsorted="grd_Sorted"
                DataKeyNames="ProjectID" DataSourceID="sds" PageSize="20">
                <Columns>
                    <asp:BoundField DataField="ProjectID" ReadOnly="True" Visible="False" />
                    <asp:BoundField DataField="ProjectName" HeaderText="Project Name" SortExpression="ProjectName" />
                    <asp:BoundField DataField="ProjectYear" HeaderText="Year" SortExpression="ProjectYear" />
                    <asp:BoundField DataField="BudgetResponsibilityAreaName" HeaderText="Area" SortExpression="BudgetResponsibilityAreaName" />
                    <asp:BoundField DataField="CountryName" HeaderText="Country" SortExpression="CountryName" />
                    <asp:BoundField DataField="PulsingPlanPriorityName" HeaderText="Priority" SortExpression="PulsingPlanPriorityName" />
                    <asp:BoundField DataField="PulsingPlanTiming" HeaderText="Timing" SortExpression="PulsingPlanTiming" />
                </Columns>
                <RowStyle CssClass="grid_row" Wrap="False" />
                <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
                <HeaderStyle CssClass="grid_header" Wrap="False" />
                <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
            </asp:GridView>
            <asp:Label ID="txtMessage" runat="server" />
       </div><br />

        <!--Grid Buttons-->
        <asp:Button ID="cmdNew" runat="server" Text="New Project" causesvalidation="false"
            onclick="cmdNew_Click" CssClass="form_button" enabled="false" width="100px" />
        <asp:Button ID="cmdEdit" runat="server" Text="Edit Project" causesvalidation="false"
            onclick="cmdEdit_Click" CssClass="form_button" enabled="false" width="100px" />
        <asp:Button ID="cmdCopy" runat="server" Text="Copy Project" causesvalidation="false"
            onclick="cmdCopy_Click" CssClass="form_button" enabled="false" width="100px" />
        <asp:Button ID="cmdDelete" runat="server" Text="Delete Project" causesvalidation="false" width="100px" 
            onclick="cmdDelete_Click" OnClientClick="return confirm('Are you sure you want to delete this project?');"
            CssClass="form_button" Enabled="false" /><br /><br />
    </asp:View>
    <asp:View ID="vw2" runat="server">
        <!--Page Header-->
        <br />
        <table border="0" width="1100px">
            <tr>
                <td align="left">
                    <asp:Label ID="lblHeader" runat="server" CssClass="page_header"/>
                </td>
            </tr>
        </table><br />
        <!--Edit Area-->
        <div id="project_edit_area">
            <uc1:ProjectEdit ID="ProjectEdit1" runat="server" />
            <table width="1080px">
                <tr><td align="right">
                    <asp:Button ID="cmdUpdate" runat="server" Text="Save" 
                        onclick="cmdUpdate_Click" CssClass="form_button" Enabled="False" validationgroup="Project" />&nbsp;
                    <asp:Button ID="cmdCancel" runat="server" Text="Cancel" causesvalidation="false"
                        onclick="cmdCancel_Click" CssClass="form_button" Enabled="true" />
                </td></tr>
            </table>
        </div>  
        
        <!-- Project Creation message ModalPopupExtender -->
        <asp:Button id="cmdDummy" runat="server" style="display:none;" />
        <asp:ModalPopupExtender ID="mp1" runat="server" PopupControlID="Panel1" TargetControlID="cmdDummy" BackgroundCssClass="modalBackground" />
        <asp:Panel ID="Panel1" runat="server" CssClass="smallModalPopup" style = "display:none" HorizontalAlign="Center">    
            <asp:Label ID="lblMessage1" runat="server" Text="Project Saved."></asp:Label><br /><br />
            <asp:Label ID="lblMessage2" runat="server" Text=""></asp:Label><br /><br /><br /><br /><br /><br />
            <!--Buttons-->
            <asp:Button ID="cmdOK" runat="server" Text="OK" onclick="cmdOK_Click" 
                CssClass="form_button" Enabled="true" CausesValidation="true"/>
        </asp:Panel>
        <!-- ModalPopupExtender -->
           
    </asp:View>
</asp:MultiView>
</asp:Content>

