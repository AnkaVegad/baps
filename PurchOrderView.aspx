﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AP.master" EnableEventValidation="false" AutoEventWireup="true" CodeFile="PurchOrderView.aspx.cs" Inherits="PurchOrderView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<!--Header and Filter Criteria-->
<table width="1100px">
    <tr><td style="font-size:2px">&nbsp;</td></tr>
    <tr>
        <td>
            <asp:Label ID="Label11" Text="POs" runat="server" CssClass="page_header"></asp:Label>
        </td>
        <td>Area</td>
        <td>
            <asp:DropDownList id="cboBudgetResponsibilityArea" runat="server" ontextchanged="cboBudgetResponsibilityArea_TextChanged" 
                DataTextField="EntityName" DataValueField="ID" AutoPostBack="True"/>
        </td>
        <td>Customer</td>
        <td>
            <asp:DropDownList id="cboCustomer" runat="server" ontextchanged="cboCustomer_TextChanged"  Width="200px"
                DataTextField="EntityName" DataValueField="ID" AutoPostBack="True"/>
        </td>
        <td>Brand</td>
        <td>
            <asp:DropDownList id="cboBrand" runat="server" ontextchanged="cboBrand_TextChanged" 
                DataTextField="EntityName" DataValueField="ID" AutoPostBack="True"/>
        </td>
        <td>Category</td>
        <td>
            <asp:DropDownList id="cboCategory" runat="server" ontextchanged="cboCategory_TextChanged" 
                DataTextField="EntityName" DataValueField="ID" AutoPostBack="True"/>
        </td>
        <td>Year</td>
        <td>
            <asp:DropDownList id="cboYear" runat="server" ontextchanged="cboYear_TextChanged" AutoPostBack="True" 
                DataTextField="EntityName" DataValueField="ID" />
        </td>
        <td align="right">
            <asp:Label ID="lblSearch" Text="Search:" runat="server"></asp:Label>
            <asp:TextBox ID="txtSearch" Runat="server" Width="80px"></asp:TextBox>
            <asp:Button ID="cmdGo" Text="Go" Runat="server" CausesValidation="false" />
        </td>
    </tr>
</table><br /> 

<!--Data Source - required to take advantage of sorting and paging functionality of the GridView-->
<asp:SqlDataSource ID="sds" runat="server" 
    ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
    FilterExpression="ProjectName Like '%{0}%' Or SupplierName Like '%{0}%'">
    <FilterParameters>
        <asp:ControlParameter ControlID="txtSearch" PropertyName="Text" />
    </FilterParameters>
</asp:SqlDataSource>

<!--Grid-->
<div id="project_grid">
    <asp:GridView ID="grd" runat="server" width="1100px"
        AutoGenerateColumns="False" GridLines="Vertical" RowStyle-Wrap="false" EmptyDataText="No Records Found" 
        HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false" SelectedRowStyle-Wrap="false"
        AllowPaging="true" PageSize="25" onpageindexchanged="grd_PageIndexChanged" 
        onselectedindexchanged="grd_SelectedIndexChanged" AllowSorting="True" onsorted="grd_Sorted"
        DataKeyNames="POID" DataSourceID="sds" 
        onrowdatabound="grd_RowDataBound" ShowFooter="True">
        <Columns>
            <asp:BoundField DataField="POID" readonly="true" Visible="false" />
            <asp:BoundField DataField="PONumber" HeaderText="PO Number" SortExpression="PONumber" />
            <asp:BoundField DataField="ProjectName" HeaderText="Project" SortExpression="ProjectName" />
            <asp:BoundField DataField="CustomerName" HeaderText="Customer" SortExpression="CustomerName" />
            <asp:BoundField DataField="IONumber" HeaderText="IO" SortExpression="IONumber" />
            <asp:BoundField DataField="SupplierName" HeaderText="Supplier" SortExpression="SupplierName" />
            <asp:BoundField DataField="SumOfPOValue" HeaderText="Value" 
                SortExpression="SumOfPOValue" DataFormatString="{0:0}" 
                ItemStyle-HorizontalAlign="Right" >
            </asp:BoundField>
            <asp:BoundField DataField="SumOfReceipted" HeaderText="Rcptd" 
                SortExpression="SumOfReceipted" DataFormatString="{0:0}" 
                ItemStyle-HorizontalAlign="Right" >
            </asp:BoundField>
            <asp:BoundField DataField="SumOfRcptRequested" HeaderText="Rcpt Reqd" 
                SortExpression="SumOfRcptRequested" DataFormatString="{0:0}" 
                ItemStyle-HorizontalAlign="Right" >
            </asp:BoundField>
            <asp:BoundField DataField="SumOfPtReceipted" HeaderText="Pt Rcptd" 
                SortExpression="SumOfPtReceipted" DataFormatString="{0:0}" 
                ItemStyle-HorizontalAlign="Right" >
            </asp:BoundField>
        </Columns>
        <RowStyle CssClass="grid_row" Wrap="False" />
        <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
        <HeaderStyle CssClass="grid_header" Wrap="False" />
        <FooterStyle CssClass="grid_footer" Wrap="False" />
        <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
    </asp:GridView>
    <asp:Label ID="txtMessage" runat="server" />
</div>
<br />

<!--Buttons-->
<table>
    <tr><td>
        <asp:Button ID="cmdPOHeader" runat="server" Text="Go To PO" Visible="true" Width="100px"
            onclick="cmdPOHeader_Click" CssClass="form_button" />
    </td>
    <td>
        <asp:Button ID="cmdBack" runat="server" Text="Back To Projects" causesvalidation="false"
            width="120px" PostbackURL="~/ProjectView.aspx" CssClass="form_button" Enabled="true" />
    </td></tr>
</table>

</asp:Content>

