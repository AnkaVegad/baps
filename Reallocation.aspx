﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AP_rs.master" EnableEventValidation="false" ValidateRequest="false" AutoEventWireup="true" CodeFile="Reallocation.aspx.cs" Inherits="Reallocation" %>

<%@ Register src="User_Controls/ReallocationGrid.ascx" tagname="ReallocationGrid" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript">
        function CopySpendTypeToDestination() {
            var FromSpendType = document.getElementById('ctl00_ContentPlaceHolder1_cboFromSpendTypeID');
            var ToSpendType = document.getElementById('ctl00_ContentPlaceHolder1_cboToSpendTypeID');
            ToSpendType.value = FromSpendType.value;
            return false;
        }
    </script>
    <asp:Panel runat="server">
        <uc2:ReallocationGrid ID="Grid1" runat="server" />
  
        <div id="footer">
            <br />&nbsp;
        <asp:Button ID="cmdSave" runat="server" Text="Save" Width="120px"
            onclick="cmdSave_Click" CssClass="form_button" Enabled="True" />
        <asp:Button ID="cmdClear" runat="server" Text="Clear Entries" Width="120px"
            onclick="cmdClear_Click" CssClass="form_button" Enabled="True" CausesValidation="false" />
        <asp:Button ID="cmdBack" runat="server" Text="Back To Projects" Width="120px"
            onclick="cmdBack_Click" CssClass="form_button" Enabled="True" CausesValidation="false" />
            </div>
    </asp:Panel>
</asp:Content>

