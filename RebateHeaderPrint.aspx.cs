﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RebateHeaderPrint : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //check for session expired
        if (Session["SelectedProjectToCustomerID"] == null) 
        {
            Response.Redirect("ProjectView.aspx");
        }

        //populate project header from Project 
        DataTable t = ProjectDataAccess.SelectProjectToCustomer(Convert.ToInt32(Session["SelectedProjectToCustomerID"]));
        DataRow r = t.Rows[0];
        ProjectName.Text = r["ProjectName"].ToString();
        CustomerName.Text = r["CustomerName"].ToString();
        IONumber.Text = r["IONumber"].ToString();
        CurrencyCode.Text = r["CurrencyCode"].ToString();
        //populate rebate defaults (never change)
        PO p = POHeaderDataAccess.SelectPOHeader(Convert.ToInt32(Session["SelectedPOID"]));
        Rebate rb = RebateHeaderDataAccess.SelectRebateHeader(Convert.ToInt32(Session["SelectedRebateID"]));
        AccrualTypeName.Text = "Lump Sum";
        AgreementTypeCode.Text = rb.AgreementTypeCode;

        int intRebateID = Convert.ToInt32(rb.RebateID);
        SAPCustomerCode.Text = rb.SAPCustomerCode;
        CustomerName2.Text = rb.CustomerName2.ToString();
        ValidFromDate.Text = DateTime.Parse(rb.InStoreStartDate.ToString()).ToString("dd/MM/yy");
        ValidToDate.Text = DateTime.Parse(rb.InStoreEndDate.ToString()).ToString("dd/MM/yy");
        Comments.Text = rb.Comments;
        RequestedBy.Text = rb.RequestedBy.ToLower();
        AgreementNumber.Text = rb.AgreementNumber.ToUpper();
        RebateCreator.Text = rb.RebateCreator.ToLower();
        ApprovedL1By.Text = rb.ApprovedL1By.ToLower();
        if (rb.ApprovedL1Date == null) { ApprovedL1Date.Text = DateTime.Parse(rb.ApprovedL1Date.ToString()).ToString("dd/MM/yy"); }

        //populate grdActivityToProduct using the first activity in the rebate
        DataTable tblA = RebateHeaderDataAccess.FillActivityToProduct(intRebateID);
        grdActivityToProduct.DataSource = tblA;
        grdActivityToProduct.DataBind();

        //populate selected items table
        DataTable tbl = RebateHeaderDataAccess.SelectActivitiesForRebateID(intRebateID);
        //populate line items grid
        grd.DataSource = tbl;
        grd.DataBind();

    }

//    double dblTotal = 0;
    double dblTotalCost = 0;

    protected void grd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            double dblCurrentItem = Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "TotalCost"));
            //increment the total 
            dblTotalCost += dblCurrentItem;
            
            //highlight on hover
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(this.grd, "Select$" + e.Row.RowIndex);
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[5].Text = "Total";
            //display the total in grid footer
            e.Row.Cells[6].Text = dblTotalCost.ToString();
        }
    }

    protected void grd_Sorting(object sender, GridViewSortEventArgs e)
    {
    }

    protected void grdActivityToProduct_Sorting(object sender, EventArgs e)
    {
    }

}
