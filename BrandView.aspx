﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AP.master" AutoEventWireup="true" CodeFile="BrandView.aspx.cs" Inherits="BrandView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <!--Page Header-->
    <table border="0" width="1100px">
        <tr>
            <td align="left">
                <asp:Label ID="Label1" Text="Brands" runat="server" CssClass="page_header"></asp:Label>
            </td>
        </tr>
    </table>
    <br /><br />


    <div id="activity_grid">
        <asp:GridView ID="grd" runat="server" width="450px"
            AutoGenerateColumns="False" GridLines="Vertical"
            RowStyle-Wrap="false" EmptyDataText="No Records Found" 
            HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
            SelectedRowStyle-Wrap="false"
            onrowcancelingedit="grd_RowCancelingEdit" OnRowEditing="grd_RowEditing" OnRowUpdating="grd_RowUpdating" Onrowdeleting="grd_RowDeleting"
            DataKeyNames="BrandID" EnableModelValidation="True" >
            <Columns>
                <asp:BoundField DataField="BrandID" ReadOnly="True" Visible="False" />
                <asp:TemplateField HeaderText="Brand" SortExpression="BrandName">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtBrandName" runat="server" 
                            Text='<%# Bind("BrandName") %>' TextMode="SingleLine" Width="400px"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("BrandName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField HeaderText="Actions" ShowEditButton="True" ShowDeleteButton="True" 
                    ShowHeader="True" />
            </Columns>
            <RowStyle CssClass="grid_row" Wrap="False" />
            <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
            <HeaderStyle CssClass="grid_header" Wrap="False" />
            <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
        </asp:GridView>
        <br />
        <asp:Label ID="Label2" runat="server" Text="Create new Brand:"></asp:Label>
        <asp:TextBox ID="txtNewBrandName" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*Required" 
            controltovalidate="txtNewBrandName" Display="Dynamic" ValidationGroup="Brand" />
        <asp:Button ID="cmdUpdate" runat="server" Text="Create" onclick="cmdUpdate_Click" CausesValidation="True" ValidationGroup="Brand" />
    </div>

</asp:Content>

