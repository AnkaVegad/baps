﻿using System;
using System.Web.UI;
using System.Web;

public partial class CalendarView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CalendarGrid1.ProjectEditView += new EventHandler(ProjectView_Show);
        if (!IsPostBack)
        {
            Session["SelectedProjectID"] = "-1";
            mvw.ActiveViewIndex = 0;
        }
    }

    public override void VerifyRenderingInServerForm(Control control) { }

    protected void ProjectView_Show(object sender, EventArgs e)
    {
        if (Session["SelectedProjectID"].ToString() == "-1")
        {
            mvw.ActiveViewIndex = 0;
            CalendarGrid1.SetScroll();
        }
        else
        {
            mvw.ActiveViewIndex = 1;
            int intProjectID = int.Parse(Session["SelectedProjectID"].ToString());
            Project a = ProjectDataAccess.SelectProject(intProjectID);
            ProjectEdit1.ToggleControlState(false);
            ProjectEdit1.PopulateForm(a);
            //Decide if Save and Cancel button should be shown based on CurrentUserLevelID
            if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 15)
            {
                cmdSave.Visible = true;
                cmdCancel.Visible = true;
                //Show multiselect
            }
            else
            {
                cmdSave.Visible = false;
                cmdCancel.Visible = false;
                //Show Textbox
            }
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        string strMessage="";
        int intProjectID = Convert.ToInt32(ProjectEdit1.ProjectIDValue);
        //Check if PulsingPlanPriorityMonth is continous, then save. Else set error message that months are not continuous so project could not be saved
        strMessage = ProjectEdit1.CheckPulsingPlanTiming();
        if (strMessage.Length==0)
        {
            strMessage = ProjectEdit1.UpdateProject();
        }
        if (strMessage.Length == 0)
        //completed without errors
        {
            mvw.ActiveViewIndex = 0;
            CalendarGrid1.FillCalendarGrid();
            CalendarGrid1.SetScroll();
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        mvw.ActiveViewIndex = 0;
        CalendarGrid1.FillCalendarGrid();
        CalendarGrid1.SetScroll();
    }
}