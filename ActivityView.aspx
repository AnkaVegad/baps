<%@ Page Title="" Language="C#" MasterPageFile="~/AP_rs.master"   EnableEventValidation="false" ValidateRequest="false" AutoEventWireup="true" CodeFile="ActivityView.aspx.cs" Inherits="ActivityView" %>

<%@ Register src="User_Controls/ActivityGrid.ascx" tagname="ActivityGrid" tagprefix="uc1" %>
<%@ Register src="User_Controls/ActivityEdit_rs.ascx" tagname="ActivityEdit_rs" tagprefix="uc2" %>
<%@ Register src="User_Controls/ProjectEdit_rs.ascx" tagname="ProjectEdit" tagprefix="uc4" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:MultiView ID="mvw" runat="server">
    <asp:View ID="vw1" runat="server">
        <uc1:ActivityGrid ID="Grid1" runat="server" />
        <div id="footer">
            <br />&nbsp;
            <!--Buttons-->
            <asp:Button ID="cmdNewActivity" runat="server" Text="New Activity" enabled="false"
                width="110px" onclick="cmdNewActivity_Click" CssClass="form_button" />
            <asp:Button ID="cmdEdit" runat="server" Text="Edit Activity" enabled="false"
                width="100px" onclick="cmdEdit_Click" CssClass="form_button" />
            <asp:Button ID="cmdDelete" runat="server" Text="Delete Activity" enabled="false"
                width="100px" onclick="cmdDelete_Click" CssClass="form_button" 
                OnClientClick="return confirm('Are you sure you want to delete this record?');" />
            <asp:Button ID="cmdPOHeader" runat="server" Text="Purch Req/Rebate"
                onclick="cmdPOHeader_Click" CssClass="form_button" Enabled="false" width="110px" />
            <asp:Button ID="cmdReceipt" runat="server" Text="Receipt"
                onclick="cmdReceipt_Click" CssClass="form_button" Enabled="false" width="70px" />            
            <asp:Button ID="cmdCopy" runat="server" Text="Copy Activity"
                onclick="cmdCopy_Click" CssClass="form_button" Enabled="true" width="110px" />
            <asp:Button ID="cmdCopyAll" runat="server" Text="Copy All"
                onclick="cmdCopyAll_Click" CssClass="form_button" Enabled="false" width="70px" />
            <asp:Button ID="cmdPaste" runat="server" Text="Paste"
                onclick="cmdPaste_Click" CssClass="form_button" Enabled="false" width="70px" />
            <asp:Button ID="cmdMaster" runat="server" Text="Master"
                width="70px" onclick="cmdMaster_Click" CssClass="form_button" Enabled="true" />
            <asp:Button ID="cmdBack" runat="server" Text="Back To Projects"
                width="110px" CssClass="form_button" Enabled="true" PostBackUrl="ProjectView_rs.aspx" />
        </div>    
    </asp:View>
    <asp:View ID="vw2" runat="server">
        <uc2:ActivityEdit_rs ID="ActivityEdit_rs1" runat="server" Visible="true" /> 
        <div id="footer">
            <br />&nbsp;           
            <asp:Button ID="cmdUpdate" runat="server" Text="Save" causesvalidation="true"
                onclick="cmdUpdate_Click" CssClass="form_button" Enabled="false"  ValidationGroup="Activity" />    
          
            <asp:Button ID="cmdCancel" runat="server" Text="Cancel" causesvalidation="false" 
                Onclick="cmdCancel_Click" CssClass="form_button" /> 
        </div>            
    </asp:View>
    <asp:View ID="vw3" runat="server">
        <uc4:ProjectEdit ID="ProjectEdit1" runat="server" />
        <div id="footer">
            <br />&nbsp;
            <!--Buttons-->       
            <asp:Button ID="cmdClose" runat="server" Text="Back To Projects" causesvalidation="false"
                onclick="cmdCancel_Click" CssClass="form_button" width="110px" />
        </div>
    </asp:View>
    <asp:View ID="vw4" runat="server">
    <!--Receipting Page Header-->
        <br /><br /><table border="0" width="1095px">
            <tr>
                <td align="left">
                    <asp:Label ID="Label2" Text="Receipting" runat="server" CssClass="page_header" />
                </td>
            </tr>
        </table><br />
        <!--Data Source - required to take advantage of sorting and paging functionality of the GridView-->
        <asp:SqlDataSource ID="sds2" runat="server" 
            ConnectionString="<%$ ConnectionStrings:APConnectionString %>" 
            SelectCommand="SELECT * FROM [qryReceiptingView] WHERE ([ActivityID] = @ActivityID)" >
            <SelectParameters>
                <asp:ControlParameter ControlID="Grid1" Name="ActivityID" 
                    PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <!--Activity Detail list-->
        <div id="worklist_grid">
            <asp:GridView ID="grdWorklist2" runat="server" width="1100px"
                AutoGenerateColumns="False" GridLines="Vertical"
                RowStyle-Wrap="false" EmptyDataText="No Records Found" 
                HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
                SelectedRowStyle-Wrap="false" DataKeyNames="ActivityDetailID"
                OnSelectedIndexChanged="grdWorklist2_SelectedIndexChanged"
                AllowSorting="True" OnSorted="grdWorklist2_Sorted" 
                onRowDataBound="grdWorklist2_RowDataBound" DataSourceID="sds2">
                <Columns>
                    <asp:BoundField DataField="ActivityDetailID" ReadOnly="True" Visible="false" />
                    <asp:BoundField DataField="ActivityName" HeaderText="Activity" SortExpression="ActivityName" />
                    <asp:BoundField DataField="MonthShortName" HeaderText="Month" SortExpression="ProjectMonth" />
                    <asp:BoundField DataField="PONumber" HeaderText="PO Number" SortExpression="PONumber" />
                    <asp:BoundField DataField="SupplierName" HeaderText="Supplier" SortExpression="SupplierName" />
                    <asp:BoundField DataField="ActivityValue" HeaderText="Value" 
                        SortExpression="ActivityValue" ItemStyle-HorizontalAlign="Right" >
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ReceiptedStatusName" HeaderText="Status" SortExpression="ReceiptedStatus" />
                </Columns>
                <RowStyle CssClass="grid_row" Wrap="False" />
                <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
                <HeaderStyle CssClass="grid_header" Wrap="False" />
                <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
            </asp:GridView>
        </div><br /><br />
        <!--Receipt Buttons-->
        <asp:Button ID="cmdRequestReceipt" runat="server" Text="Receipt" onclick="cmdRequestReceipt_Click" 
            CssClass="form_button" enabled="false" CausesValidation="true" />
        <asp:Button ID="cmdResetReceipt" runat="server" Text="Reset" onclick="cmdResetReceipt_Click" 
            CssClass="form_button" enabled="false" CausesValidation="true" />
        <asp:Button ID="cmdCancelReceipt" runat="server" Text="Cancel" onclick="cmdCancelReceipt_Click" 
            CssClass="form_button" CausesValidation="false"/>
    </asp:View></asp:MultiView>
</asp:Content>

