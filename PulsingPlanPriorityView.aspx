﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AP.master" AutoEventWireup="true" EnableEventValidation="false" CodeFile="PulsingPlanPriorityView.aspx.cs" Inherits="PulsingPlanPriorityView" %>

<%@ Register src="User_Controls/PulsingPlanPriorityGrid.ascx" tagname="PulsingPlanPriorityGrid" tagprefix="uc1" %>
<%@ Register src="User_Controls/PulsingPlanPriorityEdit.ascx" tagname="PulsingPlanPriorityEdit" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:MultiView ID="mvw" runat="server">
    <asp:View ID="vw1" runat="server">
        <uc1:PulsingPlanPriorityGrid ID="PulsingPlanPriorityGrid1" runat="server" />
        <!--Grid Buttons-->
        <asp:Button ID="cmdNew" runat="server" Text="New" causesvalidation="false"
            onclick="cmdNew_Click" CssClass="form_button" enabled="true" width="80px" />&nbsp;
        <asp:Button ID="cmdEdit" runat="server" Text="Edit" causesvalidation="false"
            onclick="cmdEdit_Click" CssClass="form_button" enabled="true" width="80px" />&nbsp;
        <asp:Button ID="cmdDelete" runat="server" Text="Delete" causesvalidation="false" width="80px" 
            onclick="cmdDelete_Click" OnClientClick="return confirm('Are you sure you want to delete this Pulsing Plan Priority?');"
            CssClass="form_button" Enabled="true" />
    </asp:View>
    <asp:View ID="vw2" runat="server">
        <!--Edit Area-->
        <uc2:PulsingPlanPriorityEdit ID="PulsingPlanPriorityEdit1" runat="server" /><br />
        <asp:Button ID="cmdUpdate" runat="server" Text="Save" causesvalidation="true"
            onclick="cmdUpdate_Click" CssClass="form_button" Enabled="true" validationgroup="PulsingPlanPriority" />&nbsp;
        <asp:Button ID="cmdCancel" runat="server" Text="Cancel" causesvalidation="false"
            onclick="cmdCancel_Click" CssClass="form_button" Enabled="True" />
    </asp:View>
</asp:MultiView>
</asp:Content>

