﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RebateHeader : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["SelectedProjectToCustomerID"] == null) //check for session expiry
        {
            Response.Redirect("ProjectView.aspx");
        }
        if (!IsPostBack)
        {
            //populate dropdowns
            AccrualTypeID.Items.Add(new ListItem("Lump Sum", "1"));
            AccrualTypeID.Items.Add(new ListItem("Rate Per Case", "2"));
            FillMonth(ValidFromMonth, 0, "");
            FillMonth(ValidToMonth, 0, "");
            FillDay(ValidFromDay);
            FillDay(ValidToDay);
            FillAgreementType();
            //DataTables for use later
            DataTable tbl;
            DataTable t;
            DataRow r;
            Rebate rb;
            string strApprovedL1By = "0";
            //populate project header
            if (Request.QueryString["Mode"] == "NewRebateSelectedItem" || Request.QueryString["Mode"] == "ExistingRebate")
            {
                //populate project header from Activity
                t = ActivityDataAccess.SelectActivityToTable(Convert.ToInt32(Session["SelectedActivityID"]));
                r = t.Rows[0];
                RebateID.Text = r["RebateID"].ToString();
            }
            else
            {
                //populate project header from Project 
                t = ProjectDataAccess.SelectProjectToCustomer(Convert.ToInt32(Session["SelectedProjectToCustomerID"]));
                r = t.Rows[0];
            }
            ProjectID.Text = r["ProjectID"].ToString();
            CustomerID.Text = r["CustomerID"].ToString();
            IONumber.Text = r["IONumber"].ToString();
            BudgetResponsibilityAreaID.Text = r["BudgetResponsibilityAreaID"].ToString();
            ProjectName.Text = r["ProjectName"].ToString();
            CustomerName.Text = r["CustomerName"].ToString();
            ProjectToCustomerID.Text = r["ProjectToCustomerID"].ToString();
            //populate rebate defaults (never change)
            ValidFromYear.Text = r["ProjectYear"].ToString();
            ValidToYear.Text = r["ProjectYear"].ToString();
            CurrencyCode.Text = r["CurrencyCode"].ToString();
            //fill grids (after table extract but before Rebate Header values set)
            FillActivity(Convert.ToInt32(ProjectToCustomerID.Text));
            FillProduct(); //new for ActivityToProduct
            //populate RebateHeader, selected activity and selected product tables
            if (Request.QueryString["Mode"] == "NewRebateSelectedItem")
            {
                tbl = PopulateNewRebate(r);
                //set up selected activity table
                DataRow dr = tbl.NewRow();
                Activity a = ActivityDataAccess.SelectActivity(Convert.ToInt32(Session["SelectedActivityID"]));
                CopyToTable(a, dr);
                tbl.Rows.Add(dr);
                //populate default agreement type based on spend type ***hardcoded for the moment***
                if (a.SpendTypeID == 2) { AgreementTypeCode.SelectedValue = "Z508"; } else { AgreementTypeCode.SelectedValue = "Z518"; }
                //populate default start day, end day and month
                string strMonthRequired = a.MonthRequired.ToString();
                if (strMonthRequired != "13")
                {
                    ValidFromDay.SelectedValue = "01";
                    ValidToDay.SelectedValue = Common.ADOLookup("LastDayOfMonth", "tblMonth", "MonthNo = '" + strMonthRequired + "'");
                    ValidFromMonth.SelectedValue = strMonthRequired;
                    ValidToMonth.SelectedValue = strMonthRequired;
                }
                //remove selected item from unallocated items dropdownlist
                RemoveFromUnallocated(a);
                //set up selected products table
                DataTable tblA = new DataTable();
                tblA.Columns.Add("PRODH");
                tblA.Columns.Add("VTEXT");
                tblA.Columns.Add("PropnPerCent");
                Session["SelectedProductItems"] = tblA;
            }
            else
            {
                if (Request.QueryString["Mode"] == "ExistingRebate")
                {
                    //populate RebateID, RebateHeader fields (including Approvers dropdown) and selected items table
                    rb = RebateHeaderDataAccess.SelectRebateHeader(Convert.ToInt32(RebateID.Text));
                    tbl = PopulateExistingRebateID(rb);
                    strApprovedL1By = rb.ApprovedL1By.ToString();
                    Session["SelectedRebateID"] = RebateID.Text;
                }
                else
                {
                    if (Request.QueryString["Mode"] == "ExistingRebateID")  //(user has come from Worklist)
                    {
                        //populate RebateID, RebateHeader fields (including Approvers dropdown) and selected items table
                        rb = RebateHeaderDataAccess.SelectRebateHeader(Convert.ToInt32(Session["SelectedRebateID"]));
                        tbl = PopulateExistingRebateID(rb);
                        strApprovedL1By = rb.ApprovedL1By.ToString();
                    }
                    else //(Mode=NewRebateNoSelection)
                    {
                        tbl = PopulateNewRebate(r);
                    }
                }
            }
            //store selected items in basket
            Session["SelectedItems"] = tbl;
            //populate line items grid
            grd.DataSource = tbl;
            grd.DataBind();
            mvw.ActiveViewIndex = 0;
        }
        //enable/disable fields 
        ApprovedL1By.Enabled = (ApprovedL1Date.Text.Trim() == String.Empty);
        RebateCreator.Enabled = (AgreementNumber.Text.Trim() == String.Empty);
        AgreementNumber.Enabled = (ApprovedL1Date.Text.Trim() != String.Empty);
        if (ApprovedL1Date.Text.Trim() != string.Empty)
        {
            CustomerName2.Enabled = false;
            SAPCustomerCode.Enabled = false;
            ValidFromYear.Enabled = false;
            ValidToYear.Enabled = false;
            //ConditionTypeCode.Enabled = false;
            AgreementTypeCode.Enabled = false;
            ValidFromDay.Enabled = false;
            ValidFromMonth.Enabled = false;
            ValidToDay.Enabled = false;
            ValidToMonth.Enabled = false;
            cmdAdd.Enabled = false;
            cmdEdit.Enabled = false;
            cmdNew.Enabled = false;
            cmdRemove.Enabled = false;
            cboSelectItem.Enabled = false;
            cmdAddProduct.Enabled = false;
            cmdRemoveProduct.Enabled = false;
            cboAddProduct.Enabled = false;
            txtProportion.Enabled = false;
        }
        //show Approve button - should only be for "Existing" rebate modes?
        //amended 05/07/14 to check for delegated user; changed 28/07/14 to use ADOLookupSafe in case there is no Delegated user found (otherwise it fails)
        string strCurrentUserName = Session["CurrentUserName"].ToString();
        string strDelegaterUserName = Common.ADOLookupSafe("UserName", "tblUser", "DelegateUserName = '" + strCurrentUserName + "'"); 
        if ((ApprovedL1By.SelectedValue == strCurrentUserName || ApprovedL1By.SelectedValue == strDelegaterUserName) && ApprovedL1Date.Text == String.Empty && RebateID.Text.Trim() != String.Empty)
        {
            cmdApprove.Visible = true;
            cmdApprove.Text = "Approve";
        }
        if ((ApprovedL1By.SelectedValue == strCurrentUserName || ApprovedL1By.SelectedValue == strDelegaterUserName) && ApprovedL1Date.Text != String.Empty && AgreementNumber.Text.Trim() == String.Empty)
        {
            cmdApprove.Visible = true;
            cmdApprove.Text = "Clear";
        }
    }

#region events

    protected void cmdUpdate_Click(object sender, EventArgs e)
    {
        //try
        //{
            //check for session expiry
            if (Session["SelectedItems"] == null)
            {
                Response.Redirect("ProjectView.aspx");
            }
            else
            {
                DataTable tbl = (DataTable)Session["SelectedItems"];
                if (tbl.Rows.Count == 0)
                {
                    MsgBox("Error", "Select one or more activities.");
                }
                else
                {
                    DataTable tblA = (DataTable)Session["SelectedProductItems"];
                    if (tblA.Rows.Count == 0)
                    {
                        MsgBox("Error", "Select one or more products.");
                    }
                    else
                    {
                        //loop through selected activities creating history records here, before Rebate Header is updated - added 18/12/12
                        foreach (DataRow rw in tbl.Rows)
                        {
                            int intActivityID = Convert.ToInt32(rw["ActivityID"]);
                            if (intActivityID != 0)
                            //existing activity
                            {
                                ActivityDataAccess.InsertActivityHistory(intActivityID);
                            }
                        }
                        //
                        int intRebateID;
                        Rebate r = new Rebate();
                        r.AccrualTypeID = Convert.ToInt16(AccrualTypeID.SelectedValue);
                        r.ConditionTypeCode = "";
                        r.AgreementTypeCode = AgreementTypeCode.Text;
                        r.SAPCustomerCode = SAPCustomerCode.Text;
                        r.CustomerName2 = CustomerName2.Text;
                        r.InStoreStartDate = ValidFromYear.Text + '-' + ValidFromMonth.SelectedValue + '-' + ValidFromDay.SelectedValue;
                        r.InStoreEndDate = ValidToYear.Text + '-' + ValidToMonth.SelectedValue + '-' + ValidToDay.SelectedValue;
                        r.Comments = Comments.Text;
                        r.RequestedBy = RequestedBy.Text;
                        r.ApprovedL1By = ApprovedL1By.SelectedValue;
                        r.RebateCreator = RebateCreator.SelectedValue;
                        if (RebateID.Text == "") //Create new RebateHeader
                        {
                            r.RebateID = 0;
                            r.CreatedBy = Session["CurrentUserName"].ToString();
                            intRebateID = RebateHeaderDataAccess.InsertRebateHeader(r);
                            if (intRebateID == 0) { MsgBox("Error", "Invalid dates."); }
                        }
                        else //edit existing RebateHeader
                        {
                            r.RebateID = Convert.ToInt32(RebateID.Text);
                            r.AgreementNumber = AgreementNumber.Text;
                            r.LastUpdatedBy = Session["CurrentUserName"].ToString();
                            //Update Rebate Header
                            string strMessage = RebateHeaderDataAccess.UpdateRebateHeader(r);
                            if (strMessage != "")
                            {
                                MsgBox("Error", strMessage);
                                intRebateID = 0;
                            }
                            else
                            {
                                intRebateID = Convert.ToInt32(RebateID.Text);
                                //clear this RebateID from all existing activities
                                RebateHeaderDataAccess.ClearRebateIDForActivities(intRebateID);
                            }
                        }

                        if (intRebateID != 0)
                        {
                            //loop through selected activities allocating new RebateID, updating details, and populating ActivityToProduct records
                            foreach (DataRow rw in tbl.Rows)
                            {
                                Activity a = CopyFromTable(rw);
                                a.RebateID = intRebateID.ToString();
                                if (a.ActivityID == 0)
                                //new Activity
                                {
                                    RebateHeaderDataAccess.InsertActivityForRebate(a);
                                }
                                else
                                //existing activity (includes clearing the ActivityToProduct records)
                                {
                                    RebateHeaderDataAccess.UpdateActivityForRebate(a);
                                }
                                //Populate ActivityToProduct records
                                foreach (DataRow rwA in tblA.Rows)
                                {
                                    RebateHeaderDataAccess.InsertActivityToProduct(a.ActivityID, rwA[0].ToString(), Convert.ToDouble(rwA[2])/100);
                                }
                            }
                            LeaveScreen();
                        }
                    }
                    
                }
            }
        //}
        //catch 
        //{
        //    MsgBox("Error", "An error occurred during saving.");
        //}
    }

    protected void cmdApprove_Click(object sender, EventArgs e)
    {
        string strMessage;
        //loop through selected activities creating history records and updating last updated date - added 02/12/12
        DataTable tbl = (DataTable)Session["SelectedItems"];
        foreach (DataRow rw in tbl.Rows)
        {
            int intActivityID = Convert.ToInt32(rw["ActivityID"]);
            {
                ActivityDataAccess.UpdateActivityLastUpdated(intActivityID, Session["CurrentUserName"].ToString(), 1);
            }
        }
        //
        if (cmdApprove.Text == "Approve")
        {
            //put current date and time in database, and current date on screen
            strMessage = RebateHeaderDataAccess.ApproveRebate(Convert.ToInt32(RebateID.Text), Session["CurrentUserName"].ToString(), Convert.ToInt32(Convert.ToDecimal(txtTotalValue.Text)), "Approve");
            if (strMessage == "OK")
            {
                ApprovedL1Date.Text = System.DateTime.Now.ToString("dd/MM/yy");
                //Fill dropdowns with just the selected names
                ApprovedL1By.Items.Clear();
                ApprovedL1By.Items.Add(new ListItem(Session["CurrentUserName"].ToString(), Session["CurrentUserName"].ToString()));
                ApprovedL1By.SelectedValue = Session["CurrentUserName"].ToString();
                ApprovedL1By.Enabled = false;
                cmdApprove.Text = "Clear";
            }
            else
            {
                MsgBox("Error", strMessage);
            }
        }
        else //clear
        {
            strMessage = RebateHeaderDataAccess.ApproveRebate(Convert.ToInt32(RebateID.Text), Session["CurrentUserName"].ToString(), Convert.ToInt32(Convert.ToDecimal(txtTotalValue.Text)), "Clear");
            ApprovedL1Date.Text = String.Empty;
            ApprovedL1By.SelectedValue = Session["CurrentUserName"].ToString();
            ApprovedL1By.Enabled = true;
            cmdApprove.Text = "Approve";
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        LeaveScreen();
    }

    protected void ApprovedL1By_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ApprovedL1By.SelectedValue == Session["CurrentUserName"].ToString() && ApprovedL1Date.Text == String.Empty && RebateID.Text.Trim() != String.Empty)
        {
            cmdApprove.Visible = true;
            cmdApprove.Text = "Approve";
        }
        else
        {
            if (ApprovedL1By.SelectedValue == Session["CurrentUserName"].ToString() && ApprovedL1Date.Text != String.Empty)
            {
                cmdApprove.Visible = true;
                cmdApprove.Text = "Clear";
            }
            else
            {
                cmdApprove.Visible = false;
            }
        }
    }

#endregion

#region gridevents

    double dblTotal = 0;
    double dblTotalCost = 0;

    protected void grd_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ApprovedL1Date.Text.Trim() == string.Empty)
        {
            cmdEdit.Enabled = true;
            cmdRemove.Enabled = true;
        }
    }

    protected void grd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            double dblCurrentItem = Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "TotalCost"));
            //increment the total 
            dblTotalCost += dblCurrentItem;
            
            //highlight on hover
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(this.grd, "Select$" + e.Row.RowIndex);
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[5].Text = "Total";
            //display the total in grid footer
            e.Row.Cells[6].Text = String.Format("{0:0.00}", dblTotalCost);
            //populate approvers drop-down (added 07/05/12 because needs to change if value changes)
            txtTotalValue.Text = dblTotalCost.ToString();
            //colour code the cell 
            //if (dblTotal == 100) { e.Row.Cells[2].BackColor = Color.LightGreen; } else { e.Row.Cells[2].BackColor = Color.PapayaWhip; }
        }
    }

    protected void grd_Sorting(object sender, GridViewSortEventArgs e)
    {
    }

    protected void grdActivityToProduct_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            //calculate total SubBrand proportion
            double dblCurrent = Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "PropnPerCent"));
            //increment the total
            dblTotal += dblCurrent;

            //highlight on hover
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            e.Row.Attributes["onclick"] = cs.GetPostBackClientHyperlink(this.grdActivityToProduct, "Select$" + e.Row.RowIndex);
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[1].Text = "Total";
            //display the total in grid and in hidden control
            e.Row.Cells[2].Text = dblTotal.ToString();
            txtTotalProportion.Text = dblTotal.ToString();
            //colour code the cell 
            if (dblTotal == 100) { e.Row.Cells[2].BackColor = Color.LightGreen; } else { e.Row.Cells[2].BackColor = Color.PapayaWhip; }
        }
    }

    protected void grdActivityToProduct_Sorting(object sender, EventArgs e)
    {
    }

#endregion

#region gridbuttons

    protected void cmdAdd_Click(object sender, EventArgs e)
    //transfer activity from unallocated drop down to activity grid (not to database yet)
    {
        //try
        //{
//            if (Session["SelectedItems"] == null) //check for session expiry
//            {
//                MsgBox("", "Your session has expired.");
//                Response.Redirect("ProjectView.aspx");
//            }
//            else
//            {
                if (cboSelectItem.Items.Count == 0)
                {
                    MsgBox("", "No items to add.");
                }
                else
                {
                    //transfer activity details from cboSelectItem into activity object 
                    Activity a = ActivityDataAccess.SelectActivity(Convert.ToInt32(cboSelectItem.SelectedValue));
                    //retrieve temp table from session
                    DataTable tbl = (DataTable)Session["SelectedItems"];
                    //loop through tbl to see if record with this unique ID has already been added
                    //to prevent duplication if refresh clicked
                    bool duplicate = false;
                    for (int i = tbl.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = tbl.Rows[i];
                        string dtitem = dr["ActivityID"].ToString();
                        if (dtitem == a.ActivityID.ToString())
                        {
                            duplicate = true;
                        }
                    }
                    if (!duplicate)
                    {
                        //add the selected item to the table
                        DataRow dr = tbl.NewRow();
                        CopyToTable(a, dr);
                        tbl.Rows.Add(dr);
//                        //populate supplier in POHeader if known from activity
//                        //***Added 10/11/10***
//                        if (a.SupplierID.ToString() != "")
//                        {
//                            DataTable tblS = SupplierDataAccess.SelectSupplier(Convert.ToInt32(a.SupplierID));
//                            SupplierID.Text = tblS.Rows[0]["SupplierID"].ToString();
//                            SAPVendorCode.Text = tblS.Rows[0]["SAPVendorCode"].ToString();
//                            SupplierName.Text = tblS.Rows[0]["SupplierName"].ToString();
//                        }
                    }
                    //refresh grid
                    grd.DataSource = tbl;
                    grd.DataBind();
                    if (tbl.Rows.Count == 0) { cmdUpdate.Enabled = false; } else { cmdUpdate.Enabled = true; }
                    //remove from the unallocated activities dropdown list
                    RemoveFromUnallocated(a);
                    //enable/disable buttons
                    cmdEdit.Enabled = true;
                    cmdRemove.Enabled = true;
                    //store basket table in session variable
                    Session["SelectedItems"] = tbl;
                }
            //}
        //}
        //catch
        //{
        //    MsgBox("Error", "An error has occurred.");
        //}
    }

    protected void cmdEdit_Click(object sender, EventArgs e)
    {
        //try //in case no selected item 
        //{
            if (grd.SelectedValue.ToString() != "")
            {
                Activity a = new Activity();
                DataTable tbl;
                tbl = (DataTable)Session["SelectedItems"];

                //loop through tbl to find the record
                for (int i = tbl.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow dr = tbl.Rows[i];
                    string dtitem = dr["ActivityID"].ToString();
                    if (dtitem == grd.SelectedValue.ToString())
                    {
                        a = CopyFromTable(dr);
                    }
                }

                //edit record
                ActivityEdit1.PopulateForm(a);
//                ActivityEdit1.SupplierIDValue = SupplierID.Text; //also disables the supplier field
                ActivityEdit1.SpendTypeIDValue = "2";
                cmdSaveActivity.Enabled = true;
                cmdCancelActivity.Enabled = true;
                cmdUpdate.Enabled = false;
                cmdCancel.Enabled = false;
                cmdPrint.Enabled = false;
                cmdAdd.Enabled = false;
                cmdNew.Enabled = false;
                cmdEdit.Enabled = false;
                cmdRemove.Enabled = false;

                Session["Mode"] = "New";
            }
        //}
        //catch { }
    }

    protected Activity CopyFromTable(DataRow dr)
    {
        Activity a = new Activity();

        a.ActivityID = Convert.ToInt32(dr["ActivityID"]);
        a.ProjectToCustomerID = Convert.ToInt32(dr["ProjectToCustomerID"]);
        a.ActivityName = dr["ActivityName"].ToString();
        a.ShortText = dr["ShortText"].ToString();
        a.ItemQuantity = Convert.ToInt32(dr["ItemQuantity"]);
        a.UnitPrice = Convert.ToDecimal(dr["UnitPrice"]);
        a.MonthRequired = dr["MonthRequired"].ToString();
        a.ProjectMonth = dr["ProjectMonth"].ToString();
        a.MonthShortName = dr["MonthShortName"].ToString();
        a.POID = dr["POID"].ToString();
        a.RebateID = dr["RebateID"].ToString();
        a.DocumentNumber = dr["DocumentNumber"].ToString();
        a.BudgetResponsibilityAreaID = BudgetResponsibilityAreaID.Text;
        a.SpendTypeID = Convert.ToInt32(dr["SpendTypeID"]);
        a.ActivityTypeID = Convert.ToInt32(dr["ActivityTypeID"]);
        a.ActivityTypeName = dr["ActivityTypeName"].ToString();
        a.SupplierID = dr["SupplierID"].ToString();
        a.SupplierRefNo = dr["SupplierRefNo"].ToString();
        a.Comments = dr["Comments"].ToString();
        a.LastUpdatedBy = dr["LastUpdatedBy"].ToString();
        a.CreatedBy = dr["CreatedBy"].ToString();
        a.Month01 = dr["Month01"].ToString();
        a.Month02 = dr["Month02"].ToString();
        a.Month03 = dr["Month03"].ToString();
        a.Month04 = dr["Month04"].ToString();
        a.Month05 = dr["Month05"].ToString();
        a.Month06 = dr["Month06"].ToString();
        a.Month07 = dr["Month07"].ToString();
        a.Month08 = dr["Month08"].ToString();
        a.Month09 = dr["Month09"].ToString();
        a.Month10 = dr["Month10"].ToString();
        a.Month11 = dr["Month11"].ToString();
        a.Month12 = dr["Month12"].ToString();
        return a;
    }
    
    protected void cmdNew_Click(object sender, EventArgs e)
    {
        //enable and populate controls for new TCC record
        ActivityEdit1.NewRecord();
//        ActivityEdit1.SupplierIDValue = SupplierID.Text;
        ActivityEdit1.BudgetResponsibilityAreaIDSelected = BudgetResponsibilityAreaID.Text;
        ActivityEdit1.SpendTypeIDValue = "2";
        //ActivityEdit1.FillActivityType(1, Convert.ToInt32(BudgetResponsibilityAreaID.Text), 1); remmed out 28/01/13

        //enable/disable buttons
        cmdSaveActivity.Enabled = true;
        cmdCancelActivity.Enabled = true;
        cmdUpdate.Enabled = false;
        cmdCancel.Enabled = false;
        cmdPrint.Enabled = false;
        cmdAdd.Enabled = false;
        cmdEdit.Enabled = false;
        cmdNew.Enabled = false;
        cmdRemove.Enabled = false;

        grd.SelectedIndex = -1;
        Session["Mode"] = "New";
    }

    protected void cmdRemove_Click(object sender, EventArgs e)
    //remove the selected line item from the grid
    {
        //try //in case no selected item 
        //{
            //loop through the table and delete if criteria found
            //*** at present removing one "New" record will remove them all - perhaps use ActivityName as criterion?***
            DataTable tbl = (DataTable)Session["SelectedItems"];
            //mark record for deletion
            for (int i = tbl.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = tbl.Rows[i];
                string dtitem = dr["ActivityID"].ToString();
                if (dtitem == grd.SelectedValue.ToString())
                {
                    tbl.Rows[i].Delete();
                }
            }
            //delete
            tbl.AcceptChanges();
            //add back in to the Select Items dropdown list
            cboSelectItem.Items.Add(new ListItem(grd.SelectedRow.Cells[1].Text, grd.SelectedValue.ToString()));
            //refresh grid
            grd.DataSource = tbl;
            grd.DataBind();
            if (tbl.Rows.Count == 0) { cmdUpdate.Enabled = false; } else { cmdUpdate.Enabled = true; }
            //enable buttons
            cmdEdit.Enabled = true;
            cmdRemove.Enabled = true;
            //store basket table back in session variable
            Session["SelectedItems"] = tbl;
        //}
        //catch { }
    }

#endregion

#region methods

    protected DataTable PopulateNewRebate(DataRow r)
    //common code for the two "new rebate" possibilities
    {
        int intCustomerID = Convert.ToInt32(CustomerID.Text);
        //Populate CustomerName2 and SAPCustomerCode defaults from customer record
        Customer c = CustomerDataAccess.SelectCustomer(intCustomerID);
        CustomerName2.Text = c.CustomerName;
        SAPCustomerCode.Text = c.SAPCustomerCode;        
        //Fill dropdowns with available approvers/creators 
        FillApprovers(Convert.ToInt32(BudgetResponsibilityAreaID.Text), Convert.ToInt32(CustomerID.Text), 0, 0);
        FillRebateCreators(Convert.ToInt32(BudgetResponsibilityAreaID.Text));
        //populate RequestedBy with current user
        RequestedBy.Text = Session["CurrentUserName"].ToString();
        //set up blank product list
        grdActivityToProduct.DataSource = "";
        grdActivityToProduct.DataBind();
        txtProportion.Text = "100";
        //set up blank selected items table
        DataTable tbl = ActivityDataAccess.BlankSelectedItemsTable();
        return tbl;
    }

    protected DataTable PopulateExistingRebateID(Rebate r)
    //common code for the two "existing rebate" possibilities
    {
        //populate header fields
        RebateID.Text = r.RebateID.ToString();
        int intRebateID = Convert.ToInt32(r.RebateID);
        //ConditionTypeCode.Text = r.ConditionTypeCode;
        AgreementTypeCode.Text = r.AgreementTypeCode;
        SAPCustomerCode.Text = r.SAPCustomerCode;
        CustomerName2.Text = r.CustomerName2.ToString();
        if (Convert.ToDateTime(r.InStoreStartDate).Day < 10) { ValidFromDay.SelectedValue = "0" + Convert.ToDateTime(r.InStoreStartDate).Day.ToString(); } else { ValidFromDay.SelectedValue = Convert.ToDateTime(r.InStoreStartDate).Day.ToString(); }
        if (Convert.ToDateTime(r.InStoreStartDate).Month < 10) { ValidFromMonth.SelectedValue = "0" + Convert.ToDateTime(r.InStoreStartDate).Month.ToString(); } else { ValidFromMonth.SelectedValue = Convert.ToDateTime(r.InStoreStartDate).Month.ToString(); }
        if (Convert.ToDateTime(r.InStoreEndDate).Day < 10) { ValidToDay.SelectedValue = "0" + Convert.ToDateTime(r.InStoreEndDate).Day.ToString(); } else { ValidToDay.SelectedValue = Convert.ToDateTime(r.InStoreEndDate).Day.ToString(); }
        if (Convert.ToDateTime(r.InStoreEndDate).Month < 10) { ValidToMonth.SelectedValue = "0" + Convert.ToDateTime(r.InStoreEndDate).Month.ToString(); } else { ValidToMonth.SelectedValue = Convert.ToDateTime(r.InStoreEndDate).Month.ToString(); }
        Comments.Text = r.Comments;
        RequestedBy.Text = r.RequestedBy.ToLower();
        AgreementNumber.Text = r.AgreementNumber.ToUpper();
        //Set up Rebate Creator drop-down
        if (AgreementNumber.Text.Trim() == String.Empty)
        {
            //Fill dropdowns with available creators 
            FillRebateCreators(Convert.ToInt32(BudgetResponsibilityAreaID.Text));
        }
        else
        {
            //Fill dropdowns with just the selected names
            RebateCreator.Items.Add(new ListItem(r.RebateCreator.ToLower(), r.RebateCreator.ToLower()));
        }
        RebateCreator.SelectedValue = r.RebateCreator.ToLower();
        //Set up Approver drop-down
        if (ApprovedL1Date.Text == String.Empty)
        {
            //Fill dropdowns with available approvers/creators 
            FillApprovers(Convert.ToInt32(BudgetResponsibilityAreaID.Text), Convert.ToInt32(CustomerID.Text), 0, 0);
        }
        else
        {
            //Fill dropdowns with just the selected names
            ApprovedL1By.Items.Add(new ListItem(r.ApprovedL1By.ToLower(), r.ApprovedL1By.ToLower()));
        }
        ApprovedL1By.SelectedValue = r.ApprovedL1By.ToLower();
        //Set up Approved Date         
        if (r.ApprovedL1Date.ToString() == String.Empty) { ApprovedL1Date.Text = String.Empty; } else { ApprovedL1Date.Text = DateTime.Parse(r.ApprovedL1Date.ToString()).ToString("dd/MM/yy"); }
        //populate grdActivityToProduct using the first activity in the rebate
        DataTable tblA = RebateHeaderDataAccess.FillActivityToProduct(intRebateID);
        Session["SelectedProductItems"] = tblA;
        grdActivityToProduct.DataSource = tblA;
        grdActivityToProduct.DataBind();
        //enable controls (must be after ProjectID is populated)
        if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 15)
        {
            ToggleControlState(true);
        }
        //populate selected items table
        DataTable tbl = RebateHeaderDataAccess.SelectActivitiesForRebateID(intRebateID);
        return tbl;
    }

    protected void RemoveFromUnallocated(Activity a)
    {
        DropDownList d = cboSelectItem;
        for (int i = 0; i < d.Items.Count; i++)
        {
            if (d.Items[i].Value == a.ActivityID.ToString())
            {
                d.Items.RemoveAt(i);
                return;
            }
        }
    }

    protected void LeaveScreen()
    {
        //clear selected items 
        Session["SelectedItems"] = null;
        Session["SelectedActivityID"] = null;

        if ((String)Session["ReturnToScreen"] == "Worklist")
        {
            Response.Redirect("Worklist.aspx");
        }
        else if((String)Session["ReturnToScreen"] == "ActivityView_rs")
        {
            Response.Redirect("ActivityView.aspx");
        }
        else
        {
            if ((String)Session["ReturnToScreen"] == "ActivityView")
            {
                Response.Redirect("Default.aspx");
            }
            else //(should never happen)
            {
                if ((String)Session["ReturnToScreen"] == "PurchOrderView")
                {
                    Response.Redirect("PurchOrderView.aspx");
                }
                else
                {
                    Response.Redirect("Default.aspx");
                }
            }
        }
    }

    protected void CopyToTable(Activity a, DataRow dr)
    {
        dr["ActivityID"] = a.ActivityID;
        dr["ProjectToCustomerID"] = ProjectToCustomerID.Text;
        dr["ActivityName"] = a.ActivityName;
        dr["ShortText"] = a.ShortText;
        dr["ItemQuantity"] = a.ItemQuantity;
        dr["UnitPrice"] = String.Format("{0:0.00}", a.UnitPrice);
        dr["MonthRequired"] = a.MonthRequired;
        dr["ProjectMonth"] = a.ProjectMonth;
        dr["MonthShortName"] = a.MonthShortName;
//        dr["POID"] = POID.Text;
        dr["RebateID"] = RebateID.Text;
        dr["DocumentNumber"] = a.DocumentNumber;
        dr["SpendTypeID"] = a.SpendTypeID;
        dr["ActivityTypeID"] = a.ActivityTypeID;
        dr["ActivityTypeName"] = a.ActivityTypeName;
 //       if (a.SupplierID != "")
 //       {
 //           dr["SupplierID"] = a.SupplierID;
 //       }
 //       dr["SupplierRefNo"] = a.SupplierRefNo;
        dr["Comments"] = a.Comments;
        dr["LastUpdatedBy"] = a.LastUpdatedBy;
        dr["CreatedBy"] = a.CreatedBy;
        dr["Month01"] = a.Month01;
        dr["Month02"] = a.Month02;
        dr["Month03"] = a.Month03;
        dr["Month04"] = a.Month04;
        dr["Month05"] = a.Month05;
        dr["Month06"] = a.Month06;
        dr["Month07"] = a.Month07;
        dr["Month08"] = a.Month08;
        dr["Month09"] = a.Month09;
        dr["Month10"] = a.Month10;
        dr["Month11"] = a.Month11;
        dr["Month12"] = a.Month12;
        dr["TotalCost"] = String.Format("{0:0.00}", a.UnitPrice * a.ItemQuantity);
    }

    protected void MsgBox(string strTitle, string strMessage)
    {
        ClientScript.RegisterStartupScript(this.GetType(), strTitle, "alert('" + Common.Left(strMessage, 35) + "');", true);
    }

    protected void AddSelectedProduct(string strPRODH, double dblProportion)
    {
        DataTable tbl = (DataTable)Session["SelectedProductItems"];
        //loop through tbl to see if record with this PPH value has already been added (to prevent duplication)
        bool duplicate = false;
        for (int i = tbl.Rows.Count - 1; i >= 0; i--)
        {
            DataRow dr = tbl.Rows[i];
            string dtitem = dr["PRODH"].ToString();
            if (dtitem == strPRODH)
            {
                duplicate = true;
            }
        }
        //if not a duplicate add the item to the table
        if (!duplicate)
        {
            DataRow dr = tbl.NewRow();
            dr["PRODH"] = strPRODH;
            dr["VTEXT"] = Common.ADOLookup("VTEXT", "tblPPH", "PRODH = '" + strPRODH + "'");
            dr["PropnPerCent"] = dblProportion;
            tbl.Rows.Add(dr);
        }
        //rebind to grid
        grdActivityToProduct.DataSource = tbl;
        grdActivityToProduct.DataBind();
        //store basket table in session variable
        Session["SelectedProductItems"] = tbl;
        //clear fields
        cboAddProduct.SelectedValue = "0";
        txtProportion.Text = "";
    }

    protected void RemoveProduct()
    //remove the selected line item from the grid
    {
        try //because an item may not be selected in the grid or the Session variable may not be valid
        {
            //loop through the table and delete if criteria found
            DataTable tbl = (DataTable)Session["SelectedProductItems"];
            //mark record for deletion
            for (int i = tbl.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = tbl.Rows[i];
                string dtitem = dr["PRODH"].ToString();
                if (dtitem == grdActivityToProduct.SelectedValue.ToString())
                {
                    tbl.Rows[i].Delete();
                }
            }
            //delete
            tbl.AcceptChanges();
            //rebind to grid
            grdActivityToProduct.DataSource = tbl;
            grdActivityToProduct.DataBind();
            //recalculate remaining proportion if no rows left (if there are rows this is calculated by the onrowdatabound event
            if (grdActivityToProduct.Rows.Count == 0)
            {
                txtTotalProportion.Text = "0";
            }
            //store basket table in session variable
            Session["SelectedProductItems"] = tbl;
        }
        catch { }
    }

        protected void ToggleControlState(bool state)
    {
        //enable or disable textboxes and dropdowns
        foreach (Control c in Controls)
        {
            if (c is TextBox) { ((TextBox)c).Enabled = state; }
            if (c is DropDownList) { ((DropDownList)c).Enabled = state; }
        }
        //other controls
        grdActivityToProduct.Enabled = state;
        cmdAddProduct.Enabled = state;
        cmdRemoveProduct.Enabled = state;
    }

#endregion

#region dropdowns

    protected void FillActivity(int intProjectToCustomerID)
    //Fill the list of unallocated TCC activities
    {
        {
            SqlCommand cmd = DataAccess.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_ap_FillUnallocatedActivityTCC";
            cmd.Parameters.AddWithValue("@ProjectToCustomerID", intProjectToCustomerID);
            DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
            DropDownList d = cboSelectItem;
            d.DataSource = tbl;
            d.DataBind();
        }
    }

    protected void FillRebateCreators(int intBudgetResponsibilityAreaID)
    //Fill the NPI Coordinator dropdownlist for the budget responsibility area
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillRebateCreators";
        cmd.Parameters.AddWithValue("@BudgetResponsibilityAreaID", intBudgetResponsibilityAreaID);
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = RebateCreator;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillApprovers(int intBudgetResponsibilityAreaID, int intCustomerID, int intBrandID, double dblRebateValue)
    //Fill the Approver dropdownlist for the budget responsibility area, customer, brand and rebate value - amended 05/07/12
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillRebateApprovers";
        cmd.Parameters.AddWithValue("@BudgetResponsibilityAreaID", intBudgetResponsibilityAreaID);
        cmd.Parameters.AddWithValue("@CustomerID", intCustomerID);
        cmd.Parameters.AddWithValue("@BrandID", intBrandID);
        cmd.Parameters.AddWithValue("@RebateValue", dblRebateValue);
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = ApprovedL1By;
        //look for current selection in tbl
        bool found = false;
        for (int i = tbl.Rows.Count - 1; i >= 0; i--)
        {
            DataRow dr = tbl.Rows[i];
            string dtitem = dr["EntityName"].ToString();
            if (dtitem == d.SelectedValue.ToString())
            {
                found = true;
            }
        }
        //if not found, clear selected value
//        MsgBox("Msg", found.ToString());
        if (found == false) { d.SelectedValue = "0"; }
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillMonth(DropDownList d, int intShowPhasedOption, string strZeroValueText)
    //fill months dropdownlist without an <All> option *** move to Common? ***
    {
        //Get a data table object containing the values
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ge_FillMonth";
        cmd.Parameters.AddWithValue("@ZeroValueText", strZeroValueText);
        cmd.Parameters.AddWithValue("@ShowPhasedOption", intShowPhasedOption);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillAgreementType()
    //fill Agreement Type dropdownlist without an <All> option 
    {
        //Get a data table object containing the values
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillAgreementType";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        //DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        //DropDownList d = AgreementTypeCode;
        //d.DataSource = tbl;
        //d.DataBind();
    }

    protected void FillDay(DropDownList d)
    //populate days dropdown
    {
        string j;
        d.Items.Add(new ListItem("", ""));
        for (int i = 1; i < 32; i++)
        {
            if (i < 10) { j = "0" + i.ToString(); } else { j = i.ToString(); }
            d.Items.Add(new ListItem(i.ToString(), j));
        }
    }

    protected void FillProduct()
    //Fill dropdownlist of all PPH values relating to project
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillProductsForRebates";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        cmd.Parameters.AddWithValue("@UserName", Session["CurrentUserName"].ToString());
        cmd.Parameters.AddWithValue("@ProjectID", Convert.ToInt32(ProjectID.Text));
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboAddProduct;
        d.DataSource = tbl;
        d.DataBind();
    }

#endregion

#region editactivitybuttons

    protected void cmdSaveActivity_Click(object sender, EventArgs e)
    //save activity record to data table (not to database yet)
    {
        if (Page.IsValid)
        {
            //transfer activity details from Activity Edit area into Activity object 
            Activity a = ActivityEdit1.SelectActivity;
            //retrieve temp table from session
            DataTable tbl = (DataTable)Session["SelectedItems"];
            //if not a new record prepare by removing the current record from the temporary table
            if (a.ActivityID.ToString() != "0")
            {
                //first, mark record for deletion
                string dtitem;
                for (int i = tbl.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow r = tbl.Rows[i];
                    dtitem = r["ActivityID"].ToString();
                    if (dtitem == a.ActivityID.ToString())
                    {
                        tbl.Rows[i].Delete();
                    }
                }
                //then delete
                tbl.AcceptChanges();
            }
            //add the selected item to the table
            DataRow dr = tbl.NewRow();
            CopyToTable(a, dr);
            tbl.Rows.Add(dr);
            //Clear form
            ActivityEdit1.ClearForm();
            //enable/disable buttons
            cmdSaveActivity.Enabled = false;
            cmdCancelActivity.Enabled = false;
            //cmdUpdate.Enabled = true; ***see below***
            cmdCancel.Enabled = true;
            cmdPrint.Enabled = true;
            cmdAdd.Enabled = true;
            cmdEdit.Enabled = false;
            cmdNew.Enabled = true;
            cmdRemove.Enabled = false;
            //refresh grid
            grd.DataSource = tbl;
            grd.DataBind();
            if (tbl.Rows.Count == 0) { cmdUpdate.Enabled = false; } else { cmdUpdate.Enabled = true; }
            //store basket table back in session variable
            Session["SelectedItems"] = tbl;
        }
    }

    protected void cmdCancelActivity_Click(object sender, EventArgs e)
    //cancel editing activity record
    {
        //clear form
        ActivityEdit1.ClearForm();
        //enable/disable buttons
        cmdSaveActivity.Enabled = false;
        cmdCancelActivity.Enabled = false;
        cmdUpdate.Enabled = true;
        cmdCancel.Enabled = true;
        cmdPrint.Enabled = true;
        cmdAdd.Enabled = true;
        cmdEdit.Enabled = false;
        cmdNew.Enabled = true;
        cmdRemove.Enabled = false;

    }

#endregion

#region subformevents

    protected void cmdAddProduct_Click(object sender, EventArgs e)
    {
        AddSelectedProduct(cboAddProduct.SelectedValue, Convert.ToDouble(txtProportion.Text));
    }

    protected void cmdRemoveProduct_Click(object sender, EventArgs e)
    {
        RemoveProduct();
    }

#endregion

}
