﻿using System;

public partial class PulsingPlanPriorityView : System.Web.UI.Page

{
    string strSingular = "Pulsing Plan Priority";
    string strPlural = "Pulsing Plan Priority";

    protected void Page_Load(object sender, EventArgs e)
    {
        //check session not expired and high enough user level
        if (Session["SelectedProjectToCustomerID"] == null || Convert.ToInt16(Session["CurrentUserLevelID"]) >= 15)
        {
            Response.Redirect("ProjectView.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                mvw.ActiveViewIndex = 0;
                PulsingPlanPriorityGrid1.HeaderText = strPlural;
            }
        }
    }

#region events

    protected void cmdUpdate_Click(object sender, EventArgs e)
    {
        int intResult;
        try 
        {
            if (Session["Mode"].ToString() == "New")
            {
                intResult = PulsingPlanPriorityEdit1.CreateRecord();
            }
            else
            {
                intResult = PulsingPlanPriorityEdit1.UpdateRecord();
            }
            if (intResult == 0)
            {
                MsgBox("Message", "The " + strSingular + " already exists.");
            }
            else
            {
                PulsingPlanPriorityGrid1.RefreshGrid();
                mvw.ActiveViewIndex = 0;
            }
        }
        catch 
        {
            MsgBox("Message", "Could not save record.");
        }
    }

    protected void cmdNew_Click(object sender, EventArgs e)
    {
        mvw.ActiveViewIndex = 1;
        //enable controls for new record
        PulsingPlanPriorityEdit1.NewRecord();
        Session["Mode"] = "New";
        PulsingPlanPriorityEdit1.HeaderText = Session["Mode"].ToString() + " " + strSingular;
    }

    protected void cmdEdit_Click(object sender, EventArgs e)
    {
        try 
        {
            PulsingPlanPriorityEdit1.PopulateForm(Convert.ToInt32(PulsingPlanPriorityGrid1.SelectedValue));
            mvw.ActiveViewIndex = 1;
            Session["Mode"] = "Edit";
            PulsingPlanPriorityEdit1.HeaderText = Session["Mode"].ToString() + " " + strSingular;
        }
        catch { } //in case no selection in grid
    }

    protected void cmdDelete_Click(object sender, EventArgs e)
    {
        try //in case referential integrity prevents deletion
        {
            BAPSMiscDataAccess.DeletePulsingPlanPriority(Convert.ToInt32(PulsingPlanPriorityGrid1.SelectedValue));
            PulsingPlanPriorityEdit1.ClearControls();
            // update grid
            PulsingPlanPriorityGrid1.DataBind();
        }
        catch
        {
            MsgBox("Message", "Could not delete record"); 
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        PulsingPlanPriorityEdit1.ClearControls();
        mvw.ActiveViewIndex = 0;
    }
    
#endregion

#region methods

    protected void MsgBox(string strTitle, string strMessage)
    {
        ClientScript.RegisterStartupScript(this.GetType(), strTitle, "alert('" + Common.Left(strMessage, 35) + "');", true);
    }

#endregion

}
