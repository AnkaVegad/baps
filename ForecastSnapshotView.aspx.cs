﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ForecastSnapshotView: System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //check session not expired and high enough user level 
        if (Session["SelectedProjectToCustomerID"] == null || Convert.ToInt16(Session["CurrentUserLevelID"]) >= 5)
        {
            Response.Redirect("ProjectView.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                FillYear();
                //get current year from session variable set in global.asax
                cboYear.SelectedValue = Session["SelectedYear"].ToString();
                FillSnapshotMonthYear();
            }
            //Populate the snapshot grids
            grd.DataBind();
            grdBudget.DataBind();
        }
    }

#region events

    protected void cmdDelete_Click(object sender, EventArgs e)
    //delete the selected Forecast Snapshot record
    {
        try
        {
            ForecastSnapshotDataAccess.DeleteForecastSnapshot(grd.SelectedValue.ToString().Substring(0, 4), grd.SelectedValue.ToString().Substring(4, 2));
            //Populate the snapshot grids
            grd.DataBind();
            grdBudget.DataBind();
        }
        catch
        {
            MsgBox("Error", "Could not delete snapshot");
        }
    }

    protected void cmdNew_Click(object sender, EventArgs e)
    //insert a Forecast Snapshot record
    {
        try
        {
            int intResult = ForecastSnapshotDataAccess.InsertForecastSnapshot(cboSnapshotMonthYear.SelectedValue.ToString().Substring(0, 4), cboSnapshotMonthYear.SelectedValue.ToString().Substring(4, 2), Session["CurrentUserName"].ToString());
            if(intResult==1) 
            {
                MsgBox("BAPS", "Created successfully");
                //Populate the snapshot grids
                grd.DataBind();
                grdBudget.DataBind();
            }
            else { MsgBox("BAPS", "Could not create snapshot"); }        
        }
        catch //in case no selected item in grid 
        {
            MsgBox("BAPS", "Could not create snapshot");        
        } 
    }

#endregion

#region gridevents

    protected void grd_Sorted(object sender, EventArgs e)
    {

    }

    protected void grd_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;
            //highlight on hover
            e.Row.Attributes["onmouseover"] = "this.style.cursor='hand';this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#AAB0FF'";
            e.Row.Attributes["onmouseout"] = "this.style.backgroundColor=this.originalstyle;";
            e.Row.Attributes["onclick"] = cs.GetPostBackClientHyperlink(this.grd, "Select$" + e.Row.RowIndex);
        }
    }

#endregion

#region dropdowns

    protected void FillYear()
    //fill year dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillYear";
        cmd.Parameters.AddWithValue("@ZeroValueText", "[None]");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboYear;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void FillSnapshotMonthYear()
    //fill year dropdownlist
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillSnapshotMonthYear";
        cmd.Parameters.AddWithValue("@ZeroValueText", "");
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        DropDownList d = cboSnapshotMonthYear;
        d.DataSource = tbl;
        d.DataBind();
    }

    protected void cboYear_TextChanged(object sender, EventArgs e)
    {
        //Session["SelectedYear"] = cboYear.SelectedValue;
        //SelectFirstRow();
    }

#endregion

#region methods

    protected void MsgBox(string strTitle, string strMessage)
    {
        ClientScript.RegisterStartupScript(this.GetType(), strTitle, "alert('" + Common.Left(strMessage, 35) + "');", true);
    }

#endregion
}
