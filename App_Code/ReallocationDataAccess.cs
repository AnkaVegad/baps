﻿using System;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for ReallocationDataAccess
/// </summary>
public class ReallocationDataAccess
{
	public ReallocationDataAccess()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static string TransactReallocation(int intFromProjectToCustomerID, int intToProjectToCustomerID, int intFromSpendTypeID, int intToSpendTypeID, string strUserName, int intBudgetAmount, string strComments, string ApproverName)
    //process a budget reallocation transaction
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_TransactReallocation201501";

        // add parameters with their values
        cmd.Parameters.AddWithValue("@FromProjectToCustomerID", intFromProjectToCustomerID);
        cmd.Parameters.AddWithValue("@ToProjectToCustomerID", intToProjectToCustomerID);
        cmd.Parameters.AddWithValue("@FromSpendTypeID", intFromSpendTypeID);
        cmd.Parameters.AddWithValue("@ToSpendTypeID", intToSpendTypeID);
        cmd.Parameters.AddWithValue("@UserName", strUserName);
        cmd.Parameters.AddWithValue("@BudgetAmount", intBudgetAmount);
        cmd.Parameters.AddWithValue("@Comments", strComments);
        cmd.Parameters.AddWithValue("@ApproverName", ApproverName);

        SqlParameter param = cmd.CreateParameter();
        param.ParameterName = "@ErrorMessage";
        param.Direction = ParameterDirection.Output;
        param.DbType = DbType.String;
        param.Size = 50;
        cmd.Parameters.Add(param);

        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);

        string strErrorMessage = cmd.Parameters["@ErrorMessage"].Value.ToString();
        return strErrorMessage;
    }

    //public static string TransactReallocation(int intFromProjectToCustomerID, int intToProjectToCustomerID, int intFromSpendTypeID, int intToSpendTypeID, string strUserName, int intBudgetAmount, string strComments)
    ////process a budget reallocation transaction
    //{
    //    SqlCommand cmd = DataAccess.CreateCommand();
    //    cmd.CommandText = "sp_ap_TransactReallocation";

    //    // add parameters with their values
    //    cmd.Parameters.AddWithValue("@FromProjectToCustomerID", intFromProjectToCustomerID);
    //    cmd.Parameters.AddWithValue("@ToProjectToCustomerID", intToProjectToCustomerID);
    //    cmd.Parameters.AddWithValue("@FromSpendTypeID", intFromSpendTypeID);
    //    cmd.Parameters.AddWithValue("@ToSpendTypeID", intToSpendTypeID);
    //    cmd.Parameters.AddWithValue("@UserName", strUserName);
    //    cmd.Parameters.AddWithValue("@BudgetAmount", intBudgetAmount);
    //    cmd.Parameters.AddWithValue("@Comments", strComments);

    //    SqlParameter param = cmd.CreateParameter();
    //    param.ParameterName = "@ErrorMessage";
    //    param.Direction = ParameterDirection.Output;
    //    param.DbType = DbType.String;
    //    param.Size = 50;
    //    cmd.Parameters.Add(param);

    //    // execute the query to update the database
    //    int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);

    //    string strErrorMessage = cmd.Parameters["@ErrorMessage"].Value.ToString();
    //    return strErrorMessage;
    //}

    public static DataTable SelectReallocationFrom(int intProjectToCustomerID)
    //Select budget reallocations from the specified project/customer combination
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "SelectReallocationFrom";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ProjectToCustomerID", intProjectToCustomerID);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static DataTable SelectReallocationTo(int intProjectToCustomerID)
    //Select budget reallocations to the specified project/customer combination
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "SelectReallocationTo";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ProjectToCustomerID", intProjectToCustomerID);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }


    public static string CopyActivityToProject(int intActivityID, int intProjectToCustomerID, string strUserName)
    //copy an activity to another project
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "CopyActivityToProject";

        // add parameters with their values
        cmd.Parameters.AddWithValue("@ActivityID", intActivityID);
        cmd.Parameters.AddWithValue("@ProjectToCustomerID", intProjectToCustomerID);
        cmd.Parameters.AddWithValue("@UserName", strUserName);

        SqlParameter param = cmd.CreateParameter();
        param.ParameterName = "@Message";
        param.Direction = ParameterDirection.Output;
        param.DbType = DbType.String;
        param.Size = 50;
        cmd.Parameters.Add(param);

        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);

        string strErrorMessage = cmd.Parameters["@Message"].Value.ToString();
        return strErrorMessage;
    }
}
