﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;

/// <summary>
/// Summary description for ProductDataAccess
/// </summary>
public class ProductDataAccess
{
    public ProductDataAccess()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static DataTable SelectProductByTUEAN(string strTUEAN)
    //return product matching selected TUEAN value
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_md_SelectProductByTUEAN";
        cmd.Parameters.AddWithValue("@TUEAN", strTUEAN);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static int UpdateProduct(string strTUEAN, int intCategoryID, int intBrandID, string strContainerCode, string strPromoName, int intSubCategoryID)
    //update category and brand values for product
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_md_UpdateProduct";
        cmd.Parameters.AddWithValue("@TUEAN", strTUEAN);
        cmd.Parameters.AddWithValue("@CategoryID", intCategoryID);
        cmd.Parameters.AddWithValue("@BrandID", intBrandID);
        cmd.Parameters.AddWithValue("@ContainerCode", strContainerCode);
        cmd.Parameters.AddWithValue("@PromoName", strPromoName);
        cmd.Parameters.AddWithValue("@SubCategoryID", intSubCategoryID);
        int intReturn = DataAccess.ExecuteUpdateCommand(cmd);
        return intReturn;
    }

}
