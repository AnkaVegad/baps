﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;

/// <summary>
/// Summary description for ForecastSnapshotDataAccess
/// </summary>
public class ForecastSnapshotDataAccess
{
	public ForecastSnapshotDataAccess()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static int InsertForecastSnapshot(string strSnapshotYear, string strSnapshotMonth, string strCurrentUser)
    //create a new ForecastSnapshot and return the ID
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "InsertForecastSnapshot";

        // add parameters with their values
        cmd.Parameters.AddWithValue("@SnapshotYear", strSnapshotYear);
        cmd.Parameters.AddWithValue("@SnapshotMonth", strSnapshotMonth);
        cmd.Parameters.AddWithValue("@CurrentUser", strCurrentUser);

        // execute the query to update the database
        int intForecastSnapshotID = DataAccess.ExecuteScalarReturnIdentity(cmd);
        return intForecastSnapshotID;
    }

    public static int DeleteForecastSnapshot(string strSnapshotYear, string strSnapshotMonth)
    //delete an existing ForecastSnapshot
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "DeleteForecastSnapshot";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@SnapshotYear", strSnapshotYear);
        cmd.Parameters.AddWithValue("@SnapshotMonth", strSnapshotMonth);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        return intRowsAffected;
    }
}
