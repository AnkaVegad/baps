﻿using System;
//using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
//using System.Web;

/// <summary>
/// Wraps GLHierarchy data
/// </summary>
public struct GLHierarchy
{
    public int GLHierarchyID;
    public string GLCode;
    public int GLLevel1ID;
    public int GLLevel2ID;
    public int GLLevel3ID;
    //public string CommodityCode;
    public string ActiveIndicator;
    public string CreatedBy;
    public string CreatedDate;
}

/// <summary>
/// Summary description for GLHierarchyDataAccess
/// </summary>
public class GLHierarchyDataAccess
{
	public GLHierarchyDataAccess()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static GLHierarchy SelectGLHierarchy(int intGLHierarchyID)
    //select an existing GLHierarchy from its ID and put it in a GLHierarchy struct object
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_SelectGLHierarchy";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@GLHierarchyID", intGLHierarchyID);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        GLHierarchy g = new GLHierarchy();
        if (tbl.Rows.Count > 0)
        {
            DataRow dr = tbl.Rows[0];
            g.GLHierarchyID = intGLHierarchyID;
            g.GLCode = dr["GLCode"].ToString();
            g.GLLevel1ID = Convert.ToInt32(dr["GLLevel1ID"]);
            g.GLLevel2ID = Convert.ToInt32(dr["GLLevel2ID"]);
            g.GLLevel3ID = Convert.ToInt32(dr["GLLevel3ID"]);
            //g.CommodityCode = String.Empty;
            g.ActiveIndicator = dr["ActiveIndicator"].ToString();
            g.CreatedBy = dr["CreatedBy"].ToString();
            g.CreatedDate = dr["CreatedDate"].ToString();
        }
        return g;
    }

    public static int UpdateGLHierarchy(GLHierarchy g)
    //update an existing GLHierarchy record
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_UpdateGLHierarchy";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@GLHierarchyID", g.GLHierarchyID);
        cmd.Parameters.AddWithValue("@GLCode", g.GLCode);
        cmd.Parameters.AddWithValue("@GLLevel1ID", g.GLLevel1ID);
        cmd.Parameters.AddWithValue("@GLLevel2ID", g.GLLevel2ID);
        cmd.Parameters.AddWithValue("@GLLevel3ID", g.GLLevel3ID);
        cmd.Parameters.AddWithValue("@CommodityCode", String.Empty);
        cmd.Parameters.AddWithValue("@ActiveIndicator", g.ActiveIndicator);
        // execute the query to update the database
        int intResult = DataAccess.ExecuteUpdateCommand(cmd);
        return intResult;
    }

    public static int InsertGLHierarchy(GLHierarchy g)
    //create a new GLHierarchy line and return the ID
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_InsertGLHierarchy";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@GLCode", g.GLCode);
        cmd.Parameters.AddWithValue("@GLLevel1ID", g.GLLevel1ID);
        cmd.Parameters.AddWithValue("@GLLevel2ID", g.GLLevel2ID);
        cmd.Parameters.AddWithValue("@GLLevel3ID", g.GLLevel3ID);
        cmd.Parameters.AddWithValue("@CommodityCode", String.Empty);
        cmd.Parameters.AddWithValue("@ActiveIndicator", g.ActiveIndicator);
        cmd.Parameters.AddWithValue("@CreatedBy", g.CreatedBy);
        // execute the query to update the database
        int intGLHierarchyID = DataAccess.ExecuteScalarReturnIdentity(cmd);
        return intGLHierarchyID;
    }

    public static int DeleteGLHierarchy(int intGLHierarchyID)
    //delete an existing GLHierarchy line
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_DeleteGLHierarchy";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@GLHierarchyID", intGLHierarchyID);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        return intRowsAffected;
    }

}
