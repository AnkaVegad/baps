﻿using System;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Wraps Project data
/// </summary>
public struct Project
{
    public int ProjectID;
    public string ProjectName;
    public string CountryCode;
    public int BudgetResponsibilityAreaID;
    public string BudgetResponsibilityAreaName;
    public string ProjectYear;
    public string ProjectDescription;
    public int PulsingPlanPriorityID;
    public string PulsingPlanPriorityName;
    public string ProjectOwner;
    public string PulsingPlanTiming;
    public string ProjectStartMonth;
    public string ProjectEndMonth;
    public string ProjectObjective;
    public string KPIs;
    public string LastUpdatedBy;
    public string LastUpdatedDate;
    public string CreatedBy;
    public string CreatedDate;
}

/// <summary>
///  Data Access Layer for Project View
/// </summary>
public class ProjectDataAccess
{
	public ProjectDataAccess()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static Project SelectProject(int intProjectID)
    //selects an existing Project and puts it in a Project struct object
    //amended 14/12/14 ProjectStartMonth and ProjectEndMonth added
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_SelectProject";

        // add parameters with their values
        cmd.Parameters.AddWithValue("@ProjectID", intProjectID);

        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);

        Project a = new Project();
        if (tbl.Rows.Count > 0)
        {
            DataRow dr = tbl.Rows[0];
            a.ProjectID = intProjectID;
            a.ProjectName = dr["ProjectName"].ToString();
            a.CountryCode = dr["CountryCode"].ToString();
            a.BudgetResponsibilityAreaID = Convert.ToInt16(dr["BudgetResponsibilityAreaID"]);
            a.BudgetResponsibilityAreaName = dr["BudgetResponsibilityAreaName"].ToString();
            a.ProjectYear = dr["ProjectYear"].ToString();
            a.ProjectDescription = dr["ProjectDescription"].ToString();
            if (dr["PulsingPlanPriorityID"] == DBNull.Value) { a.PulsingPlanPriorityID = 0; } else { a.PulsingPlanPriorityID = Convert.ToInt16(dr["PulsingPlanPriorityID"]); }
            a.ProjectStartMonth = dr["ProjectStartMonth"].ToString();
            a.ProjectEndMonth = dr["ProjectEndMonth"].ToString();
//            a.PulsingPlanTiming = dr["PulsingPlanTiming"].ToString();
            a.ProjectOwner = dr["ProjectOwner"].ToString();
            a.PulsingPlanPriorityName = dr["PulsingPlanPriorityName"].ToString();
            a.ProjectObjective = dr["ProjectObjective"].ToString();
            a.KPIs = dr["KPIs"].ToString();
            a.LastUpdatedBy = dr["LastUpdatedBy"].ToString();
            a.LastUpdatedDate = dr["LastUpdatedDate"].ToString();
            a.CreatedBy = dr["CreatedBy"].ToString();
            a.CreatedDate = dr["CreatedDate"].ToString();
        }

        return a;
    }

    //public static Project SelectProject_Calendar(int intProjectID)
    ////selects an existing Project and puts it in a Project struct object
    ////Written by Vidya (Akira) December 2014
    //{
    //    SqlCommand cmd = DataAccess.CreateCommand();
    //    cmd.CommandText = "SelectProject_Cal";

    //    // add parameters with their values
    //    cmd.Parameters.AddWithValue("@ProjectID", intProjectID);

    //    //execute stored procedure and store results in a data table
    //    DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);

    //    Project a = new Project();
    //    if (tbl.Rows.Count > 0)
    //    {
    //        DataRow dr = tbl.Rows[0];
    //        a.ProjectID = intProjectID;
    //        a.ProjectName = dr["ProjectName"].ToString();
    //        a.CountryCode = dr["CountryCode"].ToString();
    //        a.BudgetResponsibilityAreaID = Convert.ToInt16(dr["BudgetResponsibilityAreaID"]);
    //        a.ProjectYear = dr["ProjectYear"].ToString();
    //        a.ProjectDescription = dr["ProjectDescription"].ToString();
    //        if (dr["PulsingPlanPriorityID"] == DBNull.Value) { a.PulsingPlanPriorityID = 0; } else { a.PulsingPlanPriorityID = Convert.ToInt16(dr["PulsingPlanPriorityID"]); }
    //        a.ProjectStartMonth = dr["ProjectStartMonth"].ToString();
    //        a.ProjectEndMonth = dr["ProjectEndMonth"].ToString();
    //        a.ProjectOwner = dr["ProjectOwner"].ToString();
    //        a.ProjectObjective = dr["ProjectObjective"].ToString();
    //        a.KPIs = dr["KPIs"].ToString();
    //        a.LastUpdatedBy = dr["LastUpdatedBy"].ToString();
    //        a.LastUpdatedDate = dr["LastUpdatedDate"].ToString();
    //        a.CreatedBy = dr["CreatedBy"].ToString();
    //        a.CreatedDate = dr["CreatedDate"].ToString();
    //    }

    //    return a;
    //}

    public static int UpdateProject(Project a)
    //update an existing Project
    //amended 14/12/14 ProjectStartMonth and ProjectEndMonth added
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_UpdateProject";

        // add parameters with their values
        cmd.Parameters.AddWithValue("@ProjectID", a.ProjectID);
        cmd.Parameters.AddWithValue("@ProjectName", a.ProjectName);
        cmd.Parameters.AddWithValue("@BudgetResponsibilityAreaID", a.BudgetResponsibilityAreaID);
        cmd.Parameters.AddWithValue("@ProjectYear", a.ProjectYear);
        cmd.Parameters.AddWithValue("@ProjectDescription", a.ProjectDescription);
        cmd.Parameters.AddWithValue("@PulsingPlanPriorityID", a.PulsingPlanPriorityID);
        cmd.Parameters.AddWithValue("@ProjectOwner", a.ProjectOwner);
//        cmd.Parameters.AddWithValue("@PulsingPlanTiming", a.PulsingPlanTiming);
        cmd.Parameters.AddWithValue("@ProjectStartMonth", a.ProjectStartMonth);
        cmd.Parameters.AddWithValue("@ProjectEndMonth", a.ProjectEndMonth);
        cmd.Parameters.AddWithValue("@ProjectObjective", a.ProjectObjective);
        cmd.Parameters.AddWithValue("@KPIs", a.KPIs);
        cmd.Parameters.AddWithValue("@LastUpdatedBy", a.LastUpdatedBy);
        //output parameter
        SqlParameter param = cmd.CreateParameter();
        param.ParameterName = "@Message";
        param.Direction = ParameterDirection.Output;
        param.DbType = DbType.Int16;
        cmd.Parameters.Add(param);

        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        //return intRowsAffected;
        return Convert.ToInt16(cmd.Parameters["@Message"].Value);
    }

    //public static int UpdateProject_Calendar(Project a)
    ////update an existing Project
    ////Written by Vidya (Akira) December 2014
    //{
    //    SqlCommand cmd = DataAccess.CreateCommand();
    //    cmd.CommandText = "sp_ap_UpdateProject_Cal";

    //    // add parameters with their values
    //    cmd.Parameters.AddWithValue("@ProjectID", a.ProjectID);
    //    cmd.Parameters.AddWithValue("@ProjectName", a.ProjectName);
    //    cmd.Parameters.AddWithValue("@BudgetResponsibilityAreaID", a.BudgetResponsibilityAreaID);
    //    cmd.Parameters.AddWithValue("@ProjectYear", a.ProjectYear);
    //    cmd.Parameters.AddWithValue("@ProjectDescription", a.ProjectDescription);
    //    cmd.Parameters.AddWithValue("@PulsingPlanPriorityID", a.PulsingPlanPriorityID);
    //    cmd.Parameters.AddWithValue("@ProjectOwner", a.ProjectOwner);
    //    cmd.Parameters.AddWithValue("@ProjectStartMonth", a.ProjectStartMonth);
    //    cmd.Parameters.AddWithValue("@ProjectEndMonth", a.ProjectEndMonth);
    //    cmd.Parameters.AddWithValue("@ProjectObjective", a.ProjectObjective);
    //    cmd.Parameters.AddWithValue("@KPIs", a.KPIs);
    //    cmd.Parameters.AddWithValue("@LastUpdatedBy", a.LastUpdatedBy);
    //    //output parameter
    //    SqlParameter param = cmd.CreateParameter();
    //    param.ParameterName = "@Message";
    //    param.Direction = ParameterDirection.Output;
    //    param.DbType = DbType.Int16;
    //    cmd.Parameters.Add(param);

    //    // execute the query to update the database
    //    int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
    //    //return intRowsAffected;
    //    return Convert.ToInt16(cmd.Parameters["@Message"].Value);
    //}

    public static int InsertProject(Project a)
    //create a new Project
    //amended 14/12/14 ProjectStartMonth and ProjectEndMonth added
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_InsertProject";

        // add parameters with their values
        cmd.Parameters.AddWithValue("@ProjectName", a.ProjectName);
        cmd.Parameters.AddWithValue("@BudgetResponsibilityAreaID", a.BudgetResponsibilityAreaID);
        cmd.Parameters.AddWithValue("@ProjectYear", a.ProjectYear);
        cmd.Parameters.AddWithValue("@ProjectDescription", a.ProjectDescription);
        cmd.Parameters.AddWithValue("@PulsingPlanPriorityID", a.PulsingPlanPriorityID);
        cmd.Parameters.AddWithValue("@ProjectOwner", a.ProjectOwner);
//        cmd.Parameters.AddWithValue("@PulsingPlanTiming", a.PulsingPlanTiming);
        cmd.Parameters.AddWithValue("@ProjectStartMonth", a.ProjectStartMonth);
        cmd.Parameters.AddWithValue("@ProjectEndMonth", a.ProjectEndMonth);
        cmd.Parameters.AddWithValue("@ProjectObjective", a.ProjectObjective);
        cmd.Parameters.AddWithValue("@KPIs", a.KPIs);
        cmd.Parameters.AddWithValue("@CreatedBy", a.CreatedBy);
        //output parameter
        SqlParameter param = cmd.CreateParameter();
        param.ParameterName = "@Message";
        param.Direction = ParameterDirection.Output;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);

        // execute the query to update the database
        int intProjectID = DataAccess.ExecuteScalarReturnIdentity(cmd);
        //return intProjectID;
        return Convert.ToInt32(cmd.Parameters["@Message"].Value);
    }

    public static int DeleteProject(int intProjectID)
    //delete an existing Project
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "DeleteProject";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ProjectID", intProjectID);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        return intRowsAffected;
    }

    public static DataTable SelectProjectToCustomer(int intProjectToCustomerID)
    //Get the ProjectID and CustomerID for the selected ProjectToCustomerID
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "SelectProjectToCustomer";
        cmd.Parameters.AddWithValue("@ProjectToCustomerID", intProjectToCustomerID);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static string UpdateProjectToCustomer(int intProjectToCustomerID, string strIONumber, string strInStoreStartDate, string strPulsingPlanPriorityID, int intAllocatedAPBudget, int intAllocatedTCCBudget, int intAllocatedCpnBudget, string strProjectOwner, string strComments, string strLastUpdatedBy)
    //updates an existing ProjectToCustomer record
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_UpdateProjectToCustomer";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ProjectToCustomerID", intProjectToCustomerID);
        cmd.Parameters.AddWithValue("@IONumber", strIONumber);
        cmd.Parameters.AddWithValue("@InStoreStartDate", strInStoreStartDate);
        if (strPulsingPlanPriorityID == "0")
        {
            cmd.Parameters.AddWithValue("@PulsingPlanPriorityID", DBNull.Value);
        }
        else
        {
            cmd.Parameters.AddWithValue("@PulsingPlanPriorityID", strPulsingPlanPriorityID);
        }
        cmd.Parameters.AddWithValue("@AllocatedAPBudget", intAllocatedAPBudget);
        cmd.Parameters.AddWithValue("@AllocatedTCCBudget", intAllocatedTCCBudget);
        cmd.Parameters.AddWithValue("@AllocatedCpnBudget", intAllocatedCpnBudget);
        cmd.Parameters.AddWithValue("@ProjectOwner", strProjectOwner);
        cmd.Parameters.AddWithValue("@Comments", strComments);
        cmd.Parameters.AddWithValue("@LastUpdatedBy", strLastUpdatedBy);
        //output parameter
        SqlParameter param = cmd.CreateParameter();
        param.ParameterName = "@Message";
        param.Direction = ParameterDirection.Output;
        param.DbType = DbType.String;
        param.Size = 255;
        cmd.Parameters.Add(param);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        string strMessage = cmd.Parameters["@Message"].Value.ToString();
        return strMessage;
    }

    public static string UpdateProjectToCustomerWithoutBudgets(int intProjectToCustomerID, string strIONumber, string strInStoreStartDate, string strPulsingPlanPriorityID, string strProjectOwner, string strLastUpdatedBy)
    //updates an existing ProjectToCustomer record, without budgets; For new budgeting process from 2015
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_UpdateProjectToCustomerWithoutBudgets";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ProjectToCustomerID", intProjectToCustomerID);
        cmd.Parameters.AddWithValue("@IONumber", strIONumber);
        cmd.Parameters.AddWithValue("@InStoreStartDate", strInStoreStartDate);
        if (strPulsingPlanPriorityID == "0")
        {
            cmd.Parameters.AddWithValue("@PulsingPlanPriorityID", DBNull.Value);
        }
        else
        {
            cmd.Parameters.AddWithValue("@PulsingPlanPriorityID", strPulsingPlanPriorityID);
        }
        cmd.Parameters.AddWithValue("@ProjectOwner", strProjectOwner);
        cmd.Parameters.AddWithValue("@LastUpdatedBy", strLastUpdatedBy);
        //output parameter
        SqlParameter param = cmd.CreateParameter();
        param.ParameterName = "@Message";
        param.Direction = ParameterDirection.Output;
        param.DbType = DbType.String;
        param.Size = 255;
        cmd.Parameters.Add(param);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        string strMessage = cmd.Parameters["@Message"].Value.ToString();
        return strMessage;
    }

    public static string InsertProjectToCustomer(int intProjectID, int intCustomerID, string strIONumber, string strInStoreStartDate, int intPulsingPlanPriorityID, int intAllocatedAPBudget, int intAllocatedTCCBudget, int intAllocatedCpnBudget, string strProjectOwner, string strComments, string strCreatedBy)
    //creates a new ProjectToCustomer record
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_InsertProjectToCustomer";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ProjectID", intProjectID);
        cmd.Parameters.AddWithValue("@CustomerID", intCustomerID);
        cmd.Parameters.AddWithValue("@IONumber", strIONumber);
        cmd.Parameters.AddWithValue("@InStoreStartDate", strInStoreStartDate);
        cmd.Parameters.AddWithValue("@PulsingPlanPriorityID", intPulsingPlanPriorityID);
        cmd.Parameters.AddWithValue("@AllocatedAPBudget", intAllocatedAPBudget);
        cmd.Parameters.AddWithValue("@AllocatedTCCBudget", intAllocatedTCCBudget);
        cmd.Parameters.AddWithValue("@AllocatedCpnBudget", intAllocatedCpnBudget);
        cmd.Parameters.AddWithValue("@ProjectOwner", strProjectOwner);
        cmd.Parameters.AddWithValue("@Comments", strComments);
        cmd.Parameters.AddWithValue("@CreatedBy", strCreatedBy);
        //output parameter
        SqlParameter param = cmd.CreateParameter();
        param.ParameterName = "@Message";
        param.Direction = ParameterDirection.Output;
        param.DbType = DbType.String;
        param.Size = 255;
        cmd.Parameters.Add(param);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        string strMessage = cmd.Parameters["@Message"].Value.ToString();
        return strMessage;
    }

    public static string MergeProjectToCustomer(string strProjectID, DataTable inputTable, string strUserName)
    //insert Customers for selected project
    //Written by Vidya (Akira) December 2014
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_MergeProjectToCustomer";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ProjectID", strProjectID);
        cmd.Parameters.AddWithValue("@InputTable", inputTable);
        cmd.Parameters.AddWithValue("@UserName", strUserName);
        //output parameter
        SqlParameter param = cmd.CreateParameter();
        param.ParameterName = "@Message";
        param.Direction = ParameterDirection.Output;
        param.DbType = DbType.String;
        param.Size = 255;
        cmd.Parameters.Add(param);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        string strMessage = cmd.Parameters["@Message"].Value.ToString();
        return strMessage;
    }

    public static int DeleteProjectToCustomer(int intProjectToCustomerID)
    //delete an existing ProjectToCustomer record
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "DeleteProjectToCustomer";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ProjectToCustomerID", intProjectToCustomerID);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        return intRowsAffected;
    }

    public static DataTable FillProjectToSubBrand(int intProjectID)
    //Get the SubBrand list for the selected ProjectID
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillProjectToSubBrand";
        cmd.Parameters.AddWithValue("@ProjectID", intProjectID);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static void ClearProjectToSubBrand(int intProjectID)
    //delete all SubBrands for selected project
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "ClearProjectToSubBrand";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ProjectID", intProjectID);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
    }

    public static void InsertProjectToSubBrand(int intProjectID, int intSubBrandID, double dblProportion, string strUserName)
    //insert SubBrands for selected project
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "InsertProjectToSubBrand";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ProjectID", intProjectID);
        cmd.Parameters.AddWithValue("@SubBrandID", intSubBrandID);
        cmd.Parameters.AddWithValue("@Proportion", dblProportion);
        cmd.Parameters.AddWithValue("@UserName", strUserName);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
    }

    public static void MergeProjectToSubBrand(string strProjectID, DataTable inputTable, string strUserName)
    //insert SubBrands for selected project
    //Written by Vidya (Akira) December 2014
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "MergeProjectToSubBrand";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ProjectID", strProjectID);
        cmd.Parameters.AddWithValue("@InputTable", inputTable);
        cmd.Parameters.AddWithValue("@UserName", strUserName);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
    }

    public static DataTable FillCustomersForSelectedProject(int intProjectID, string strUserName)
    //fill list of customers for selected project for which user has permissions
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillCustomersForSelectedProject";
        cmd.Parameters.AddWithValue("@UserName", strUserName);
        cmd.Parameters.AddWithValue("@ProjectID", intProjectID);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static DataTable FindIONumber(string strIONumber, string strUserName)
    //Find ProjectToCustomerID from IO Number, checking permission
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FindIONumber";
        cmd.Parameters.AddWithValue("@IONumber", strIONumber);
        cmd.Parameters.AddWithValue("@UserName", strUserName);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static DataTable FindPONumber(string strPONumber, string strUserName)
    //Find ProjectToCustomerID from PO Number, checking permission
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FindPONumber";
        cmd.Parameters.AddWithValue("@PONumber", strPONumber);
        cmd.Parameters.AddWithValue("@UserName", strUserName);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static DataTable FindRebateNumber(string strRebateNumber, string strUserName)
    //Find ProjectToCustomerID from Rebate Number, checking permission
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FindRebateNumber";
        cmd.Parameters.AddWithValue("@RebateNumber", strRebateNumber);
        cmd.Parameters.AddWithValue("@UserName", strUserName);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static DataTable FillSecondaryIO(string strIONumber)
    //Get the Secondary IO list for the selected Primary IO
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillSecondaryIO";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@IONumber", strIONumber);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static void ClearSecondaryIOs(string strIONumber)
    //delete all Secondary IOs for selected Primary IO
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_ClearSecondaryIOs";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@IONumber", strIONumber);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
    }

    public static void InsertSecondaryIO(string strIONumberPrimary, string strIONumberSecondary, double dblAllocatedBudget, double dblProportion, string strUserName)
    //insert a Secondary IO for selected Primary IO
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_InsertSecondaryIO";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@IONumberPrimary", strIONumberPrimary);
        cmd.Parameters.AddWithValue("@IONumberSecondary", strIONumberSecondary);
        cmd.Parameters.AddWithValue("@AllocatedBudget", dblAllocatedBudget);
        cmd.Parameters.AddWithValue("@Proportion", dblProportion);
        cmd.Parameters.AddWithValue("@UserName", strUserName);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
    }
}
