﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;

/// <summary>
/// Summary description for UserDataAccess
/// </summary>
public class UserDataAccess
{
	public UserDataAccess()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static DataTable SelectUser(string strUserName)
    {
        //get a configured SqlCommand object
        SqlCommand cmd = DataAccess.CreateCommand();
        //set the stored procedure name
        cmd.CommandText = "SelectUser";
        //create OptionName parameter
        cmd.Parameters.AddWithValue("@UserName", strUserName);
        //execute stored procedure and store results in a table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static DataTable SelectActiveUser(string strUserName)
    {
        //get a configured SqlCommand object
        SqlCommand cmd = DataAccess.CreateCommand();
        //set the stored procedure name
        cmd.CommandText = "SelectActiveUser";
        //create OptionName parameter
        cmd.Parameters.AddWithValue("@UserName", strUserName);
        //execute stored procedure and store results in a table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static int UpdateUserSelections(int intCustomerID, int intBudgetResponsibilityAreaID, int intBrandID, int intCategoryID, string strPulsingPlanPriorityMatchType, int intPulsingPlanPriorityID, string strUserName)
    //updates the database with the last selections chosen by the user
    //amended 18/11/12 added LastCategoryID
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "UpdateUserSelections";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@LastCustomerID", intCustomerID);
        cmd.Parameters.AddWithValue("@LastBudgetResponsibilityAreaID", intBudgetResponsibilityAreaID);
        cmd.Parameters.AddWithValue("@LastBrandID", intBrandID);
        cmd.Parameters.AddWithValue("@LastCategoryID", intCategoryID);
        cmd.Parameters.AddWithValue("@LastPulsingPlanPriorityMatchType", strPulsingPlanPriorityMatchType);
        cmd.Parameters.AddWithValue("@LastPulsingPlanPriorityID", intPulsingPlanPriorityID);
        cmd.Parameters.AddWithValue("@UserName", strUserName);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        return intRowsAffected;
    }

    public static int UpdateUserSelections_ReallocationGrid(int intCustomerID, int intBudgetResponsibilityAreaID, int intBrandID, int intCategoryID, int intPulsingPlanPriorityID, string strUserName)
    //updates the database with the last selections chosen by the user
    //amended 18/11/12 added LastCategoryID
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "UpdateUserSelections_ReallocationGrid";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@LastCustomerID", intCustomerID);
        cmd.Parameters.AddWithValue("@LastBudgetResponsibilityAreaID", intBudgetResponsibilityAreaID);
        cmd.Parameters.AddWithValue("@LastBrandID", intBrandID);
        cmd.Parameters.AddWithValue("@LastCategoryID", intCategoryID);
        cmd.Parameters.AddWithValue("@LastPulsingPlanPriorityID", intPulsingPlanPriorityID);
        cmd.Parameters.AddWithValue("@UserName", strUserName);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        return intRowsAffected;
    }

    public static int UpdateBudgetUserSelections(int intBudgetLevelID, int intBudgetResponsibilityAreaID, int intCategoryID, int intBrandID, int intCustomerID, string strUserName)
    //updates the database with the last selections chosen by the user in the budgeting process
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "UpdateBudgetUserSelections";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@BudgetLevelID", intBudgetLevelID);
        cmd.Parameters.AddWithValue("@LastBudgetResponsibilityAreaID", intBudgetResponsibilityAreaID);
        cmd.Parameters.AddWithValue("@LastCategoryID", intCategoryID);
        cmd.Parameters.AddWithValue("@LastBrandID", intBrandID);
        cmd.Parameters.AddWithValue("@LastCustomerID", intCustomerID);
        cmd.Parameters.AddWithValue("@UserName", strUserName);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        return intRowsAffected;
    }

    public static int UpdateCalendarUserSelections(string BudgetResponsibilityAreaID, string CategoryID
        , string BrandID, string Year, string PulsingPlanPriorityID, string UserName)
    //updates the database with the last selections chosen by the user in the calendar page
    {
        if (BudgetResponsibilityAreaID.Contains(","))
        {
            BudgetResponsibilityAreaID = "-1";
        }

        if (CategoryID.Contains(","))
        {
            CategoryID = "-1";
        }

        if (BrandID.Contains(","))
        {
            BrandID = "-1";
        }

        if (PulsingPlanPriorityID.Contains(","))
        {
            PulsingPlanPriorityID = "-1";
        }

        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "UpdateCalendarUserSelections";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@LastBudgetResponsibilityAreaID", int.Parse(BudgetResponsibilityAreaID));
        cmd.Parameters.AddWithValue("@LastCategoryID", int.Parse(CategoryID));
        cmd.Parameters.AddWithValue("@LastBrandID", int.Parse(BrandID));
        cmd.Parameters.AddWithValue("@LastYear", Year);
        cmd.Parameters.AddWithValue("@LastPulsingPlanPriorityID", int.Parse(PulsingPlanPriorityID));
        cmd.Parameters.AddWithValue("@UserName", UserName);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        return intRowsAffected;
    }

    public static DataTable SelectUserAuthorisations(int intUserID)
    //selects an existing Project and puts it in a Project struct object
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "SelectUserAuthorisations";

        // add parameters with their values
        cmd.Parameters.AddWithValue("@UserID", intUserID);

        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static int UpdateUser(int intUserID, int intActiveIndicator, int intUserLevelID, string strDelegateUserName, string strLastUpdatedBy)
    //update an existing User and return an error code
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "UpdateUser";

        // add parameters with their values
        cmd.Parameters.AddWithValue("@UserID", intUserID);
        cmd.Parameters.AddWithValue("@ActiveIndicator", intActiveIndicator);
        cmd.Parameters.AddWithValue("@UserLevelID", intUserLevelID);
        cmd.Parameters.AddWithValue("@DelegateUserName", strDelegateUserName);
        cmd.Parameters.AddWithValue("@LastUpdatedBy", strLastUpdatedBy);
        //output parameter
        SqlParameter param = cmd.CreateParameter();
        param.ParameterName = "@Message";
        param.Direction = ParameterDirection.Output;
        param.DbType = DbType.Int16;
        cmd.Parameters.Add(param);
        // execute the query to update the database
        int intResult = DataAccess.ExecuteUpdateCommand(cmd);
        return Convert.ToInt16(cmd.Parameters["@Message"].Value);
        // execute the query to update the database
    }

    public static int InsertUser(string strUserName, int intActiveIndicator, int intUserLevelID, string strDelegateUserName, string strCreatedBy)
    //create a new User and return the ID
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "InsertUser";

        // add parameters with their values
        cmd.Parameters.AddWithValue("@UserName", strUserName);
        cmd.Parameters.AddWithValue("@ActiveIndicator", intActiveIndicator);
        cmd.Parameters.AddWithValue("@UserLevelID", intUserLevelID);
        cmd.Parameters.AddWithValue("@DelegateUserName", strDelegateUserName);
        cmd.Parameters.AddWithValue("@CreatedBy", strCreatedBy);

        // execute the query to update the database
        int intUserID = DataAccess.ExecuteScalarReturnIdentity(cmd);
        return intUserID;
    }

    public static void InsertUserAuthorisation(int intUserID, int intBudgetResponsibilityAreaID, int intCustomerID, int intBrandID, int intCategoryID, string strCreatedBy)
    //insert authorisation for selected user
    //Amended 07/12/12 CategoryID criterion added
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "InsertUserAuthorisation";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@UserID", intUserID);
        cmd.Parameters.AddWithValue("@BudgetResponsibilityAreaID", intBudgetResponsibilityAreaID);
        cmd.Parameters.AddWithValue("@CustomerID", intCustomerID);
        cmd.Parameters.AddWithValue("@BrandID", intBrandID);
        cmd.Parameters.AddWithValue("@CategoryID", intCategoryID);
        cmd.Parameters.AddWithValue("@CreatedBy", strCreatedBy);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
    }

    public static void ClearUserAuthorisations(int intUserID)
    //delete all authorisations for selected user
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "ClearUserAuthorisations";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@UserID", intUserID);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
    }

    public static int DeleteUser(int intUserID)
    //delete an existing User
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "DeleteUser";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@UserID", intUserID);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        return intRowsAffected;
    }

    public static int SOPUpdateUserSelections(int intCustomerID, string strBUCode, string strUserName)
    //updates the database with the last selections chosen by the user
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_jln_UpdateUserSelections";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@LastCustomerID", intCustomerID);
        cmd.Parameters.AddWithValue("@LastBUCode", strBUCode);
        cmd.Parameters.AddWithValue("@UserName", strUserName);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        return intRowsAffected;
    }

    public static void UpdateUserSelections(DataTable inputTable)
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_UpdateUserSelections";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@InputTable", inputTable);
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
    }

    public static DataTable SelectUserSelections(String strUserName)
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_SelectUserSelections";
        cmd.Parameters.AddWithValue("@UserName", strUserName);
        // execute the query to update the database
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }
}
