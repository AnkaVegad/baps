﻿using System;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Wraps SubBrand data
/// </summary>
public struct SubBrand
{
    public int SubBrandID;
    public string SubBrandName;
    public string CreatedBy;
    public string CreatedDate;
}

/// <summary>
///  Data Access Layer for SubBrand View
/// </summary>
public class SubBrandDataAccess
{
	public SubBrandDataAccess()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static SubBrand SelectSubBrand(int intSubBrandID)
    //selects an existing SubBrand and puts it in a SubBrand struct object
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_SelectSubBrand";

        // add parameters with their values
        cmd.Parameters.AddWithValue("@SubBrandID", intSubBrandID);

        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);

        SubBrand a = new SubBrand();
        if (tbl.Rows.Count > 0)
        {
            DataRow dr = tbl.Rows[0];
            a.SubBrandID = intSubBrandID;
            a.SubBrandName = dr["SubBrandName"].ToString();
            a.CreatedBy = dr["CreatedBy"].ToString();
            a.CreatedDate = dr["CreatedDate"].ToString();
        }

        return a;
    }

    public static int UpdateSubBrand(SubBrand a)
    //update an existing SubBrand
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_UpdateSubBrand";

        // add parameters with their values
        cmd.Parameters.AddWithValue("@SubBrandID", a.SubBrandID);
        cmd.Parameters.AddWithValue("@SubBrandName", a.SubBrandName);

        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        return intRowsAffected;
    }

    public static int InsertSubBrand(SubBrand a)
    //create a new SubBrand
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_InsertSubBrand";

        // add parameters with their values
        cmd.Parameters.AddWithValue("@SubBrandName", a.SubBrandName);
        cmd.Parameters.AddWithValue("@CreatedBy", a.CreatedBy);

        // execute the query to update the database
        int intSubBrandID = DataAccess.ExecuteScalarReturnIdentity(cmd);
        return intSubBrandID;
    }

    public static int DeleteSubBrand(int intSubBrandID)
    //delete an existing SubBrand
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_DeleteSubBrand";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@SubBrandID", intSubBrandID);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        return intRowsAffected;
    }

    public static DataTable FillSubBrandToPPH(int intSubBrandID)
    //Get the PPHL2 list for the selected SubBrandID
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillSubBrandToPPH";
        cmd.Parameters.AddWithValue("@SubBrandID", intSubBrandID);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static void ClearSubBrandToPPH(int intSubBrandID)
    //delete all PPHL2 for selected SubBrand
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_ClearSubBrandToPPH";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@SubBrandID", intSubBrandID);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
    }

    public static void InsertSubBrandToPPH(int intSubBrandID, string strPPHL2, string strUserName)
    //insert PPHL2 for selected SubBrand
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_InsertSubBrandToPPH";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@SubBrandID", intSubBrandID);
        cmd.Parameters.AddWithValue("@PPHL2", strPPHL2);
        //cmd.Parameters.AddWithValue("@UserName", strUserName);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
    }

}
