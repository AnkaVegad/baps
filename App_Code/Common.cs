﻿using System;
using System.Net;
using System.Net.Mail;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Class contains Common code. Data Access Layer.
/// </summary>
public static class Common
{
	static Common()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    //Generic method for sending emails
    public static void SendMail(string iFrom, string iTo, string iSubject, string iBody)
    {
        //configure mail client
        SmtpClient mailClient = new SmtpClient(APConfiguration.MailServer);

        //Set credentials (for SMTP servers requiring authentication)
        //mailClient.Credentials = new NetworkCredential(APConfiguration.MailUsername, APConfiguration.MailPassword);
        MailMessage mailMessage = new MailMessage(iFrom, iTo, iSubject, iBody);
        mailMessage.IsBodyHtml = true;
        //Send mail
        mailClient.Send(mailMessage);
    }

    //Send error log mail
    //c# E-Commerce p92
    public static void LogError(Exception ex)
    {
        //get current date and time 
        string dateTime = DateTime.Now.ToLongDateString() + ", at " + DateTime.Now.ToShortTimeString();
        //stores the error message
        string strError = "Exception generated on " + dateTime;
        //obtain the page that generated the error
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        strError += "\n\n Page Location: " + context.Request.RawUrl;
        //build the error message
        strError += "\n\n Message: " + ex.Message;
        strError += "\n\n Source: " + ex.Source;
        strError += "\n\n Method: " + ex.TargetSite;
        strError += "\n\n Stack Trace: \n\n" + ex.StackTrace;
        //send error email if option is activated in web.config
        if (APConfiguration.EnableErrorLogEmail)
        {
            string iFrom = APConfiguration.MailFrom;
            string iTo = APConfiguration.ErrorLogEmail;
            string iSubject = "AP Error Report";
            string iBody = strError;
            SendMail(iFrom, iTo, iSubject, iBody);
        }
    }

    public static string GetOptionValue(string iOptionName)
    {
        //get a configured SqlCommand object
        SqlCommand cmd = DataAccess.CreateCommand();
        //set the stored procedure name
        cmd.CommandText = "GetOptionValue";
        //create OptionName parameter
        cmd.Parameters.AddWithValue("@OptionName", iOptionName);
        //execute stored procedure and store results in a string
        string strOptionValue = DataAccess.ExecuteScalar(cmd).ToString();
        return strOptionValue;
    }

    public static string Right(string strInput, int intLength)
    {
        try         
        {
            return strInput.Substring(strInput.Length - intLength);
        }
        catch //in case input string is shorter than the requested substring
        {
            return strInput;
        }
    }

    public static string Left(string strInput, int intLength)
    {
        try         
        {
            return strInput.Substring(0, intLength);
        }
        catch //in case input string is shorter than the requested substring
        {
            return strInput;
        } 
    }

    public static string ReverseDate(string strInput)
    //converts a dd/MM/yyyy string into yyyy-MM-dd
    {
        try         
        {
            return strInput.Substring(6, 4) + "-" + strInput.Substring(3, 2) + "-" + strInput.Substring(0, 2);
        }
        catch //in case input string is shorter than the requested substring
        {
            return strInput;
        }
    }
    
    public static DataTable FillDropDown(string strSPName)
    {
        //Get a data table object containing the values
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = strSPName;
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static string ADOLookup(string strField, string strTable, string strCrit)
    //lookup values in the database
    //note: ADOLookup also in DataAccess.cs
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        string strSQL = "SELECT " + strField + " FROM " + strTable + " WHERE " + strCrit;
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = strSQL;
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl.Rows[0][0].ToString();
    }

    public static string ADOLookupSafe(string strField, string strTable, string strCrit)
    //lookup values in the database and return empty string if not found
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        string strSQL = "SELECT " + strField + " FROM " + strTable + " WHERE " + strCrit;
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = strSQL;
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        if (tbl.Rows.Count > 0)
        {
            return tbl.Rows[0][0].ToString();
        }
        else
        {
            return String.Empty;
        }
    }

    public static int CheckAuthorisations(int intProjectToCustomerID, string strUserName)
    //return whether user has permissions for the item
    //"1" = Yes
    //"0" = No
    {
        //get a configured SqlCommand object
        SqlCommand cmd = DataAccess.CreateCommand();
        //set the stored procedure name
        cmd.CommandText = "sp_ap_CheckAuthorisations";
        //create OptionName parameter
        cmd.Parameters.AddWithValue("@ProjectToCustomerID", intProjectToCustomerID);
        cmd.Parameters.AddWithValue("@UserName", strUserName);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return Convert.ToInt32(tbl.Rows[0][0]);
    }

    public static object ReturnNullableValue(string strTextBoxValue, string strType)
    {
        object objNullableValue = DBNull.Value;
        if (strTextBoxValue == String.Empty)
        {
            objNullableValue = DBNull.Value;
        }
        else
        {
            switch (strType)
            {
                case "Int32":
                    objNullableValue = Convert.ToInt32(strTextBoxValue);
                    break;
                case "Double":
                    objNullableValue = Convert.ToDouble(strTextBoxValue);
                    break;
            }
        }
        return objNullableValue;
    }

    public static double DblNullToZero(object objInput)
    {
        double dblNullToZero;
        try
        {
            dblNullToZero = Convert.ToDouble(objInput);
        }
        catch
        {
            dblNullToZero = 0;
        }
        return dblNullToZero;
    }

    public static int IntNullToZero(object objInput)
    {
        int intNullToZero;
        try
        {
            intNullToZero = Convert.ToInt32(objInput);
        }
        catch
        {
            intNullToZero = 0;
        }
        return intNullToZero;
    }

   /* public static bool ValidatePromo(JolinPromo p, int intEvaluationStatusID, int intFundingTypeID)
    {
        bool blnValidatePromo = false;
        switch (intEvaluationStatusID)
        {
            case 1: //Pre Evaluated
                if (intFundingTypeID == 1)
                {
                    blnValidatePromo = (p.OPSOBaseLineQty != DBNull.Value && p.PlannedSellOutQty != DBNull.Value && p.PlannedNIV != DBNull.Value && p.PlannedLTA != DBNull.Value && p.PlannedMOP != DBNull.Value && p.PlannedCOGS != DBNull.Value && p.PlannedPriceDealOnInv != DBNull.Value && p.PlannedGateFee != DBNull.Value);
                }
                if (intFundingTypeID == 2)
                {
                    blnValidatePromo = (p.OPSOBaseLineQty != DBNull.Value && p.PlannedSellOutQty != DBNull.Value && p.PlannedNIV != DBNull.Value && p.PlannedLTA != DBNull.Value && p.PlannedMOP != DBNull.Value && p.PlannedCOGS != DBNull.Value && p.PlannedAccrualOffInv != DBNull.Value && p.PlannedRedemptionRate != DBNull.Value && p.PlannedGateFee != DBNull.Value);
                }
                try //in case PlannedGateFee is null
                {
                    blnValidatePromo = (blnValidatePromo && Convert.ToDouble(p.PlannedGateFee) >= 0);
                }
                catch { }
                break;
            case 2: //Post Evaluated
                if (intFundingTypeID == 1)
                {
                    blnValidatePromo = (p.OPSOBaseLineQty != DBNull.Value && p.PlannedSellOutQty != DBNull.Value && p.PlannedNIV != DBNull.Value && p.PlannedLTA != DBNull.Value && p.PlannedMOP != DBNull.Value && p.PlannedCOGS != DBNull.Value && p.PlannedPriceDealOnInv != DBNull.Value && p.PlannedGateFee != DBNull.Value && p.ActualSellOutQty != DBNull.Value && p.ActualNIV != DBNull.Value && p.ActualLTA != DBNull.Value && p.ActualMOP != DBNull.Value && p.ActualCOGS != DBNull.Value && p.ActualPriceDealOnInv != DBNull.Value && p.ActualGateFee != DBNull.Value);
                }
                if (intFundingTypeID == 2)
                {
                    blnValidatePromo = (p.OPSOBaseLineQty != DBNull.Value && p.PlannedSellOutQty != DBNull.Value && p.PlannedNIV != DBNull.Value && p.PlannedLTA != DBNull.Value && p.PlannedMOP != DBNull.Value && p.PlannedCOGS != DBNull.Value && p.PlannedAccrualOffInv != DBNull.Value && p.PlannedRedemptionRate != DBNull.Value && p.PlannedGateFee != DBNull.Value && p.ActualSellOutQty != DBNull.Value && p.ActualNIV != DBNull.Value && p.ActualLTA != DBNull.Value && p.ActualMOP != DBNull.Value && p.ActualCOGS != DBNull.Value && p.ActualAccrualOffInv != DBNull.Value && p.ActualRedemptionRate != DBNull.Value && p.ActualGateFee != DBNull.Value);
                }
                try //in case PlannedGateFee is null
                {
                    blnValidatePromo = (blnValidatePromo && Convert.ToDouble(p.PlannedGateFee) >= 0);
                }
                catch { }
                break;
            case 3: //Non Evaluated
                blnValidatePromo = true;
                break;
            case 4: //Excluded
                //***same as post evaluated - check***
                if (intFundingTypeID == 1)
                {
                    blnValidatePromo = (p.OPSOBaseLineQty != DBNull.Value && p.PlannedSellOutQty != DBNull.Value && p.PlannedNIV != DBNull.Value && p.PlannedLTA != DBNull.Value && p.PlannedMOP != DBNull.Value && p.PlannedCOGS != DBNull.Value && p.PlannedPriceDealOnInv != DBNull.Value && p.PlannedGateFee != DBNull.Value && p.ActualSellOutQty != DBNull.Value && p.ActualNIV != DBNull.Value && p.ActualLTA != DBNull.Value && p.ActualMOP != DBNull.Value && p.ActualCOGS != DBNull.Value && p.ActualPriceDealOnInv != DBNull.Value && p.ActualGateFee != DBNull.Value);
                }
                if (intFundingTypeID == 2)
                {
                    blnValidatePromo = (p.OPSOBaseLineQty != DBNull.Value && p.PlannedSellOutQty != DBNull.Value && p.PlannedNIV != DBNull.Value && p.PlannedLTA != DBNull.Value && p.PlannedMOP != DBNull.Value && p.PlannedCOGS != DBNull.Value && p.PlannedAccrualOffInv != DBNull.Value && p.PlannedRedemptionRate != DBNull.Value && p.PlannedGateFee != DBNull.Value && p.ActualSellOutQty != DBNull.Value && p.ActualNIV != DBNull.Value && p.ActualLTA != DBNull.Value && p.ActualMOP != DBNull.Value && p.ActualCOGS != DBNull.Value && p.ActualAccrualOffInv != DBNull.Value && p.ActualRedemptionRate != DBNull.Value && p.ActualGateFee != DBNull.Value);
                }
                try //in case PlannedGateFee is null
                {
                    blnValidatePromo = (blnValidatePromo && Convert.ToDouble(p.PlannedGateFee) >= 0);
                }
                catch { }
                break;
        }
        return blnValidatePromo;
    }*/

}
