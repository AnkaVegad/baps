﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// 
/// CalendarDisplayLogic takes input a table [BrandList, ProjectStartMonth, ProjectEndMonth, ProjectID, ProjectName, PulsingPlanPriorityName(for colour coding)]
/// This information is pivoted to output months (Jan-Dec) and display BrandList, ProjectName, ProjectID, ColourCoding
/// The number of rows is minimized based on BrandList and ProjectTiming
/// If a given BrandList has two projects that do not have overlapping ProjectTiming then display them in the same row, else display in separate rows
/// The objective is to minimize the number of rows being displayed
/// 
/// </summary>
public class CalendarDisplayLogic
{
    private DataTable CalendarInput;

    private int[] IndexArray;

    private class ProjectEntity
    {
        int ProjectID;
        int StartMonth;
        int EndMonth;
        string ProjectName;
        string ColourCoding;

        public ProjectEntity()
        {
            ProjectID = 0;
            StartMonth = 0;
            EndMonth = 0;
        }

        public void SetProjectID(int id)
        {
            ProjectID = id;
        }

        public void SetStartMonth(int start)
        {
            StartMonth = start;
        }

        public void SetEndMonth(int end)
        {
            EndMonth = end;
        }

        public void SetProjectName(string input)
        {
            ProjectName = input;
        }

        public void SetColourCoding(string input)
        {
            ColourCoding = input;
        }

        public int GetProjectID()
        {
            return ProjectID;
        }

        public int GetStartMonth()
        {
            return StartMonth;
        }

        public int GetEndMonth()
        {
            return EndMonth;
        }

        public string GetProjectName()
        {
            return ProjectName;
        }

        public string GetColourCoding()
        {
            return ColourCoding;
        }
    };

    private class BrandListEntity
    {
        string BrandList;
        List<ProjectEntity> ProjectList;
        int MinStart;
        int MaxEnd;

        public BrandListEntity()
        {
            ProjectList = new List<ProjectEntity>();
            BrandList = "";
            MinStart = 0;
            MaxEnd = 0;
        }

        public List<ProjectEntity> GetProjectList()
        {
            return ProjectList;
        }

        public string GetBrandList()
        {
            return BrandList;
        }

        public int GetMinStart()
        {
            return MinStart;
        }
        
        public int GetMaxEnd()
        {
            return MaxEnd;
        }

        public void AddProjectEntityToList(ProjectEntity objProjectEntity)
        {
            ProjectList.Add(objProjectEntity);
        }

        public void SetMinStart(int start)
        {
            MinStart = start;
        }

        public void SetMaxEnd(int end)
        {
            MaxEnd = end;
        }

        public void SetBrandList(string InputBrandList)
        {
            BrandList = InputBrandList;
        }
    };

    private List<BrandListEntity> BrandListList;

    public CalendarDisplayLogic()
	{
        CalendarInput = new DataTable();
        BrandListList = new List<BrandListEntity>();
        IndexArray = new int[26];
        for (int i = 0; i < 26; i++)
        {
            IndexArray[i] = int.MaxValue;
        }
	}

    public void SetCalendarInput(DataTable input)
    {
        CalendarInput = input;
    }

    private void ArrangeProjects()
    {
        foreach (DataRow row in CalendarInput.Rows)
        {    
            int IndexPosition;
            int StartLocation;
            int EndLocation;

            if (row.ItemArray[0].ToString().Trim().Length > 0)
            {
                //Find which index position to retrieve from
                IndexPosition = Convert.ToInt32(row.ItemArray[0].ToString().Trim().ToLower().First()) - Convert.ToInt32('a');

                //Retrieve from index
                if (IndexArray[IndexPosition] == int.MaxValue)
                {
                    StartLocation = 0;
                    EndLocation = BrandListList.Count;
                    IndexArray[IndexPosition] = EndLocation;
                }
                else if (IndexArray[IndexPosition + 1] == int.MaxValue)
                {
                    StartLocation = IndexArray[IndexPosition];
                    EndLocation = BrandListList.Count;
                }
                else
                {
                    StartLocation = IndexArray[IndexPosition];
                    EndLocation = IndexArray[IndexPosition + 1];
                }

                //Initialize ProjectListEntity object with known values
                int addedToList = 0;
                ProjectEntity objProjectEntity = new ProjectEntity();

                int StartMonth = int.Parse(row.ItemArray[1].ToString());
                int EndMonth = int.Parse(row.ItemArray[2].ToString());

                objProjectEntity.SetProjectID(int.Parse(row.ItemArray[3].ToString()));
                objProjectEntity.SetEndMonth(EndMonth);
                objProjectEntity.SetStartMonth(StartMonth);
                objProjectEntity.SetProjectName(row.ItemArray[4].ToString());
                objProjectEntity.SetColourCoding(row.ItemArray[5].ToString());

                //Iterate between start and end locations retieved from index
                for (int i = StartLocation; i < EndLocation; i++)
                {
                    //If brandlist already exists, add to project list
                    if (int.Parse(row.ItemArray[1].ToString()) > BrandListList.ElementAt(i).GetMaxEnd()
                        && row.ItemArray[0].ToString() == BrandListList.ElementAt(i).GetBrandList())
                    {
                        BrandListList.ElementAt(i).AddProjectEntityToList(objProjectEntity);
                        BrandListList.ElementAt(i).SetMaxEnd(EndMonth);
                        addedToList = 1;
                        //If added to brandlist break from for loop
                        i = EndLocation;
                    }
                }

                //If brandlist does not exist, add new brandlist at end of the list and add project to project list
                if (addedToList == 0)
                {
                    BrandListEntity objBrandListEntity = new BrandListEntity();
                    objBrandListEntity.SetBrandList(row.ItemArray[0].ToString());
                    objBrandListEntity.SetMinStart(StartMonth);
                    objBrandListEntity.SetMaxEnd(EndMonth);
                    objBrandListEntity.AddProjectEntityToList(objProjectEntity);
                    BrandListList.Add(objBrandListEntity);
                    addedToList = 1;
                }
            }
        }
        return;
    }

    public DataTable ReturnResult()
    {
        ArrangeProjects();

        //Create a datatable to return result
        DataTable ResultTable = new DataTable();
        ResultTable.Columns.Add("Brand", typeof(string));
        ResultTable.Columns.Add("JanID", typeof(string));
        ResultTable.Columns.Add("FebID", typeof(string));
        ResultTable.Columns.Add("MarID", typeof(string));
        ResultTable.Columns.Add("AprID", typeof(string));
        ResultTable.Columns.Add("MayID", typeof(string));
        ResultTable.Columns.Add("JunID", typeof(string));
        ResultTable.Columns.Add("JulID", typeof(string));
        ResultTable.Columns.Add("AugID", typeof(string));
        ResultTable.Columns.Add("SepID", typeof(string));
        ResultTable.Columns.Add("OctID", typeof(string));
        ResultTable.Columns.Add("NovID", typeof(string));
        ResultTable.Columns.Add("DecID", typeof(string));
        ResultTable.Columns.Add("Jan", typeof(string));
        ResultTable.Columns.Add("Feb", typeof(string));
        ResultTable.Columns.Add("Mar", typeof(string));
        ResultTable.Columns.Add("Apr", typeof(string));
        ResultTable.Columns.Add("May", typeof(string));
        ResultTable.Columns.Add("Jun", typeof(string));
        ResultTable.Columns.Add("Jul", typeof(string));
        ResultTable.Columns.Add("Aug", typeof(string));
        ResultTable.Columns.Add("Sep", typeof(string));
        ResultTable.Columns.Add("Oct", typeof(string));
        ResultTable.Columns.Add("Nov", typeof(string));
        ResultTable.Columns.Add("Dec", typeof(string));
        ResultTable.Columns.Add("JanColour", typeof(string));
        ResultTable.Columns.Add("FebColour", typeof(string));
        ResultTable.Columns.Add("MarColour", typeof(string));
        ResultTable.Columns.Add("AprColour", typeof(string));
        ResultTable.Columns.Add("MayColour", typeof(string));
        ResultTable.Columns.Add("JunColour", typeof(string));
        ResultTable.Columns.Add("JulColour", typeof(string));
        ResultTable.Columns.Add("AugColour", typeof(string));
        ResultTable.Columns.Add("SepColour", typeof(string));
        ResultTable.Columns.Add("OctColour", typeof(string));
        ResultTable.Columns.Add("NovColour", typeof(string));
        ResultTable.Columns.Add("DecColour", typeof(string));

        //Fill result table
        foreach (BrandListEntity objBrandListEntity in BrandListList)
        {
            //Create a string array to store intermediate result of each row
            string[] result;
            result = new string[37];

            //set brand list at column 0
            result[0] = objBrandListEntity.GetBrandList();

            //lastEntry variable acts as iterator
            int lastEntry = 1;

            //Iterate through projectlist
            foreach (ProjectEntity objProjectEntity in objBrandListEntity.GetProjectList())
            {
                //Set values from lastEntry to start month as -1
                //This is the gap between projects in the same row
                for (; lastEntry < objProjectEntity.GetStartMonth(); lastEntry++)
                {
                    result[lastEntry] = "-1";//ID
                    result[lastEntry + 12] = "";//Name
                    result[lastEntry + 24] = "";//Colour
                }

                //Iterate from projectstart to projectend
                for (; lastEntry <= objProjectEntity.GetEndMonth(); lastEntry++)
                {
                    result[lastEntry] = objProjectEntity.GetProjectID().ToString();
                    result[lastEntry + 12] = objProjectEntity.GetProjectName();
                    result[lastEntry + 24] = objProjectEntity.GetColourCoding();
                }
            }

            //iterate from last projectend to decemmber and set as -1
            for (; lastEntry <= 12 ; lastEntry++)
            {
                result[lastEntry] = "-1";//ID
                result[lastEntry + 12] = "";//Name
                result[lastEntry + 24] = "";//Colour
            }

            //Add the string result as a row to datatable ResultTable for returning
            ResultTable.Rows.Add(result);
        }
        return ResultTable;
    }
}