﻿using System;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Wraps ActivityType data
/// </summary>
public struct ActivityType
{
    public int ActivityTypeID;
    public string ActivityTypeName;
    public string GLCode;
    public string NeedsFinanceApprovalFlag;
    public string CreatedBy;
    public string CreatedDate;
}

/// <summary>
///  Data Access Layer for ActivityType View
/// </summary>
public class ActivityTypeDataAccess
{
	public ActivityTypeDataAccess()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static ActivityType SelectActivityType(int intActivityTypeID)
    //selects an existing ActivityType and puts it in a ActivityType struct object
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "SelectActivityType";

        // add parameters with their values
        cmd.Parameters.AddWithValue("@ActivityTypeID", intActivityTypeID);

        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);

        ActivityType a = new ActivityType();
        if (tbl.Rows.Count > 0)
        {
            DataRow dr = tbl.Rows[0];
            a.ActivityTypeID = intActivityTypeID;
            a.ActivityTypeName = dr["ActivityTypeName"].ToString();
            a.GLCode = dr["GLCode"].ToString();
            a.CreatedBy = dr["CreatedBy"].ToString();
            a.CreatedDate = dr["CreatedDate"].ToString();
            a.NeedsFinanceApprovalFlag = dr["NeedsFinanceApprovalFlag"].ToString();
        }

        return a;
    }

    public static int UpdateActivityType(ActivityType a)
    //update an existing ActivityType
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_UpdateActivityType";

        // add parameters with their values
        cmd.Parameters.AddWithValue("@ActivityTypeID", a.ActivityTypeID);
        cmd.Parameters.AddWithValue("@ActivityTypeName", a.ActivityTypeName);
        cmd.Parameters.AddWithValue("@GLCode", a.GLCode);
        cmd.Parameters.AddWithValue("@NeedsFinanceApprovalFlag",a.NeedsFinanceApprovalFlag);

        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        return intRowsAffected;
    }

    public static int InsertActivityType(ActivityType a)
    //create a new ActivityType
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_InsertActivityType";

        // add parameters with their values
        cmd.Parameters.AddWithValue("@ActivityTypeName", a.ActivityTypeName);
        cmd.Parameters.AddWithValue("@GLCode", a.GLCode);
        cmd.Parameters.AddWithValue("@CreatedBy", a.CreatedBy);
        cmd.Parameters.AddWithValue("@NeedsFinanceApprovalFlag", a.NeedsFinanceApprovalFlag);
       

        // execute the query to update the database
        int intActivityTypeID = DataAccess.ExecuteScalarReturnIdentity(cmd);
        return intActivityTypeID;
    }

    public static int DeleteActivityType(int intActivityTypeID)
    //delete an existing ActivityType
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "DeleteActivityType";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ActivityTypeID", intActivityTypeID);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        return intRowsAffected;
    }

    public static DataTable FillActivityTypeToSpendType(int intActivityTypeID)
    //Get the SpendType/BudgetResponsibilityArea list for the selected ActivityTypeID
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillActivityTypeToSpendType";
        cmd.Parameters.AddWithValue("@ActivityTypeID", intActivityTypeID);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static void ClearActivityTypeToSpendType(int intActivityTypeID)
    //delete all SpendType/BudgetResponsibilityArea for selected ActivityType
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "ClearActivityTypeToSpendType";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ActivityTypeID", intActivityTypeID);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
    }

    public static void InsertActivityTypeToSpendType(int intActivityTypeID, int intBudgetResponsibilityAreaID, int intSpendTypeID, int intActivityTypeGroupID, int intActiveIndicator, string strUserName)
    //insert SpendType/BudgetResponsibilityArea for selected ActivityType
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "InsertActivityTypeToSpendType";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ActivityTypeID", intActivityTypeID);
        cmd.Parameters.AddWithValue("@BudgetResponsibilityAreaID", intBudgetResponsibilityAreaID);
        cmd.Parameters.AddWithValue("@SpendTypeID", intSpendTypeID);
        cmd.Parameters.AddWithValue("@ActivityTypeGroupID", intActivityTypeGroupID);
        cmd.Parameters.AddWithValue("@ActiveIndicator", intActiveIndicator);
        cmd.Parameters.AddWithValue("@UserName", strUserName);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
    }

}
