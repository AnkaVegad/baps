﻿using System;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Contains Data Access functionality to be accessed from the business tier
/// </summary>
public static class DataAccess
{
	//static constructor
    static DataAccess()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    //execute apro commmand and return the identity  
    public static int ExecuteScalarReturnIdentity(SqlCommand cmd)
    {
        //The value to be returned
        int intIdentity;
        //Execute the command then close connection
        try
        {
            //Open data connection
            cmd.Connection.Open();
            //Execute the command and save results in a data table
            intIdentity = Convert.ToInt32(cmd.ExecuteScalar());
        }
        catch (Exception ex)
        {
            Common.LogError(ex);
            throw;
        }
        finally
        {
            //close the connection
            cmd.Connection.Close();
        }
        return intIdentity;
    }

    public static object ExecuteScalar(SqlCommand cmd)
    {
        //The value to be returned
        object objRetVal;
        //Execute the command then close connection
        try
        {
            //Open data connection
            cmd.Connection.Open();
            //Execute the command and save results in a data table
            objRetVal = cmd.ExecuteScalar();
        }
        catch (Exception ex)
        {
            Common.LogError(ex);
            throw;
        }
        finally
        {
            //close the connection
            cmd.Connection.Close();
        }
        return objRetVal;
    }

    //execute a commmand and return results as a DataTable object via a DataReader  
    public static DataTable ExecuteSelectCommand(SqlCommand cmd)
    {
        //The data table to be returned
        DataTable tbl;
        //Execute the command then close connection
        try
        {
            //Open data connection
            cmd.Connection.Open();
            //Execute the command and save results in a data table
            SqlDataReader rdr = cmd.ExecuteReader();
            tbl = new DataTable();
            tbl.Load(rdr);

            //Close the reader
            rdr.Close();
        }
        catch (Exception ex)
        {
            Common.LogError(ex);
            throw;
        }
        finally
        {
            //close the connection
            cmd.Connection.Close();
        }
        return tbl;
    }

    public static int ExecuteUpdateCommand(SqlCommand cmd)
    //generic update command routine when return value to be default (no of rows affected)
    {
        //The no of rows updated
        int NoOfAffectedRows;
        //Execute the command then close connection
        try
        {
            //Open data connection
            cmd.Connection.Open();
            //Execute the command and return rows affected
            NoOfAffectedRows = cmd.ExecuteNonQuery();

        }
        catch (Exception ex)
        {
            Common.LogError(ex);
            throw;
        }
        finally
        {
            //close the connection
            cmd.Connection.Close();
        }
        return NoOfAffectedRows;
    }


    //creates and prepares a new Command object on a new Connection
    public static SqlCommand CreateCommand()
    {
        //obtain the connection string
        string strConString = APConfiguration.DbConnectionString;
        //set connection string
        SqlConnection con = new SqlConnection(strConString);
        //create database-specific command object
        SqlCommand cmd = con.CreateCommand();
        //set command type to stored procedure
        cmd.CommandType = CommandType.StoredProcedure;
        //return the initialised command object
        return cmd;
    }

    public static string ADOLookup(string strField, string strTable, string strCrit)
    //lookup values in the database
    //note: ADOLookup also in Common.cs
    {
        String strConn = APConfiguration.DbConnectionString;
        SqlConnection sqlConn = new SqlConnection(strConn);

        string strSQL = "SELECT " + strField + " FROM " + strTable + " WHERE " + strCrit;
        SqlCommand sqlComm = new SqlCommand(strSQL, sqlConn);
        sqlComm.CommandType = CommandType.Text;

        SqlDataReader dr = null;
        try
        {
            // open the connection
            sqlConn.Open();

            dr = sqlComm.ExecuteReader();
            dr.Read();
            string str = dr[0].ToString();
            return str;
        }
        finally
        {
            if (dr != null)
            {
                dr.Close();
            }
            //close connection (no matter what)
            sqlConn.Close();
        }
    }

}
