﻿using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for AuditDataAccess
/// </summary>
public class AuditDataAccess
{
	public AuditDataAccess()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static DataTable SelectActivityHistory(int intProjectToCustomerID)
    //selects Activity History records in the database and puts them in a datatable
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "SelectActivityHistory";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ProjectToCustomerID", intProjectToCustomerID);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static DataTable SelectProjectToCustomerHistory(int intProjectToCustomerID)
    //selects Budget History records in the database and puts them in a datatable
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "SelectProjectToCustomerHistory";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ProjectToCustomerID", intProjectToCustomerID);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }
}
