﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;

/// <summary>
/// Wraps ProductHierarchy data
/// </summary>
public struct ProductHierarchy
{
    public int ProductHierarchyID;
    public int BusinessGroupID;
    public int BusinessUnitID;
    public int EuropeanCategoryID;
    public int UKCategoryID;
    public int BrandID;
    public int SubBrandID;
    public string ActiveIndicator;
    //public string LastUpdatedBy;
    //public string LastUpdatedDate;
    public string CreatedBy;
    public string CreatedDate;
}

/// <summary>
/// Summary description for ProductHierarchyDataAccess
/// </summary>
public class ProductHierarchyDataAccess
{
	public ProductHierarchyDataAccess()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static ProductHierarchy SelectProductHierarchy(int intProductHierarchyID)
    //select an existing ProductHierarchy from its ID and put it in a ProductHierarchy struct object
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_SelectProductHierarchy";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ProductHierarchyID", intProductHierarchyID);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        ProductHierarchy h = new ProductHierarchy();
        if (tbl.Rows.Count > 0)
        {
            DataRow dr = tbl.Rows[0];
            h.ProductHierarchyID = intProductHierarchyID;
            h.BusinessGroupID = Convert.ToInt32(dr["BusinessGroupID"]);
            h.BusinessUnitID = Convert.ToInt32(dr["BusinessUnitID"]);
            h.EuropeanCategoryID = Convert.ToInt32(dr["EuropeanCategoryID"]);
            h.UKCategoryID = Convert.ToInt32(dr["UKCategoryID"]);
            h.BrandID = Convert.ToInt32(dr["BrandID"]);
            h.SubBrandID = Convert.ToInt32(dr["SubBrandID"]);
            h.ActiveIndicator = dr["ActiveIndicator"].ToString();
            h.CreatedBy = dr["CreatedBy"].ToString();
            h.CreatedDate = dr["CreatedDate"].ToString();
        }
        return h;
    }

    public static int UpdateProductHierarchy(ProductHierarchy h)
    //update an existing ProductHierarchy record
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_UpdateProductHierarchy";

        // add parameters with their values
        cmd.Parameters.AddWithValue("@ProductHierarchyID", h.ProductHierarchyID);
        cmd.Parameters.AddWithValue("@BusinessGroupID", h.BusinessGroupID);
        cmd.Parameters.AddWithValue("@BusinessUnitID", h.BusinessUnitID);
        cmd.Parameters.AddWithValue("@EuropeanCategoryID", h.EuropeanCategoryID);
        cmd.Parameters.AddWithValue("@UKCategoryID", h.UKCategoryID);
        cmd.Parameters.AddWithValue("@BrandID", h.BrandID);
        cmd.Parameters.AddWithValue("@SubBrandID", h.SubBrandID);
        cmd.Parameters.AddWithValue("@ActiveIndicator", h.ActiveIndicator);

        // execute the query to update the database
        int intResult = DataAccess.ExecuteUpdateCommand(cmd);
        return intResult;
    }

    public static int InsertProductHierarchy(ProductHierarchy h)
    //create a new ProductHierarchy line and return the ID
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_InsertProductHierarchy";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@BusinessGroupID", h.BusinessGroupID);
        cmd.Parameters.AddWithValue("@BusinessUnitID", h.BusinessUnitID);
        cmd.Parameters.AddWithValue("@EuropeanCategoryID", h.EuropeanCategoryID);
        cmd.Parameters.AddWithValue("@UKCategoryID", h.UKCategoryID);
        cmd.Parameters.AddWithValue("@BrandID", h.BrandID);
        cmd.Parameters.AddWithValue("@SubBrandID", h.SubBrandID);
        cmd.Parameters.AddWithValue("@ActiveIndicator", h.ActiveIndicator);
        cmd.Parameters.AddWithValue("@CreatedBy", h.CreatedBy);
        // execute the query to update the database
        int intProductHierarchyID = DataAccess.ExecuteScalarReturnIdentity(cmd);
        return intProductHierarchyID;
    }

    public static int DeleteProductHierarchy(int intProductHierarchyID)
    //delete an existing ProductHierarchy line
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_DeleteProductHierarchy";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ProductHierarchyID", intProductHierarchyID);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        return intRowsAffected;
    }

}
