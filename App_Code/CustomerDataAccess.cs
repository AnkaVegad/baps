﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;

/// <summary>
/// Wraps Customer data
/// </summary>
public struct Customer
{
    public int CustomerID;
    public string CustomerName;
    public string SAPCustomerCode;
    public int ActiveIndicator;
    public string LastUpdatedBy;
    public string LastUpdatedDate;
    public string CreatedBy;
    public string CreatedDate;
}

/// <summary>
/// Summary description for CustomerDataAccess
/// </summary>
public class CustomerDataAccess
{
	public CustomerDataAccess()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static Customer SelectCustomer(int intCustomerID)
    //selects an existing Customer and puts it in a Customer struct object
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "SelectCustomer";

        // add parameters with their values
        cmd.Parameters.AddWithValue("@CustomerID", intCustomerID);

        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);

        Customer c = new Customer();
        if (tbl.Rows.Count > 0)
        {
            DataRow dr = tbl.Rows[0];
            c.CustomerID = intCustomerID;
            c.CustomerName = dr["CustomerName"].ToString();
            c.SAPCustomerCode = dr["SAPCustomerCode"].ToString();
            c.ActiveIndicator = Convert.ToInt16(dr["ActiveIndicator"]);
            c.LastUpdatedBy = dr["LastUpdatedBy"].ToString();
            c.LastUpdatedDate = dr["LastUpdatedDate"].ToString();
            c.CreatedBy = dr["CreatedBy"].ToString();
            c.CreatedDate = dr["CreatedDate"].ToString();
        }

        return c;
    }

    public static int UpdateCustomer(Customer c)
    //update an existing Customer
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "UpdateCustomer";

        // add parameters with their values
        cmd.Parameters.AddWithValue("@CustomerID", c.CustomerID);
        cmd.Parameters.AddWithValue("@CustomerName", c.CustomerName);
        cmd.Parameters.AddWithValue("@SAPCustomerCode", c.SAPCustomerCode);
        cmd.Parameters.AddWithValue("@ActiveIndicator", c.ActiveIndicator);
        cmd.Parameters.AddWithValue("@LastUpdatedBy", c.LastUpdatedBy);

        // execute the query to update the database
        int intResult = DataAccess.ExecuteUpdateCommand(cmd);
        return intResult;
    }

    public static int InsertCustomer(Customer c)
    //create a new Customer and return the ID
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "InsertCustomer";

        // add parameters with their values
        cmd.Parameters.AddWithValue("@CustomerName", c.CustomerName);
        cmd.Parameters.AddWithValue("@SAPCustomerCode", c.SAPCustomerCode);
        cmd.Parameters.AddWithValue("@ActiveIndicator", c.ActiveIndicator);
        cmd.Parameters.AddWithValue("@CreatedBy", c.CreatedBy);

        // execute the query to update the database
        int intCustomerID = DataAccess.ExecuteScalarReturnIdentity(cmd);
        return intCustomerID;
    }

    public static int DeleteCustomer(int intCustomerID)
    //delete an existing Customer
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "DeleteCustomer";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@CustomerID", intCustomerID);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        return intRowsAffected;
    }

    public static DataTable FillCustomerHierarchy(int intCustomerID)
    //Get the Customer Hierarchy list for the selected CustomerID
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "FillCustomerHierarchy";
        cmd.Parameters.AddWithValue("@CustomerID", intCustomerID);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static void ClearCustomerHierarchy(int intCustomerID)
    //delete all CustomerHierarchy for selected Customer
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "ClearCustomerHierarchy";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@CustomerID", intCustomerID);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
    }

    public static void InsertCustomerHierarchy(int intCustomerID, int intBudgetResponsibilityAreaID, int intCustomerGroupID, string strCurrencyCode, int intActiveIndicator, string strUserName)
    //insert CustomerHierarchy for selected Customer
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "InsertCustomerHierarchy";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@CustomerID", intCustomerID);
        cmd.Parameters.AddWithValue("@BudgetResponsibilityAreaID", intBudgetResponsibilityAreaID);
        cmd.Parameters.AddWithValue("@CustomerGroupID", intCustomerGroupID);
        cmd.Parameters.AddWithValue("@CurrencyCode", strCurrencyCode);
        cmd.Parameters.AddWithValue("@ActiveIndicator", intActiveIndicator);
        cmd.Parameters.AddWithValue("@UserName", strUserName);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
    }
}
