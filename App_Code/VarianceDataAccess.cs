﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;

/// <summary>
/// Wraps RiskTracker data
/// </summary>
public struct RiskTracker
{
    public int RiskTrackerID;
    public int ProjectToCustomerID;
    public int SpendTypeID;
    public string ProjectMonth;
    public int OpportunityOrRisk;
    public string KeyDrivers;
    public int FinancialImpact;
    public string CreatedBy;
    public string CreatedDate;
}

/// <summary>
/// Summary description for RiskTrackerDataAccess
/// </summary>
public class VarianceDataAccess
{
	public VarianceDataAccess()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static DataTable SelectRiskTracker(int intRiskTrackerID)
    //select an existing RiskTracker from its ID and put it in a DataTable object
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_SelectRiskTracker";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@RiskTrackerID", intRiskTrackerID);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        //RiskTracker h = new RiskTracker();
        //if (tbl.Rows.Count > 0)
        //{
        //    DataRow dr = tbl.Rows[0];
        //    h.RiskTrackerID = intRiskTrackerID;
        //    h.ProjectToCustomerID = Convert.ToInt32(dr["ProjectToCustomerID"]);
        //    h.SpendTypeID = Convert.ToInt32(dr["SpendTypeID"]);
        //    h.ProjectMonth = Convert.ToInt16(dr["ProjectMonth"]);
        //    h.OpportunityOrRisk = Convert.ToInt16(dr["OpportunityOrRisk"]);
        //    h.KeyDrivers = dr["KeyDrivers"].ToString();
        //    h.FinancialImpact = Convert.ToDouble(dr["FinancialImpact"]);
        //    h.CreatedBy = dr["CreatedBy"].ToString();
        //    h.CreatedDate = dr["CreatedDate"].ToString();
        //}
        return tbl;
    }

    public static int UpdateRiskTracker(RiskTracker h)
    //update an existing RiskTracker record
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_UpdateRiskTracker";

        // add parameters with their values
        cmd.Parameters.AddWithValue("@RiskTrackerID", h.RiskTrackerID);
        cmd.Parameters.AddWithValue("@ProjectToCustomerID", h.ProjectToCustomerID);
        cmd.Parameters.AddWithValue("@SpendTypeID", h.SpendTypeID);
        cmd.Parameters.AddWithValue("@ProjectMonth", h.ProjectMonth);
        cmd.Parameters.AddWithValue("@OpportunityOrRisk", h.OpportunityOrRisk);
        cmd.Parameters.AddWithValue("@KeyDrivers", h.KeyDrivers);
        cmd.Parameters.AddWithValue("@FinancialImpact", h.FinancialImpact);
        cmd.Parameters.AddWithValue("@LastUpdatedBy", h.CreatedBy);
        // execute the query to update the database
        int intResult = DataAccess.ExecuteUpdateCommand(cmd);
        return intResult;
    }

    public static int InsertRiskTracker(RiskTracker h)
    //create a new RiskTracker line and return the ID
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_InsertRiskTracker";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ProjectToCustomerID", h.ProjectToCustomerID);
        cmd.Parameters.AddWithValue("@SpendTypeID", h.SpendTypeID);
        cmd.Parameters.AddWithValue("@ProjectMonth", h.ProjectMonth);
        cmd.Parameters.AddWithValue("@OpportunityOrRisk", h.OpportunityOrRisk);
        cmd.Parameters.AddWithValue("@KeyDrivers", h.KeyDrivers);
        cmd.Parameters.AddWithValue("@FinancialImpact", h.FinancialImpact);
        cmd.Parameters.AddWithValue("@CreatedBy", h.CreatedBy);
        // execute the query to update the database
        int intRiskTrackerID = DataAccess.ExecuteScalarReturnIdentity(cmd);
        return intRiskTrackerID;
    }

    public static int DeleteRiskTracker(int intRiskTrackerID)
    //delete an existing RiskTracker line
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_DeleteRiskTracker";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@RiskTrackerID", intRiskTrackerID);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        return intRowsAffected;
    }

}
