﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;

/// <summary>
/// Summary description for BAPSMiscDataAccess
/// </summary>
public class BAPSMiscDataAccess
{
	public BAPSMiscDataAccess()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static DataTable SelectPulsingPlanPriority(int intPulsingPlanPriorityID)
    //select PulsingPlanPriority from PulsingPlanPriorityID
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_SelectPulsingPlanPriority";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@PulsingPlanPriorityID", intPulsingPlanPriorityID);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static int DeletePulsingPlanPriority(int intPulsingPlanPriorityID)
    //delete an existing PulsingPlanPriority record
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_DeletePulsingPlanPriority";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@PulsingPlanPriorityID", intPulsingPlanPriorityID);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        return intRowsAffected;
    }

    public static int InsertPulsingPlanPriority(string strPulsingPlanPriorityName)
    //create a new PulsingPlanPriority record
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_InsertPulsingPlanPriority";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@PulsingPlanPriorityName", strPulsingPlanPriorityName);
        //output parameter
        SqlParameter param = cmd.CreateParameter();
        param.ParameterName = "@NewPulsingPlanPriorityID";
        param.Direction = ParameterDirection.Output;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        // execute the query to update the database and return the new ID
        int intPulsingPlanPriorityID = DataAccess.ExecuteScalarReturnIdentity(cmd);
        //return intID;
        return Convert.ToInt32(cmd.Parameters["@NewPulsingPlanPriorityID"].Value);
    }

    public static string UpdatePulsingPlanPriority(int intPulsingPlanPriorityID, string strPulsingPlanPriorityName)
    //update an existing PulsingPlanPriority record
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_UpdatePulsingPlanPriority";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@PulsingPlanPriorityID", intPulsingPlanPriorityID);
        cmd.Parameters.AddWithValue("@PulsingPlanPriorityName", strPulsingPlanPriorityName);
        // output parameter
        SqlParameter param = cmd.CreateParameter();
        param.ParameterName = "@Message";
        param.Direction = ParameterDirection.Output;
        param.DbType = DbType.String;
        param.Size = 255;
        cmd.Parameters.Add(param);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        // return the message
        string strMessage = cmd.Parameters["@Message"].Value.ToString();
        return strMessage;
    }


}
