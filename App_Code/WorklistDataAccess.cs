﻿using System;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Data Access Layer for Worklists
/// </summary>
public class WorklistDataAccess
{
	public WorklistDataAccess()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static DataTable WorklistSummary(string userName)
    //Select the data for worklist 1 - no longer used by Worklist Grids
    {
        //get a configured SqlCommand object
        SqlCommand cmd = DataAccess.CreateCommand();
        //set the stored procedure name
        cmd.CommandText = "WorklistSummary";
        //parameters
        cmd.Parameters.AddWithValue("@UserName", userName);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static DataTable SelectWorklist1(string strNPICoordinator)
    //Select the data for worklist 1 - no longer used by Worklist Grids
    {
        //get a configured SqlCommand object
        SqlCommand cmd = DataAccess.CreateCommand();
        //set the stored procedure name
        cmd.CommandText = "SelectWorklist1";
        //parameters
        cmd.Parameters.AddWithValue("@UserName", strNPICoordinator);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static DataTable SelectWorklist2(string strRequisitionBy)
    //Select the data for worklist 2 - no longer used by Worklist Grids
    {
        //get a configured SqlCommand object
        SqlCommand cmd = DataAccess.CreateCommand();
        //set the stored procedure name
        cmd.CommandText = "SelectWorklist2";
        //parameters
        cmd.Parameters.AddWithValue("@UserName", strRequisitionBy);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static DataTable SelectWorklist3(string strNPICoordinator)
    //Select the data for worklist 3 - no longer used by Worklist Grids
    {
        //get a configured SqlCommand object
        SqlCommand cmd = DataAccess.CreateCommand();
        //set the stored procedure name
        cmd.CommandText = "SelectWorklist3";
        //parameters
        cmd.Parameters.AddWithValue("@UserName", strNPICoordinator);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static DataTable SelectWorklist4(string strNPICoordinator)
    //Select the data for worklist 4 - no longer used by Worklist Grids
    {
        //get a configured SqlCommand object
        SqlCommand cmd = DataAccess.CreateCommand();
        //set the stored procedure name
        cmd.CommandText = "SelectWorklist4";
        //parameters
        cmd.Parameters.AddWithValue("@UserName", strNPICoordinator);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static DataTable SelectWorklist5(string strNPICoordinator)
    //Select the data for worklist 5 - no longer used by Worklist Grids
    {
        //get a configured SqlCommand object
        SqlCommand cmd = DataAccess.CreateCommand();
        //set the stored procedure name
        cmd.CommandText = "sp_ap_SelectWorklist5";
        //parameters
        cmd.Parameters.AddWithValue("@UserName", strNPICoordinator);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static DataTable SelectWorklist7(string strNPICoordinator)
    //Select the data for worklist 7 - not used by Worklist Grids, only WorklistSummary
    {
        //get a configured SqlCommand object
        SqlCommand cmd = DataAccess.CreateCommand();
        //set the stored procedure name
        cmd.CommandText = "sp_ap_SelectWorklist7";
        //parameters
        cmd.Parameters.AddWithValue("@UserName", strNPICoordinator);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static DataTable SelectWorklist8(string strNPICoordinator)
    //Select the data for worklist 8 - not used by Worklist Grids, only WorklistSummary
    {
        //get a configured SqlCommand object
        SqlCommand cmd = DataAccess.CreateCommand();
        //set the stored procedure name
        cmd.CommandText = "sp_ap_SelectWorklist8";
        //parameters
        cmd.Parameters.AddWithValue("@UserName", strNPICoordinator);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static DataTable SelectWorklist6(string strNPICoordinator)
    //Select the data for worklist 6 - not used by Worklist Grids, only WorklistSummary
    {
        //get a configured SqlCommand object
        SqlCommand cmd = DataAccess.CreateCommand();
        //set the stored procedure name
        cmd.CommandText = "sp_ap_SelectWorklist6";
        //parameters
        cmd.Parameters.AddWithValue("@UserName", strNPICoordinator);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static DataTable SelectWorklist9(string UserName)
    //Select the data for worklist 9 - not used by Worklist Grids, only WorklistSummary
    {
        //get a configured SqlCommand object
        SqlCommand cmd = DataAccess.CreateCommand();
        //set the stored procedure name
        cmd.CommandText = "SelectWorklist9";
        //parameters
        cmd.Parameters.AddWithValue("@UserName", UserName);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static DataTable SelectWorklist10(string UserName)
    //Select the data for worklist 10 - not used by Worklist Grids, only WorklistSummary
    {
        //get a configured SqlCommand object
        SqlCommand cmd = DataAccess.CreateCommand();
        //set the stored procedure name
        cmd.CommandText = "SelectWorklist10";
        //parameters
        cmd.Parameters.AddWithValue("@UserName", UserName);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static String UpdateWorklist9(int ReallocationID, int status)
    //Approve or Reject Worklist9 entries
    {
        //get a configured SqlCommand object
        SqlCommand cmd = DataAccess.CreateCommand();
        //set the stored procedure name
        cmd.CommandText = "UpdateWorklist9";
        //parameters
        cmd.Parameters.AddWithValue("@ReallocationID", ReallocationID);
        cmd.Parameters.AddWithValue("@Status", status);

        SqlParameter param = cmd.CreateParameter();
        param.ParameterName = "@ErrorMessage";
        param.Direction = ParameterDirection.Output;
        param.DbType = DbType.String;
        param.Size = 150;
        cmd.Parameters.Add(param);

        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);

        string strErrorMessage = cmd.Parameters["@ErrorMessage"].Value.ToString();
        return strErrorMessage;
    }

    public static DataTable DeleteWorklist10(int ReallocationID)
    //Delete entries from Worklist10
    {
        //get a configured SqlCommand object
        SqlCommand cmd = DataAccess.CreateCommand();
        //set the stored procedure name
        cmd.CommandText = "DeleteWorklist10";
        //parameters
        cmd.Parameters.AddWithValue("@ReallocationID", ReallocationID);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static int UpdatePOChange(int intPOChangeID)
    //Action a selected POChange record
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "UpdatePOChange";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@POChangeID", intPOChangeID);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        return intRowsAffected;
    }

    public static int UpdateRebateChange(int intRebateChangeID)
    //Action a selected RebateChange record
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_UpdateRebateChange";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@RebateChangeID", intRebateChangeID);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        return intRowsAffected;
    }

    public static int UpdateReceiptedStatus(int intActivityDetailID, string strReceiptedStatus)
    //Update receipted status for selected ActivityDetail record
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "UpdateReceiptedStatus";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ActivityDetailID", intActivityDetailID);
        cmd.Parameters.AddWithValue("@ReceiptedStatus", strReceiptedStatus);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        return intRowsAffected;
    }

    public static int UpdateReceiptedStatus(int intActivityDetailID, string strReceiptedStatus, string strUserName)
    //Update receipted status for selected ActivityDetail record
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_UpdateReceiptedStatus";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ActivityDetailID", intActivityDetailID);
        cmd.Parameters.AddWithValue("@ReceiptedStatus", strReceiptedStatus);
        cmd.Parameters.AddWithValue("@UserName", strUserName);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        return intRowsAffected;
    }
}
