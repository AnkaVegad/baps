﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Wraps RebateHeader data
/// </summary>
public struct Rebate
{
    public int RebateID;
    public int AccrualTypeID;
    public string AgreementNumber;
    public string AgreementTypeCode;
    public string ConditionTypeCode;
    public string InStoreStartDate;
    public string InStoreEndDate;
    public string SAPCustomerCode;
    public string CustomerName2;
    public string Comments;
    public string RequestedBy;
    public string RequestedDate;
    public string ApprovedL1By;
    public string ApprovedL1Date;
    public string ApprovedL2By;
    public string ApprovedL2Date;
    public string RebateCreator;
    public string RebateCreatedDate;
    public string LastUpdatedBy;
    public string LastUpdatedDate;
    public string CreatedBy;
    public string CreatedDate;
}

/// <summary>
/// Data Access Layer for Rebate Header
/// </summary>
public class RebateHeaderDataAccess
{
	public RebateHeaderDataAccess()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static Rebate SelectRebateHeader(int intRebateID)
    //selects an existing RebateHeader and puts it in a Rebate struct object
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_SelectRebateHeader";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@RebateID", intRebateID);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);

        Rebate r = new Rebate();
        if (tbl.Rows.Count > 0)
        {
            DataRow dr = tbl.Rows[0];
            r.RebateID = intRebateID;
            r.AccrualTypeID = Convert.ToInt16(dr["AccrualTypeID"]);
            r.AgreementNumber = dr["AgreementNumber"].ToString();
            r.AgreementTypeCode = dr["AgreementTypeCode"].ToString();
            r.ConditionTypeCode = dr["ConditionTypeCode"].ToString();
            r.InStoreStartDate = dr["InStoreStartDate"].ToString();
            r.InStoreEndDate = dr["InStoreEndDate"].ToString();
            r.SAPCustomerCode = dr["SAPCustomerCode"].ToString();
            r.CustomerName2 = dr["CustomerName"].ToString();
            r.Comments = dr["Comments"].ToString();
            r.RequestedBy = dr["RequestedBy"].ToString();
            r.RequestedDate = dr["RequestedDate"].ToString();
            r.ApprovedL1By = dr["ApprovedL1By"].ToString();
            r.ApprovedL1Date = dr["ApprovedL1Date"].ToString();
            r.ApprovedL2By = dr["ApprovedL2By"].ToString();
            r.ApprovedL2Date = dr["ApprovedL2Date"].ToString();
            r.RebateCreator = dr["RebateCreator"].ToString();
            r.RebateCreatedDate = dr["RebateCreatedDate"].ToString();
            r.LastUpdatedBy = dr["LastUpdatedBy"].ToString();
            r.LastUpdatedDate = dr["LastUpdatedDate"].ToString();
            r.CreatedBy = dr["CreatedBy"].ToString();
            r.CreatedBy = dr["CreatedDate"].ToString();
        }

        return r;
    }

    public static string UpdateRebateHeader(Rebate r)
    //Update a selected RebateHeader record
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_UpdateRebateHeader";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@RebateID", r.RebateID);
        cmd.Parameters.AddWithValue("@AccrualTypeID", r.AccrualTypeID);
        if (r.AgreementNumber == "") { cmd.Parameters.AddWithValue("@AgreementNumber", DBNull.Value); } else { cmd.Parameters.AddWithValue("@AgreementNumber", r.AgreementNumber); }
        cmd.Parameters.AddWithValue("@AgreementTypeCode", r.AgreementTypeCode);
        cmd.Parameters.AddWithValue("@ConditionTypeCode", r.ConditionTypeCode);
        cmd.Parameters.AddWithValue("@InStoreStartDate", r.InStoreStartDate);
        cmd.Parameters.AddWithValue("@InStoreEndDate", r.InStoreEndDate);
        cmd.Parameters.AddWithValue("@SAPCustomerCode", r.SAPCustomerCode);
        cmd.Parameters.AddWithValue("@CustomerName", r.CustomerName2);
        cmd.Parameters.AddWithValue("@Comments", r.Comments);
        cmd.Parameters.AddWithValue("@RequestedBy", r.RequestedBy);
        cmd.Parameters.AddWithValue("@ApprovedL1By", r.ApprovedL1By);
        cmd.Parameters.AddWithValue("@RebateCreator", r.RebateCreator);
        cmd.Parameters.AddWithValue("@LastUpdatedBy", r.LastUpdatedBy);

        SqlParameter param = cmd.CreateParameter();
        param.ParameterName = "@Message";
        param.Direction = ParameterDirection.Output;
        param.DbType = DbType.String;
        param.Size = 255;
        cmd.Parameters.Add(param);

        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);

        string strMessage = cmd.Parameters["@Message"].Value.ToString();
        return strMessage;
    }

    public static int InsertRebateHeader(Rebate r)
    //create a new RebateHeader; AgreementNumber, ApprovedL1Date, RebateCreatedDate cannot be known at this time
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_InsertRebateHeader";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@AccrualTypeID", r.AccrualTypeID);
        cmd.Parameters.AddWithValue("@AgreementTypeCode", r.AgreementTypeCode);
        cmd.Parameters.AddWithValue("@ConditionTypeCode", r.ConditionTypeCode);
        cmd.Parameters.AddWithValue("@InStoreStartDate", r.InStoreStartDate);
        cmd.Parameters.AddWithValue("@InStoreEndDate", r.InStoreEndDate);
        cmd.Parameters.AddWithValue("@SAPCustomerCode", r.SAPCustomerCode);
        cmd.Parameters.AddWithValue("@CustomerName", r.CustomerName2);
        cmd.Parameters.AddWithValue("@Comments", r.Comments);
        cmd.Parameters.AddWithValue("@RequestedBy", r.RequestedBy);
        cmd.Parameters.AddWithValue("@ApprovedL1By", r.ApprovedL1By);
        cmd.Parameters.AddWithValue("@RebateCreator", r.RebateCreator);
        cmd.Parameters.AddWithValue("@CreatedBy", r.CreatedBy);

        // execute the query to update the database
        int intRebateID = DataAccess.ExecuteScalarReturnIdentity(cmd);
        return intRebateID;
    }

    public static DataTable SelectActivitiesForRebateID(int intRebateID)
    //retrieve the Activities for selected Rebate
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_SelectActivitiesForRebateID";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@RebateID", intRebateID);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static DataTable SelectActivityToProduct(int intActivityID)
    //
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_SelectActivityToProduct";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ActivityID", intActivityID);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static int InsertRebateActuals(string strUserName, string strNewYearMonth)
    //
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_InsertRebateActuals";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@UserName", strUserName);
        cmd.Parameters.AddWithValue("@NewYearMonth", strNewYearMonth);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        //added AH 03/10/12
        return intRowsAffected;
    }

    public static void ClearRebateIDForActivities(int intRebateID)
    //clear RebateID from all existing activities
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_ClearRebateIDForActivities";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@RebateID", intRebateID);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
    }

    public static int UpdateActivityForRebate(Activity a)
    //updates an existing activity
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_UpdateActivityForRebate";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ActivityID", a.ActivityID);
        cmd.Parameters.AddWithValue("@LastUpdatedBy", a.LastUpdatedBy);
        CreateParameters(cmd, a);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        return intRowsAffected;
    }

    public static string ApproveRebate(int intRebateID, string strApprovedL1By, int intApprovalValue, string strMode)
    //approves a rebate if user has sufficient authority, or clears the approval
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_ApproveRebate";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@RebateID", intRebateID);
        cmd.Parameters.AddWithValue("@ApprovedL1By", strApprovedL1By);
        cmd.Parameters.AddWithValue("@ApprovalValue", intApprovalValue);
        cmd.Parameters.AddWithValue("@Mode", strMode);
        //output message parameter
        SqlParameter param = cmd.CreateParameter();
        param.ParameterName = "@Message";
        param.Direction = ParameterDirection.Output;
        param.DbType = DbType.String;
        param.Size = 255;
        cmd.Parameters.Add(param);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);

        string strMessage = cmd.Parameters["@Message"].Value.ToString();
        return strMessage;
    }

    public static void InsertActivityForRebate(Activity a)
    //creates a new activity
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_InsertActivityForRebate";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ProjectToCustomerID", a.ProjectToCustomerID);
        cmd.Parameters.AddWithValue("@CreatedBy", a.CreatedBy);
        CreateParameters(cmd, a);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
    }

    protected static void CreateParameters(SqlCommand cmd, Activity a)
    //common parameter list for UpdateActivity and InsertActivity
    {
        cmd.Parameters.AddWithValue("@ActivityName", a.ActivityName);
        cmd.Parameters.AddWithValue("@ShortText", a.ShortText);
        cmd.Parameters.AddWithValue("@ItemQuantity", a.ItemQuantity);
        cmd.Parameters.AddWithValue("@UnitPrice", a.UnitPrice);
        cmd.Parameters.AddWithValue("@ProjectMonth", a.ProjectMonth);
        if (a.POID == "") { cmd.Parameters.AddWithValue("@POID", DBNull.Value); } else { cmd.Parameters.AddWithValue("@POID", a.POID); }
        if (a.RebateID == "") { cmd.Parameters.AddWithValue("@RebateID", DBNull.Value); } else { cmd.Parameters.AddWithValue("@RebateID", a.RebateID); }
        cmd.Parameters.AddWithValue("@DocumentNumber", a.DocumentNumber);
        cmd.Parameters.AddWithValue("@ActivityTypeID", a.ActivityTypeID);
        cmd.Parameters.AddWithValue("@SpendTypeID", a.SpendTypeID);
        if (a.SupplierID == "") { cmd.Parameters.AddWithValue("@SupplierID", DBNull.Value); } else { cmd.Parameters.AddWithValue("@SupplierID", a.SupplierID); }
        cmd.Parameters.AddWithValue("@SupplierRefNo", a.SupplierRefNo);
        cmd.Parameters.AddWithValue("@Comments", a.Comments);
        cmd.Parameters.AddWithValue("@Month01", a.Month01);
        cmd.Parameters.AddWithValue("@Month02", a.Month02);
        cmd.Parameters.AddWithValue("@Month03", a.Month03);
        cmd.Parameters.AddWithValue("@Month04", a.Month04);
        cmd.Parameters.AddWithValue("@Month05", a.Month05);
        cmd.Parameters.AddWithValue("@Month06", a.Month06);
        cmd.Parameters.AddWithValue("@Month07", a.Month07);
        cmd.Parameters.AddWithValue("@Month08", a.Month08);
        cmd.Parameters.AddWithValue("@Month09", a.Month09);
        cmd.Parameters.AddWithValue("@Month10", a.Month10);
        cmd.Parameters.AddWithValue("@Month11", a.Month11);
        cmd.Parameters.AddWithValue("@Month12", a.Month12);
    }

    public static DataTable FillActivityToProduct(int intRebateID)
    //Get the Product list for the first Activity in the selected rebate
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "sp_ap_FillActivityToProduct";
        cmd.Parameters.AddWithValue("@RebateID", intRebateID);
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static void InsertActivityToProduct(int intActivityID, string strProductCode, double dblProportion)
    //creates a new ActivityToProduct record
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_InsertActivityToProduct";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ActivityID", intActivityID);
        cmd.Parameters.AddWithValue("@ProductCode", strProductCode);
        cmd.Parameters.AddWithValue("@Proportion", dblProportion);
        //cmd.Parameters.AddWithValue("@CreatedBy", a.CreatedBy);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
    }

    public static DataTable SelectProjectForRebateID(int intRebateID)
    //return project record for selected Rebate
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_SelectProjectForRebateID";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@RebateID", intRebateID);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static void ClearRebatesInput()
    //clear the TCC input staging table
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_ClearRebatesInput";
        //execute stored procedure
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
    }

}
