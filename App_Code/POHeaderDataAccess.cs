﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Wraps POHeader (purchase requisition) data
/// </summary>
public struct PO
{
    public int POID;
    public string PRNumber;
    public string PONumber;
    public int SupplierID;
    public string SupplierName;
    public string SAPVendorCode;
    public int DeliveryAddressID;
    public string DeliveryAddress;
    public string Comments;
    public string GRDate;
    public string RequisitionBy;
    public string NPICoordinator;
    public string ReleasedBy;
    public string LastUpdatedBy;
    public string LastUpdatedDate;
    public string CreatedBy;
    public string CreatedDate;
    public string FinanceApprovalFlag;
    public string NeedsApproval;
}

/// <summary>
/// Data Access Layer for PO Header
/// </summary>
public class POHeaderDataAccess
{
	public POHeaderDataAccess()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    //selects an existing POHeader and puts it in a POHeader struct object
    public static PO SelectPOHeader(int intPOID)
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "SelectPOHeader";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@POID", intPOID);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);

        PO p = new PO();
        if (tbl.Rows.Count > 0)
        {
            DataRow dr = tbl.Rows[0];
            p.POID = intPOID;
            p.PRNumber = dr["PRNumber"].ToString();
            p.PONumber = dr["PONumber"].ToString();
            p.SupplierID = Convert.ToInt32(dr["SupplierID"]);
            p.SupplierName = dr["SupplierName"].ToString();
            p.SAPVendorCode = dr["SAPVendorCode"].ToString();
            p.DeliveryAddressID = Convert.ToInt16(dr["DeliveryAddressID"]);
            p.DeliveryAddress = dr["DeliveryAddress"].ToString();
            p.Comments = dr["Comments"].ToString();
            p.GRDate = dr["GRDate"].ToString();
            p.RequisitionBy = dr["RequisitionBy"].ToString();
            p.NPICoordinator = dr["NPICoordinator"].ToString();
            p.ReleasedBy = dr["ReleasedBy"].ToString();
            //p.ReleasedDate = dr["ReleasedDate"].ToString();
            p.LastUpdatedBy = dr["LastUpdatedBy"].ToString();
            p.LastUpdatedDate = dr["LastUpdatedDate"].ToString();
            p.CreatedBy = dr["CreatedBy"].ToString();
            p.CreatedBy = dr["CreatedDate"].ToString();
            p.FinanceApprovalFlag = dr["FinanceApprovalFlag"].ToString();
            p.NeedsApproval = dr["NeedsApproval"].ToString();
        }

        return p;
    }

    public static string UpdatePOHeader(PO p)
    //Update a selected POHeader record
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "UpdatePOHeader";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@POID", p.POID);
        if (p.PRNumber == "") { cmd.Parameters.AddWithValue("@PRNumber", DBNull.Value); } else { cmd.Parameters.AddWithValue("@PRNumber", p.PRNumber); }
        if (p.PONumber == "") { cmd.Parameters.AddWithValue("@PONumber", DBNull.Value); } else { cmd.Parameters.AddWithValue("@PONumber", p.PONumber); }
        cmd.Parameters.AddWithValue("@SupplierID", p.SupplierID);
        cmd.Parameters.AddWithValue("@DeliveryAddressID", p.DeliveryAddressID);
        cmd.Parameters.AddWithValue("@Comments", p.Comments);
        cmd.Parameters.AddWithValue("@RequisitionBy", p.RequisitionBy);
        cmd.Parameters.AddWithValue("@NPICoordinator", p.NPICoordinator);
        cmd.Parameters.AddWithValue("@ReleasedBy", p.ReleasedBy);
        cmd.Parameters.AddWithValue("@LastUpdatedBy", p.LastUpdatedBy);
        cmd.Parameters.AddWithValue("@FinanceApproval", p.FinanceApprovalFlag);
        //cmd.Parameters.AddWithValue("@NeedsApproval", p.NeedsApproval);
        


        SqlParameter param = cmd.CreateParameter();
        param.ParameterName = "@Message";
        param.Direction = ParameterDirection.Output;
        param.DbType = DbType.String;
        param.Size = 255;
        cmd.Parameters.Add(param);

        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);

        string strMessage = cmd.Parameters["@Message"].Value.ToString();
        return strMessage;
    }

    public static int InsertPOHeader(PO p)
    //create a new POHeader; PRNumber and PONumber cannot be known at this time
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "InsertPOHeader";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@SupplierID", p.SupplierID);
        cmd.Parameters.AddWithValue("@DeliveryAddressID", p.DeliveryAddressID);
        cmd.Parameters.AddWithValue("@Comments", p.Comments);
        cmd.Parameters.AddWithValue("@NPICoordinator", p.NPICoordinator);
        cmd.Parameters.AddWithValue("@RequisitionBy", p.RequisitionBy);
        cmd.Parameters.AddWithValue("@ReleasedBy", p.ReleasedBy);
        cmd.Parameters.AddWithValue("@CreatedBy", p.CreatedBy);
        cmd.Parameters.AddWithValue("@FinanceApproval", p.FinanceApprovalFlag);
        //cmd.Parameters.AddWithValue("@NeedsApproval", p.NeedsApproval);

            // execute the query to update the database
        int intPOID = DataAccess.ExecuteScalarReturnIdentity(cmd);
        return intPOID;
    }

    public static DataTable SelectActivitiesForPOID(int intPOID)
    //retrieve the Activities for selected POHeader
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "SelectActivitiesForPOID";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@POID", intPOID);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static DataTable SelectProjectForPOID(int intPOID)
    // for selected POHeader
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "SelectProjectForPOID";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@POID", intPOID);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static void ClearPOIDForActivities(int intPOID)
    //clear POID from all existing activities
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "ClearPOIDForActivities";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@POID", intPOID);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
    }

    //selects an existing POHeader and puts it in a POHeader struct object
    public static string SelectDeliveryAddress(int intDeliveryAddressID)
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "SelectDeliveryAddress";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@DeliveryAddressID", intDeliveryAddressID);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl.Rows[0]["DeliveryAddress"].ToString();
    }

    public static object GetPOValue(int intPOID)
    {
        //get a configured SqlCommand object
        SqlCommand cmd = DataAccess.CreateCommand();
        //set the stored procedure name
        cmd.CommandText = "GetPOValue";
        //create OptionName parameter
        cmd.Parameters.AddWithValue("@POID", intPOID);
        //execute stored procedure and store results in a string
        object objPOValue = DataAccess.ExecuteScalar(cmd);
        return objPOValue;
    }

    public static int InsertPOChange(int intPOID, double dblPOOldValue, double dblPONewValue, string strUserName)
    //create a POChange record and return the ID
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "InsertPOChange";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@POID", intPOID);
        cmd.Parameters.AddWithValue("@POOldValue", dblPOOldValue);
        cmd.Parameters.AddWithValue("@PONewValue", dblPONewValue);
        cmd.Parameters.AddWithValue("@ChangedBy", strUserName);
        // execute the query to update the database
        int intPOChangeID = DataAccess.ExecuteScalarReturnIdentity(cmd);
        return intPOChangeID;
    }

    public static int ReadyToReceipt(int intActivityID)
    //returns the number of items ready for receipt for an activity 
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "ReadyToReceipt";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ActivityID", intActivityID);
        // execute the query to update the database
        return Convert.ToInt16(DataAccess.ExecuteScalar(cmd));
    }
}
