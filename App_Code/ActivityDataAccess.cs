﻿using System;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Wraps Activity data
/// </summary>
public struct Activity
{
    public int ActivityID;
    public int ProjectToCustomerID;
    public string ActivityName;
    public string ShortText;
    public int ItemQuantity;
    public decimal UnitPrice;
    public string MonthRequired;
    public string ProjectMonth;
    public string MonthShortName;
    public string POID;
    public string RebateID; //added 24/02/12 for rebate maint functionality
    public string DocumentNumber;
    public int ActivityTypeID;
    public string ActivityTypeName;
    public string GLCode; //added 29/05/14 for new Activity Type structure
    public string CommodityCode; //added 29/05/14 for new Activity Type structure
    public int SpendTypeID;
    public string SpendTypeName;
    public string SupplierID; //amended 06/09/10 for change to string datatype - enables blanks to be stored 
    public string SupplierName;
    public string SupplierRefNo;
    //added 05/10/14
    public string UoM;
    public string POItemNo;
    public string CommittedOverrider;
    public string PromoIndicator;
    //end
    public string Comments;
    public string LastUpdatedBy;
    public string LastUpdatedDate;
    public string CreatedBy;
    public string CreatedDate;
    public string Month01;
    public string Month02;
    public string Month03;
    public string Month04;
    public string Month05;
    public string Month06;
    public string Month07;
    public string Month08;
    public string Month09;
    public string Month10;
    public string Month11;
    public string Month12;
    public string ActivityStatusID;
    public string ActivityStatusName;
    public string BudgetResponsibilityAreaID;
}

/// <summary>
/// Data Access Layer for Activity
/// </summary>
public class ActivityDataAccess
{
	public ActivityDataAccess()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static Activity SelectActivity(int intActivityID)
    //selects an existing Activity in the database using qryActivity and puts it in an Activity struct object (via a datatable)
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "SelectActivity";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ActivityID", intActivityID);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        Activity a = new Activity();
        if (tbl.Rows.Count > 0)
        {
            DataRow dr = tbl.Rows[0];
            a.ActivityID = intActivityID;
            a.ProjectToCustomerID = Convert.ToInt32(dr["ProjectToCustomerID"]);
            a.ActivityName = dr["ActivityName"].ToString();
            a.ShortText = dr["ShortText"].ToString();
            a.ItemQuantity = Convert.ToInt32(dr["ItemQuantity"]);
            a.UnitPrice = Decimal.Parse(dr["UnitPrice"].ToString());
            a.UoM = dr["UoM"].ToString();
            a.MonthRequired = dr["MonthRequired"].ToString();
            a.ProjectMonth = dr["ProjectMonth"].ToString();
            a.MonthShortName = dr["MonthShortName"].ToString();
            if (dr["POID"] == DBNull.Value) { a.POID = ""; } else { a.POID = dr["POID"].ToString(); }
            if (dr["RebateID"] == DBNull.Value) { a.RebateID = ""; } else { a.RebateID = dr["RebateID"].ToString(); } //added 27/02/12 for rebate maint functionality
            a.DocumentNumber = dr["DocumentNumber"].ToString();
            a.POItemNo = dr["POItemNo"].ToString();
            a.BudgetResponsibilityAreaID = dr["BudgetResponsibilityAreaID"].ToString();
            a.SpendTypeID = Convert.ToInt16(dr["SpendTypeID"]);
            a.SpendTypeName = dr["SpendTypeName"].ToString();
            a.ActivityTypeID = Convert.ToInt16(dr["ActivityTypeID"]);
            a.ActivityTypeName = dr["ActivityTypeName"].ToString();
            a.SupplierID = dr["SupplierID"].ToString(); //amended 06/09/10 for change to string datatype
            a.SupplierName = dr["SupplierName"].ToString();
            a.SupplierRefNo = dr["SupplierRefNo"].ToString();
            a.Comments = dr["Comments"].ToString();
            a.CommittedOverrider = dr["CommittedOverrider"].ToString();
            a.PromoIndicator = dr["PromoIndicator"].ToString();
            a.LastUpdatedBy = dr["LastUpdatedBy"].ToString();
            a.LastUpdatedDate = dr["LastUpdatedDate"].ToString();
            a.CreatedBy = dr["CreatedBy"].ToString();
            a.CreatedDate = dr["CreatedDate"].ToString();
            a.Month01 = dr["Month01"].ToString();
            a.Month02 = dr["Month02"].ToString();
            a.Month03 = dr["Month03"].ToString();
            a.Month04 = dr["Month04"].ToString();
            a.Month05 = dr["Month05"].ToString();
            a.Month06 = dr["Month06"].ToString();
            a.Month07 = dr["Month07"].ToString();
            a.Month08 = dr["Month08"].ToString();
            a.Month09 = dr["Month09"].ToString();
            a.Month10 = dr["Month10"].ToString();
            a.Month11 = dr["Month11"].ToString();
            a.Month12 = dr["Month12"].ToString();
            a.ActivityStatusID = dr["ActivityStatusID"].ToString();
            a.ActivityStatusName = dr["ActivityStatusName"].ToString();
            a.GLCode = dr["GLCode"].ToString();
            a.CommodityCode = dr["CommodityCode"].ToString();
        }

        return a;
    }

    public static DataTable SelectActivityToTable(int intActivityID)
    //selects an existing Activity in the database and puts it in a datatable
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "SelectActivity";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ActivityID", intActivityID);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static int UpdateActivity(Activity a, int intInsertHistoryIndicator)
    //updates an existing activity
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "UpdateActivity";

        // add parameters with their values
        cmd.Parameters.AddWithValue("@ActivityID", a.ActivityID);
        cmd.Parameters.AddWithValue("@InsertHistoryIndicator", intInsertHistoryIndicator);
        cmd.Parameters.AddWithValue("@LastUpdatedBy", a.LastUpdatedBy);
        CreateParameters(cmd, a);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        return intRowsAffected;
    }

    public static void InsertActivity(Activity a)
    //creates a new activity
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "InsertActivity";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ProjectToCustomerID", a.ProjectToCustomerID);
        cmd.Parameters.AddWithValue("@CreatedBy", a.CreatedBy);
        CreateParameters(cmd, a);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
    }

    public static int UpdateActivity_rs(Activity a, int intInsertHistoryIndicator)
    //updates an existing activity; replaces InsertActivity with additional fields
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_UpdateActivity";

        // add parameters with their values
        cmd.Parameters.AddWithValue("@ActivityID", a.ActivityID);
        cmd.Parameters.AddWithValue("@InsertHistoryIndicator", intInsertHistoryIndicator);
        cmd.Parameters.AddWithValue("@CommittedOverrider", a.CommittedOverrider);
        cmd.Parameters.AddWithValue("@PromoIndicator", a.PromoIndicator);
        //cmd.Parameters.AddWithValue("@UoM", a.UoM);
        //cmd.Parameters.AddWithValue("@POItemNo", a.POItemNo);
        cmd.Parameters.AddWithValue("@LastUpdatedBy", a.LastUpdatedBy);
        CreateParameters(cmd, a);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        return intRowsAffected;
    }

    public static void InsertActivity_rs(Activity a)
    //creates a new activity; replaces InsertActivity with additional fields
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_InsertActivity";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ProjectToCustomerID", a.ProjectToCustomerID);
        cmd.Parameters.AddWithValue("@CommittedOverrider", a.CommittedOverrider);
        cmd.Parameters.AddWithValue("@PromoIndicator", a.PromoIndicator);
        //cmd.Parameters.AddWithValue("@UoM", a.UoM);
        //cmd.Parameters.AddWithValue("@POItemNo", a.POItemNo);
        cmd.Parameters.AddWithValue("@CreatedBy", a.CreatedBy);
        CreateParameters(cmd, a);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
    }

    public static void InsertActivityHistory(int intActivityID)
    //creates a new activity history record
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_InsertActivityHistory";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ActivityID", intActivityID);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
    }

    protected static void CreateParameters(SqlCommand cmd, Activity a)
    //common parameter list for UpdateActivity and InsertActivity
    {
        cmd.Parameters.AddWithValue("@ActivityName", a.ActivityName);
        cmd.Parameters.AddWithValue("@ShortText", a.ShortText);
        cmd.Parameters.AddWithValue("@ItemQuantity", a.ItemQuantity);
        cmd.Parameters.AddWithValue("@UnitPrice", a.UnitPrice);
        cmd.Parameters.AddWithValue("@ProjectMonth", a.ProjectMonth);
        if (a.POID == "")
        {
            cmd.Parameters.AddWithValue("@POID", DBNull.Value);
        }
        else
        {
            cmd.Parameters.AddWithValue("@POID", a.POID);
        }
        cmd.Parameters.AddWithValue("@DocumentNumber", a.DocumentNumber);
        cmd.Parameters.AddWithValue("@ActivityTypeID", a.ActivityTypeID);
        cmd.Parameters.AddWithValue("@SpendTypeID", a.SpendTypeID);
        if (a.SupplierID == "")
        {
            cmd.Parameters.AddWithValue("@SupplierID", DBNull.Value);
        }
        else
        {
            cmd.Parameters.AddWithValue("@SupplierID", a.SupplierID);
        }
        cmd.Parameters.AddWithValue("@SupplierRefNo", a.SupplierRefNo);
        cmd.Parameters.AddWithValue("@Comments", a.Comments);
        cmd.Parameters.AddWithValue("@Month01", a.Month01);
        cmd.Parameters.AddWithValue("@Month02", a.Month02);
        cmd.Parameters.AddWithValue("@Month03", a.Month03);
        cmd.Parameters.AddWithValue("@Month04", a.Month04);
        cmd.Parameters.AddWithValue("@Month05", a.Month05);
        cmd.Parameters.AddWithValue("@Month06", a.Month06);
        cmd.Parameters.AddWithValue("@Month07", a.Month07);
        cmd.Parameters.AddWithValue("@Month08", a.Month08);
        cmd.Parameters.AddWithValue("@Month09", a.Month09);
        cmd.Parameters.AddWithValue("@Month10", a.Month10);
        cmd.Parameters.AddWithValue("@Month11", a.Month11);
        cmd.Parameters.AddWithValue("@Month12", a.Month12);
    }
    
    public static string DeleteActivity(int intActivityID, string strUserName)
    //deletes an existing activity
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "DeleteActivity";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ActivityID", intActivityID);
        cmd.Parameters.AddWithValue("@UserName", strUserName);
        //add output parameter
        SqlParameter param = cmd.CreateParameter();
        param.ParameterName = "@Message";
        param.Direction = ParameterDirection.Output;
        param.DbType = DbType.String;
        param.Size = 255;
        cmd.Parameters.Add(param);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        string strMessage = cmd.Parameters["@Message"].Value.ToString();
        return strMessage;
    }

    public static DataTable SelectActivityView(int intProjectToCustomerID)
    //retrieve the activity records for selected project
    {
        //get a configured SqlCommand object
        SqlCommand cmd = DataAccess.CreateCommand();
        //set the stored procedure name
        cmd.CommandText = "SelectActivityView";
        //parameters
        cmd.Parameters.AddWithValue("@ProjectToCustomerID", intProjectToCustomerID);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static DataTable SelectIONumber(int intProjectID, int intCustomerID)
    //return IO Number for project and customer
    {
        //get a configured SqlCommand object
        SqlCommand cmd = DataAccess.CreateCommand();
        //set the stored procedure name
        cmd.CommandText = "SelectIONumber";
        //parameters
        cmd.Parameters.AddWithValue("@ProjectID", intProjectID);
        cmd.Parameters.AddWithValue("@CustomerID", intCustomerID);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static DataTable SelectActivityViewHeader(int intProjectID, int intCustomerID)
    //return Activity View header data for project/customer combination
    {
        //get a configured SqlCommand object
        SqlCommand cmd = DataAccess.CreateCommand();
        //set the stored procedure name
        cmd.CommandText = "SelectActivityViewHeader";
        //parameters
        cmd.Parameters.AddWithValue("@ProjectID", intProjectID);
        cmd.Parameters.AddWithValue("@CustomerID", intCustomerID);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static DataTable SelectActivityViewHeader(int intProjectToCustomerID)
    //return Activity View header data for project/customer combination
    {
        //get a configured SqlCommand object
        SqlCommand cmd = DataAccess.CreateCommand();
        //set the stored procedure name
        cmd.CommandText = "sp_ap_SelectActivityAnnualTotals";
        //parameters
        cmd.Parameters.AddWithValue("@ProjectToCustomerID", intProjectToCustomerID);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static DataTable SelectActivityMonthlySummary(int intProjectID, int intCustomerID)
    //return Activity View monthly summary data for project/customer combination
    {
        //get a configured SqlCommand object
        SqlCommand cmd = DataAccess.CreateCommand();
        //set the stored procedure name
        cmd.CommandText = "SelectActivityMonthlySummary";
        //parameters
        cmd.Parameters.AddWithValue("@ProjectID", intProjectID);
        cmd.Parameters.AddWithValue("@CustomerID", intCustomerID);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static DataTable SelectForecastTotals(int intProjectToCustomerID)
    //Select Forecast Totals for grid footer
    {
        //get a configured SqlCommand object
        SqlCommand cmd = DataAccess.CreateCommand();
        //set the stored procedure name
        cmd.CommandText = "SelectForecastTotals";
        //parameters
        cmd.Parameters.AddWithValue("@ProjectToCustomerID", intProjectToCustomerID);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static DataTable SelectForecastTotals(int intProjectToCustomerID, int intActivityTypeID, int intSpendTypeID)
    //Select Forecast Totals for grid footer
        //Override This is the latest code as on Feb 25 2015. Remove other overloads if unused in the system
    {
        //get a configured SqlCommand object
        SqlCommand cmd = DataAccess.CreateCommand();
        //set the stored procedure name 
        cmd.CommandText = "sp_ap_SelectForecastTotals";
        //parameters
        cmd.Parameters.AddWithValue("@ProjectToCustomerID", intProjectToCustomerID);
        cmd.Parameters.AddWithValue("@ActivityTypeID", intActivityTypeID);
        cmd.Parameters.AddWithValue("@SpendTypeID", intSpendTypeID);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static DataTable SelectActualTotals(int intProjectToCustomerID)
    //Select Actual Totals for grid footer
    {
        //get a configured SqlCommand object
        SqlCommand cmd = DataAccess.CreateCommand();
        //set the stored procedure name
        cmd.CommandText = "SelectActualTotals";
        //parameters
        cmd.Parameters.AddWithValue("@ProjectToCustomerID", intProjectToCustomerID);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static DataTable SelectActualTotals(int intProjectToCustomerID, int intSpendTypeID)
    //Select Actual Totals for grid footer
    //Override This is the latest code as on Feb 25 2015. Remove other overloads if unused in the system
    {
        //get a configured SqlCommand object
        SqlCommand cmd = DataAccess.CreateCommand();
        //set the stored procedure name
        cmd.CommandText = "sp_ap_SelectActualTotals";
        //parameters
        cmd.Parameters.AddWithValue("@ProjectToCustomerID", intProjectToCustomerID);
        cmd.Parameters.AddWithValue("@SpendTypeID", intSpendTypeID);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static DataRow CopyActivityToTable(Activity a, DataRow dr)
    {
        dr["ActivityID"] = a.ActivityID;
        //dr["ProjectToCustomerID"] = ProjectToCustomerID.Text;
        dr["ActivityName"] = a.ActivityName;
        dr["ShortText"] = a.ShortText;
        dr["ItemQuantity"] = a.ItemQuantity;
        dr["UnitPrice"] = String.Format("{0:0.0000}", a.UnitPrice);
        dr["MonthRequired"] = a.MonthRequired;
        dr["ProjectMonth"] = a.ProjectMonth;
        dr["MonthShortName"] = a.MonthShortName;
        //dr["POID"] = POID.Text;
        dr["DocumentNumber"] = a.DocumentNumber;
        dr["ActivityTypeID"] = a.ActivityTypeID;
        dr["ActivityTypeName"] = a.ActivityTypeName;
        dr["GLCode"] = a.GLCode; //added 29/05/14
        dr["CommodityCode"] = a.CommodityCode; //added 29/05/14
        dr["SpendTypeID"] = a.SpendTypeID;
        dr["SupplierID"] = a.SupplierID;
        dr["SupplierRefNo"] = a.SupplierRefNo;
        dr["Comments"] = a.Comments;
        dr["LastUpdatedBy"] = a.LastUpdatedBy;
        dr["CreatedBy"] = a.CreatedBy;
        dr["Month01"] = a.Month01;
        dr["Month02"] = a.Month02;
        dr["Month03"] = a.Month03;
        dr["Month04"] = a.Month04;
        dr["Month05"] = a.Month05;
        dr["Month06"] = a.Month06;
        dr["Month07"] = a.Month07;
        dr["Month08"] = a.Month08;
        dr["Month09"] = a.Month09;
        dr["Month10"] = a.Month10;
        dr["Month11"] = a.Month11;
        dr["Month12"] = a.Month12;
        dr["TotalCost"] = String.Format("{0:0.00}", a.UnitPrice * a.ItemQuantity);
        return dr;
    }

    public static DataTable BlankSelectedItemsTable()
    {
        DataTable tbl = new DataTable();
        tbl.Columns.Add("ActivityID");
        tbl.Columns.Add("ProjectToCustomerID");
        tbl.Columns.Add("ActivityName");
        tbl.Columns.Add("ShortText");
        tbl.Columns.Add("ActivityTypeName");
        tbl.Columns.Add("GLCode"); //added 29/05/14
        tbl.Columns.Add("CommodityCode");  //added 29/05/14
        tbl.Columns.Add("ItemQuantity");
        tbl.Columns.Add("UnitPrice");
        tbl.Columns.Add("MonthRequired");
        tbl.Columns.Add("ProjectMonth");
        tbl.Columns.Add("MonthShortName");
        tbl.Columns.Add("POID");
        tbl.Columns.Add("RebateID"); //added 27/02/12 for Rebate maint functionality
        tbl.Columns.Add("DocumentNumber");
        tbl.Columns.Add("ActivityTypeID");
        tbl.Columns.Add("SpendTypeID");
        tbl.Columns.Add("SupplierID");
        tbl.Columns.Add("SupplierRefNo");
        tbl.Columns.Add("Comments");
        tbl.Columns.Add("LastUpdatedBy");
        tbl.Columns.Add("CreatedBy");
        for (int i = 1; i <= 12; i++)
        {
            if (i < 10)
            {
                tbl.Columns.Add("Month0" + i.ToString(), System.Type.GetType("System.String"));
                tbl.Columns.Add("ReceiptedStatus0" + i.ToString());
            }
            else
            {
                tbl.Columns.Add("Month" + i.ToString(), System.Type.GetType("System.String"));
                tbl.Columns.Add("ReceiptedStatus" + i.ToString());
            }
        }
        tbl.Columns.Add("TotalCost");
        return tbl;
    }

    public static int SetUserActivityView(string strUserName, int intActivityView)
    //Save user's Activity View settings
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "SetUserActivityView";

        // add parameters with their values
        cmd.Parameters.AddWithValue("@UserName", strUserName);
        cmd.Parameters.AddWithValue("@ActivityView", intActivityView);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        return intRowsAffected;
    }

    public static int SetUserActivityRows(string strUserName, int intNoOfRows)
    //Save user's No Of Row settings
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "SetUserActivityRows";

        // add parameters with their values
        cmd.Parameters.AddWithValue("@UserName", strUserName);
        cmd.Parameters.AddWithValue("@NoOfRows", intNoOfRows);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        return intRowsAffected;
    }

    public static int UpdateActivityLastUpdated(int intActivityID, string strLastUpdatedBy, int intInsertHistoryIndicator)
    //Stamp the current date time and user on activity (for when rebate approved)
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_UpdateActivityLastUpdated";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@ActivityID", intActivityID);
        cmd.Parameters.AddWithValue("@LastUpdatedBy", strLastUpdatedBy);
        cmd.Parameters.AddWithValue("@InsertHistoryIndicator", intInsertHistoryIndicator);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        return intRowsAffected;
    }
}
