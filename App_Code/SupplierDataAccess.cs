﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;

/// <summary>
/// Summary description for SupplierDataAccess
/// </summary>
public class SupplierDataAccess
{
	public SupplierDataAccess()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static DataTable SelectSupplier(int intSupplierID)
    //return supplier details
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "SelectSupplier";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@SupplierID", intSupplierID);
        //execute stored procedure and store results in a data table
        DataTable tbl = DataAccess.ExecuteSelectCommand(cmd);
        return tbl;
    }

    public static int UpdateSupplier(int intSupplierID, string strSupplierName, string strSAPVendorCode, string strActiveIndicator, string strComments, string strLastUpdatedBy)
    //update an existing Supplier
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_UpdateSupplier";

        // add parameters with their values
        cmd.Parameters.AddWithValue("@SupplierID", intSupplierID);
        cmd.Parameters.AddWithValue("@SupplierName", strSupplierName);
        cmd.Parameters.AddWithValue("@SAPVendorCode", strSAPVendorCode);
        cmd.Parameters.AddWithValue("@ActiveIndicator", strActiveIndicator);
        cmd.Parameters.AddWithValue("@Comments", strComments);
        cmd.Parameters.AddWithValue("@LastUpdatedBy", strLastUpdatedBy);

        // execute the query to update the database
        int intResult = DataAccess.ExecuteUpdateCommand(cmd);
        return intResult;
    }

    public static int InsertSupplier(string strSupplierName, string strSAPVendorCode, string strActiveIndicator, string strComments, string strCreatedBy)
    //create a new Supplier and return the ID
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "sp_ap_InsertSupplier";

        // add parameters with their values
        cmd.Parameters.AddWithValue("@SupplierName", strSupplierName);
        cmd.Parameters.AddWithValue("@SAPVendorCode", strSAPVendorCode);
        cmd.Parameters.AddWithValue("@ActiveIndicator", strActiveIndicator);
        cmd.Parameters.AddWithValue("@Comments", strComments);
        cmd.Parameters.AddWithValue("@CreatedBy", strCreatedBy);

        // execute the query to update the database
        int intSupplierID = DataAccess.ExecuteScalarReturnIdentity(cmd);
        return intSupplierID;
    }

    public static int DeleteSupplier(int intSupplierID)
    //delete an existing Supplier
    {
        SqlCommand cmd = DataAccess.CreateCommand();
        cmd.CommandText = "DeleteSupplier";
        // add parameters with their values
        cmd.Parameters.AddWithValue("@SupplierID", intSupplierID);
        // execute the query to update the database
        int intRowsAffected = DataAccess.ExecuteUpdateCommand(cmd);
        return intRowsAffected;
    }
}
