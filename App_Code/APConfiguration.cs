﻿using System.Configuration;

/// <summary>
/// Repository for Colourcards Configuration settings including database and mail server settings
/// </summary>
public static class APConfiguration
{
	//Cache the connection string
	private static string dbConnectionString;
    //Cache the data provider name
    private static string dbProviderName; 

	static APConfiguration()
	{
        dbConnectionString = ConfigurationManager.ConnectionStrings["APConnectionString"].ConnectionString;
    }

    //returns the connection string for the Wyvern database
    public static string DbConnectionString
    {
        get
        {
            return dbConnectionString;
        }
    }

    //returns the data provider name
    public static string DbProviderName
    {
        get
        {
            return dbProviderName;
        }
    }

    //return the address of the mail server
    public static string MailServer
    {
        get
        {
            return ConfigurationManager.AppSettings["MailServer"];
        }
    }

    //return the email username
    public static string MailUsername
    {
        get
        {
            return ConfigurationManager.AppSettings["MailUsername"];
        }
    }

    //return the email password
    public static string MailPassword
    {
        get
        {
            return ConfigurationManager.AppSettings["MailPassword"];
        }
    }

    //return the email account
    public static string MailFrom
    {
        get
        {
            return ConfigurationManager.AppSettings["MailFrom"];
        }
    }

    //return the default view in the Activity Grid View screen (if user's settings not found)
    public static string DefaultActivityView
    {
        get
        {
            return ConfigurationManager.AppSettings["DefaultActivityView"];
        }
    }

    //return the default no of rows in the Activity Grid View screen (if user's settings not found)
    public static string DefaultActivityRows
    {
        get
        {
            return ConfigurationManager.AppSettings["DefaultActivityRows"];
        }
    }

    //Send error log emails?
    public static bool EnableErrorLogEmail
    {
        get
        {
            return bool.Parse(ConfigurationManager.AppSettings["EnableErrorLogEmail"]);
        }
    }

    //return the email address to send error emails to
    public static string ErrorLogEmail
    {
        get
        {
            return ConfigurationManager.AppSettings["ErrorLogEmail"];
        }
    }

}
