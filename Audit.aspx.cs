﻿using System;
using System.Data;

public partial class Audit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //check session not expired
        if (Session["SelectedProjectToCustomerID"] == null)
        //and high enough user level (removed 02/12/12)
        // || Convert.ToInt16(Session["CurrentUserLevelID"]) >= 15
        {
            Response.Redirect("ProjectView.aspx");
        }
        else
        {
            //retrieve the ProjectID and CustomerID for this ProjectToCustomer record
            DataTable tbl;
            if (IsPostBack)
            {
                tbl = ProjectDataAccess.SelectProjectToCustomer(Convert.ToInt32(ProjectToCustomerID.Text));
            }
            else
            {
                tbl = ProjectDataAccess.SelectProjectToCustomer(Convert.ToInt32(Session["SelectedProjectToCustomerID"]));
            }
            DataRow dr = tbl.Rows[0];
            ProjectID.Text = dr["ProjectID"].ToString();
            CustomerID.Text = dr["CustomerID"].ToString();
            ProjectToCustomerID.Text = Session["SelectedProjectToCustomerID"].ToString();

            //populate the header values
            DataTable t = ActivityDataAccess.SelectActivityViewHeader(int.Parse(ProjectID.Text), int.Parse(CustomerID.Text));
            DataRow r = t.Rows[0];
            IONumber.Text = r["IONumber"].ToString();
            ProjectName.Text = r["ProjectName"].ToString();
            CustomerName.Text = r["CustomerName"].ToString();

            //Populate the activity audit grid
            grdAudit1.DataBind();

            //Populate the budget audit grid
            grdAudit2.DataBind();

            //populate the reallocation grids
            ReallocationGrids1.RefreshGrids(Convert.ToInt32(ProjectToCustomerID.Text));
        }
    }

    #region gridevents

        protected void grdAudit1_PageIndexChanged(object sender, EventArgs e)
        {

        }

        protected void grdAudit1_Sorted(object sender, EventArgs e)
        {

        }

        protected void grdAudit2_PageIndexChanged(object sender, EventArgs e)
        {

        }

        protected void grdAudit2_Sorted(object sender, EventArgs e)
        {

        }

    #endregion
}
