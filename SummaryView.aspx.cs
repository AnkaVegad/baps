﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SummaryView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //check for session expired
        if (Session["SelectedProjectToCustomerID"] == null)
        {
            Response.Redirect("ProjectView.aspx");
        }
        //retrieve the ProjectID and CustomerID for this ProjectToCustomer record
        DataTable tbl = ProjectDataAccess.SelectProjectToCustomer(Convert.ToInt32(Session["SelectedProjectToCustomerID"]));
        DataRow dr = tbl.Rows[0];
        ProjectID.Text = dr["ProjectID"].ToString();
        CustomerID.Text = dr["CustomerID"].ToString();
        ProjectToCustomerID.Text = Session["SelectedProjectToCustomerID"].ToString();

        //populate the header values
        DataTable t = ActivityDataAccess.SelectActivityViewHeader(int.Parse(ProjectID.Text), int.Parse(CustomerID.Text));
        DataRow r = t.Rows[0];
        IONumber.Text = r["IONumber"].ToString();
        ProjectName.Text = r["ProjectName"].ToString();
        CustomerName.Text = r["CustomerName"].ToString();
        AnnualTotals1.PopulateAnnualValues(r);

        MonthlyTotals1.PopulateGrid(Convert.ToInt32(ProjectID.Text), Convert.ToInt32(CustomerID.Text));
    }

}
