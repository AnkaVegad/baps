<%@ Page Title="" Language="C#" MasterPageFile="~/AP.master" AutoEventWireup="true" CodeFile="ActualDetailView.aspx.cs" Inherits="ActualDetailView" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <table border="0" width="1095px"><tr><td align="left" style="font-weight: bold; font-size: 12px">
        <asp:Label ID="Label901" Text="A&P Actual Values" runat="server" CssClass="page_header"></asp:Label>    
    </td></tr></table>
    <table style="width:600px" border="0">
        <tr><td style="font-size:2px">&nbsp;</td></tr>
        <tr>
            <td>IONumber</td>
            <td>
                <asp:TextBox ID="txtSearch" Runat="server" Width="120px" MaxLength="12"></asp:TextBox>
            </td>
            <td>Year</td>
            <td>
                <asp:DropDownList id="cboYear" runat="server" width="120px" DataTextField="EntityName" DataValueField="ID" />
                <asp:Button ID="cmdGo" Text="Go" Runat="server" CausesValidation="false" OnClick="cmdGo_Click" />
            </td>
        </tr>
    </table><br /><br />

    <!--Actuals Detail Grid-->
    <div id="project_grid">
        <asp:GridView ID="grd" runat="server" width="680px"
            AutoGenerateColumns="False" GridLines="Vertical"
            RowStyle-Wrap="false" EmptyDataText="No Records Found" 
            HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
            SelectedRowStyle-Wrap="false"             
            AllowSorting="false" AllowPaging="False"
            onrowcancelingedit="grd_RowCancelingEdit" OnRowEditing="grd_RowEditing" OnRowUpdating="grd_RowUpdating"
            DataKeyNames="ActualID" EnableModelValidation="True" >
            <Columns>
                <asp:BoundField DataField="ActualID" ReadOnly="True" Visible="False" />
                <asp:BoundField DataField="ActualMonth" ReadOnly="True" HeaderText="Actual Month" SortExpression="ActualMonth" />
                <asp:BoundField DataField="ActualYear" ReadOnly="True" HeaderText="Actual Year" SortExpression="ActualYear" />
                <asp:TemplateField HeaderText="Project Year">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtProjectYear" runat="server" Text='<%# Bind("ProjectYear") %>' TextMode="SingleLine" Width="50px" MaxLength="4" />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("ProjectYear") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="GLCode" ReadOnly="True" HeaderText="GL Code" SortExpression="GLCode" />
                <asp:BoundField DataField="ActualValue" ReadOnly="True" HeaderText="Actual Value" SortExpression="ActualValue" />
                <asp:CommandField HeaderText="Actions" ShowEditButton="True" ShowDeleteButton="False" 
                    ShowHeader="True" />
            </Columns>
            <RowStyle CssClass="grid_row" Wrap="False" />
            <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
            <HeaderStyle CssClass="grid_header" Wrap="False" />
            <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
        </asp:GridView>
    </div>
</asp:Content>

