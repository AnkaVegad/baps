﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserAdminView : System.Web.UI.Page
{
    string strSingular = "User";

    protected void Page_Load(object sender, EventArgs e)
    {
        //check for high enough user level
        if (Convert.ToInt16(Session["CurrentUserLevelID"]) > 10)
        {
            Response.Redirect("ProjectView.aspx");
        }
        if (!IsPostBack)
        {
            mvw.ActiveViewIndex = 0;
        }
    }

#region events

    protected void cmdNew_Click(object sender, EventArgs e)
    {
        mvw.ActiveViewIndex = 1;
        //enable controls for new record
        UserAdminEdit1.NewRecord();
        Session["Mode"] = "New";
        UserAdminEdit1.HeaderText = Session["Mode"].ToString() + " " + strSingular;
    }

    protected void cmdDelete_Click(object sender, EventArgs e)
    {
        try //in case referential integrity prevents deletion
        {
            UserDataAccess.DeleteUser(Convert.ToInt32(UserAdminGrid1.SelectedValue));
            UserAdminEdit1.ClearControls();
            // update grid
            UserAdminGrid1.DataBind();
        }
        catch
        {
            MsgBox("Message", "Could not delete " + strSingular);
        }
    }

    protected void cmdEdit_Click(object sender, EventArgs e)
    //amended 19/01/12 to prevent editing user of higher user level
    {
        try //added 19/12/12 to try to establish cause of userlevelid lookup failing
        {
            if (UserAdminGrid1.SelectedValue != null)
            {
                if (Convert.ToInt16(Session["CurrentUserLevelID"]) - Convert.ToInt16(Common.ADOLookup("UserLevelID", "tblUser", "UserID = " + UserAdminGrid1.SelectedValue)) > 1)
                {
                    MsgBox("Message", "No authorisation to edit");
                    //       MsgBox("Message", Common.ADOLookup("UserLevelID", "tblUser", "UserID = " + UserAdminGrid1.SelectedValue)); 
                }
                else
                {
                    UserAdminEdit1.PopulateForm(Convert.ToInt32(UserAdminGrid1.SelectedValue));
                    mvw.ActiveViewIndex = 1;
                    Session["Mode"] = "Edit";
                    UserAdminEdit1.HeaderText = Session["Mode"].ToString() + " " + strSingular;
                }
            }
        }
        catch //in case no selection in grid 
        {
            MsgBox("BAPS", "Selected User ID: " + UserAdminGrid1.SelectedValue); 
        } 
    }

    protected void cmdUpdate_Click(object sender, EventArgs e)
    {
        string strMessage;
        if (Page.IsValid)
        {
            if (Session["Mode"].ToString() == "New")
            {
                strMessage = UserAdminEdit1.CreateUser();
            }
            else //must be Edit mode
            {
                strMessage = UserAdminEdit1.UpdateUser();
            }
            if (strMessage == "")
            {
                mvw.ActiveViewIndex = 0;
                //refresh grid
                UserAdminGrid1.DataBind();
            }
            else
            {
                MsgBox("", strMessage);
            }
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        mvw.ActiveViewIndex = 0;
    }

#endregion

#region methods

    protected void MsgBox(string strTitle, string strMessage)
    {
        ClientScript.RegisterStartupScript(this.GetType(), strTitle, "alert('" + Common.Left(strMessage, 35) + "');", true);
    }

#endregion
}
