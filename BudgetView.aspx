﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AP.master" AutoEventWireup="true" CodeFile="BudgetView.aspx.cs" Inherits="BudgetView" %>

<%@ Register src="User_Controls/AnnualTotals.ascx" tagname="AnnualTotals" tagprefix="uc1" %>
<%@ Register src="User_Controls/ProjectToCustomerEdit.ascx" tagname="ProjectToCustomerEdit" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <!--Page Header-->
    <table border="0" width="1100px">
        <tr>
            <td align="left">
                <asp:Label ID="Label1" Text="Budgets" runat="server" CssClass="page_header"></asp:Label>
            </td><td>
                <asp:TextBox ID="ProjectID" runat="server" visible="false"></asp:TextBox>
            </td>
            <td align="right">
                <asp:Label ID="Label901" Text="Project" runat="server" CssClass="page_header"></asp:Label>
            </td>
            <td align="left">
                <asp:Label ID="ProjectName" runat="server" CssClass="page_header_text" Width="250px" />
            </td><td>
                <asp:TextBox ID="CustomerID" runat="server" visible="false"></asp:TextBox>
            </td>
            <td align="right">
                <asp:Label ID="Label902" Text="Customer" runat="server" CssClass="page_header"></asp:Label>
            </td>
            <td align="left">
                <asp:Label ID="CustomerName" runat="server" CssClass="page_header_text" Width="150px" />
            </td><td>
                <asp:TextBox ID="ProjectToCustomerID" runat="server" visible="false" Width="30px"></asp:TextBox>
            </td>
            <td align="right">
                <asp:Label ID="Label903" Text="IO Number" runat="server" CssClass="page_header" />
            </td>
            <td align="left">
                <asp:Label ID="IONumber" runat="server" CssClass="page_header_text" Width="110px" />
            </td>
            <td align="right">
                <asp:Label ID="Label997" Text="Year" runat="server" CssClass="page_header"></asp:Label>
            </td>
            <td align="left">
                <asp:Label ID="ProjectYear" Text="" runat="server" CssClass="page_header_text" Width="50px" />
            </td>
        </tr>
    </table>
    <br /><br />

    <!--Project Annual Totals Area--> 
    <uc1:AnnualTotals ID="AnnualTotals1" runat="server" />

    <!--Edit Project Details Area-->
    <uc2:ProjectToCustomerEdit ID="ProjectToCustomerEdit1" runat="server" Visible="false" />

    <!--Buttons-->
    <table>
        <tr><td>
            <asp:Button ID="cmdEdit" runat="server" Text="Edit Budget" Width="100px" Visible="false"
                onclick="cmdEdit_Click" CssClass="form_button" />
        </td><td>
            <asp:Button ID="cmdReallocateFrom" runat="server" Text="Reallocate From" Width="120px" Visible="false"
                onclick="cmdReallocateFrom_Click" CssClass="form_button" />
        </td><td>
            <asp:Button ID="cmdReallocateTo" runat="server" Text="Reallocate To" Width="120px" Visible="false"
                onclick="cmdReallocateTo_Click" CssClass="form_button" />
        </td><td>
            <asp:Button ID="cmdBack" runat="server" Text="Back To Projects" causesvalidation="false"
                    width="120px" onclick="cmdBack_Click" CssClass="form_button" Enabled="true" />
        </td></tr>
        <tr><td>
            <asp:Button ID="cmdUpdate" runat="server" Text="Save" causesvalidation="true" Visible="false"
                onclick="cmdUpdate_Click" CssClass="form_button" />            
        </td><td>
            <asp:Button ID="cmdCancel" runat="server" Text="Cancel" causesvalidation="false" Visible="false"
                onclick="cmdCancel_Click" CssClass="form_button" />            
        </td></tr>
    </table><br />

    <!--Reallocations From Grid-->
    Reallocations From This Project/Customer:<br /><br />
    <div id="activity_grid">
        <asp:GridView ID="grdReallocationFrom" runat="server" width="1070px"
            AutoGenerateColumns="False" GridLines="Vertical"
            RowStyle-Wrap="false" EmptyDataText="No Records Found" 
            HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
            SelectedRowStyle-Wrap="false"
            AllowSorting="false" AllowPaging="false"
            DataKeyNames="ReallocationID" >
            <Columns>
                <asp:BoundField DataField="ReallocationID" InsertVisible="False" ReadOnly="True" Visible="False" />
                <asp:BoundField DataField="CreatedBy" HeaderText="User" SortExpression="CreatedDate" ItemStyle-Width="100px" />
                <asp:BoundField DataField="CreatedDate" HeaderText="Date" SortExpression="CreatedDate" ItemStyle-Width="50px" DataFormatString="{0:dd/MM/yy}" />
                <asp:BoundField DataField="BudgetAmount" HeaderText="Amount" SortExpression="CreatedDate" ItemStyle-Width="50px" />
                <asp:BoundField DataField="ToProjectName" HeaderText="To Project" SortExpression="CreatedDate" ItemStyle-Width="250px" />
                <asp:BoundField DataField="ToCustomerName" HeaderText="To Customer" SortExpression="CreatedDate" ItemStyle-Width="100px" />
                <asp:BoundField DataField="FromSpendTypeName" HeaderText="From" SortExpression="CreatedDate" ItemStyle-Width="40px" />
                <asp:BoundField DataField="ToSpendTypeName" HeaderText="To" SortExpression="CreatedDate" ItemStyle-Width="40px" />
                <asp:BoundField DataField="Comments" HeaderText="Comments" SortExpression="CreatedDate" ItemStyle-Width="200px" />
            </Columns>
            <RowStyle CssClass="grid_row" Wrap="False" />
            <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
            <HeaderStyle CssClass="grid_header" Wrap="False" />
            <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
        </asp:GridView>
        <br />
        <br />

        <!--Reallocations To Grid-->
        Reallocations To This Project/Customer:<br /><br />
        <asp:GridView ID="grdReallocationTo" runat="server" width="1070px"
            AutoGenerateColumns="False" GridLines="Vertical"
            RowStyle-Wrap="false" EmptyDataText="No Records Found" 
            HeaderStyle-Wrap="false" AlternatingRowStyle-Wrap="false"
            SelectedRowStyle-Wrap="false"
            AllowSorting="false" AllowPaging="false"
            DataKeyNames="ReallocationID" >
            <Columns>
                <asp:BoundField DataField="ReallocationID" InsertVisible="False" ReadOnly="True" Visible="False" />
                <asp:BoundField DataField="CreatedBy" HeaderText="User" SortExpression="CreatedDate" ItemStyle-Width="100px" />
                <asp:BoundField DataField="CreatedDate" HeaderText="Date" SortExpression="CreatedDate" ItemStyle-Width="50px" DataFormatString="{0:dd/MM/yy}" />
                <asp:BoundField DataField="BudgetAmount" HeaderText="Amount" SortExpression="CreatedDate" ItemStyle-Width="50px" />
                <asp:BoundField DataField="FromProjectName" HeaderText="From Project" SortExpression="CreatedDate" ItemStyle-Width="250px" />
                <asp:BoundField DataField="FromCustomerName" HeaderText="From Customer" SortExpression="CreatedDate" ItemStyle-Width="100px" />
                <asp:BoundField DataField="FromSpendTypeName" HeaderText="From" SortExpression="CreatedDate" ItemStyle-Width="40px" />
                <asp:BoundField DataField="ToSpendTypeName" HeaderText="To" SortExpression="CreatedDate" ItemStyle-Width="40px" />
                <asp:BoundField DataField="Comments" HeaderText="Comments" SortExpression="CreatedDate" ItemStyle-Width="200px" />
            </Columns>
            <RowStyle CssClass="grid_row" Wrap="False" />
            <SelectedRowStyle CssClass="grid_selected" Wrap="False" />
            <HeaderStyle CssClass="grid_header" Wrap="False" />
            <AlternatingRowStyle CssClass="grid_alternating_row" Wrap="False" />
        </asp:GridView>
    </div><br />
    <br />

</asp:Content>

