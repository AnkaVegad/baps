﻿<%@ Application Language="C#" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        // Code that runs on application startup
    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs

    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started
        //*** S2 domain is hardcoded ***
        //string strUserName = User.Identity.Name;
       string strUserName = "alec.hagues";
        //if (Common.Left(strUserName, 2) == "S2") { strUserName = strUserName.Substring(3); }
        System.Data.DataTable tbl = UserDataAccess.SelectActiveUser(strUserName);
	    if (tbl.Rows.Count == 0) 
        { 
            Response.Redirect("InvalidUser.aspx"); 
        }
	    else 
        {
            Session["CurrentUserName"] = tbl.Rows[0]["UserName"].ToString();
            Session["CurrentUserLevelID"] = tbl.Rows[0]["UserLevelID"].ToString();
            Session["CurrentUserLevelName"] = tbl.Rows[0]["UserLevelName"].ToString();
            Session["SelectedYear"] = Common.GetOptionValue("CurrentYear");
            Response.Cookies["ShowWorklist"]["value"] = tbl.Rows[0]["ShowWorklistPopup"].ToString();
      	}
    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends
        //(but only when it times out - AH). 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.
    
        //save the session values to the user's profile in the database
//        UserDataAccess.UpdateUserSelections(Convert.ToInt32(Session["LastCustomerID"]), Convert.ToInt32(Session["LastBudgetResponsibilityAreaID"]), Convert.ToInt32(Session["LastBrandID"]), Session["CurrentUserName"].ToString());
    }
       
</script>
