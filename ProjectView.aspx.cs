﻿using System;
using System.Web.UI;

public partial class ProjectView : System.Web.UI.Page
{
    string strSingular = "Project";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) 
        { 
            mvw.ActiveViewIndex = 0;
            //updated AH 20/08/2012 - only Administrator and above should be able to add, edit or delete
            if (Convert.ToInt16(Session["CurrentUserLevelID"]) < 15)
            {
                cmdUpdate.Enabled = true;
                cmdNew.Enabled = true;
                cmdDelete.Enabled = true;
            }
            //updated AH 20/08/2012 - everyone except reader should see their worklist summary 
            if (Convert.ToInt16(Session["CurrentUserLevelID"]) <= 38)
            {
                WorklistSummary1.Visible = true;
            }
        }
    }

#region events

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        ProjectToCustomerEdit1.ClearControls();
        mvw.ActiveViewIndex = 0;
    }

    protected void cmdUpdate_Click(object sender, EventArgs e)
    {
        //try
        //{
            if (Page.IsValid)
            {
                string strMessage;
                if (ProjectToCustomerEdit1.ProjectToCustomerIDValue == "0") //New
                {
                    strMessage = ProjectToCustomerEdit1.CreateRecord();
                }
                else
                {
                    strMessage = ProjectToCustomerEdit1.UpdateRecord();
                }
                if (strMessage == "[none]")
                {
                    // switch to grid and refresh
                    mvw.ActiveViewIndex = 0;
                    ProjectGrid1.Refresh();
                }
                else
                {
                    MsgBox("Error", strMessage);
                }
            }
        //}
        //catch
        //{
        //    MsgBox("Error", "Could not save " + strSingular);
        //}
    }

    protected void cmdDelete_Click(object sender, EventArgs e)
    {
        try
        {
            if (Common.CheckAuthorisations(Convert.ToInt32(ProjectGrid1.SelectedValue), Session["CurrentUserName"].ToString()) > 0)
            {
                //delete the selected ProjectToCustomer record
                ProjectDataAccess.DeleteProjectToCustomer(Convert.ToInt32(ProjectGrid1.SelectedValue));
                ProjectGrid1.Refresh();
            }
            else
            {
                MsgBox("BAPS", "No authorisation for project");
            }
        }
        catch
        {
            MsgBox("Error", "Could not delete " + strSingular);
        }
    }

    protected void cmdMaster_Click(object sender, EventArgs e)
    {
        //try 
        //{
        //if (Common.CheckAuthorisations(Convert.ToInt32(ProjectGrid1.SelectedValue), Session["CurrentUserName"].ToString()) > 0)
        //{
        //enable controls to edit record
        int intProjectID = Convert.ToInt32(Common.ADOLookup("ProjectID", "tblProjectToCustomer", "ProjectToCustomerID = " + ProjectGrid1.SelectedValue));
        Project a = ProjectDataAccess.SelectProject(intProjectID);
        ProjectEdit1.PopulateForm(a);
        ProjectEdit1.ToggleControlState(false);
        mvw.ActiveViewIndex = 2;
        //***lock fields for editing***//
        //}
        //else
        //{
        //    MsgBox("BAPS", "No authorisation for project");
        //}
        //}
        //catch { } //in case no selected item in grid
    }

    protected void cmdEdit_Click(object sender, EventArgs e)
    {
        //try 
        //{
            //if (Common.CheckAuthorisations(Convert.ToInt32(ProjectGrid1.SelectedValue), Session["CurrentUserName"].ToString()) > 0)
            //{
                //enable controls to edit record
                ProjectToCustomerEdit1.PopulateForm(Convert.ToInt32(ProjectGrid1.SelectedValue));
                mvw.ActiveViewIndex = 1;
            //}
            //else
            //{
            //    MsgBox("BAPS", "No authorisation for project");
            //}
        //}
        //catch { } //in case no selected item in grid
    }

    protected void cmdNew_Click(object sender, EventArgs e)
    {
        try 
        {
            if (Common.CheckAuthorisations(Convert.ToInt32(ProjectGrid1.SelectedValue), Session["CurrentUserName"].ToString()) > 0)
            {
                //enable controls for new ProjectToCustomer record for same ProjectID as selection
                ProjectToCustomerEdit1.NewRecord(Convert.ToInt32(ProjectGrid1.SelectedProjectID));
                mvw.ActiveViewIndex = 1;
            }
            else
            {
                MsgBox("BAPS", "No authorisation for project");
            }
        }
        catch { } //in case no selected item in grid
    }

    protected void cmdBudget_Click(object sender, EventArgs e)
    {
        //will bounce back if no selected ProjectToCustomerID
        Response.Redirect("BudgetView.aspx");
    }

    protected void cmdActivities_Click(object sender, EventArgs e)
    {
        //will bounce back if no selected ProjectToCustomerID
        Response.Redirect("Default.aspx");
    }

#endregion

#region methods

    protected void MsgBox(string strTitle, string strMessage)
    {
        ClientScript.RegisterStartupScript(this.GetType(), strTitle, "alert('" + Common.Left(strMessage, 35) + "');", true);
    }

#endregion

}
